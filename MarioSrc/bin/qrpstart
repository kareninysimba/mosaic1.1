#!/bin/csh -f
#
# PLRUN -- Run pipeline.

set pipeApp = "NEWFIRM"

if ( "$1" == "-h" ) then
    goto Usage
endif

if ( ! -e ${NHPPS_PIPESRC}/${pipeApp}/ ) then
    echo "Unknown pipeApp ($pipeApp)"
    exit 0
endif

# Set the nodes. Currently this is defined by environment variables.
switch (`hostname`)
case nfpipe-01:
case nfpipe-02:
    set dmnode = $NHPPS_NODE_DM
    set nodes  = $PROCNODES
    set dsnode = $NHPPS_NODE_DS
    breaksw
default:
    echo "plrun: must be logged into a pipeline machine"
    exit 1
endsw

# Configuration directory.
cd ${NHPPS_PIPESRC}/${pipeApp}/config

# Check arguments.
set cflag = "start"
set rflag = 0
set sflag = 0
set aflag = 0
set vflag = 0
set config = ""
while ("$1" != "")
    switch ("$1")
    case -h:
    case --help:
        goto Usage
    case -c:
	shift
	set cflag = $1
	breaksw
    case -l:
	grep -FH '#*' *
	exit 0
    case -r:
	set rflag = 1
	breaksw
    case -s:
	set sflag = 1
	breaksw
    case -a:
	set aflag = 1
	breaksw
    case -d:
	shift
        set dmnode = $1
	breaksw
    case -e:
	shift
        set dsnode = $1
	breaksw
    case -n:
	shift
	set nodes = $1
	breaksw
    case -v:
	set vflag = 1
	breaksw
    default:
	break
    endsw

    shift
end

if ($config == "") set config = "QRP"

# Check configuration.
if (! (-e $config)) then
    echo "#* ERROR: Unknown configuration ($config)"
    printf "#*\n#* Available configuration:\n"
    grep -FH '#*' *
    exit 1
endif

echo ==================================================================
echo Starting the pipeline
date
echo Data manager node = "$dmnode"
echo Directory server node =  "$dsnode"
echo Processing nodes =  "$nodes"

if ($sflag) then
    echo Stopping any current pipeline processes...
    plssh "$dmnode,$nodes" stopall >>& /dev/null
endif

if ($rflag && $dmnode != "") then
    echo Resetting pipeline scheduler database...
    plssh "$dmnode" psqreset submitted resubmit >>& /dev/null
endif

switch ($cflag)
case newrun:
    echo Cleaning pipeline processing data directories...
    plssh "$dmnode,$nodes" cleanall $pipeApp +MarioHome +MarioCal -DIR -DPS >>& /dev/null
    breaksw
case restart:
    echo Cleaning pipeline processing data directories...
    plssh "$dmnode,$nodes" cleanall $pipeApp +MarioHome +MarioCal -DIR -DTS -DPS -NDP >>& /dev/null
    breaksw
case start:
    echo Cleaning pipeline processing data directories...
    plssh "$dmnode,$nodes" cleanall $pipeApp +MarioHome +MarioCal -DTS -DPS -NDP >>& /dev/null
    breaksw
case all:
    echo Cleaning all pipeline data directories...
    plssh "$dmnode,$nodes" cleanall $pipeApp +MarioHome +MarioCal >>& /dev/null
    breaksw
endsw

if ($dmnode != "") then
    echo Starting the name server...
    plsshf "$dmnode" serverns >>& /dev/null
endif
if ($dmnode != "") then
    echo Starting the directory server...
    plsshf "$dsnode" serverds >>& /dev/null
endif
if ($dmnode != "") then
    echo Starting the calibration and data manager servers...
    if ($vflag) then
	plsshf "$dmnode,$nodes" serverdm --app=$pipeApp --verbose >>& /dev/null
    else
	plsshf "$dmnode,$nodes" serverdm --app=$pipeApp >>& /dev/null
    endif
endif
sleep 2

echo Starting the node managers...
plsshf "$dmnode,$nodes" servernm >>& /dev/null
sleep 2

# Run the configuration.
echo source $config "$dmnode" "$nodes"
source $config "$dmnode" "$nodes"

# Check the status.
plstatus

echo Pipeline started successfully
echo ==================================================================

exit 0

Usage:
    echo '#* Usage: plrun pipeApp [options] [configuration]'
    echo '#*   -a           Auto-Submit datasets.'
    echo '#*   -c start|restart|all|none  Clean pipeline data directories'
    echo '#*   -h           Print this help'
    echo '#*   -l           List available configurations'
    echo '#*   -n nodes     List of processing nodes (comma separated list)'
    echo '#*   -r           Reset submitted PSQ entries for restart'
    echo '#*   -s           Stop the pipeline servers first'
    echo '#*   -v           Verbose'
    exit 0
