#!/usr/bin/env python

import os
import sys
import string
import getopt
import sqlite
import time

def main(myname, argv):

    while True:
        loop( myname, argv )
        time.sleep(300)

def loop(myname, argv):

    # Set default values, if available
    recent = 1
    defnumrecent = 16
    numrecent = defnumrecent
    average = 0
    dtime = 300
    graph = 0
    filter = 0
    usage = 0

    global debug
    debug = 0

    try:
        opts, args = getopt.getopt(argv, 'gdr:a:f:h', ['graph', 'debug',
                                                   'recent=', 'average=',
                                                   'filter=', 'help' ] )
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-d', '--debug'):
            debug = 1
        elif opt in ('-g', '--graph'):
            graph = 1
        elif opt in ('-r', '--recent'):
            numrecent = arg
        elif opt in ('-a', '--average'):
            dtime = arg
            average = 1
        elif opt in ('-f', '--filter'):
            filter = 1
            selectedfilter = arg
        elif opt in ('-h', '--help'):
            usage = 1

    if usage:
        print 'Usage: %s [OPTIONS]' % sys.argv[0]
        print 'Display the seeing, photometric zeropoint, and background levels'
        print 'for recent exposures.'
        print '    -a, --average <time interval>     NOT IMPLEMENTED YET'
        print '        Average data quality parameters over the requested time interval.'
        print '    -d, --debug'
        print '    -f, --filter <filter name>'
        print '        Only display data quality parameters for the requested filter.'
        print '    -g, --graph                       NOT IMPLEMENTED YET'
        print '        Display the data quality parameters in graphical form.'
        print '    -h, --help'
        print '    -r, --recent <number of recent exposures>'
        print '        Show the data quality parameters for the requested number of'
        print '        exposures. The default value is %d.' % defnumrecent
        sys.exit(1)

    if debug:
        print 'Graph = %s' % graph
        print 'Debug = %s' % debug
        print 'Recent = %s (%s)' % (recent, numrecent)
        print 'Average = %s (%s)' % (average, dtime)

    success = False
    while not success:

        # Set up a connection to the database
        try:
            con = sqlite.connect( os.environ['DMData']+"/NEWFIRM/catalog.db" )
        except:
            print 'Could not open database file catalog.db'
        else:
            success = 1
        try:
            cur = con.cursor()
        except:
            print 'Could not establish a connection to the database'
        else:
            success = 2

        # Get all the relevant data from the database
        query = 'select id_str,obsid,observeMJD,dqsemef,dqphazp,dqskaval,filter from ProcessingData where id_str like \'%_00\' and filter != \'None\' '
        if filter:
            query += 'and filter == \'%s\'' % selectedfilter
        query += 'order by observeMJD desc limit '+str(numrecent)
        try:
            cur.execute( query )
        except:
            pass
        else:
            results = cur.fetchall()
            success = 3

            print "\n--------------------------------------------------------------------------------"
	    print "%12s %16s  %6s %7s  %7s  %5s %13s" % ("Name", "Date/Time", "Seeing", "Mzero", "Bkg", "Filter", "MJD")
            print "                               arcsec     mag      ADU"
            print "--------------------------------------------------------------------------------"
            nrow = 0
            for row in results:
		name = string.split (row[0], '_', 2)
                dt = string.split (row[1], '.', 2)
                if row[3]==None:
                    see = '-- '
                    bck = '-- '
                else:
                    see = '%4.2f' % row[3]
                    bck = '%8.1f' % row[5]
                if row[4]==None:
                    zp = '--  '
                else:
                    zp = '%5.3f' % row[4]
	        print '%12s %16s' % (name[0], dt[1]),
                print ' %6s %7s %7s' % (see, zp, bck),
                print ' %5s %14s' % (row[6], row[2])
                nrow += 1
            if nrow==0:
                print '                    No data available'
            print "--------------------------------------------------------------------------------\n"
        
        if success>=2:
            cur.close
        if success>=1:
            con.close
        if success==3:
            success=1
        else:
            success=0
            

if __name__ == "__main__":
    main(sys.argv[0], sys.argv[1:])
