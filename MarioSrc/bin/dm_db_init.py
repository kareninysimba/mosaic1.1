#!/bin/env python

__version__ = "0.1"

import sys
import re
#import os
import sqlite

""" dm_db_init.py : a short script to initialize the 
    the data manager SQLite database.
	
    WARNING : all information in the database related to the SQL
    will be lost after running this program.

    usage : dm_db_init.py <sql_command_file> <catalog.db>

"""

# Ok, ok, this *is* overkill, but I got it from a python
# recipe book..and didnt know any better at the time..
class SQLChunkReader:

	def __init__ (self, fileobj, separator=';\n'):
		# Ensure that we get a line-rading sequence in the
		# best way possible
		try:
			#Check if the file-like object has an xreadlines
			# method available
			self.seq = fileobj.readlines()
			#self.seq = fileobj.xreadlines()
		except:
			# No? so fall back to the xreadlines module's 
			# implementation
			self.seq = xreadlines.xreadlines(fileobj)

		self.line_num = 0   # current index into self.seq (line number)
		self.chunk_num = 0  # current index into self (chunk number)

		self.separator = '.*'+separator+'$' 

	def __getitem__ (self, index):

		if (index != self.chunk_num):
			raise TypeError(), "Only sequential access supported"
		self.chunk_num += 1
		
		result = [""] 

		while 1:
			# Intercept Index Error, since we have one last chunk to restore
			try:
				#let's check if there's at least one more line in self.seq
				line = self.seq[self.line_num]
			except IndexError:
				# self.seq is finished, so we exit from the loop
				break
			# increment index into self.seq for next time
			self.line_num += 1
			result.append(line)
			if (re.match(self.separator, line)): break; # termination of SQL

		return ''.join(result)


def usage(progname): 
	print "Usage: %s <sql_command_file> <catalog.db>" % progname 
	sys.exit(-1)

def configureDB (database, sqlcommands, chatty=True):

	# open existing/create new SQLite database
	db = sqlite.connect(database)

	# obtain a cursor
	cursor = db.cursor()

	# open SQL parser on our SQL file
	sqlReader = SQLChunkReader(open(sqlcommands))

	# db command exec loop for each SQL command
	for chunk in sqlReader:
		if not chunk: break

		# pretty up output : dont print/chew on whitespace
		if (re.match("^\s*$",chunk)) : 
			continue

		if chatty: print chunk,

		try:
			cursor.execute(chunk)
			db.commit()
		except sqlite.DatabaseError:
			dberror = str(sys.exc_value)
			if ( re.match("^no such table:.*", dberror) or re.match("^no such index:.*", dberror)): 
				pass # no need to worry about dropping unknown table/index
			else: 
				print " DatabaseError:"
				print " type:(%s)" % sys.exc_type
				print " value:(%s)" % sys.exc_value
				print " traceback:(%s)" % sys.exc_traceback
				db.close();
				sys.exit(-1);
		except:
			print " Some strange error on SQL chunk: (%s)" % chunk
			db.close();
			sys.exit(-1);
	# <--- end command loop

	# clean up
	print "** Finished SQL commands"

if __name__ == '__main__':

	if len(sys.argv) < 3:
		usage(sys.argv[0])

	commands, database = sys.argv[1], sys.argv[2] 

	configureDB (database, commands)

	sys.exit(0)

