#!/bin/env pipecl
#
# CVSVER -- Determine version for working source.
# This routine extracts the current revision numbers for all source in the
# specified directory and below.  It associates logical version tags
# with the revision.  The final version for all the source is either the
# common version tag or "none" or "dev" if all the source do not have
# the a common version tag.

string	dir			# Starting directory.

string	fname, wrev, trev, tname, ver

task	$cvsstatus = "$!cvs -q status -v"

# Go to directory if specified.
if (fscan (args, dir) == 1)
    cd (dir)
;

# Generate version and revision information.
cvsstatus (>& "/tmp/cvsver")

# Associate logical tags with working revisions.
list = "/tmp/cvsver"; s1 = ""; s2 = ""; s3 = ""; fname = ""; ver = "none"
tname = ""
while (fscan (list, s1, s2, s3) != EOF) {
    if (s1 == "File:") {
        if (fname != "") {
	    printf ("%s %s\n", fname, tname)
	    if (ver == "none")
		ver = tname
	    else if (ver != tname)
		ver = "dev"
            ;
	}
	;
	fname = s2; tname = "none"
    } else if (s1 == "Working")
        wrev = s3
    else if (s2 == "(revision:") {
        if (tname == "none") {
	    trev = substr (s3, 1, stridx(")",s3)-1)
	    if (trev == wrev)
		tname = s1
	    ;
	}
	;
    }
    ;
    s1 = ""; s2 = ""; s3 = ""
}
list = ""; delete ("/tmp/cvsver")
printf ("%s %s\n", fname, tname)
printf ("Version %s\n", ver)

logout 0
