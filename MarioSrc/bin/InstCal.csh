#!/bin/csh -f
#

# Check for required files.
set dbdir = /shared2/data/pipeline/HdrDB
set db = $dbdir/hdr.db
set sedfile = $dbdir/InstCal.sed
if (! -e $db || ! -e $sedfile) then
printf "ERROR: Database files not found\n"
exit 1
endif

# Check if the directory has been processed.
echo Extracting directory $1
cd $1
if (-e InstCal.done) then
printf "WARNING: Directory has been processed (remove InstCal.done to proceed)\n"
exit 1
endif
rm InstCal* >& /dev/null

# Create sql file.
# Do this over all files in one transaction for efficiency.

foreach file (`grep -l InstCal *.hdr`)

# Give some processing info.
#echo Extracting from $file ...

# Run sed script to extract data.
sed -n -f $sedfile $file > InstCal.dat

# Format the sql.
printf "INSERT OR REPLACE INTO InstCal (" > InstCal.sql
foreach keyword (`cut -f 1 InstCal.dat`)
printf "\n%s," $keyword >> InstCal.sql
end
printf ")\nVALUES (" >> InstCal.sql
foreach value (`cut -f 2 InstCal.dat`)
set value = `echo "$value" | tr "#" " "`
printf "\n%s," "$value" >> InstCal.sql
end
printf ");\n" >> InstCal.sql

# Remove last comma of each clause.
sed -i 's/,)/)/' InstCal.sql

# Add to final sql file.
if (! -e InstCalAll.sql ) then
printf "BEGIN TRANSACTION;\n" > InstCalAll.sql
endif
cat InstCal.sql >> InstCalAll.sql

end

# Enter data into database if data was found.
if ( -e InstCalAll.sql) then

printf "COMMIT;\n" >> InstCalAll.sql
#echo Entering data into database $db
sqlite3 $db < InstCalAll.sql

# Clean up.
#rm InstCal*sql InstCal.dat

endif

# Leave record of processing.
touch InstCal.done
