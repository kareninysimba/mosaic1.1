#!/bin/env pipecl

int     imageid
real    mean, sigma
string  bpm,bpmfits,infile,ffile

images
imgeom
proto

task $findnoisychannel="$findnoisychannel $(1).fits $(2) -m $3 -s $4 -v"

if ( fscan(cl.args, infile ) < 1 ) {
    printf ("Usage: fixnoisychan.cl infile\n")
    logout 0
}
;

# Check whether the infile ends in .fits already, and if so, drop
# the extension
if (strlstr(".fits",infile)>0) {
    infile = substr (infile, 1, strlstr(".fits",infile)-1)
}
;
print( infile )

# Read the bad pixel mask from the header
hselect( infile, "BPM", yes ) | scan( bpm )
if (strlstr(".pl",bpm)>0) {
    bpm = substr( bpm, 1, strlstr(".pl",bpm)-1 )
}
;
s1 = osfn( bpm )
s2 = substr( s1, strldx("/",s1)+1,999)
if ( imaccess( s2 ) )
    imdel( s2 )
;
imcopy( bpm, s2 )
bpm = s2

# Read the imageid
hselect( infile, "imageid", yes ) | scan( imageid )

# For imageid 2 and 3, rotate the image and BPM
ffile = infile//"f"
bpmfits = infile // "bpm.fits"
if ( imaccess( ffile ) )
    imdel( ffile )
;
if ( imaccess( bpmfits ) ) 
    imdel( bpmfits )
;
if ( (imageid==2) || (imageid==3) ) {
    imtranspose( infile//"[*,-*]", ffile )
    imtranspose( bpm//"[*,-*]", bpmfits )
} else {
    imcopy( infile, ffile )
    imcopy( bpm, bpmfits )
}

# Determine the global statistics
mimstat( ffile, imasks=bpmfits, lsigma=3, usigma=3, nclip=3, format-,
    fields="mean,stddev" ) | scan( mean, sigma )

# Now run the noisy pixel / channel detection
printf("FF3 %s %s %f %f\n", ffile, bpmfits, mean, sigma )
findnoisychannel( ffile, bpmfits, mean, sigma )
if ( imaccess( ffile//"_out" ) )
    print "FF3 findnoisychannel succeeded"
else {
    print "FF3 findnoisychannel failed"
    logout 0
}

# The output of the code above is ffile_out and ffile_bpm. Replace
# the input fits file with the corrected output, and update the mask.
if ( imaccess( infile//"_fnp" ) )
    imdel( infile//"_fnp" )
;
if ( imaccess( bpm//"_fnp.pl" ) )
    imdel( bpm//"_fnp.pl" )
;
if ( (imageid==2) || (imageid==3) ) {
    imtranspose( ffile//"_out[-*,*]", infile//"_fnp" )
    imtranspose( ffile//"_bpm[-*,*]", bpm//"_fnp.pl" )
    imdel( ffile//"_out" )
} else {
    imrename( ffile//"_out", infile//"_fnp" )
    imcopy( ffile//"_bpm", bpm//"_fnp.pl" )
}
imdel( ffile )
imdel( ffile//"_bpm" )

if ( imaccess( "tempmask.pl" ) )
    imdel( "tempmask.pl" )
;
mskexpr( "(i<3)?i: 0", "tempmask.pl", bpm//"_fnp.pl" )
imdel( bpm//"_fnp.pl" )
imrename( "tempmask.pl", bpm//"_fnp.pl" )
hedit( infile//"_fnp", "BPM", bpm//"_fnp.pl", add+, update+, ver- )
imdel( bpmfits )
imdel( bpm )

logout 1
