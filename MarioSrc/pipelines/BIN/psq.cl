#!/bin/env pipecl

# PSQ -- Pipeline Scheduler Queue Module
#
# This is the first implementation based on SQLITE and DCI.
# This is called with submit undefined (default to zero) when the pipeline
# is first started to select as many datasets as there are processing nodes
# (obtained with the pipeselect call).  Subsequently, when a dataset is
# completed this module is called with submit=1 and with the EVENT_TYPE
# and  OSF_DATASET environment variables set.  This allows the completed
# dataset to be marked in the queue.  Then a new dataset is submitted.
# 
# The SQL calls are sent to the processing queue and to the DCI database
# through interface tasks to allow changing the implementation (such
# as converting from sqlite to mysql) without changing this module.
# The interface tasks are psql for querying the queue tables and psdata
# for querying the data service database.

# These are the arguments
string	queue = "enabled"		# Queue to be called
string	ds = "pending"			# Dataset to be selected
string	state = ""			# State
int     submit = 0			# Maximum submitted per queue

# These are editable parameters for testing and control.
bool	showpending = no		# Show pending entries?
bool	listdata = yes			# List data?
bool	execute = yes			# Execute?
int	status = 1

# These are local variables.
bool	pverbose
int     nsuball, maxsubmit, npending, nsubmit, nsubmitted, nfiles, npipes, idx
int     t, tmax
file    pstmp, queues, entries, pipes, files, iqsquery
file	fname, pathname, dirname, lastname
string  pipeline, psqname, data, pipe, propid, qname, node
string	 dataset, dataset1, datalist
string	gmd, gmdt, tstamp
struct  sql, cmd
struct  *qlist, *elist, *plist, *flist

# Define packages and tasks.
servers
proto
utilities
noao
astutil
task  $timestamp = "$!date +%s"
task  $ldate = "$!date +%Y%m%d"
#task $psql = "$!sqlite3 -separator "" "" -nullvalue 'NULL' $DMData/psq.db ""$1"""
#task psql = "NHPPS_PIPESRC$/MOSAIC/PSQ/psql.cl"
task $psql = "$!echo ""$1"" | mysql -N -pMosaic_DHS $USER"
#task $psdata = "$!sqlite $DMData/dci.db ""select pathname from dtkpct where $1 order by substr(pathname,1,8);"""
#task $psmaxdate = "$!sqlite $DMData/dci.db ""select max(date) from dtkpct where $1;"""
task $psmaxdate = "$!echo ""select max(date) from dtkpct where $1;"" | mysql -N -h chive -pMosaic_DHS dci"
#task $psdata = "$!psdata ""select pathname from dci.dtkpct where $1 order by substring(pathname,1,8);"""
#task $psdata = "$!psdata ""select pathname from dtkpct where $1 order by substr(pathname,1,8);"""
#task $psmaxdate = "$!psdata ""select max(date) from dci.dtkpct where $1;"""
task $psdata = "$!psdata $1"
task $pipeselect = "$!pipeselect"
task psqredo = "NHPPS_PIPESRC$/BIN/psqredo.cl"

# Get arguments.
if (fscan (args, queue, ds, submit, psq_limit, psq_order) < 0) {
    printf ("Usage: psq [queue dataset maxsubmit limit order]\n")
    logout (0)
}
;

# Define files.
pstmp = mktemp ("pstmp")
queues = pstmp // ".queues"
entries = pstmp // ".entries"
pipes = pstmp // ".pipes"
files = pstmp // ".files"
iqsquery = pstmp // ".iqs"

# Set the PSQ database server.
#psql.server = dm

# Set current date and time.
print ("print($GMD, $GMDT)") | astcalc (prompt="") | scan (gmd, gmdt)
print (gmd) | translit ("STDIN","-", del+) | scan (gmd)

# Set verbose.
pverbose = (verbose > 0 || execute == NO)
pverbose = yes

# Log task.
printf ("\nPSQ: "); time

# Work in input directory.
cd ("MOSAIC_PSQ$/input/")

# Handle returned data.
node = ""
if (submit != 0) {
    if (defvar ("EVENT_TYPE")) {
	state = "enabled"
	if (envget ("EVENT_TYPE") == "EVENT_FILE_EVENT") {
	    dataset = envget ("OSF_DATASET")
	    if (dataset != "start") {
		idx = stridx ("_", dataset)
		node = substr (dataset, 1, idx-1)
		dataset1 = substr (dataset, idx+1, 1000)
		idx = stridx ("_", dataset1)
		queue = substr (dataset1, 1, idx-1)
		data = substr (dataset1, idx+1, 1000)
		idx = stridx ("_", data)
		data = substr (data, 1, idx-1)

		if (access (dataset // ".dir") && queue != "") {
		    count (dataset//".dir") | scan (i)
		    if (pverbose) {
			if (i > 0)
			    printf ("\nUpdate dataset %s as completed\n",
			        dataset1)
			else
			    printf ("\nUpdate dataset %s as nodata\n",
			        dataset1)
		    }
		    ;
		    if (execute) {
			if (i > 0) {
			    rename (dataset//".dir",
				"MOSAIC_PSQ$/"//"output/"//dataset1//".list")
			    printf ("update %s set status='completed', completed='%s' \
				where dataset='%s';\n", queue, gmdt, data) |
				scan (sql)
			    psql (sql)
			} else {
			    delete (dataset//".dir")
			    printf ("update %s set status='nodata', completed='%s' \
				where dataset='%s';\n", queue, gmdt, data) |
				scan (sql)
			    psql (sql)
			}
		    }
		    ;
		} else if (queue != "") {
		    if (pverbose)
			printf ("\nUpdate dataset %s as error\n", dataset1)
		    ;
		    if (execute) {
			printf ("update %s set status='error', completed='%s' \
			    where dataset='%s';\n", queue, gmdt, data) |
			    scan (sql)
			    psql (sql)
		    }
		    ;
		}
		;
		queue = "enabled"
	    }
	    ;
	}
	;
    }
    ;
}
;

# Find queues and pipelines.
printf ("select psqname, queue, data, pipeline from PSQ\n") | scan (sql)
if (queue == "enabled" || queue == "disabled")
    printf ("%s where state='%s'\n", sql, queue) | scan (sql)
else if (state != "")
    printf ("%s where state='%s' and queue='%s'\n", sql, state, queue) |
        scan (sql)
else
    printf ("%s where queue='%s'\n", sql, queue) | scan (sql)
sql += ";"
if (pverbose)
    print (sql)
;
psql (sql, > queues)
if (pverbose) {
    print ("Queues:")
    type (queues)
}
;

# For each queue get entries.
nsuball = 0
qlist = queues
while (fscan (qlist, psqname, queue, data, pipeline) != EOF) {

    # Determine number of distinct pipelines.
    if (execute) {
	pipeselect (envget ("NHPPS_SYS_NAME"), pipeline, 0, 0, 10000000, > pipes)
	if (pverbose)
	    type (pipes)
	;
	count (pipes) | scan (npipes)
	delete (pipes)
    } else
	npipes = 1

    if (npipes < 1) {
	printf ("WARNING: pipeline %s not available\n", pipeline)
	status = 2
	break
    }
    ;

    # Determine number of pending entries.
    if (ds == "pending" || ds == "error" || ds == "submitted")
	printf ("select count(*) from %s where status='%s';\n", queue, ds) |
	    scan (sql)
    else
	printf ("select count(*) from %s where dataset='%s';\n", queue, ds) |
	    scan (sql)
    psql (sql) | scan (npending)
    if (npending == 0) {
        if (pverbose)
	    printf ("\nQueue %s: no pending entries\n", queue)
	;
        next
    }
    ;

    # Determine number of submitted entries.
    if (ds == "pending") {
	printf ("select count(*) from %s where status='submitted';\n",
	    queue) | scan (sql)
	psql (sql) | scan (nsubmitted)
    } else
        nsubmitted = 0

    # Set maximum to submit.
    if (execute == NO)
        maxsubmit = nsubmitted + npending
    else if (submit == 0)
        maxsubmit = nsubmitted + max (0, npipes - nsuball)
    else
	maxsubmit = nsubmitted + max (0, min (npipes, submit) - nsuball)
    if (pverbose)
       printf ("\nQueue %s: currently submitted = %d/%d\n",
           queue, nsubmitted, maxsubmit)
    ;
    nsubmit = maxsubmit - nsubmitted
    if (nsubmit <= 0)
        next
    ;

    # Set pipeline directories.
    if (execute) {
	pipeselect (envget ("NHPPS_SYS_NAME"), pipeline, nsubmit, 0, 10000000, > pipes)
	if (node != "" && nsubmit == 1) {
	    sql = ""
	    pipeselect (envget ("NHPPS_SYS_NAME"), pipeline, 0, 0, 10000000) |
	        match (node//"!") | scan (pipe)
	    if (pipe != "")
	        print (pipe, > pipes)
	    ;
	}
	;
    } else {
	for (i=1; i<=nsubmit; i+=1)
	    pathname (".", >> pipes)
    }
    plist = pipes

    # Select entries to submit.
    if (ds == "pending" || ds == "error" || ds == "submitted")
	printf ("select dataset, end, query, 'and', subquery from PSQ, %s, %s \
	    where status='%s' and queue='%s' and dataset=name \
	    order by priority, end %s;\n", queue, data, ds, queue, psq_order) |
	    scan (sql)
    else
	printf ("select dataset, end, query, 'and', subquery from PSQ, %s, %s \
	    where dataset='%s' and queue='%s' and dataset=name \
	    order by priority, end %s;\n", queue, data, ds, queue, psq_order) |
	    scan (sql)
    psql (sql, > entries)
    if (pverbose) {
        printf ("Queue %s: %d pending entries\n", queue, npending)
	if (showpending)
	    type (entries)
	;
    }
    ;

    # Read queue and qname configuration file for psq_redo and psq_limit.
    # This was not read when the CL started up because this pipeline may
    # not have a dataset associated with it.

    s1 = "PipeConfig$/" // queue // ".cl"
    if (access (s1))
        cl (< s1)
    ;
    if (ds != "pending" && ds != "error" && ds != "submitted") {
	s1 = "PipeConfig$/" // ds // ".cl"
	if (access (s1))
	    cl (< s1)
	;
    }
    ;

    # Get dataset and submit to pipeline.
    #tmax = INDEF
    ldate | scan (tmax)
    elist = entries
    while (fscan (elist, qname, t, sql) != EOF) {
        if (isindef(tmax) == NO && t >= tmax)
	    next
	;

        timestamp | scan (tstamp)
        #dataset = pipeline // tstamp // "_" // queue // "_" // qname
        dataset = queue // "_" // qname // "_" // tstamp
	if (pverbose)
	    printf ("Queue %s, dataset %s:\n%s\n", queue, dataset, sql)
	;

	# Query the IQS for ENIDs.
	s1 = "PipeConfig$/" // qname // ".enids"
	if (access (s1))
	    copy (s1, files)
	else {
	    printf ("select pathname from dtkpct\n", > iqsquery)
	    printf ("where %s\n", sql, >> iqsquery)
	    printf ("and pathname like '%%gz'\n", >> iqsquery)
	    printf ("order by substr(pathname,1,8)\n", >> iqsquery)
	    printf ("limit %d;\n", psq_limit, >> iqsquery)
	    psdata (iqsquery, > files)
	    delete (iqsquery, verify-)
	}

        # Check for data.
	nfiles = 0; count (files) | scan (nfiles)
        if (nfiles == 0) {
	    # Get date of dataset.
	    printf ("select end from %s where name='%s';\n",
	        data, qname) | scan (sql)
	    t = 0; psql (sql) | scan (t)

	    # Check for lag in database.
	    if (isindef (tmax)) {
		printf ("select query from PSQ where queue='%s';\n",
		    queue) | scan (sql)
		psql (sql) | scan (sql)
	        psmaxdate (sql) | scan (tmax)
		if (pverbose)
		    printf ("Queue %s: last DCI entry = %d\n", queue, tmax)
		;
	    }
	    ;
	    if (t <= tmax) {
		printf ("WARNING: dataset %s has no files\n", qname)
		delete (files)
		if (execute) {
		    printf ("update %s set status='nodata' \
			where dataset='%s';\n", queue, qname) | scan (sql)
		    psql (sql)
		}
	    } else if (pverbose)
	        printf ("Queue %s: waiting for %s\n", queue, qname)
	    ;
            next
        }
        ;

	# Set data list.
        datalist = dataset // "." // pipeline
	rename (files, datalist)

	# Eliminate processed enids if desired.
	if (psq_redo != "yes") {
	    if (pverbose)
	        printf ("Queue %s: Checking previous processing for %s\n",
		    queue, qname)
	    ;
	    psqredo (queue, qname, datalist)
	    idx = nfiles; count (datalist) | scan (nfiles); idx -= nfiles
	    if (pverbose && idx > 0) {
	        if (nfiles == 0)
		    printf ("Queue %s: All files previously processed for %s\n",
			queue, qname)
		else
		    printf ("Queue %s: Excluded %d previously processed files for %s\n", queue, idx, qname)
	    }
	    if (nfiles == 0)
	        next
	    ;
	}
	;

        # Submit dataset to pipeline.
	if (execute) {
	    if (fscan (plist, pipe) == EOF)
		break
	    ;
	    if (pverbose) {
		printf ("Queue %s: submitting dataset %s (%d files) at %s\n",
		    queue, dataset, nfiles, pipe)
		if (listdata)
		    type (datalist)
		;
	    }
	    node = substr (pipe, 1, stridx ("!",pipe)-1)
	    pathname ("MOSAIC_PSQ$/input/" // node // "_" // datalist,
	        > pipe//"return/"//datalist)
	    copy (datalist, pipe); delete (datalist)
	    touch (pipe // datalist // "trig")
	} else {
	    if (pverbose) {
		printf ("Queue %s: submitting dataset %s (%d files)\n",
		    queue, dataset, nfiles)
		if (listdata)
		    type (datalist)
		;
	    }
	    delete (datalist)
	}

        # Update queue.
	if (execute) {
	    printf ("update %s set status='submitted', submitted='%s' \
		where dataset='%s';\n", queue, gmdt, qname) | scan (sql)
	    psql (sql)
	}
	;

        # Increment submitted datasets and quit when maxsubmit is reached.
	nsuball += 1
        nsubmitted += 1
	if (pverbose)
	    printf ("Queue %s: submitted = %d/%d\n",
	        queue, nsubmitted, min(npending,maxsubmit)
	;
        nsubmit = maxsubmit - nsubmitted
        if (nsubmit <= 0)
            break
	;
    }
    elist = ""; delete (entries)
    plist = ""; delete (pipes)
    status = 1
    if (submit > 0 && nsuball >= submit)
        break
    ;
}
qlist = ""; delete (queues)

# Delete any temporary files not already deleted.
delete (pstmp//".*")

logout (status)
