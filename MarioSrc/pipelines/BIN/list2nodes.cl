#!/bin/env pipecl2
#
# LIST2NODES -- Break up an input list into node lists.
## 
## =============
## list2nodes.cl
## =============
## -----------------------------------
## break up an input file list by node
## -----------------------------------
## 
## :Manual Group:   BIN
## :Manual Section: 1
## 
## Usage
## =====
## 
## ``list2nodes.cl tpipe [listloc]``
## 
## | ..
## |   tpipe   - target pipeline
## |   listloc - list location (``input``|``data``)
## 
## Input
## =====
## 
## - list with files having full IRAF paths.  This may be in the standard
##   input directory with the name ``[dataset].[pipe]`` or in the standard
##   data directory with the name ``[dataset].list`` if the list location
##   parameter is ``input`` or ``data`` respectively.
## 
## Output
## ======
## 
## - temporary lists with names ``[tpipe]_[node].tmp`` and a list of these
##   lists called ``[tpipe].tmp``
## 
## Exit Status
## ===========
## 
## | 0 - unspecified error
## | 1 - successful creation of the output
## | 2 - no data found in the input list
## 
## Description
## ===========
## 
## The input list is specified as either ``input`` for
## ``[indir]/[dataset].[pipe]`` or ``data`` for ``[datadir]/[dataset].list``.
## The output lists are:
## 
##   | ``[datadir]/[tpipe]_[node].tmp``
## 
## and the list of lists is:
## 
##   | ``[datadir]/[tpipe].tmp``
## 
## Note that if a line in the input list does not have an IRAF node then
## it is ignored.

string	tpipe			# Target pipeline
string	ilist = "data"		# Input list

# Get input arguments.
if (nhpps.dataset == "None")
    plexit (code=0, name="MISSING", class="WARNING",
        description="Missing dataset")
;
if (fscan (args, tpipe, ilist) < 1)
    plexit (code=0, name="MISSING", class="WARNING",
        description="Missing arguments")
;

# Find the input list.
if (ilist == "input")
    ilist = names.indir // nhpps.dataset // "." // nhpps.pipe
else if (ilist == "data")
    ilist = nhpps.dataset // ".list"
;

if (access(ilist) == NO) {
    sendmsg ("ERROR", "No input list", ilist, "VRFY")
    plexit (code=0, name="MISSING", class="ERROR",
	description="Missing input list", arg=ilist)
}
;

# Make node lists.
delete (tpipe//"*.tmp")
list = ilist
while (fscan (list, s1) != EOF) {
    i = stridx ("!", s1)
    if (i == 0)
        next
    ;

    s2 = substr (s1, 1, i-1)
    print (s1, >> tpipe//"_"//s2//".tmp")
}
list = ""

pathnames (tpipe//"_*.tmp", > tpipe//".tmp")
count (tpipe//".tmp") | scan (i)
if (i == 0) {
    delete (tpipe//".tmp")
    sendmsg ("WARNING", "No data", ilist, "VRFY")
    plexit (code=2, name="NODATA", class="WARNING",
        description="No data", arg=ilist)
}
;

plexit
