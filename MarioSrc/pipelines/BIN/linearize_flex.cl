#!/bin/env pipecl

# Linearize an input array using constant or 2D linearity coefficients

int status=1
string infile, detector, lineq, lincoeff, observat
string a, b

file    caldir = "MC$"

images
servers

# Check that there is at least one argument in the call to linearize_flex.
if (fscan(cl.args,infile) < 1) {
    printf("Usage: linearize_flex.cl infile\n")
    logout 0
}
;

# Create short version of the filename
s1 = substr (infile, strldx("/",infile)+1, strlstr(".fits",infile)-1)

# Get the detector and observatory name
detector = ""
print( infile )
hselect( infile, "detector", yes ) | scan( detector )
observat = ""
hselect( infile, "OBSERVAT", yes ) | scan( observat )

# Get the linearity coefficient and equation from the calibration database.
# It is not strictly necessary to get the linearity coefficient, because
# that was already obtained in the setup stage of the calling module, 
# but the getcal below is maintained for maximum flexibility.
getcal( infile, lincoeftype, cm, caldir, 
    obstype="", detector="!instrume", imageid="!detector",
    filter="", exptime="", mjd="!mjd-obs")
if ( getcal.statcode == 0 ) {
    getcal( infile, "lineq", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs")
}
;
if (getcal.statcode>0) { # Getcal was not successful
    sendmsg( "ERROR",
        "Getcal failed - no nonlinearity correction performed",
        s1, "CAL" )
    status = 0
} else { # Getcal was successful
    # Get the linearity coefficient from the header
    lincoeff = ""
    lineq = ""
    hsel(infile,lincoeftype,yes) | scan(lincoeff)
    hsel(infile,"lineq",yes) | scan(lineq)
#    hsel( infile, "lincoeff,lineq", yes ) | scan( lincoeff, lineq )
    if ( lincoeff == "" ) { # No coefficient in the header
        sendmsg( "ERROR",
            "No lincoeff in header - no nonlinearity correction performed",
            s1, "CAL" )
        status = 0
    } else {
	if ( lineq == "" ) {
            sendmsg( "ERROR",
                "No lineq in header - no nonlinearity correction performed",
                s1, "CAL" )
            status = 0
        } else {
            # Apply the nonlinearity correction using the 
            # linearity imexpression database
	    if ( observat == 'CTIO' ) {
	        print "Using CTIO lineariziation"
                imexpr( lineq, "output.fits", a=infile, b=lincoeff,
                    exprdb="pipedata/linearity_ctio.db", outtype="real" )
	    } else {
	        print "Using KPNO lineariziation"
                imexpr( lineq, "output.fits", a=infile, b=lincoeff,
                    exprdb="pipedata/linearity.db", outtype="real" )
            }
imstat (infile//",output.fits")
            imdel( infile )
            imrename( "output.fits", infile )
            printf( "Linearized: coefficient image: %s\n", lincoeff ) |
                scan( line )
            hedit( infile, "LINEAR", line, add+, ver- )
        }
    }
    ;
}

logout( status )
