#!/bin/env pipecl
#
# DELDEL -- Delete files.

file	ilist

names ("del", envget("OSF_DATASET"))
ilist = names.indir // names.dataset // ".del"

# Log start of processing.
printf ("\nDELDEL (%s): ", names.dataset)
time
concat (ilist)

# Delete files.
if (access (ilist))
    delete ("@"//ilist)
;
logout 1
