#!/bin/env pipecl
#
# RETURNDATA -- Move return data to the local host.
#
# When a called pipeline on a different node returns its list of results
# this routine moves the files to the local node and modifies the list.
# The end result will be as if the called pipeline ran on the local node.
# This is used to reduce the disk requirements of called nodes with the
# requirement that the local node has sufficient disk space to keep the
# results.
#
# Note that this is different than calling the DTS pipeline which moves
# data to a node running the DTS pipeline and puts the data into a
# directory controlled by the DTS pipeline.  
#
# This routine is normally called by return.cl.
# This requires the local file structure is the same as the remote one.


string  input			# Input list of files

file	tmp

# Get arguments.  If no input list return.
if (fscan (cl.args, input) < 1)
    logout 0
;

# Get local host name.
pathname "/" | scan (s1)
i = strlen (s1) - 1
s1 = substr (s1, 1, i)

# Check if there are any remote files in the list and return if not.
b1 = NO
list = input
while (fscan (list, s2) != EOF) {
    if (substr (s2, 1, i) == s1)
        next
    ;
    b1 = YES
    break
}
list = ""

if (b1 == NO)
    logout 0
;

# Move files.
tmp = mktemp ("tmp")
list = input
while (fscan (list, s2) != EOF) {
    s3 = s1 // substr (s2, stridx("/",s2), 1000)

    if (substr (s2, 1, i) != s1) {
	print (substr(s3,stridx("/",s3),strldx("/",s3)))
	# Check if directory exists.  We only check for the final directory.
	if (access (substr(s3,stridx("/",s3),strldx("/",s3))) == NO)
	    mkdir (substr(s3,stridx("/",s3),strldx("/",s3)))
	;

	# Rename the file.
	printf ("%s -> %s\n", s2, s3)
	rename (s2, s3)
    }
    ;

    # Add to list.
    print (s3, >> tmp)
}
list = ""

# Reset list.
rename (tmp, input)

logout 1
