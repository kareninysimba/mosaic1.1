#!/bin/env pipecl
#
# TRIGGER -- Trigger a node with a file in the spool directory.
#
# This routine must be run from the spool directory.  The file must not
# already be assigned to a node or be locked.  This routine will
# check if the requested node is running by updating the list of pipelines.

string	new				# Spool file name
string	node				# Target node
string  tpipe                           # Target pipeline
string  textn = ""                      # Target pipeline trigger extension
string  rextn = ""                      # Return trigger file extension
string  aextn = ""                      # Alternate trigger file extensions

string	pipes, cname, tname, odir

# Get arguments.
if (fscan (cl.args, new, node, tpipe) != 3) {
    printf ("Usage: trigger.cl fname node tpipe\n")
    logout 0
}
;

textn = "." // tpipe
rextn = substr (new, strldx(".",new), 1000)
aextn = rextn

# Tasks and packages.
task $pipeselect = "$!pipeselect"

# Check file status.
if (access(new) ==NO) {
    printf ("ERROR: File not found (%s)\n", new)
    logout 1
} else if (access (new // ".node")) {
    head (new//".node") | scan (s1)
    printf ("ERROR: File already assigned to %s (%s)\n", s1, new)
    logout 1
} else if (access (new // ".lock")) {
    printf ("ERROR: File in use (%s)\n", new)
    logout 1
} else
    touch (new//".lock")

# Update pipelines.
i = strldx (".", new)
j = strldx ("-", new)
if (i == 0 || j == 0) {
    printf ("ERROR: Input filename format wrong (%s)\n", new)
    delete (new//".lock")
    logout 0
}
;
pipes = substr (new,1,j-1) // rextn
while (YES) {
    if (access (pipes//".lock") == NO) {
	touch (pipes//".lock")
	delete (pipes)
	if (defvar ("NHPPS_MIN_DISK"))
	    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, envget("NHPPS_MIN_DISK"), > pipes)
	else
	    pipeselect (envget ("NHPPS_SYS_NAME"), tpipe, 0, 0, > pipes)
	delete (pipes//".lock")
	break
    }
    ;
    sleep 1
}
;

# Trigger the file.
s1 = ""; odir = ""
match (node, pipes) | scan (odir)
if (access (pipes//".exclude"))
    match (node, pipes//".exclude") | scan (s1)
;
if (odir == s1)
    printf ("ERROR: Pipeline %s not running or excluded on node %s\n",
        tpipe, node)
else {
    cname = substr (new, 1, strlstr(rextn,new)-1)
    head (new) | scan (tname)
    copy ("../data/"//tname//textn, odir//tname//textn)
    if (aextn != textn) {
	pathname ("../"//cname//rextn, > odir//"return/"//tname//aextn)
	pathname ("../"//cname//textn, > odir//"return/"//tname//textn)
    } else
	pathname ("../"//cname//rextn, > odir//"return/"//tname//textn)
    print (odir, > cname//rextn//".node")
    touch (odir//tname//textn//"trig")
}
delete (new//".lock")

logout 1
