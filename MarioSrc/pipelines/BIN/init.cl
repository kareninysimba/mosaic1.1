#!/bin/env pipecl
#
# INIT -- Initialize the pipeline directories
#
# This is parameterized for use by different pipelines which follow the
# same basic directory substructure.  This is like start.cl but does
# nothing to the OSF, and nothing is logged

string	pipe = ""		# Pipeline ID

string	datadir

# Tasks and packages.
task $ln = "$!ln -s $1 $2"
utilities

# Get arguments.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
pipe = names.pipe
i = fscan (cl.args, pipe)

# Set filenames.
datadir = names.datadir

# Create dataset directory if needed.
if (access (datadir)) {
    cd (datadir)
} else {
    mkdir (datadir)
    cd (datadir)
    ln (osfn("uparm$"), "uparm")
}

logout 1
