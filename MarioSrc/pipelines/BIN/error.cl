#!/bin/env pipecl
#
# ERROR -- Error procedure.
#
# This is primarily intended to send an error to a calling program
# through a return trigger without a return list.  An argument can
# be given to return the dataset list or an empty list.

string	extns	= ""		# List of return file extensions
string	option	= ""		# Option
string	stage	= ""		# For backwards compatibility

string	dataset, indir, datadir, rtnfile, lfile, rfile, extn
struct	extnlist

# Packages and tasks.
utilities

# Get arguments.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
extns = "." // names.pipe
stage = envget ("NHPPS_MODULE_NAME")

i = fscan (cl.args, extns, option)
if (option != "empty" && option != "list")
    stage = option
;

dataset = names.dataset
indir   = names.indir
datadir = names.datadir
lfile   = names.lfile
set (uparm = names.uparm)

# Log start of processing.
printf ("\nERROR: %s (%s): ", stage, dataset)
time
if (access (lfile)) {
    printf ("\nERROR: %s (%s): ", stage, dataset, >> lfile)
    time (>> lfile)
}
;

# Return with or without a datafile.

if (all_erract == "return") { 
    print (extns) | translit ("STDIN", ",", " ", delete-, collapse+) |
        scan (extnlist)
    while (fscan (extnlist, extn, extnlist) != 0) {
	rtnfile = indir // "return/" // dataset // extn
	printf( "Triggering: Looking for file %s\n", rtnfile )
	if (access (rtnfile)) {
	    head (rtnfile, nlines=1) | scan (rfile)
	    delete (rtnfile)
	    if (option == "empty") {
		printf ("Triggering empty data file in %s\n", rfile)
	        touch (rfile)
	    } else if (option == "list") {
		if (access(datadir//dataset//".list")) {
		    printf ("Triggering dataset list file in %s\n", rfile)
		    copy (datadir//dataset//".list", rfile)
		} else {
		    printf ("Triggering empty data file in %s\n", rfile)
		    touch (rfile)
		}
	    } else
		printf ("Triggering error in %s\n", rfile)
	    touch (rfile//"trig")
	}
	;
    }
    ;
}
;

logout 1
