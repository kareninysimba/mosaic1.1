#
# REJECTSIF1
#
# Description:
#
#   This is a relatively general procedure for rejecting images based on
#   quality.  Here, quality is assessed from the seeing, transparency, and
#   skynoise.
#
#   Given a list of images (infile), this module uses keyword values of
#   SEEING1, MAGZERO, SKYNOIS1 for seeing, magzero calibration, and
#   skynoise, respectively, to determine which images to accept and which
#   to reject.  The magzero calibration is converted to a transparency,
#   relative to that for magzero = 22.  The distributions for seeing,
#   transparency, and skynoise are initially characterized (midpt, stddev
#   are computed) by making use of the following input parameters (*0SEE,
#   *0TR, *0SN, *0RAT, *0):
#
#       usig0SEE:   upper sigma threshold for seeing
#       up0SEE:     upper threshold value for seeing
#       lsig0TR:    lower sigma threshold for transparency
#       low0TR:     lower threshold value for transparency (relative to
#                   transparency for MAGZERO=22)
#       usig0SN:    upper sigma threshold for skynoise
#       lsig0RAT:   lower sigma threshold for ratio of transparency/skynoise
#       nclp0:      number of iterations performed for each distribution
#                   determination
#
#   After these initial characterizations, the images may be filtered.
#   Rejection/acceptance of images is controlled by the following input
#   parameters (*1TR, *1SN, *1RAT):
#
#       usig1SEE:   upper sigma threshold for seeing
#       up1SEE:     upper threshold for seeing
#       const1SEE:  a constant above the midpt seeing
#       lsig1TR:    lower sigma threshold for transparency
#       frac1TR:    lower threshold value for transparency, expressed
#                   RELATIVE to midptTR (~mode of transparency distribution)                   
#       low1TR:     lower threshold value for transparency
#       usig1SN:    upper sigma threshold for skynoise
#       lsig1RAT:   lower sigma threshold for ratio (transparency/skynoise)
#
#   The rejection status is written to the header keyword REJECT, and 
#   may have the following values:
#
#       "Y!"//reason    : reject image, based on user request.  If the module
#                       : has also found reason to reject the image, then that
#                       : reason is appended.
#       "N!"//reason    : accept image, based on user request.  If the module
#                       : has found reason to reject the image, then that
#                       : reason is appended.  Nevertheless, the image is
#                       : accepted.
#       "Y"//reason     : reject image, based on the reason found by the module.
#       "N"             : accept image.  No reason for rejection was found by
#                       : the module.
#
#   The module may include the following reasons, which may be strung together
#   and separated by commas.
#   
#       "tr"    : transparency
#       "sn"    : skynoise
#       "rat"   : transparency/skynoise ratio
#       "see"   : seeing
#
#   For example, if REJECT = "N!tr,see" indicates that the image will not be
#   rejected, per the user's request, but the module suggests that it should
#   be rejected based on the transparency and seeing criteria.
#
#   An output table (outfile) is created with the following columns: filename,
#   REJECT.
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful (not currently set in module)
# 
# Future Work:
#
#   An improvement could be to add input parameters to specify the names of
#   the seeing, magzero, and skynoise keywords.
#
# History
#
#   T. Huard    200904--    Created
#   T. Huard    200905--    Now writing REJECT keyword and creating outfile
#                           table.
#   T. Huard    20090507    Added {prompt=""} to global parameter declarations.
#                           I don't think these are strictly necessary.  Also,
#                           infile was made a visible, required parameter
#                           instead of a hidden parameter.
#   T. Huard    20090617    Made "const1TR" obsolete.  It was not a practically
#                           useful parameter.  In its place, I have implemented
#                           the "frac1TR" parameter, which thresholds by a 
#                           fraction of the midpoint (or ~mode) of the
#                           transparency.  For example, if we want to reject
#                           images with a MAGZERO value that is 1.0 below (i.e.,
#                           less sensitive) than the mode MAGZERO value, then
#                           frac1TR=10**(-1.0/2.5)=0.40.  In this example, all
#                           images with transparency less than 0.40 of the mode
#                           transparency will be rejected. 
#   T. Huard    20090813    Printing many lines to a log file (rejectsif1-debug.log)
#                           for debugging purposes.
#   Last Revised:   T. Huard    20090904   11:30am 

procedure rejectsif1 (infile)

# Declare global parameters.
string  infile                      {prompt="Input file"}
string  outfile = "rejectsif1.out"  {prompt="Output file"}
real    up1SEE = 5.                 {prompt="Upper seeing threshold"}
real    const1SEE = INDEF           {prompt="Constant above seeing midpt"}
real    usig1SEE = 10.              {prompt="Upper sigma seeing threshold"}
real    lsig1TR = INDEF             {prompt="Lower sigma transp threshold"}
real    const1TR = INDEF            {prompt="Constant below best transp"}
real    frac1TR = INDEF             {prompt="Lower RELATIVE transp threshold"}
real    low1TR = INDEF              {prompt="Lower transp threshold"}
real    usig1SN = INDEF             {prompt="Upper sigma skynoise threshold"}
real    lsig1RAT = 10.              {prompt="Lower sigma ratio threshold"}
real    usig0SEE = 10.              {prompt="Upper sigma seeing threshold0"}
real    up0SEE = INDEF              {prompt="Upper seeing threshold0"}
real    lsig0TR = 10.               {prompt="Lower sigma transp threshold0"}
real    low0TR = INDEF              {prompt="Lower transp threshold0"}
real    usig0SN = 10.               {prompt="Upper sigma skynoise threshold0"}
real    lsig0RAT = 10.              {prompt="Lower sigma ratio threshold0"}
real    nclp0 = 3.                  {prompt="Number of iterations"}
int     status = 1                  {prompt="Exit status"}

begin

    # Declare local parameters.
    int     nrows,rejectTR,rejectSN,rejectRAT,rejectSEE
    real    npixSEE,meanSEE,midptSEE,stddevSEE,minSEE,maxSEE
    real    npixTR,meanTR,midptTR,stddevTR,minTR,maxTR
    real    npixSN,meanSN,midptSN,stddevSN,minSN,maxSN
    real    npixRAT,meanRAT,midptRAT,stddevRAT,minRAT,maxRAT
    real    seeing,magzero,skynoise,trans,ratio
    string  msg,dim,reject,rejTMP

    # Print to a log file for debug purposes
    if (access("rejectsif1-debug.log")) delete("rejectsif1-debug.log",verify-)
    ;
    printf("%s\n","***BEGINNING rejectsif1:***",>>"rejectsif1-debug.log")
    printf("%s\n","infile: "//infile,>>"rejectsif1-debug.log")
    printf("%s\n","outfile: "//outfile,>>"rejectsif1-debug.log")
    printf("%s\n","up1SEE: "//str(up1SEE),>>"rejectsif1-debug.log")
    printf("%s\n","const1SEE: "//str(const1SEE),>>"rejectsif1-debug.log")
    printf("%s\n","usig1SEE: "//str(usig1SEE),>>"rejectsif1-debug.log")
    printf("%s\n","lsig1TR: "//str(lsig1TR),>>"rejectsif1-debug.log")
    printf("%s\n","const1TR: "//str(const1TR),>>"rejectsif1-debug.log")
    printf("%s\n","frac1TR: "//str(frac1TR),>>"rejectsif1-debug.log")
    printf("%s\n","low1TR: "//str(low1TR),>>"rejectsif1-debug.log")
    printf("%s\n","usig1SN: "//str(usig1SN),>>"rejectsif1-debug.log")
    printf("%s\n","lsig1RAT: "//str(lsig1RAT),>>"rejectsif1-debug.log")
    printf("%s\n","usig0SEE: "//str(usig0SEE),>>"rejectsif1-debug.log")
    printf("%s\n","up0SEE: "//str(up0SEE),>>"rejectsif1-debug.log")
    printf("%s\n","lsig0TR: "//str(lsig0TR),>>"rejectsif1-debug.log")
    printf("%s\n","low0TR: "//str(low0TR),>>"rejectsif1-debug.log")
    printf("%s\n","usig0SN: "//str(usig0SN),>>"rejectsif1-debug.log")
    printf("%s\n","lsig0RAT: "//str(lsig0RAT),>>"rejectsif1-debug.log")
    printf("%s\n","nclp0: "//str(nclp0),>>"rejectsif1-debug.log")
    printf("%s\n","status: "//str(status),>>"rejectsif1-debug.log")

    # Create table (rejectsif1-props2.tmp) containing information that will be
    # used for determining whether image is accepted or rejected.
    if (access("rejectsif1-props.tmp")) delete("rejectsif1-props.tmp",verify-)
    ;
    if (access("rejectsif1-props2.tmp")) delete("rejectsif1-props2.tmp",verify-)
    ;
    hselect("@"//infile,"$I,SEEING1,MAGZERO,SKYNOIS1",yes,
        >"rejectsif1-props.tmp")

    # Print to a log file for debug purposes
    printf("\n%s\n","***FILE rejectsif1-props.tmp:***",>>"rejectsif1-debug.log")
    concat("rejectsif1-props.tmp",>>"rejectsif1-debug.log")

    list="rejectsif1-props.tmp"
    while(fscan(list,s1,seeing,magzero,skynoise) != EOF) {
        trans=10**((magzero-22.)/2.5)
        ratio=trans/skynoise
        printf("%s %s %s %s %s\n",s1,seeing,trans,skynoise,ratio,
            >>"rejectsif1-props2.tmp")
    }
    list=""

    # Print to a log file for debug purposes
    printf("\n%s\n","***FILE rejectsif1-props2.tmp:***",>>"rejectsif1-debug.log")
    concat("rejectsif1-props2.tmp",>>"rejectsif1-debug.log")

    # Create FITS images (seeing, transparency, skynoise, and
    # transparency/skynoise ratio) from the table so that the initial
    # distributions may be characterized.
    if (imaccess("rejectsif1-see")) imdelete("rejectsif1-see",verify-)
    ;
    if (imaccess("rejectsif1-trans")) imdelete("rejectsif1-trans",verify-)
    ;
    if (imaccess("rejectsif1-skynoise")) imdelete("rejectsif1-skynoise",verify-)
    ;
    if (imaccess("rejectsif1-ratio")) imdelete("rejectsif1-ratio",verify-)
    ;
    count(infile) | scan(nrows)
    dim="1,"//str(nrows)

    # Print to a log file for debug purposes
    printf("\n%s\n","***MIDDLE rejectsif1.cl:***",>>"rejectsif1-debug.log")
    printf("%s\n","nrows: "//str(nrows),>>"rejectsif1-debug.log")

    fields("rejectsif1-props2.tmp","2",lines="1-") | 
        rtextimage("STDIN","rejectsif1-see",header-,pixels+,nskip=0,dim=dim)
    fields("rejectsif1-props2.tmp","3",lines="1-") | 
        rtextimage("STDIN","rejectsif1-trans",header-,pixels+,nskip=0,dim=dim)
    fields("rejectsif1-props2.tmp","4",lines="1-") | 
        rtextimage("STDIN","rejectsif1-skynoise",header-,pixels+,nskip=0,
        dim=dim)
    fields("rejectsif1-props2.tmp","5",lines="1-") | 
        rtextimage("STDIN","rejectsif1-ratio",header-,pixels+,nskip=0,dim=dim)
	
    # Characterize the initial distributions.  I know there are a lot of
    # variables saved here, and unnecessarily so.  I do this only in order
    # to make it more convenient to revise this procedure to other rejection
    # methods.
    imstat("rejectsif1-see.fits",fields="npix,mean,midpt,stddev,min,max",
        lower=INDEF,upper=up0SEE,nclip=nclp0,lsigma=INDEF,usigma=usig0SEE,
        format-,cache-) | scan(npixSEE,meanSEE,midptSEE,stddevSEE,minSEE,maxSEE)
    imstat("rejectsif1-trans.fits",fields="npix,mean,midpt,stddev,min,max",
        lower=low0TR,upper=INDEF,nclip=nclp0,lsigma=lsig0TR,usigma=INDEF,
        format-,cache-) | scan(npixTR,meanTR,midptTR,stddevTR,minTR,maxTR)
    imstat("rejectsif1-skynoise.fits",fields="npix,mean,midpt,stddev,min,max",
        lower=INDEF,upper=INDEF,nclip=nclp0,lsigma=INDEF,usigma=usig0SN,
        format-,cache-) | scan(npixSN,meanSN,midptSN,stddevSN,minSN,maxSN)
    imstat("rejectsif1-ratio.fits",fields="npix,mean,midpt,stddev,min,max",
        lower=INDEF,upper=INDEF,nclip=nclp0,lsigma=lsig0RAT,usigma=INDEF,
        format-,cache-) | scan(npixRAT,meanRAT,midptRAT,stddevRAT,minRAT,maxRAT)

    # Print to a log file for debug purposes
    printf("\n%s\n","***MIDDLE rejectsif1.cl:***",>>"rejectsif1-debug.log")
    printf("%s %s\n","npixSEE, meanSEE, midptSEE: ",
	    str(npixSEE)//", "//str(meanSEE)//", "//str(midptSEE),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","stddevSEE, minSEE, maxSEE: ",
	    str(stddevSEE)//", "//str(minSEE)//", "//str(maxSEE),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","npixTR, meanTR, midptTR: ",
	    str(npixTR)//", "//str(meanTR)//", "//str(midptTR),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","stddevTR, minTR, maxTR: ",
	    str(stddevTR)//", "//str(minTR)//", "//str(maxTR),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","npixSN, meanSN, midptSN: ",
	    str(npixSN)//", "//str(meanSN)//", "//str(midptSN),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","stddevSN, minSN, maxSN: ",
	    str(stddevSN)//", "//str(minSN)//", "//str(maxSN),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","npixRAT, meanRAT, midptRAT: ",
	    str(npixRAT)//", "//str(meanRAT)//", "//str(midptRAT),
        >>"rejectsif1-debug.log")
    printf("%s %s\n","stddevRAT, minRAT, maxRAT: ",
	    str(stddevRAT)//", "//str(minRAT)//", "//str(maxRAT),
        >>"rejectsif1-debug.log")

    # Apply rejection criteria and determine the value of the REJECT keyword
    # to be added to the image headers.  Create output table listing the
    # images and the REJECT values.
    if (access(outfile)) delete(outfile,verify-)
    ;    
    list="rejectsif1-props2.tmp"
    while(fscan(list,s1,seeing,trans,skynoise,ratio) != EOF) {
        # Originally, I had hoped that using INDEFs for rejection parameters
        # would be flexible and allow images to pass if these parameters were
        # set to INDEF.  For example, if up1SEE=INDEF, then no upper limit on
        # the seeing would be established, and all images would pass this 
        # criterion.  However, it turns out that no image passed this criterion
        # since IRAF interprets (seeing <= INDEF) and (seeing >= INDEF) both
        # as false.  So, I needed to include checks such as
        # (const1SEE != INDEF), and string together many more criteria.  The
        # code below was the most straightforward way to apply the criteria
        # (and clearest way for a reader to follow).
        #
        # reject=0 indicates image is accepted, and is the default.
        # reject=1 indicates image is rejected, and is set as such if any one
        # 	of the criteria is met
        reject=""
        rejectTR=0
        rejectSN=0
        rejectRAT=0
        rejectSEE=0

        # Print to a log file for debug purposes
        printf("%s %s\n","Rejection procedure for image:",s1,>>"rejectsif1-debug.log")

        if ((low1TR != INDEF) && (trans < low1TR)) {
            rejectTR=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #1 met",>>"rejectsif1-debug.log")
        }
        ;
        if ((const1TR != INDEF) && (trans < maxTR*const1TR)) {
            rejectTR=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #2 met",>>"rejectsif1-debug.log")
        }
        ;	
        if ((frac1TR != INDEF) && (trans < frac1TR*midptTR)) {
            rejectTR=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #3 met",>>"rejectsif1-debug.log")
        }
        ;	
        if ((lsig1TR != INDEF) && (trans < midptTR-lsig1TR*stddevTR)) {
            rejectTR=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #4 met",>>"rejectsif1-debug.log")
        }
        ;	
        if ((usig1SN != INDEF) && (skynoise > midptSN+usig1SN*stddevSN)) {
            rejectSN=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #5 met",>>"rejectsif1-debug.log")
        }
        ;	
        if ((lsig1RAT != INDEF) && (ratio < midptRAT-lsig1RAT*stddevRAT)) {
            rejectRAT=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #6 met",>>"rejectsif1-debug.log")
        }
        ;	
        if ((up1SEE != INDEF) && (seeing > up1SEE)) {
            rejectSEE=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #7 met",>>"rejectsif1-debug.log")
        }
        ;
        if ((const1SEE != INDEF) && (seeing > midptSEE+const1SEE)) {
            rejectSEE=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #8 met",>>"rejectsif1-debug.log")
        }
        ;
        if ((usig1SEE != INDEF) && (seeing > midptSEE+usig1SEE*stddevSEE)) {
            rejectSEE=1
            # Print to a log file for debug purposes
            printf("%s\n","Condition #9 met",>>"rejectsif1-debug.log")
        }
        ;

        if (rejectTR == 1) {
            reject=reject//"tr,"
        }
        ;
        if (rejectSN == 1) {
            reject=reject//"sn,"
        }
        ;
        if (rejectRAT == 1) {
            reject=reject//"rat,"
        }
        ;
        if (rejectSEE == 1) {
            reject=reject//"see,"
        }
        ;
        if (reject != "") {
            reject=substr(reject,1,strlen(reject)-1)
        }
        ;

        # Print to a log file for debug purposes
        printf("%s\n","REJECT value (before header): "//reject,>>"rejectsif1-debug.log")
        
# This code is preserved here, for now, to remind me of a major "gotcha"
# in IRAF.  If any of the conditions are INDEF, then the condition is
# is never met, even if the conditions are connected by ORs and one of 
# them is clearly met.
#   
#        if (((low1TR != INDEF) && (trans < low1TR)) ||
#            ((const1TR != INDEF) && (trans < midptTR-const1TR)) ||
#            ((lsig1TR != INDEF) && (trans < midptTR-lsig1TR*stddevTR))) {
#            reject="tr,"
#        }
#        ;	
#        if (((up1SEE != INDEF) && (seeing > up1SEE)) ||
#            ((const1SEE != INDEF) && (seeing > midptSEE+const1SEE)) ||
#            ((usig1SEE != INDEF) && (seeing > midptSEE+usig1SEE*stddevSEE))) {
#            reject=reject//"see,"
#        }
#        ;
        
        # Check whether REJECT keyword has been previously set to "N!" or
        # "Y!".  If it has, then the rejection algorithm is overruled, but the
        # algorith decision is still appended to the "N!" or "Y!".  If the
        # REJECT keyword has been previously set, but not to "N!" or "Y!",
        # then act as if it was not previously set.  If REJECT keyword has 
        # not been previously set, then set it to "N" if the image meets none
        # of the rejection criteria.  Otherwise, set it to "Y"+reason, where
        # reason is "tr", "sn", "rat", "see" (or a combination of these four
        # reasons), depending on which rejection criterion the image meets.
        rejTMP="" ; hselect(s1,"REJECT",yes) | scan(rejTMP)

        # Print to a log file for debug purposes
        printf("%s\n","REJECT value (from header): "//rejTMP,>>"rejectsif1-debug.log")

        if (rejTMP != "") {
            rejTMP=substr(rejTMP,1,2)
            if ((rejTMP == "Y!") || (rejTMP == "N!")) {
                reject=rejTMP//reject
            } else {
                if (reject != "") {
                    reject="Y"//reject
                } else {
                    reject="N"
                }
            }
        } else {
            if (reject != "") {
                reject="Y"//reject
            } else {
                reject="N"
            }
        }

        # Print to a log file for debug purposes
        printf("%s\n","REJECT value (after header): "//reject,>>"rejectsif1-debug.log")
                
        # Write image and REJECT value to output table (outfile).
        # Also, write REJECT to header
        printf("%s %s\n",s1,reject,>>outfile)
        hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
    }
    list=""

    # Clean up.
#     delete("rejectsif1-*.tmp",verify-)
#     delete("rejectsif1-*.fits",verify-)
#     delete("rejectsif1-debug.log",verify-)

end
