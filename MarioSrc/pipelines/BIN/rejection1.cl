#!/bin/env pipecl
#
# REJECTION1
#
# Description:
#
#   This module controls the rejection of SIFs based on quality.
#   In its current implementation, it simply runs the rejectsif1 procedure
#   with parameters set as specified by rule.  For example, if
#   rule = "default", then rejectsif1 is run with the default parameters.
#
#   The module returns with an exit status.  If successful, it should
#   have produced an output file and written REJECT to the headers (see
#   documentation for rejectsif1.cl).
#
# Calling sequence:
#
#   rejection1(infile,rule)
#
# Examples:
#
#   rejection1(infile,"default")
#       This example will filter the list of SIFs given in
#       infile using the rejectsif1 procedure and its
#       default parameter values.
#
#   rejection1(infile,"badseeing,1.5")
#       This example will filter the list of SIFs given in 
#       infile using the rejectsif1 procedure and its
#       default parameter values, except it will reject any image
#       with seeing greater than 1.5" (i.e., up1SEE=1.5).
#
#   rejection1(infile,"badrmagzero,1.0")
#       This example will filter the list of SIFs given in
#       infile using the rejectsif1 procedure and its
#       default parameter values, except it will reject any image
#       with MAGZERO less than midpt(MAGZERO)-1.0.  This means
#       that any image with transparency less than 0.40 of the 
#       typical transparency of the suite of images will be 
#       rejected.
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful (call does not include appropriate input)
#   0 = Unsuccessful (rejection rule does not exist)
#
# History
#
#   T. Huard    200904-- Created
#   T. Huard    200905-- Restructured.
#   T. Huard    20090507 Previously worked within the datadir, but broke when
#                        I moved the code to more general location.  Needed
#                        to task rejectsif1.cl module with NHPPS_PIPESRC$/BIN/
#                        prepended in order for module to work.  Code was 
#                        also added to record information into log file.  Also,
#                        infile was made a visible, required parameter of
#                        rejectsif1.cl instead of a hidden parameter.  So,
#                        this module was changed accordingly.
#   T. Huard    20090617 Created "badrmagzero" method.  In doing so, I realized
#                        that const1TR in rejectsif1.cl was not practically
#                        useful.  So, const1TR was made obsolete and replaced
#                        with the more useful frac1TR.
#   T. Huard    20090813 Printing many lines to log file for debugging purposes.
#   Last Revised:   T. Huard    20090813    8:30am  

# Declare parameters
string  dataset,datadir,lfile
string  infile,rule,method
real    arg
int     status,istat

# Set filenames
names(envget("NHPPS_SUBPIPE_NAME"),envget("OSF_DATASET"))
dataset=names.dataset
datadir=names.datadir
lfile=datadir//names.lfile
set(uparm=names.uparm)
set(pipedata=names.pipedata)

# Do processing in data directory
cd(datadir)

# Log start of processing.
time | scan(line)
printf("\nREJECTION1 (%s): %s\n",dataset,line,>>lfile)

# Default exit status is one where rejection rule is not found
status=0

# Check that there is at least one argument (infile).
rule="default"
method="default"
arg=INDEF
istat=fscan(cl.args,infile,rule)
# Print to log file for debug purposes
printf("%s %s\n","...DEBUG (before check):  infile: ",infile,>>lfile)
printf("%s %s\n","...DEBUG (before check):  rule: ",rule,>>lfile)
printf("%s %s\n","...DEBUG (before check):  method: ",method,>>lfile)
printf("%s %s\n","...DEBUG (before check):  istat: ",str(istat),>>lfile)
if ((istat != 1) && (istat != 2)) {
    printf("0 Usage: rejection1.cl infile (rule)\n")
    logout
} else {
    if (rule != "default") {
        i=stridx(",",rule)
        if (i != 0) {
            method=substr(rule,1,i-1)
            arg=real(substr(rule,i+1,strlen(rule)))
        } else {
            method=rule
        }
    }
    ;
}
;
# Print to log file for debug purposes
printf("%s %s\n","...DEBUG (after check):  method: ",method,>>lfile)
printf("%s %s\n","...DEBUG (after check):  arg: ",arg,>>lfile)

# Load packages.
images
servers
proto
dataio
task rejectsif1 = "NHPPS_PIPESRC$/BIN/rejectsif1.cl"
cache rejectsif1

# Set default values for rejectsif parameters 
rejectsif1.outfile="rejection.out"
rejectsif1.up1SEE=5.
rejectsif1.const1SEE=INDEF
rejectsif1.usig1SEE=6.
rejectsif1.lsig1TR=INDEF
rejectsif1.const1TR = INDEF
rejectsif1.frac1TR=INDEF
rejectsif1.low1TR=INDEF
rejectsif1.usig1SN=INDEF
rejectsif1.lsig1RAT=6.
rejectsif1.usig0SEE=3.
rejectsif1.up0SEE=INDEF
rejectsif1.lsig0TR=3.
rejectsif1.low0TR=INDEF
rejectsif1.usig0SN=3.
rejectsif1.lsig0RAT=3.
rejectsif1.nclp0=3.
rejectsif1.status=1

# Print to log file for debug purposes
printf("%s %s\n","...DEBUG: rejectsif1.outfile",rejectsif1.outfile,>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.up1SEE",str(rejectsif1.up1SEE),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.const1SEE",str(rejectsif1.const1SEE),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.usig1SEE",str(rejectsif1.usig1SEE),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.lsig1TR",str(rejectsif1.lsig1TR),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.const1TR",str(rejectsif1.const1TR),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.frac1TR",str(rejectsif1.frac1TR),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.low1TR",str(rejectsif1.low1TR),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.usig1SN",str(rejectsif1.usig1SN),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.lsig1RAT",str(rejectsif1.lsig1RAT),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.usig0SEE",str(rejectsif1.usig0SEE),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.up0SEE",str(rejectsif1.up0SEE),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.lsig0TR",str(rejectsif1.lsig0TR),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.low0TR",str(rejectsif1.low0TR),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.usig0SN",str(rejectsif1.usig0SN),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.lsig0RAT",str(rejectsif1.lsig0RAT),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.nclp0",str(rejectsif1.nclp0),>>lfile)
printf("%s %s\n","...DEBUG: rejectsif1.status",str(rejectsif1.status),>>lfile)

# Apply rejection method
if (method == "default") {
    rejectsif1(infile)
    status=rejectsif1.status
    # Print to log file for debug purposes
    printf("%s %s\n","...DEBUG: status (default method):",str(status),>>lfile)
}
;		

if (method == "badseeing") {
    if (arg != INDEF) {
        rejectsif1.up1SEE=arg
        rejectsif1(infile)
        status=rejectsif1.status
        # Print to log file for debug purposes
        printf("%s %s\n","...DEBUG: status (badseeing method):",str(status),>>lfile)
    }
    ;	
}
;

if (method == "badrmagzero") {
    if (arg != INDEF) {
        rejectsif1.frac1TR=10.**(-arg/2.5)
        rejectsif1(infile)
        status=rejectsif1.status
        # Print to log file for debug purposes
        printf("%s %s\n","...DEBUG: status (badrmagzero method):",str(status),>>lfile)
    }
    ;	
}
;

if (method == "badbmagzero") {
    # Reject data x mag worse than the best magzero
    if (arg != INDEF) {
        rejectsif1.const1TR=10.**(-arg/2.5)
        rejectsif1(infile)
        status=rejectsif1.status
        # Print to log file for debug purposes
        printf("%s %s\n","...DEBUG: status (badbmagzero method):",str(status),>>lfile)
    }
    ;	
}
;

# Return status value
print(status)

logout
