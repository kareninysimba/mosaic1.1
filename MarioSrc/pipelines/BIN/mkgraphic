#!/bin/env pipecl
#
# MKGRAPHIC -- Make a graphic file from a list of images.
#
# The images are first block averaged (if desired) and then pasted together
# into a single image with IMCOMBINE using WCS offsets.  Then a GIF
# is made with automatic zscaling.  Finally the it is converted to the
# desired graphic type (if needed).

string	input			{prompt = "List of input images"}
string	output			{prompt = "Output rootname"}
string	offsets = "world"	{prompt = "Offsets"}
string	type = "gif"		{prompt = "Output type/extension"}
int	blk = 1			{prompt = "Blocking factor"}
string	zscale = "none"		{prompt = "Zscalem expression"}
string  combscale = "none"	{prompt = "Scaling for MEF"}

file	out, bpm
string	tmp, tmp1, tmp2

# Packages and tasks.
images
#dataio
mario
task $convert = "$!convert"
cache sections

# Get arguments.
if (fscan (cl.args, input, output, offsets, type, blk, zscale, combscale) < 2) {
    print ("usage: mkgraphic input output [offsets type blk zscale combscale]\n")
    logout 0
}
;

# Output filename.  We look for existing extension for various reasons
# including backwards compatibility with mkgif.
i = strlstr (".", output)
if (i == 0 || substr(output,i+1,i+strlen(type)) != type)
    out = output // "." // type
else
    out = output

# Temporary files.
tmp = mktemp ("mkgraphic")
tmp1 = tmp // "1.lis"
tmp2 = tmp // "2.lis"

# Expand list.
sections (input, option="fullname", > tmp1)

# Check for images.
if (sections.nimages == 0) {
    printf ("No images found (%s)\n", input)
    delete (tmp//"*")
    logout 0
}
;

# Block average images if needed.
if (blk > 1) {
    i = 0
    list = tmp1
    while (fscan (list, s1) != EOF) {
	i = i + 1
	blkavg (s1, tmp//i, blk, blk, option="average")
	print (tmp//i, >> tmp2)
    }
    list = ""
    rename (tmp2, tmp1)
}
;

# Replace existing output.
if (access (out))
    delete (out)
;

# Make GIF.
if (sections.nimages == 1) {
    bpm = ""; hselect ("@"//tmp1, "BPM", yes) | scan (bpm)
    if (bpm == "" || imaccess(bpm)==NO) {
	if (zscale == "" || zscale == "none")
	    printf ("zscale(i1)\n") | scan (zscale)
	else
	    printf ("zscalem(i1,%s)\n", zscale) | scan (zscale)
	export ("@"//tmp1, tmp, "gif", header="yes", outtype="",
	    outbands=zscale, interleave=0, bswap="no",
	    verbose=no, >& "dev$null")
    } else {
        if (blk > 1) {
	    blkavg (bpm, tmp//"_bpm.pl", blk, blk, option="sum")
	    bpm = tmp // "_bpm.pl"
	}
	;
	if (zscale == "" || zscale == "none")
	    printf ("zscalem(i1,i2==0)\n") | scan (zscale)
	else
	    printf ("zscalem(i1,i2==0&&%s)\n", zscale) | scan (zscale)
	export ("@"//tmp1//","//bpm, tmp, "gif", header="yes", outtype="",
	    outbands=zscale, interleave=0, bswap="no",
	    verbose=no, >& "dev$null")
	if (blk > 1)
	    imdelete (bpm)
	;
    }

} else {
    # Set offsets file.
    if (access(offsets)) {
        if (blk > 1) {
	    list = offsets
	    while (fscan (list, i, j) != EOF) {
	        i /= blk
		j /= blk
		printf ("%d %d\n", i, j, >> tmp2)
	    }
	    list = ""
	    s1 = tmp2
	} else
	    s1 = offsets
    } else if (offsets == "world")
        s1 = "world"
    else if (offsets == "dtv") {
	list = tmp1
	while (fscan (list, s1) != EOF) {
	    line = ""
	    if (blk == 1)
		hselect (s1, "dtv1,dtv2", yes) | scan (line)
	    else {
	        hselect (s1, "dtv1,dtv2", yes) | scan (i, j)
		if (nscan() == 2) {
		    i /= blk
		    j /= blk
		    print (i, j) | scan (line)
		}
		;
	    }
	    print (line, >> tmp2)
	    if (line == "") {
		delete (tmp2)
		break
	    }
	    ;
	}
	list = ""
	if (access(tmp2))
	    s1 = tmp2
	else
	    s1 = "world"
    } else {
	list = tmp1
	while (fscan (list, s1) != EOF) {
	    line = ""; hselect (s1, offsets, yes) | scan (line)
	    print (line, >> tmp2)
	    if (line == "") {
		delete (tmp2)
		break
	    }
	    ;
	}
	list = ""
	if (access(tmp2))
	    s1 = tmp2
	else
	    s1 = "world"
    }

    imcombine ("@"//tmp1, tmp, headers="", imcmb="",
	bpmasks=tmp//"_bpm", rejmasks="", nrejmasks="", expmasks="",
	sigmas="", logfile="", combine="average", reject="none",
	project=no, outtype="real", outlimits="", offsets=s1,
	masktype="none", maskvalue=0., blank=0., scale=combscale,
	zero="none", weight="none", statsec="", expname="",
	lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1, nkeep=1,
	mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
	snoise="0.", sigscale=0.1, pclip=-0.5, grow=0.)

    if (zscale == "" || zscale == "none")
	printf ("zscalem(i1,i2==0)\n") | scan (zscale)
    else
	printf ("zscalem(i1,i2==0&&%s)\n", zscale) | scan (zscale)
    export (tmp//","//tmp//"_bpm", tmp, "gif", header="yes",
	outtype="", outbands=zscale, interleave=0,
	bswap="no", verbose=no, >& "dev$null")

    imdelete (tmp//","//tmp//"_bpm")
}

if (type == "gif")
    rename (tmp//".gif", out)
else
    convert (tmp//".gif", out)

# Verbose output.
if ((verbose>=1) && access (out))
    printf ("MKGRAPHIC: Created %s\n", out)
;

# Clean up.
if (blk > 1)
    imdelete ("@"//tmp1)
;
delete (tmp//"*")

logout 1
