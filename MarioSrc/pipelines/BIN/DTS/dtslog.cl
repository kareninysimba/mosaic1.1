#!/bin/env pipecl2
#
# DTSLOG -- Log final DTS results.
#
## =========
## dtslog.cl
## =========
## ---------------------
## log final DTS results
## ---------------------
## 
## :Manual group:   BIN/DTS
## :Manual section: 1
## 
## Usage
## =====
## 
## ``dtslog.cl``
## 
## Input
## =====
## 
## * Input dataset list
## 
## Output
## ======
## 
## * Standard outputs: log file, list
## * PSQ database updated with completion time
## 
## Exit Status
## ===========
## 
## | 0 Unspecified error
## | 1 Successful completion
## 
## Description
## ===========
## 
## This module logs the number of files saved in the log files and marks the
## PSQ entry as completed.

string	ilist, odir, udir
int	nlist, nfiles

# Load tasks and packages.
task $psqlog = "$!psqlog"

# Set input.
ilist = names.indir // names.dataset // "." // names.pipe
if (access (ilist) == NO)
    logout 0
;


# Set output.
odir = names.rootdir // "output/" // names.dataset
if (access(names.indir//names.dataset//".odir")) {
    concat (names.indir//names.dataset//".odir") | scan (odir)
    udir = odir
} else if (access(names.indir//"default.odir")) {
    concat (names.indir//"default.odir") | scan (odir)
    udir = odir
} else
    udir = ""
;
odir += "/"
path (odir) | scan (odir)

# Create output list with new pathnames.
pathnames (odir//"*", > "dtslog.tmp")
if (udir == "")
    match (names.rootdir, ilist, >> "dtslog.tmp")
;
count ("dtslog.tmp") | scan (nfiles)
if (nfiles == 0)
    print ("# No files saved", >> "dtslog.tmp")
;

# Log number of files moved.
sort (ilist) | uniq | count | scan (nlist)
printf ("Saved %d/%d files\n", nfiles, nlist) | tee (names.lfile)

# Finish up.
delete (ilist)
if (udir == "")
    rename ("dtslog.tmp", names.dataset//".list")
else
    delete ("dtslog.tmp")

# Log in PSQ.
psqlog (names.shortname, "completed")

logout 1
