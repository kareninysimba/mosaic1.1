#!/bin/env pipecl2
#
# DTS2SDT -- Setup for calling SDT pipelines.
#
## ==========
## dts2sdt.cl
## ==========
## -------------------------------------------
## separate input list into node lists for SDT
## -------------------------------------------
## 
## :Manual Group:   BIN/DTS
## :Manual Section: 1
## 
## Usage
## =====
## 
## ``dts2sdt.cl``
## 
## Input
## =====
## 
## - standard input list containing files to be separated
## - optional file ``[dataset].odir`` or ``default.odir`` in the standard
##   input directory
## 
## Output
## ======
## 
## - temporary lists in the data directory with names ``sdt_[node].tmp``
##   and a list of these lists called ``sdt.tmp``
## - the directory specified by the contents of a ``.odir`` file or, if
##   neither is present, ``output/[dataset]`` is created if needed
## 
## Exit Status
## ===========
## 
## | 0 - unspecified error
## | 1 - successful creation of the output
## | 2 - no data was found in the input list
## 
## Description
## ===========
## 
## The standard input list containing files from multiple nodes is separated
## into lists of files from each node.  A list of lists is also produced.
## These files are intended to be used to trigger instances of the SDT
## pipeline.  The output directory for the SDT pipelines to save files is
## created if needed.  The output directory may be specified by the contents
## of the file ``[dataset].odir`` or ``default.odir`` (in that order) in
## the input directory.  If neither if present a directory ``[dataset]``
## in the standard pipeline output directory is used.
## 
## The actual work of separating the input list is performed by
## ``list2nodes.cl``.

string	ilist, rootdir, odir

# Set tasks.
task	$list2nodes = "$!list2nodes.cl"

ilist = names.indir // names.dataset // "." // names.pipe
path (names.rootdir) | scan (rootdir)

# The output directory can be overridden by a file in the input directory.
# It is an error if the default directory already exists.
odir = names.rootdir // "output/" // names.dataset
if (access(names.indir//names.dataset//".odir"))
    concat (names.indir//names.dataset//".odir") | scan (odir)
else if (access(names.indir//"default.odir"))
    concat (names.indir//"default.odir") | scan (odir)
;
odir += "/"
path (odir) | scan (odir)

# Create output directory if necessary.
if (access (odir) == NO) {
    mkdir (odir)
    sleep 1
}
;

# Create data files.
list2nodes ("sdt", "input") | match ("PLEXIT") | scan (line)

plexit (exitstring=line)
