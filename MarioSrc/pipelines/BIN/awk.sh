#!/bin/sh

# check the input parameters
ARGN=$#
if [ "$ARGN" != "2" ] ; then 
    echo "usage: $0 string pattern"
    exit 1
fi

# send the string to awk
echo $1 | awk "$2"

exit 0
