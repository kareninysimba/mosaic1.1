#!/bin/env pipecl
#
# WEIGHTING1
#
# Description:
#
#   This module controls the weighting of SIFs for the purpose of stacking.
#   In its current implementation, it simply runs the weightsif1 procedure
#   with parameters set as specified by rule.  For example, if
#   rule = "default" or rule = "total", then weightsif1 is run with
#   the default parameters and the weighting is performed based on
#   seeing, transparency, and skynoise.  See below for examples.
#
#   The module expects the infile to have the following columns: filename,
#   seeing, transparency, skynoise.  The module produces an output table,
#   "weighting.out", with the following columns: filename, weeing,
#   wtransns, wtot.
#
#   The module returns its exit status.
#
# Calling sequence:
#
#   weighting1(infile,rule)
#
# Examples:
#
#   weighting1(infile,"see")
#       This example will compute the seeing, transparency/skynoise, and
#       total weighting factors using weightsif1 and its default parameter
#       values.  When stacking, however, it will consider only the seeing
#       weights.
#
#   weighting1(infile,"transns")
#       This example will compute the seeing, transparency/skynoise, and
#       total weighting factors using weightsif1 and its default parameter
#       values.  When stacking, however, it will consider only the weights
#       based on transparency/skynoise.
#
#   weighting1(infile,"total")
#       This example will compute the seeing, transparency/skynoise, and
#       total weighting factors using weightsif1 and its default parameter
#       values.  When stacking it will consider the total weights.
#
#   weighting1(infile,"default")
#       Same as example with rejection1("stkselect-toWEIGHT.txt","total").
#
#   weighting1(infile,"see,2.")
#       This example will compute the seeing, transparency/skynoise, and
#       total weighting factors using weightsif1 and its default parameter
#       values, except usig1SEE=2.  When stacking, however, it will consider
#       only the seeing weights.
#
#   weighting1(infile,"transns,2.")
#       This example will compute the seeing, transparency/skynoise, and
#       total weighting factors using weightsif1 and its default parameter
#       values, except usig1SEE=2.  When stacking, however, it will consider
#       only the weights based on transparency/skynoise.  So, the argument
#       "2." does not really affect the stack, but this functionality is
#       included since it enables one to more easily restack SIFs based on
#       the seeing or total weights (with usig1SEE=2), if desired.
#
#   weighting1(infile,"total,2.")
#       This example will compute the seeing, transparency/skynoise, and
#       total weighting factors using weightsif1 and its default parameter
#       values, except usig1SEE=2.  When stacking it will consider the
#       total weights.
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful (call does not include appropriate input)
#   0 = Unsuccessful (weighting rule does not exist)
#
# History
#
#   T. Huard    20090506 Created
#   T. Huard    20090507 Previously worked within the datadir, but broke when
#                        I moved the code to more general location.  Needed
#                        to task rejectsif1.cl module with NHPPS_PIPESRC$/BIN/
#                        prepended in order for module to work.  Code was 
#                        also added to record information into log file.
#                        Also, infile was made a visible, required parameter
#                        of weightsif1.cl instead of a hidden parameter.
#                        So, this module was changed accordingly.
#   T. Huard    20090813 Printing many lines to log file for debugging purposes.
#   Last Revised:   T. Huard    20090813    10:00am  

# Declare parameters
string  dataset,datadir,lfile
string  infile,rule,method
real    arg
int     status,istat

# Set filenames
names(envget("NHPPS_SUBPIPE_NAME"),envget("OSF_DATASET"))
dataset=names.dataset
datadir=names.datadir
lfile=datadir//names.lfile
set(uparm=names.uparm)
set(pipedata=names.pipedata)

# Do processing in data directory
cd(datadir)

# Log start of processing.
time | scan(line)
printf("\nWEIGHTING1 (%s): %s\n",dataset,line,>>lfile)

# Default exit status is one where weighting rule is not found
status=0

# Check that there is at least one argument (infile).
rule="default"
method="default"
arg=INDEF
istat=fscan(cl.args,infile,rule)
# Print to log file for debug purposes
printf("%s %s\n","...DEBUG (before check):  infile: ",infile,>>lfile)
printf("%s %s\n","...DEBUG (before check):  rule: ",rule,>>lfile)
printf("%s %s\n","...DEBUG (before check):  method: ",method,>>lfile)
printf("%s %s\n","...DEBUG (before check):  istat: ",str(istat),>>lfile)
if ((istat != 1) && (istat != 2)) {
    printf("0 Usage: weighting1.cl infile (rule)\n")
    logout
} else {
    if (rule != "default") {
        i=stridx(",",rule)
        if (i != 0) {
            method=substr(rule,1,i-1)
            arg=real(substr(rule,i+1,strlen(rule)))
        } else {
            method=rule
        }
    }
    ;
}
;
# Print to log file for debug purposes
printf("%s %s\n","...DEBUG (after check):  method: ",method,>>lfile)
printf("%s %s\n","...DEBUG (after check):  arg: ",arg,>>lfile)

# Load packages.
images
servers
proto
dataio
task weightsif1 = "NHPPS_PIPESRC$/BIN/weightsif1.cl"
cache weightsif1

# Set default values for weightsif1 parameters 
weightsif1.outfile = "weighting.out"
weightsif1.usig1SEE = 1.
weightsif1.usig0SEE = 3.
weightsif1.up0SEE = INDEF
weightsif1.nclp0 = 3.
weightsif1.status = 1

# Print to log file for debug purposes
printf("%s %s\n","...DEBUG: weightsif1.outfile",weightsif1.outfile,>>lfile)
printf("%s %s\n","...DEBUG: weightsif1.usig1SEE",weightsif1.usig1SEE,>>lfile)
printf("%s %s\n","...DEBUG: weightsif1.usig0SEE",weightsif1.usig0SEE,>>lfile)
printf("%s %s\n","...DEBUG: weightsif1.up0SEE",weightsif1.up0SEE,>>lfile)
printf("%s %s\n","...DEBUG: weightsif1.nclp0",weightsif1.nclp0,>>lfile)
printf("%s %s\n","...DEBUG: weightsif1.status",weightsif1.status,>>lfile)
	
# Apply weighting method, SORT the images in the output file by
# decreasing weight (whichever weight is indicated by method),
# and add column of relative weights to output file.
if ((method == "default") || (method == "total") || (method =="see") ||
    (method == "transns")) {
    # Weighting method
    if (arg != INDEF) weightsif.usig1SEE=arg
    ;
    weightsif1(infile)
    status=weightsif1.status
    print(status)
} else {
    print("0 Weighting rule does not exist for weighting.")
    logout
}

logout
