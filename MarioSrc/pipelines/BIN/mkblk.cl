#!/bin/env pipecl
#
# MKBLK -- Block average images.  This is used for making GIFs of the
# mosaic field.

int     bk1 = 8        # First blocking factor
int     bk2 = 8        # Second blocking factor applied to first block average
int     xsize = 2048    # CCD size in x
int     ysize = 4096    # CCD size in y
int     gap = 64        # Gap between CCDs before blocking

string	pipename, dataset, lfile, bpm, blk1, blk2, bpmblk1, bpmblk2, blklist
int	xbin, ybin, xoff, yoff, blk

# Tasks and packages.
utilities
images

# Get optional parameters.
i = fscan (cl.args, bk1, bk2, xsize, ysize, gap)

# Set directories and files.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
lfile = names.datadir // names.lfile
set (uparm = names.uparm)
cd (names.datadir)

# Log start of processing.
printf ("\nMKBLK (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Make list of exposures.  We need special steps for scl files.
pipename = names.pipe
if (pipename == "scl") {
    sclnames (dataset)
    blklist = "mkblk.tmp"
    print (sclnames.cal, > blklist)
} else if (pipename == "sif") {
    sifnames (dataset)
    blklist = "mkblk.tmp"
    printf ("%s %s %s %s %s %s\n", sifnames.image, sifnames.bpm,
        sifnames.blk1, sifnames.bpmblk1, sifnames.blk2, sifnames.bpmblk2,
	> blklist)
} else if (pipename == "frg") {
    frgnames (dataset)
    blklist = frgnames.blklist
} else if (pipename == "sft") {
    sftnames (dataset)
    blklist = sftnames.blklist
} else if (pipename == "pgr") {
    pgrnames (dataset)
    blklist = pgrnames.blklist
} else {
    blklist = "mkblk.tmp"
    files (dataset//".fit?,*_frg.fits,*_sft.fits,*_s.fits",
	> blklist)
}

# Set initial offsets.
xoff = INDEF; yoff = INDEF
s1 = ""; head (blklist) | scan (s1)
hselect (s1, "detsec", yes) | translit ("STDIN", "[:,]", " ") |
    scan (xoff, i, yoff, i)
xbin = 1; ybin = 1; hselect (s1, "ccdsum", yes) | scan (line)
i = fscan (line, xbin, ybin)
if (xoff != INDEF && yoff != INDEF) {
    xoff -= 1; xoff /= xbin; xoff += 64 * (xoff / 2048) / xbin
    yoff -= 1; yoff /= ybin; yoff += 64 * (yoff / 4096) / ybin
    printf ("%d %d\n", xoff, yoff) | scan (line)
} else
    line = ""

# Do first block average.
blk = bk1 / xbin
if (line != "") {
    xoff /= blk
    yoff /= blk
    printf ("%d %d\n", xoff, yoff) | scan (line)
}
;

list = blklist
while (fscan (list, s3, bpm, blk1, bpmblk1, blk2, bpmblk2) != EOF) {
    i = strlstr (".fits", s3) - 1
    if (i > 0)
	s1 = substr (s3, 1, i)
    else
        s1 = s3

    if (nscan() == 1) {
        bpm = s1 // "_bpm"
	blk1 = s1 // "_blk" // blk
	bpmblk1 = bpm // "_blk" // blk
    }
    ;

    if (imaccess (bpmblk1))
        ;
    else if (imaccess(bpm))
	blkavg (bpm, bpmblk1//".pl", blk, blk, option="sum")
    else if (substr(s1,i-1,i) == "_s")
        bpm = substr (s1, 1, i-2) // "_bpm"
    else
        bpm = ""

    if (imaccess(s1)) {
	blkavg (s1, blk1, blk, blk, option="average")
	if (bpm != "")
	    hedit (blk1, "bpm", bpmblk1, add+, update+, verify-, show-)
	else
	    hedit (blk1, "bpm", del+, update+, verify-, show-)
	if (line != "")
	    hedit (blk1, "gifoff", line, add+, update+, verify-, show-)
	;
    }
    ;
}

# Do second block average.
if (bk2 > 1) {
    blk = bk2 / xbin
    if (line != "") {
	xoff /= blk
	yoff /= blk
	printf ("%d %d\n", xoff, yoff) | scan (line)
    }

    list = blklist
    while (fscan (list, s3, bpm, blk1, bpmblk1, blk2, bpmblk2) != EOF) {
	i = strlstr (".fits", s3) - 1
	if (i > 0)
	    s1 = substr (s3, 1, i)
	else
	    s1 = s3

	if (nscan() == 1) {
	    blk1 = s1 // "_blk8"
	    bpmblk1 = bpm // "_blk8"
	    blk2 = s1 // "_blk" // (blk * blk) 
	    bpmblk2 = bpm // "_blk" // (blk * blk)
	}

	if (imaccess(bpmblk2))
	    ;
	else if (imaccess(bpm))
	    blkavg (bpmblk1, bpmblk2//".pl", blk, blk, option="sum")
	else if (substr(s1,i-1,i) == "_s")
	    bpm = substr (s1, 1, i-2) // "_bpm"
	else
	    bpm = ""

	if (imaccess(s1)) {
	    blkavg (blk1, blk2, blk, blk, option="average")
	    if (bpm != "")
		hedit (blk2, "bpm", bpmblk2, add+, update+, verify-, show-)
	    else
		hedit (blk2, "bpm", del+, update+, verify-, show-)
	    if (line != "")
		hedit (blk2, "gifoff", line, add+, update+, verify-, show-)
	    ;
	}
	;
    }
    list = ""
}
;

delete (blklist)

logout 1
