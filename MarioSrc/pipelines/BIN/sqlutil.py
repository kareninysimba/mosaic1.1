#
#  sql utilities
#

from pipeutil import *
from PipelineServers.DataManager import deserialize_sql_result_pkt

def execute_sql( sql, addr, port, timeout=30 ):
    pkt = composePacket ({'COMMAND': 'sql_query', 'SQL': sql})
    res = sendPacketTo (pkt, addr, int(port), timeout=timeout, will_respond=True)
    res = deserialize_sql_result_pkt ( res )
    fields = res['fields']
    data = res['data']
    return fields, data

def fetchrow ( fields, data):

    try:
        row = data[0]
    except:
        # No more data, return an empty result
        return
    del data[0]

    if ( (len(fields)>1) and (len(fields)!=len(row)) ):
        print "Cannot fetch row because fields and data do not match"
        #TODO sendmsg
        return
    
    col = 0
    refrow = {}
    for field in fields:
        field = field.lower ()
        refrow[field] = row[col]
        col += 1

    return refrow
