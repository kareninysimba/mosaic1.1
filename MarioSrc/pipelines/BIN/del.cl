#!/bin/env pipecl2
# 
# DEL -- delete files in specified list.

string  ilist = ""
bool	dellist = NO

# Get arguments.
i = fscan (args, ilist, dellist)
if (ilist == "")
    ilist = names.indir // nhpps.dataset // "." // nhpps.pipe
;

# Check files.
if (access(ilist)==NO) {
    printf ("ERROR: Can't access input list file (%s)\n", ilist)
    logout 0
}
;

print ("delete files")
#delete ("@"//ilist)
#if (dellist == YES)
#    delete (ilist)
#;

logout 1
