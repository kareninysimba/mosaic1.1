#!/bin/env pipecl
#
# strsetup
#
# Description:
#
# 	This module copies the badpixel masks to the current directory.
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful (Currently not set in module)
#
# History:
#
#	T. Huard  200808--	Created, documented.
#	T. Huard  20080816	Cleaned up code and documentation.
#	T. Huard  20080828	Added information to log about number of bpms expected but not copied.
#	T. Huard  20090911	Using strnames.cl now to help define names in this module.
#	Last Revised:  T. Huard  20080911  5:30pm
#

# Declare variables
int	exitval,icount,icountBAD
string  dataset,indir,datadir,ilist,lfile
string  bpm,dirFULL

# Load packages
task	$cp = "$!cp -r $(1) $(2)"
task	$ln = "$!ln -s $(1) $(2)"
servers
images
noao
nproto
nfextern
ace
redefine mscred = mscred$mscred.cl
mscred

# Set file and path names
strnames(envget("OSF_DATASET"))
	print("stackFILE in module: "//strnames.stackFILE)
	print("sifFILE in module: "//strnames.sifFILE)
	print("sifFILEBASE in module: "//strnames.sifFILEBASE)
	print("bpmFILE in module: "//strnames.bpmFILEBASE)
	print("bpmFILEBASE in module: "//strnames.bpmFILE)
	print("imcropFILE in module: "//strnames.imcropFILE)
	print("bpmcropFILE in module: "//strnames.bpmcropFILE)

dataset=strnames.dataset
indir=strnames.indir
datadir=strnames.datadir
ilist=strnames.ilist
lfile=strnames.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=strnames.uparm)
set (pipedata=strnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSTRsetup (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Setup directories.
delete (substr(strnames.uparm, 1, strlen(strnames.uparm)-1))
iferr {
    setdirs (strnames.sifFILE)
} then {
    exitval = 0
} else
    ;

if (exitval == 0)
    logout (exitval)
;

d_trace

# Copy the badpixel masks to the current directory.
icount=0; icountBAD=0

bpm = strnames.bpmFILEBASE
if (access(bpm)) delete(bpm,verify-)
imcopy (strnames.bpmFILE, bpm, verbose-)
if (access(bpm)) icount=icount+1
else icountBAD=icountBAD+1

#hedit (strnames.stackFILE, "BPM", "K4M07B_20071213_74acc7e-Uspare-kp4m20071214T084556-ccd4_stk_crp_bpm.pl")
bpm="" ; hselect(strnames.stackFILE,"BPM",yes) | scan(bpm)
# Deal with long names truncating the bpm.
if (strstr (".pl", bpm) == 0) {
    i = strldx (".", bpm)
    if (i > 0) {
	bpm = substr (bpm, 1, strldx(".",bpm)-1)
	hedit (strnames.stackFILE,"BPM",bpm)
    }
    ;
    bpm += ".pl"
}
;
print (bpm)
if (bpm != "") {
	if (access(bpm)) delete(bpm,verify-)
	;
	dirFULL=substr(strnames.stackFILE,1,strldx("/",strnames.stackFILE))
	imcopy(dirFULL//bpm,bpm,verbose-)
	if (access(bpm)) icount=icount+1
	else icountBAD=icountBAD+1
} else {
	printf("%s\n","No valid BPM keyword value found in header: "//bpm) | tee(lfile)
}

# Write some general information to the log file
printf("%s\n","STRsetup: Number of badpixel masks copied: "//str(icount)) | tee(lfile)
if (icountBAD >= 1) {
	printf("%s\n","STRsetup: Number of badpixel masks EXPECTED BUT NOT FOUND: "//str(icountBAD)) | tee(lfile)
	printf("%s\n","***************** SOME BPMs NOT FOUND!!! *****************") | tee(lfile)
}
;

logout(exitval)
