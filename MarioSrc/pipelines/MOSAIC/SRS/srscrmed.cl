#!/bin/env pipecl
#
# SRScrmed
#
# Description:
#
# 	This module runs crmedian on a list of (non-resampled) SIFs.  Backup files of the 
#	non-crmedian-corrected SIFs are made.
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful.  Currently, an unsuccessful run of this module yields this exit
#		status when there is no valid BPM keyword value found in the header of the
#		non-resampled SIF.  This error should not occur, in general.
#	3 = Unsuccessful.  For some reason crmedian failed to create an output file for at 
#		least one of the SIFs.  In this case, the input to the crmedian task (the
#		backup SIF) is copied to the output, the module finishes, the blackboard
#		entry is set to "E" (by srs.xml), and the pipeline is stopped.  Then, the
#		operator may resume the pipeline by changing this entry to "c".  I am not
#		sure if/when this error would occur, but I account for it out of my paranoi.
#
# History:
#
#	T. Huard  20080707	Created.
#	T. Huard  20080723	Debugged.
#	T. Huard  20080724	Added code to merge crmask to previous bpm.
#	T. Huard  20080729	Added documentation, checks, and some streamlining.
#	T. Huard  20080905	Changed bpm value from 6 to 7 for those pixels flagged 
#				by crmedian.  The sections of code previously used to merge
#				the original bpm with the one output from crmedian is now
#				written as a single mskexpr statement.
#	T. Huard  20080911	Added check of srs_trrej to determine crmedian should be used.
#				If not, log out with exit status of 1 (successful).
#	Last Revised:  T. Huard  20080911  12:55pm


# Declare variables
int    exitval
string bpmmerge,bpmorig,basename,orig,output,crmask,filename,raSTR,decSTR
string dataset,ifile,datadir,lfile

# Load packages
redefine mscred=mscred$mscred.cl
noao
imred
crutil
nproto
mscred
cache mscred

# Set file and path names
names ("srs", envget("OSF_DATASET"))
dataset = names.dataset
ifile = names.indir // dataset // ".srs"
datadir = names.datadir
lfile = datadir // names.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Check srs_trrej to determine whether crmedian should be used.  If not,
# log out with exit status of 1 (successful).
if (srs_trrej != "crmedian") {
	printf("%s\n","SRSCRMED: No CR/transient rejection done in this module.")
	logout(1)
}
;

# Undo previous run or crash of this module.  Backup the original input file.
if (access(ifile//"ORIG")) {
	if (access(ifile)) delete(ifile,verify-)
	;
	rename(ifile//"ORIG",ifile)
}
;
rename(ifile,ifile//"ORIG")
touch(ifile)

# Remove cosmic rays and artifacts using crmedian
exitval=1
list = ifile//"ORIG"
while (fscan(list,s1,raSTR,decSTR) != EOF) {

#	Copy previous (non-crmedian-merged) BPM to a backup (_NOCRMED.pl), and
#	copy it to FITS files (.fits and _NOCRMED.fits) in order to later merge
#	it with the crmedian mask.  Also, undo previous run or crash of this module.
	bpmmerge=""; hselect(s1,"BPM",yes) | scan(bpmmerge)
	if (bpmmerge == "") {
		printf("%s\n","No valid BPM keyword value found in header of non-resampled SIF." | tee(lfile)
		printf("%s\n","Log out with exit status of 2") | tee(lfile)
		logout(2)
	}
	;
	bpmorig=substr(bpmmerge,1,strlstr(".pl",bpmmerge)-1)//"_NOCRMED"//".pl"
	if (access(bpmorig)) {
		imdel(bpmmerge); imcopy(bpmorig,bpmmerge); imdel(bpmorig)
	}
	;
	copy(bpmmerge,bpmorig)
		
# 	Determine basename of non-resampled SIF
	basename=substr(s1,strldx("/",s1)+1,strstr(".fits",s1)-1)
	i=strlen(basename)
	if (substr(basename,i-1,i-1) == "_") {
		basename=substr(basename,1,i-2)
	}
	;
	
#	Set input ("orig"), output, and crmask filenames.  Also, undo previous run or
#	crash of this module.
	orig=basename+"_NOCRMED"
	output=basename		
	crmask=basename+"_crmask"	
	if (access(orig//".fits")) delete(orig//".fits",verify-)
	;
	if (access(output//".fits")) delete(output//".fits",verify-)
	;
	if (access(crmask//".fits")) delete(crmask//".fits",verify-)
	;
	imcopy(s1,orig)	# copy input to backup filename, use as input to crmedian
	
#	Run crmedian on SIF
	crmedian(orig,output,crmask=crmask,median="",sigma="",residual="",
		var0=0.,var1=0.,var2=0.,lsigma=10,hsigma=3,ncmed=5,nlmed=5,
		ncsig=25,nlsig=25)		
	if (access(output//".fits") == NO) {
		printf("%s\n","crmedian failed to create output for "//basename) | tee(lfile)
		printf("%s\n","The backup (non-crmedian-corrected) SIF has been copied as the output.") | tee(lfile)
		printf("%s\n","Then, to continue the pipeline without a crmedian-corrected SIF for") | tee(lfile)
		printf("%s\n","this dataset, change the blackboard entry for this module to 'c'.") | tee(lfile)
		exitval=3
		imcopy(orig,output)  # copy non-crmedian-corrected SIF; bpm is already copied
	}
	;
	pathnames(datadir//output//".fits") | scan(filename)
	printf("%s %s %s\n",filename,raSTR,decSTR,>>ifile)

#	Merge the mask from crmedian with the original mask.  Pixels identified in the 
#	crmedian mask are assigned a value of 7 in the merged mask, while pixels identified
#	as bad in the original mask are assigned their original values.  If a pixel is 
#	identified both in crmedian mask and the original mask, that pixel is flagged
#	in the merged mask with the original bad pixel value!  In other words, if a pixel
#	is identified as "bad", we do not trust that crmedian has identified a cosmic ray
#	hit, transient, or artifact affecting that pixel.
	if (exitval != 3) {
		mskexpr("m>0?m:(i>0? 7: 0)","mergedbpm.pl",crmask//".fits[pl]",refmask=bpmmerge)
		delete(bpmmerge,verify-)
		imrename("mergedbpm.pl",bpmmerge)
	}
	;
}
list=""

logout(exitval)
