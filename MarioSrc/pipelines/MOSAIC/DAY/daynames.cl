# DAYNAMES -- Directory and filenames for the DAY pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure daynames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	frggif			{prompt = "Fringe template GIF"}
file	frggif2			{prompt = "Fringe template GIF"}
file	frghdr			{prompt = "Header for fringe template"}
file	sftgif			{prompt = "Sky flat GIF"}
file	sftgif2			{prompt = "Sky flat GIF"}
file	sfthdr			{prompt = "Header for sky flat"}
file	pgrgif			{prompt = "Pupil ghost GIF"}
file	pgrgif2			{prompt = "Pupil ghost GIF"}
file	pgrhdr			{prompt = "Header for pupil ghost"}
file	gif			{prompt = "GIF"}
file	gif2			{prompt = "GIF"}
file	ogif2			{prompt = "Overlay GIF"}
file	hdr			{prompt = "Header for GIF"}
bool    base = no               {prompt = "Child part of dataset"}
string	pattern = ""		{prompt = "Pattern"}

begin
	# Set generic names.
	names ("day", name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	frggif = shortname // "_frg.gif"
	frggif2 = frggif // "2"
	frghdr = shortname // "_frg_00.fits"
	sftgif = shortname // "_sft.gif"
	sftgif2 = sftgif // "2"
	sfthdr = shortname // "_sft_00.fits"
	gif = shortname // "_s.gif"
	gif2 = gif // "2"
	pgrgif = shortname // "_pgr.gif"
	pgrgif2 = pgrgif // "2"
	pgrhdr = shortname // "_pgr_00.fits"
	hdr = shortname // "_s_00.fits"
	ogif = shortname // "_so.gif"

	if (pattern != "") {
	    ;
	}
end
