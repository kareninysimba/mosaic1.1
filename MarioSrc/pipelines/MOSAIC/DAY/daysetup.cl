#!/bin/env pipecl
#
# DAYSETUP -- Setup DAY processing data. 
# 
# The input is a list of @files for each filter.  Use the first one to
# define the uparm.

string	dataset, indir, datadir, ifile, lfile, refim, calops
int	founddir, status
file	caldir = "MC$"

# Packages and tasks.
task $cp = "$!cp"
images
servers

# Files and directories.
daynames (envget("OSF_DATASET"))
dataset = daynames.dataset
indir = daynames.indir
datadir = daynames.datadir
ifile = indir // dataset // ".day"
lfile = datadir // daynames.lfile

# Set a reference image.  If one is not found then trigger a no data found.
refim = ""; match ("ccd[0-9]*.fits", ifile) | scan (refim)
if (refim == "") {
    printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset); time
    sendmsg ("WARNING", "No data found", "", "VRFY")
    s1 = indir // "return/" // dataset // ".day"
    if (access (s1)) {
	head (s1, nlines=1) | scan (s2)
	delete (s1)
	touch (s2)
	print ("No data found for DAY pipeline", >> s2//"log")
	touch (s2//"trig")
    }
    ;
    logout 9
}
;

# Log start of processing.
printf ("\nDAYSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup UPARM.
s1 = ""; hselect (refim, "uparm", yes) | scan (s1)
if (s1 == "" || access (s1) == NO) {
    getcal (refim, "uparm", cm, caldir, obstype="", detector=instrument,
        imageid="", filter="", exptime="", mjd="!mjd-obs") | scan (i, line)
    if (i != 0) {
        sendmsg ("ERROR", "Getcal failed for uparm", line, "CAL")
	logout 0
    }
    ;
    s1 = ""; hselect (refim, "uparm", yes) | scan (s1)
}
;

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

cp ("-r", osfn(s1), "uparm")
if (access ("uparm") == NO)
    sendmsg ("ERROR", "Cannot copy uparm directory", "", "CAL")
;
set (uparm = daynames.uparm)
flpr

# Set status return based on operations to be performed.  This lets us
# use the xml file to define the logic of the operations.

s1 = ""; hselect (refim, "calops", yes) | scan (calops)

# Pupil Ghost
if (pgr_cal == "no")
    i = 0
else
    i = min (1, stridx ("G", calops))

# Fringe Correction
if (frg_cal == "no")
    j = 0
else
    j = min (1, stridx ("A", calops))

# Sky Flat
if (sft_cal == "no")
    k = 0
else
    k = min (1, stridx ("H", calops))

status = 1 + k + 2 * j + 4 * i

# Write findings to log file:
printf("CALOPS string: %s\n", calops )
i = status
if (i>4) {
    printf("Pupil ghost removal flag is set\n")
    i = i-4
} else {
    printf("Pupil ghost removal flag is not set\n")
}
if (i>2) {
    printf("Fringe correction flag is set\n")
    i = i-2
} else {
    printf("Fringe correction flag is not set\n")
}
if (i>1) {
    printf("Sky flat flag is set\n")
    i = i-1
} else {
    printf("Sky flat flag is not set\n")
}

logout (status)
