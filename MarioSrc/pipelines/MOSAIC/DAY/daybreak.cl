#!/bin/env pipecl
#
# DAYBREAK -- Start DAY pipeline at day break.

file	dataset

# Load packages and tasks.
task $date = "$!date -u +%Y%m%dT%H%M%S"

# Define dataset name.
cd ("MOSAIC_DAY$/input/")
date | scan (dataset)
while (access (dataset//".day") == YES) {
    sleep (1)
    date | scan (dataset)
}

# Collect data processed by the MEF pipeline.
pathnames ("MOSAIC_MEF$" // "/output/*trig", > dataset//".tmp")
list = dataset//".tmp"
while (fscan (list, s2) != EOF) {
    s2 = substr (s2, 1, strlstr ("trig", s2)-1)
    print (s2)
    print (s2, >> dataset//".day")
}
list = ""

# Clean up.
delete ("@"//dataset//".tmp")
delete (dataset//".tmp")

# Trigger the DAY pipeline.
if (access (dataset//".day")) {
    printf ("\nDAYBREAK (%s): ", dataset)
    time
    touch (dataset//".daytrig")
}
;

logout 1
