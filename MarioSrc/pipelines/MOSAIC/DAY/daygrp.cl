#!/bin/env pipecl
#
# DAYGRP
#
# Description:
#
#    Sort through a list of SIFs to create sublists of dither suites.
#    This requires RSP to have run and also set RSPGRP keyword.
# 
# Exit Status Values:
#
#    1 = Successful
#    2 = No rebinned SIFs to be stacked.  This can occur in two cases:
#	    - when there are no rebinned SIFs
#	    - when are no sequences have more than 1 SIF to be stacked

# Declare variables
string	dataset, indir, datadir, ilist, lfile, rspgrp, infile

# Load packages
images
proto

# Set file and path names. Work in data directory.
names ("day",envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir//dataset//".day"
lfile = datadir//names.lfile
set (uparm=names.uparm)
cd (datadir)

# Log start of processing.
printf ("\nDAYGRP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Input file is the return file from SRS to RSP.  If this file does not exist,
# then exit since there are no rebinned SIFs to group. 

infile=datadir//dataset//"-rsp.rsp"
if (access(infile)==NO) logout (2)
;

# Get list of rebinned SIF images.  Loop through list and group by RSPGRP.
# Associate each image with it's full path BPM.  Add column
# for sorting so that images are combined in mdsstack in
# the same order.

match ("_r.fits", infile, >"daygrp.tmp")
list = "daygrp.tmp"
while (fscan (list, s1) != EOF) {
    s3 = substr (s1, strldx("/",s1)+1, 999)
    hselect (s1, "RSPGRP,BPM", yes) | scan (rspgrp, s2)
    match (s2, infile) | scan (s2)
    printf ("%s %s %s\n", s3, s1, s2, >> "rspgrp_"//rspgrp//".list")
}
list = ""; delete ("daygrp.tmp")

# Create lists for those with 2 or more in a group.
count ("rspgrp_*.list", > "daygrp.tmp")
list = "daygrp.tmp"
while (fscan (list, i, j, j, s1) != EOF) {
    if (i < 2 || s1 == "Total")
        next
    ;
    sort (s1) | fields ("STDIN", "2,3", > s1)
    path (s1, >> "daygrp.list")
}
list = ""; delete ("daygrp.tmp")

# Exit with appropriate error code.
if (access("daygrp.list")==NO)
    logout (2)
else
    logout (1)
