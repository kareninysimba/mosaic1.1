#!/bin/env pipecl
#
# DAY2PGR -- Send list of images to the PGR pipeline.
#
# This module sets the list of images and then uses call.cl to call the
# pipeline.

int	status = 2
string  dataset, indir, datadir, ilist, lfile, olist, temp1, temp2

# Tasks and packages.
task callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Setup directories and files.
daynames (envget ("OSF_DATASET"))
dataset = daynames.dataset
indir = daynames.indir
datadir = daynames.datadir
ilist = indir // dataset // ".day"
lfile = datadir // daynames.lfile
olist = "day2pgr1.tmp"
temp1 = "day2pgr2.tmp"
temp2 = "day2pgr3.tmp"
set (uparm = daynames.uparm)
cd (datadir)

# Create list to process.
match ("ccd[0-9]*.fits", ilist, > olist)
count (olist) | scan (i)
if (i == 0) {
    delete (olist)
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Find obm data.
match ("_obm.fits", ilist, > temp1)
count (olist) | scan (i)
if (i == 0) {
    delete (olist)
    delete (temp1)
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Add to output list.
concat (temp1, >> olist)
delete (temp1)

# Set comments.
line=""; match ("Bad sky coverage pupil ghost", lfile) | scan (line)
if (line != "")
    printf ("#A %s\n", line, >> olist)
;

# Call pipeline.
callpipe ("day", "pgr", "copy", datadir//olist, temp1, temp2)
concat (temp2) | scan (status)
concat (temp1)
delete ("day2frg*.tmp")

logout (status)
