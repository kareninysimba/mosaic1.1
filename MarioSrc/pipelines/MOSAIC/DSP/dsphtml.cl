#!/bin/env pipecl
#
# DSPHTML -- Make HTML pages for each GIF in the dataset list.
# This is called from the working directory.

string	dataset				# Dataset

file	htmlfile, temp1

# Get dataset.
if (fscan (cl.args, dataset) < 1)
    logout 1
;

# Get list of postage stamp GIFs.
temp1 = dataset//".tmp"
match (".gif2", dataset//".dsp", > temp1)

# For each postage stamp GIF make an HTML page.
list = temp1
while (fscan (list, s1) != EOF) {
    s1 = substr (s1, strldx ("/",s1)+1, strlstr(".gif2",s1)-1)
    s2 = substr (s1, stridx("_",s1)+1, 1000)
    htmlfile = s2 // ".html"

    printf ("<HTML><BODY>\n", > htmlfile)
    printf ("\n<IMG SRC=\"%s\">\n", s1//".gif", >> htmlfile)
    printf ("</TABLE>\n</BODY></HTML>\n", >> htmlfile)
}
list = ""; delete (temp1)
    
logout 1
