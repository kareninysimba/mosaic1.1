#!/bin/env pipecl
#
# DSPNGT -- Make HTML summary page for a night.
# This is called from the working directory.

string  s1last, s2last, type, col, hdr, s4, title
file	htmlfile, temp1, temp2

# Tasks and packages.
task dspsummary = "NHPPS_PIPESRC$/MOSAIC/DSP/dspsummary.cl"
task $ls = "$!ls"
images
utilities

htmlfile = "index.html"
pathname (".") | scan (title)
title = substr (title, 1, strldx("/",title)-1)
title = substr (title, strldx("/",title)+1,1000)

temp1 = mktemp ("mkhtml")
temp2 = mktemp ("mkhtml")

# Get list of gif2 files.
ls ("*/*.gif2", > temp1)

list = temp1
while (fscan (list, s3) != EOF) {
    s3 = substr (s3, 1, strlstr(".gif2",s3)-1)
    i = strldx ("/", s3)
    s4 = substr (s3, 1, i)
    s2 = substr (s3, i+1, 1000)
    j = 1 + strldx ("-", s2)
    i = strldx ("_", substr(s2, j, 1000))
    if (i > 0) {
        i += j
	type = substr (s2, i, 1000)
	if (type == "s" || type == "m")
	    s2 = substr (s2, 1, i-2)
        else if (type == "frg" || type == "sft") {
	    s2 = substr (s2, 1, i-2)
	    s2 = substr (s2,1,strldx("_",s2)-1)
	}
	;
    }
    ;

    # Default.
    type = "A"
    col= "c3"
    hdr = s3

    # Do special things for the different types of files.
    j = 1 + strldx ("-", s3)
    i = strldx ("_", substr(s3, j, 1000))
    if (i > 0) {
	i += j
	if (substr (s3, i, 1000) == "s") {
	    col= "c4"
	    #hdr = s4 // s2
	} else if (substr (s3, i, 1000) == "m") {
	    col= "c2"
	    hdr = substr (s3, 1, i-2)
	} else if (substr (s3, i, 1000) == "frg") {
	    type = "D"
	    s2 = substr (s3, strldx("/",s3)+1, i-2)
	    col= "c2"
	    s1 = "Fringe_Templates"
	} else if (substr (s3, i, 1000) == "sft") {
	    type = "E"
	    s2 = substr (s3, strldx("/",s3)+1, i-2)
	    col= "c2"
	    s1 = "Sky_Flats"
	}
	;
    }
    ;

    # Set grouping.
    line="Unknown_OBSTYPE"; hselect (hdr//"_00.fits", "obstype", yes) |
	translit ("STDIN", '"', delete+) |
	translit ("STDIN", ' ', '_') | scan (line)
    if (line == "zero") {
        type = "B"
	s1 = "Zero"
	col= "c2"
    } else if (line == "dome_flat") {
        type = "C"
	s1 = "Dome_Flat"
	col= "c2"
    } else if (type == "A") {
	line="Unknown_Filter"; hselect (hdr//"_00.fits", "filter", yes) |
	    translit ("STDIN", '"', delete+) |
	    translit ("STDIN", ' ', '_') | scan (line)
	s1 = line
    }

    printf ("%s %s %s %s %s %s\n", type, s1, s2, col, s3, hdr, >> temp2)
}
list = ""; delete (temp1)

if (access (temp2) == NO)
    logout 0
;
sort (temp2, > temp1); delete (temp2)

# Make the HTML page.
printf ("<HTML><TITLE>%s</TITLE><BODY><CENTER><H1>%s</H1></CENTER>\n",
   title, title,  > htmlfile)

s1last = ""; s2last = ""
list = temp1
while (fscan (list, type, s1, s2, col, s3, hdr) != EOF) {
    # Finish the last row.
    if (s2last != s2 && s2last != "")
	printf ("</TR>\n", >> htmlfile)
    ;

    # Finish the last table.
    if (s1 != s1last && s1last != "") {
	printf ("</TABLE>\n\n", >> htmlfile)
	s2last = ""
    }
    ;

    # Start a new table.
    if (s1last != s1) {
	print (s1) | translit ("STDIN", "_", " ") | scan (line)
	printf ("<CENTER><H2>%s</H2></CENTER>\n", line, >> htmlfile)
	printf ("\n<TABLE BORDER=\"1\">\n", >> htmlfile)
	printf ("<TH>HDR</TH>", >> htmlfile)
	if (type == "A")
	    printf ("<TH>NGT1</TH><TH>NGT2</TH><TH>DAY</TH>\n", >> htmlfile)
	else if (type == "B")
	    printf ("<TH>CAL:ZERO</TH>\n", >> htmlfile)
	else if (type == "C")
	    printf ("<TH>CAL:DFLAT</TH>\n", >> htmlfile)
	else if (type == "D")
	    printf ("<TH>FRG</TH>\n", >> htmlfile)
	else if (type == "E")
	    printf ("<TH>SFT</TH>\n", >> htmlfile)
	;
    }
    ;

    # Start a new row.
    if (s2last != s2) {
        printf ("\n<TR>\n", >> htmlfile)
	printf ("<TD ALIGN=\"LEFT\" VALIGN=\"CENTER\">\n", >> htmlfile)
	printf ("<A HREF=\"%s.hdr\" target=\"c1\">\n", hdr, >> htmlfile)
	dspsummary (hdr//"_00.fits", >> htmlfile)
	printf ("</A>\n</TD>\n", >> htmlfile)
	j = 2
    }
    ;

    # Skip to desired column.
    while (j < int(substr(col,2,2))) {
	printf ("<TD>&nbsp;</TD>\n", >> htmlfile)
	j += 1
    }

    # Make a shorter names.
    s4 = substr (s2, strldx("-",s2)+1, 1000)

    # Make table entry.
    printf ("<TD ALIGN=\"CENTER\" VALIGN=\"BOTTOM\">\n", >> htmlfile)

    printf ("<A HREF=\"%s.gif\" target=\"%s\">\n", s3, col, >> htmlfile)
    printf ("<IMG SRC=\"%s.gif2\"></A><BR>\n", s3, >> htmlfile)
    printf ("<A HREF=\"%s.hdr\" target=\"c1\">%s</A>\n", hdr, s4, >> htmlfile)
    if (access(s3//"_o.gif"))
        printf ("<A HREF=\"%s_o.gif\" target=\"%s\"> O</A>\n",
	    s3, col, >> htmlfile)
    else if (access(s3//"o.gif"))
        printf ("<A HREF=\"%so.gif\" target=\"%s\"> O</A>\n",
	    s3, col, >> htmlfile)
    ;

    printf ("</TD>\n", >> htmlfile)
    j += 1

    # Save last values.
    s1last = s1
    s2last = s2
}
list = ""; delete (temp1)

# Finish last row.
printf ("</TR>\n", >> htmlfile)

# Finish table.
printf ("</TABLE>\n", >> htmlfile)

# Finish HTML.
printf ("\n</BODY></HTML>\n", >> htmlfile)

logout 1
