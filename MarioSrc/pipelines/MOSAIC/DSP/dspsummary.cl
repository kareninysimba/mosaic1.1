# DSPSUMMAY -- Make HTML summary info from an image header.
# This is an HTML fragment written to the STDOUT to be used by
# the calling program.  The image argument may be  wildcard but only
# the first image will be processed.

procedure dspsummary (image)

file	image				{prompt="Image"}

begin
	file	im
	int	ival
	struct	sval

	# Tasks and packages.
	images
	utilities

	im = image

	# Image Title
	sval=""; hselect (im, "title", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	ival = strstr ("- DelRA",sval)
	if (ival > 0)
	    sval = substr (sval, 1, ival-1)
	printf ("%s<BR>\n", sval)

	# Original filename or number combined.
	ival=INDEF; hselect (im, "ncombine", yes) | scan (ival)
	if (ival == INDEF) {
	    sval=""; hselect (im, "filename", yes) |
		translit ("STDIN", '"', delete+) | scan (sval)
	    if (sval != "")
		printf ("%s<BR>\n", sval)
	} else
	    printf ("NCOMBINE = %d<BR>\n", ival)

	# Filter
	sval=""; hselect (im, "filter", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	if (sval != "")
	    printf ("%s<BR>\n", sval)

	# Magnitude zero point
	sval=""; hselect (im, "magzero", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	if (sval != "")
	    printf ("%s<BR>\n", sval)

	# Seeing
	sval=""; hselect (im, "dqseav", yes) | scan (sval)
	if (sval != "")
	    printf ("%s<BR>\n", sval)
end
