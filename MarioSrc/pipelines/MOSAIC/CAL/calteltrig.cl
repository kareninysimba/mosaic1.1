#!/bin/env pipecl
#
# CALTELTRIG -- Trigger calibrations from telescope @files.
#
# The telescope @files are produced by the CALTEL module.  This routine
# checks how long since new calibrations have been added to the @file
# and triggers the CAL pipeline based this time.

int	t = 300		# Default trigger interval

# Since there may be multiple instances of this module we lock to
# minimize potential conflicts over files.

if (fscan (cl.args, t) < 0)
    ;
;

# Load tasks and packages.
task $ls = "$!ls -lt --time-style=+%s"

# Set directories and files.
cd ("MOSAIC_CAL$/input")

# Since there may be multiple instances of this module we lock to
# minimize potential conflicts over files.  The lock file also sets
# the current time.

if (access ("calteltrig.lock"))
    logout 1
else
    touch ("calteltrig.lock")

# Get modification times.
ls ("calteltrig.lock") | scan (s1, s1, s1, s1, s1, i)
ls ("*.teltmp", >& "calteltrig.tmp")

# Determine times since last modification and trigger those which
# exceed a threshold.

list = "calteltrig.tmp"
while (fscan (list, s1, s1, s1, s1, s1, j, s1) != EOF) {
    if (nscan() != 7)
        next
    if (i - j < t)
        next
    head (s1) | scan (s2)
    s3 = substr (s2, strldx("/",s2)+1, strlstr(".cal",s2)-1)
    if (substr(s1,1,1) == "z") {
	rename (s1, s3//"Z.cal")
	touch (s3//"Z.caltrig")
    } else {
	rename (s1, s3//"F.cal")
	touch (s3//"F.caltrig")
    }
}
list = ""; delete ("calteltrig.*")

logout 1
