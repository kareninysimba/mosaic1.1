#!/bin/env pipecl
#
# CALSETUP -- Set CAL processing data including calibrations.
#
# The module is responsible for checking the input and doing initialization
# for a particular CAL dataset.  This includes setting parameters and getting
# calibrations.  Note that only some calibrations need to set here because
# calibrations specific to the SIF pipeline will be set later.

string	dataset, indir, datadir, ifile, ifile1, lfile
int	status = 1
int	founddir
file	caldir = "MC$"

# Packages and tasks.
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Files and directories.
calnames (envget("OSF_DATASET"))
dataset = calnames.dataset
indir   = calnames.indir
datadir = calnames.datadir
lfile   = datadir // calnames.lfile
ifile1  = indir   // dataset // ".cal"
ifile   = indir   // dataset // ".cal1"
set (uparm = calnames.uparm)
set (pipedata = calnames.pipedata)
cd (datadir)

# Convert list of lists into a simple list.
concatenate ("@"//ifile1, ifile)

# Log start of processing.
printf ("\nCALSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup directories first.
# This is done for all files so that the SCL pipelines don't have to
# do a getcal.
 
delete (substr(calnames.uparm, 1, strlen(calnames.uparm)-1))
list = ifile
while (fscan (list, s1) != EOF) {
    iferr {
        setdirs (s1//"[0]")
    } then {
        status = 0
        break
    } else
        ;
}
list = ""

if (status == 0)
    logout (status)
;

# Check input data.
cd (indir)
list = ifile
while (fscan (list, s1) != EOF) {
    # Check data is accessible.
    if (!imaccess (s1)) {
	list = ""
        s3 = substr( s1, strldx("/",s1)+1, strlen(s1) )
	sendmsg ("ERROR", "Input data not found", s3, "CAL")
	printf ("Input data %s not found.\n", s1) | tee (lfile)
	status = 0
	break
    } else
        ;
    # Check whether we need to wait for nightly zero.
    s3 = ""; hselect (s1//"[1]", "obstype", yes) | scan (s3)
    if (s3 != "zero" && s3 != "dark") {
	getcal (s1//"[1]", "zero", cm, caldir,
	    obstype="", detector=instrument, imageid="!ccdname",
	    filter="", exptime="", mjd="!mjd-obs",
	    match="!nextend,ccdsum") | scan (i, line)
	if (i != 0) {
            s2 = substr( s1, strldx("/",s1)+1, strlen(s1) )
	    #sendmsg ("WARNING", "Waiting for zero", s2//" - "//line, "CAL")
	    #status = 2
	    sendmsg ("ERROR", "No zero found", s2//" - "//line, "CAL")
	    status = 0
	    break
	}
	;
    }
    ;
}
list = ""
if (status != 1) {
    delete (ifile)
    logout (status)
}
;

# Clean up.
delete ("@"//ifile1)
rename (ifile, ifile1)

logout (status)
