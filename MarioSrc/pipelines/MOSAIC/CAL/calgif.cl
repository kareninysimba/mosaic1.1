#!/bin/env pipecl
# 
# Create GIFs from the SCL results.

string  dataset, datadir, lfile, hdr

# Load tasks and packages
task $mkgraphic = "$!mkgraphic"
noao
artdata

# Set names and directories.
calnames (envget ("OSF_DATASET"))
sclnames (calnames.dataset, pattern="ccd[0-9]*")
dataset = calnames.dataset
datadir = calnames.datadir
lfile = calnames.lfile
set (uparm = calnames.uparm)
cd (datadir)

# Check for files and exit if not available.
match (sclnames.blk1, "*.scl", print-) | sort ("STDIN", > "calgif.tmp")
count ("calgif.tmp") | scan (i)
if (i == 0) {
    delete ("calgif.tmp")
    logout 1
}
;

# Log start of module.
printf ("\nCALGIF (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Make GIFs.
#match (sclnames.blk1, "*.scl", print-) | sort ("STDIN", > "calgif.tmp")
mkgraphic ("@calgif.tmp", calnames.gif, "gifoff")

match (sclnames.blk2, "*.scl", print-) | sort ("STDIN", > "calgif.tmp")
mkgraphic ("@calgif.tmp", calnames.gif2, "gifoff")

hdr = ""; head ("calgif.tmp", nl=1) | scan (hdr)
if (hdr == "")
    logout 0
;

mkpattern (calnames.hdr, ndim=0, header=hdr)

delete ("calgif*.tmp")

logout 1
