# RSPNAMES -- Directory and filenames for the RSP pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure rspnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	parent			{prompt = "Parent part of dataset"}
file	child			{prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	rsp			{prompt = "Resampled image"}
file	bpm			{prompt = "Bad pixel mask"}
bool	base = no		{prompt = "Child part of dataset"}
string	pattern = ""		{prompt = "Pattern"}

begin
	# Set generic names.
	names ("rsp", name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	rsp = shortname // "_r"
	bpm = rsp // "_bpm"

	if (pattern != "") {
	    rsp += ".fits"
	    bpm += ".pl"
	}
end
