#!/bin/env pipecl
#
# SCLCMB -- Combine a list of single CCD calibration images.

int	status, istwilight
real	quality = 0.
string	dataset, indir, datadir, lfile, cal, calmap, ilist, im, flattype
struct	obstype

# Set packages and tasks.
images
cache mscred
mscred
servers
dataqual
task scldqzero	= "NHPPS_PIPESRC$/MOSAIC/SCL/scldqzero.cl"
task scldqflat	= "NHPPS_PIPESRC$/MOSAIC/SCL/scldqflat.cl"
task mkmap	= "NHPPS_PIPESRC$/MOSAIC/SCL/mkmap.cl"
cache mscred

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Set filenames.
sclnames (envget("OSF_DATASET"))
dataset = sclnames.dataset
indir = sclnames.indir
datadir = sclnames.datadir
lfile = datadir // sclnames.lfile
ilist = indir // dataset // ".scl"
mscred.logfile = ""
mscred.instrument = "pipedata$mosaic.dat"
mscred.verbose = yes
set (uparm = sclnames.uparm)
set (pipedata = sclnames.pipedata)
cal = sclnames.cal
calmap = sclnames.cal // "_map"

# Do processing in data directory.
cd (datadir)

# Log start of processing.
printf ("\nSCLCMB (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check if there are any images to use.
i = 0; count (ilist) | scan (i)
if (i == 0) {
    sendmsg ("ERROR", "No data to combine for this data set", "", "VRFY")
    logout (0)
}
;

# Determine the calibration type from the header of the first image.
head (ilist, nline=1) | scan (im)
flattype = "none"
istwilight = -1
obstype = ""; hselect (im, "obstype", yes) | scan (obstype)
if (obstype == "dome flat") {
    obstype = "flat"
    flattype = "dome"
    istwilight = 0
} else if (obstype == "sky flat") {
    obstype = "sflat"
    flattype = "twilight"
    istwilight = 1
}
;
if (obstype != "zero" && obstype != "dark" &&
    obstype != "flat" && obstype != "sflat") {
    sendmsg ("ERROR", "Unknown calibration type", obstype, "CAL")
    logout (0)
}
;

# Make the master calibration.
if (obstype == "zero" || obstype == "dark") {
    scldqzero (dataset, ilist, cal) | tee (lfile)
    quality = scldqzero.quality
    status = scldqzero.status
    if (status != 1) {
        if (i > 1) {
	    zcombine ("@"//ilist, cal, amps=no, offset="physical",
		imcmb="DTNSANAM", >> lfile)
	    quality = 0.
	    status = 1
	} else {
	    imcopy ("@"//ilist, cal)
	    quality = 0.
	    status = 1
	}
    }
    ;
} else {
    concat (ilist)
    scldqflat (dataset, ilist, flattype, cal, calmap) | tee (lfile)
    quality = scldqflat.quality
    status = scldqflat.status
}

# Delete the individual calibration images.
hselect ("@"//ilist, "BPM", yes, > "sclcmb.tmp")
imdelete ("@sclcmb.tmp", verify=no)
delete ("sclcmb.tmp")
imdelete ("@"//ilist, verify=no)
delete (ilist)

if (imaccess(cal)==YES) {
    # Set keyword for future reference.
    if (obstype == "zero") {
	hedit (cal, "POBSTYPE", "zero", add+, verify-, show+, update+,
	    >> lfile)
    } else if (obstype == "dark") {
	hedit (cal, "POBSTYPE", "dark", add+, verify-, show+, update+,
	    >> lfile)
    } else if (obstype == "flat") {
	hedit (cal, "POBSTYPE", "dflat", add+, verify-, show+, update+,
	    >> lfile)
    } else if (obstype == "sflat") {
	hedit (cal, "POBSTYPE", "sflat", add+, verify-, show+, update+,
	    >> lfile)
    }
    ;

    hedit( cal, "proctype", "MasterCal", add+, ver-, >> lfile )
    hedit (cal, "quality", quality, add+, ver-, >> lfile)
    pathname (cal//".fits", > ilist)
} else
    touch (ilist)

logout (status)
