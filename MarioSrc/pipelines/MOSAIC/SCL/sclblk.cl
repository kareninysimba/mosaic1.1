#!/bin/env pipecl
#
# SCLBLK -- Make block averaged versions.
# This is used for making preview images and so in this task we first decide
# if the preview images are desired.  All the work, including logging, is
# done by mkblk.

# Tasks and packages.
task pipeselect = "$!pipeselect"
task mkblk = "$!mkblk.cl $1; echo $? > $2"

# Set paths and files.
sclnames (envget("OSF_DATASET"))
cd (sclnames.datadir)

# Check if DSP pipeline running.
s1 = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "dsp", 0, 0) | scan (s1)
if (s1 == "")
    logout 1
;

# Make the block averaged versions.
mkblk (8, "sclblk.tmp")
concat ("sclblk.tmp") | scan (i)
delete ("sclblk.tmp")

logout (i)
