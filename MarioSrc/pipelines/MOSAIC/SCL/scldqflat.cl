# SCLDQFLAT -- Combine flat images with data quality checking.
#
# When the input list has more than one flat it checks for good ones.
# This rejection of bad flats is different from the rejection that is
# done by fcombine. Here rejection is done based on global statistics,
# whereas in fcombine rejection is done based on a per-pixel basis. In
# principle, a flat frame could be globally bad (e.g., over-exposed)
# but some pixels of such a frame would not be rejected by fcombine.
#
# The most likely problem with input flats is that they are either
# under- or over-exposed. Other errors, such as light leaks are much less
# common. To validate the input flats, the algorithm first checks the
# levels in the input frames, rejects ones that are either too faint or too
# bright. Next the remaining flats are combined and compared against
# a recent good flat.

procedure scldqflat (dataset, ilist, flattype, imname, immap)

string	dataset			{prompt="Dataset name"}
file	ilist			{prompt="List of input flat images"}
string	flattype		{prompt="Flat field type"}
file	imname			{prompt="Output flat image"}
file	immap			{prompt="Output flat map image"}
real	quality			{prompt="Quality"}
int	status			{prompt="Status return"}

struct	*fd

begin
	int	nin, nout, rulepassed, ampn, nkey
	int	result = 0
	real	rdnoise, gain, dqglmean, dqglsig, dqglfsat, dqdfcrat
	real	exptime, saturate, exprdnoise, mjd, mean, min
	string	rulepath, im, sim, refmap, bandtype, cast
	struct	filter
	file	olist
	file	caldir = "MC$"

	# Initialize.
	printf( "Starting SCLDQFLAT\n", >> lfile)

	quality = -1.
	status = 0
	nout = 0
	olist = mktemp ("tmpdq")
	
	# Define rules
	findrule (dm, "enough_domeflats", validate=1) | scan (rulepath)
	task enough_domeflats = (rulepath)
	findrule (dm, "flat_ok", validate=1) | scan (rulepath)
	task flat_ok = (rulepath)
	findrule (dm, "flatshape_ok", validate=1) | scan (rulepath)
	task flatshape_ok = (rulepath)
	findrule (dm, "domeflat_range", validate=1) | scan (rulepath)
	task domeflat_range = (rulepath)
	cache	flatshape_ok
	
	# Loop through list of flat fields.
	fd = ilist
	for (nin=0; fscan (fd, im)!=EOF; nin+=1) {
	    # Remove trailing .fits if present
            if (strlstr(".fits",im) > 0)
	        im = substr (im, 1, strlstr(".fits",im)-1)
	    ;
	    sim = substr (im, strldx("/",im)+1, 1000)

	    # Enter data product.
	    hselect (im, "mjd-obs", yes) | scan (mjd)
	    newDataProduct(sim, mjd, "domeflatimage", "") |
		scan( result, line )
	    if (result > 0)
	        sendmsg ("WARNING", "newDataProduct failed", line, "DM")
	    ;
	
	    # Get required header info and return with an error if missing.
	    rdnoise = INDEF; hselect (im, "rdnoise", yes) | scan (rdnoise)
	        nkey = nscan()
	    gain = INDEF; hselect (im, "gain", yes) | scan (gain)
	        nkey += nscan()
	    dqglsig = INDEF; hselect (im, "dqglsig", yes) | scan (dqglsig)
	        nkey += nscan()
	    dqglmean = INDEF; hselect (im, "dqglmean", yes) | scan (dqglmean)
	        nkey += nscan()
	    dqglfsat = INDEF; hselect (im, "dqglfsat", yes) | scan (dqglfsat)
	        nkey += nscan()
	    saturate = INDEF; hselect (im, "SATURATE", yes) | scan (saturate)
	        nkey += nscan()
	    exptime = INDEF; hselect (im, "exptime", yes) | scan (exptime)
	        nkey += nscan()
	    ampn = INDEF; hselect (im, "imageid", yes) | scan (ampn)
	        nkey += nscan()
	    filter = INDEF; hselect (im, "filter", yes) | scan (filter)
	        nkey += nscan()
	    bandtype = INDEF; hselect (im, "bandtype", yes) | scan (bandtype)
	        nkey += nscan()
	    if (nkey < 10) {
		fd = ""
		sendmsg ("WARNING", "Missing header information", sim, "DQ")
		print (rdnoise, gain, dqglsig, dqglmean, dqglfsat, saturate,
		    exptime, ampn, filter, bandtype)
		return
	    }
	
	    # Check basic parameters.
	    flat_ok (ampn, filter, dqglmean, saturate, dqglfsat, bandtype,
	        flattype) | scan (rulepassed)
	    if (rulepassed==1) {
		quality = 1.
		status = 1

		# Calculate count rate
		dqdfcrat = dqglmean / exptime
	
		printf ("%s\n", im, >> olist)
		nout = nout+1
	    } else {
		quality = -1.
		status = 0

		# Count rate not reliable, set to -1
		dqdfcrat = -1
	
                printf("Details: %f %f %f %s\n",
		    dqglmean, saturate, dqglfsat, bandtype )
		if (dqglmean > 100 && dqglmean <= 2000) {
		    quality = 0.
		    status = 2
		    printf ("%s\n", im, >> olist)
		    nout = nout + 1
		    sendmsg ("WARNING", "Level unusually high or low in flat",
		        sim, "DQ")
		} else
		    sendmsg ("WARNING", "Level too high or too low in rejected \
			flat", sim, "DQ")
	    }
	
	    # Write count rate to logfile, add to header and send to PMAS
	    if (verbose>=2)
		printf ("DQ | lamp count rate: %f\n", dqdfcrat)
	    hedit (im, "dqdfcrat", dqdfcrat, add+, verify-, show+, update+,
	        >> lfile)
	    printf ("%12.5e\n", dqdfcrat) | scan (cast)
	    setkeyval( class="domeflatimage", id=sim, dm=dm,
		keyword="dqdfcrat", value=cast )
	    printf ("%d\n", istwilight) | scan (cast)
	    setkeyval( class="domeflatimage", id=sim, dm=dm,
		keyword="dqistwi", value=cast )
	}
	fd = ""
	
	# No images.
	if (nout==0 || access(olist)==NO) {
	    sendmsg ("WARNING", "No flats to combine after level rejection",
	        "", "DQ")
	    status = 0
	    return
	}
	
	# Check for enough flats.
	enough_domeflats (nout) | scan (rulepassed)
	if (rulepassed == 0) {
	    quality /= 2.
	    sendmsg ("WARNING", "Small number of flats", str(nout), "DQ")
	}
	
	# Make the average flat.
	if (nout>1) {
	    fcombine ("@"//olist, imname, bpmask=sclnames.bpm//".pl", amps=no,
	        offset="physical", imcmb="DTNSANAM",
		masktype="novalue", maskvalue=3, >> lfile)
	} else  {
	    hselect ("@"//olist, "BPM", yes) | scan (im)
	    imcopy ("@"//olist, imname, verb+, >> lfile)
	    imcopy (im, sclnames.bpm//".pl", verb+, >> lfile)
	    hedit (imname, "BPM", sclnames.bpm//".pl", add+, show-)
	}

	# Determine mean and minimum values through mask and set everything
	# below the min to 1.  Later this is reset to a global value.
	mimstat (imname, imask="!BPM", fields="mean, min", nclip=0, format-) |
	    scan (mean, min)
	hedit (imname, "CCDMIN", min, add+, show-)
	imreplace (imname, 1., lower=INDEF, upper=min, radius=0)

	# Create a map of the flat and check the shape against a reference.
	if (flattype == "dome") {
	    mkmap (imname, immap, mean=mean, lthresh=min)
	    getcal (imname, "refflat", cm, caldir, obstype="",
		detector=instrument, imageid="!ccdname", filter="!filter",
		exptime="", mjd="!mjd-obs", dmjd=cal_dmjd, quality=0.,
		match="!nextend,ccdsum") | scan (i, line)
	    if (i != 0) {
		getcal (imname, "refflat", cm, caldir, obstype="",
		    detector=instrument, imageid="!ccdname", filter="!filter",
		    exptime="", mjd="!mjd-obs", dmjd=cal_dmjd, quality=0.,
		    match="!nextend,ccdsum") | scan (i, line)
	    }
	    ;
	    flpr hselect
	    refmap = ""; hselect (imname, "refflat", yes) | scan (refmap)
	    if (refmap == "") {
		sendmsg ("WARNING", "No reference flat field map", "", "DQ")
		flatshape_ok (immap, "") | scan (rulepassed)
	    } else
		flatshape_ok (immap, refmap) | scan (rulepassed)
	    if (rulepassed == 0) {
		sendmsg ("WARNING", "Bad flat field shape",
		    flatshape_ok.logstr, "DQ")
		printf ("WARNING: Bad flat field shape (%s)\n",
		    flatshape_ok.logstr)
		#quality = -1; status = 2
		quality = -0.5; status = 1
	    } else {
		printf ("Flat field shape OK (%s)\n", flatshape_ok.logstr)
		status = 1
	    }
	} else
	    status = 1
	
	if (status == 1) {
	    # Enter data product.
	    hselect (imname, "mjd-obs", yes) | scan (mjd)
	    newDataProduct(dataset, mjd, "avdomeflatimage", "") |
		scan( result, line )
	    if (result>0)
		sendmsg("WARNING", "newDataProduct failed", line, "DM")
	    ;
	    storekeywords( class="avdomeflatimage", id=imname, sid=dataset,
	        dm=dm )
	    status = 1
	}
	;
	# Finish up.
	delete (olist)
end
