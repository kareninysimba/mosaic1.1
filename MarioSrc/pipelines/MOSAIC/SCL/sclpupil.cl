#!/bin/env pipecl
#
# SCLPUPIL -- Remove pupil ghost from flat

bool pgflag
int ampn, rulepassed,nitem
string dataset, datadir, lfile, rulepath
string photindx, cal
struct tel, filter, obstype

# Load packages.
servers
images
proto
mscred
task mscextensions = "mscsrc$x_mscred.e"
task mscpupil = "mscsrc$mscpupil.cl"
task pupilfit = "mscsrc$x_mscred.e"
cache mscred, mscextensions

# Set file and path names.
sclnames (envget("OSF_DATASET"))
dataset = sclnames.dataset

# Define rules
findrule( dm, "remove_pupil_df", validate=1 ) | scan( rulepath )
task remove_pupil_df = (rulepath)
    
# Set filenames.
datadir = sclnames.datadir
lfile = datadir // sclnames.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = sclnames.uparm)
set (pipedata = sclnames.pipedata)
cal = sclnames.cal

# Log start of processing.
printf ("\nSCLPUPIL (%s): ", dataset) | tee (lfile)
time | tee (lfile)
    
# Go to data directory
cd (datadir)
    
obstype = ""
hselect (cal, "obstype", yes) | scan (obstype)
if (obstype == "dome flat")
    obstype = "dflat"
else if (obstype == "sky flat")
    obstype = "sflat"
;
if (obstype == "dflat" || obstype == "sflat") {

    # The reason the pupil ghost needs to be removed from the flat is
    # that otherwise the additive pupil ghost in the object frames will be
    # partially divided out, resulting in an apparently clean but incorrect
    # image, because the pupil ghost should be subtracted out of the object
    # frames. However, to remove the pupil ghost from the flat it should
    # be divided out. 
    # On dividing vs. subtracting the pupil ghost from the flat, from
    # the NOAO Deep Wide-Field Survey website:
    # "The choice of dividing (as indicated by the choice type=ratio in the
    # parameter file below) the ghost out of the dome-flat vs. subtracting
    # it has confused MANY of the researchers working with KPNO Mosaic data
    # -- and in fact I (BTJ) have failed to convince some of them that
    # "ratio" is the correct choice -- but it is! One way to think about this
    # is that because of the pupil ghost the CCD was not illuminated by the
    # light from a uniform flat field screen, but rather an image that had a
    # brighter 'circle' (the image of the pupil) around the center of the
    # field of view."
    # From the same website, on which filters should be corrected and how:
    # "The edges of the CCDs (a few rows on each) show up as brighter than
    # the rest of the CCD in the I and z-band flats. This flat field
    # structure will be mistakenly fit as part of the pupil ghost unless the
    # following fix is applied. For I-band and z-band dome-flats, as well as
    # other filters in this wavelength range, set the parameter
    # lmedian=yes. For data in the U- or Bw-bands this parameter should be
    # set to no.  Flats in the V, R, etc., bands do not need the
    # mscpupil task applied to them."
    
    # Check whether pupil ghost removal is requested in CALOPS
    s1=""
    hselect (cal, "CALOPS", yes) | scan (s1)
    pgflag = (stridx("G",s1) > 0)
    
    if (pgflag) {
        hselect( cal, "telescop", yes ) | scan( tel )
        nitem = nscan()
        hselect( cal, "filter", yes ) | scan( filter )
        nitem = nitem+nscan()
        hselect( cal, "photindx,imageid", yes ) | scan( photindx,ampn )
        if ((nitem+nscan())==4) {
    
            # Test whether pupil ghost should be removed.
            remove_pupil_df( tel, photindx, ampn ) | scan( rulepassed )
        
            # remove_pupil_df will return one of three values:
	    # 0 for no correction,
            # 1 for blue filter correction, and
	    # 2 for red filter correction. 
        
            if (rulepassed==0) {
                printf("Pupil ghost removal not needed for %s:\n", cal)
                printf("Telescope: %s\n", tel )
                printf("Filter: %s (%s)\n", filter,photindx )
                printf("Amplifier: %d\n", ampn )
            } else {
		# Name to save the original image.
		s1 = cal // "wp"
		imrename( cal, s1 )

		# For Mosaic1.1 we need to normalize the two amps first.
		# Don't use imarith because it deletes CCDMEAN, etc.
		if (cl.instrument == "Mosaic1.1") {
		    imrename (s1, "sclpupil1")
		    blkavg ("sclpupil1", "sclpupil2", 1, 4096)
		    blkrep ("sclpupil2", s1//"n", 1, 4096)
		    imexpr ("a/b", s1, "sclpupil1", s1//"n", verb-)
		    imdelete ("sclpupil*.fits")
		}
		;
		    
                # Remove pupil ghost.
                if (rulepassed==1) {
                    printf("Removing pupil ghost for %s with lmedian=no\n",
		        cal )
		    mscpupil (s1, cal, masks="", type="ratio",
			lmedian=no, verbose=yes)

                }
                ;
                if (rulepassed==2) {
                    printf("Removing pupil ghost for %s with lmedian=yes\n",
		        cal )
		    mscpupil (s1, cal, masks="", type="ratio",
			lmedian=yes, verbose=yes)
                }
                ;
		
		# Remove normalization if needed.
		if (imaccess(s1//"n")) {
		    imrename (cal, "sclpupil1")
		    imexpr ("a*b", cal, "sclpupil1", s1//"n", verb-)
		    imrename (s1, "sclpupil2")
		    imexpr ("a*b", s1, "sclpupil2", s1//"n", verb-)
		    imdelete (s1//"n,sclpupil*.fits")
		}
		;
		
            }
        } else {
           # TODO: sendmsg
           printf("ERROR!\n")
        }
    } else {
        printf( "Pupil ghost removal not needed for %s according to CALOPS\n", cal )
    }
    
} else {
    printf( "Skipping pupil ghost removal for %s with image type %s\n", cal, obstype )
}

#TODO pupil amplitudes to PMAS

logout 1

