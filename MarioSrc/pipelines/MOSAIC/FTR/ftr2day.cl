#!/bin/env pipecl
#
# FTR2DAY -- Call the DAY pipeline with lists separated by CCD.
#
# This is a wrapper to lis2pipe to set up return lists for computing the
# global pupil ghost and fringe scales.

int	status = 2
string	dataset, datadir, lfile, ilist

# Tasks and packages.
task $patcmd = "$!ftr2dayR $1"
task $lis2pipe = "$!lis2pipe.cl datalist $3 day 1000 2>&1 > $1; echo $? > $2"

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
ilist = datadir // dataset // ".list"
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", "ftr2day", dataset) | tee (lfile)
time | tee (lfile)

# Generate patterns and loop over each pattern to create return lists
# for the scale calculations.
patcmd (ilist, > "ftr2day1.tmp")
list = "ftr2day1.tmp"
while (fscan (list, s1, s2) != EOF) {
    match (s1, ilist) | count | scan (i)
    if (i > 0) {
	print (dataset//"-day"//s2//".evl", >> dataset//".evl")
	if (pgr_scale == "global")
	    print (dataset//"-"//s2//".pgr", >> dataset//".pgr")
	;
	if (frg_scale == "global")
	    print (dataset//"-"//s2//".frg", >> dataset//".frg")
	;
    }
    ;
}
list =""; delete ("ftr2day1.tmp")

# Submit to the target pipeline.
if ( strstr( "F-", dataset ) > 0 )
    lis2pipe ("ftr2day1.tmp", "ftr2day2.tmp", "ftr2dayR")
else
    lis2pipe ("ftr2day1.tmp", "ftr2day2.tmp", "ftr2day")
concat ("ftr2day2.tmp") | scan (status)
if (access("ftr2day1.tmp"))
    concat ("ftr2day1.tmp")
;
delete ("ftr2day*.tmp")

if (status == 2) {
    if (pgr_scale == "global" && frg_scale == "global")
	status = 6
    else if (frg_scale == "global")
        status = 5
    else if (pgr_scale == "global")
        status = 4
    ;
}
;
logout (status)
