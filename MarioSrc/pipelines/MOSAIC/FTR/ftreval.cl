#!/bin/env pipecl
#
# FTREVAL -- Evalute sky coverage historgrams

string	dataset, datadir, ilist, lfile, html, flag
int	bin, sumbin

# Packages and tasks.
task $osf_update = "$!osf_update -a $3 -p day -f $1 -c 3 -s $2"
images
servers

findrule (dm, "ftrevalrule", validate=1) | scan (s1)
task ftrevalrule = (s1)

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = datadir // dataset // ".list"
lfile = datadir // names.lfile
html = names.shortname // "_fig.html"
cd (datadir)

delete ("ftreval*.tmp,ftrev[0-9]*")

# Log start of processing.
printf ("\n%s (%s): ", "ftreval", dataset) | tee (lfile)
time | tee (lfile)

flag = envget ("OSF_FLAG")

# If OSF_FLAG is the default '_' then the histograms are evaluated.
# Otherwise the value of "Y" or "N" define success or failure.

if (flag == "_") {

    # Combine histograms.
    bin = 0; sumbin = 0
    concat ("*-dayccd*.evl") | sort (num+, > "ftreval1.tmp")
    list = "ftreval1.tmp"
    while (fscan (list, x, j) != EOF) {
	i = int(x)
	if (i != bin) {
	    printf ("%3d %8d\n", bin, sumbin, >> "ftreval2.tmp")
	    sumbin = 0
	}
	;
	sumbin += j
	bin = i
    }
    printf ("%3d %8d\n", bin, sumbin, >> "ftreval2.tmp")
    list = ""; delete ("ftreval1.tmp")

    # Write to log and html files.
    printf ("\n# Combined Sky Pixel Histogram\n", >> lfile)
    concat ("ftreval2.tmp", >> lfile)

    if (access(html) == NO) {
	pathname (html, >> ilist)
	printf ('<HTML><HEAD><TITLE>Skies for %s</TITLE></HEAD></BODY>\n',
	    names.shortname, > html)
	printf ('<CENTER><H2>Sky Analysis for %s</H2></CENTER>\n',
	    names.shortname, >> html)
    }
    ;

    printf ('\n<H4> Overlap Statistics </H4>\n\n', >> html)
    printf ("<PRE>\n# Combined Sky Pixel Histogram\n", >> html)
    concat ("ftreval2.tmp", >> html)

    # Evaluate rule which also computes a cumulative file.
    ftrevalrule ("ftreval2.tmp", params=sft_overlap) | scan (b1)

    # Log result.
    printf ("\n# Percentile Cumulative Histogram\n", >> lfile)
    concat ("ftreval2.tmp", >> lfile)
    if (b1 == no) {
	sendmsg ("WARNING", "Bad sky coverage for fringe calibrations", dataset, "CAL")
	sendmsg ("WARNING", "Bad sky coverage for sky flat calibrations", dataset, "CAL")
	printf ("WARNING: Bad sky coverage for fringe day calibrations (%s)\n",
	    dataset, >> lfile)
	printf ("WARNING: Bad sky coverage for sky flat calibrations (%s)\n",
	    dataset, >> lfile)
    }
    ;

    printf ("\n# Percentile Cumulative Histogram\n", >> html)
    concat ("ftreval2.tmp", >> html)
    printf ('\n# Threshold parameters = "%s"\n</PRE>\n', sft_overlap, >> html)
    if (b1 == no) {
	printf ("<P><B>Bad sky coverage for fringe calibrations.</B>\n",
	    >> html)
	printf ("<P><B>Bad sky coverage for sky flat calibrations.</B>\n",
	    >> html)
    } else {
	printf ("<P><B>Sky coverage OK for fringe calibrations.</B>\n",
	    >> html)
	printf ("<P><B>Sky coverage OK for sky flat calibrations.</B>\n",
	    >> html)
    }

} else if (flag == "Y") {
    bin = 0
    b1 = YES
    printf ("<P><B>Ignore sky coverage.</B>\n", >> html)
} else {
    bin = 0
    b1 = NO
}

# Trigger day pipeline if successful, or not interactive, or forced.
if (b1 == YES || plinteract != "yes" || flag != "_" || bin < 2) {
#    if (b1 == NO) {
#	match ("MOSAIC_MEF?*_00", ilist, > "ftreval3.tmp")
#	match ("MOSAIC_MEF?*_sky", ilist, >> "ftreval3.tmp")
#	match ("MOSAIC_SIF?*-ccd[0-9]*.fits", ilist, >> "ftreval3.tmp")
#	match ("MOSAIC_SIF?*-ccd[0-9]*_obm.fits", ilist, > "ftreval4.tmp")
#	list = "ftreval4.tmp"
#	while (fscan (list, s1) != EOF)
#	    print (s1//"[pl]", >> "ftreval3.tmp")
#	list = "ftreval3.tmp"
#	while (fscan (list, s1) != EOF) {
#	    hedit (s1, "SFTFLAG",
#		"((SFTFLAG=='Y')?'N bad coverage':SFTFLAG)", verify-, show+)
#	    hedit (s1, "FRGFLAG",
#		"((FRGFLAG=='Y')?'N bad coverage':FRGFLAG)", verify-, show-)
#	    flpr
#	}
#	list = ""
#    }
#    ;
#
    files ("*-dayccd*.evl", > "ftreval1.tmp")
    list = "ftreval1.tmp"
    while (fscan (list, s1) != EOF) {
	s2 = substr (s1, 1, strstr(".evl",s1)-1)
	if (b1)
	    osf_update (s2, "Y", envget ("NHPPS_SYS_NAME"))
	else
	    osf_update (s2, "N", envget ("NHPPS_SYS_NAME"))
    }
    list = ""
}
;

delete ("ftreval*.tmp")

if (b1)
    logout 1
else if (plinteract != "yes" || flag != "_" || bin < 2)
    logout 2
else
    logout 3
