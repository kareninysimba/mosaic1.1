#!/bin/env pipecl
#
# FTRPATFIT -- Compute global pattern scales.

string	tpipe				# Target pipeline

string	module, dataset, datadir, lfile
real	scale, statwt, wt

# Packages and tasks.
task $osf_update = "$!osf_update -a $4 -c 6 -s _ -h $1 -p $2 -f $3"

if (fscan (args, tpipe) < 1)
    logout 0
;

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
module = envget ("NHPPS_MODULE_NAME")
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", module, dataset) | tee (lfile)
time | tee (lfile)

# Compute global scales.
match ("!", "*-ccd*."//tpipe, stop+, print-) | sort (> module//"1.tmp")
scale = 0; statwt = 0; wt = 1.
list = module // "1.tmp"
while (fscan (list, s1, x, y) != EOF) {
    if (s1 != s2 && statwt > 0) {
        scale /= statwt
	printf ("%s %.2f\n", s2, scale, >> module//"2.tmp")
	scale = 0; statwt = 0; wt = 1.
    }
    ;
    scale += x * y * wt
    statwt += y * wt
    s2 = s1
}
list = ""; delete (module//"1.tmp")
if (statwt > 0) {
    scale /= statwt
    printf ("%s %.2f\n", s2, scale, >> module//"2.tmp")
}
;

type (module//"2.tmp", >> lfile)

# Trigger results.
match ("!", "*-ccd*."//tpipe, print-, > module//"1.tmp")
list = module // "1.tmp"
while (fscan (list, s1) != EOF) {
    s2 = substr (s1, 1, stridx("!",s1)-1)
    s3 = substr (s1, 1, strldx("/",s1)-1)
    s3 = substr (s3, strldx("/",s3)+1, 1000)
    copy (module//"2.tmp", s1)
    osf_update (s2, tpipe, s3, envget ("NHPPS_SYS_NAME"))
}
list = ""

delete (module//"*.tmp")

logout 1
