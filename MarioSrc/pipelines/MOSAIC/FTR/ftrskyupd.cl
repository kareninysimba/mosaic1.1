#!/bin/env pipecl

# FTRSKYUPD -- Update sky flags.

int	status = 1
bool	update = yes
bool	show = no
int	figno = 1
struct	*fd

bool	sameref
int	n, ndet, ngroup
real	quality = -1.
string	s4, dataset, datadir, lfile, ilist, refskymap, stk, obm, fig, fign
real	hsigma, lsigma
real	mean = 1.
real	stddev = INDEF
real	stddev2 = INDEF

# Tasks and packages.
task $mkgraphic = "$!mkgraphic $1 $2 world png 1 'i1>0'"
images
proto
utilities
servers
noao
nproto
nfextern
ace
mscred
cache mscred, scombine
task mscskysub = "mscsrc$x_mscred.e"
task scmb = "mariosrc$scmb.cl"
task sftselect = "NHPPS_PIPESRC$/MOSAIC/SFT/sftselect.cl"
cache sftselect
task ftrmontage = "NHPPS_PIPESRC$/MOSAIC/FTR/ftrmontage.cl"
task ftrsigclip = "NHPPS_PIPESRC$/MOSAIC/FTR/ftrsigclip.cl"
cache ftrsigclip
findrule (dm, "enough_for_sflat", validate=1) | scan (line)
task enough_for_sflat = (line)

# Files and directories.
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
ilist = datadir // dataset // ".list"
fig = names.shortname// "_fig"
stk = names.shortname // "_skymap"
obm = names.shortname // "_skymap_obm"
mscred.logfile = ""
mscred.verbose = yes
mscred.instrument = "pipedata$mosaic.dat"
reset (pipedata=names.pipedata)
reset (uparm=names.uparm)
cd (datadir)

# Set flags.
if (access("ftrskyrej.tmp")) {
    touch ("ftrsky.tmp,ftrskyobm.tmp")
    list = "ftrskyrej.tmp"
    while (fscan (list, s1, line) != EOF) {
	s2 = substr (s1, 1, strstr("_sky",s1)-1)
        print (line, >> "ftrsky.tmp")
	match (s2//"_00", ilist, >> "ftrsky.tmp")
	match (s2//"_sky", ilist, >> "ftrsky.tmp")
	match (s2//"-ccd[0-9]*.fits", ilist, >> "ftrsky.tmp")
        print (line, >> "ftrskyobm.tmp")
	match (s2//"-ccd[0-9]*_obm.fits", ilist, >> "ftrskyobm.tmp")
    }
    list = ""

    list = "ftrskyobm.tmp"
    while (fscan (list, s1, line) != EOF) {
	if (s1 == "#")
	    printf ("%s %s\n", s1, line, >> "ftrsky.tmp")
	else
	    print (s1//"[pl]", >> "ftrsky.tmp")
    }
    list = ""

    list = "ftrsky.tmp"; s2 = ""
    while (fscan (list, s1, s3) != EOF) {
	if (s1 == "#") {
	    s2 = s3
	    next
	}
	;
	printf ("Update %s = %s\n", s1, s2)
	if (s2 == "Standard") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N standard deviation':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N standard deviation':FRGFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "PGRFLAG",
		"((PGRFLAG=='Y')?'N standard deviation':PGRFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Sky") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N sky mean':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N sky mean':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Magnitude") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N magnitude zeropoint':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N magnitude zeropoint':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "RMS") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N RMS':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N RMS':FRGFLAG)",
		show=show, verify-, update=update)
	} else if (s2 == "Source") {
	    hedit (s1, "SFTFLAG",
		"((SFTFLAG=='Y')?'N extended source light':SFTFLAG)",
		show=show, verify-, update=update)
	    hedit (s1, "FRGFLAG",
		"((FRGFLAG=='Y')?'N extended source light':FRGFLAG)",
		show=show, verify-, update=update)
	}
	;
	flpr
    }
    list = ""
    delete ("ftrskyrej.tmp,ftrsky.tmp,ftrskyobm.tmp")
}
;

logout 1
