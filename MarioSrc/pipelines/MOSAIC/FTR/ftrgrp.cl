#!/bin/env pipecl
# 
# FTRgrp
# 
# Description:
# 
#     This module sorts through a list of stacked-SIFs to create sublists
#     of dither suites.
# 
# Exit Status Values:
# 
#     1 = Successful
#     2 = No stacked-SIFs included in infile.
# 
# History:
# 
# T. Huard  200803--  Created.
# T. Huard  20080408  Changed the lists included within the list of lists
#     to contain two columns: filename, badpix mask.	These columns
#     contain the full pathnames.  A check has been added to be sure
#     the BPM value in the image headers is only the filename with no
#     path info.  If there is path info in BPM in the header, it is
#     revised to only include the filename.  Also, changed the exit
#     status value to 2 for the cases when there are no stacked-SIFs
#     in infile.  Finally, some improvements in code were made, based
#     on suggestions Frank had for gpsgrp.cl.  The code was made as
#     analogous to gpsgrp.cl as possible.
# T. Huard 20080409  Fixed seqlabel to include kp4m or ct4m, but not ccd*
#     part since this module groups files to call MDC, which combines
#     stacked SIFs (ccd1, ccd2, ..., ccd8).  Any "-" that occurs in
#     seqlabel (there shouldn't be any though!) are removed since they
#     may cause problems with the FTR/MDC return (see similar note in
#     gpsgrp.cl in GPS).  The naming convention for the stacked products
#     has been changed from mdsstack_* to *_sifstk*, and the matching
#     in this module has been changed accordingly.
# T. Huard 20080424  Moved to the FTR pipeline. 
# T. Huard 20080818  Revised to make single-column list for MDC instead
#     of double-column list.
# T. Huard 20080822  Following the retirement of MDS, which created
#     stacked SIFs that MDC would then combine into a composite image,
#     this procedure now generates lists of resampled SIFs instead of
#     stacked SIFs to give to MDCs.  A list of these lists is constructed
#     for the MDC call.  This procedure also now records the number of
#     sequences that include full complete sets (equal numbers of CCD1,
#     CCD2, ..., CCD8 resampled SIFs), and the number that do not.
#     Exit status values of 3 and 4 were removed.
# T. Huard 20080826  Renaming of sequence list files.
# Last Revised:  T. Huard  20080828  1:30pm

# Declare variables
int	nccd1, nccd2, nccd3, nccd4, nccd5, nccd6, nccd7, nccd8
string	dataset, indir, datadir, ilist, lfile
string	fname, mef, rspgrp
string  infile
struct	*fd

# Load packages
images
proto
utilities

# Set file and path names
names ("ftr", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir//dataset//".ftr"
lfile = datadir//names.lfile
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)
delete ("ftrgrp*.tmp")

# Log start of processing.
printf ("\nFTRgrp (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Input file is the FTR data list file after the return from DAY, where the 
# resampling is done by the RSP-SRS pipelines.  If this file does not exist,
# then exit since there are no resampled SIFs to group. (However, this file
# should always exist.)

infile = datadir//dataset//".list"
if (access(infile) == NO)
    logout 2
;

# Sort through infile to obtain a list of the resampled SIF filenames (full
# pathnames).  If there are no resampled SIFs, then the procedure exits.

match ("ccd?_r.fits", infile, >"ftrgrp2.tmp")
count ("ftrgrp2.tmp") | scan (i)
if (i == 0)
    logout 2
;

# Determine which sequence each resampled SIF belongs to (RSPGRP header
# keyword), and generate lists of filenames associated with each sequence.

list = "ftrgrp2.tmp"
while (fscan (list, fname) != EOF) {
    mef = substr (fname, strldx("/",fname)+1, strstr("-ccd",fname)-1)
    hselect (fname, "RSPGRP", yes) | scan (rspgrp)
    printf ("%s %s\n", fname, mef, >> "ftrgrp_"//rspgrp//".tmp")
    print ("ftrgrp_"//rspgrp//".tmp", >> "ftrgrp1.tmp")
}
list = ""; delete ("ftrgrp2.tmp")
sort ("ftrgrp1.tmp") | unique (>"ftrgrp1.tmp")

# Sort the list of grouped resampled SIFs by their short filenames (basenames).
# Skip those that don't have at least two exposures.
# Break up long sequences if desired (e.g. imcombine problems).

list = "ftrgrp1.tmp"
while (fscan (list, fname) != EOF) {
    fields (fname, "2") | sort | unique (>"ftrgrp2.tmp")
    sort (fname, col=2) | fields ("STDIN", "1", > fname)
    match ("ccd1_r.fits", fname) | count | scan (nccd1)
    match ("ccd2_r.fits", fname) | count | scan (nccd2)
    match ("ccd3_r.fits", fname) | count | scan (nccd3)
    match ("ccd4_r.fits", fname) | count | scan (nccd4)
    match ("ccd5_r.fits", fname) | count | scan (nccd5)
    match ("ccd6_r.fits", fname) | count | scan (nccd6)
    match ("ccd7_r.fits", fname) | count | scan (nccd7)
    match ("ccd8_r.fits", fname) | count | scan (nccd8)
    i = max(nccd1,nccd2,nccd3,nccd4,nccd5,nccd6,nccd7,nccd8)
    if (i < 2)
	next
    ;
    if (i <= mdc_maxstack) {
	pathname (fname, >> "ftrgrp.tmp")
        next
    }
    ;
    x = real(i) / (i / mdc_maxstack + 1)
    fd = "ftrgrp2.tmp"; j = 0
    for (i=1; fscan(fd,s1)!=EOF; i+=1) {
        if (i > int(j*x)) {
	    j = j + 1
	    printf ("ftrgrp%d_%s.tmp\n",
	        j,  substr (s1, strldx("-",s1)+1, 999)) | scan (s2)
	    pathname (s2, >> "ftrgrp.tmp")
	}
	;
	match (s1, fname, >> s2)
    }
    fd = ""; delete (fname)
}
list = ""; delete ("ftrgrp[12].tmp")

if (access ("ftrgrp.tmp")==NO)
    logout 2
;

logout 1
