#!/bin/env pipecl
#
# FTRCLEANUP -- Cleanup the FTR pipeline.

string	dataset, module, indir, datadir, lfile

# Tasks and packages.
task $osf_update = "$!osf_update -a $5 -p $1 -f $2 -m $3 -s $4"
task $psqlog = "$!psqlog"
utilities

# Files and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
module = envget ("NHPPS_MODULE_NAME")
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", module, dataset) | tee (lfile)
time | tee (lfile)

# Close the DRV pipeline.  Ignore any errors.
osf_update ("drv", dataset//"-drv", "drvdone", "P", envget ("NHPPS_SYS_NAME"), > "dev$null")

# Do this in dtmv to insure the data has been transfered.
## Log in PSQ.  This should be done through the data manager but this
## is a quick hack which assumes this module runs on the DM machine.
#
#psqlog (names.shortname, "completed")

logout 1
