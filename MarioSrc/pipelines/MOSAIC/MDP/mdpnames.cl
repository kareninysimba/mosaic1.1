# MDPNAMES -- Directory and filenames for the MDP pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure mdpnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	pipe			{prompt = "Pipeline name"}
string	shortname		{prompt = "Short name"}
file	rootdir			{prompt = "Root directory"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file	lfile			{prompt = "Log file"}
file	mef			{prompt = "MEF name"}
file	bpm			{prompt = "BPM name"}
file	rsp			{prompt = "Resampled image"}
file	rspbpm			{prompt = "BM for resampled image"}
file	mefpng			{prompt = "MEF png FITS file"}
file	mefpng1			{prompt = "MEF png"}
file	mefpng2			{prompt = "MEF preview png"}
file	rsppng			{prompt = "Resampled png FITS file"}
file	rsppng1			{prompt = "Resampled png"}
file	rsppng2			{prompt = "Resampled preview png"}
bool    base = no               {prompt = "Child part of dataset"}
string	pattern = ""		{prompt = "Pattern"}

begin
	# Set generic names.
	names (envget("NHPPS_SUBPIPE_NAME"), name, pattern=pattern, base=base)
	dataset = names.dataset
	pipe = names.pipe
	shortname = names.shortname
	rootdir = names.rootdir
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata
	parent = names.parent
	child = names.child
	lfile = names.lfile

	# Set pipeline specific names.
	mef = shortname
	bpm = mef // "_bpm"
	rsp = shortname // "_r"
	rspbpm = rsp // "_bpm"
	#mefpng = mef // "_png.fits"
	mefpng = mef // "_png"
	if (png_blk == 1)
	    mefpng1 = mef // ".png"
	else
	    mefpng1 = mef // "_x" // png_blk // ".png"
	mefpng2 = mef // "_" // png_pix // ".png"
	#rsppng = rsp // "_png.fits"
	rsppng = rsp // "_png"
	if (png_blk == 1)
	    rsppng1 = rsp // ".png"
	else
	    rsppng1 = rsp // "_x" // png_blk // ".png"
	rsppng2 = rsp // "_" // png_pix // ".png"

	if (pattern != "") {
	    mef += ".fits"
	    bpm += ".fits"
	    rsp += ".fits"
	    rspbpm += ".fits"
	}
end
