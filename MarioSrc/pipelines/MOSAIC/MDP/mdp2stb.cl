#!/bin/env pipecl
# 
# MDP2STB -- Send data products to STB.

string  dataset, datadir, lfile, node, path, fname
string	plqueue, plqname, plprocid, plfname, ptype1, ptype2
struct	instrument, mjd, proctype, prodtype, obstype

# Load tasks and packages.
task $ckarchive = "$!ssh pipeline@pipedmn plexec ckarchive"
task $dtsproc = "$!dtsproc $(1) 2> $(2)"
task $du = "$!du -B M $(1)"
task $rdtsproc = "$!ssh $3 plexec dtsproc $(1) 2> $(2)"
task $rdu = "$!ssh $2 du -B M $(1)"
task $psqpltable = "$!psqpltable"
images
tables
ttools
servers

# Set names and directories.
mdpnames (envget("OSF_DATASET"))
dataset = mdpnames.dataset
datadir = mdpnames.datadir
lfile = datadir // mdpnames.lfile
set (uparm = mdpnames.uparm)
cd (datadir)

# We override the PMAS address used by newdataproduct in order to record
# size information for the reports.
set (NHPPS_NODE_DM=envget("NHPPS_NODE_CM"))
set (NHPPS_PORT_DM=envget("NHPPS_PORT_CM"))

# Log start of module.
printf ("\n%s (%s): ", "mdp2stb", dataset) | tee (lfile)
time | tee (lfile)

# Setup hierarchy.
plqname = substr (dataset, 1, stridx("-",dataset)-1)
i = stridx ('_', plqname)
if (i > 0) {
    plqueue = substr (plqname, 1, i-1)
    plqname = substr (plqname, i+1, 1000)
    i = stridx ('_', plqname)
    if (i > 0) {
	plprocid = substr (plqname, i+1, 1000)
	plqname = substr (plqname, 1, i-1)
    } else 
        plprocid = 'TEST'
} else {
    plqueue = 'TEST'
    plprocid = 'TEST'
}

# Send files.
if (access(dataset//".dpslist")==NO) {
concat ("*.dps", > dataset//".dpslist"); delete ("*.dps")
}
;
list = dataset//".dpslist"
while (fscan (list, s1) != EOF) {
    i = stridx ("!", s1)
    path = substr (s1, i+1, 1000)
    if (i > 0) {
        node = substr (s1, 1, i-1)
	i = stridx (".", node)
	if (i > 0)
	    node = substr (node, 1, i-1)
	;
    } else
	node = ""

    fname = substr (path, strldx('/',path)+1, 1000)
    i = strlstr (".fits", fname)
    if (i > 0)
        fname = substr (fname, 1, i-1)
    ;
    plfname = substr (fname, stridx('-',fname)+1, 1000)

    # Set STB keywords.  For string format.
    nhedit (s1//"[0]", "PLQUEUE", "dummy", "PL Queue", add+)
    nhedit (s1//"[0]", "PLQNAME", "dummy", "PL Dataset", add+)
    nhedit (s1//"[0]", "PLPROCID", "dummy", "PL Processing ID", add+)
    nhedit (s1//"[0]", "PLFNAME", "dummy", "PL Filename", add+)
    nhedit (s1//"[0]", "PLQUEUE", plqueue, ".")
    nhedit (s1//"[0]", "PLQNAME", plqname, ".")
    nhedit (s1//"[0]", "PLPROCID", plprocid, ".")
    nhedit (s1//"[0]", "PLFNAME", plfname, ".")

    # Set the filename keyword.
    hselect (s1//"[0]", "$FILENAME", yes) | scan (s2)
    hselect (s1//"[0]", "$PROCTYPE", yes) | scan (proctype)
    hselect (s1//"[0]", "$PRODTYPE", yes) | scan (prodtype)
    if (s2 != "INDEF") {
	nhedit (s1//"[0]", "RAWFILE", s2, "Original raw file", add+,
	    before="FILENAME")

	i = strlstr (".fits", s2)
	if (i > 0)
	    s2 = substr (s2, 1, i-1)
	;
	ptype1 = proctype; ptype2 = prodtype
        if (ptype1 == "Resampled")
	    ptype1 = "_r"
	else if (ptype1 == "Stacked")
	    ptype1 = "_stk"
	else
	    ptype1 = ""
	if (ptype2 == "dqmask")
	    ptype2 = "_bpm"
	else if (ptype2 == "expmap")
	    ptype2 = "_expmap"
	else if (ptype2 == "png")
	    ptype2 = "_png"
	else
	    ptype2 = ""
	if (ptype1 == "")
	    ptype1 = "_pl"
	;
	nhedit (s1//"[0]", "PLOFNAME", s2//ptype1//ptype2,
	    "Original file name", add+)
    }
    ;
    #nhedit (s1//"[0]", "FILENAME", fname, "Current filename", add+)
    nhedit (s1//"[0]", "FILENAME", plqueue//"_"//plqname//"-"//plfname,
        "Current filename", add+)

    nhedit (s1//"[0]", "add_blank", "", "", add+, before="PLQUEUE")
    #nhedit (s1//"[0]", "add_blank", " # PL information",
    #    "", add+, before="PLQUEUE")

    if (dps_archive=="no" ||
        (dps_archive != "yes" && dps_archive != "dummy")  ||
	envget("OSF_FLAG")=="N") {
	if (node == "")
	    du (path) | scan (x)
	else
	    rdu (path, node) | scan (x)
	newdataproduct (fname, class="STBOutput", size=x)
        next
    }
    ;

    # Check for previously archived version.
    hselect (s1//"[0]", "$INSTRUME", yes) | scan (instrument)
    hselect (s1//"[0]", "$MJD-OBS", yes) | scan (mjd)
    hselect (s1//"[0]", "$OBSTYPE", yes) | scan (obstype)
    if (obstype == "dome flat")
        obstype = "dome%flat"
    ;
    printf ("''%s' '%s' '%s' '%s' '%s''\n",
        instrument, mjd, obstype, proctype, prodtype) | scan (line)
    s2 = ""; ckarchive (line) | scan (s2)
    if (s2 != "") {
	sendmsg ("WARN", "Earlier version in archive", s2, "DTS")
        next
    }
    ;

    # Send to STB.
    printf ("  Sending %s to DTS:\n", s1)
    if (node == "")
	dtsproc (path, "mdp2stb.tmp")
    else
	rdtsproc (path, "mdp2stb.tmp", node)

    # Check status and record dataproduct sent.
    count ("mdp2stb.tmp") | scan (i)
    if (i == 0) {
	printf ("  %s sent to DTS\n", s1) | tee (lfile)
	printf ("    PLQUEUE = %s\n", plqueue) | tee (lfile)
	printf ("    PLQNAME = %s\n", plqname) | tee (lfile)
	printf ("    PLPROCID = %s\n", plprocid) | tee (lfile)
	printf ("    PLFNAME = %s\n", plfname) | tee (lfile)

	psqpltable (mdpnames.shortname, "archived")

	if (node == "")
	    du (path) | scan (x)
	else
	    rdu (path, node) | scan (x)
	newdataproduct (s1, class="STBOutput", size=x)

	if (dts_ftp == "no" && dts_save == "") {
	    if (strstr("_png",s1) == 0)
		delete (s1)
	    ;
	}
	;

    } else {
	concat ("mdp2stb.tmp")
	sendmsg ("ERROR", "DTS failed", s1, "DTS")
    }
    delete ("mdp2stb.tmp")
}
list = ""

logout 1
