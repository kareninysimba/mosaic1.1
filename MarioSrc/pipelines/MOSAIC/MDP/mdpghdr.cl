# MDPHDR -- Set header for data product.

procedure mdpghdr (image)

string	image			{prompt="Image header to edit"}

begin
	struct	obstype
	int	status = 1
	int	ntot
	bool	success
	real	x1, x2, x3, x4, x5, x6, see1, see2
	real	racen, deccen
	string  img, img0, caldir, s1

	img = image
	img0 = image//"[0]"
	caldir = "MC$"

	s1 = substr( image, strlstr("/",image)+1, 1000 )

	# If the seeing keyword is present already, it is the Tololo
	# DIMM seeing measurement
        hselect( img0, "seeing", yes ) | scan( line )
	if ( nscan()==1 ) {
	    hedit( img0, "seeingd", line, add+, ver- )
	    hedit( img0, "seeing", delete+, ver- )
	}
	;

	# If the dqsemef keyword is present, copy it to a keyword
	# name "seeing"
        see1 = INDEF
	hselect( img0, "dqsemef", yes ) | scan( see1 )
	success = no
	if (nscan()==1) {
	    # Get the approximate pixel size as well. Initially it was tried
	    # to do this through the pixscalN keywords, but those have been
	    # deleted by mdphdr.cl. So use WCS keywords, which is better 
	    # anyway. Note that the WCS solution is only available in the
	    # subsets.
	    hselect( img//"[1]", "cd1_1,cd1_2,cd2_1,cd2_2", yes ) | scan( x1, x2, x3, x4 )
	    if (nscan()==4) {
	        x5 = sqrt( x1*x1+x3*x3 )
	        x6 = sqrt( x2*x2+x4*x4 )
		see2 = 3600*see1*(x5+x6)/2
	        success = yes
	    }
	    ;
	}
	;
	if (!success) {
	    hselect( img0, "rspscale", yes ) | scan( x1 )
	    if (nscan()==1) {
	        see2 = see1*x1
		success = yes
	    }
	    ;
	}
	;
	hselect( img0, "obstype", yes ) | scan( obstype )
	if (obstype == "dome flat")
	    obstype = "flat"
	else if (obstype == "sky flat")
	    obstype = cal_skyflat
	;
	if ((obstype!="zero")&&(obstype!="flat")) {
	    hedit( img0, "seeingp", see1, add+, ver- )
	    if (success) {
		hedit( img0, "seeing", see2, add+, ver- )
	    } else {
	        sendmsg ("WARNING", "pixel scale could not be read from header", s1, "CAL")
		hedit( img0, "seeing", INDEF, add+, ver- )
	    }
	} else {
	    hedit( img0, "seeing", INDEF, add+, ver- )
	    hedit( img0, "seeingp", INDEF, add+, ver- )
	}

	# Set the uncertainty in the wcs as determined by ccmap inmefwcs.cl
	hselect( img0, "dqwccaxr,dqwccayr", yes ) | scan( x1, x2 )
	if (nscan()==2) {
	    hedit( img0, "wcsrmsx", x1, add+, ver- )
	    hedit( img0, "wcsrmsy", x2, add+, ver- )
	} else {
	    printf("Could not read dqwccaxr and dqwccayr from header\n")
	}

        # Get the RA and Dec of the center of the exposure, and the four corners.
	if (access("mdphdr_allwcs.tmp")) {
	    # The center coordinates are simply the average of the corners
	    racen = INDEF
	    deccen = INDEF
	    sort( "mdphdr_allwcs.tmp", column=1, ignore=yes, numeric=yes, reverse=no, > "mdpghdr.tmp" )
	    head( "mdpghdr.tmp", nlines=1 ) | scan( x1, x2 )
	    ntot = nscan()
	    if (ntot==2) {
	        hedit( img0, "gcor1ra", x1, add+, ver- )
	        hedit( img0, "gcor1dec", x2, add+, ver- )
	        racen = x1
	    }
	    ;
	    delete( "mdpghdr.tmp" )
	    sort( "mdphdr_allwcs.tmp", column=1, ignore=yes, numeric=yes, reverse=yes, > "mdpghdr.tmp" )
	    head( "mdpghdr.tmp", nlines=1 ) | scan( x1, x2 )
	    ntot=ntot+nscan()
	    if (ntot==4) {
	        hedit( img0, "gcor3ra", x1, add+, ver- )
	        hedit( img0, "gcor3dec", x2, add+, ver- )
	        racen = (racen+x1)/2
	    }
	    ;
	    delete( "mdpghdr.tmp" )
	    sort( "mdphdr_allwcs.tmp", column=2, ignore=yes, numeric=yes, reverse=no, > "mdpghdr.tmp" )
	    head( "mdpghdr.tmp", nlines=1 ) | scan( x1, x2 )
	    ntot=ntot+nscan()
	    if (ntot==6) {
	        hedit( img0, "gcor2ra", x1, add+, ver- )
	        hedit( img0, "gcor2dec", x2, add+, ver- )
	        deccen = x2
	    }
	    ;
	    delete( "mdpghdr.tmp" )
	    sort( "mdphdr_allwcs.tmp", column=2, ignore=yes, numeric=yes, reverse=yes, > "mdpghdr.tmp" )
	    head( "mdpghdr.tmp", nlines=1 ) | scan( x1, x2 )
	    ntot=ntot+nscan()
	    if (ntot==8) {
	        hedit( img0, "gcor4ra", x1, add+, ver- )
	        hedit( img0, "gcor4dec", x2, add+, ver- )
	        deccen = (deccen+x2)/2

	        hedit( img0, "gcenra", racen, add+, ver- )
	        hedit( img0, "gcendec", deccen, add+, ver- )

	        printf("%.2H\n", racen) | scan( s1 )
	        hedit( img0, "ra", s1, update+, ver- )
	        printf("%.1h\n", deccen) | scan( s1 )
	        hedit( img0, "dec", s1, update+, ver- )
	    }
	    ;
	    delete( "mdpghdr.tmp" )
	}

end


