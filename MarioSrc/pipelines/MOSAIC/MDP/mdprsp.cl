#!/bin/env pipecl
# 
# MDPRSP -- Make resampled files.
#
# This routine makes the resampled image, a bad pixel mask for the image,
# a block averaged png, and a thumbnail.  The png's are
# converted to FITS foreign extensions.
#
# It may be called with a flag that causes a resampled image to be created
# from MEF pipeline version.

string  dataset, datadir, ifile, lfile, s4, mcat
real	ra, dec, seeing, cd1, cd2, cd3, cd4, scale
struct	proctype, obstype

if (day_dorsp == NO)
    logout 1
;

# Load tasks and packages
task $fpack = "$!fpack -D -Y -q 4 -v $(1)"
task $mkgraphic = "$!mkgraphic"
task $mkgraphico = "$!mkgraphico"
task $convert = "$!convert"
task $plver = "$!plver"
task mdphdr = "NHPPS_PIPESRC$/MOSAIC/MDP/mdphdr.cl"
task mdprwcs = "NHPPS_PIPESRC$/MOSAIC/MDP/mdprwcs.cl"
redefine mscred = mscred$mscred.cl
fitsutil
images
servers
proto
utilities
noao
nproto
artdata
mscred
cache mscred

# Set names and directories.
mdpnames (envget ("OSF_DATASET"))
dataset = mdpnames.dataset
datadir = mdpnames.datadir
ifile = mdpnames.datadir // dataset // ".mdp"
lfile = datadir // mdpnames.lfile
mscred.logfile = lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = mdpnames.uparm)
set (pipedata = mdpnames.pipedata)
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

delete ("mdprsp*tmp", >& "dev$null")
delete (mdpnames.rsp//".fits", >& "dev$null")
delete (mdpnames.rspbpm//".fits", >& "dev$null")
delete (mdpnames.rsppng, >& "dev$null")
delete ("*_r_bpm.pl", > "dev$null")

# Collect the pieces.
touch ("mdprsp1.tmp")
if (envget("OSF_FLAG") == "_") {
    match ("STR?*_r.fits", ifile) | sort | uniq (> "mdprsp1.tmp")
    count ("mdprsp1.tmp") | scan (i)
    if (i == 0)
	match ("SRS?*_r.fits", ifile) | sort | uniq (> "mdprsp1.tmp")
    ;
} else if (imaccess(mdpnames.rsp)) {
    # Set scale to account for binning. Round to nearest quarter arcsec.
    #scale = rsp_scale
    hselect (mdpnames.rsp, "CD*", yes) | scan (cd1, cd2, cd3, cd4)
    scale = sqrt ((cd1**2 + cd2**2 + cd3**2 + cd4**2) / 2.) * 3600.
    scale = nint (scale / 0.25) * 0.25

    hselect (mdpnames.rsp, "crval1,crval2", yes) | scan (ra, dec); ra /= 15
    match ("ccd[0-9]*.fits", ifile, > "mdprsp2.tmp")
    list = "mdprsp2.tmp"
    for (i=1; fscan(list,s1)!=EOF; i+=1) {
	s2 = "mdprsp" // i // "_r"
	mscimage (s1, s2, pixmask-, verb+, wcssource="parameters",
	    ra=ra, dec=dec, scale=scale, rotation=0,
	    interpolant=rsp_interp, boundary="constant", constant=0) |
	    tee (lfile)
	print (s2, >> "mdprsp1.tmp")
    }
    list = ""; delete ("mdprsp2.tmp")
    imdelete (mdpnames.rsp)
    imdelete (mdpnames.rspbpm)
    imdelete (mdpnames.rsppng)
} else
    i = 0
	    
count ("mdprsp1.tmp") | scan (i)
if (i == 0) {
    delete ("mdprsp1.tmp")
    sendmsg ("WARNING", "No data found", "", "VRFY")
    logout 1
}
;

# Get the masks.  These must be in the working directory for imcombine but may
# then be deleted.
match ("STR?*_r_bpm.pl", ifile) | sort | uniq (> "mdprsp2.tmp")
count ("mdprsp2.tmp") | scan (i)
if (i == 0)
    match ("SRS?*_r_bpm.pl", ifile) | sort | uniq (> "mdprsp2.tmp")
;
copy ("@mdprsp2.tmp", ".")
files ("*_r_bpm.pl", > "mdprsp2.tmp")

# Make the global header.
mkglbhdr ("@mdprsp1.tmp", "mdprsp.fits", exclude="@pipedata$mkglbhdr.exclude")
head ("mdprsp1.tmp") | scan (s1)
tail ("mdprsp1.tmp", nl=-1, >> "mdprsp3.tmp")
imcopy (s1, "mdprsp.fits[dummy,append,inherit]", verbose-)

# Make the resampled image.
imcombine ("mdprsp.fits[1],@mdprsp3.tmp", mdpnames.rsp, logfile=lfile,
     imcmb="", offsets="wcs", masktype="novalue", maskvalue=2, blank=0.)
imdelete ("mdprsp.fits")

# Fix the header.
proctype = "Resampled"; obstype = "object"
mdphdr (mdpnames.rsp, proctype, obstype, "")
mdprwcs (mdpnames.rsp)
nhedit (mdpnames.rsp, "DQMASK", mdpnames.rspbpm//"[pl]",
    "Data quality mask", add+)
nhedit (mdpnames.rsp, comfile="pipedata$mdprsp.nhedit")

# Make the bad pixel mask.
mkglbhdr (mdpnames.rsp, mdpnames.rspbpm)
imcombine ("@mdprsp2.tmp", "mdprsp3.pl", logfile=lfile, offsets="wcs",
    imcmb="", blank=2)
nhedit ("mdprsp3", "*", del+)
nhedit (mdpnames.rspbpm, "OBJECT", "Mask for "//mdpnames.rsp, ".", add+)
nhedit (mdpnames.rspbpm, "PRODTYPE", "dqmask", ".", add+)
nhedit (mdpnames.rspbpm, "DQMFOR", mdpnames.rsp,
    "Data for which this mask applies", add+)
imcopy ("mdprsp3", mdpnames.rspbpm//"[pl,type=mask,append]", verbose-)
imdelete ("@mdprsp2.tmp")

# Convert to short if needed.
if (mdp_doshort)
    toshort (mdpnames.rsp, mdpnames.rsp, inmasks="!DQMASK", sigma="!SIGMEAN",
        maxbscale=mdp_maxbscale, docopy-, logfile=lfile)
;

# Make the graphic files.
hselect (mdpnames.rsp, "$SKYMEAN", yes) | scan (x)
if (isindef(x) || x < 0)
    mkgraphic (mdpnames.rsp, mdpnames.rsppng1, "none", "png", png_blk,
        "'i1!=0'")
else
    mkgraphic (mdpnames.rsp, mdpnames.rsppng1, "none", "png", png_blk,
        "'i1>0'")
imdelete ("mdprsp3")
convert ("-resize "//png_pix, mdpnames.rsppng1, mdpnames.rsppng2)
#Encapsulate pngs into fits:
#fgwrite (mdpnames.rsppng1//" "//mdpnames.rsppng2,
printf ("fgwrite encapsulates large png %s\n", mdpnames.rsppng1)
printf ("fgwrite encapsulates small png %s\n", mdpnames.rsppng2)
fgwrite (mdpnames.rsppng1, "mdprsp_png1.fits", group="pipeline", checksum+)
fgwrite (mdpnames.rsppng2, "mdprsp_png2.fits", group="pipeline", checksum+)

# Edit encapsulated large png header:
#mkglbhdr (mdpnames.rsp, mdpnames.rsppng)
mkglbhdr (mdpnames.rsp, mdpnames.rsppng//"1")
#nhedit (mdpnames.rsppng//"[0]", "PRODTYPE", "png", ".", add+)
#nhedit (mdpnames.rsppng//"[0]", "MIMETYPE", "image/png", ".", add+)
nhedit (mdpnames.rsppng//"1[0]", "PRODTYPE", "png1", ".", add+)
nhedit (mdpnames.rsppng//"1[0]", "MIMETYPE", "image/png", ".", add+)
#fxcopy ("mdprsp_png.fits", mdpnames.rsppng, groups="1,2", new_file-)
fxcopy ("mdprsp_png1.fits", mdpnames.rsppng//"1.fits", groups="1", new_file-)

# Edit encapsulated small png header:
#mkglbhdr (mdpnames.rsp, mdpnames.rsppng)
mkglbhdr (mdpnames.rsp, mdpnames.rsppng//"2")
#nhedit (mdpnames.rsppng//"[0]", "PRODTYPE", "png", ".", add+)
#nhedit (mdpnames.rsppng//"[0]", "MIMETYPE", "image/png", ".", add+)
nhedit (mdpnames.rsppng//"2[0]", "PRODTYPE", "png2", ".", add+)
nhedit (mdpnames.rsppng//"2[0]", "MIMETYPE", "image/png", ".", add+)
#fxcopy ("mdprsp_png.fits", mdpnames.rsppng, groups="1,2", new_file-)
fxcopy ("mdprsp_png2.fits", mdpnames.rsppng//"2.fits", groups="1", new_file-)

#logout 0

imdelete ("mdprsp_png.fits")
imdelete ("mdprsp_png1.fits")
imdelete ("mdprsp_png2.fits")
delete (mdpnames.rsppng1)
delete (mdpnames.rsppng2)

# Clean up
delete ("mdprsp*tmp")

# Set whether to review the results.
if (dps_review)
    logout 2
;

if (access(mdpnames.rsp//".fits")) {
    rename (mdpnames.rsp//".fits", "mdprsp.fits")
    mkglbhdr ("mdprsp.fits", mdpnames.rsp, exclude="")
    imcopy ("mdprsp.fits", mdpnames.rsp//"[resampled,append,inherit]", verbose-)
    imdelete ("mdprsp.fits")
    fpack (mdpnames.rsp//".fits")
    print (mdpnames.rsp//".fits.fz", > "mdprsp.dps")
}
;
if (access(mdpnames.rspbpm//".fits"))
    print (mdpnames.rspbpm//".fits", >> "mdprsp.dps")
;
#if (access(mdpnames.rsppng))
#    print (mdpnames.rsppng, >> "mdprsp.dps")
#;

if (access(mdpnames.rsppng//"1.fits"))
    print (mdpnames.rsppng//"1.fits", >> "mdprsp.dps")
;
if (access(mdpnames.rsppng//"2.fits"))
    print (mdpnames.rsppng//"2.fits", >> "mdprsp.dps")
;
logout 1
