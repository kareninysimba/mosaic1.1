#!/bin/env pipecl
#
# PGRMKPGR -- Make a pupil ghost template.
#
# This makes the pupil ghost template

int	rulepassed
string	dataset, datadir, ifile, lfile
string	filter, rulepath, cast
real	zmin, zmax, mjd

# Packages and task.
images
proto
utilities
servers
dataqual
mscred
cache mscred
task scmb = "mariosrc$scmb.cl"
task sftselect = "NHPPS_PIPESRC$/MOSAIC/SFT/sftselect.cl"
cache sftselect
task mscextensions = "mscsrc$x_mscred.e"
task pupilfit = "mscsrc$x_mscred.e"
cache mscextensions

# Define rules
findrule( dm, "enough_for_pupilghost", validate=1 ) | scan( rulepath )
task enough_for_pupilghost = (rulepath)

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Set paths and files.
pgrnames (envget ("OSF_DATASET"))

# Note that the use of the ???names script forces the use of data set
# names that follow the pipeline naming conventions. For example,
# the PGR pipeline expects ArbitraryPrefix-ftrI-ngt-mefObsId-sifim?.fits
# (although the specific pipeline names [ngt, mef] are not important)
# pgrnames is loaded as part of the mario package (see
# $MarioSrc/src/mario/mario.cl)

dataset = pgrnames.dataset
datadir = pgrnames.datadir
lfile = datadir // pgrnames.lfile
ifile = pgrnames.indir // dataset // ".pgr"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = pgrnames.uparm)
set (pipedata = pgrnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nPGRMKPGR (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Select images and scale factors.
sftselect ("PGRFLAG", ifile, "pgrmkpgr1.tmp", "pgrmkpgr2.tmp", cksftflag+)

# Check the rule.
if (sftselect.nselect > 0 && sftselect.filter == "default")
    sendmsg ("WARNING", "FILTER not found, using default rule", "", "VRFY")
;
enough_for_pupilghost (sftselect.filter, sftselect.nselect) | scan (i)
if (sftselect.nforced > 0  && sftselect.nselect > 0)
    rulepassed = 1
else
    rulepassed = i

if (rulepassed == 0) {
    sendmsg ("WARNING", "Not enough images to make pupil template",
        str(sftselect.nselect), "DQ")
    printf ("WARNING: Not enough images to make pupil template (%d).\n",
        sftselect.nselect) | tee (lfile)
    sftselect.nselect = 0
}
;

# This is a pain but we need to do this outside an if block because of a bug.
enough_for_pupilghost (sftselect.filter, sftselect.nselect) | scan (i)
if (rulepassed == 0 && i == 0) {
    delete ("pgrmkpgr*.tmp")
    logout 2
}
;

# Run scombine with the object masks to create a frame with the
# pupil ghost but any objects filtered out.  If the number is large do it in
# blocks.
scmb ("pgrmkpgr1.tmp", pgrnames.sflat, pgrnames.bpm, "pgrmkpgr2.tmp",
    "pgrmkpgra", imcmb="IMCMBMEF")
hedit (pgrnames.sflat, "IMCMBMEF,IMCMBSIF", del+, ver-, show-)

# Clean up
delete ("pgrmkpgr*")

# Check for success.
if (imaccess (pgrnames.sflat) == NO) {
    sendmsg ("WARNING", "Raw pupil ghost pattern not created", pgrnames.sflat, "PROC")
    printf ("WARNING: No raw pupil ghost pattern created (%s)\n", dataset) |
    logout 3
}
;

# Construct the pupil ghost template and mask.
pupilfit (pgrnames.sflat, pgrnames.pgr, type="data")

# Check for success.
if (imaccess (pgrnames.pgr) == NO) {
    sendmsg ("WARNING", "Pupil ghost pattern not created", pgrnames.pgr, "PROC")
    printf ("WARNING: No pupil ghost pattern created (%s)\n", dataset) |
    tee (lfile)
    logout 3
}
;

# Sigma clip the template to avoid bad data.  The pattern mask is updated
# to remove the clipped pixels.  However, since when we use library templates
# there is no mask stored we also replace the clipped pixels by the thresholds.

imrename (pgrnames.pgr, "pgrmkpgr.fits")
imrename (pgrnames.pgm, "pgrmkpgr.pl")
mimstat ("pgrmkpgr.fits", imask="pgrmkpgr.pl", omask=pgrnames.pgm,
    nclip=3, lsig=3, usig=5, fields="min,max", format-) | scan (x, y)
printf ("Clipped data between %.3g and %.3g\n", x, y)
imexpr ("max(b,min(c,a))", pgrnames.pgr, "pgrmkpgr.fits", x, y, outtype="real",
    verbose-)
imdelete ("pgrmkpgr.fits,pgrmkpgr.pl")

# Add mask to pupil ghost template header.
hedit (pgrnames.pgr, "BPM", pgrnames.pgm, add+)

# Set identifying keywords
hedit( pgrnames.pgr, "PROCTYPE", "MasterCal", add+, ver- )
hedit( pgrnames.pgr, "OBSTYPE", "pupil", add+, ver- )

# Enter new dataproduct.
hsel( pgrnames.pgr, "mjd-obs", yes ) | scan( mjd )
newDataProduct(pgrnames.pgr, mjd, "pupilghostimage", "")
storekeywords( class="pupilghostimage", id=pgrnames.pgr, sid=pgrnames.pgr, dm=dm )

# Add number of contributing exposures to header and PMAS
hedit( pgrnames.pgr, "DQPGNUSD", sftselect.nselect, add+, verify-, show+,
    update+, >> lfile )
printf("%d\n", sftselect.nselect) | scan( cast )
setkeyval( class="pupilghostimage", id=pgrnames.pgr, dm=dm, keyword="dqpgnusd",
    value=cast )

# Add quality value.
if (rulepassed == 0)
    hedit (pgrnames.pgr, "QUALITY", -1, add+, ver-)
else
    hedit (pgrnames.pgr, "QUALITY", 0, add+, ver-)

logout 1
