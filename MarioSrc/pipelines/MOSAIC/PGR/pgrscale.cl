#!/bin/env pipecl
#
# PGRSCALE -- Compute scale factors.

int	status = 1
real	quality = -1
string	dataset, datadir, ifile, sfile, lfile, ftrname, pgr
real	scale, statwt
file	caldir = "MC$"

# Packages and task.
task $pipeselect = "$!pipeselect $1 $2 0 0"
task $osf_test = "$!osf_test -a $2 -p ftr -f $1"
images
servers
mscred
task patfit = "mscsrc$x_mscred.e"
cache mscred

# Set paths and files.
pgrnames (envget("OSF_DATASET"))
daynames (pgrnames.parent)
ftrname = daynames.parent
dataset = pgrnames.dataset
datadir = pgrnames.datadir
ifile = pgrnames.indir // dataset // ".pgr"
sfile = dataset // ".scl"
lfile = datadir // pgrnames.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = pgrnames.uparm)
cd (datadir)

# Log start of processing.
printf ("\nPGRSCALE (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean-up
delete ("*.scl,*.tmp")

# Determine if calibration exists and its quality.
if (imaccess(pgrnames.pgr))
    hselect (pgrnames.pgr, "quality", yes) | scan (quality)
;

# Compute scale factors.
scale = 1.; statwt = 1.
if (imaccess(pgrnames.pgm)) {
    list = ifile
    while (fscan (list, s1) != EOF) {
	# Set pupil ghost template.
	if (quality >= 0.)
	    pgr = pgrnames.pgr
	else
	    pgr = ""
	hselect (s1, "PUPIL", yes) | scan (pgr)
	if (imaccess(pgr) == NO) {
	    getcal (s1, "pupil", cm, caldir, obstype="",
	        detector=cl.instrument, imageid="!ccdname", filter="!filter",
		exptime="", mjd="!mjd-obs", dmjd=10*cal_dmjd,
		quality=0., match="!ccdsum", > "dev$null")
	    if (getcal.statcode != 0)
		getcal (s1, "pupil", cm, caldir, obstype="",
		    detector=cl.instrument, imageid="!ccdname", filter="",
		    exptime="", mjd="!mjd-obs", dmjd=10*cal_dmjd,
		    quality=0., match="!ccdsum", > "dev$null")
	    ;
	    if (getcal.statcode == 0)
	        hselect (s1, "PUPIL", yes) | scan (pgr)
	    else
	        sendmsg ("WARNING", "No library pupil template to apply",
		    substr(s1,strldx("/",s1)+1,1000), "CAL")
	}
	if (imaccess(pgr) == NO)
	    next
	;

	if (pgr_scale != "none") {
	    #patfit (s1, "", pgr, masks="!objmask",
	#	patmasks=pgrnames.pgm, background="!skymean", bkgpattern="",
	#	bkgweight="", outtype="none", ncblk=5, nlblk=5,
	#	logname="PGRSCALE", logfile=lfile, verbose+, > "pgrscale.tmp")
	    patfit (s1, "", pgr, masks="!objmask",
		patmasks=pgrnames.pgm, background="", bkgpattern="",
		bkgweight="", outtype="none", ncblk=5, nlblk=5,
		logname="PGRSCALE", logfile=lfile, verbose+, > "pgrscale.tmp")

	    match ("scale", "pgrscale.tmp") | scanf ("  scale = %g", scale)
	    match ("statwt", "pgrscale.tmp") | scanf ("  statwt = %g", statwt)
	    delete ("pgrscale.tmp")

	    # Place reality checks on the scale.
	    if (scale < 0.)
	        scale = 0.
	    ;
#	    if (scale < 0.5 || scale > 2.5) {
#		scale = max (0., min (10., scale))
#		statwt = statwt / 4.
#	    }
#	    ;
	}
	;

	s2 = substr (s1, strldx("/",s1)+1, 1000)
	s2 = substr (s2, 1, strldx("-",s2)-1)
	s2 = substr (s2, strldx("-",s2)+1, 1000)
	printf ("%s %.2f %.4g\n", s2, scale, statwt, >> sfile)
    }
    list = ""
}
;

if (pgr_scale != "global")
    logout 1
;

# Trigger ftr pipeline to compute global scales.

# Find pipeline.
pipeselect (envget ("NHPPS_SYS_NAME"), "ftr", > "pgrscale.tmp")
count ("pgrscale.tmp") | scan (i)
s1 = ""
if (i == 1)
    head ("pgrscale.tmp") | scan (s1)
else if (i > 1) {
    osf_test (ftrname, envget ("NHPPS_SYS_NAME")) | scan (s2)
    s2 = substr (s2, strldx(":",s2)+1, 1000)
    if (stridx("_",s2) > 0)
	s2 = substr (s2, 1, strldx("_",s2)-1)
    match (s2, "pgrscale.tmp") | scan (s1)
}
;
delete ("pgrscale.tmp")

# Trigger pipeline.
if (s1 != "") {
    s2 = s1 // ftrname // "-" // daynames.child // ".pgr"
    if (access(sfile)) {
	pathname (sfile, >> sfile)
	copy (sfile, s2)
	delete (sfile)
	status = 2
    } else {
        touch (s2)
	status = 1
    }
    touch (s2//"trig")
} else
    status = 0

logout (status)
