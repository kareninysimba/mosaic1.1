#!/bin/env pipecl
#
# DRVSETUP -- Setup the DRV pipeline.
# 
# The input is a list of DAY datasets.

string	dataset, indir, datadir, ifile, lfile, refim, calops
int	founddir, status

# Packages and tasks.

# Files and directories.
names ("drv", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ifile = indir // dataset // ".drv"
lfile = datadir // names.lfile

# Wait for data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nDAYSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup files.
list = ifile
while (fscan (list, s1) != EOF) {
    print (s1//".pgr", >> dataset//".pgr")
    print (s1//".frg", >> dataset//".frg")
    print (s1//".sft", >> dataset//".sft")
}
list = ""

# Delete return file.
delete (indir//"return/"//dataset //".drv")

logout 1
