#!/bin/env python

import sys
import string
from os import environ

def expandIRAFpath (fileName):
    """
    Converts any environment variable directory references
    from the DIR$ format in IRAF to the $DIR format in unix,
    then expands the reference to create a full directory
    pathname.
    """

    startSearch = 0
    ind = string.find (fileName, '$', startSearch)
    while (ind >= 0):
        prev = string.rfind (fileName, '/', 0, ind)
        if (prev >=0):
            fileName = fileName[0:prev+1] + environ[fileName[prev+1:ind]] + fileName[ind+1:]
        else:
            fileName = environ[fileName[:ind]] + fileName[ind+1:]
        startSearch = ind
        ind = string.find (fileName, '$', startSearch)
    return fileName            
        

def removeForm( fileName ):
    """
    Comments out the <FORM...> ... </FORM> element in the review page
    after the page has been moved to the reviewed directory.

    INPUT:
        IRAF formatted filename:
        DATADIR$/.../.../file.html
    """

    fileName = expandIRAFpath (fileName)
    fIn = file( fileName, 'r' )
    lines = fIn.readlines()
    fIn.close()

    fOut = file( fileName, 'w' )

    for line in lines:
        x = line.find( "<FORM" )
        if ( x != -1 ):
            line = line[0:x] + "<!-- " + line[x:]
        x = line.find( "</FORM" )
        if( x != -1 ):
            y = len("</FORM>")
            line = line[0:x+y] + " -->" + line[x+y:]
        fOut.write( line )
    fOut.close()

if __name__ == "__main__":
    if( len( sys.argv ) == 2 ):
        removeForm( sys.argv[1] )

