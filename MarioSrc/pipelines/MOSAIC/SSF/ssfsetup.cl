#!/bin/env pipecl
#
# SSFSETUP -- Setup SSF processing data. 
#
# This is needed to set the pipedata and uparm directory.

string	dataset, datadir, ifile, lfile
int	founddir

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
names ("ssf", envget("OSF_DATASET"))
dataset = names.dataset
ifile = names.indir // dataset // ".ssf"
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nSSFSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup UPARM.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

logout 1
