#!/bin/env pipecl
#
# SSFSFT -- Apply flat field.

string	dataset, datadir, ifile, lfile, s, sft
real	mjd
file	caldir = "MC$"

# Packages and task.
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"
images
servers
dataqual
mscred
cache mscred

# Set paths and files.
names ("ssf", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".ssf"
lfile = datadir // names.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSSFSFT (%s): ", dataset) | tee (lfile)
time | tee (lfile)

list = ifile
while (fscan (list, s1) != EOF) {
    # Set the calibrated name from the uncalibrated name.
    i = strldx ("/", s1)
    j = strlstr ("_f.fits", s1)
    if (j > 0)
	s2 = substr (s1, i+1, j-1)
    else {
	j = strlstr ("_p.fits", s1)
	if (j == 0)
	    j = strlstr (".fits", s1)
	;
	s2 = substr (s1, i+1, j-1)
    }
    s = s2 // "_s"

    # Set the sky flat.
    sft = ""; hselect (s1, "sft", yes) | scan (sft)
    if (sft != "") {
	if (imaccess(sft) == NO) {
	    sft = ""
	    getcal (s1, "sft", cm, caldir, obstype="",
	        detector=cl.instrument, imageid="!ccdname", filter="!filter",
		exptime="", mjd="!mjd-obs", quality=0.,
		match="!nextend,ccdsum") | scan (i, line)
	    if (i == 0)
		hselect (s1, "sft", yes) | scan (sft)
	    else
	        sendmsg ("WARNING", "No library sky flat template to apply",
		    substr(s1,strldx("/",s1)+1,1000), "CAL")
	}
	;
    }
    ;

    # Apply sky flat to data.
    if (sft != "") {
	_ccdtool (s1, s, "", "", proctype="object", fixpix-, overscan-,
	    trim-, zerocor-, darkcor-, flatcor-, sflatcor+, sflat="!sft")
	hedit (s, "SFLAT", sft, add+)
	hselect (s, "mjd-obs", yes) | scan (mjd)
	newDataProduct (s, mjd, "objectimage", "")
	storekeywords (class="objectimage", id=s, sid=s, dm=dm)
	#setkeyval (class="objectimage", id=s, dm=dm, keyword="dqpgcal",
	#    value=sft)
    } else
	imcopy (s1, s, verbose-)
}
list = ""

logout 1
