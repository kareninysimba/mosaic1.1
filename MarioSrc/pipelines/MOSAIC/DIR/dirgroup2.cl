#!/bin/env pipecl
#
# DIRGROUP2 -- Set list of flats for call to FTR pipeline.
#
# 	STATUS	MEANING
#	0	Untrapped or trapped error
#	1	Found data
#	2	Found no data

string dataset, indir, datadir, inlist, lfile, flist, pattern

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".fdir"
flist = datadir // "flist.tmp"

# Log start of processing.
printf ("\nDIRGROUP2 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Find list of flats or return no data status.
if (access (flist)) {
    rename (flist, inlist)
    type (inlist)
    type ("@"//inlist)
    logout 1
} else
    logout 2
