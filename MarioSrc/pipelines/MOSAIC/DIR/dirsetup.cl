#!/bin/env pipecl
#
# DIRSETUP -- Setup DIR processing. 
# 
# This is to setup the link for pipedata which contains files used
# by dirverify.

string	dataset, datadir, ifile, lfile
int     max, num, nextend

# Packages and tasks.
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers
proto
task colmatch = "$!awk \'{if(\$2==$1){print \$0}}\' $2"

# Set paths and files.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".dir"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Setdirs below will set the uparm and pipedata directories based on the
# first exposure in the list for which it can successfully execute the
# getcal for uparm/pipedata. It is possible, however, that that first
# image has a different value for NEXTEND in the header than the majority
# of the rest of the observations. Thus, the contents of pipedata (and 
# especially of keyword.dct, could be set such that the majority of the
# data will be rejected during dirverify. To prevent this from happening,
# the values of NEXTEND are screened, and, if more than one value for
# NEXTEND is found, only those with the most data are passed on.

# Keep original ifile to make reprocessing possible
if ( access( "rerun" ) ) {
    delete( ifile )
    copy( "rerun", ifile )
} else
    copy( ifile, "rerun" )

# Append "[0]" to all files in the list.
list = ifile
while (fscan( list, s1) != EOF)
    print (s1//"[0]", >> "dirsetup1.tmp")
list = ""
# Retrieve the filename and NEXTEND
hselect( "@dirsetup1.tmp", "$I,NEXTEND", yes, >> "dirsetup2.tmp" )
# Get the unique values of NEXTEND
fields( "dirsetup2.tmp", 2 ) | sort( numeric+ ) | unique( > "dirsetup3.tmp" )
# Loop over all values, and determine the one that is most common
list = "dirsetup3.tmp"
max = 0
nextend = 0
while ( fscan( list, i ) != EOF ) {
    # Only match against the value of the second column
    colmatch( i, "dirsetup2.tmp" ) | count | scan( num )
    if ( num > max ) { 
	max = num
	nextend = i
    }
    ;
}
# Select only those entries from the original list that have the 
# most common value of NEXTEND
colmatch( nextend, "dirsetup2.tmp" ) | fields( "STDIN", "1", > "dirsetup4.tmp" )

# Replace the original file list if needed
count( "dirsetup3.tmp" ) | scan( i )
if ( i > 1 ) {
    # Remove the [0] from the list
    list = "dirsetup4.tmp"
    while ( fscan( list, s1 ) != EOF )
        print ( substr( s1, 1, strldx("[",s1)-1 ), >> "dirsetup5.tmp" )
    list = ""
    # Replace ifile
    delete( ifile )
    rename( "dirsetup5.tmp", ifile )
}
;

# Clean up
delete( "dirsetup?.tmp" )

# Set directories.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
iferr {
    setdirs ("@"//ifile, extn="[0]", mjddef="")
} then 
    logout 0
else
    ;

logout 1
