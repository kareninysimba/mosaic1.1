#!/bin/env pipecl
#
# DIRGROUP1 -- Set list of zeros for call to FTR pipeline.
#
# 	STATUS	MEANING
#	0	Untrapped or trapped error
#	1	Found data
#	2	Found no data

string dataset, indir, datadir, inlist, lfile, zlist, pattern

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".zdir"
zlist = datadir // "zlist.tmp"

# Log start of processing.
printf ("\nDIRGROUP1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Find list of zeros or return no data status.
if (access (zlist)) {
    rename (zlist, inlist)
    type (inlist)
    type ("@"//inlist)
    logout 1
} else
    logout 2
