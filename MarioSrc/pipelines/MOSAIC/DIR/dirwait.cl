# DIRWAIT -- Template for enter operator wait.

procedure dirwait (dataset, ifile, lfile)

string	dataset			{prompt="Dataset Name"}
file	ifile			{prompt="Input file"}
file	lfile			{prompt="Log file"}
int	status = 2		{prompt="Return status"}

begin
	status = 0

	sendmsg ("OPER", "Waiting for special preprocessing", "", "OPER")

	status = 2
end
