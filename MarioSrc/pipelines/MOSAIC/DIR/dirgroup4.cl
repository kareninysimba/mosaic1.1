#!/bin/env pipecl
#
# DIRGROUP4 -- Set list of short exposure objects for call to FTR pipeline.
#
# 	STATUS	MEANING
#	0	Untrapped or trapped error
#	1	Found data
#	2	Found no data

string dataset, indir, datadir, inlist, lfile, slist, pattern

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".sdir"
slist = datadir // "slist.tmp"

# Log start of processing.
printf ("\nDIRGROUP4 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Find list of objects or return no data status.
if (access (slist)) {
    rename (slist, inlist)
    type (inlist)
    type ("@"//inlist)
    logout 1
} else
    logout 2
