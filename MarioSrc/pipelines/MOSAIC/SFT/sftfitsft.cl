#!/bin/env pipecl
#
# SFTFITSFT -- Determine the noise in the sky flat and fit a
# smooth surface to the sky flat

int	status = 1
int	edge = 400
int	rulepassed, ia, ib, nimg
real	x1, x2, x3, x4
string	dataset, datadir, lfile, tmpfile, datasft, datassft, cast, ifile
string  filter, rulepath, func

# Packages and task.
images
utilities
proto
servers
mscred
cache mscred
task scmb = "mariosrc$scmb.cl"

# Define rules
findrule( dm, "enough_for_sflat", validate=1 ) | scan( rulepath )
task enough_for_sflat = (rulepath)

# Set paths and files.
sftnames (envget ("OSF_DATASET"))
dataset = sftnames.dataset
datadir = sftnames.datadir
datasft = sftnames.sft
datassft = sftnames.ssft
lfile = sftnames.lfile 
ifile = sftnames.indir // dataset // ".sft"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = sftnames.uparm)
set (pipedata = sftnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSFTFITSFT (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up.
delete ("sftfitsft*.fits,sftfitsft*.tmp,sfttmp*")
delete (datasft//"[ab].*")
delete (datassft//".*")
delete (dataset//"_sft_bpm[ab].pl")

# Check what needs to be done.
if (sft_smooth == "no" || sft_window <= 1)
    logout 2
;

# Evaluate data quality information for automatic decision on smoothing.
sft_smooth = "yes"
if (sft_smooth != "yes") {
    ia = 0; ib = 0; j = 0
    hselect ("@"//ifile, "$I,SKYMEAN,SFTFLAG", yes) |
	translit ("STDIN", '"', delete+, > "sftfitsft3.tmp")
    list = "sftfitsft3.tmp"
    while (fscan (list, s1, x, s3) != EOF) {
	s2 = substr( s1, strldx("/",s1)+1, strlen(s1) )
	if (nscan() < 3) {
	    sendmsg ("WARNING", "Missing keywords", s2, "VRFY")
	    next
	}
	;
	if (substr(s3,1,1) == "N")
	    next
	else if (x <= 0.01) {
	    sendmsg ("WARNING", "SKYMEAN must be positive",
		s2//" - "//str(x), "VRFY")
	    next
	} else
	    x = 1. / x

	# To determine the noise we need two input files,
	# each containing half of the sky image. To minimize the risk of a
	# systematic difference between the two if the sky level is changing
	# (although this should be taken out by the scaling) the images
	# are divided with an ABBA pattern.
	if (j==0) {
	    print (s1, >> "sftfitsft1a.tmp")
	    print (x, >> "sftfitsft2a.tmp")
	    ia += 1
	}
	;
	if (j==1) {
	    print (s1, >> "sftfitsft1b.tmp")
	    print (x, >> "sftfitsft2b.tmp")
	    ib += 1
	}
	;
	if (j==2) {
	    print (s1, >> "sftfitsft1b.tmp")
	    print (x, >> "sftfitsft2b.tmp")
	    ib += 1
	}
	;
	if (j==3) {
	    print (s1, >> "sftfitsft1a.tmp")
	    print (x, >> "sftfitsft2a.tmp")
	    ia += 1
	}
	;
	j += 1
	if (j==4) {
	    j = 0
	}
	;
    }
    list = ""
    delete ("sftfitsft3.tmp")
    # Determine the smaller of ia and ib
    nimg = ib
    if (ia<nimg) {
	nimg = ia
    }
    ;

    # Get filter name from last image in list
    hselect( s1, "filter", yes ) | scan( filter)
    if (nscan()!=1) {
	sendmsg ("WARNING", "FILTER not found in header, using default rule",
	    s2, "VRFY")
	filter = "default"
    }
    ;

    # Test whether we have enough frames in each half to run scombine
    enough_for_sflat( filter, nimg ) | scan( rulepassed )
    if (rulepassed==0) {
	printf ("Not enough images to determine noise in sky flat (%d)\n",
	    nimg, >> lfile)
	sendmsg ("WARNING", "Not enough images to determine noise in sky flat",
	    str(nimg), "DQ")
	delete ("sftfitsft*.tmp")
	status = 2
    } else {
	# Enough images to run scombine on each half
	scmb ("sftfitsft1a.tmp", datasft//"a", dataset//"_sft_bpma",
	    "sftfitsft2a.tmp", "sftfitsfta", imcmb="IMCMBMEF")
	scmb ("sftfitsft1b.tmp", datasft//"b", dataset//"_sft_bpmb",
	    "sftfitsft2b.tmp", "sftfitsftb", imcmb="IMCMBMEF")
	## TODO Combine the bad pixel mask and use for statistics

	tmpfile = mktemp("sfttmp")
	imarith( datasft//"a", "-", datasft//"b", tmpfile )
	imstat( tmpfile, fields="mean,stddev", nclip=1, lsigma=3, usigma=3,
	    format- ) | scan( x1, x2 )
	imdel( tmpfile )

	# Noise in difference image is the is roughly 2 times higher
	# than the real noise in the sky flat image determined in sftmksft.cl,
	# because the latter has been normalized, whereas the former has 
	# not yet been. However, the factor is not exactly 2, because:
	# 1) both halves of the sky flat calculated here may
	#    have different numbers of contributing exposures (this happens
	#    if the total number of images is an odd number).
	# 2) the noise levels of the contributing images is not the same
	# The second one not easily corrected, so it is assumed that the 
	# ABBA pattern used to divide the images into two groups will divide
	# images in such a way that the average noise is the same in both
	# groups.
	# The first one can be calculated, if one assumes that the noise
	# is the same in all images. In that case, the noise in skyflat1 (S1)
	# is s1/sqrt(n1), where si is the noise in a single image and n1
	# the number of images. Similarly, the noise in S2 is si/sqrt(n2).
	# Hence, the noise in the difference image D is sd=sqrt(si^2/n1+si^2/n2).
	# Now the factor F is needed that will normalize the noise in D
	# to the expected noise level of st=si/sqrt(nt), where nt is the
	# total number of images. This factor F can be determined by
	# solving sd/F=st. Because si can be divided out of the equation, we
	# get, after squaring both sides and moving F to the right hand
	# side: 1/n1+1/n2=F^2/nt. Realizing that nt=n1+2, we finally
	# get F=(n1+n2)/sqrt(n1*n2). The lowest possible odd number of images
	# for which we can determine both halves of the skyflat is 3. In
	# this extreme case, F=3/sqrt(2)=2.12. For more realistic numbers,
	# say 7 (minimum number as defined by enough_for_sflat on 10/19/05),
	# F=2.0045. Because this is only marginally different from 2, and
	# because variations in noise level are likely to play a more important
	# role that the difference in number of contributing images to
	# each of the sky flats, it is assumed that F=2.

	x2 = x2/2 # Normalize the noise

	# If successful, write the statistics to the header and PMAS
	if (nscan()==2) {
	    hedit( datasft, "dqsflcmn", x1, add+, verify-, show+, update+,
	        >> lfile)
	    hedit( datasft, "dqsflcsi", x2, add+, verify-, show+, update+,
	        >> lfile)
	    hsel( datasft, "dqsfglme", yes ) | scan( x3 ) # Get global mean
	    x4 = x3/x2 # Signal-to-noise ratio
	    hedit( datasft, "dqsfsnr", x4, add+, verify-, show+, update+,
	        >> lfile)
	    printf("%12.5e\n", x1 ) | scan( cast )
	    setkeyval( class="skyflatimage", id=datasft, dm=dm,
		keyword="dqsflcmn", value=cast )
	    printf("%12.5e\n", x2 ) | scan( cast )
	    setkeyval( class="skyflatimage", id=datasft, dm=dm,
		keyword="dqsflcsi", value=cast )
	    printf("%12.5e\n", x4 ) | scan( cast )
	    setkeyval( class="skyflatimage", id=datasft, dm=dm,
		keyword="dqsfsnr", value=cast )
	}
	;
    }
}
;

# Determine a smooth version.
# Remove average line and column signature first and then add it back
# after smoothing. Check for dual amps and allow a discontinuity in
# the gain.

if (sft_projsmooth > 0) {
    if (sft_projsmooth > 3)
        func = "spline3"
    else
        func = "chebyshev"
    i=0; hselect (datasft, "NEXTEND,NAXIS1,NAXIS2", yes) | scan (i, ia, ib)
    imrename (datasft, "sftfitsft1")
    blkavg ("sftfitsft1", "sftfitsft2", 1, ib)
    if (i > 8) {
	if (func == "spline3")
	    k = min (ia / 32, sft_projsmooth - 3)
	else
	    k = sft_projsmooth
	i = 1; j = ia / 2
	printf ("[%d:%d,*]\n", i, j) | scan (s1)
	fit1d ("sftfitsft2"//s1, "sftfitsft2", "fit", axis=1, func=func,
	    order=k, interact-)
	i = edge + 1
	printf ("[%d:%d,*]\n", i, j) | scan (s1)
	fit1d ("sftfitsft2"//s1, "sftfitsft2", "fit", axis=1, func="chebyshev",
	    order=4, interact-)
	i = ia/2+1; j = ia
	printf ("[%d:%d,*]\n", i, j) | scan (s1)
	fit1d ("sftfitsft2"//s1, "sftfitsft2", "fit", axis=1, func=func,
	    order=k, interact-)
	j = ia - edge
	printf ("[%d:%d,*]\n", i, j) | scan (s1)
	fit1d ("sftfitsft2"//s1, "sftfitsft2", "fit", axis=1, func="chebyshev",
	    order=4, interact-)
    } else {
	if (func == "spline3")
	    k = min (ia / 16, sft_projsmooth - 3)
	else
	    k = sft_projsmooth
	fit1d ("sftfitsft2", "sftfitsft2", "fit", axis=1, func=func,
	    order=k, interact-)
	i = edge + 1; j = ia - edge
	printf ("[%d:%d,*]\n", i, j) | scan (s1)
	fit1d ("sftfitsft2"//s1, "sftfitsft2", "fit", axis=1, func="chebyshev",
	    order=4, interact-)
    }

    blkrep ("sftfitsft2", "sftfitsft3", 1, ib)
    imdelete ("sftfitsft2")
    imexpr ("a/max(0.01,b)", "sftfitsft4", "sftfitsft1", "sftfitsft3", verb-)
    blkavg ("sftfitsft4", "sftfitsft2", ia, 1)
    imdelete ("sftfitsft4")
    if (func == "spline3")
	k = min (ia / 16, sft_projsmooth - 3)
    else
        k = sft_projsmooth
    fit1d ("sftfitsft2", "sftfitsft2", "fit", axis=2, func=func,
	order=k, interact-)
    i = edge + 1; j = ib - edge
    printf ("[*,%d:%d]\n", i, j) | scan (s1)
    fit1d ("sftfitsft2"//s1, "sftfitsft2", "fit", axis=2, func="chebyshev",
	order=4, interact-)
    blkrep ("sftfitsft2", "sftfitsft4", ia, 1)
    imdelete ("sftfitsft2")
    imexpr ("max(0.01,a*b)", "sftfitsft2", "sftfitsft3", "sftfitsft4", verb-)
    imdelete ("sftfitsft3,sftfitsft4")
    imexpr ("a/b", datasft, "sftfitsft1", "sftfitsft2", verb-)
}
;

if (sft_func == "median")
    fmedian( datasft, datassft, xwindow=sft_window, ywindow=sft_window )
else
    imsurfit (datasft, datassft, sft_xorder, sft_yorder, type_output="fit",
	function=sft_func, cross_terms=no, xmedian=11, ymedian=11,
	median_perce=50., lower=0., upper=0., ngrow=0, niter=0, regions="all",
	rows="*", columns="*", border="50", sections="", circle="",
	div_min=INDEF)

if (sft_projsmooth > 0) {
    imdelete (datasft)
    imrename ("sftfitsft1", datasft)
    imrename (datassft, "sftfitsft1")
    imexpr ("a*b", datassft, "sftfitsft1", "sftfitsft2", verb-)
    imdelete ("sftfitsft1,sftfitsft2")
}
;

# Check for success and log some data quality information.
if (imaccess (datassft) == NO ) {
    sendmsg ("WARNING", "Smoothed sky flat not created", "", "PROC")
    status = 2
} else {
    # Estimate signal-to-noise ratio for resulting sky flat. It is assumed
    # that the pixel-to-pixel variations have mostly been taken out by the
    # dome flat, and that the remaining variations are mostly the result
    # of noise. This method is not as accurate as the one above, but it
    # can be determined for all skyflats, whereas for the above method more
    # images are needed.  To estimate the noise from the skyflat directly,
    # one cannot use the global sigma, because that could be dominated by
    # large scale variations in the sky flat. To get the noise, one should
    # either determine the noise locally for many points over the sky flat
    # (say in 10x10 boxes), or one can subtract the large scale variations,
    # which is what is done here.

    tmpfile = mktemp("sfttmp")
    # Subtract the smooth version from the original
    imarith( datasft, "-", datassft, tmpfile )
    # Determine mean and sigma
    imstat( tmpfile, fields="mean,stddev", nclip=1, lsigma=3, usigma=3,
        format=no ) | scan( x1, x2 )
    imdel( tmpfile )
    # If successful, write the statistics to the header and PMAS
    if (nscan()==2) {
        hedit( datasft, "dqsflcme", x1, add+, verify-, show+, update+, >> lfile)
        hedit( datasft, "dqsflcse", x2, add+, verify-, show+, update+, >> lfile)
        hsel( datasft, "dqsfglme", yes ) | scan( x3 ) # Get global mean
        x4 = x3/x2 # Signal-to-noise ratio
        hedit( datasft, "dqsfsnre", x4, add+, verify-, show+, update+, >> lfile)
        printf("%12.5e\n", x1 ) | scan( cast )
        setkeyval( class="skyflatimage", id=datasft, dm=dm,
            keyword="dqsflcme", value=cast )
        printf("%12.5e\n", x2 ) | scan( cast )
        setkeyval( class="skyflatimage", id=datasft, dm=dm,
            keyword="dqsflcse", value=cast )
        printf("%12.5e\n", x4 ) | scan( cast )
        setkeyval( class="skyflatimage", id=datasft, dm=dm,
            keyword="dqsfsnre", value=cast )
    }
    ;
}

delete ("sftfitsft*.tmp")
imdel( "*_obm.fits" )

logout (status)
