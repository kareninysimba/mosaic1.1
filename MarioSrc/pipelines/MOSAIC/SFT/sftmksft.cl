#!/bin/env pipecl
#
# SFTMKSFT -- Make sky flat.

int	rulepassed
string	dataset, datadir, ifile, lfile
string  filter, rulepath
string	procid, datasft, cast
real	mjd, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Packages and task.
images
proto
utilities
servers
dataqual
mscred
cache mscred
task scmb = "mariosrc$scmb.cl"
task sftselect = "NHPPS_PIPESRC$/MOSAIC/SFT/sftselect.cl"
cache sftselect

# Define rules
findrule( dm, "enough_for_sflat", validate=1 ) | scan( rulepath )
task enough_for_sflat = (rulepath)

# Set paths and files.
sftnames (envget ("OSF_DATASET"))
dataset = sftnames.dataset
datadir = sftnames.datadir
datasft = sftnames.sft
lfile = datadir // sftnames.lfile
ifile = sftnames.indir // dataset // ".sft"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = sftnames.uparm)
set (pipedata = sftnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nSFTMKSFT (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# CLearup from previous processing.
delete ("sftmksft[^_]*")
if (imaccess(datasft))
    imdelete (datasft)
;
if (imaccess(sftnames.bpm))
    imdelete (sftnames.bpm)
;

# Select images and scale factors.
sftselect ("SFTFLAG", ifile, "sftmksft1.tmp", "sftmksft2.tmp", cksftflag+)

# Check the rule.
if (sftselect.nselect > 0 && sftselect.filter == "default")
    sendmsg ("WARNING", "FILTER not found, using default rule", "", "VRFY")
;
enough_for_sflat (sftselect.filter, sftselect.nselect) | scan (i)
if (sftselect.nforced > 0  && sftselect.nselect > 0)
    rulepassed = 1
else
    rulepassed = i

if (rulepassed == 0) {
    sendmsg ("WARNING", "Not enough images to make sky flat",
	str(sftselect.nselect), "DQ")
    printf ("WARNING: Not enough images to make sky flat (%d).\n",
	sftselect.nselect) | tee (lfile)
    sftselect.nselect = 0
}
;

# This is a pain but we need to do this outside an if block because of a bug.
enough_for_sflat (sftselect.filter, sftselect.nselect) | scan (i)
if (rulepassed == 0 && i == 0) {
    delete ("sftmksft[^_]*")
    logout 2
}
;

# Run scombine with the object masks.  If the number is large do it in blocks.
scmb ("sftmksft1.tmp", datasft, sftnames.bpm, "sftmksft2.tmp", "sftmksfta",
    imcmb="IMCMBMEF")
hedit (datasft, "IMCMBSIF,IMCMBMEF", del+, ver-, show-)

# Clean up
delete ("sftmksft[^_]*")

# Check whether sky flat has been created
if (imaccess (datasft) == NO) {
    sendmsg ("WARNING", "Sky flat not created", datasft, "PROC")
    printf ("WARNING: No sky flat created (%s)\n", dataset) | tee (lfile)
    logout 0
}
;

# Sigma clip the template to avoid bad data.
imrename (datasft, "sftmksft.fits")
imrename (sftnames.bpm, "sftmksft.pl")
mimstat ("sftmksft.fits", imask="sftmksft.pl", omask=sftnames.bpm,
    nclip=3, lsig=10, usig=10, fields="min,max", format-) | scan (x, y)
printf ("Clipped data between %.3g and %.3g\n", x, y)
imexpr ("max(b,min(c,a))", datasft, "sftmksft.fits", x, y, outtype="real",
    verbose-)
imdelete ("sftmksft.fits,sftmksft.pl")

# Set CCDMEAN level to global sky value and delete CCDMEANT to tell
# ccdproc not to recompute it.
hedit (datasft, "CCDMEAN", "(SKYMEAN)", add+)
hedit (datasft, "CCDMEANT", del+)

# Remove trailing .fits if present
if (strlstr(".fits",datasft)>0) {
    s1 = substr (datasft, 1, strlstr(".fits",datasft)-1)
    datasft = s1
}
;

# Enter new dataproduct.
hsel( datasft, "mjd-obs", yes ) | scan( mjd )
newDataProduct(datasft, mjd, "skyflatimage", "")
storekeywords( class="skyflatimage", id=datasft, sid=datasft, dm=dm )

# Add number of contributing exposures to header and PMAS
hedit( datasft, "DQSFNUSD", sftselect.nselect, add+, verify-,
    show+, update+, >> lfile)
printf("%d\n", sftselect.nselect) | scan( cast )
setkeyval( class="skyflatimage", id=datasft, dm=dm,
    keyword="dqsfnusd", value=cast )

sflatstruct( datasft )
flpr # Force IRAF to update the header of datasft

# Read results from flatstruct from header
hselect (datasft, "dqsfxslp,dqsfyslp,dqsfxsig,dqsfysig,\
    dqsfglme,dqsfglsi,dqsfxmea,dqsfxcef,dqsfymea,dqsfycef", yes ) |
    scan( x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 )
if ( nscan() == 10 ) {
    printf("%12.5e\n", x1 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfxslp", value=cast )
    printf("%12.5e\n", x2 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfyslp", value=cast )
    printf("%12.5e\n", x3 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfxsig", value=cast )
    printf("%12.5e\n", x4 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfysig", value=cast )
    printf("%12.5e\n", x5 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfglme", value=cast )
    printf("%12.5e\n", x6 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfglsi", value=cast )
    printf("%12.5e\n", x7 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfxmea", value=cast )
    printf("%12.5e\n", x8 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfxcef", value=cast )
    printf("%12.5e\n", x9 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfymea", value=cast )
    printf("%12.5e\n", x10 ) | scan( cast )
    setkeyval( class="skyflatimage", id=datasft, dm=dm,
	keyword="dqsfycef", value=cast )
} else
    sendmsg ("ERROR", "Could not read all data from sflatstruct",
	"", "DQ")

# Set identifying keywords
hedit( datasft, "PROCTYPE", "MasterCal", add+, ver- )
hedit( datasft, "OBSTYPE", "sky flat", add+, ver- )

# Set quality flag.
if (rulepassed == 0)
    hedit (datasft, "QUALITY", -1, add+, ver-)
else
    hedit (datasft, "QUALITY", 0, add+, ver-)

logout 1
