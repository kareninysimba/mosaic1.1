#!/bin/env pipecl
#
# GPCgrp
#
# Description:
#
# 	This module sorts through a list of stacked-SIFs to create sublists of dither suites.
# 
# Exit Status Values:
#
#	1 = Successful
#	2 = No stacked-SIFs included in ilist.
#	3 = At least one set of stacked SIFs did not contain eight stacked SIFs, but contained
#		more than one.
#	4 = At least one set of stacked SIFs contained only ONE stacked SIF.  There should be
#		eight stacked SIFs for each set.  This exit status value overrides all others.
#
# History:
#
# 	T. Huard  200803--  Created.
#	T. Huard  20080408  Changed the lists included within the list of lists to contain
#		two columns: filename, badpix mask.  These columns contain the full 
#		pathnames.  A check has been added to be sure the BPM value in the image
#		headers is only the filename with no path info.  If there is path info in
#		BPM in the header, it is revised to only include the filename.  Also, changed
#		the exit status value to 2 for the cases when there are no stacked-SIFs in 
#		ilist.  Finally, some improvements in code were made, based on suggestions
#		Frank had for gpsgrp.cl.  The code was made as analogous to gpsgrp.cl as
#		possible.
#	T. Huard 20080409  Fixed seqlabel to include kp4m or ct4m, but not ccd* part since
#		this module groups files to call MDC, which combines stacked SIFs (ccd1,
#		ccd2, ..., ccd8).  Any "-" that occurs in seqlabel (there shouldn't be any
#		though!) are removed since they may cause problems with the GPC/MDC return (see
#		similar note in gpsgrp.cl in GPS).  The naming convention for the stacked
#		products has been changed from mdsstack_* to *_sifstk*, and the matching in
#		this module has been changed accordingly.
#		Also, the revised ilist (REV,REVBPM) names have been slightly changed.
# 	Last Revised:  T. Huard  20080409  2:15pm

# Declare variables
int	exitval,icheck,ipos1,ipos2,ipos3,slen,icount,icount2,icount3
string	dataset,indir,datadir,ilist,lfile
string	ilistREV,ilistREVBPM,filename,bpm,filenameSHORT,seqlabel,stmp,datadirFULL

# Load packages
images
proto

# Set file and path names
names("gpc",envget("OSF_DATASET"))
dataset=names.dataset
indir=names.indir
datadir=names.datadir
ilist=indir//dataset//".gpc"
lfile=datadir//names.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nGPCgrp (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful).
exitval=1

# Sort through ilist to obtain a list of the stacked-SIF filenames.  
# If there are no stacked SIFs, then the procedure exits.
ilistREV=indir//dataset//"-gpc.gpcREV"
match("ccd?*stk.fits",ilist,metacharacters=no,>ilistREV)
count(ilistREV) | scan(icheck)
if (icheck == 0) {
	exitval=2
	logout(exitval)
}
;

# Sort through ilist to obtain a list of badpixel masks for the stacked SIFs.
ilistREVBPM=indir//dataset//"-gpc.gpcREVBPM"
match("ccd?*stk_bpm.pl",ilist,metacharacters=no,>ilistREVBPM)

# Create a list FILENAME,BPM, where BPM in this list is the value of the header keyword.
hselect("@"//ilistREV,"$I,BPM",yes,>"gpcgrp_tmp.list")

# Match the BPM for each image with the lines in ilistREVBPM, which contain the
# full path.  Replace the BPM column with the full pathname.
# To be safe, let's be sure that the original BPM column (from the BPM values in
# headers) does not include any path information.  If the original BPM column
# does contain some path information, then change the BPM value in the header 
# such that it is only the filename.
if (access("gpcgrp_tmp2.list")) delete("gpcgrp_tmp2.list",verify-)
;
if (access("gpcgrp_seqlabels.list")) delete("gpcgrp_seqlabels.list",verify-)
;
list="gpcgrp_tmp.list"
while (fscan(list,filename,bpm) != EOF) {
#	Convert bpm to filename only (no path), if some path exists.
#	Be sure that the BPM value in the header is only the filename also.
#	Then, compare the filename-only bpm with ilistREVBPM to obtain the 
#	full-pathname bpm.
	ipos1=strlstr("/",bpm)
	if (ipos1 > 0) {
		slen=strlen(bpm)
		bpm=substr(bpm,ipos1+1,slen)
		hedit(filename,"BPM",bpm,add-,addonly-,verify-)
		print("*****GPCgrp: BPM contained path info for file: "//filename)
	}
	;
	match(bpm,ilistREVBPM,metacharacters=no) | head("STDIN",nlines=1) | scan(bpm)
#	Create a shortened filename with no pathname, for sorting purposes
	ipos1=strlstr("/",filename)
	if (ipos1 > 0) {
		slen=strlen(filename)
		filenameSHORT=substr(filename,ipos1+1,slen)
	} else {
		filenameSHORT=filename
	}
	seqlabel = substr (filename,strldx("-",filename)+1,strldx("_",filename)-1)
	printf("%s %s %s %s\n",filename,bpm,seqlabel,filenameSHORT,>>"gpcgrp_tmp2.list")
	printf("%s\n",seqlabel,>>"gpcgrp_seqlabels.list")
}
list=""
sort("gpcgrp_tmp2.list",column=4,ignore-,numeric-,>"gpcgrp_seqlabelsFULL.list")
delete("gpcgrp_tmp.list",verify-)
delete("gpcgrp_tmp2.list",verify-)

# Construct list of unique dither sequence labels
sort("gpcgrp_seqlabels.list",ignore-,numeric-) | unique(>"gpcgrp_seqlabelsUNIQUE.list")

# Obtain filenames for stacked-SIFs in each dither sequence by comparing the list
# of unique dither sequence labels with the full list.  Include these filenames
# in a list with the name unique to that dither sequence.  The list of all stacked-SIFs
# dither sequence lists is named "gpcgrp_seqALL.list" (YES, that is a tongue-twister!)
pathnames | scan(datadirFULL)
icount=0   # Number of sequences
icount2=0  # Number of sequences with more than 1 stacked SIFs, but not 8.
icount3=0  # Number of sequences with only 1 stacked-SIFs
list="gpcgrp_seqlabelsUNIQUE.list"
while(fscan(list,seqlabel) != EOF) {
	match(seqlabel,"gpcgrp_seqlabelsFULL.list",>"gpcgrp_tmp.list")
	count("gpcgrp_tmp.list") | scan(icheck)
	if (icheck == 8) {
#		Construct the stacked-SIFs dither sequence list
		if (access("gpcgrp_seq_"//seqlabel//".list")) delete("gpcgrp_seq_"//seqlabel//".list",verify-)
		;
		fields("gpcgrp_tmp.list","1,2",>"gpcgrp_seq_"//seqlabel//".list")
#		Append filename of the stacked-SIFs dither sequence list to the lis of all stacked-SIF dither
#		sequence lists.
		print(datadirFULL//"gpcgrp_seq_"//seqlabel//".list",>>"gpcgrp_seqALL.list")
	} else {
		if (icheck > 1) {
			icount2=icount2+1
#			Construct the stacked-SIFs dither sequence list
			seqlabel=line
			if (access("gpcgrp_seq_"//seqlabel//".list")) delete("gpcgrp_seq_"//seqlabel//".list",verify-)
			;
			fields("gpcgrp_tmp.list","1,2",>"gpcgrp_seq_"//seqlabel//".list")
#			Append filename of the stacked-SIFs dither sequence list to the lis of all stacked-SIF dither
#			sequence lists.
			print(datadirFULL//"gpcgrp_seq_"//seqlabel//".list",>>"gpcgrp_seqALL.list")
		} else {
			icount3=icount3+1
			seqlabel=line
			if (access("gpcgrp_seq_"//seqlabel//".list")) delete("gpcgrp_seq_"//seqlabel//".list",verify-)
			;
			fields("gpcgrp_tmp.list","1,2",>"gpcgrp_seq_"//seqlabel//".list")
		}
	}
	delete("gpcgrp_tmp.list",verify-)
	icount=icount+1
}
list=""

# Check that there are more than 1 sequences to be stacked and write some general information to the log file.
print("GPCgrp: Number of dither sequences found: "//icount)
if (icount2 > 0) {
	exitval=3
	print("ERROR:  GPCgrp:  GPC found "//str(icount2)//" incomplete set(s) of stacked-SIFs, but these incomplete sets")
	print("        had at least two stacked-SIFs to combine.  There should be eight stacked-SIFs for each")
	print("        dither sequence!  Nevertheless, the stacked-SIFs were combined for each set.")
}
;
if (icount3 > 0) {
	exitval=4
	print("ERROR:  GPCgrp:  GPC found "//str(icount3)//" incomplete set(s) of stacked-SIFs with only ONE stacked-SIF")
	print("        to combine.  There should be eight stacked-SIFs for each dither sequence!")
}
;
if (icount > 0) {
	print("GPCgrp: List of dither sequence stacked-SIF lists written to file: gpcgrp_seqALL.list")
}
;

logout(exitval)
