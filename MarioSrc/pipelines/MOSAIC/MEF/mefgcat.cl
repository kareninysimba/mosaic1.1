#!/bin/env pipecl
#
# MEFGCAT -- Get reference catalogs for WCS and photometric calibration.
#
# The way in which the catalogs are obtain may change with time but this
# routine encapsulates this.  It is run as a separate stage so that it
# can proceed in parallel with other operations.

int	rulepassed, nfound
real	ra, dec, equinox, radius
string	cat, rtnfile, rfile, lfile, rulepath

# Set dataset directory.
mefnames (envget("OSF_DATASET"))
cat = mefnames.shortname // "_usno"
lfile = mefnames.lfile
set (uparm = mefnames.uparm)
cd (mefnames.datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), mefnames.dataset) | tee (lfile)
time | tee (lfile)

# Load packages.
proto
images
noao
astutil
servers
task	mefgetcat = "NHPPS_PIPESRC$/MOSAIC/MEF/mefgetcat.cl"

# Define rules
findrule( dm, "enough_usno_entries", validate=1 ) | scan( rulepath )
task enough_usno_entries = (rulepath)

# Region is centered on telescope RA and DEC.  The catalog is part of the
# parameter set.

s1 = "@" // "MOSAIC_MEF$/input/" // mefnames.dataset // ".mef//\[0]"
ra = INDEF; dec = INDEF; equinox = 2000.
hselect (s1, "TELRA,TELDEC,TELEQUIN", yes) | scan (ra, dec, equinox)
if (isindef(ra) || isindef(dec) || isindef(equinox))
    hselect (s1, "RA,DEC,EQUINOX", yes) | scan (ra, dec, equinox)
;
if (isindef(ra) || isindef(dec) || isindef(equinox)) {
    hselect (s1, "CRVAL1,CRVAL2", yes) | scan (ra, dec)
    ra /= 15
}
;
if (isindef(ra) || isindef(dec) || isindef(equinox)) {
    sendmsg ("ERROR", "Pointing information not found", "", "VRFY")
    logout 2
}
;

if (abs(dec_shift) > 300.)
    dec -= dec_shift / 3600.
;
if (abs(ra_shift) > 300.) {
    ra -= ra_shift / 3600. / 15. / dcos (dec)
    if (ra < 0)
        ra += 24.
    else if (ra > 24)
        ra -= 24.
    ;
}
;

if (abs(equinox-2000.) > 0.01) {
    printf ("Precess from %.1f to 2000.\n", equinox) | tee (lfile)
    printf ("%g %g\n", ra, dec) |
    precess ("STDIN", equinox, 2000.) |
    scan (ra, dec)
}
;

if (telescope == "kp09m")
    radius = 0:40:00
else
    radius = 0:30:00
mefgetcat (ra, dec, radius, cat, bmax=23., vmax=23.)

# Check if a reasonable catalog returned.
if (access (cat) == NO) {
    sendmsg ("ERROR", "No USNO match catalog returned", "", "PROC")
    logout 0
} else
    ;
count (cat) | scan (nfound)
enough_usno_entries( nfound ) | scan( rulepassed )
if (rulepassed==0) {
    sendmsg ("ERROR", "Too few entries in USNO match catalog", str(nfound),
        "PROC")
    delete (cat)
    logout 2
}
;

# Log something.
printf ("    Retrieved %d reference objects from USNO-B1.0\n     \
    centered at %.2h %.1h\n", nfound, ra, dec) | tee (lfile)

logout 1
