#!/bin/env pipecl
#
# MEFHDR -- Trigger the header updating.
#
# This must be called after all stages which write header information have
# completed.

int	numccd1, numccd2, numccd3, numccd4, numccd5, numccd6, numccd7, numccd8
real	seeing, x1, zp, ezp, skyval, eskyval, skynoise, skymag, depth
string	cast, dataset, lfile, s4, s5, s6, s7, s8

# Load packages.
images
servers

# Directories and files.
mefnames (envget("OSF_DATASET"))
dataset = mefnames.dataset
lfile = mefnames.datadir // mefnames.lfile
set (uparm = mefnames.uparm)
cd (mefnames.datadir)

# Log start of processing.
printf ("\nMEFHDR (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Get seeing from each of the amplifiers, calculate the average,
# and store in PMAS

numccd1 = 0; numccd2 = 0; numccd3 = 0; numccd4 = 0
numccd5 = 0; numccd6 = 0; numccd7 = 0; numccd8 = 0
seeing = 0; zp = 0; ezp = 0; skyval = 0; eskyval = 0
skymag = 0; depth = 0; skynoise = 0
files ("*ccd*.ace", > "mefhdr.tmp")
list = "mefhdr.tmp"
while (fscan (list, s1) != EOF) {
    s2 = substr (s1, 1, strstr(".ace",s1)-1)
    sifnames (s2)
    printf( "%s\n", sifnames.image )
    getkeyval (dm=dm,id=sifnames.image,keyword="dqseamp",class="objectimage") | scan (s1)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqphzp",class="objectimage") | scan (s2)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqphezp",class="objectimage") | scan (s3)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqskval",class="objectimage") | scan (s4)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqskeval",class="objectimage") | scan (s5)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqskmag",class="objectimage") | scan (s6)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqphdpps",class="objectimage") | scan (s7)
    getkeyval (dm=dm,id=sifnames.image,keyword="dqsksig",class="objectimage") | scan (s8)
    printf("%s %s %s %s %s %s %s %s\n", s1, s2, s3, s4, s5, s6, s7, s8 )
    if (s1!="None") {
	x1 = INDEF; printf("%s\n",s1) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd1 = numccd1+1
	    seeing = seeing+x1
	}
	;
    }
    ;
    if (s2!="None") {
	x1 = INDEF; printf("%s\n",s2) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd2 = numccd2+1
	    zp = zp+x1
	}
	;
    }
    ;
    if (s3!="None") {
	x1 = INDEF; printf("%s\n",s3) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd3 = numccd3+1
	    ezp = ezp+x1
	}
	;
    }
    ;
    if (s4!="None") {
	x1 = INDEF; printf("%s\n",s4) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd4 = numccd4+1
	    skyval = skyval+x1
	}
	;
    }
    ;
    if (s5!="None") {
	x1 = INDEF; printf("%s\n",s5) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd5 = numccd5+1
	    eskyval = eskyval+x1
	}
	;
    }
    ;
    if (s6!="None") {
	x1 = INDEF; printf("%s\n",s6) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd6 = numccd6+1
	    skymag = skymag+x1
	}
	;
    }
    ;
    if (s7!="None") {
	x1 = INDEF; printf("%s\n",s7) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd7 = numccd7+1
	    depth = depth+x1
	}
	;
    }
    ;
    if (s8!="None") {
	x1 = INDEF; printf("%s\n",s8) | scan( x1 )
	if (isindef(x1)==NO) {
	    numccd8 = numccd8+1
	    skynoise = skynoise+x1
	}
	;
    }
    ;
}
list = ""

# Compute global parameters.
if (numccd1 != 0) {
    seeing = seeing / numccd1
    hedit (mefnames.hdr, "DQSEMEF", seeing, add+)
    printf("%12.5e\n", seeing ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqsemef", value=cast )
#    # Store average seeing also at amplifier/ccd level
#    list = "mefhdr.tmp"
#    while (fscan (list, s1) != EOF) {
#        s2 = substr (s1, 1, strstr(".ace",s1)-1)
#        sifnames (s2)
#        hedit( sifnames.datadir//sifnames.shortname, "DQSEMEF", seeing, add+, ver- )
#    }
#    list = ""; 
} else
    if (verbose>=1)
        print( "WARNING: Cannot calculate global seeing (no dqseamp found)" )
    ;
;

if (numccd2 != 0) {
    zp = zp / numccd2
    printf("%12.5e\n", zp ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqphazp", value=cast )
} else
    if (verbose>=1)
        print ("WARNING: Cannot calculate global zero point (no dqphzp found)")
    ;
;

if (numccd3 != 0) {
    ezp = ezp / numccd3
    printf("%12.5e\n", ezp ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqpheazp", value=cast )
} else
    if (verbose>=1)
        print( "WARNING: Cannot calculate global error on zeropoint (no dqphezp found)" )
    ;
;

if (numccd4 != 0) {
    skyval = skyval / numccd4
    hedit( mefnames.hdr, "SKYADU", skyval, add+ )
    printf("%12.5e\n",skyval  ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqskaval", value=cast )
} else
    hedit( mefnames.hdr, "SKYVAL", INDEF, add+ )
    if (verbose>=1)
        print( "WARNING: Cannot calculate global sky value (no dqskval found)" )
    ;
;

if (numccd5 != 0) {
    eskyval = eskyval / numccd5
    printf("%12.5e\n", eskyval ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqskeavl", value=cast )
} else
    if (verbose>=1)
        print( "WARNING: Cannot calculate global error on sky value (no dqskeval found)" )
    ;
;

if (numccd6 != 0) {
    skymag = skymag / numccd6
    hedit( mefnames.hdr, "SKYMAG", skymag, add+ )
    printf("%12.5e\n", skymag ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqskamag", value=cast )
} else
    hedit( mefnames.hdr, "SKYMAG", INDEF, add+ )
    if (verbose>=1)
        print( "WARNING: Cannot calculate global sky brightness (no dqskmag found)" )
    ;
;

if (numccd7 != 0) {
    depth = depth / numccd7
    hedit (mefnames.hdr, "PHOTDPTH", depth, add+)
    printf("%12.5e\n", depth ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqphadps", value=cast )
} else
    if (verbose>=1)
        print( "WARNING: Cannot calculate global point source depth (no dqphdpps found)" )
    ;
;

if (numccd8 != 0) {
    skynoise = skynoise / numccd8
    hedit( mefnames.hdr, "SKYNOISE", skynoise, add+ )
    printf("%12.5e\n",skynoise  ) | scan( cast )
    setkeyval( class="mefobjectimage", id=mefnames.shortname, dm=dm,
        keyword="dqskasig", value=cast )
} else
    hedit( mefnames.hdr, "SKYNOISE", INDEF, add+ )
    if (verbose>=1)
        print( "WARNING: Cannot calculate global sky noise (no dqsksig found)" )
    ;
;

# Add IMCMBMEF.
hedit (mefnames.hdr, "IMCMBMEF", mefnames.shortname, add+, ver-, show-)

#delete ("mefhdr.tmp")

logout 1
