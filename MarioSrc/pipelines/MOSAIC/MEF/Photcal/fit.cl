#                 Estimating Photometric Zero Point
# 
# X = Z(X) - 2.5 log (F) = wb(X) * B + wv(X) * V + f(B-V)
# Z(X) = <wb(X) * B + wv(X) * V + f(B-V) + 2.5 log (F)>
# 
# Z(X):      zero point for filter X
# 
# F:         measured instrumental flux of source from ACE catalog
# B/V:       magnitudes from the reference catalog (i.e. USNO)
# 
# f(B-V) is a polynomial fit to the Landolt standards to give a rough
# conversion for the reference B and V to the nearest Landolt passband to X.
# For B and V obviously the coefficients are trivial.  For U we use B as
# the reference and for R and I we use V as the reference.
#
# The following script fits:
#
# X = wb*B + wv*V + a + b*(B-V) + c*(B-V)^2 + d*(B-V)^3 + e*(B-V)^4 + f*(B-V)^5
#
# X  wb  wv      a       b       c       d       e       f
# ---------------------------------------------------------
# U  1.  0. -0.1569 1.75773 -6.2406 9.62015 -5.1459 0.91215
# B  1.  0.      0.      0.      0.      0.      0.      0.
# V  0.  1.      0.      0.      0.      0.      0.      0.
# R  0.  1. -0.0138 -0.5458 -0.1785 0.44198 -0.2952 0.05657
# I  0.  1. -0.0208 -1.1758 -0.2917 1.14894 -0.8706  0.1867

real	a, b, c, d, e, f, g, h

i = 6
j = 21 + i
s1 = j//"-"

a = 0; b = 0; c = 0; d = 0; e = 0; f = 0

stty ncols=200
delete ("fit.mc")

# U = B + a + b*(B-V) + c*(B-V)^2 + d*(B-V)^3 + e*(B-V)^4 + f*(B-V)^5
fields ("landolt.dat", "5,6", > "temp")
curfit ("temp", function="legendre", weighting="uniform", order=i,
    interactive=yes, axis=1, listdata=no, verbose=no, calctype="double",
    power=yes, device="stdgraph", cursor="cursor.dat", >>G "fit.mc") |
    translit ("STDIN", "#", "", delete=yes, collapse=no) |
    fields ("STDIN", "2", lines=s1) |
    table ("STDIN", first_col=0, last_col=0, ncols=6, maxstrlen=0) |
    scan (a, b, c, d, e, f)
delete ("temp")
printf ("%s %3.2g %3.2g %7.6g %7.6g %7.6g %7.6g %7.6g %7.6g\n",
    "U", 1., 0., a, b, c, d, e, f)

# B = B
printf ("%s %3.2g %3.2g %7.6g %7.6g %7.6g %7.6g %7.6g %7.6g\n",
    "B", 1., 0., 0., 0., 0., 0., 0., 0.)

# V = V
printf ("%s %3.2g %3.2g %7.6g %7.6g %7.6g %7.6g %7.6g %7.6g\n",
    "V", 0., 1., 0., 0., 0., 0., 0., 0.)

# R = V + a + b*(B-V) + c*(B-V)^2 + d*(B-V)^3 + e*(B-V)^4 + f*(B-V)^5
fields ("landolt.dat", "5,7", > "temp1")
list = "temp1"
while (fscan (list,x,y)!=EOF)
    print (x, -y, >> "temp")
list = ""; delete ("temp1")
curfit ("temp", function="legendre", weighting="uniform", order=i,
    interactive=yes, axis=1, listdata=no, verbose=no, calctype="double",
    power=yes, device="stdgraph", cursor="cursor.dat", >>G "fit.mc") |
    translit ("STDIN", "#", "", delete=yes, collapse=no) |
    fields ("STDIN", "2", lines=s1) |
    table ("STDIN", first_col=0, last_col=0, ncols=6, maxstrlen=0) |
    scan (a, b, c, d, e, f)
delete ("temp")
printf ("%s %3.2g %3.2g %7.6g %7.6g %7.6g %7.6g %7.6g %7.6g\n",
    "R", 0., 1., a, b, c, d, e, f)

# I = V + a + b*(B-V) + c*(B-V)^2 + d*(B-V)^3 + e*(B-V)^4 + f*(B-V)^5
fields ("landolt.dat", "5,9", > "temp1")
list = "temp1"
while (fscan (list,x,y)!=EOF)
    print (x, -y, >> "temp")
list = ""; delete ("temp1")
curfit ("temp", function="legendre", weighting="uniform", order=i,
    interactive=yes, axis=1, listdata=no, verbose=no, calctype="double",
    power=yes, device="stdgraph", cursor="cursor.dat", >>G "fit.mc") |
    translit ("STDIN", "#", "", delete=yes, collapse=no) |
    fields ("STDIN", "2", lines=s1) |
    table ("STDIN", first_col=0, last_col=0, ncols=6, maxstrlen=0) |
    scan (a, b, c, d, e, f)
printf ("%s %3.2g %3.2g %7.6g %7.6g %7.6g %7.6g %7.6g %7.6g\n",
    "I", 0., 1., a, b, c, d, e, f)
delete ("temp")
