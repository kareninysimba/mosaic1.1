# AVGZEROPT -- Averge the zero points across all detectors.
# The input consists of a line begingning with the filter followed
# by 8 zeropoints for each CCD.  The zeropoints are for data in
# ADU/s/pixel and relative to a magnitude of 25.  To convert to zeropoints
# for data in photons/s/arcsec^2 adjust offset.

real	offset, c1, c2, c3, c4, c5, c6, c7, c8

offset = 25 + 2.5 * log10 (2.6)

list = "olsen.dat"

while (fscan (list, s1, c1, c2, c3, c4, c5, c6, c7, c8) != EOF) {
    x = offset - c1
    printf ("%g\n", x) | average > dev$null
    x = offset - c2
    printf ("%g\n", x) | average add > dev$null
    x = offset - c3
    printf ("%g\n", x) | average add > dev$null
    x = offset - c4
    printf ("%g\n", x) | average add > dev$null
    x = offset - c5
    printf ("%g\n", x) | average add > dev$null
    x = offset - c6
    printf ("%g\n", x) | average add > dev$null
    x = offset - c7
    printf ("%g\n", x) | average add > dev$null
    x = offset - c8
    printf ("%g\n", x) | average add > dev$null
    printf ("%s %6.3f (%6.3f)\n", s1, average.mean, average.sigma)
}
list = ""
