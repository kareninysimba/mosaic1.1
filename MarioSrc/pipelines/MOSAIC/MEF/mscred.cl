#{ MSCRED -- Mosaic CCD Reduction Package
# This is a minimal package definition for host CL scripts.
# It is needed in order for package parameters to be found when the
# package prefix is not given; i.e. =instrument.

cl < "mscred$lib/zzsetenv.def"
package mscred, bin = mscbin$

clbye()
