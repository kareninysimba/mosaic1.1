#!/bin/env pipecl
# 
# MEF2DSP -- Call the DSP pipeline.

string  dsp, dataset, datadir, lfile

# Tasks and packages
proto
task pipeselect = "$!pipeselect"

# Set names and directories.
mefnames (envget ("OSF_DATASET"))
dataset = mefnames.dataset
datadir = mefnames.datadir
lfile = mefnames.lfile
set (uparm = mefnames.uparm)
cd (datadir)

# Check if there is a preview file.
if (access(mefnames.gif) == NO)
    logout 1
;

# Log start of module.
printf ("\nMEF2DSP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check if DSP pipeline is running and exit if not.
dsp = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "dsp", 1, 0) | scan (dsp)
if (dsp == "") {
    sendmsg ("WARNING", "No pipeline running", "dsp", "PIPE")
    logout 3
}
;

# Create the file to send to the DSP pipeline.
pathnames (mefnames.lfile, >> dsp//dataset//".dsp")
pathnames (mefnames.hdr, >> dsp//dataset//".dsp")
pathnames (mefnames.gif, >> dsp//dataset//".dsp")
pathnames (mefnames.gif2, >> dsp//dataset//".dsp")
pathnames (mefnames.ogif, >> dsp//dataset//".dsp")
pathnames (mefnames.mgif, >> dsp//dataset//".dsp")
pathnames (mefnames.mgif2, >> dsp//dataset//".dsp")

# Trigger the DSP pipeline.
touch (dsp//dataset//".dsptrig")

logout 1
