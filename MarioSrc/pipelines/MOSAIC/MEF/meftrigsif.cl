#!/bin/env pipecl
#
# MEFtrigsif -- Trigger the header updating in the SIF pipeline
#
# This must be called after all stages which write header information have
# completed.

string	dataset, lfile

# Load packages.
images
servers

# Directories and files.
mefnames (envget("OSF_DATASET"))
dataset = mefnames.dataset
lfile = mefnames.datadir // mefnames.lfile
set (uparm = mefnames.uparm)
cd (mefnames.datadir)

# Log start of processing.
printf ("\nMEFTRIGSIF (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Trigger the SIF pipelines.
match ("hdrtrig", "*.ace", print-, > "meftrigsif.tmp")
concat ("meftrigsif.tmp")
touch ("@meftrigsif.tmp", verbose+)
delete ("meftrigsif.tmp")

logout 1
