#!/bin/env pipecl
#
# MEFCALCK -- Check for calibrations in waiting datasets.
#
# This module finds datasets with the start flag set to 'w'.  It then
# checks if the dataset has calibrations available.  If the calibrations
# are available it retriggers the dataset.  This routine runs
# under a timer.

file	caldir = "MC$"

# Define packages and tasks.  Until the flipper version of osf_test prints
# the desired fields we need to parse the OSF which is somewhat fragile.
task $osf_test = "$!osf_test -a $1 -p mef -c 00 -s w"
servers

cd ("MOSAIC_MEF$/input")

s1 = mktemp ("mefchk")
osf_test (envget ("NHPPS_SYS_NAME"), > s1)

list = s1
while (fscan (list, s2) != EOF) {
    s2 = substr (s2, stridx (".",s2)+1, 1000)
    s2 = substr (s2, 1, strstr ("__", s2)-1)
    head (s2//".mef") | scan (s3)
    if (nscan()==1) {
        getcal (s3//"[1]", "zero", cm, caldir,
            obstype="", detector=instrument, imageid="!ccdname",
	    filter="", exptime="", mjd="!mjd-obs",
	    match="!nextend,ccdsum") | scan (i, line)
        if (i == 0) {
	    getcal (s3//"[1]", "flat", cm, caldir,
	        obstype="", detector=instrument, imageid="!ccdname",
	        filter="!filter", exptime="", mjd="!mjd-obs", dmjdmin="-0.75",
		dmjdmax="0.75", match="!nextend,ccdsum") | scan (i, line)
	    if (i == 0)
	        touch (s2//".meftrig")
	    ;
        }
        ;
    }
}
list = ""; delete (s1)

logout 1
