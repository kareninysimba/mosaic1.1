#!/bin/env pipecl
#
# SIFSETUP -- Setup SIF processing data including calibrations.

int	status = 1
int	founddir
struct	mask_name
string	dataset, indir, datadir, ifile, im, lfile, calops
file	caldir = "MC$"

# Packages and tasks.
task    $cp = "$!cp -r $(1) $(2)"
task    $ln = "$!ln -s $(1) $(2)"
servers
images
utilities
noao
astutil
mscred
task sifsetwcs = "NHPPS_PIPESRC$/MOSAIC/SIF/sifsetwcs.cl"
task mscwcs = "mscsrc$x_mscred.e"

# Set paths and files.
sifnames (envget("OSF_DATASET"))
dataset = sifnames.dataset
indir   = sifnames.indir
datadir = sifnames.datadir
lfile   = datadir // sifnames.lfile
ifile   = indir // dataset // ".sif"

set (uparm = sifnames.uparm)
set (pipedata = sifnames.pipedata)

# Log start of processing.
printf ("\nSIFSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Set secondary return files.
cd (indir//"return")
if (access (dataset//".sif")) {
    head (dataset//".sif") | scan (s1)
    s1 = substr (s1, 1, strstr("/input/",s1)) 
    s2 = dataset // ".ace"
    print (s1//"input/"//s2, > dataset//".ace")
}
;

# Need to be able to access the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Setup data subdirectories.
delete (substr(sifnames.uparm, 1, strlen(sifnames.uparm)-1))
s1 = ""; head (ifile) | scan (s1)
iferr {
    setdirs (s1//"[0]", umatch="!ccdsum")
} then
    logout 0
else
    ;

# Get the calibrations.  There are two types.  The first gets required
# calibrations and the script exits with an error if the calibrations
# are not found.  The second is for optional calibrations.  Only
# warnings are printed in that case.
#
# A revision is now to continue if zero and/or flats are not available.
# Another revision is to use the sky flat if dome/twilight are not
# available.  This is to use the sky flat as a second pass.

list = ifile
while (fscan (list, s1) != EOF) {
    hselect (s1, "CALOPS", yes) | scan (calops)

    # Set zero/dark calibration.
    # Because darks will have preference over zeros we only use the
    # exposure time if cal_dmjd is within 5 days.  This is a hack to
    # allow forcing use of darks upon request.
    getcal.statcode = 1
    if (cal_dmjd <= 5.)
	getcal (s1, "zero", cm, caldir,
	    obstype="!obstype", detector=cl.instrument, imageid="!ccdname",
	    filter="!filter", exptime="!exptime", dexptime=0.1, quality=0.,
	    mjd="!mjd-obs", dmjd=cal_dmjd, match="!nextend,ccdsum")
    if (getcal.statcode != 0)
	getcal (s1, "zero", cm, caldir,
	    obstype="!obstype", detector=cl.instrument, imageid="!ccdname",
	    filter="!filter", exptime="", dexptime=0.1, quality=0.,
	    mjd="!mjd-obs", dmjd=cal_dmjd, match="!nextend,ccdsum")
    ;
    if (getcal.statcode != 0) {
	sendmsg ("WARNING", "Getcal failed for zero",
	    str(getcal.statcode)//" "//getcal.statstr, "CAL")
	print (calops) | translit ("STDIN", "Z", del+) | scan (calops)
	hedit (s1, "CALOPS", calops)
    }
    ;
	
    # Set basic flat: give preference to dome or twilight flats.
    if (stridx ("F", calops) > 0)
	getcal (s1, "_flat asc", cm, caldir,
	    obstype="!obstype", detector=cl.instrument, imageid="!ccdname",
	    filter="!filter", exptime="", quality=0.,
	    mjd="!mjd-obs", dmjd=cal_dmjd, match="!nextend,ccdsum")
    else
	getcal (s1, "_flat desc", cm, caldir,
	    obstype="!obstype", detector=cl.instrument, imageid="!ccdname",
	    filter="!filter", exptime="", quality=0.,
	    mjd="!mjd-obs", dmjd=cal_dmjd, match="!nextend,ccdsum")
    if (getcal.statcode != 0) {
	sendmsg ("WARNING", "Getcal failed for flat",
	    str(getcal.statcode)//" "//getcal.statstr, "CAL")
	print (calops) | translit ("STDIN", "EF", del+) | scan (calops)
	hedit (s1, "CALOPS", calops)
    }
    ;

    # Set detector bad pixel mask.
    getcal (s1, "bpm", cm, caldir, obstype="", detector=cl.instrument,
        imageid="!ccdname", filter="!filter", exptime="", quality=0.,
	mjd="!mjd-obs")
    if (getcal.statcode != 0) {
        sendmsg ("WARNING", "Getcal failed for bpm",
	    str(getcal.statcode)//" "//getcal.statstr, "CAL")
	status = 99
	break
    }
    ;

    # I think this is where there is a problem with getcal values
    # not making it into the headers.  Add flpr/sleep.
    flpr getcal
    flpr hedit
    sleep 1

    hedit (s1, "bpmccd", getcal.value, add+, show-)
}
list = ""

if (status != 1)
    logout (status)
;

# Wait for the FITS kernel.
flpr
sleep 1

# Set the WCS.  and copy the crosstalk masks to the data directory.
# The crosstalk mask has the same name as the MEF dataset it refers to,
# with "_xtm" appended to it and ".pl" extension.  The amplifier number
# (e.g. 01) comes after the "_xtm" string and before the file extension.

list = ifile
while (fscan (list, s1) != EOF) {
    sifsetwcs (s1, ra_shift, dec_shift, rotation) | tee (lfile)
    i = strldx ("_", s1)
    printf ("%sxtm%s\n", substr(s1,1,i), substr(s1,i,100)) | scan (s2)
    if (imaccess (s2))
        imrename (s2, ".")
    ;
}
list = ""

logout (status)
