#!/bin/env pipecl
#
# SIFACE1 -- First ACE pass.

int	status = 1
file	bpm, obm, cat, sky, sig
string	dataset, image, indir, rtnfile, rfile, lfile

real	dqskfrc, dqobmxa, seeingp, mz, p1, p2, dpthadu
real	skyadu, eskyadu, skyxgrad, skyygrad
real	skynoise, eskynoise, sigxgrad, sigygrad
struct	skyline, sigline

# Define packages and tasks.
fitsutil
servers
utilities
proto
tables
ttools
images
noao
nproto
nfextern
ace
dataqual
mario

# For unknown reasons started to get an error that was cleared by unlearning tstat.
# Let's just unlearn the package.

unlearn ttools

# Set directories and files.
sifnames (envget("OSF_DATASET"))
dataset = sifnames.dataset
indir = sifnames.indir
image = sifnames.image
lfile = sifnames.datadir // sifnames.lfile
set (uparm = sifnames.uparm)
set (pipedata = sifnames.pipedata)

cd (sifnames.datadir)

# Log start of processing.
printf ("\nSIFACE1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up.
bpm = sifnames.bpm
sky = sifnames.sky; imdelete (sky, >& "dev$null")
sig = sifnames.sig; imdelete (sig, >& "dev$null")
obm = sifnames.obm; imdelete (obm, >& "dev$null")
cat = sifnames.cat; delete (cat, >& "dev$null")
delete ("siface[^_]*")

hselect (image, "DQREJ", yes) | scan (skyline)
if (skyline == "") {
    # Detect and catalog sources, make sky and sigma maps.
    aceall (image, masks=bpm, skyotype="subsky", skies=sky,
	sigmas=sig, objmasks=obm, catalogs=cat, skyimage="",
	exps="", gains="", omtype="all", extnames="",
	catdefs="pipedata$siface.def", catfilter="", logfiles=lfile,
	verbose=2, order="", nmaxrec=INDEF, gwtsig=INDEF, gwtnsig=INDEF,
	fitstep=100, fitblk1d=10, fithclip=3.,
	fitlclip=4., fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
	blksize=-10, blknsubblks=2, updatesky=yes, bpdetect="3",
	bpflag="1-10", convolve="", hsigma=3., lsigma=10., hdetect=yes,
	ldetect=no, neighbors="8", minpix=6, sigavg=4., sigmax=4.,
	bpval=INDEF, splitmax=INDEF, splitstep=0.4, splitthresh=5.,
	sminpix=8, ssigavg=10., ssigmax=5., ngrow=5, agrow=2.,
	magzero="!MAGZREF")
    rename (cat//".txt", cat)
} else
    printf ("DQREJ = '%s'\n", skyline)

# Determine data quality measures.
dqskfrc = INDEF
if (imaccess(obm//"[pl]")) {
    imrename (obm, "siface1")
    imexpr ("(b==4||b==5)?max(a,b):a", obm//"[pl,type=mask]",
        "siface1[pl]", bpm, verbose-)
    imdelete ("siface1")
    imstat (obm//"[pl]", fields="npix", format-) | scan (x)
    imstat (obm//"[pl]", fields="npix", format-, lower=2.5, upper=3.5) |
        scan (z)
    imstat (obm//"[pl]", fields="npix", format-, upper=0.5) | scan (y)
    x = max (1.,x-z)
    printf ("%.3f\n", y/x) | scan (dqskfrc)
} else  {
    sendmsg ("WARNING", "Object mask not created", image, "PROC")
    status = 2
}

seeingp = INDEF; dqobmxa = INDEF
if (access(cat)) {
    thselect (cat, "FWHM", yes) | scan (seeingp)
    tstat (cat, "NPIX", >> lfile)
    dqobmxa = tstat.vmax
} else {
    sendmsg ("WARNING", "Catalog not created", image, "PROC")
    status = 2
}

skyline = "INDEF"
skyadu = INDEF; eskyadu = INDEF; skyxgrad = INDEF; skyygrad = INDEF
if (imaccess(sky)) {
    listpix (sky, wcs="physical", verbose-) |
	surfit ("STDIN", func="chebyshev", xorder=2, yorder=2, xterms="half",
	    weight="user") |
	fields ("STDIN", "3-4", lines="16-18", > "siface1.tmp")
    list = "siface1.tmp"
    i = fscan (list, skyline)
    i = fscan (skyline, skyadu, eskyadu)
    i = fscan (list, skyxgrad)
    i = fscan (list, skyygrad)
    list = ""; delete ("siface1.tmp")
} else {
    sendmsg ("WARNING", "Sky map not created", image, "PROC")
    status = 2
}

sigline = "INDEF"
skynoise = INDEF; eskynoise = INDEF; sigxgrad = INDEF; sigygrad = INDEF
if (imaccess(sig)) {
    listpix (sig, wcs="physical", verbose-) |
	surfit ("STDIN", func="chebyshev", xorder=2, yorder=2, xterms="half",
	    weight="user") |
	fields ("STDIN", "3-4", lines="16-18", >> "siface1.tmp")
    list = "siface1.tmp"
    i = fscan (list, sigline)
    i = fscan (sigline, skynoise, eskynoise)
    i = fscan (list, sigxgrad)
    i = fscan (list, sigygrad)
    list = ""; delete ("siface1.tmp")
} else {
    sendmsg ("WARNING", "Sigma map not created", image, "PROC")
    status = 2
}

dpthadu = INDEF
hselect (image, "MAGZREF,PIXSCAL1,PIXSCAL2", yes) | scan (mz, p1, p2)
if (skynoise>0. &&isindef(seeingp)==NO && isindef(skynoise)==NO && nscan()==3) {
    # Set photdth as 5 sigma detection with an optimal aperture of
    # 1.35 FWHM (which is formally optimal for pure Gaussian FWHM).
    # Later an average will define the global photometric depth and
    # adjust the zero point.

    photdpth (skynoise, seeingp, mz, sqrt(p1*p2), verbose=0)
    printf ("%.5g\n", photdpth.dpthadu) | scan (dpthadu)
}
;

# Update image header.
if (imaccess(image)) {
    if (imaccess(sky))
	nhedit (image, "SKYIM", sky, "Sky map", add+)
    ;
    if (imaccess(sig))
	nhedit (image, "SIGIM", sig, "Sigma map", add+)
    ;
    if (isindef(seeingp) == NO)
	nhedit (image, "SEEINGP1", seeingp, "[pix] seeing", add+)
    ;
    if (isindef(dpthadu) == NO)
        nhedit (image, "DPTHADU1", dpthadu, "Photometric depth (BUNIT)", add+)
    ;
    if (isindef(dqskfrc) == NO)
	nhedit (image, "DQSKFRC", dqskfrc, "Frac of pixels which are sky",
	    add+)
    ;
    if (isindef(dqobmxa) == NO)
	nhedit (image, "DQOBMXA", dqobmxa, "Max num of pixels in one object",
	    add+)
    ;
    if (skyline != "INDEF") {
	nhedit (image, "SKY", skyline, "Mean and StdDev of sky (BUNIT)", add+)
	nhedit( image, "SKYADU1", skyadu, "Mean sky (BUNIT)", add+)
	nhedit (image, "SKYXGRAD", skyxgrad, "Sky X gradient (BUNIT/pix)", add+)
	nhedit (image, "SKYYGRAD", skyygrad, "Sky Y gradient (BUNIT/pix)", add+)
    }
    ;

    if (sigline != "INDEF") {
	nhedit (image, "SIG", sigline, "Mean and StdDev of sky sigma (BUNIT)",
	    add+)
	nhedit( image, "SKYNOIS1", skynoise, "Mean sky noise (BUNIT)", add+)
	nhedit (image, "SIGXGRAD", sigxgrad, "Sky sigma X gradient (BUNIT/pix)",
	    add+)
	nhedit (image, "SIGYGRAD", sigygrad, "Sky sigma Y gradient (BUNIT/pix)",
	    add+)
    }
    ;
}
;

# Update catalog header.  This is used by the MEF DQ stages for global values.
if (access(cat)) {
    if (isindef(seeingp) == NO)
	thedit (cat, "SEEINGP1", seeingp, show-)
    ;
    if (isindef(dpthadu) == NO)
        thedit (cat, "DPTHADU1", dpthadu, show-)
    ;
    if (isindef(dqskfrc) == NO)
	thedit (cat, "DQSKFRC", dqskfrc, show-)
    ;
    if (isindef(dqobmxa) == NO)
	thedit (cat, "DQOBMXA", dqobmxa, show-)
    ;
    if (skyline != "INDEF") {
	thedit (cat, "SKY", skyline, show-)
	thedit( cat, "SKYADU1", skyadu, show-)
	thedit (cat, "SKYXGRAD", skyxgrad, show-)
	thedit (cat, "SKYYGRAD", skyygrad, show-)
    }
    ;

    if (sigline != "INDEF") {
	thedit (cat, "SIG", sigline, show-)
	thedit( cat, "SKYNOIS1", skynoise, show-)
	thedit (cat, "SIGXGRAD", sigxgrad, show-)
	thedit (cat, "SIGYGRAD", sigygrad, show-)
    }
    ;
}
;

# Send DQ to PMASS.
if (dm != "") {
    if (isindef(seeingp) == NO) {
        printf ("%.2f\n", seeingp) | scan (s1)
	setkeyval (dm, "objectimage", image, "dqseamp", s1)
    }
    ;
    if (isindef(dqskfrc) == NO) {
	printf ("%.3f\n", dqskfrc) | scan (s1)
	setkeyval (dm, "objectimage", image, "dqskfrc", s1)
    }
    ;
    if (isindef(dqobmxa) == NO) {
	printf ("%d\n", dqobmxa) | scan (s1)
	setkeyval (dm, "objectimage", image, "dqobmxa", s1)
    }
    ;
    if (skyline != "INDEF") {
	i = fscan (skyline, s1, s2)
	setkeyval (dm, "objectimage", image, "dqskval", s1 )
	setkeyval (dm, "objectimage", image, "dqskeval", s2 )
	printf("%.4g\n",skyxgrad) | scan( s1 )
	setkeyval (dm, "objectimage", image, "dqskvxgr", s1 )
	printf("%.4g\n",skyygrad) | scan( s1 )
	setkeyval (dm, "objectimage", image, "dqskvygr", s1 )
    }
    ;
    if (sigline != "INDEF") {
	i = fscan (sigline, s1, s2)
	setkeyval (dm, "objectimage", image, "dqsksig", s1 )
	setkeyval (dm, "objectimage", image, "dqskesig", s2 )
	printf("%.4g\n",sigxgrad) | scan( s1 )
	setkeyval (dm, "objectimage", image, "dqsksxgr", s1 )
	printf("%.4g\n",sigygrad) | scan( s1 )
	setkeyval (dm, "objectimage", image, "dqsksygr", s1 )
    }
    ;
}
;

# Set error flag.
if (status != 1) {
    line = ""; hselect (image, "SIFERR", yes) | scan (line)
    line = line // "A"
    nhedit (image, "SIFERR", line, ".", add+, show+)
}
;

# Return results.
rtnfile = indir // "return/" // dataset // ".ace"
if (access (rtnfile)) {
    printf ("%s\n", rtnfile)
    type (rtnfile)
    head (rtnfile, nlines=1) | scan (rfile)
    pathnames (indir//dataset//".hdrtrig", > dataset//".ace")
    pathnames ("*cat*,*sky*,*sig*,*obm*", sort-, >> dataset//".ace")
    copy (dataset//".ace", rfile); delete (dataset//".ace")
    touch (rfile//"trig")
    delete (rtnfile)
}
;

logout (status)
