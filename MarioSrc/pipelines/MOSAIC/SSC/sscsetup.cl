#!/bin/env pipecl
#
# SSCSETUP -- Setup SSC processing. 

int	founddir
string	dataset, indir, datadir, ilist, lfile

# Tasks and packages.
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Files and directories.
names ("ssc", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ssc"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nSSCSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup uparm directory from header of first file.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s2 = substr (dataset, 1, strldx("s",dataset)-1)
s1 = ""; match (s2//"_s.fits", ilist) | scan (s1)
if (s1 == "")
    match (s2//".fits", ilist) | scan (s1)
;
iferr {
    setdirs (s1)
} then {
    logout 0
} else
    ;

logout 1
