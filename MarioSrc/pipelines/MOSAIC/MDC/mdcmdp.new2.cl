#!/bin/env pipecl
#
# MDCMDP -- Make stacked data product.

# Declare variables
string  dataset, indir, datadir, ilist, lfile, pnga, pngb
struct	title,obstype,proctype

# Load packages
task $mkgraphic = "$!mkgraphic"
task $convert = "$!convert"
task $plver = "$!plver"
task mdphdr = "NHPPS_PIPESRC$/MOSAIC/MDP/mdphdr.cl"
task mdprwcs = "NHPPS_PIPESRC$/MOSAIC/MDP/mdprwcs.cl"
fitsutil
images
servers
utilities
proto
noao
artdata

# Set file and path names
mdcnames (envget("OSF_DATASET"), pass=2)
dataset = mdcnames.dataset
indir = mdcnames.indir
datadir = mdcnames.datadir
ilist = mdcnames.ilist
lfile = mdcnames.lfile
pnga = "png1"
pngb = "png2"
set (uparm=mdcnames.uparm)
set (pipedata=mdcnames.pipedata)

cd (datadir)

# Log start of processing.
printf ("\nMDCMDP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

proctype = "Stacked"; obstype = "object"

# Primary science image ---------------

mdphdr (mdcnames.seqlabel, proctype, obstype, "")
mdprwcs (mdcnames.seqlabel)
hedit (mdcnames.seqlabel, "DQMASK", mdcnames.bpm//"[pl]",add+)
nhedit (mdcnames.seqlabel, comfile="pipedata$mdcstack.nhedit")

# Update the title to remove mosdither component.
hselect (mdcnames.seqlabel, "$OBJECT", yes) | scan (title)
i = strstr (" - DelRA", title)
if (i > 0) {
    title = substr (title, 1, i-1)
    hedit (mdcnames.seqlabel, "OBJECT", title)
}
;

# Set the exposures combined.
# In rare cases the number combined is larger than a image header
# keyword template allows.  So use lists and loops.

imhead (mdcnames.seqlabel, l+) | match ("IMCMB") |
    translit ("STDIN", "='", del+, > "mdcmdp.tmp")
list = "mdcmdp.tmp"
for (i=0; fscan(list,s1)!=EOF; )
    hedit (mdcnames.seqlabel, s1, del+, show-)
list = ""
flpr

fields ("mdcmdp.tmp", 2) | sort | unique (> "mdcmdp.tmp")
list = "mdcmdp.tmp"
for (i=0; fscan(list,s1)!=EOF; ) {
    i += 1
    printf ("IMCMB%03d\n", i) | scan (s2)
    nhedit (mdcnames.seqlabel, s2, s1, "Contributing exposure", add+, show+)
}
list = ""; delete ("mdcmdp.tmp")
nhedit (mdcnames.seqlabel, "NCOMBINE", i, "Number stacked", add+, show+)
nhedit (mdcnames.seqlabel, "EXPTIME", "(EXPTIME*"//i//")", "Total exposure",
    add+, show+)
flpr

# Place holders for mdpstb to workaround remote header expansion problem.
hedit (mdcnames.seqlabel, "PLOFNAME", "", add+)
hedit (mdcnames.seqlabel, "PLQUEUE", "", add+)
hedit (mdcnames.seqlabel, "PLQNAME", "", add+)
hedit (mdcnames.seqlabel, "PLPROCID", "", add+)
hedit (mdcnames.seqlabel, "PLFNAME", "", add+)

# Data quality mask ---------------

imrename (mdcnames.bpm, "mdcmdp_bpm")
mkglbhdr (mdcnames.seqlabel, mdcnames.bpm)
nhedit ("mdcmdp_bpm", "*", del+, >& "dev$null")
nhedit (mdcnames.bpm, "OBJECT", "Mask for "//mdcnames.seqlabel, ".", add+)
nhedit (mdcnames.bpm, "PRODTYPE", "dqmask", ".", add+)
nhedit (mdcnames.bpm, "DQMFOR", mdcnames.seqlabel, "Data for which this mask applies", add+)
imcopy ("mdcmdp_bpm", mdcnames.bpm//"[pl,type=mask,append,inherit]", verbose-)
imdelete ("mdcmdp_bpm")

# Exposure map --------------

imrename (mdcnames.expmask, "mdcmdp_expmask")
mkglbhdr (mdcnames.seqlabel, mdcnames.expmask)
hselect ("mdcmdp_expmask", "$MASKZERO,$MASKSCAL", yes) | scan (x, y)
nhedit ("mdcmdp_expmask", "*", del+, >& "dev$null")
if (isindef(x)==NO)
    nhedit ("mdcmdp_expmask", "MASKZERO", x, "Conversion to exposure time")
;
if (isindef(y)==NO)
    nhedit ("mdcmdp_expmask", "MASKSCAL", y, "Conversion to exposure time")
;
nhedit (mdcnames.expmask, "OBJECT", "Exposure map for "//mdcnames.seqlabel,
    ".", add+)
nhedit (mdcnames.expmask, "PRODTYPE", "expmap", ".", add+)
nhedit (mdcnames.expmask, "EXPMFOR", mdcnames.seqlabel,
    "Data for which this map applies", add+)
imcopy ("mdcmdp_expmask", mdcnames.expmask//"[pl,type=mask,append,inherit]",
    verbose-)
imdelete ("mdcmdp_expmask")

# Graphic files ------------


mkgraphic (mdcnames.seqlabel, mdcnames.png1, "none", "png", png_blk, "'i1!=0'")
convert ("-resize "//png_pix, mdcnames.png1, mdcnames.png2)
# Encapsulate pngs into fits:
#fgwrite (mdcnames.png1//" "//mdcnames.png2, "mdcmdp_png.fits",
#     group="pipeline", checksum+)
fgwrite (mdcnames.png1, "mdcmdp_png1.fits", group="pipeline", checksum+)
fgwrite (mdcnames.png2, "mdcmdp_png2.fits", group="pipeline", checksum+)

# Edit encansulated large png header:
mkglbhdr (mdcnames.seqlabel, mdcnames.pnga)
##nhedit (mdcnames.png//"[0]", "prodtype", "png", ".", add+)
nhedit (mdcnames.pnga//"[0]", "prodtype", "png1", ".", add+)
nhedit (mdcnames.pnga//"[0]", "mimetype", "image/png", ".", add+)
##fxcopy ("mdcmdp_png.fits", mdcnames.png//".fits", groups="1", new_file-)
fxcopy ("mdcmdp_png1.fits", mdcnames.pnga//".fits", groups="1", new_file-)
# Edit encansulated small png header:
mkglbhdr (mdcnames.seqlabel, mdcnames.pngb)
##nhedit (mdcnames.png//"[0]", "prodtype", "png", ".", add+)
nhedit (mdcnames.pngb//"[0]", "prodtype", "png2", ".", add+)
nhedit (mdcnames.pngb//"[0]", "mimetype", "image/png", ".", add+)
##fxcopy ("mdcmdp_png.fits", mdcnames.png//".fits", groups="1", new_file-)
fxcopy ("mdcmdp_png2.fits", mdcnames.pngb//".fits", groups="1", new_file-)


##imdelete ("mdcmdp_png.fits",verify-)
#imdelete ("mdcmdp_png1.fits",verify-)
#imdelete ("mdcmdp_png2.fits",verify-)
#delete (mdcnames.png1,verify-)
#delete (mdcnames.png2,verify-)

logout 0
