#!/bin/env pipecl
#
# MDCsetup1
#
# Description:
#
# 	This module copies the badpixel masks to the current directory.
#
# Exit Status Values:
#
# 	1 = Successful
#	2 = Successful, but indicates that single-pass stacking should be performed
#	
# History:
# 
# 	T. Huard  20080408  Created.	
#	T. Huard  20080409  Backup of ilist is made since ilist is modified at the end of the module.
#	T. Huard  20080815  Changed the format of ilist to be a single column list instead of a double
#				column list, where the second column was the full BPM pathname. 
#	T. Huard  20080828  Added information to log about number of bpms expected but not copied.
#	T. Huard  20080911  Added check for number of images (nimages) and comparison with mdc_n2pass,
#				and check for value of str_crrej.  Exit status of 2 now occurs when
#				single-pass stacking should be performed.  Single-pass stacking should
#				be performed if either (nimages < mdc_n2pass) or (str_trrej = "none").
#				Last, using mdcnames.cl now to help define names in this module. 
# 	Last Revised: T. Huard  20080911  1:15pm

# Declare variables
int	exitval,nimages,icount,icountBAD
string  dataset,indir,datadir,ilist,lfile
string  bpmFILE,dirFULL

# Load packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers
proto

# Set file and path names
mdcnames(envget("OSF_DATASET"), pass=1)
	print("dataset in module: "//mdcnames.dataset)
	print("datadirFULL in module: "//mdcnames.datadirFULL)
	print("seqlabel in module: "//mdcnames.seqlabel)
	print("bpm in module: "//mdcnames.bpm)
	print("expmask in module: "//mdcnames.expmask)
	print("png in module: "//mdcnames.png)
	print("png1 in module: "//mdcnames.png1)
	print("png2 in module: "//mdcnames.png2)
	
dataset=mdcnames.dataset
indir=mdcnames.indir
datadir=mdcnames.datadir
ilist=mdcnames.ilist
lfile=mdcnames.lfile
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nMDCsetup1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Setup data directories.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
iferr {
    setdirs ("@"//ilist)
} then {
    logout 0
} else
    ;

# Initialize exit status to 1 (successful)
exitval=1

# Copy the badpixel masks to the current directory.
icount=0
icountBAD=0
list=ilist
while (fscan(list,s1) != EOF) {
	bpmFILE="" ; hselect(s1,"BPM",yes) | scan(bpmFILE)
	if (bpmFILE != "") {
		if (access(bpmFILE)) delete(bpmFILE,verify-)
		;
		dirFULL=substr(s1,1,strldx("/",s1))
		imcopy(dirFULL//bpmFILE,bpmFILE,verbose-)
		if (access(bpmFILE)) icount=icount+1
		else icountBAD=icountBAD+1
	} else {
		printf("%s\n","WARNING: No valid BPM keyword value found in header of stack of SIFs: "//bpmFILE) | tee(lfile)
	}
}
list=""

# Check number of images to be stacked, in order to determine stacking method.
# Write some general information to the log file
count(ilist) | scan(nimages)	
printf("%s\n","MDCsetup1: Number of images in stacking pipeline: "//str(nimages)) | tee(lfile)
printf("%s\n","MDCsetup1: Number of badpixel masks copied: "//str(icount)) | tee(lfile)
if (icountBAD >= 1) {
	printf("%s\n","MDCsetup1: Number of badpixel masks EXPECTED BUT NOT FOUND: "//str(icountBAD)) | tee(lfile)
	printf("%s\n","***************** SOME BPMs NOT FOUND!!! *****************") | tee(lfile)
}	
;
if ((nimages < mdc_n2pass) || (str_trrej == "none")) {
	exitval=2
	printf("%s\n","MDCsetup1: SINGLE-PASS STACKING WILL BE DONE") | tee(lfile)
} else {
	printf("%s\n","MDCsetup1: TWO-PASS STACKING WILL BE DONE") | tee(lfile)
}

logout(exitval)
