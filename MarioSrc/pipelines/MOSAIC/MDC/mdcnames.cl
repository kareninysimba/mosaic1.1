# MDCNAMES -- Directory and filenames for the MDC pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.
#
# History:
#
# 	T. Huard 20080906  Created.
#	Last Revised:  T. Huard  20080912  7:00pm

procedure mdcnames (name)

string	name			{prompt = "Name"}
int	pass=2			{prompt = "Pass (1 or 2)"}

string	dataset			{prompt = "Dataset name"}
string	shortname		{prompt = "Short name"}
string	indir			{prompt = "Input directory"}
string	datadir			{prompt = "Dataset directory"}
string	uparm			{prompt = "Uparm directory"}
string	pipedata		{prompt = "Pipeline data directory"}
string	ilist			{prompt = "Input list"}
string	lfile			{prompt = "Log file"}

string	datadirFULL		{prompt = "Full explicit data directory path"}
string	seqlabel		{prompt = "Sequence (stack) label"}
string	bpm			{prompt = "BPM of stack"}
string	expmask			{prompt = "Exposure mask of stack"}
string	png			{prompt = "PNG of stack"}
string	png1			{prompt = "PNG1 of stack"}
string	png2			{prompt = "PNG2 of stack"}
string	pnga			{prompt = "PNGa of stack"}
string	pngb			{prompt = "PNGb of stack"}

begin
	int	p

	# Set generic names, from names.cl
	names ("mdc", name)
	dataset = names.dataset
	shortname = names.shortname
	indir = names.indir
	datadir = names.datadir
	uparm = names.uparm
	pipedata = names.pipedata	
	
	# Modify generic names and set pipeline specific names.	
	ilist = indir//dataset//".mdc"
	lfile = datadir//names.lfile
	datadirFULL="" ; pathnames(datadir) | scan(datadirFULL)
	p = pass
	if ((p == 1) || (p == 2)) {
	    if (p == 1)
		seqlabel = names.shortname//"_sk1"
	    else
		seqlabel = names.shortname//"_stk"
	    bpm = seqlabel//"_bpm"
	    expmask = seqlabel//"_expmask"
	    png = seqlabel//"_png"
	    if (png_blk == 1)
		png1 = seqlabel//".png"
	    else
		png1 = seqlabel//"_x"//png_blk//".png"
	    png2 = seqlabel//"_"//png_pix//".png"
	    pnga = seqlabel//"_a.png"
	    pngb = seqlabel//"_b.png"
	} else
	    printf("ERROR: mdcnames: pass must be 1 or 2.")

end
