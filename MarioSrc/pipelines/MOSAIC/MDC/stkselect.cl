# SFTSELECT -- Select sky stack images and set scale factors.
# The scale factors are based on the SKYMEAN values.
# Note this has output parameters and so the task should be cached.

procedure stkselect (flagkwd, input, output, scales)

string	flagkwd			{prompt="Selection flag keyword"}
file	input			{prompt="File of input images"}
file	output			{prompt="File of selected images"}
file	scales			{prompt="File of scale factors"}
string	flagdef = "Y"		{prompt="Default missing flag value?"}
bool	ckflag = yes		{prompt="Check flag keyword?"}
int	nselect			{prompt="Number selected"}
int	nforced			{prompt="Number of forced selections"}
struct	*fd

begin
	file	in, out, scl, im, temp
	string	flgkwd, flag
	real	skymean, magzero, scale, I0

	# Get parameters.
	flgkwd = flagkwd
	in = input
	out = output
	scl = scales
	if (out == "STDOUT")
	    out = ""
	if (scl == "STDOUT")
	    scl = ""

	# Initialize.
	temp = mktemp ("stkselect")
	if (out != "") {
	    if (access(out))
		delete (out)
	    touch (out)
	}
	if (scl != "") {
	    if (access(scl))
		delete (scl)
	    touch (scl)
	}
	nselect = 0
	nforced = 0

	# Get data.
	hselect ("@"//in, "$I,$SKYMEAN,$MAGZERO,$"//flgkwd, yes,
	    missing="INDEF") | translit ("STDIN", '"', delete+, > temp)

	# Check images.
	fd = temp; I0 = INDEF
	while (fscan (fd, im, skymean, magzero, flag) != EOF) {

	    # If scale output is requested then skip if metadata is missing.
	    if ((isindef(skymean) || isindef(magzero)) && scl != "")
	        next

	    # Set the default value for the flag.
	    if (flag == "INDEF")
	        flag = flagdef

	    # Check the flag.
	    if (flag == "Y!" || flag == "N!")
		nforced += 1
	    if (ckflag && substr(flag,1,1) == "N")
	        next
	    nselect += 1

	    # Compute the scales if desired.
	    if (!(isindef(skymean) || isindef(magzero))) {
		if (isindef(I0))
		    I0 = 10. ** (0.4 * magzero)
		scale = I0 / (10. ** (0.4 * magzero))
		skymean = -skymean

		# Output the results.
		if (out == scl) {
		    if (out == "")
			printf ("%s %g %g %g\n", im, scale, skymean, magzero)
		    else
			printf ("%s %g %g %g\n", im, scale, skymean, magzero, >> out)
		} else {
		    if (out == "")
			printf ("%s\n", im)
		    else
			printf ("%s\n", im, >> out)
		    if (scl == "")
			printf ("%g %g %g\n", scale, skymean, magzero)
		    else
			printf ("%g %g %g\n", scale, skymean, magzero, >> scl)
		}
	    } else {
		# Output the results.
		if (out == "")
		    printf ("%s\n", im)
		else
		    printf ("%s\n", im, >> out)
	    }
	}
	fd = ""; delete (temp, verify-)
end
