#!/bin/env pipecl
#
# MDCSTACK2 -- Second-pass stacking of SIFs from STR.
# This is an average stack using masks to eliminate transients.

# Declare variables
int	nimages
string  dataset, indir, datadir, ilist, lfile
string	rspord, filename, magzero, magzero0
string  trrej, listfile
real	scale, sky

# Load packages
images
servers
utilities
proto
task stkselect = "NHPPS_PIPEAPPSRC$/MDC/stkselect.cl"

# Set file and path names
mdcnames (envget("OSF_DATASET"), pass=2)
dataset = mdcnames.dataset
indir = mdcnames.indir
datadir = mdcnames.datadir
ilist = mdcnames.ilist
lfile = mdcnames.lfile
set (uparm=names.uparm)
set (pipedata=names.pipedata)

cd (datadir)
delete ("mdcstack*.tmp")

# Log start of processing.
printf ("\nMDCSTACK2 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Check whether single-pass or two-pass stacking is being performed.
# For single-pass to be performed instead of two-pass, one of two criteria
# should be met: (1) the number of images is less than mdc_n2pass, or (2)
# str_crrej is "none".  If single-pass, then ilist is the list of resampled
# SIFs to be stacked.  If two-pass, then search list file in datadir to
# obtain the full pathnames of the new resampled SIFs created by STR.

count(ilist) | scan(nimages)
if ((nimages < mdc_n2pass) || (str_trrej == "none")) {
    trrej = str(srs_trrej) // "+none"
    copy (ilist, "mdcstack2.list")
} else {
    trrej = str(srs_trrej) // "+" // str(str_trrej)
    match ("/MOSAIC_STR/data/", datadir//dataset//".list") |
        match("_r.fits", >"mdcstack2.list")
}

# Read the headers of the stacked SIFs in order to run some basic checks and
# help with stacking properly.  Now, this file is created for diagnostic
# purposes only since the stacks of resampled SIFs that will be combined
# in MDC have been scaled and zero'd appropriately in MDS.

# Compute the relative scaling factors (derived from MAGZERO values) and
# zeros (additive shifts derived from the SKYMEAN values) for the resampled
# SIFs and write these to their FITS headers (create new keywords MDCSKY2
# and MDCSCALE2).  In this way, imcombine will be able to use these values to
# properly scale and shift the individual resampled SIFs.  IMPORTANT NOTE:
# The SKYMEAN values in the SIF headers are negative those necessary to be
# useful for imcombine to subtract the sky before stacking the SIFs.  Also,
# in imcombine, the scaling is applied before the zero (see documentation
# for imcombine).  However, the task accounts for the change in zero
# (new zero is -SKYMEAN*SCALE) resulting from rescaling.  So, the effect
# of scale on the zero should not be explicitly done before inputting the
# scales into imcombine.

stkselect ("STKFLAG", "mdcstack2.list", "mdcstack1.tmp", "mdcstack1.tmp")
if (stkselect.nselect == 0)
    logout (0)
;

list = "mdcstack1.tmp"; delete ("mdcstack2.list")
while (fscan(list,filename,scale,sky,magzero) != EOF) {
    if (nscan() < 4)
        next
    ;
    hedit (filename, "MDCSCALE2", scale, add+)
    hedit (filename, "MDCSKY2", sky, add+)
    print (filename, >> "mdcstack2.list")
    if (abs(scale - 1.0) < 0.001)
        magzero0 = magzero
    ;
}
list = ""; delete ("mdcstack1.tmp")


# Combine the resampled SIFs in the sequence, using the WCS information
# included in their headers to align the images.  The images are scaled by
# multiplicative factors, previously determined, to account for changes in
# the transparency.  Also, the images are adjusted by additive constants,
# previously determined, to account for changes in the background sky levels
# during the dither sequence.  In combining, the AVERAGE of pixel values
# in a stacked pixel column is used, after rejecting the pixels identified
# in the bad pixel masks, which include pixels affected by cosmic rays,
# artifacts, and transients since STR has been run.

imdelete (mdcnames.seqlabel, >& "dev$null")
imdelete (mdcnames.bpm, >& "dev$null")
imdelete (mdcnames.expmask, >& "dev$null")
count ("mdcstack2.list") | scan (i)
if (i > 480) {
    list = "mdcstack2.list"
    while (fscan(list,s1)!=EOF) {
        s2 = substr (s1, strldx("/",s1)+1, 999)
	s2 = substr (s2, 1, strstr("-ccd",s2)-1)
	print (s2, >> "mdcstack1.tmp")
    }
    list = ""
    sort ("mdcstack1.tmp") | unique (> "mdcstack1.tmp")
    list = "mdcstack1.tmp"
    while (fscan(list,s1)!=EOF) {
	if (imaccess(s1)==YES) {
	    imdelete (s1)
	    imdelete (s1//"_bpm.pl")
	}
	;
        match (s1, ilist, > "mdcstack2.tmp")
	imcombine ("@mdcstack2.tmp", s1,
	    bpmasks=s1//"_bpm.pl", headers="", rejmask="",
	    nrejmask="", expmask="", imcmb="",
	    logfile=lfile, combine="average", reject="none",
	    project-, outtype="real", outlimits="", offsets="wcs",
	    masktype="novalue", maskvalue=1, blank=0, scale="none",
	    zero="none", weight="none", statsec="", expname="",
	    lthresh=INDEF, hthresh=INDEF)
	flpr imcombine
	print (s1, >> "mdcstack3.tmp")
	print (s1, >> "mdcstack4.tmp")
	print (s1//"_bpm.pl", >> "mdcstack4.tmp")
    }
    list = ""; delete ("mdcstack1.tmp,mdcstack2.tmp")

} else
    copy ("mdcstack2.list", "mdcstack3.tmp")

imcombine("@mdcstack3.tmp", mdcnames.seqlabel, bpmasks=mdcnames.bpm//".pl",
    headers="", rejmask="", nrejmas="", expmask=mdcnames.expmask//".pl",
    imcmb="IMCMBR", logfile=lfile, combine="average",  reject="none",
    project-, outtype="real", outlimits="", offsets="wcs",
    masktype="novalue", maskvalue=1, blank=0, scale="!MDCSCALE2",
    zero="!MDCSKY2", weight="none", statsec="", expname="EXPTIME",
    lthresh=INDEF, hthresh=INDEF)

if (imaccess(mdcnames.seqlabel)==NO)
    logout 0
;

# Set headers.
hedit (mdcnames.seqlabel, "SKYMEAN", 0.00, add+)
hedit (mdcnames.seqlabel, "MAGZERO", magzero0, add+)
hedit (mdcnames.seqlabel, "TRREJECT", trrej, add+)

# Clean up.
if (access("mdcstack4.tmp"))
    imdelete ("@mdcstack4.tmp")
;
delete ("mdcstack2.list,mdcstack*.tmp")

logout 1
