#!/bin/env pipecl
#
# MDCSTACK1 -- First-pass stacking of SIFs from SRS.
# This is a harsh (median) stack to eliminate transients.

# Declare variables
string  dataset, indir, datadir, ilist, lfile
string	filename, sky, magzero, magzero0
real	I0, scale

# Load packages
images
servers
utilities
proto
task stkselect = "NHPPS_PIPEAPPSRC$/MDC/stkselect.cl"

# Set file and path names
mdcnames (envget("OSF_DATASET"),  pass=1)
dataset = mdcnames.dataset
indir = mdcnames.indir
datadir = mdcnames.datadir
ilist = mdcnames.ilist
lfile = mdcnames.lfile
set (uparm=mdcnames.uparm)
set (pipedata=mdcnames.pipedata)

cd (datadir)
delete ("mdcstack*.tmp")

# Log start of processing.
printf ("\nMDCSTACK1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Compute the relative scaling factors (derived from MAGZERO values) and
# zeros (additive shifts derived from the SKYMEAN values) for the resampled
# SIFs and write these to their FITS headers (create new keywords MDCSKY1
# and MDCSCALE1).  In this way, imcombine will be able to use these values to
# properly scale and shift the individual resampled SIFs.  IMPORTANT NOTE:
# The SKYMEAN values in the SIF headers are negative those necessary to be
# useful for imcombine to subtract the sky before stacking the SIFs.  Also,
# in imcombine, the scaling is applied before the zero (see documentation
# for imcombine).  However, the task accounts for the change in zero
# (new zero is -SKYMEAN*SCALE) resulting from rescaling.  So, the effect
# of scale on the zero should not be done explicitly before inputting the
# scales into imcombine.

stkselect ("STKFLAG", ilist, "mdcstack1.tmp", "mdcstack1.tmp")
if (stkselect.nselect == 0)
    logout (0)
;

list = "mdcstack1.tmp"; delete (ilist)
while (fscan(list,filename,scale,sky,magzero) != EOF) {
    if (nscan() < 4)
        next
    ;
    hedit (filename, "MDCSCALE1", scale, add+)
    hedit (filename, "MDCSKY1", sky, add+)
    print (filename, >> ilist)
    if (abs(scale - 1.0) < 0.001)
        magzero0 = magzero
    ;
}
list = ""; delete ("mdcstack1.tmp")

# Combine the resampled SIFs in the sequence, using the WCS information
# included in their headers to align the images.  In combining, the MEDIAN
# of pixel values in a stacked pixel column is used, after rejecting the
# pixels identified in the bad pixel masks.

imdelete (mdcnames.seqlabel, >& "dev$null")
imdelete (mdcnames.bpm, >& "dev$null")
imdelete (mdcnames.expmask, >& "dev$null")
count (ilist) | scan (i)
if (i > 480) {
    list = ilist
    while (fscan(list,s1)!=EOF) {
        s2 = substr (s1, strldx("/",s1)+1, 999)
	s2 = substr (s2, 1, strstr("-ccd",s2)-1)
	print (s2, >> "mdcstack1.tmp")
    }
    list = ""
    sort ("mdcstack1.tmp") | unique (> "mdcstack1.tmp")
    list = "mdcstack1.tmp"
    while (fscan(list,s1)!=EOF) {
	if (imaccess(s1)==YES) {
	    imdelete (s1)
	    imdelete (s1//"_bpm.pl")
	}
	;
        match (s1, ilist, > "mdcstack2.tmp")
	imcombine ("@mdcstack2.tmp", s1,
	    bpmasks=s1//"_bpm.pl", headers="", rejmask="",
	    nrejmask="", expmask="", imcmb="",
	    logfile=lfile, combine="average", reject="none",
	    project-, outtype="real", outlimits="", offsets="wcs",
	    masktype="novalue", maskvalue=1, blank=0, scale="none",
	    zero="none", weight="none", statsec="", expname="",
	    lthresh=INDEF, hthresh=INDEF)
	flpr imcombine
	print (s1, >> "mdcstack3.tmp")
	print (s1, >> "mdcstack4.tmp")
	print (s1//"_bpm.pl", >> "mdcstack4.tmp")
    }
    list = ""; delete ("mdcstack1.tmp,mdcstack2.tmp")

} else
    copy (ilist, "mdcstack3.tmp")

imcombine ("@mdcstack3.tmp", mdcnames.seqlabel, bpmasks=mdcnames.bpm//".pl",
    headers="", rejmask="", nrejmask="", expmask=mdcnames.expmask//".pl",
    imcmb="IMCMBR",  logfile=lfile, combine="median", reject="none",
    project-, outtype="real", outlimits="", offsets="wcs",
    masktype="novalue", maskvalue=1, blank=0, scale="!MDCSCALE1",
    zero="!MDCSKY1", weight="none", statsec="", expname="EXPTIME",
    lthresh=INDEF, hthresh=INDEF)

if (imaccess(mdcnames.seqlabel)==NO)
    logout 0
;

# Set header.
hedit (mdcnames.seqlabel, "SKYMEAN", 0.00, add+)
hedit (mdcnames.seqlabel, "MAGZERO", magzero0, add+)

# Clean up.
if (access("mdcstack4.tmp"))
    imdelete ("@mdcstack4.tmp")
;
delete ("mdcstack*.tmp")

logout 1
