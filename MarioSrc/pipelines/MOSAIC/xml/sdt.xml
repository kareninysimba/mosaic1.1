<?xml version="1.0" encoding="utf-8" standalone="no" ?>
<!DOCTYPE Pipeline SYSTEM "NHPPS.dtd">
<Pipeline system="ENV(NHPPS_SYS_NAME)" name="sdt" poll="0.1">

<Description>
single node data transport pipeline

The SDT pipeline transports selected data from its input list to the
output directory of the calling pipeline and deletes the rest.  There is a
feature that provides for specifying any output directory instead of the
standard output directory.  Typically instances of this pipeline would
run on every node and would be triggered with a list of files that are
only on its own node.  In this way the saving and deleting of processing
files can be run in parallel on each node.  Note, however, that this is
not a requirement and this pipeline can handle data from any set of nodes.

One optimization is to return to the calling pipeline immediately after
transporting the data to be saved and leaving the deletion of unwanted
files to be performed while the rest of the pipeline application goes on.

This pipeline is typically called by a single higher level DTS pipeline
that separates all the data by node, submits it to this SDT pipeline,
and takes care of logging completion of the data transport operation.


Input
=====

- standard input list containing candidate files with full IRAF paths
- standard return file to parent pipeline
- optional [dataset].odir or default.odir file in the parent pipeline
  input directory

Output
======

- saved files to output/[dataset] of the parent pipeline if not
  overridden by .odir file in the parent pipeline input directory
- non-saved files in the input list are deleted
- standard list of files in the output directory

Parameters
==========

- ``dts_save`` specifies a list of patterns for files to
  be saved and files not matching a pattern will be deleted.  This
  parameter is actually used by the SDT pipeline but logically it
  applies to DTS as well

</Description>

<Module name="sdtstart">
<Description>
standard start stage
</Description>
  <Trigger>
    <FileRequirement fnPattern="*.sdttrig"/>
  </Trigger>
  <PreProcAction>
    <RenameTrigger argv="$EVENT_NAME.sdttrig $EVENT_NAME.sdtproc"/>
  </PreProcAction>
  <ProcAction>
    <StartPipe argv="start.cl"/>
  </ProcAction>
  <PostProcAction>
    <ExitCode val="1"/>
    <ExitCode val="2"/>
    <RemoveTrigger argv="$EVENT_NAME.sdtproc"/>
    <OSFUpdate argv="sdtstart p"/>
  </PostProcAction>
  <PostProcAction isFailure="true">
    <ExitCode val="0"/>
    <ExitCode val="11"/>
    <RenameTrigger argv="$EVENT_NAME.sdtproc $EVENT_NAME.sdterr"/>
    <OSFUpdate argv="${self.name} e"/>
  </PostProcAction>
</Module>

<Module name="sdtsave">
<Description>
save selected data to the parent output directory
</Description>
  <Trigger>
    <OSFRequirement argv="${self.name} = _"/>
    <OSFRequirement argv="sdtstart = p"/>
  </Trigger>
  <MaxExec time="0:2:0:0"/>
  <PreProcAction>
    <OSFUpdate argv="${self.name} p"/>
  </PreProcAction>
  <ProcAction>
    <Foreign argv="sdtsave.cl"/>
  </ProcAction>
  <PostProcAction>
    <ExitCode val="1"/>
    <OSFUpdate argv="${self.name} c"/>
  </PostProcAction>
  <PostProcAction isFailure="true">
    <ExitCode val="0"/>
    <ExitCode val="11"/>
    <OSFUpdate argv="${self.name} e"/>
  </PostProcAction>
</Module>

<Module name="sdtclean">
<Description>
clean (delete) unwanted files

For efficiency this stage actually runs after the done stage.
</Description>
  <Trigger>
    <OSFRequirement argv="${self.name} = _"/>
    <OSFRequirement argv="sdtsave = c"/>
    <OSFRequirement argv="sdtdone = c"/>
  </Trigger>
  <MaxExec time="0:2:0:0"/>
  <PreProcAction>
    <OSFUpdate argv="${self.name} p"/>
  </PreProcAction>
  <ProcAction>
    <Foreign argv="sdtclean.cl"/>
  </ProcAction>
  <PostProcAction>
    <ExitCode val="1"/>
    <OSFUpdate argv="${self.name} c"/>
    <OSFUpdate argv="sdtstart c"/>
    <OSFUpdate argv="sdtdone d"/>
    <OSFConditRemove/>
  </PostProcAction>
  <PostProcAction isFailure="true">
    <ExitCode val="0"/>
    <ExitCode val="11"/>
    <OSFUpdate argv="${self.name} e"/>
  </PostProcAction>
</Module>

<Module name="sdtdone">
<Description>
standard done stage

Note this is not the last stage to complete.  It runs after the save
stage.  At this point the desired files have been saved but the deleteion
of unwanted files will continue in parallel.
</Description>
  <Trigger>
    <OSFRequirement argv="${self.name} = _"/>
    <OSFRequirement argv="sdtsave = c"/>
  </Trigger>
  <PreProcAction>
    <OSFUpdate argv="${self.name} p"/>
  </PreProcAction>
  <ProcAction>
    <DonePipe argv="done.cl .sdt SDTDONE no"/>
  </ProcAction>
  <PostProcAction>
    <ExitCode val="1"/>
    <OSFUpdate argv="${self.name} c"/>
  </PostProcAction>
  <PostProcAction isFailure="true">
    <ExitCode val="0"/>
    <ExitCode val="11"/>
    <OSFUpdate argv="${self.name} e"/>
  </PostProcAction>
</Module>

</Pipeline>
