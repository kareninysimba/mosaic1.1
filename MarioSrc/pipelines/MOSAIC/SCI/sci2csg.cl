#!/bin/env pipecl
#
# SCI2CSG -- Call cosmic string pipeline.
# 
# The input list is all pipeline products from FTR.  A list of lists for
# each parallel group (a single exposure or stack) is created and then the
# pipelines are triggered by the standard call routine with the split option.
# 
# The input to the CSG pipeline is a list calibrated SIFs for
# a single exposure or a stack and source catalogs of the images.
#
# STATUS:
#    0 = error
#    1 = no data
#    2 = pipeline called
#    3 = pipeline not running

file	ilist, cat, im, csg

logout 1

# Load packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"
utilities

# Set file and path names.
names ("sci", envget("OSF_DATASET"))

# Set working directory.
cd (names.datadir)
ilist = names.indir // names.dataset // ".sci"
delete ("sci2csg*.tmp")

# Log start of processing.  The callpipe will log to the logfile.
printf ("\nSCI2CSG-SETUP (%s): ", names.dataset)
time

# Find catalogs.
match ("MOSAIC_SIF/?*-ccd[0-9]*_cat", ilist, > "sci2csg1.tmp")
match ("MOSAIC_MDC/?*_stk_cat", ilist, >> "sci2csg1.tmp")

# Find calibrated SIFs.
match ("_s.fits", ilist, > "sci2csg2.tmp")
count ("sci2csg2.tmp") | scan (i)
if (i == 0) {
    match ("_f.fits", ilist, > "sci2csg2.tmp")
    count ("sci2csg2.tmp") | scan (i)
}
;
if (i == 0) {
    match ("_p.fits", ilist, > "sci2csg2.tmp")
    count ("sci2csg2.tmp") | scan (i)
}
;
if (i == 0)
    match ("MOSAIC_SIF/?*-ccd[0-9]*.fits", ilist, > "sci2csg2.tmp")
;
match ("MOSAIC_MDC/?*_stk.fits", ilist, >> "sci2csg2.tmp")

# Match catalogs and images into CSG input lists.
list = "sci2csg1.tmp"
while (fscan (list, cat) != EOF) {
    csg = substr (cat, strldx("/",cat)+1, 999)
    csg = substr (csg, 1, strstr("_cat",csg)-1)
    i = strstr ("-ccd", csg)
    if (i > 0) {
	im = ""; match (csg, "sci2csg2.tmp") | scan (im)
	csg = substr (csg, 1, i-1)
	csg = substr (csg, strldx("-",csg)+1, 999)
    } else {
	im = ""; match (csg, "sci2csg2.tmp") | scan (im)
	csg = substr (csg, 1, strldx("_",csg)-1)
	csg = substr (csg, strldx("-",csg)+1, 999)
	csg += "stk"
    }
    if (im != "")
	printf ("%s\n%s\n", cat, im, >> "sci2csg_"//csg//".tmp")
    ;
}
list = ""; delete ("sci2csg[12].tmp")

# Make list of lists.
pathnames ("sci2csg_*.tmp", > "sci2csg1.tmp")

# Submit to the CSG pipelines.
callpipe ("sci", "csg", "split", "sci2csg1.tmp", "sci2csg2.tmp", "sci2csg3.tmp")
concat ("sci2csg2.tmp")
i = 1; concat ("sci2csg3.tmp") | scan (i)
delete ("sci2csg*.tmp")
logout (i)
