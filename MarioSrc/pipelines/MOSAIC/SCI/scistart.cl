#!/bin/env pipecl
#
# SCISTART -- Start SCI pipeline.
#
# This is triggered by a list of all the data products from the
# previous processing.  The datasets to process are defined by the
# global fits files from the MEF pipeline.  If there are no files
# then the SCI pipeline is not created and an appropriate return
# to the calling pipeline is made.

string	dataset, indir, datadir, ifile, lfile, rtnfile, rfile, tmp

# Set file and pathnames.
names ("sci", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
ifile = indir // dataset // ".sci"
tmp = dataset // ".tmp"
cd (indir)

# Check if there is any data.
match ("MOSAIC_MEF?*_00.fits", ifile) | count | scan (i)
if (i == 0) {
    # Trigger no data found return.
    printf ("\n%s (%s): ", "SCISTART", dataset); time
    printf ("   WARNING: No data found for SCI pipeline\n")
    rtnfile = indir // "return/" // dataset // ".sci"
    if (access (rtnfile)) {
	head (rtnfile, nlines=1) | scan (rfile)
	delete (rtnfile)
	touch (rfile)
	print ("No data found for SCI pipeline", >> rfile//"log")
	touch (rfile//"trig")
    }
    ;
    logout 2
}
;

# Create dataset directory if needed.
if (access (datadir) == NO)
    mkdir (datadir)
;

# Log processing.
printf ("\n%s (%s): ", "SCISTART", dataset) | tee (lfile)
time | tee (lfile)

logout 1
