#!/bin/env pipecl
#
# FRGMKFRG -- Make a fringe template.
#
# This makes a sky flat and then removes the median background.

int	rulepassed
string	dataset, datadir, ifile, lfile
string	filter, rulepath, cast
real	mjd

# Packages and task.
images
proto
utilities
servers
dataqual
mscred
cache mscred
task scmb = "mariosrc$scmb.cl"
task sftselect = "NHPPS_PIPESRC$/MOSAIC/SFT/sftselect.cl"
cache sftselect

# Define rules
findrule( dm, "enough_for_fringe", validate=1 ) | scan( rulepath )
task enough_for_fringe = (rulepath)

# Define the newDataProduct python interface layer
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"

# Set paths and files.
frgnames (envget ("OSF_DATASET"))
dataset = frgnames.dataset
datadir = frgnames.datadir
lfile = datadir // frgnames.lfile
ifile = frgnames.indir // dataset // ".frg"
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set (uparm = frgnames.uparm)
set (pipedata = frgnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nFRGMKFRG (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up from previous processing.
delete ("frgmkfrg[^_]*")
if (imaccess(frgnames.sflat))
    imdelete (frgnames.sflag)
;
if (imaccess(frgnames.bpm))
    imdelete (frgnames.bpm)
;

# Select images and scale factors.
sftselect ("FRGFLAG", ifile, "frgmkfrg1.tmp", "frgmkfrg2.tmp", cksftflag+)

# Check the rule.
if (sftselect.nselect > 0 && sftselect.filter == "default")
    sendmsg ("WARNING", "FILTER not found, using default rule", "", "VRFY")
;
enough_for_fringe (sftselect.filter, sftselect.nselect) | scan (i)
if (sftselect.nforced > 0  && sftselect.nselect > 0)
    rulepassed = 1
else
    rulepassed = i

if (rulepassed == 0) {
    sendmsg ("WARNING", "Not enough images to make fringe template",
        str(sftselect.nselect), "DQ")
    printf ("WARNING: Not enough images to make fringe template (%d).\n",
        sftselect.nselect) | tee (lfile)
    sftselect.nselect = 0
}
;

# This is a pain but we need to do this outside an if block because of a bug.
enough_for_fringe (sftselect.filter, sftselect.nselect) | scan (i)
if (rulepassed == 0 && i == 0) {
    delete ("frgmkfrg[^_]*")
    logout 2
}
;

# Run scombine with the object masks.  If the number is large do it in blocks.
scmb ("frgmkfrg1.tmp", frgnames.sflat, frgnames.bpm, "frgmkfrg2.tmp",
    "frgmkfrga", imcmb="IMCMBMEF")
hedit (frgnames.sflat, "IMCMBMEF,IMCMBSIF", del+, ver-, show-)

# Clean up
delete ("frgmkfrg[^_]*")

# Check for success.
if (imaccess (frgnames.sflat) == NO) {
    sendmsg ("WARNING", "Fringe pattern not created", frgnames.frg, "PROC")
    printf ("WARNING: No fringe pattern created (%s)\n", dataset) | tee (lfile)
    logout 3
}
;

# Sigma clip the template to avoid bad data.
imrename (frgnames.sflat, "frgmkfrg.fits")
imrename (frgnames.bpm, "frgmkfrg.pl")
mimstat ("frgmkfrg.fits", imask="frgmkfrg.pl", omask=frgnames.bpm,
    nclip=3, lsig=10, usig=10, fields="min,max", format-) | scan (x, y)
printf ("Clipped data between %.3g and %.3g\n", x, y)
imexpr ("max(b,min(c,a))", frgnames.sflat, "frgmkfrg.fits", x, y,
    outtype="real", verbose-)
imdelete ("frgmkfrg.fits,frgmkfrg.pl")

# Subtract median.
fmedian (frgnames.sflat, frgnames.med, xwindow=129, ywindow=129, >> lfile)
imarith (frgnames.sflat, "-", frgnames.med, frgnames.frg)
imdelete (frgnames.sflat//","//frgnames.med)

# Set identifying keywords
hedit( frgnames.frg, "PROCTYPE", "MasterCal", add+, ver- )
hedit( frgnames.frg, "OBSTYPE", "fringe", add+, ver- )

# Enter new dataproduct.
hsel( frgnames.frg, "mjd-obs", yes ) | scan( mjd )
newDataProduct(frgnames.frg, mjd, "fringeimage", "")
storekeywords( class="fringeimage", id=frgnames.frg, sid=frgnames.frg, dm=dm )

# Add number of contributing exposures to header and PMAS
hedit( frgnames.frg, "DQFRNUSD", sftselect.nselect, add+, verify-, show+,
    update+, >> lfile)
printf("%d\n", sftselect.nselect) | scan( cast )
setkeyval( class="fringeimage", id=frgnames.frg, dm=dm, keyword="dqfrnusd",
    value=cast )

# Add quality value.
if (rulepassed == 0)
    hedit (frgnames.frg, "QUALITY", -1, add+, ver-)
else
    hedit (frgnames.frg, "QUALITY", 0, add+, ver-)

logout 1
