#!/bin/env pipecl
#
# FRGSET -- Verify template and set fringe template.
#
# Flags:
# _     Fringe created.  Possibly ask operator to review.
#       If no review enter in calibration library.
# Y     Fringe is ok.  Enter in calibration library.
# N     Fringe is not ok.  Use calibration library.
# A     Fringe not created.  Use calibration library.
# S     Skip fringe correction.

int	status = 1
real	quality = -1
real	mjd, mjdstart, mjdend
string	dataset, datadir, lfile
string	flag

# Packages and tasks.
images
servers

# Set paths and files.
flag = envget ("OSF_FLAG")
frgnames (envget("OSF_DATASET"))
dataset = frgnames.dataset
datadir = frgnames.datadir
lfile = datadir // frgnames.lfile
set (uparm = frgnames.uparm)
cd (datadir)

# Log start of processing.
printf ("\nFRGSET (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Evaluate the fringe template.
# We don't have anything here yet.

# Trigger DRV pipeline if it is running.
if (flag == "_") {
    task $pipeselect = "$!pipeselect"
    s1 = ""; pipeselect (envget ("NHPPS_SYS_NAME"), "drv", 0, 0) | scan (s1)
    if (s1 != "") {
	daynames (frgnames.parent)
	s2 = daynames.parent // "-drv-" // daynames.child
	s3 = s1 // s2 // ".frg"
	pathname (frgnames.frg, > s3)
	touch (s3//"trig")
	if (!dps_review)
	    flag = "Y"
	;
    } else
        flag = "Y"
}
;

# Wait for review.
if (flag == "_")
    logout 2
;

# Put the calibration files in the Data Manager.
if (flag == "Y") {
    hselect (frgnames.frg, "mjd-obs,quality", yes) | scan (mjd, quality)
    mjdstart = mjd + cal_mjdmin; mjdend = mjd + cal_mjdmax
    putcal (frgnames.frg//".fits", "image", dm=cm, class="fringe",
	detector=instrument, imageid="!ccdname", filter="!filter",
	exptime="", quality=quality, match="!ccdsum",
	mjdstart=mjdstart, mjdend=mjdend)
    if (putcal.statcode != 0) {
	sendmsg ("ERROR", "Putcal failed", putcal.statstr, "CAL")
	status = 0
    }
    ;
} else if (imaccess(frgnames.frg))
    imdelete (frgnames.frg)
;
if (status == 0)
    logout 4
;

# Don't apply fringe correction.  This is here rather than earlier so the code
# above can reset the flag.
if (flag == "A" && frg_cal == "no")
    flag = "S"
;
if (flag == "S")
    logout 3
;

logout 1
