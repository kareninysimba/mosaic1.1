#!/bin/env pipecl
#
# FRGSETUP -- Setup FRG processing data. 
# 
# The input list of images is assumed to contain the uparm keyword.

string	dataset, datadir, ifile, lfile, mkcal
int	founddir
real	qual = 0
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
frgnames (envget("OSF_DATASET"))
dataset = frgnames.dataset
ifile = frgnames.indir // dataset // ".frg"
datadir = frgnames.datadir
lfile = datadir // frgnames.lfile
set (uparm = frgnames.uparm)
set (pipedata = frgnames.pipedata)

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

# Log start of processing.
printf ("\nFRGSETUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Clean up from previous processing.
if (access(ifile//"obm")) {
    concat (ifile//"obm", ifile, append+)
    delete (ifile//"obm")
}
;
if (access(ifile//"sky")) {
    concat (ifile//"sky", ifile, append+)
    delete (ifile//"sky")
}
;
delete ("*.fits")

# Separate obm and sky files.
match ("_obm.fits", ifile) | sort ( > ifile//"obm")
match ("_sky.fits", ifile) | sort ( > ifile//"sky")
match ("_obm.fits", ifile, stop+) | sort ( > "frgsetup.tmp")
match ("_sky.fits", "frgsetup.tmp", stop+) | sort ( > ifile)
delete ("frgsetup.tmp")

# Setup UPARM.
delete (substr(frgnames.uparm, 1, strlen(frgnames.uparm)-1))
s1 = ""; head (ifile, nlines=1) | scan (s1)
iferr {
    setdirs (s1, umatch="!ccdsum")
} then {
    logout 0
} else
    ;

# Check for library calibration.  The purpose of this loop here is to
# allow skipping generating an fringe template if there is one in 
# the library with higher precedence.  Note that the frg_cal flag can force
# creation of an template.
    
i = fscan (frg_cal, mkcal, qual)
if (mkcal == "libfirst" || mkcal == "libonly") {
    b1 = yes
    list = ifile
    while (fscan (list, s1) != EOF) {
        getcal (s1, "fringe", cm, caldir, obstype="",
	    detector=instrument, imageid="!ccdname", filter="!filter",
	    exptime="", mjd="!mjd-obs", dmjd=cal_dmjd, quality=qual,
	    match="!ccdsum", > "dev$null")
        if (getcal.statcode != 0) {
	    b1 = no
	    break
	}
        ;
    }
    list = ""
} else
    b1 = no

if (mkcal == "libonly" || (b1 && mkcal == "libfirst")) {
    sendmsg ("WARNING", "Creation of fringe pattern skipped", "", "CAL")
    logout 2
}
;

# Check dataset comments.
s1 = ""
if (access(ifile//"com"))
    match ("\#A", ifile//"com") | scan (s1)
;
if (s1 == "#A" && mkcal == "no")
    logout 2
;

# We're going to need the object masks from the input data so copy them.
# While we are at it we change the values to compress the object mask.
# Note that we can choose the values that are good and bad here.

list = ifile
while (fscan (list, s1) != EOF) {
    s2 = ""; hselect (s1, "objmask", yes) | scan (s2)
    if (s2 == "")
        next
    ;
    i = strldx ("[", s2)
    if (i > 0)
        s2 = substr (s2, 1, i-1) // ".fits"
    ;
    match (s2, ifile//"obm") | scan (s3)
    #copy (s3, ".", verbose+)
    iferr {
	imexpr ("min(a,11)", s2//"[pl,type=mask]", s3//"[pl]", verb-)
    } then {
	copy (s3, ".", verbose+)
    } else
        ;
}
list = ""

# Allow time for object masks to actually appear.  This is some kind of
# bug/problem with our pipeline OS such that the next stage might not
# actually find the files without the following delay.
sleep (1)

logout 1
