#!/bin/env pipecl
#
# SFGFRG -- Remove fringing.

real	quality = -1
string	dataset, datadir, ifile, lfile, cast, f, sfg
real	scale, mjd
file	caldir = "MC$"

# Packages and task.
task $newDataProduct = "$!newDataProduct.py $1 $2 $3 -u $4"
images
servers
dataqual

# Set paths and files.
names ("sfg", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ifile = names.indir // dataset // ".sfg"
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# Log start of processing.
printf ("\nSFGFRG (%s): ", dataset) | tee (lfile)
time | tee (lfile)

list = ifile
while (fscan (list, s1) != EOF) {
    # Set the calibrated name from the uncalibrated name.
    i = strldx ("/", s1)
    j = strlstr ("_p.fits", s1)
    if (j == 0)
	j = strlstr (".fits", s1)
    ;
    s2 = substr (s1, i+1, j-1)
    f = s2 // "_f"
    s3 = substr (s2, 1, strldx("-",s2)-1)
    s3 = substr (s3, strldx("-",s3)+1,1000)

    # Set the fringe template and scaling.
    sfg = ""; hselect (s1, "FRINGE", yes) | scan (sfg)
    scale = INDEF; hselect (s1, "FRGSCALE", yes) | scan (scale)
    if (scale == INDEF)
        sfg = ""
    if (sfg != "") {
	if (imaccess(sfg) == NO) {
	    sfg = ""
	    getcal (s1, "fringe", cm, caldir, obstype="",
	        detector=instrument, imageid="!ccdname", filter="!filter",
		exptime="", mjd="!mjd-obs", quality=0.,
		match="!ccdsum") | scan (i, line)
	    if (i == 0)
		hselect (s1, "FRINGE", yes) | scan (sfg)
	    else
	        sendmsg ("WARNING", "No library finge template to apply",
		    substr(s1,strldx("/",s1)+1,1000), "CAL")
	}
	;
    }
    ;

    # Subtract scaled fringe template from data.
    if (sfg != "") {
	if (scale == 1.) {
	    printf ("%s = %s - %s\n", f, s1, sfg)
	    imexpr ("a - b", f, s1, sfg, outtype="real",
		verbose-)
	} else {
	    printf ("%s = %s - %g * %s\n", f, s1, scale, sfg)
	    imexpr ("a - b * c", f, s1, sfg, scale, outtype="real",
	    verbose-)
	}
	hselect (f, "mjd-obs", yes) | scan (mjd)
	newDataProduct (f, mjd, "objectimage", "")
	storekeywords (class="objectimage", id=f, sid=f, dm=dm)
	printf ("%12.5e\n", scale) | scan ( cast )
	setkeyval (class="objectimage", id=f, dm=dm, keyword="dqpgscal",
	    value=cast)
	setkeyval (class="objectimage", id=f, dm=dm, keyword="dqpgcal",
	    value=sfg)
    } else {
	printf ("%s = %s\n", f, s1)
	imcopy (s1, f, verbose-)
    }
}
list = ""

logout 1
