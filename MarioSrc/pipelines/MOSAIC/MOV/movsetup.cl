#!/bin/env pipecl
#
# MOVSETUP -- Setup MOV processing data. 
# 
# The input list of images is assumed to contain the uparm keyword.

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers
proto

# Set paths and files.
names ("mov", envget("OSF_DATASET"))
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in the data directory.  Wait for datadir to appear.
for (i=1; access(names.datadir)==NO && i<=10; i+=1) {
    sleep (1)
    printf("Retrying to access %s\n", names.datadir)
}
if (access(names.datadir)==NO)
    logout (1)
;
cd (names.datadir)

# Log start of processing.
printf ("\nMOVSETUP (%s): ", names.dataset) | tee (names.lfile)
time | tee (names.lfile)

# Setup directories.
match (".fits", names.indir//names.dataset//".mov", > "movsetup.tmp")
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
iferr {
    setdirs ("@movsetup.tmp")
    delete ("movsetup.tmp")
} then {
    logout 0
} else
    ;

logout 1
