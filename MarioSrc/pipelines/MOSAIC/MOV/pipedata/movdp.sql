#DROP TABLE IF EXISTS MOVDS;
CREATE TABLE IF NOT EXISTS MOVDS (
    dataset          varchar(48),
    procid           varchar(48),
    cutouts          varchar(52),
    nexposures       int,
    date             varchar(24),
    mjd              double,
    lst              varchar(13),
    ra_field         varchar(13),
    dec_field        varchar(13),
    gamma_field      varchar(13),
    beta_field       varchar(13),
    opposition       varchar(13),
    filter           varchar(64),
    ngrp             int,
    PRIMARY KEY (dataset)
    )
    ;

#DROP TABLE IF EXISTS MOVEXP;
CREATE TABLE IF NOT EXISTS MOVEXP (
    dataset          varchar(48),
    procid           varchar(48),
    expname          varchar(48),
    expid            int,
    tsep             double,
    exptime          double,
    PRIMARY KEY (dataset, expid)
    )
    ;

#DROP TABLE IF EXISTS MOVGRP;
CREATE TABLE IF NOT EXISTS MOVGRP (
    dataset          varchar(48),
    procid           varchar(48),
    groupid          int,
    nobs             int,
    rate             double,
    pa               double,
    mag              double,
    PRIMARY KEY (dataset, groupid)
    )
    ;

#DROP TABLE IF EXISTS MOVOBS;
CREATE TABLE IF NOT EXISTS MOVOBS (
    dataset          varchar(48),
    procid           varchar(48),
    groupid          int,
    expid            int,
    imageid          varchar(8),
    ra_obs           varchar(13),
    dec_obs          varchar(13),
    extname          varchar(16),
    PRIMARY KEY (dataset, groupid, expid)
    )
    ;

#DROP TABLE IF EXISTS MOVMPC;
CREATE TABLE IF NOT EXISTS MOVMPC (
    dataset          varchar(48),
    procid           varchar(48),
    movid            varchar(6),
    groupid          int,
    expid            int,
    mpcrec           char(80),
    PRIMARY KEY (dataset, movid, groupid, expid)
    )
    ;
