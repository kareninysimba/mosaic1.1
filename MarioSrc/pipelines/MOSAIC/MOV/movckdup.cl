# MOVCKDUP -- Check for duplicate associations in moving cutout file.

procedure movckdup ()

file	input = "*_mov.fits"	{prompt="Input cutout file"}
string	output = "movckdup"	{prompt="Output root name"}
file	movdata = "NHPPS_PIPEAPPSRC$/MOV/pipedata/" {prompt="Data file directory"}

begin
	int	i
	file	in, out, tmp1, tmp2

	# Set input query parameters.
	in = input
	out = output

	# Set temp files.
	tmp1 = out // "1.tmp"
	tmp2 = out // "2.tmp"
	delete (out//"[12].tmp")

	# Expand input if necessary.
	files (in) | count | scan (i)
	if (i != 1)
	    error (1, "Unknown or ambiguous input file\n")
	files (in) | scan (in)
	i = strstr (".fits", in) - 1
	if (i > 0)
	    in = substr (in, 1, i)

	# Create catalog.
	hselect (in//"[0]", "RSPRA,RSPDEC", yes) | scan (x, y)
	printf ("#c COGRPID i %%3d\n", > tmp2)
	printf ("#c COEXPID i %%2d\n", >> tmp2)
	printf ("#c CORA d %%12.2h hr\n", >> tmp2)
	printf ("#c CODEC d %%12.1h deg\n", >> tmp2)
	printf ("#k RSPRA = %12.1h\n", x, >> tmp2)
	printf ("#k RSPDEC = %12.2h\n", y, >> tmp2)
	mscselect (in, "COGRPID,COEXPID,CORA,CODEC", extname="AA?*") |
	    match ("INDEF", stop+, >> tmp2)
	tinfo (tmp2, ttout-)
	if (tinfo.nrows > 0) {

	    tcalc   (tmp2, "N", "ROWNUM", datatype="int", colfmt="%3d") 

	    # Set clustering coordinates.
	    acecopy (tmp2, tmp1, catdef=movdata//"movckdup1.dat",
		filter="", verb-)

	    # Cluster.
	    acecluster (tmp1, out//".cat", "0:00:02,0:00:02,0.1", mincluster=2,
		icatdef=movdata//"movckdup2.dat", ocatdef=movdata//"movckdup2.dat",
		ifilter="", ofilter="")

	    tinfo (out//".cat", ttout-)
	    if (tinfo.nrows > 0)
		printf ("Duplicates found for %s\n", in)
	    else
		delete (out//".cat", verify-)
	}
	;

	delete (out//"[12].tmp")
end
