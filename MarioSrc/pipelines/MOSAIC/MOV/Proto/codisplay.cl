# CODISPLAY -- Cutout display tool for moving objects.
#
# There is a bug in partab that truncates strings.  So for long enough
# filenames this can be aproblem.

procedure codisplay ()

file	cofile  = "*_mov.fits"	{prompt="Primary cutout MEF"}
int	cluster = 1		{prompt="Cluster"}
int	frame   = 1		{prompt="Primary frame"}
string	select = "XA"		{prompt="Selection pattern"}
string	accept = "A"		{prompt="Acceptance character"}
string	delete = "D"		{prompt="Deletion character"}
int	maxcluster = 3		{prompt="Maximum cluster size"}
file	cohelp = "cohelp.key"	{prompt="Help file\n"}

file	zcofile = ")codisplay.cofile"	{prompt="Alternate cutout MEF"}
string	zselect = "XB"		{prompt="Alternate selection pattern"}
int	zframe  = 2		{prompt="Alternate frame"}

string	space = "+"		{prompt="Map space to desired key"}
string	radii = "10,11,12"	{prompt="Radii"}

struct	*fd, *fd1

begin
	int	i, j, k, l, nexp, nim, ncluster, nmin, wcs, f, dir, prev, s, row
	int	c1, c2, l1, l2, nl, ltv1, dltv1, ltv2, dltv2, c1last, l1last
	real	x, y, px, py, ra, dec, ltm2
	string	key, extn, rasex, decsex, arg1, arg2
	file	coroot, coinfo, comef, colist, cocat, cotmp, im1, im2
	struct	cmd, line

	# Set query parameter.
	im2 = cofile
	files (im2) | count | scan (i)
	if (i != 1)
	    error (1, "Non-existent or ambiguous cutout file")
	files (im2) | scan (im2)

	# Set clobber.
	set clobber = yes

	# Set working files.
	coroot = "codisp"
	coinfo = coroot // ".info"
	delete (coroot//'*[pst]', verify-)

	# Create working files.
	if (access(coinfo)) {
	    im1 = im2
	    files (im1) | scan (im1)
	    if (strlen(im1) > 50 && strstr(".fits",im1) > 0)
	        im1 = substr (im1, 1, strstr(".fits",im1)-1)
	    cotmp = ""
	    thselect (coinfo, "COFILE", yes) | scan (cotmp)
	    if (cotmp != im1)
	        delete (coinfo, verify-)
	}
	if (!access("BAKDIR/"))
	    mkdir ("BAKDIR/")
	if (!imaccess("BAKDIR/"//im2//"[0]")) {
	    if (access(im2))
		copy (im2, "BAKDIR/")
	    else if (access(im2//".fits"))
		copy (im2//".fits", "BAKDIR/")
	}
	if (!access(coinfo)) {
	    im1 = im2
	    if (strlen(im1) > 50 && strstr(".fits",im1) > 0)
	        im1 = substr (im1, 1, strstr(".fits",im1)-1)
	    cotmp = coroot // ".tmp"
	    hselect (im1//"[0]", "CONEXP", yes) | scan (nexp)
	    mscselect (im1, "$I,COEXPID,COGRPID,CORA,CODEC",
	        extname="", > cotmp)
	    im2 = zcofile
	    files (im2) | scan (im2)
	    if (strlen(im2) > 50 && strstr(".fits",im2) > 0)
	        im2 = substr (im2, 1, strstr(".fits",im2)-1)
	    if (im2 != im1)
		mscselect (im2, "$I,COEXPID,COGRPID,CORA,CODEC",
		    extname="", >> cotmp)
	    tinfo (cotmp, ttout-)
	    if (tinfo.nrows == 0) {
		printf ("WARNING: No Data Found\n")
		delete (coroot//"*", verify-)
		return
	    }
	    tcalc (cotmp, "ROW", "rownum", datatype="int")
	    tchcol (cotmp, "c1", "IMAGE", "", "", verbose-)
	    tchcol (cotmp, "c4", "RA", "", "", verbose-)
	    tchcol (cotmp, "c5", "DEC", "", "", verbose-)
	    tchcol (cotmp, "c3", "CLUSTER", "", "", verbose-)
	    tchcol (cotmp, "c2", "EXPID", "", "", verbose-)
	    tproject (cotmp, coinfo, "IMAGE,ROW,RA,DEC,CLUSTER,EXPID", uniq-)
	    delete (cotmp, verify-)
	    thedit (coinfo, "COFILE", im1, del-, show+)
	    thedit (coinfo, "CONEXP", nexp, del-, show-)

	    cluster = 1
	}

	# Initialize
	thselect (coinfo, "CONEXP", yes) | scan (nexp)
	tstat (coinfo, "CLUSTER", outtable="")
	nmin = 1
	ncluster = tstat.vmax
	if (cluster > ncluster)
	    cluster = 1
	dir = 1; wcs = 100 * frame; prev = 0

	# Loop through each cluster.
	key = 'r'
	while (key != 'q' && key != 'Q') {

	    # Default mapping.
	    if (key == '\\040')
	        key = space

	    # Switch on cursor command.
	    if (key == ':') {
		if (stridx ("#", cmd) == 0)
		    print (cmd) | translit ("STDIN", "=", " ") | scan (cmd)
	        i = fscan (cmd, arg1, arg2, line)
		if (i > 0) {
		    if (arg1 == "cluster") {
		        if (i > 1)
			    cluster = int (arg2)
			printf ("cluster %d\n", cluster)
		    } else if (arg1 == "frame") {
		        if (i > 1)
			    frame = int (arg2)
			printf ("frame %d\n", frame)
		    } else if (arg1 == "zframe") {
		        if (i > 1)
			    zframe = int (arg2)
			printf ("zframe %d\n", zframe)
		    } else if (arg1 == "#" || arg1 == "##") {
			# Add comment
			j = wcs / 100; i = mod (wcs, 100)
			if (i > nim)
			   goto rdcursor
			colist = coroot // j // ".list"
			fd = colist
			for (j=1; fscan(fd,im1)!=EOF; j+=1) {
			    if (substr(im1,1,1) == '#') {
			        j -= 1
				next
			    }
			    if (substr(im1,1,1) == "-") {
			        j = 0
				next
			    }
			    if (arg1 == "#" && j != i)
			        next
			    hedit (im1, arg2, line, add+,show+,verify-,update+)
			}
			fd = ""
		    } else if (arg1 == "maxcluster") {
			if (i > 1)
			    maxcluster = int (arg2)
			printf ("maxcluster = %d\n", maxcluster)
		    } else if (arg1 == "nmin") {
			if (i > 1)
			    nmin = int (arg2)
			printf ("nmin = %d\n", nmin)
		    } else if (arg1 == "space") {
		        if (i > 1)
			    space = arg2
			printf ("space = %s\n", space)
		    } else if (arg1 == "radii") {
		        if (i > 1)
			    radii = arg2
			printf ("radii = %s\n", radii)
		    } else {
		        if (fscan (arg1, cluster) == 1) {
			    prev = cluster
			    cluster = int (arg1)
			    f = wcs / 100; key = 'r'; next
			}
		    }
		}
	    } else if (key == 'r') {
	    	# Select cutouts.
		f = wcs / 100
		colist = coroot // f // ".list"
		cotmp = coroot // ".tmp"
		for (i=0; cluster!=i;
		    cluster=mod(ncluster+cluster-1+dir,ncluster)+1) {
		    if (f == frame)
			printf ("%s???%03d_?*\n", select, cluster) | scan(extn)
		    else
			printf ("%s???%03d_?*\n", zselect, cluster) | scan(extn)
		    match (extn, coinfo, stop-, meta+) | sort (> cotmp)
		    if (f == frame) {
			count (cotmp) | scan (nim)
			if (nim > maxcluster * nexp) {
			    concat (cotmp) |
				match ("INDEF", stop+, > cotmp)
			    count (cotmp) | scan (nim)
			}
			match ("INDEF", cotmp, stop+) | count | scan (k)
			match ("INDEF", cotmp, stop+) | fields ("STDIN", "6") |
			    sort | uniq | count | scan (l)
		    }
		    rename (cotmp, colist)
		    print ("-----", >> colist)
		    if (f == frame)
			printf ("%s???%03d_?*\n", zselect, cluster) | scan(extn)
		    else
			printf ("%s???%03d_?*\n", select, cluster) | scan(extn)
		    match (extn, coinfo, stop-, meta+) | sort (> cotmp)
		    if (f == zframe) {
			count (cotmp) | scan (nim)
			if (nim > maxcluster * nexp) {
			    concat (cotmp) |
				match ("INDEF", stop+, > cotmp)
			    count (cotmp) | scan (nim)
			}
			match ("INDEF", cotmp, stop+) | count | scan (k)
			match ("INDEF", cotmp, stop+) | fields ("STDIN", "6") |
			    sort | uniq | count | scan (l)
		    }
		    concat (cotmp, >> colist); flpr concat
		    delete (cotmp, verify-)
		    if (k > 0 && nim <= maxcluster * nexp && l >= nmin)
			break
		    if (nim == 0)
		        ;
		    else if (k == 0)
			printf ("Cluster %d: No Data\n", cluster)
		    else if (l < nmin)
			printf ("Cluster %d: Not in enough images (%d)\n",
			    cluster, l)
		    else if (nim > maxcluster * nexp)
		        printf ("Cluster %d: Too big (%d)\n", cluster, nim)
		    if (i == 0)
		        i = cluster
		}
		if (k == 0 || nim > maxcluster * nexp || l < nmin) {
		    printf ("WARNING: No Data Found\n")
		    key = 'q'
		    next
		}
		if (l < 2) {
		    key = 'D'; next
		}
	    } else if (key == 'd') {
		# Mark cutout as deleted.
	        j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		fd = colist
		for (j=1; fscan(fd,im1,row)!=EOF; j+=1) {
		    if (substr(im1,1,1) == '#') {
		        j -= 1
			next
		    }
		    if (substr(im1,1,1) == '-') {
			j = 0
			next
		    }
		    if (j != i)
			next
		    k = stridx ("[", im1) + 1; l = stridx ("]", im1) - 1
		    arg1 = 'D' // substr (im1,k+1,l)
		    hedit (im1, "EXTNAME", arg1, show-, verify-, update+)
		    im2 = substr(im1,1,k-1) // arg1 // substr(im1,l+1,999)
		    partab (im2, coinfo, "IMAGE", row)
		}
		fd = ""
		#key = 'r'; next
	    } else if (key == 'i') {
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		comef = coroot // j // ".fits"
		cocat = coroot // j // ".cat"
		tdump (colist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im1, row, px, py, k, l)

		# Get information.
		im2 = comef // substr (im1, stridx("[",im1), 999)
		hselect (im2, "COIMAGE,COSEC,EXPTIME,CORATE", yes) |
		    scan (arg1, arg2, x, y)
		z = y * x / 3600.
		printf ("%s%s\n", arg1, arg2)
		printf ("EXPTIME = %d sec, RATE = %.1f arcsec/hr, \
		    TRAIL = %.1f arcsec\n", x, y, z)
	    } else if (key == 'c') {
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		comef = coroot // j // ".fits"
		tdump (colist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im1, row, ra, dec, k, l)
		printf ("Again:\n")
		if (fscan (imcur, x, y, wcs, key, cmd) == EOF)
		    next
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		comef = coroot // j // ".fits"
		tdump (colist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im2, row, px, py, k, l)
		print (ra, dec) |
		wcsctran ("STDIN", "STDOUT", im1, "world", "physical",
		    columns="1 2", units="h n", formats="",
		    min_sigdigit=9, verbose-) | scan (x, y)
		print (ra, dec) |
		wcsctran ("STDIN", "STDOUT", im2, "world", "physical",
		    columns="1 2", units="h n", formats="",
		    min_sigdigit=9, verbose-) | scan (ra, dec)
		print (px, py) |
		wcsctran ("STDIN", "STDOUT", im2, "world", "physical",
		    columns="1 2", units="h n", formats="",
		    min_sigdigit=9, verbose-) | scan (px, py)
		hselect (im1, "COIMID", yes) | scan (arg1)
		printf ("%s: (%.1f,%.1f) -> (%.1f,%.1f), (%.1f,%.1f)\n",
		    arg1, x, y, ra, dec,  px, py)
	    } else if (key == 's') {
		# Move cutout to new cluster
		s = ncluster + 1
	        j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		fd = colist
		for (j=1; fscan(fd,im1,row)!=EOF; j+=1) {
		    if (substr(im1,1,1) == '#') {
		        j -= 1
			next
		    }
		    if (substr(im1,1,1) == '-') {
			j = 0
			next
		    }
		    if (j != i)
			next
		    k = stridx ("[", im1) + 1; l = stridx ("]", im1) - 1
		    arg1 = substr (im1, k, k+4); arg2 = substr (im1, k+8, l) 
		    printf ("%s%03d%s\n", arg1, s, arg2) | scan (extn)
		    hedit (im1, "COTILE2", s, show-, verify-, update+)
		    hedit (im1, "COGRPID", s, show-, verify-, update+)
		    hedit (im1, "EXTNAME", extn, show-, verify-, update+)
		    im2 = substr(im1,1,k-1) // extn // substr(im1,l+1,999)
		    partab (im2, coinfo, "IMAGE", row)
		    partab (s, coinfo, "CLUSTER", row)
		}
		fd = ""
		#key = 'r'; next
	    } else if (key == 'S') {
	        # Close out split.
		ncluster = s
	    } else if (key == 'A' || key == 'D') {
		# Mark cluster as accepted or deleted.
	        j = wcs / 100
		colist = coroot // j // ".list"
		fd = colist
		while (fscan (fd, im1, row) != EOF) {
		    if (substr(im1,1,1) == '#' || substr(im1,1,1) == '-')
			next
		    k = stridx ("[", im1) + 1; l = stridx ("]", im1) - 1
		    if (key == 'A')
		        key = accept
		    else if (key == 'D')
		        key = delete
		    arg1 = key // substr (im1,k+1,l)
		    hedit (im1, "EXTNAME", arg1, show-, verify-, update+)
		    im2 = substr(im1,1,k-1) // arg1 // substr(im1,l+1,999)
		    partab (im2, coinfo, "IMAGE", row)
		}
		fd = ""
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		f = j; key = 'r'; next
	    } else if (key == 'm' || key == 'x' || key == 'X') {
		j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		comef = coroot // j // ".fits"
		cocat = coroot // j // ".cat"
		tdump (colist, cd="", pf="", data="STDOUT",
		    col="", row=i) | scan (im1, row, px, py, k, l)
		if (key == 'm') {
		    # Get coordinates.
		    im2 = comef // substr (im1, stridx("[",im1), 999)
		    print (x, y) |
		    wcsctran ("STDIN", "STDOUT", im2, "logical", "physical",
			columns="1 2", units="", formats="",
			min_sigdigit=9, verbose-) | scan (px, py)
		    hselect (im2, "DETSEC,LTM2_2", yes) |
			translit ("STDIN", "[:,]", " ", del-) |
			scan (x, ra, y, dec, ltm2)
		    py -= (y - 1) / ltm2
		    print (px, py) |
		    wcsctran ("STDIN", "STDOUT", im2, "physical", "world",
			columns="1 2", units="", formats="%.2H %.1h",
			min_sigdigit=9, verbose-) | scan (rasex, decsex)
		} else {
		    x = px; y = py
		    rasex = "INDEF"; decsex = "INDEF"
		}

		# Update source.
	        j = wcs / 100; i = mod (wcs, 100)
		if (i > nim)
		   goto rdcursor
		colist = coroot // j // ".list"
		fd = colist
		for (j=1; fscan(fd,im1,row)!=EOF; j+=1) {
		    if (substr(im1,1,1) == '#') {
		        j -= 1
			next
		    }
		    if (substr(im1,1,1) == '-') {
			j = 0
			next
		    }
		    if (j != i)
			next
		    hedit (im1, "CORA", rasex, show-, verify-, update+)
		    hedit (im1, "CODEC", decsex, show-, verify-, update+)
		    print (im1) | translit ("STDIN", "[]", " ", del-) |
		       scan (im2, extn)
		    partab (rasex, coinfo, "RA", row)
		    partab (decsex, coinfo, "DEC", row)
		}
		fd = ""

		# If 'X' remove all references to the source within 2 arcsec.
		if (key == 'X' && !isindef(x) && !isindef(y)) {
		    cotmp = coroot // ".tmp"
		    match ("INDEF", coinfo, stop+, > coroot//"1.tmp")
		    print (x, y, l, > coroot//"2.tmp")
		    tmatch (coroot//"1.tmp", coroot//"2.tmp", cotmp,
		        "RA,DEC,EXPID", "c1,c2,c3", 0:00:02, incol1="",
			incol2="", factor="15,1,1", sphere+,
			diagfile="", >& "dev$null")
		    fd = cotmp
		    while (fscan(fd,im1,row)!=EOF) {
		        if (substr(im1,1,1) == '#')
			    next
			hedit (im1, "CORA", rasex, show-, verify-, update+)
			hedit (im1, "CODEC", decsex, show-, verify-, update+)
			print (im1) | translit ("STDIN", "[]", " ", del-) |
			   scan (im2, extn)
			partab ("INDEF", coinfo, "RA", row)
			partab ("INDEF", coinfo, "DEC", row)
		    }
		    fd = ""
		    delete (coroot//"*.tmp", verify-)
		}
	    } else if (key == 'Z') {
		# Delete all sources at the position.
	        j = wcs / 100; i = mod (wcs, 100)
		colist = coroot // j // ".list"
		fd = colist
		while (fscan(fd,im1,row,x,y,k,l)!=EOF) {
		    if (substr(im1,1,1) == '#' || substr(im1,1,1) == '-')
			next
		    if (isindef(x) || isindef(y))
			next
		    cotmp = coroot // ".tmp"
		    match ("INDEF", coinfo, stop+, > coroot//"1.tmp")
		    print (x, y, l, > coroot//"2.tmp")
		    tmatch (coroot//"1.tmp", coroot//"2.tmp", cotmp,
		        "RA,DEC,EXPID", "c1,c2,c3", 0:00:02, incol1="", incol2="",
			factor="15,1,1", sphere+, diagfile="", >& "dev$null")
		    fd1 = cotmp
		    while (fscan(fd1,im1,row)!=EOF) {
			if (substr(im1,1,1) == '#')
			    next
			hedit (im1, "CORA", "INDEF", show-, verify-, update+)
			hedit (im1, "CODEC", "INDEF", show-, verify-, update+)
			partab ("INDEF", coinfo, "RA", row)
			partab ("INDEF", coinfo, "DEC", row)
		    }
		    fd1 = ""
		    delete (coroot//"*.tmp", verify-)
		}
		fd = ""
		key = 'D'; next
	    } else if (key == 'E') {
		# Delete exposure
	        j = wcs / 100; i = mod (wcs, 100)
		colist = coroot // j // ".list"
		tabpar (colist, "C6", i)
		i = int (tabpar.value)
		cotmp = coroot // ".tmp"
		match ("INDEF", coinfo, stop+, > cotmp)
		fd = cotmp
		while (fscan(fd,im1,row,x,y,k,l)!=EOF) {
		    if (substr(im1,1,1) == '#')
			next
		    if (l != i)
		        next
		    hedit (im1, "CORA", "INDEF", show-, verify-, update+)
		    hedit (im1, "CODEC", "INDEF", show-, verify-, update+)
		    partab ("INDEF", coinfo, "RA", row)
		    partab ("INDEF", coinfo, "DEC", row)
		}
		fd = ""; delete (cotmp)
		nmin -= 1
	    } else if (key == 'b') {
		j = wcs / 100; i = mod (wcs, 100)
		comef = coroot // j // ".fits"
		cotmp = coroot // ".tmp"
		mscextensions (comef, output="file", index="",
		    extname="", extver="", lindex=no, lname=yes,
		    lver=no, dataless=no, ikparams="", > cotmp)
		fd = cotmp
		for (j=1; fscan(fd,im2,row)!=EOF; j+=1)
		    display (im2, j, bpm="", >& "dev$null")
		fd = ""; delete (cotmp, verify-)
	    } else if (key == 'p') {
	        if (prev != 0) {
		    i = cluster
		    cluster = prev
		    prev = i
		    key = 'r'; next
		}
	    } else if (key == 'z') {
	        wcs = zframe * 100
		key = 'r'; next
	    } else if (key == '?') {
	        page (cohelp)
	    } else if (key == '-') {
	        dir = -1
		prev = cluster
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		key = 'r'; next
	    } else if (key == '+') {
	        dir = 1
		prev = cluster
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		key = 'r'; next
	    } else {
	        dir = 1
		prev = cluster
	        cluster = mod (ncluster+cluster-1+dir, ncluster) + 1
		key = 'r'; next
	    }

	    # Display cutouts.
	    if (f > 0) {

		# Extract cutouts and set overlay.
		# Note we have to change the physical coordinate system
		# because currently multiple tiles with the same physical
		# coordinates don't work correctly.

		colist = coroot // f // ".list"
		comef = coroot // f // ".fits"
		cocat = coroot // f // ".cat"
		cotmp = coroot // ".tmp"

		fd = colist; flpr
		for (i=0; fscan (fd, im1, row) != EOF; i += 1) {
		    if (substr(im1,1,1) == '#') {
		        i -= 1
		        next
		    }
		    if (substr(im1,1,1) == '-')
		        break
		    if (i == 0) {
		        imdelete (comef, verify-, >& "dev$null")
			delete (cocat, verify-, >& "dev$null")
		    }
		    extn = substr (im1, stridx("[",im1)+1, stridx("]",im1)-1)
		    im2 = comef//"[" // extn // ",append]"
		    imcopy (im1, im2, verb-)
		    im2 = comef//"[" // extn // "]"
		    printf ("Cluster %d / %d\n", cluster, ncluster) |
		        scan (line)
		    hedit (im2, "TITLE", line, show-, verify-, update+)
		}
		fd = ""

		# Separate overlapping tiles.
		mscselect (comef, "DETSEC,EXTNAME", expr="yes", extnames="") |
		    translit ("STDIN", "[:,]", " ", del-, > cotmp)
		fd = cotmp; c1last=0; dltv1 = 0; dltv2 = 0
		while (fscan (fd, c1, c2, l1, l2, extn) != EOF) {
		    im2 = comef // "[" // extn // "]"
		    if (c1 == c1last) {
			i = l1
		        l1 = l1last + nl + 2
			nl = l2 - i + 1
		        l2 = l1 + nl - 1
			dltv2 += 1000
			printf ("[%d:%d,%d:%d]\n", c1, c2, l1, l2) |
			    scan (key)
			hedit (im2, "DETSEC", key,
			    update+, show-, verify-)
			hselect (im2, "$LTV1,$LTV2", yes, missing="0.") |
			    scan (ltv1, ltv2)
			ltv1 -= dltv1; ltv2 -= dltv2
			hedit (im2, "LTV1", ltv1, add+, update+, show-, verify-)
			hedit (im2, "LTV2", ltv2, add+, update+, show-, verify-)
		    } else {
			hselect (im2, "$LTV1", yes, missing="0.") | scan (ltv1)
			ltv1 -= dltv1
			hedit (im2, "LTV1", ltv1, add+, update+, show-, verify-)
			nl = l2 - l1 + 1
			dltv1 += 1000 
		        dltv2 = 0
		    }
		    c1last = c1
		    l1last = l1

		    flpr
		    ra = INDEF; dec = INDEF
		    hselect (im2, "CORA,CODEC", yes) | scan (ra, dec)
		    if (isindef(ra) || isindef(dec))
			print (ra, dec, >> cocat)
		    else {
			print (ra, dec) |
			wcsctran ("STDIN", "STDOUT", im2, "world", "physical",
			    columns="1 2", units="h n", formats="",
			    min_sigdigit=9, verbose-, >> cocat)
		    }
		}
		fd = ""; delete (cotmp, verify-)

		# Display cutouts.
		if (access(comef)) {
		    printf ("Cluster %d / %d\n", cluster, ncluster)
		    acetvmark (comef, frame=f, erase=yes, catalog=cocat,
			fields="C1,C2", catfilter="", wcs="physical",
			mark="circle", radii=radii,
			zcombine="none", xgap=0, ygap=0, > "dev$null")
		} else
		    printf ("Cluster %d: No Data\n", cluster)

		f = 0
	    }

	    # Read cursor.
rdcursor:
	    if (fscan (imcur, x, y, wcs, key, cmd) == EOF)
	        key = "q"
	}

	if (key == 'q')
	    delete (coroot//'*[pst]', verify-)
end
