# Script to make MEF format and overlay with catalog.

list = "mef.list"
for (j=1; fscan(list,s1)!=EOF; j+=1) {

    # Create MEF if needed.
    if (access(s1//".fits")==NO) {
	mkglbhdr (s1//"-ccd![1-8]_s.fits", s1)
	for (i=1; i<=8; i+=1) {
	    s2 = s1 // "-ccd"//i//"_s.fits"
	    if (imaccess(s2))
		imcopy (s2, s1//"[im"//i//",append,inherit]", verbose+)
	    ;
	}
    }
    ;

    # Display and overlay.
    delete ("doit.tmp")
#    files ("*_imov.cat") | scan (s2)
#    acefilter ("NONE", icatalogs=s2,
#	ocatalogs="doit.tmp", nmaxrec=INDEF, iobjmasks="",
#	oobjmasks="", catfilter="EXPID=="//j, logfiles="", verbose=0,
#	extnames="", omtype="all", catomid="NUM", update=no)
    files ("*_mmov.cat") | scan (s2)
    acefilter ("NONE", icatalogs=s2,
	ocatalogs="doit.tmp", nmaxrec=INDEF, iobjmasks="",
	oobjmasks="", catfilter="EXPID=="//j, logfiles="", verbose=0,
	extnames="", omtype="all", catomid="NUM", update=no)
#    acefilter ("NONE",
#	icatalogs="junk.tmp",
#	ocatalogs="doit.tmp", nmaxrec=INDEF, iobjmasks="",
#	oobjmasks="", catfilter="EXPID=="//j, logfiles="", verbose=0,
#	extnames="", omtype="all", catomid="NUM", update=no)
    acetvmark (s1, catalog="doit.tmp",
	frame=j, erase=yes, fields="ra,dec", catfilter="",
	wcs="world", extnames="", mark="circle", radii="10", lengths="0",
	font="raster", color=204, label=no, nxoffset=0, nyoffset=0,
	pointsize=3, txsize=1, zcombine="auto", zscale=yes, z1=0., z2=1000.,
	xgap=36., ygap=72.)
    delete ("doit.tmp")
}
list = ""
