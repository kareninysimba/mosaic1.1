#!/bin/env pipecl
#
# MOVFIND -- Find moving sources from difference catalogs.

logout 1

# Add source identifiers to the catalogs so that we can merged the catalogs.

struct	*fd, wcs
int	l, m, n, nim, ncl, nout
real	cotsep, avtsep
string	ilist, movdata, im, cofile, coimid, s4
string	cat, icat, mcat, ocat, pcat, acat, bcat, ccat, dcat, tcat

# Tasks and packages.
utilities
lists
noao
nproto
nfextern
ace
aceproto
cache	tinfo, tstat

# Paths and files.
names ("mov", envget("OSF_DATASET"))
ilist = names.indir // names.dataset // ".mov"
cofile = names.shortname // "_mov"
icat = names.shortname // "_imov.cat"
mcat = names.shortname // "_mmov.cat"
movdata = names.src // "pipedata/"
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in data directory.
cd (names.datadir)

# Log start of processing.
printf ("\nMOVFIND (%s): ", names.dataset) | tee (names.lfile)
time | tee (names.lfile)

# Get catalogs.
delete ("*diff.cat,movfind*.tmp")
match ("diff.cat", names.indir//names.dataset//".mov", > "movfind0.tmp")

copy ("@movfind0.tmp", ".")
files ("*diff.cat", sort+, > "movfind0.tmp")

# Add new identifiers for merged catalog.
list = "movfind0.tmp"; s1 = ""; nim = 0
for (i=10; fscan(list,cat)!=EOF; i+=tinfo.nrows) {
    # Filter the catalog.
    thselect (cat, "CRMIN1,CRMAX1,CRMIN2,CRMAX2", yes) | scan (j, k, l, m)
    j += 15; k -= 15; l += 15; m -= 15
    printf ("X>%d&&X<%d&&Y>%d&&Y<%d\n", j, k, l, m) | scan (s2)
    acefilter ("NONE", icatalogs=cat, ocatalogs="movfind1.tmp",
	nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	catfilter=s2, logfiles="", verbose=0, extnames="", omtype="all",
	catomid="NUM", update-)
    flpr
    rename ("movfind1.tmp", cat)
    x = 3
    printf ("B>0.4&&ISIGMAX>max(%.1f,min(6*ISIGAVG-6,1.1*ISIGAVG+3.8))\n", x) |
        scan (s2)
    acefilter ("NONE", icatalogs=cat, ocatalogs="movfind1.tmp",
	nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	catfilter=s2, logfiles="", verbose=0, extnames="", omtype="all",
	catomid="NUM", update-)
    flpr
    rename ("movfind1.tmp", cat)
    tinfo (cat, ttout-)
    for (x=5.; tinfo.nrows > 200 && x<=6.; x+=1.) {
	printf ("ISIGMAX>max%.1f\n", x) | scan (s2)
	acefilter ("NONE", icatalogs=cat, ocatalogs="movfind1.tmp",
	    nmaxrec=INDEF, iobjmasks="", oobjmasks="",
	    catfilter=s2, logfiles="", verbose=0, extnames="", omtype="all",
	    catomid="NUM", update-)
	flpr
	rename ("movfind1.tmp", cat)
	tinfo (cat, ttout-)
    }
    ;

    s2 = substr (cat, 1, strldx("-",cat)-1)
    coimid = substr (cat, strldx("-",cat)+1, strldx("_",cat)-1)
    if (s1 != s2) {
        nim += 1
	s1 = s2
    }
    ;

    thedit (cat, "COEXPID", nim, del-, show-)
    thedit (cat, "COIMID", coimid, del-, show-)

    if (tinfo.nrows == 0)
        next
    ;

    tcalc (cat, "COEXPID", nim, datatype="int")
    tcalc (cat, "NUM1", i//"+ROWNUM", datatype="int")
    thselect (cat, "MJD-OBS,EXPTIME", yes) | scan (x, y)
    z = x + y / 2 / 86400
    tcalc (cat, "MJD", z, datatype="double")
    tcalc (cat, "EXP", y, datatype="real")
    tinfo (cat, ttout-)
}


# Merge the catalogs. Reverse the list to get the header we want.
delete (icat, >& "dev$null")
sort ("movfind0.tmp", reverse+, > "movfind0r.tmp")
tmerge ("@movfind0r.tmp", icat, "append", allcols=yes,
    tbltype="default", allrows=100, extracol=0)

# Make asteroid catalogs.
acat = "movfinda.tmp"
bcat = "movfindb.tmp"
ccat = "movfindc.tmp"
dcat = "movfindd.tmp"
ocat = "movfindo.tmp"
pcat = "movfindp.tmp"
tcat = "movfindt.tmp"
delete ("movfind[abcdopt].tmp")

while (YES) {
    # Create pair catalog. Spatial limits are in arc seconds.
    for (x = 60.; x >= 10.; x -= 2.) {
	acepalign (icat, ocat, pcat, acat, bcat,
	    icatdef=movdata//"acepairs.dat", ocatdef="", pcatdef="",
	    filter="", iwin=4, minsep=1.0, maxsep=x, minrate=0.1,
	    maxrate=120., maxdm=1.5, maxdw=1.0, maxde=0.2, maxdp=15,
	    type="moving", wempty=no, maxdpp=4., maxdr=-0.6, align=0.)
	if (nim >= 5 && access(acat)==YES)
	    rename (acat, pcat)
	;
	if (access(pcat) == YES) {
	    tinfo (pcat, ttout-)
	    ncl = tinfo.nrows
	} else
	    ncl = 0
	if (ncl < 500.)
	    break
	;
    }
    if (ncl > 0) {
	# Add columns for clustering.
	tcalc (pcat, "COSP", "cos(P/57.296)", datatype="real",
	    colunits="", colfmt="%.6g")
	tcalc (pcat, "SINP", "sin(P/57.296)", datatype="real",
	    colunits="", colfmt="%.6g")
	tcalc (pcat, "DH", "(X*COSP+Y*SINP)-(RATE/3600*U)", datatype="real",
	    colunits="arcsec", colfmt="%.6g")

	# Cluster the pairs along lines.
	if (nim < 4)
	   ncl = 1
	else if (nim < 5)
	   ncl = 2
	else
	   ncl = 3
	acecluster (pcat, ccat, "30,30,60,-0.3", mincluster=ncl,
	    icatdef=movdata//"acecluster.dat",
	    ocatdef=movdata//"acecluster1.dat",
	    ifilter="", ofilter="")
	if (access(ccat) == YES) {
	    tinfo (ccat, ttout-)
	    ncl = tinfo.nrows
	} else
	    ncl = 0
	if (ncl > 0) {
	    # Set the average standard coordinates at the average time.
	    thselect ("@movfind0.tmp", "TSEP", yes) | average | scan (avtsep)
	    tcalc (ccat, "AVDH", "AVDH+AVRATE/3600*"//avtsep, datatype="real",
		colunits="arcsec", colfmt="%.4f")
	    tcalc (ccat, "AVXI", "AVXH+AVDH*AVCOSP", datatype="real",
		colunits="arcsec", colfmt="%.4f")
	    tcalc (ccat, "AVETA", "AVYH+AVDH*AVSINP", datatype="real",
		colunits="arcsec", colfmt="%.4f")
	    tcalc (ccat, "AVP", "57.296*atan2(AVSINP,AVCOSP)", datatype="real",
		colunits="arcsec", colfmt="%.4f")

	    # Make catalog of clusters.
	    tproject (ccat, dcat,
		"COGRPID AVXH AVYH AVDH AVRATE AVCOSP AVSINP AVXI AVETA AVP",
		uniq+)
	    tinfo (dcat, ttout-)
	    ncl = tinfo.nrows

	    # Expand the pair catalog to include cluster information.
	    tjoin (pcat, ccat, tcat, "N", "N", extrarows="neither",
	        tolerance="0.0")
	    rename (tcat, pcat)

	    # Separate pairs into unique sources.
	    tproject (pcat, "movfind2.tmp",
	        "NA COGRPID AVXI AVETA AVRATE", uniq-)
	    tchcol ("movfind2.tmp", "NA", "N", "", "", verbose-)
	    tproject (pcat, "movfind3.tmp",
	        "NB COGRPID AVXI AVETA AVRATE", uniq-)
	    tchcol ("movfind3.tmp", "NB", "N", "", "", verbose-)
	    tmerge ("movfind2.tmp,movfind3.tmp", "movfind4.tmp", "append",
		allcols=yes, tbltype="default")
	    delete ("movfind[23].tmp")
	    tproject ("movfind4.tmp", "movfind2.tmp",
	        "N COGRPID AVXI AVETA AVRATE", uniq+)
	    tproject (icat, "movfind3.tmp", "NUM1 COEXPID")
	    tjoin ("movfind2.tmp", "movfind3.tmp", tcat, "N", "NUM1")
	    rename (tcat, ocat)
	    delete ("movfind[234].tmp")
	    
	    # Require a minimum number of unique exposures in a cluster.
	    i = nim / 2
	    tproject (ocat, "movfind2.tmp", "COGRPID COEXPID", uniq+)
	    thist ("movfind2.tmp", "STDOUT", "COGRPID", nbins=INDEF,
	        lowval=INDEF, highval=INDEF, dx=1., clow=INDEF, chigh=INDEF,
		rows="-", outcolx="COGRPID", outcoly="CONEXPID",
		> "movfind3.tmp")
	    tchcol ("movfind3.tmp", "C1", "COGRPID", "%d", "", verb-)
	    tchcol ("movfind3.tmp", "C2", "CONEXPID", "%d", "", verb-)
	    tselect ("movfind3.tmp", "movfind4.tmp", "CONEXPID>="//i)
	    tinfo ("movfind4.tmp", ttout-)
	    ncl = tinfo.nrows
	    if (ncl > 0) {
		tcalc ("movfind4.tmp", "REGRPID", "ROWNUM", datatype="int")
		tjoin (ocat, "movfind4.tmp", "movfind5.tmp", "COGRPID",
		    "COGRPID", extra="neither", tol=0.0, case-)
		delete (ocat)
		tproject ("movfind5.tmp", ocat, "!COGRPID", uniq-)
		tchcol (ocat, "REGRPID", "COGRPID", "", "", verbose-)
		tjoin (dcat, "movfind4.tmp", "movfind6.tmp", "COGRPID",
		    "COGRPID", extra="neither", tol=0.0, case-)
		delete (dcat)
		tproject ("movfind6.tmp", dcat, "!COGRPID", uniq-)
		tchcol (dcat, "REGRPID", "COGRPID", "", "", verbose-)
	    }
	    ;
	    delete ("movfind[23456].tmp")
	}
	;
    }
    ;

    # Create cutouts and final catalogs.
    imdelete (cofile, >& "dev$null")

    # Make global header.
    match ("fits", ilist, stop-) | sort ( > "movfind2.tmp")
    mkglbhdr ("@movfind2.tmp", cofile, ref="", exclude="")

    # Add some global keywords.
    hedit (cofile, "CONGRP", ncl, add+)
    hedit (cofile, "CODATA", names.shortname, add+)
    hedit (cofile, "COFILE", cofile, add+)
    head ("movfind2.tmp", nl=1) | scan (im)
    hselect (im, "DATE-OBS,MJD-OBS,LSTHDR,ST", yes) | scan (s1, x, y)
    hedit (cofile, "CODATE", s1, add+)
    hedit (cofile, "COMJD", x, add+); hedit (cofile, "COMJD", x)
    printf ("%10.1h\n", y) | scan (s3)
    hedit (cofile, "COLST", "", add+); hedit (cofile, "COLST", s3)
    hedit (cofile, "CONEXP", nim, add+)
    hedit (cofile, "COFILTER", "(FILTER)", add+)

    # Average pointing.
    hselect ("@movfind2.tmp", "CRVAL1,CRVAL2", yes, > "movfind3.tmp")
    tstat ("movfind3.tmp", "c1", outtable="")
    printf ("%13.2H\n", tstat.mean) | scan (s1)
    hedit (cofile, "CORACEN", "", add+);  hedit (cofile, "CORACEN", s1)
    tstat ("movfind3.tmp", "c2", outtable="")
    printf ("%13.1h\n", tstat.mean) | scan (s1)
    hedit (cofile, "CODECCEN", "", add+);  hedit (cofile, "CODECCEN", s1)
    hselect (cofile, "CORACEN,CODECCEN", yes, > "movfind3.tmp")
    acecopy ("movfind3.tmp", "movfind4.tmp",
        catdef="#ELON(c1,c2)\nELAT(c1,c2)", filter="", verb-)
    tdump ("movfind4.tmp", cdfile="", pfile="", datafile="STDOUT", columns="",
	rows="-", pwidth=-1) | scan (x, y)
    printf ("%13.1h %13.1h\n", x, y) | scan (s1, s2)
    hedit (cofile, "COGAMMA", "", add+); hedit (cofile, "COGAMMA", s1)
    hedit (cofile, "COBETA", "", add+); hedit (cofile, "COBETA", s2)
    delete ("movfind[234].tmp")

    # Exposure information.
    thselect ("@movfind0.tmp", "IMCMBMEF,COEXPID,TSEP,EXPTIME", yes,
        > "movfind2.tmp")
    tproject ("movfind2.tmp", "movfind3.tmp", "", uniq+)
    tdump ("movfind3.tmp", cdfile="", pfile="", datafile="movfind4.tmp",
        columns="", rows="-", pwidth=-1)
    list = "movfind4.tmp"
    for (i=1; fscan(list,s1,j,x,y)!=EOF; i+=1) {
        hedit (cofile, "COIM"//i, s1, add+)
        hedit (cofile, "COEXPI"//i, i, add+)
        hedit (cofile, "COTSEP"//i, x, add+)
        hedit (cofile, "COEXPT"//i, y, add+)
    }
    list = ""
    delete ("movfind[234].tmp")

    if (ncl == 0)
        break
    ;

    list = "movfind0.tmp"
    for (nout=0; fscan(list,cat)!=EOF;) {
	thselect (cat, "IMAGE,COEXPID,COIMID,TSEP,RSPRA,RSPDEC", yes) |
	    scan (s1, i, coimid, cotsep, x, y)
	z = cotsep - avtsep
	s1 = substr (s1, 1, strstr(".fits",s1)-1)
	match (s1//"?*.fits", ilist, stop-) | scan (im)
	printf ("gnomonic %g %g\n", 15*x, y) | scan (wcs)

	# Catalog of clusters.
	tcalc (dcat, "COEXPID", i, datatype="int")
	tcalc (dcat, "DH", "AVDH-AVRATE/3600*"//z, datatype="real",
	    colunits="arcsec", colfmt="%.4f")
	tcalc (dcat, "XI1", "AVXH+DH*AVCOSP", datatype="real",
	    colunits="arcsec", colfmt="%.4f")
	tcalc (dcat, "ETA1", "AVYH+DH*AVSINP", datatype="real",
	    colunits="arcsec", colfmt="%.4f")

	# Object catalog.
	tinfo (cat, ttout-)
	if (tinfo.nrows > 0) {
	    tjoin (ocat, cat, tcat, "N COEXPID", "NUM1 COEXPID",
	        extra="neither") 
	    rename (tcat, cat)
	}
	;

	# Cutouts
	if (tinfo.nrows > 0) {
	    tproject (dcat, "movfind2.tmp",
		"COEXPID COGRPID XI1 ETA1 AVXI AVETA AVRATE", uniq-)
	    tproject (cat, "movfind3.tmp",
		"COEXPID COGRPID RA DEC MAG XI ETA AVXI AVETA", uniq-)
	    tjoin ("movfind2.tmp", "movfind3.tmp", "movfind4.tmp",
		"COEXPID,COGRPID,AVXI,AVETA", "COEXPID,COGRPID,AVXI,AVETA",
		extrarows="first", tolerance="0.0")
	    delete ("movfind[23].tmp")

	    # Because ttools doesn't handle INDEFs well we need to manually
	    # create the cutout catalog.

	    tprint ("movfind4.tmp", prparam-, prdata+, pwidth=1024,
		plength=0, showrow-, showhdr-,
	        columns="XI,ETA,XI1,ETA1,AVXI,AVETA,AVRATE,COEXPID,COGRPID,RA,DEC,MAG",
		rows="-", option="plain", align-, > "movfind3.tmp")
	    delete ("movfind4.tmp")

	    print ("#c XI d %.4f arcsec", > "movfind2.tmp")
	    print ("#c ETA d %.4f arcsec", >> "movfind2.tmp")
	    print ("#c AVXI d %.4f arcsec", >> "movfind2.tmp")
	    print ("#c AVETA d %.4f arcsec", >> "movfind2.tmp")
	    print ("#c CORATE d %.4f arcsec/hr", >> "movfind2.tmp")
	    print ("#c COEXPID i %11d", >> "movfind2.tmp")
	    print ("#c COGRPID i %11d", >> "movfind2.tmp")
	    print ("#c RA d %13.3h hr", >> "movfind2.tmp")
	    print ("#c DEC d %13.2h deg", >> "movfind2.tmp")
	    print ("#c COMAG r %6.2f", >> "movfind2.tmp")

	    print ("#c COIMID ch*8 %11d", >> "movfind2.tmp")
	    print ("#c CONOBS i %d", >> "movfind2.tmp")
	    print ("#c COTSEP i %d sec", >> "movfind2.tmp")
	    print ("#c COPA r %6.1f", >> "movfind2.tmp")

	    fd = "movfind3.tmp"
	    for (; fscan (fd, s1, s2, s3, s4, line) != EOF; nout+=1) {
		if (s1=="INDEF")
		    printf ("%s %s %s %s 1 %d 0.\n",
		        s3, s4, line, coimid, cotsep, >> "movfind2.tmp")
		else
		    printf ("%s %s %s %s 1 %d 0.\n",
		        s1, s2, line, coimid, cotsep, >> "movfind2.tmp")
	    }
	    fd = ""; delete ("movfind3.tmp")
	} else {
	    tproject (dcat, "movfind4.tmp",
		"COEXPID COGRPID XI1 ETA1 AVXI AVETA AVRATE", uniq-)
	    tprint ("movfind4.tmp", prparam-, prdata+, pwidth=1024,
		plength=0, showrow-, showhdr-,
	        columns="XI1,ETA1,AVXI,AVETA,AVRATE,COEXPID,COGRPID",
		rows="-", option="plain", align-, > "movfind3.tmp")
	    delete ("movfind4.tmp")

	    print ("#c XI d %.4f arcsec", > "movfind2.tmp")
	    print ("#c ETA d %.4f arcsec", >> "movfind2.tmp")
	    print ("#c AVXI d %.4f arcsec", >> "movfind2.tmp")
	    print ("#c AVETA d %.4f arcsec", >> "movfind2.tmp")
	    print ("#c CORATE d %.4f arcsec/hr", >> "movfind2.tmp")
	    print ("#c COEXPID i %11d", >> "movfind2.tmp")
	    print ("#c COGRPID i %11d", >> "movfind2.tmp")

	    print ("#c RA d %13.3h hr", >> "movfind2.tmp")
	    print ("#c DEC d %13.2h deg", >> "movfind2.tmp")
	    print ("#c COMAG r %6.2f", >> "movfind2.tmp")
	    print ("#c COIMID ch*8 %11d", >> "movfind2.tmp")
	    print ("#c CONOBS i %d", >> "movfind2.tmp")
	    print ("#c COTSEP i %d sec", >> "movfind2.tmp")
	    print ("#c COPA r %6.1f", >> "movfind2.tmp")

	    fd = "movfind3.tmp"
	    for (; fscan (fd, line) != EOF; nout+=1) {
		printf ("%s INDEF INDEF INDEF %s 1 %d 0.\n",
		    line, coimid, cotsep, >> "movfind2.tmp")
	    }
	    fd = ""; delete ("movfind3.tmp")
	}

	# For now we quit if the number is too large.
	if (nout > 200)
	    break
	;
	
	acecutout (im, "movfind2.tmp", cofile, coords="XI,ETA,RA,DEC",
	    wcs=wcs, tiles="COEXPID,COGRPID", ncpix=300, nlpix=300,
	    blkavg=1, gap=2, extname="XA",
	    fields="COGRPID,COEXPID,COIMID,CONOBS,COTSEP,CORATE,COPA,COMAG")
#	acecutout (im, "movfind2.tmp", cofile, coords="AVXI,AVETA,RA,DEC",
#	    wcs=wcs, tiles="COEXPID,COGRPID", ncpix=300, nlpix=300,
#	    blkavg=2, gap=6, extname="XB",
#	    fields="COGRPID,COEXPID,COIMID,CONOBS,COTSEP,CORATE,COPA,COMAG")
	delete ("movfind2.tmp")
    }
    list = ""

    break
}

# Merge the catalogs.
delete (mcat, >& "dev$null")
if (ncl > 0)
    tmerge ("@movfind0r.tmp", mcat, "append", allcols=yes,
	tbltype="default", allrows=100, extracol=0)
;

# Clean up.
delete ("@movfind0.tmp")
delete ("movfind*.tmp")

# Temporary
rename (ilist, ilist//"done")

logout 1
