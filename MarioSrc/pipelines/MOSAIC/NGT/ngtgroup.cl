#!/bin/env pipecl
#
# NGTGROUP -- Group observations.

string	dataset, indir, datadir
file	inlist, zlist, flist, olist, ilist, lfile
struct	enid
struct	*fd1, *fd2

# Set filenames.
names ("ngt", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile

# Load packages
images

# Log start of processing.
printf ("\nNGTGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Place files in the data subdirectory of the input directory.
cd (indir // "data")

# Create group list files.
inlist = indir // dataset // ".ngt"
zlist = indir // dataset // ".zngt"
flist = indir // dataset // ".fngt"
olist = indir // dataset // ".ongt"
ilist = indir // dataset // ".ingt"
ngtgroup ("@"//inlist, zlist, flist, olist, ilist, skyflat=cal_skyflat,
    verbose+)

# Check calibration sequence lengths.
if (access(zlist)) {
    list = zlist
    while (fscan (list, s1) != EOF) {
        i=0; count (s1) | scan (i)
	if (i <= cal_nseqmax)
	    next
	;
	sendmsg ("WARNING", "Too many in calibration sequence",
	    s1//" "//str(i), "CAL")
	head (s1, nl=cal_nseqmax, > "ngtgroup.tmp")
	rename ("ngtgroup.tmp", s1)
    }
    list = ""
}
;
if (access(flist)) {
    list = flist
    while (fscan (list, s1) != EOF) {
        i=0; count (s1) | scan (i)
	if (i <= cal_nseqmax)
	    next
	;
	sendmsg ("WARNING", "Too many in calibration sequence",
	    s1//" "//str(i), "CAL")
	head (s1, nl=cal_nseqmax, > "ngtgroup.tmp")
	rename ("ngtgroup.tmp", s1)
    }
    list = ""
}
;

# Log name mappings.
printf ("\nName mappings:\n") | tee (lfile)
if (access(zlist)) {
    list = zlist
    while (fscan (list, s1) != EOF) {
        fd1 = s1
	s1 = substr (s1, strldx("/",s1)+1,1000)
	while (fscan (fd1, s2) != EOF) {
	    fd2 = s2
	    s2 = substr (s2, strldx("/",s2)+1,1000)
	    while (fscan (fd2, s3) != EOF) {
	    	enid=""; hselect (s3//"[0]", "enid", yes) | scan (enid)
		s3 = substr (s3, strldx("/",s3)+1,1000)
		printf ("%s %s %s %s\n", enid, s3, s2, s1) | tee (lfile)
	    }
	    fd2 = ""
	}
	fd1 = ""
    }
    list = ""
}
;
if (access(flist)) {
    list = flist
    while (fscan (list, s1) != EOF) {
        fd1 = s1
	s1 = substr (s1, strldx("/",s1)+1,1000)
	while (fscan (fd1, s2) != EOF) {
	    fd2 = s2
	    s2 = substr (s2, strldx("/",s2)+1,1000)
	    while (fscan (fd2, s3) != EOF) {
	    	enid=""; hselect (s3//"[0]", "enid", yes) | scan (enid)
		s3 = substr (s3, strldx("/",s3)+1,1000)
		printf ("%s %s %s %s\n", enid, s3, s2, s1) | tee (lfile)
	    }
	    fd2 = ""
	}
	fd1 = ""
    }
    list = ""
}
;
if (access(olist)) {
    list = olist
    while (fscan (list, s1) != EOF) {
        fd1 = s1
	s1 = substr (s1, strldx("/",s1)+1,1000)
	while (fscan (fd1, s2) != EOF) {
	    enid=""; hselect (s2//"[0]", "enid", yes) | scan (enid)
	    s2 = substr (s2, strldx("/",s2)+1,1000)
	    printf ("%s %s %s\n", enid, s2, s1) | tee (lfile)
	}
	fd1 = ""
    }
    list = ""
}
;
	        
delete (inlist)
if (access (olist))
    rename (olist, inlist)
;

# If any input exposures were ignored return a warning status.
if (access(ilist) == NO)
    logout 1
else {
    count (ilist) | scan (i)
    sendmsg ("WARNING", "Images ignored", i, "VRFY")
    logout 2
}
