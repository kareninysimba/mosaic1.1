#!/bin/env pipecl

string  indir, datadir, dataset
string  temp, ds_list, filter_ds, ds, ds0, fltr
string  ext_list, pattern, odir, element, temp2
string  extn, stage, oname, rfile, olist, trfile
struct  f, f0
int     n_min, n, n_trgs
int	founddir
real    size, expand

# Get arguments.
if (fscan (cl.args, extn, stage) != 2)
    logout 0
else
    ;

print ("Defining external tasks and variables...")
# external tasks and packages
task $grep = "$!grep"
task $awk = "$!awk.sh"
task $basename = "$!basename"
task $pipeselect = "$!pipeselect"
#pipeconfig
images
utilities
mario

dataset = envget ("OSF_DATASET")

# Check if FRG pipeline is running and skip if not found.
odir = ""
pipeselect (envget ("NHPPS_SYS_NAME"), "frg", 1, 0) | scan (odir)
if (odir == "") {
    sendmsg ("WARNING", "Pipeline not running", "frg", "PIPE")
    logout 3
} else
    ;

# minimum number of images to generate a Sky Flat
n_min = pipeconfig.sfat_nmin

indir = "NHPPS_DIR_ROOT$input/"
datadir = "NHPPS_DIR_ROOT$data/" // dataset // "/"
set (uparm = datadir//"uparm/")
olist = datadir // dataset // "." // extn
print ("Done.")

# Work in the data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}
cd (datadir)

print ("Extracting list of files (from SIF pipeline)...")
# extract CCD 1 SIF outputs
ds_list = mktemp (dataset)
grep ("-Eh", "[0-9]\{8\}T[0-9]\{6\}_ccd1\.fits", dataset // "*.omef", > ds_list)
print ("Done.")

print ("Determine file size (per extension)...")
head (ds_list, nlines=1) | scan (ds0)
size = -1
filesize (ds0) | scan (size)
if (size == -1) {
    sendmsg ("ERROR", "Cannot determine file size", ds0, "VRFY")
    logout 0
} else
    ;

# take dataset expansion into consideration
expand = 1
expand = pipeconfig.expansion
size = size * expand
print ("Done.")

print ("Extracting filter names...")
# extract the filter names
temp = mktemp (dataset)

hselect ("@"//ds_list, "$I, filter", yes, > temp)
print ("Done.")

print ("Sorting image list by filters...")
# sort the list by filter
filter_ds = mktemp (dataset)

sort (temp, column=2, > filter_ds)
delete (temp, ver-)

# start the grouping by filter
head (filter_ds, nlines=1) | scan (ds0, f0)
print ("Done.")

print ("Counting number of (MEF) extensions...")
# now we need to figure out the number of extensions!
# we assume that all the original MEFs had the same 
# number of extensions
basename (ds0, "1.fits") | scan (ds)

ext_list = mktemp ("/tmp/" // dataset)
pattern = ds // "[0-9]{1,}\.fits"

grep ("-Eh", '"'//pattern//'"', "*.omef", > temp)
list = temp
while (fscan (list, s1) != EOF) {
    awk (s1, '\'{n=split ($0, a, "/"); split (a[n], b, "_ccd"); split (b[2], c, ".fits"); print (c[1])}\'', >> ext_list)
}
list = ""
delete (temp, ver-)

sort (ext_list, numeric_sort+, > temp)
tail (temp, nlines=1) | scan (n)
print ("Done.")

print ("Creating trigger and return files...")
list = filter_ds
n_trgs = 0
j = 0
# remove the spaces, if any, from the filter name
print (f0) | translit ("STDIN", " ", de+) | scan (fltr)
ds_list = dataset // "_" // fltr // "_ccd"
while (fscan (list, ds, f) != EOF) {
    basename (ds, "1.fits") | scan (ds)
    if (f == f0) {
        # add the dataset to the list, each extension to its file
        i = 1
        while (i<=n) {
            pattern = ds // i // "\.fits"
            grep ("-h", pattern, "*.omef", >> ds_list // i // ".frg")
            i = i + 1
        }
        # increment the counter
        j = j + 1
    } else {
        # we should close the previous list
        # and save it if we have enough files
        # or delete it otherwise.
        if (j < n_min) {
            # not enough files!
            printf ("Not enough data: %d < %d\n", j, n_min)
            
            delete (ds_list // i // ".frg", ver-)
        } else {
            # move the @files and create trigger files
            i = 1
            while (i<=n) {
                n_trgs = n_trgs + 1
                
                # choose a host for this extension. Take file sizes
                # into consideration as well!
                pipeselect (envget ("NHPPS_SYS_NAME"), "frg", 1, 0, size) | scan (odir)
                
                # setup the corresponding return file and temporary
                # return file.
                rfile = odir // "return/" // ds_list // "." // extn
                
                # update the return list
                printf ("%s.%s\n", ds_list, extn, >> olist)
                
                # write the return file
                trfile = ds_list // "_" // extn
                pathname (indir//ds_list//"."//extn, > trfile)
                rename (trfile, rfile)
                delete (trfile, verify-)
                
                # copy the file list and create a trigger file
                copy (ds_list // i // ".frg", odir // "/")
                copy (ds_list // i // ".frg", odir // "/" // ds_list // i // ".frgtrig")
                delete (ds_list // i // ".frg")
                
                # go to the next extension
                i = i + 1
            }
        }
        # start fresh with a new filter and a new list
        f0 = f
        print (f0) | translit ("STDIN", " ", de+) | scan (fltr)
        ds_list = dataset // "_" // fltr // "_ccd"
        # add the dataset to the list, each extension to its file
        i = 1
        while (i<=n) {
            pattern = ds // i // "\.fits"
            grep ("-h", pattern, "*.omef", >> ds_list // i // ".frg")
            i = i + 1
        }
        # reset the counter
        j = 0
    }
} # <-- end of the while loop

# try and close the last files that did not make 
# it in the "else" clause of the if statement above
# we should close the previous list
# and save it if we have enough files
# or delete it otherwise.
if (j < n_min) {
    # not enough files!
    print ("Not enough data: %d < %d\n", j, n_min)
    
    delete (ds_list, ver-)
} else {
    # move the @file and create a trigger file
    i = 1
    while (i<=n) {
        n_trgs = n_trgs + 1
        
        # choose a host for this extension
        pipeselect (envget ("NHPPS_SYS_NAME"), "frg", 1, 0) | scan (odir)
        
        # setup the corresponding return file and temporary
        # return file.
        rfile = odir // "return/" // ds_list // i // "." // extn
        
        # update the return list
        printf ("%s.%s\n", ds_list//i, extn, >> olist)
        
        # write the return file
        trfile = ds_list// i // "_" // extn
        pathname (indir//ds_list//i//"."//extn, > trfile)
        rename (trfile, rfile)
        delete (trfile, verify-)
        
        # copy the file list and create a trigger file
        copy (ds_list // i // ".frg", odir // "/")
        copy (ds_list // i // ".frg", odir // "/" // ds_list // i // ".frgtrig")
        delete (ds_list // i // ".frg")
        
        # go to the next extension
        i = i + 1
    }
}
print ("Done.")

print ("Clening up...")
# cleanup and exit
delete (temp, ver-)
print ("Done.")

if (n_trgs != 0)
    logout 2
else
    logout 1








