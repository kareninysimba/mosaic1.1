# NGTGROUP -- Take a list of images and group into sequences.
#
# This is specialized for NOAO Mosaic keywords.
# 
# The input template is expanded and sorted by MJDHDR.  The image names are
# stripped of any kernel or image sections to allow categorization of MEF
# files using one extension.  They are expanded to a full path including
# the node so that other nodes may access the files.
# 
# Each exposure is recorded in file with name based on the OBSID.
# Any periods in the OBSID have to be removed because of an OPUS bug.
# All further references to an exposure are through this file.
# 
# A calibration sequence consists of successive exposures in this list which
# have the same OBSTYPE and FILTER and differ in MJDHDR by less than "dtmax".
# Sequences are recorded in file with the OBSID of the first exposure and
# prefixed with S.
# 
# The returned calibration and object lists are lists of sequence or OBSID
# reference files.
# 
# Exposure names with "test" are ignored.
# OBSTYPEs of "sky flat" are treated as objects.
# OBSTYPEs of "focus" are ignored.

procedure ngtgroup (input, zerolist, flatlist, objlist, verbose)

string	input			{prompt="Template of images"}
file	zerolist		{prompt="Output list of zero sequences"}
file	flatlist		{prompt="Output list of flat sequences"}
file	objlist			{prompt="Output list of objects"}
bool	verbose = no		{prompt="Verbose?"}

real	dtmax = 00:03:30	{prompt="Maximum interval between cal exp"}

struct	*fd

begin
	int	i
	file	fnames, output
	string	mjdhdr, mjdhdr1
	string	obsid, obsid1, obsname
	string	fname, fname1
	struct	obstype, obstype1
	struct	filter, filter1

	# Get query parameters.
	string	in
	file	zlist, flist, olist
	bool	vb

	in = input
	zlist = zerolist
	flist = flatlist
	olist = objlist
	vb = verbose

	if (access (zlist))
	    delete (zlist, verify-)
	if (access (flist))
	    delete (flist, verify-)
	if (access (olist))
	    delete (olist, verify-)

	# Define temporary files.
	fnames = mktemp ("tmp$ngt")

	# Expand the template, extract keywords, and sort by observation time.
	# OBSTYPE and FILTER keywords may have spaces so put on separate lines.

	# hselect (in, "mjdhdr,obsid,$I,obstype,filter", yes) |
	hselect (in, "mjd-obs,obsid,$I,obstype,filter", yes) |
	    sort ("STDIN", col=1, ignore-, numeric+, reverse-) |
	    words ("STDIN", > fnames)

	# Read data for first image, then loop on subsequent images to compare
	# if they belong to a sequence with the  preceeding image.  Steps are
	# requred to deal with test exposures and certain OBSTYPE values.
	# The filenames are stripped of any sections.

	fd = fnames
	if (fscan (fd, mjdhdr) == EOF) {
	    fd = ""; delete (fnames, verify-)
	    return
	}
	i = fscan (fd, obsid); i = fscan (fd, fname)
	i = fscan (fd, obstype); i = fscan (fd, filter)
	print (obsid) | translit ("STDIN", ".", "", delete+, collapse-) |
	    pathname ("STDIN") | scan (obsname)
	output = obsname // "S"

	i = stridx ("[", fname) - 1
	if (i > 0)
	    fname = substr (fname, 1, i)
	pathnames (fname) | match ("test[^/]*$", "STDIN", meta+, stop-) |
	    count ("STDIN") | scan (i)
	if (i != 0)
	    obstype = "test"
	if (obstype == "object" || obstype == "sky flat")
	    obstype = "object"
	else if (obstype == "zero")
	    obstype = "zero"
	else if (obstype == "dome flat")
	    obstype = "dflat"
	else
	    obstype = ""

	if (obstype != "") {
	    if (access (obsname))
		 delete (obsname, verify-)
	    pathnames (fname, > obsname)
	    if (vb) {
		printf ("%s (%s):\n", obsname, obstype)
		if (obstype == "zero" || obstype == "dflat")
		    printf ("\t%s (%s)\n", fname, output)
		else
		    printf ("\t%s\n", fname)
	    }
	    fname = obsname
	}

	dt = 0
	while (fscan (fd, mjdhdr1) != EOF) {
	    i = fscan (fd, obsid1); i = fscan (fd, fname1)
	    i = fscan (fd, obstype1); i = fscan (fd, filter1)
	    print (obsid1) | translit ("STDIN", ".", "", delete+,collapse-) |
		pathname ("STDIN") | scan (obsname)

	    i = stridx ("[", fname1) - 1
	    if (i > 0)
		fname1 = substr (fname1, 1, i)
	    pathnames (fname1) | match ("test[^/]*$", "STDIN", meta+, stop-) |
		count ("STDIN") | scan (i)
	    if (i != 0)
		obstype1 = "test"
	    if (obstype1 == "object" || obstype1 == "sky flat")
		obstype1 = "object"
	    else if (obstype1 == "zero")
		obstype1 = "zero"
	    else if (obstype1 == "dome flat")
		obstype1 = "dflat"
	    else
		obstype1 = ""

	    if (obstype1 != "") {
		if (access (obsname))
		     delete (obsname, verify-)
		pathnames (fname1, > obsname)
		if (vb) {
		    printf ("%s (%s):\n", obsname, obstype1)
		    if (obstype == "zero" || obstype == "dflat")
			printf ("\t%s (%s)\n", fname1, output)
		    else
			printf ("\t%s\n", fname1)
		}
		fname1 = obsname

		if (obstype == "zero" || obstype == "dflat") {
		    if (dt == 0) {
			if (access (output))
			     delete (output, verify-)
			pathnames (fname, > output)
		    } else
			pathnames (fname, >> output)
		}
	    }

	    # Here is were we determine the end of a sequence.
	    if (obstype1=="object" || obstype1!=obstype ||
	        filter!=filter1 || dt>dtmax) {
		if (obstype == "zero")
		    print (output, >> zlist)
		else if (obstype == "dflat")
		    print (output, >> flist)
		else if (obstype == "object")
		    print (fname, >> olist)
		mjdhdr = mjdhdr1
		output = obsname // "S"
	    }

	    dt = (real (mjdhdr1) - real (mjdhdr)) * 24
	    mjdhdr = mjdhdr1
	    obsid = obsid1
	    fname = fname1
	    obstype = obstype1
	    filter = filter1
	}
	if (obstype == "zero" || obstype == "dflat")
	    pathnames (fname, >> output)
	if (obstype == "zero")
	    print (output, >> zlist)
	else if (obstype == "dflat")
	    print (output, >> flist)
	else if (obstype == "object")
	    print (fname, >> olist)
	fd = ""; delete (fnames, verify-)
end
