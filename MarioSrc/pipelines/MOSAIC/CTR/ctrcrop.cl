#!/bin/env pipecl
#
# ctrcrop
#
# Description:
#
#	Extract subimage from first-pass (harsh, median) MDC stack to match the field of
#	a stack of resampled SIFs of a particular extension.  This module also assembles
#	lists of this subimage and an associated non-resampled SIF that composed the
#	resampled-SIF stack, and the list of these lists for the call to the STR pipeline.
#	(Each list has only two rows: the first row is the full pathname of the subimage,
#	the second row is the full pathname of a non-resampled SIF.)
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful.  (Currently not set in module)
#
# History:
#
#	T. Huard  200807--	Created, documented.
#	T. Huard  20080815	Cleaned up, and better documented.
#	T. Huard  20080825	Instead of prepending "ctrcrop_" to the basename of the
#				subimage and bpm, now "_ctrcrop" is appended.  Also, the
#				list files are named sif1.list, sif2.list, ..., in order
#				to streamline the data sets that appear on the blackboard.
#	T. Huard  20080826	Changed list of lists filename to ctr2strALL.list
#	T. Huard  20080828	Revised to accept the output list of ctrstack, which 
#				includes the resampled SIFs as well as the harsh reference
#				stack and stack of resampled SIFs.  No scaling or zeroing
#				of the subimage is necessary here since the subimage will
#				need to be scaled and zero'd for each resampled SIF.
#	F. Valdes  20080910	Instead of appending "ctrcrop_" use crp.
#	T. Huard  20080911	Using ctrnames.cl now to help define names in this module.
#				Exit status value of 2 is no longer set in this module.
#	Last Revised:  T. Huard  20080915  12:15pm
#

# Declare variables
int	exitval,icount
string	datadirFULL,mdcstackFILE,mdsstackFILE,imcropFILE,bpmcropFILE,olist
string	dataset,indir,datadir,ilist,lfile

# Load packages
redefine mscred = mscred$mscred.cl
noao
nproto
mscred

# Set file and path names
ctrnames(envget("OSF_DATASET"))
dataset=ctrnames.dataset
indir=ctrnames.indir
datadir=ctrnames.datadir
ilist=ctrnames.ilist
lfile=ctrnames.lfile
datadirFULL=ctrnames.datadirFULL
mdcstackFILE=ctrnames.mdcstackFILE
mdsstackFILE=ctrnames.mdsstackFILE
imcropFILE=ctrnames.imcropFILE
bpmcropFILE=ctrnames.bpmcropFILE
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=ctrnames.uparm)
set (pipedata=ctrnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nCTRcrop (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Obtain list of resampled SIFs from the input list.
if (access("ctrcrop_sifFILES.list")) delete("ctrcrop_sifFILES.list",verify-)
;
fields(ilist,"1",lines="2-",>"ctrcrop_sifFILES.list")

# Use mscimage to extract appropriate subimage from harsh (median) MDC stack for the resampled SIF stack
if (access(imcropFILE)) delete(imcropFILE,verify-)
;
if (access(bpmcropFILE)) delete(bpmcropFILE,verify-)
;
mscimage(mdcstackFILE,imcropFILE,format="image",pixmask+,wcssource="match",reference=mdsstackFILE,
	ra=INDEF,dec=INDEF,scale=INDEF,rotation=INDEF,blank=0.,interpol="linear",minterpol="linear",
	boundary="constant",constant=0.,fluxcon-,ntrim=8,nxblock=INDEF,nyblock=INDEF,interact-,
	nx=10,ny=20,fitgeom="general",xxorder=4,xyorder=4,xxterms="half",yxorder=4,yyorder=4,
	yxterms="half")

# Construct file (list of lists) for the calls to STR.  Also, construct the lists that will be used within
# the STR pipelines.
if (access("ctr2strALL.list")) {
	delete("ctr2strALL.list",verify-)
	delete("sif*.list",verify-)
}
;
icount=0
list="ctrcrop_sifFILES.list"
while (fscan(list,s1) != EOF) {
	icount=icount+1
	olist=datadirFULL//"sif"//str(icount)//".list"
	printf("%s\n",olist,>>"ctr2strALL.list")
	printf("%s\n",datadirFULL//imcropFILE,>olist)
	#printf("%s\n",substr(s1,1,strstr("_r.fits",s1)-1)//".fits",>>olist)
	printf("%s\n",substr(s1,1,strstr("_r.fits",s1)-1)//"_sif.list",>>olist)
}
list=""

logout(exitval)
