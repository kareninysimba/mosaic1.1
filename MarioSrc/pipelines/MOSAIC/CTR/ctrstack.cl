#!/bin/env pipecl
#
# ctrstack
#
# Description:
#
# 	This module stacks the resampled SIFs, by extension, that comprise a sequence of
#	MOSAIC observations.
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful (Currently not set in module)
#
# History:
#
#	T. Huard  20080828	Created.
#	T. Huard  20080911	Using ctrnames.cl now to help define names in this module.
#				This has shortened module significantly, and it no longer
#				needs to generate an input file for ctrcrop.cl
#	Last Revised:  T. Huard  20080911  3:30pm
#

# Declare variables
int	exitval
string  mdcstackFILE,mdsstackFILE
string  dataset,indir,datadir,ilist,lfile

# Load packages
proto

# Set file and path names
ctrnames(envget("OSF_DATASET"))
dataset=ctrnames.dataset
indir=ctrnames.indir
datadir=ctrnames.datadir
ilist=ctrnames.ilist
lfile=ctrnames.lfile
mdcstackFILE=ctrnames.mdcstackFILE
mdsstackFILE=ctrnames.mdsstackFILE
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=ctrnames.uparm)
set (pipedata=ctrnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nCTRstack (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Obtain list of resampled SIFs from the input list.
if (access("ctrcrop_sifFILES.list")) delete("ctrcrop_sifFILES.list",verify-)
;
fields(ilist,"1",lines="2-",>"ctrcrop_sifFILES.list")
hselect ("@ctrcrop_sifFILES.list", "$RSPORD,$I", yes) |
    sort | fields ("STDIN", "2", > "ctrcrop_sifFILES.list")

# Create stack of the resampled SIFs of current extension.
if (access(mdsstackFILE)) delete(mdsstackFILE,verify-)
;
imcombine("@ctrcrop_sifFILES.list",mdsstackFILE,bpmasks="",headers="",rejmask="",nrejmas="",
	expmask="",imcmb="IMCMBR",logfile=lfile,combine="median",reject="none",project-,
	outtype="real",outlimits="",offsets="wcs",
	masktype="novalue",maskvalue=1,blank=0,scale="!MDCSCALE1",zero="!MDCSKY1",weight="none",
	statsec="",expname="",lthresh=INDEF,hthresh=INDEF)

# Clean up.
delete("ctrcrop_sifFILES.list",verify-)

logout(exitval)
