#!/bin/env pipecl
#
# ctrsetup
#
# Description:
#
# 	This module copies the badpixel masks to the current directory.
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful (Currently not set in module)
#
# History:
#
#	T. Huard  200807--	Created.
#	T. Huard  20080815	Cleaned up code and documentation.
#	T. Huard  20080828	Added information to log about number of bpms expected but not copied.
#				Also, added check for whether "[pl]" occurs in BPM value in headers.
#	T. Huard  20080904	Using "copy" instead of "imcopy" since it works correctly in cases with [pl]
#	T. Huard  20080911	Using ctrnames.cl now to help define names in this module.
#	T. Huard  20080912	No longer exit with status 2 when not finding BPM.  This error should never
#				occur.
#	Last Revised:  T. Huard  20080912  4:00pm
#

# Declare variables
int	exitval,icount,icountBAD,icheck
string  dataset,indir,datadir,ilist,lfile
string  bpmFILE,dirFULL

# Load packages
proto

# Set file and path names
ctrnames(envget("OSF_DATASET"))
	print("datadirFULL in module: "//ctrnames.datadirFULL)
	print("mdcstackFILE in module: "//ctrnames.mdcstackFILE)
	print("mdsstackFILE in module: "//ctrnames.mdsstackFILE)
	print("imcropFILE in module: "//ctrnames.imcropFILE)
	print("bpmcropFILE in module: "//ctrnames.bpmcropFILE)
	
dataset=ctrnames.dataset
indir=ctrnames.indir
datadir=ctrnames.datadir
ilist=ctrnames.ilist
lfile=ctrnames.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=ctrnames.uparm)
set (pipedata=ctrnames.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nCTRsetup (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Copy the badpixel masks to the current directory.
exitval=1
icount=0
icountBAD=0
list=ilist
while (fscan(list,s1) != EOF) {
	bpmFILE="" ; hselect(s1,"BPM",yes) | scan(bpmFILE)
	if (bpmFILE != "") {
		icheck=strlstr("[pl]",bpmFILE)
		if (icheck >= 1) bpmFILE=substr(bpmFILE,1,icheck-1)
		;
		if (access(bpmFILE)) delete(bpmFILE,verify-)
		;
		dirFULL=substr(s1,1,strldx("/",s1))
		copy(dirFULL//bpmFILE,bpmFILE)
		if (access(bpmFILE)) icount=icount+1
		else icountBAD=icountBAD+1
	} else {
		printf("%s\n","No valid BPM keyword value found in header: "//bpmFILE) | tee(lfile)
	}
}
list=""

# Write some general information to the log file
printf("%s\n","CTRsetup: Number of badpixel masks copied: "//str(icount)) | tee(lfile)
if (icountBAD >= 1) {
	printf("%s\n","CTRsetup: Number of badpixel masks EXPECTED BUT NOT FOUND: "//str(icountBAD)) | tee(lfile)
	printf("%s\n","***************** SOME BPMs NOT FOUND!!! *****************") | tee(lfile)
}
;

logout(exitval)
