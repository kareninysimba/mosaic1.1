# CTRNAMES -- Directory and filenames for the CTR pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.
#
# History:
#
# 	T. Huard 20080911  Created.
#	Last Revised:  T. Huard  20080915  11:50am


procedure ctrnames (name)

string	name			{prompt = "Name"}

string	dataset			{prompt = "Dataset name"}
string	shortname		{prompt = "Short name"}
file	indir			{prompt = "Input directory"}
file	datadir			{prompt = "Dataset directory"}
file	uparm			{prompt = "Uparm directory"}
file	pipedata		{prompt = "Pipeline data directory"}
file	ilist			{prompt = "Input list"}
file	lfile			{prompt = "Log file"}

file	datadirFULL		{prompt = "Full explicit data directory path"}
file	mdcstackFILE		{prompt = "Full pathname to harsh (median) stack"}
file	mdsstackFILE		{prompt = "Filename of single-extension stack"}
file	imcropFILE		{prompt = "Filename of CTR-cropped harsh stack"}
file	bpmcropFILE		{prompt = "Filename of CTR-cropped harsh stack BPM"}

begin
	# Set generic names, from names.cl
	names("ctr",name)
	dataset=names.dataset
	shortname=names.shortname
	indir=names.indir
	datadir=names.datadir
	uparm=names.uparm
	pipedata=names.pipedata	
	
	# Modify generic names and set pipeline specific names.	
	ilist=indir//dataset//".ctr"
	lfile=datadir//names.lfile
	datadirFULL="" ; pathnames(datadir) | scan(datadirFULL)
	mdcstackFILE="" ; fields(ilist,"1",lines="1") | scan(mdcstackFILE)
	mdsstackFILE=substr(mdcstackFILE,strlstr("/",mdcstackFILE)+1,strlstr("_sk1",mdcstackFILE)-1) \
		//"-"//substr(dataset,strlstr("-ctr",dataset)+4,strlstr("-ctr",dataset)+7) \
		//"_sk1.fits"
	imcropFILE=substr(mdsstackFILE,strldx("/",mdsstackFILE)+1,strlstr(".fits",mdsstackFILE)-1)//"_crp.fits"
	bpmcropFILE=substr(mdsstackFILE,strldx("/",mdsstackFILE)+1,strstr(".fits",mdsstackFILE)-1)//"_crp_bpm.pl"
end
