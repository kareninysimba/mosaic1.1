# DIRCTEST -- Special processing for CTEST.

procedure dirCTEST (dataset, ifile, lfile)

string	dataset			{prompt="Dataset Name"}
file	ifile			{prompt="Input file"}
file	lfile			{prompt="Log file"}
int	status = 1		{prompt="Return status"}

struct	*fd

begin
	file	im

	# Log start of processing.
	printf ("\n%s (%s): ", "dirCTEST", dataset)
	time

	printf ("Changing DTPROPID to PLTEST...\n")

	status = 0
	fd = ifile
	while (fscan (fd, im) != EOF)
	    hedit (im//"[0]", "DTPROPID", "PLTEST", verify-, show+)
	fd = ""
	status = 1
end
