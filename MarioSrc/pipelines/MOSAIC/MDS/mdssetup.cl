#!/bin/env pipecl
#
# MDSsetup
#
# Description:
#
# 	This module copies the badpixel masks to the current directory, and then change the BPM column
#	in ilist from a full pathname to only the filename.
#
# Exit Status Values:
#
# 	1 = Successful
#
# History:
#
# 	T. Huard  20080403  Created.
#	T. Huard  20080409  Backup of ilist is made since ilist is modified at the end of the module.
# 	Last Revised: T. Huard  20080409  11:30am

# Declare variables
int	exitval,icount,ipos1,slen
string  dataset,indir,datadir,ilist,lfile
string  filename,bpm

# Load packages
proto

# Set file and path names
names("mds",envget("OSF_DATASET"))
dataset=names.dataset
indir=names.indir
datadir=names.datadir
ilist=indir//dataset//".mds"
lfile=datadir//names.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nMDSsetup (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Copy the badpixel masks to the current directory.
copy(ilist,ilist//"BACKUP")
fields(ilist,"2",>"mdssetup_tmp.list")
copy("@mdssetup_tmp.list",".",verbose-)
count("mdssetup_tmp.list") | scan(icount)

# Change the BPM column in ilist from the full pathname
# to only the filename.
if (access("mdssetup_tmp2.list")) delete("mdssetup_tmp2.list",verify-)
;
touch("mdssetup_tmp2.list")
list=ilist
while (fscan(list,filename,bpm) != EOF) {
	ipos1=strlstr("/",bpm)
	slen=strlen(bpm)
	bpm=substr(bpm,ipos1+1,slen)
	print(filename,>>"mdssetup_tmp2.list")
}
list=""
copy("mdssetup_tmp2.list",ilist)
delete("mdssetup_tmp.list",verify-)
delete("mdssetup_tmp2.list",verify-)


# Write some general information to the log file
print("MDSsetup: Number of badpixel masks copied: "//icount)

logout(exitval)
