#!/bin/env pipecl
#
# MDSstack
#
# Description:
#
# 	This module stacks the reduced (not sky-subtracted), resampled SIFs comprising
# 	a sequence of MOSAIC observations.
#
# Exit Status Values:
#
#	1 = Successful
#	2 = Unsuccessful (Currently not set in module)
#
# History:
#
# 	T. Huard  200711--  Created.	
# 	T. Huard  20080313  Changed basenames of stacked products to reflect first filename
#		included in the stack, by convention, instead of using the dataset.
#	T. Huard  20080403  Shortened the basenames and made other changes in code, as
#		recommended by Frank.
#	T. Huard  20080408  Changed exit status value to 2 for inconsistent exposure times.
#	T. Huard  20080409  Fixed seqlabel to include kp4m or ct4m through ccd* part of filename,
#		and removing any "-" that occurs since they cause problems with the GPS/MDS return.
#		The naming convention for the stacked products has been changed from mdsstack_*
#		to *_sifstk*
#	T. Huard  20080410  Included imcombine log file into log file for the pipeline.
#	T. Huard  20080429  Corrected SKYMEAN header value of stack.
#	T. Huard  20080618  Changed imcombine to use median instead of average.  This produces
#	                    a "harsher" stack, free from cosmic rays, transients, and artifacts,
#                           that can be used as a reference image for removal of such features
#                           in the individual images.
#	T. Huard  20080815  Cleaned up the code and documentation.  Removed the "checks" of
#	                    exposure times, etc, on the resampled SIFs.  These were originally
#	                    written for combining a single dither sequence, and are no longer
#	                    necessary.  The image scales were changed from:
#	                                scale=10.**(-real(magzero)/2.5)
#	                    to:
#	                                scale=I0/(10.**(-real(magzero)/2.5))
#	                    where I0 is:
#	                                I0=10.**(-real(magzero0)/2.5)
#	                    and magzero0 is the MAGZERO value of the first image in the stack.
#	                    With this change, the stacks should have a scaling that is appropriate
#	                    for the MAGZERO value given by the first image in the stack.
#	                    Finally, one of the list files ("mdsstack_checks.list") was left
#	                    open; now, it is closed.
# 	Last Revised:  T. Huard  20080815  3:00pm

# Declare variables
int	exitval,icheck
string  dataset,indir,datadir,ilist,lfile
string  seqlabel,filename0,exptime0,skymean0,magzero0
string  filename,exptime,skymean,magzero
real	I0,scale

# Load packages
images
mscred
utilities

# Set file and path names
names("mds",envget("OSF_DATASET"))
dataset=names.dataset
indir=names.indir
datadir=names.datadir
ilist=indir//dataset//".mds"
lfile=datadir//names.lfile
mscred.logfile=lfile
mscred.instrument="pipedata$mosaic.dat"
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\nMDSstack (%s): ",dataset) | tee (lfile)
time | tee (lfile)

# Initialize exit status to 1 (successful)
exitval=1

# Set the names based on the shortname convention.
seqlabel=names.shortname
seqlabel=seqlabel//"_stk"  # seqlabel is modified to be used for naming stack products

# Read the headers of the resampled SIFs in order to run some basic checks and help with
# stacking properly.
hselect("@"//ilist,"$I,EXPTIME,SKYMEAN,MAGZERO,OBJECT",yes,>"mdsstack_checks.list")

# Compute the relative scaling factors (derived from MAGZERO values) and zeros (additive
# shifts derived from the SKYMEAN values) for the resampled SIFs and write these to their
# FITS headers (create new keywords MDSSKY and MDSSCALE).  In this way, imcombine will
# be able to use these values to properly scale and shift the individual resampled SIFs.
# IMPORTANT NOTE:  The SKYMEAN values in the SIF headers are negative those necessary to
# be useful for imcombine to subtract the sky before stacking the SIFs.  Also, in imcombine,
# the scaling is applied before the zero (see documentation for imcombine).  However, the
# task accounts for the change in zero (new zero is -SKYMEAN*SCALE) resulting from
# rescaling.  So, the effect of scale on the zero should not be explicitly done
# before inputting the scales into imcombine.
list="mdsstack_checks.list"
icheck=fscan(list,filename0,exptime0,skymean0,magzero0)
I0=10.**(-real(magzero0)/2.5)
hedit(filename0,"MDSSKY",str(-real(skymean0)),add+,ver-)
hedit(filename0,"MDSSCALE",str(1.0),add+,ver-)
while (fscan(list,filename,exptime,skymean,magzero) != EOF) {
	scale=I0/(10.**(-real(magzero)/2.5))
	hedit(filename,"MDSSKY",str(-real(skymean)),add+,ver-)
	hedit(filename,"MDSSCALE",str(scale),add+,ver-)
}
list=""

# Combine the resampled SIFs in the sequence, using the WCS information included in their
# headers to align the images.  The images are scaled by multiplicative factors, previously
# determined, to account for changes in the transparency.  Also, the images are adjusted by
# additive constants, previously determined, to account for changes in the background sky
# levels during the dither sequence.  In combining, the MEDIAN of pixel values in a stacked
# pixel column is used, after rejecting the pixels identified in the bad pixel masks.
if (access(seqlabel//".fits")) delete(seqlabel//".fits",verify-)
;
if (access(seqlabel//"_bpm.pl")) delete(seqlabel//"_bpm.pl",verify-)
;
if (access(seqlabel//"_expmask.fits")) delete(seqlabel//"_expmask.fits",verify-)
;
if (access(seqlabel//"_sigma.fits")) delete(seqlabel//"_sigma.fits",verify-)
;
imcombine("@"//ilist,seqlabel,bpmasks=seqlabel//"_bpm",headers="",rejmask="",nrejmas="",
	expmask=seqlabel//"_expmask",
#	imcmb="IMCMBR",logfile=lfile,
	logfile=lfile,
	combine="median",reject="none",project-,outtype="real",outlimits="",offsets="wcs",
	masktype="novalue",maskvalue=1,blank=0,scale="!MDSSCALE",zero="!MDSSKY",
	weight="none",statsec="",expname="",lthresh=INDEF,hthresh=INDEF)

# Set headers.
hedit(seqlabel,"IMCMBSIF,IMCMBMEF",del+,ver-)
hedit(seqlabel,"SKYMEAN",str(0.00),add+,ver-)		# Sky has been subtracted by imcombine and MDSSKY
hedit(seqlabel,"MAGZERO",str(magzero0),add+,ver-)	# Imcombine scaled all input images to the
							# MAGZERO of the first image in the stack.
							# So, the resulting stacked of SIFs will 
							# have MAGZERO=MAGZERO0.
# Clean up.
# DO NOT DELETE mdsstack_checks.list SINCE IT IS USED BY MDC.

logout(exitval)
