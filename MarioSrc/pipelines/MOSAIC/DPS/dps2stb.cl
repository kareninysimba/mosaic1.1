#!/bin/env pipecl
# 
# DPS2STB -- Send data products to STB.
#
# Because the data is trigger back to the MDP pipeline on each node.

string  dataset, datadir, lfile, host, mdpdataset, flag

# Load tasks and packages
task $osf_update = "$!osf_update -a $4 -p mdp -f $1 -m $2 -s $3"

# Set calling flag.
flag = envget ("OSF_FLAG")

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
cd (datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Check for a request to use just the NGT data and if this makes sense.
if (flag == "P") {
    files ("*pgr.mdp,*frg.mdp,*sft.mdp") | count | scan (i)
    if (i == 0)
        flag = "N"
    ;
}
;

# Trigger the MDP pipeline.
if (flag == "P") {
    files ("*-mdp*.mdp", > "dps2stb.tmp")
    list = "dps2stb.tmp"
    while (fscan (list, s1) != EOF) {
	mdpdataset = substr (s1, 1, strstr(".mdp",s1)-1)
	osf_update (mdpdataset, "mdpmef", flag, envget ("NHPPS_SYS_NAME"))
    }
    list = ""; delete ("dps2stb.tmp")
    delete ("@"//dataset//".mdp")
    delete (dataset//".mdp")
    delete (dataset//".list")
    logout 2
} else {
    files ("*-mdp*.mdp", > "dps2stb.tmp")
    list = "dps2stb.tmp"
    while (fscan (list, s1) != EOF) {
	mdpdataset = substr (s1, 1, strstr(".mdp",s1)-1)
	osf_update (mdpdataset, "mdp2stb", flag, envget ("NHPPS_SYS_NAME"))
    }
    list = ""; delete ("dps2stb.tmp")
    logout 1
}
