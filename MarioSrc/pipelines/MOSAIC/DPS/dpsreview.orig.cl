#!/bin/env pipecl
# 
# DPSREVIEW -- Review data products.
#
# This routine makes a review HTML.  This will be made whether or not
# a review is needed for sending the data products to STB.
#
# Status: 1= No STB review, 2 = STB review.

string  dataset, datadir, ilist, lfile, pngdir, html
string	rootname, s4, s5, osf_cmd, host, sftflag, raw, propid
struct	obstype, proctype

# Load tasks and packages
task $psqlog = "$!psqlog"
task $hostname = "$!hostname -f"
task $pwd = "$!pwd"
task $resize = "$!convert -resize"
task dpssummary = "NHPPS_PIPESRC$/MOSAIC/DPS/dpssummary.cl"
task dpssummary1 = "NHPPS_PIPESRC$/MOSAIC/DPS/dpssummary1.cl"
images
fitsutil
utilities

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = names.indir // dataset // ".dps"
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

psqlog (names.shortname, "completed", "dpsreview")

# Create data subdirectories.  Note that this may be called more than once
# so we delete any old data.
cd (names.rootdir//"output")
if (access("html")==NO)
    mkdir ("html")
;
cd ("html")
if (access("png")==NO)
    mkdir ("png")
;
rootname = substr (dataset, 1, stridx("-",dataset)-1)
rootname = substr (rootname, 1, strldx("_",rootname)-1)
if (access (rootname) == NO)
    mkdir (rootname)
;
cd ("png")
pngdir = names.shortname//"/"
if (access(pngdir))
    delete (pngdir//"*")
else
    mkdir (pngdir)
cd (pngdir)
pngdir = "../png/"//names.shortname//"/"
html = "../../"//rootname//"/"//names.shortname // ".html"
if (access(html))
    delete (html)
;

# Check if there are any raw PNGs.
match ("_raw.png", ilist) | count | scan (i)
b1 = (i > 0)

# Create list of datasets.
files (datadir//"*.mdp", > "dpsrev1.tmp")
list = "dpsrev1.tmp"
while (fscan (list, s1) != EOF) {
    s2 = ""
    match ("_png.fits", s1) | match ("_r_png", "STDIN", stop+) | scan (s2)
    if (s2 == "")
        next
    ;
    rootname = substr (s2, strldx("/",s2)+1, 1000)
    rootname = substr (rootname, 1, strstr("_png.fits",rootname)-1)

    # Create the HTML file and initialize the table.
    if (access(html)==NO) {
	printf ('<HTML><TITLE>%s</TITLE><BODY><CENTER><H1>%s</H1></CENTER>\n',
	    names.shortname, names.shortname, > html)

	# Insert the approve/reject/skip form if needed.
	if (dps_review) {
	    # Create the "standard" part of the osf_update call
	    osf_cmd = envget ("NHPPS_DIR_BASE") // '/bin/osf_update -a '
	    osf_cmd = osf_cmd // envget ("NHPPS_SYS_NAME") // ' -p dps -m dps2stb -f ' // dataset // ' -s'

	    hostname | scan (host)

	    printf ('\n<FORM ACTION="http://%s/cgi-bin/sendCmd.cgi" METHOD="post">', host, >> html)

	    # Set the USER environment variable of the CGI environment
	    printf ('\n<INPUT NAME="env" TYPE="hidden" VALUE="USER=%s">', envget ("USER"), >> html)

	    # Set the PYTHONPATH environment variable of the CGI environment
	    printf ('\n<INPUT NAME="env" TYPE="hidden" VALUE="PYTHONPATH=%s">', envget("PYTHONPATH"), >> html)

	    # Insert a radio selection device to allow the user to choose between the 
	    # Accepted (Y), Rejected (N), and the Skip (S) status flags
	    printf ('\nAccept: <INPUT NAME="cmd" TYPE="radio" VALUE="%s Y" tabindex="1" checked>', osf_cmd, >> html)
	    printf ('\nReject: <INPUT NAME="cmd" TYPE="radio" VALUE="%s N" tabindex="2">', osf_cmd, >> html)
	    printf ('\nReprocess using Night Pipeline: <INPUT NAME="cmd" TYPE="radio" VALUE="%s P" tabindex="3">', osf_cmd, >> html)

	    # Insert submit button
	    printf ('\n<INPUT TYPE="submit">', >> html)

	    # End the form
	    printf ('\n</FORM>', >> html)

	    # END of form insertion text for this block
#	} else if (dps_archive=="no" && dts_ftp=="no") {
#	    printf ('\nArchive: \n', >> html)
#	    printf ('<A HREF="/cgi-bin/stbsubmit.cgi?%s">submit</A>\n',
#	        dataset, >> html)
#	    printf ('<P>\n\n', >> html)
	}
	;

	printf ('\n<TABLE BORDER="1">\n', >> html)
	printf ('<TR>\n', >> html)
	printf ('<TH>Summary\n', >> html)
	if (stridx("-",names.shortname)==0 ||
 	    strstr("Zero",names.shortname)>0 ||
 	    strstr("F-",names.shortname)>0)
	    printf ('<TH>Calibration<BR>MEF\n', >> html)
	else {
	    printf ('<TH>Raw<BR>MEF\n', >> html)
	    printf ('<TH>Calibrated<BR>MEF\n', >> html)
	    printf ('<TH>Rebinned<BR>Image\n', >> html)
	    printf ('<TH>Dither Stack<BR>Image\n', >> html)
	}
	printf ('</TR>\n', >> html)
    } else
	printf ('</TR>\n', >> html)


    # Get raw and calibrated MEF pngs.
    copy (s2, ".", verbose-)
    s2 = substr (s2, strldx("/",s2)+1, 1000)
    s5 = substr (s2, 1, strstr("_png.fits",s2)-1)
    rootname = s5
    i = strldx ("-", s5)
    if (i == 0)
        s4 = s5
    else
	s4 = substr (s5, i+1, 1000)
    if (b1) {
	raw = ""; match (s5//"_raw.png", ilist) | scan (raw)
	if (raw != "") {
	    copy (raw, ".", verbose-)
	    raw = substr (raw, strldx("/",raw)+1, 1000)
	}
	;
    } else
        raw = ""

    # Set the PROPID so we can later separate by it.
    propid = ""; hselect (s2//"[0]", "DTPROPID", yes) | scan (propid)
    if (propid == "")
       propid = "unknown"
    ;

    # Add PROPID to filenames and create header listing.
    imrename (s2, propid//"_"//s2)
    s2 = propid//"_"//s2
    s3 = propid//"_"//s5
    if (raw != "") {
	rename (raw, propid//"_"//raw)
	raw = propid//"_"//raw
    }
    ;
    imhead (s2//"[0]", l+, > s3//".hdr")

    # Set observation type and processing type and various keywords.
    obstype = ""; hselect (s2//"[0]", "obstype", yes) | scan (obstype)
    proctype = ""; hselect (s2//"[0]", "proctype", yes) | scan (proctype)
    sftflag = ""; hselect (s2//"[0]", "SFTFLAG", yes) |
	translit ("STDIN", '"', delete+) | scan (sftflag)

    # Start a row.  Put in any comments.
    printf ("<!-- PROPID %s -->\n", propid, >> html)
    printf ("<TR>\n", >> html)

    # Put summary information.
    printf ('<TD ALIGN="LEFT" VALIGN="CENTER">\n', >> html)
    dpssummary (s2//"[0]", >> html)
    printf ('</A></TD>\n', >> html)
#    printf ('<TD ALIGN="LEFT" VALIGN="CENTER">\n', >> html)
#    dpssummary1 (s2//"[0]", >> html)
#    printf ('</A></TD>\n', >> html)

    # Put raw PNG.
    if (b1) {
	printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">\n', >> html)
	if (raw != "") {
	    printf ('<IMG SRC="%s%s">\n', pngdir, raw, >> html)
	    printf ('<BR>%s<BR><BR>\n', s4, >> html)
	}
	;
	printf ('</TD>\n', >> html)
    }
    ;

    # Put MEF pngs.
    fgread (s2, "1,2", "", verbose-)
    delete (s2)
    rename (s5//"_"//png_pix//".png", s3//"_"//png_pix//".png")
    rename (s5//"_x"//png_blk//".png", s3//"_x"//png_blk//".png")
    #resize (800, s3//"_x"//png_blk//".png", s3//"_800.png")
    printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">\n', >> html)
    if (proctype != "MasterCal") {
	if (substr(sftflag,1,1) != "Y")
	    printf ('<INPUT TYPE="CHECKBOX">Sky Stack<BR>\n', >> html)
	else
	    printf ('<INPUT TYPE="CHECKBOX" CHECKED>Sky Stack<BR>\n', >> html)
    }
    ;
    printf ('<IMG SRC="%s%s_%d.png">\n', pngdir, s3, png_pix, >> html)
    printf ('<BR><A HREF="%s%s.hdr">%s</A>\n', pngdir, s3, s4, >> html)
#    printf ('<BR>[<A HREF="%s%s_%d.png">%d</A>',
#        pngdir, s3, 800, 800, >> html)
#    printf (', <A HREF="%s%s_x%d.png">x%d</A>]\n',
#        pngdir, s3, png_blk, png_blk, >> html)
    printf ('<BR>[<A HREF="%s/%s_x%d.png">x%d</A>\n',
        pngdir, s3, png_blk, png_blk, >> html)
    printf ('<A HREF="/cgi-bin/pngrestore.cgi?pipeline&MOSAIC&%s/%s_x%d.png">(restore)</A>]\n',
        names.shortname, s3, png_blk, >> html)
    printf ('</TD>\n', >> html)

    # Add the resampled version.
    s2 = ""; match ("_r_png.fits", s1) | scan (s2)
    if (s2 == "")
        next
    ;

    copy (s2, ".", verbose-)
    s2 = substr (s2, strldx("/",s2)+1, 1000)
    s5 = substr (s2, 1, strstr("_png.fits",s2)-1)
    i = strldx ("-", s5)
    if (i == 0)
	s4 = s5
    else
	s4 = substr (s5, i+1, 1000)

    # Add PROPID to filenames and create header listing.
    imrename (s2, propid//"_"//s2)
    s2 = propid//"_"//s2
    s3 = propid//"_"//s5
    imhead (s2//"[0]", l+, > s3//".hdr")

    # Set PNGs.
#    fgread (s2, "1,2", "", verbose-)
    fgread (s2, "2", "", verbose-)
    delete (s2)
    rename (s5//"_"//png_pix//".png", s3//"_"//png_pix//".png")
#    rename (s5//"_x"//png_blk//".png", s3//"_x"//png_blk//".png")
    #resize (800, s3//"_x"//png_blk//".png", s3//"_800.png")
    printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">\n', >> html)
    printf ('<IMG SRC="%s%s_%d.png">\n', pngdir, s3, png_pix, >> html)
    printf ('<BR><A HREF="%s%s.hdr">%s</A>\n', pngdir, s3, s4, >> html)
#    printf ('<BR>[<A HREF="%s%s_%d.png">%d</A>',
#	pngdir, s3, 800, 800, >> html)
#    printf (', <A HREF="%s%s_x%d.png">x%d</A>]\n',
#	pngdir, s3, png_blk, png_blk, >> html)
#    printf ('<BR>[<A HREF="%s%s_x%d.png">x%d</A>]\n',
#	pngdir, s3, png_blk, png_blk, >> html)
#    printf ('<BR>[<A HREF="/cgi-bin/pngrestore.cgi?pipeline&MOSAIC&%s/%s_x%d.png">x%d</A>]\n',
#        names.shortname, s3, png_blk, png_blk, >> html)
    printf ('<BR><BR></TD>\n', >> html)

    # Add the stacked version.
    s2 = ""; match (rootname//"_stk_png.fits", ilist) | scan (s2)
    if (s2 == "")
        next
    ;

    copy (s2, ".", verbose-)
    s2 = substr (s2, strldx("/",s2)+1, 1000)
    s5 = substr (s2, 1, strstr("_png.fits",s2)-1)
    i = strldx ("-", s5)
    if (i == 0)
	s4 = s5
    else
	s4 = substr (s5, i+1, 1000)

    # Add PROPID to filenames and create header listing.
    imrename (s2, propid//"_"//s2)
    s2 = propid//"_"//s2
    s3 = propid//"_"//s5
    imhead (s2//"[0]", l+, > s3//".hdr")

    # Set PNGs.
    fgread (s2, "1,2", "", verbose-)
    delete (s2)
    rename (s5//"_"//png_pix//".png", s3//"_"//png_pix//".png")
    rename (s5//"_x"//png_blk//".png", s3//"_x"//png_blk//".png")
    #resize (800, s3//"_x"//png_blk//".png", s3//"_800.png")
    printf ('<TD ALIGN="CENTER" VALIGN="BOTTOM">\n', >> html)
    printf ('<IMG SRC="%s%s_%d.png">\n', pngdir, s3, png_pix, >> html)
    printf ('<BR><A HREF="%s%s.hdr">%s</A>\n', pngdir, s3, s4, >> html)
#    printf ('<BR>[<A HREF="%s%s_%d.png">%d</A>',
#	pngdir, s3, 800, 800, >> html)
#    printf (', <A HREF="%s%s_x%d.png">x%d</A>]\n',
#	pngdir, s3, png_blk, png_blk, >> html)
#    printf ('<BR>[<A HREF="%s%s_x%d.png">x%d</A>]\n',
#	pngdir, s3, png_blk, png_blk, >> html)
    printf ('<BR>[<A HREF="%s/%s_x%d.png">x%d</A>\n',
        pngdir, s3, png_blk, png_blk, >> html)
    printf ('<A HREF="/cgi-bin/pngrestore.cgi?pipeline&MOSAIC&%s/%s_x%d.png">(restore)</A>]\n',
        names.shortname, s3, png_blk, >> html)
    printf ('</TD>\n', >> html)

}
list = ""; delete ("dpsrev1.tmp")

# Finish table.
if (access(html))
    printf ('</TR>\n', >> html)
;

# Add link to sky analysis.
s1 = ""; match ("fig.html", ilist) | scan (s1)
if (s1 != "") {
    s2 = substr (s1, strldx("/",s1)+1, 1000)
    match ("fig", ilist, > "dtsrev1.tmp")
    copy ("@dtsrev1.tmp", ".", verbose-)
    printf ('\n<H4><A HREF="%s%s">Sky Analysis</A></H4>\n',
        pngdir, s2, >> html)
}
;

# If there is nothing to review return no review status.
if (access(html)==NO)
    logout 1
else {
    # Finish HTML.
    printf ('\n</TABLE></BODY></HTML>\n', >> html)
}

# Return review status.
# dps_review is defined in MOSAIC/config/Mario.cl
if (dps_review) {
    cd ("../html")
    hostname | scan (s1)
    pwd | scan (s2)
    printf ("http://%s%s/%s\n", s1, s2, names.shortname//".html") |
        scan (s3)
    sendmsg ("REVIEW", s3, "", "MAIL")
    logout 2
} else
    logout 1
