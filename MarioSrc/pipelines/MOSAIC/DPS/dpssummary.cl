# DSPSUMMARY -- Make HTML summary info from an image header.
# This is an HTML fragment written to the STDOUT to be used by
# the calling program.  The image argument may be  wildcard but only
# the first image will be processed.

procedure dspsummary (image)

file	image				{prompt="Image"}

begin
	file	im
	int	ival
	real	rval
	string	strval
	struct	sval, obstype, proctype

	# Tasks and packages.
	images
	utilities

	im = image

	# Observation type and processing type.
	hselect (im, "$OBSTYPE", yes) | scan (obstype)
	hselect (im, "$PROCTYPE", yes) | scan (proctype)

	# Date of observation
	hselect (im, "$DATE-OBS", yes) | scan (sval)
	if (sval != "INDEF")
	    printf ("%s<BR>\n", sval)

	# Image Title
	sval=""; hselect (im, "TITLE", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	ival = strstr ("- DelRA",sval)
	if (ival > 0)
	    sval = substr (sval, 1, ival-1)
	printf ("%s<BR>\n", sval)

	# Original filename or number combined.
	hselect (im, "$NCOMBINE", yes) | scan (ival)
	if (isindef(ival)) {
	    hselect (im, "$PLOFNAME", yes) | scan (sval)
	    if (sval != "INDEF") {
	        ival = strstr ("_pl_png", sval)
		if (ival > 0)
		    sval = substr (sval, 1, ival-1)
		printf ("%s<BR>\n", sval)
	    }
	} else
	    printf ("NCOMBINE = %d<BR>\n", ival)

	if (proctype == "MasterCal") {
	    hselect (im, "$QUALITY", yes) | scan (rval)
	    if (isindef(rval)==NO) {
	        printf ("QUALITY = %g<BR>\n", rval)
		if (rval < 0.)
		    printf ("<B>For Evaluation Only</B><BR>\n")
	    }
	}
	
	if (obstype == "zero")
	    return

	# Filter
	hselect (im, "$FILTER", yes) | scan (sval)
	if (sval != "INDEF")
	    printf ("%s<BR>\n", sval)

	if (proctype == "MasterCal")
	    return

	# Exposure time.
	hselect (im, "$EXPTIME", yes) | scan (rval)
	if (isindef(rval)==NO)
	    printf ("EXPTIME = %d<BR>\n", rval)

	# Proposal title
	hselect (im, "$DTTITLE", yes) | scan (sval)
	if (sval != "INDEF") {
	    if (strlen (sval) > 20)
		printf ("%.30s...<BR>\n", sval)
	    else
		printf ("%s<BR>\n", sval)
	}

	# Sky level
	hselect (im, "$SKYMEAN", yes) | scan (rval)
	if (isindef(rval)==NO)
	    printf ("SKYMEAN = %.3g<BR>\n", rval)

	# Airmass.
	hselect (im, "$AIRMASS", yes) | scan (rval)
	if (isindef(rval)==NO)
	    printf ("AIRMASS = %.2g<BR>\n", rval)

	# WCS and Photometry.
	hselect (im, "$WCSCAL", yes) | scan (sval)
	if (sval == "INDEF" || sval == "F") {
	    hselect (im, "TELRA,TELDEC", yes) | scan (rval, rval)
	    if (nscan() < 2)
		printf ("<B>Telescope Coordinates Missing</B><BR>\n")
	    else
		printf ("<B>WCS/Phot Calibration Failed</B><BR>\n")
	} else {
	    # Magnitude zero point
	    hselect (im, "$PHOTFILT", yes) | scan (strval)
	    hselect (im, "$MAGZERO", yes) | scan (rval)
	    if (isindef(rval)==NO)
		printf ("MAGZERO(%s) = %.2f<BR>\n", strval, rval)

	    # Magnitude depth.
	    hselect (im, "$PHOTDPTH", yes) | scan (rval)
	    if (isindef(rval)==NO)
		printf ("PHOTDPTH(%s) = %.2f<BR>\n", strval, rval)

	    # Seeing
	    hselect (im, "$SEEING", yes) | scan (rval)
	    if (isindef(rval)==NO)
		printf ("SEEING = %.2f<BR>\n", rval)
	}
end
