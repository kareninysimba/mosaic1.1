  �V  0�  K�$TITLE = "Domeflats (afternoon) filter1 = B"
$CTIME = 988967925
$MTIME = 988967925
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-14T20:02:51' / Date FITS file was generated
IRAF-TLM= '13:02:59 (14/11/2006)' / Time of last modification
OBJECT  = 'Domeflats (afternoon) filter1 = B' / Name of the object observed
FILENAME= 'dflat011'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '20:01:49.85'        / RA of observation (hr)
DEC     = '20:39:35.79'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-12-08T19:33:49.3' / Date of observation start (UTC approximate)
TIME-OBS= '19:33:49.302'       / Time of observation start
MJD-OBS =       53347.81515396 / MJD of observation start
MJDHDR  =       53347.81507176 / MJD of header creation
LSTHDR  = '20:02:03.4        ' / LST of header creation

OBSERVAT= 'CTIO    '           / Observatory
TELESCOP= 'CTIO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2000.0 / Equinox of tel coords
TELRA   = '20:01:49.85       ' / RA of telescope (hr)
TELDEC  = '20:39:35.8        ' / DEC of telescope (deg)
HA      = '00:00:01.1        ' / hour angle (H:M:S)
ZD      = '50.8              ' / Zenith distance
AIRMASS =                1.581 / Airmass
TELFOCUS= '16071             ' / Telescope focus
CORRCTOR= 'Blanco Corrector'   / Corrector Identification
ADC     = 'Blanco ADC'         / ADC Identification

DETECTOR= 'Mosaic2 '           / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                   16 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.270 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.270 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    3 / Filter position
FILTER  = 'B Harris c6002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -1.6010000000000E+00 / North TV Camera focus position
TV2FOC  = -1.9970000000000E+00 / South Camera focus position
ENVTEM  =  2.0900000000000E+01 / Ambient temperature (C)
DEWAR   = 'Mosaic2 Dewar'      / Dewar identification
DEWTEM  =  0.0000000000000E+00 / Dewar temperature (C)
DEWTEM2 = -4.9799999000000E+01 / Fill Neck temperature (C)
DEWTEM3 = 'N2  90.6'
CCDTEM  = -9.4000000000000E+01 / CCD temperature (C)
CCDTEM2 = 'CCD 169.8'

WEATDATE= 'Dec 08 19:31:02 2004' / Date and time of last update
WINDSPD = '4.7     '           / Wind speed (mph)
WINDDIR = '248     '           / Wind direction (degrees)
AMBTEMP = '23.2    '           / Ambient temperature (degrees C)
HUMIDITY= '24      '           / Ambient relative humidity (percent)
PRESSURE= '782     '           / Barometric pressure (millibars)
DIMMSEE = 'mysql_query():Unknown Error seeing=column' / Tololo DIMM seeing

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '13July99ver7_30'    / Controller software version
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Wed Dec  8 15:51:13 2004' / Date waveforms last compiled

OBSERVER= 'ESSENCE/SuperMACHO' / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'ct4m.20041208T193349' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/ctio/4meter/caldir/Mosaic2/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic2_xtalk040501.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =               135390  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



























































































































DTSITE  = 'ct                '  /  observatory location
DTTELESC= 'ct4m              '  /  telescope identifier
DTINSTRU= 'optic             '  /  instrument identifier
DTCALDAT= '2004-12-08        '  /  calendar date from observing schedule
DTPUBDAT= 'none              '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= 'noao              '  /  observing proposal ID
DTPI    = 'Christopher Stubbs'  /  Principal Investigator
DTPIAFFL= 'University of Washington'  /  PI affiliation
DTTITLE = 'A Next Generation Microlensing Survey of the LMC'  /  title of observ
DTACQUIS= 'ctioa8.ctio.noao.edu'  /  host name of data acquisition computer
DTACCOUN= 'mosaic            '  /  observing account name
DTACQNAM= '/ua80/mosaic/tonight/dflat011.fits'  /  file name supplied at telesco
DTNSANAM= 'ct135390.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041208/ct4m/noao/ct135390.fits.gz'
UPARM   = 'MarioCal$/Mosaic2_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M2_53166'
IMAGEID =                   13 / Image identification
EXPTIME =               35.000 / Exposure time in secs
DARKTIME=               43.820 / Total elapsed time in secs

CCDNAME = 'SITe #98421FABR07-02 (NOAO 31)' / CCD name
AMPNAME = 'SITe #98421FABR07-02 (NOAO 31), upper left (Amp21)' / Amplifier name
GAIN    =                  2.4 / gain expected for amp 421 (e-/ADU)
RDNOISE =                  5.3 / read noise expected for amp 421 (e-)
SATURATE=                42000 / Maximum good data value (ADU)
CONHWV  = 'ACEB004_AMP21'      / Controller hardware version
ARCONG  =                  2.4 / gain expected for amp 421 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[4097:6144,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                4096. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'ct4m.20020927T000014 (USNO N B Harris c6002) by F. Valdes 2002-10-24'
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =            300.45771 / Coordinate reference value
CRVAL2  =            20.659942 / Coordinate reference value
CRPIX1  =           -91.217079 / Coordinate reference pixel
CRPIX2  =              -23.221 / Coordinate reference pixel
CD1_1   =       -2.5939867e-07 / Coordinate matrix
CD2_1   =         7.447199e-05 / Coordinate matrix
CD1_2   =         7.412796e-05 / Coordinate matrix
CD2_2   =       -1.5464468e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. 0.001240543243571545 0.305'
WAT1_002= '32552795606 0.006230573831111668 0.1593080082351559 -2.8632748045930'
WAT1_003= '67E-4 -6.862667742811570E-5 -6.463288649230908E-4 -1.079645858275469'
WAT1_004= 'E-4 1.092766070802854E-5 -2.252037309694490E-4 3.580175912155704E-6 '
WAT1_005= '-4.815066627492912E-5 -5.152519584463043E-5 8.627535980683276E-6 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. 0.001240543243571545 0.30'
WAT2_002= '532552795606 0.006230573831111668 0.1593080082351559 -5.739566530878'
WAT2_003= '452E-5 1.936750166860838E-5 -8.869602715288811E-5 -1.062977553389737'
WAT2_004= 'E-5 -5.748265540240881E-5 -4.161766317930904E-4 -1.026564891359817E-'
WAT2_005= '4 -8.743224220471274E-5 6.620581601025897E-6 -2.287772548745048E-5 "'

CHECKSUM= 'aACZa8AZaAAZa5AZ'    /  ASCII 1's complement checksum
DATASUM = '135814224 '          /  checksum of data records
XTALKCOR= 'Oct 23 22:52 Crosstalk is 7.05E-4*im14+9.20E-4*im15+6.71E-4*im16'
ZERO    = 'MarioCal$/UBVRI_C20041208_1161662005-ct4m20041208T190047Z-ccd7.fits'
CALOPS  = 'XOTZFWSBP'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             1374.908 / min in overscan region
DQOVMAX =             1377.485 / max in overscan region
DQOVMEAN=             1376.136 / mean in overscan region
DQOVSIG =              2.32207 / sigma in overscan region
DQOVSMEA=             1376.136 / mean in collapsed overscan strip
DQOVSSIG=             0.398158 / sigma in collapsed overscan strip
TRIM    = 'Oct 24  0:05 Trim is [25:1048,1:4096]'
OVERSCAN= 'Oct 24  0:05 Overscan is [1063:1112,1:4096], function=minmax'
ZEROCOR = 'Oct 24  0:05 Zero is MarioCal$/UBVRI_C20041208_1161662005-ct4m200412'
CCDPROC = 'Oct 24  0:05 CCD processing done'
PROCID  = 'ct4m.20041208T193349V5'
OVSCNMTD=                    3
AMPMERGE= 'Oct 24  0:05 Merged 2 amps'
DQGLFSAT=                   0.
DQGLMEAN=             12048.43
DQGLSIG =             779.6708
DQDFCRAT=             344.2408
DQDFGLME=             12045.33 / DQ global mean of zero frame
DQDFGLSI=             784.8552 / DQ global sigma of zero frame
DQDFXMEA=             12045.33 / DQ mean of collapsed x bias
DQDFXSIG=             752.3671 / DQ sigma of collapsed x bias
DQDFXSLP=            -1.162863 / DQ slope of collapsed x bias
DQDFXCEF=            0.9139954 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             12045.33 / DQ mean of collapsed y bias
DQDFYSIG=             158.8582 / DQ sigma of collapsed y bias
DQDFYSLP=           -0.0145364 / DQ slope of collapsed y bias
DQDFYCEF=            0.1082106 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  708
FLAT    = 'MarioCal$/UBVRI_C20041208_1161662005-B-ct4m20041210T195018F-ccd7'
DQADNUSD=                    9
DQADNREJ=                    0
DQADGLME=              8714.57 / DQ global mean of zero frame
DQADGLSI=             566.9507 / DQ global sigma of zero frame
DQADXMEA=              8714.57 / DQ mean of collapsed x bias
DQADXSIG=             543.0048 / DQ sigma of collapsed x bias
DQADXSLP=            -0.838334 / DQ slope of collapsed x bias
DQADXCEF=            0.9129748 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=              8714.57 / DQ mean of collapsed y bias
DQADYSIG=              127.085 / DQ sigma of collapsed y bias
DQADYSLP=          -0.03150687 / DQ slope of collapsed y bias
DQADYCEF=            0.2931793 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=             1.056009
GAINMEAN=                 2.55
PROCID01= 'ct4m.20041208T193349V4'
PROCID02= 'ct4m.20041208T202848V4'
PROCID03= 'ct4m.20041208T201149V4'
PROCID04= 'ct4m.20041208T195415V4'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53347.82
MJDMAX  =             53740.88
OBJMASK = 'bp2mkbpm6.pl'
    �                          H�  H�     +  K�      ���               ��+      ``�`` @`` @	 @`` @ @ @` @ @ @ @`` @ @` @` @ @ @` @` @ @ @` @ @ @ @
 @` @` @` @	` @) @ @ @
`` @ @` @ @`` @ @` @ @ @@ @` @ @` @` @ @`` @-` @` @ @ @``` @)`````` @`` @ @ @ @ @` @ @ @ @ @ @` @ @
` @` @` @ @ @ @`` @` @`` @` @` @` @ @ @ @ @ @ @ @`` @` @```` @`` @` @` @ @ @ @ @` @` @` @	` @` @ @ @	``` @ @` @ @` @````` @ @	` @````````` @ @ @ @` @`` @` @#` @``` @`` @` @* @ @ @` @
 @ @` @ @` @	` @ @ @ @ @ @ @` @ @` @`````` @` @ @ @ @ @ @
``` @	 @ @```` @``` @ @ @
` @` @` @%` @````` @` @
```` @` @`` @``` @` @& @` @`````` @$` @` @ @ @ @ @` @ @+`
 @`
 @`
 @`
`` @ @ 
@` @ 
@` 
@` @ @``
` 
@ @`
 @````` @ @ @ @```` @ 
@
 @"`
 @`
 @`
` 
@ @`
` @`
`` @ 
@` @`
 @	 
@` @ 
@ @`
`````` @ 
@ 	@`
 @* 
@ @`
 @`
 @`
 @2`
 @`
 @`
``
``
 @ 
@` @ @ 
@ @
`
`` @`
` @``` @ 
@ @`
` @`
` @`
 @ 
@ @`
 @ 
@ @`
``
` @`
 @	`
`` @ @`
 @`
````
 @`
``````
```
````
```````#`/`8`9`>`U`a`p`�`�`�`�aa!aha�a�a�b(bXbvbpbyb�bqb{a�a�a�aqa9`�`�`�`y`L`O`4`&`)````` 
@`
`	 
E�   ��         �� �   �   PEP'PPPQ0P�p`  �@P @ @ @P @ @	 @ @PP
 $@PP&PJPP
PP @PP @P @p P
PPP @PPPPP @ @PP @
 @ @ @ @P 
@P @ @ @ @PPP @P @ @ @ @P @P
 @PP @ @ @PPP @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @/ @ @P @P @ @ @ @ @ @ @X @� @ @ @1  ��       P�0QZp` ?@p0R� P� @  ��        �@0QYp`0RA`0R� P�p   ��        �@0Q[RBR�P�   ��      RLRBR�P�   ��      RL @0Q.R�P�   ��      RL Q0Q/R�P�  	 ��      RLRBR�P�   ��      J@RBR�P�   ��      J@RB Q0Q�P�   ��       �@)0 /@RBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��       �@/0 _@RBR�P�   ��      RLRBR�P�   ��      RLRBR�  �@0P)   ��      RLRBR�P�   ��      RL@@R�P�   ��      RL Q�0 L@R�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��       �@0P�@@R�P�   ��       Q�0P�@@R�P�   ��      RLRBR�P�   ��      RLRB &@0PiP�   ��      RLRB %@0PiP�   ��      RLRBR�P�   ��      RL RAp`0R�P�   ��      RLRBR�P�   ��      RL@@`0R�P�   ��       �@(0P4@@`0R�P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��       �@-0PhRBR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�  
 ��      J@RBR�P�   ��      RLRBR�P�   ��      RLRB  �@0Q�P�   ��      RLRBR�P�   ��      RLRB @0PpP�   ��      RLRBR�P�   ��       PE0RRBR�P�  	 ��      RLRBR�P�   ��      RLRB P�0Q�P�  ) ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�  
 ��      K@RAR�P�   ��      K@ Q-0QR�P�   ��      RL Q.0QR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      J@RBR� P�0PI   ��      J@RBR�P�   ��      RLRBR�P�   ��      K@RAR�P�   ��      K@RA R 0P�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      K@RAR�P�   ��      RLRBR�P�   ��       PM0Q�RBR�P�  @ ��      RLRBR�P�   ��      RL �@0P�R�P�   ��      RLRBR�P�   ��      RLRB  %@0RiP�   ��      RLRBR�P�   ��      RLRB Q@0QQP�   ��      RLRB >@0QPP�   ��      RLRB ?@0QP P`0P�   ��      RLRBR�P�   ��      RLRB �@0P�P�   ��      RLRBR�P�   ��      RLRB QO0QBP�  - ��      RLRBR�P�   ��       P0R0RBR�P�   ��      RLRBR�P�   ��      RLRB Q�P0P�P�   ��      RLRBR�P�   ��        e@0Q�RBR�P�   ��      RLRBR�P�   ��      RLRBR� P�0P
   ��      RLRBR�P�   ��       Q�0PkRBR� P�0PW  ) ��      RLRBR�P�   ��      RLRB  �@ @0Q�P�   ��      RLRB  �@0Q�P�   ��      RLRB  �@0Q�P�   ��      RLRB  �@0Q�P�   ��      RLRB  �@0Q�P�   ��      RLRB  �@0Q�P�   ��      RLRBR�P�   ��      RLRB  @0RP�   ��      RLRB  @0R~P�   ��      RLRBR�P�   ��      RL P�0Q�R�P�   ��      RLRBR�P�   ��       E@0QRBR�P�   ��      RLRBR�P�   ��       Q^0P�RBR�P�   ��      RLRBR�P�   ��      RLRBR�  �@0P   ��      RLRBR�P�   ��      K@RAR�P�   ��       @0<@RAR�P�   ��      K@RAR�P�   ��      K@RA Q�0P�P�   ��      K@RAR�P�  
 ��      RLRBR�P�   ��      RLRB P�0Q�P�   ��      RLRBR�P�   ��      RL PQ0Q�R�P�   ��      RLRBR�P�   ��      RLRB X@0P7P�   ��      RLRB �@0P�P�   ��      RLRB  �@0Q�P�   ��      RLRBR�P�   ��      RLRB Qg0Q*P�   ��      RLRBR�P�   ��       P�0Q~RBR�P�   ��      RLRBR�P�   ��      RLRB Q 0QqP�   ��      RLRBR�P�   ��      RLRBR� P�p   ��      RLRBR�  �@   ��      RLRBR�  �@   ��      RLRBR� P�p   ��      RLRBR�P�   ��      RLA@R�P�   ��      RLRBR�P�   ��      RL@@ l@0Q#P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      J@RBR�P�   ��      RLRBR�P�   ��      K@RAR�P�   ��      K@RAR� P� @0P   ��      K@RAR�P�   ��      K@RAR� P0P�   ��      K@RAR�P�   ��      RLRBR�P�   ��      RLA@R�P�   ��       P�0Q�RBR�P�   ��      RLA@ ,@0PbP�   ��      RLRB -@0PbP�   ��      RLRBR�P�   ��      RLRB �@0QP�   ��      RLRB Q�0QP�   ��      RLRBR�P�   ��      RL R0P6R�P�   ��      RLRBR�P�   ��      RLRBR� P�0PR   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      RL@@R�P�   ��      RLRBR�P�   ��      RLRB Pc0R.P�   ��      RLRBR�P�   ��      RL@@R�P�  	 ��      RLRBR�P�   ��      RLRB Q�0P�P�   ��      RLRBR�P�   ��      RL c@0P�R�P�   ��      RLRBR�P�   ��      RL @0Q%R�P�  	 ��      RLRBR�P�   ��      RLRBR� �  ��      RL Q0Q,R� �  ��      RL  i@0Q�R� �  ��      RLRBR� �  ��       Pv0Q�RBR� �  ��      RL P�0QIR� �  ��      RL  �@0QGR� �  ��      RLRBR� �  ��       Q0Q4RBR� �  ��      RLRBR� �  ��      RLRB e@0P* �  ��        �@0QfRBR� �  ��        �@0QeA@R� �  ��        �@0QfA@R� �  ��        �@0QhA@R� �  ��      RLA@R� � 	 ��      RLRBR� �  ��      RLRB Pr0R �  ��      RLRBR� �  ��       [@0@0P�RBR� �  ��       QZ0P @p0P�RBR� �  ��       X@p`0@0P�RBR� �  ��       W@0@ @0@0P�RBR� �  ��       X@ @0@0P�@@R� �  ��       QW0@ @0@0P�RBR� �  ��       QX @0@0P�@@R� �  ��       W@ @0@0P�RBR� �  ��       X@`0@0P�RBR� �  ��       Y@0P�RBR� �  ��      RLRBR� �  ��      RLRB RU0P< PY �  ��      RLRBR� �  ��      RLRBR� P� 8  ��      RLRBR� �  ��      RL  R@0Q�R� �  ��      RL  S@0Q�R� �  ��      RLRBR� �  ��      RLRB Q�0Q � # ��      RLRBR� �  ��      RLRB P30R^ �  ��      RLRBR� �  ��      J@RBR� �  ��      RL P�0QwR� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      RL@@R� � * ��      RLRBR� �  ��      RLRB �@0Q �  ��      RLRBR� �  ��      RLRB  �@0R �  ��      RLRB P�0R � 
 ��      RLRBR� �  ��      RLRBR� P�   ��      RLRBR� �  ��      RL Q�0P�R� �  ��      RLRB  �@0Q� �  ��      RLRBR� �  ��      RL P�0QkR� � 	 ��      RLRBR� �  ��      RL P0R@R� �  ��      RLRBR� �  ��      RL@@R� �  ��      RLRBR� �  ��      K@RAR� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      RLA@R� �  ��      RLRBR� �  ��      RLA@R� �  ��       P�0Q�A@R� �  ��      RLA@R� �  ��      RL @0+@R� �  ��      RL Q0+@R� �  ��      RLA@R� �  ��      RLA@ P{0R �  ��      RL P�0�@  z@0R �  ��      RLA@ P{0R �  ��      RLA@R� �  ��      RLRBR� �  ��      RLA@R� �  ��      RLRBR� �  ��      J@RBR� �  ��      RLRBR� �  ��      K@RAR� � 
 ��      RLRBR� �  ��      RLRB  �@0R �  ��      RL@@  �@ Q�0P �  ��      RL@@  �@0R � 	 ��      RL@@R� �  ��      RL?@R� �  ��      RL@@R� �  ��      RL P�0�@R� �  ��      RL@@R� �  ��      RLRBR� �  ��      RL@@R� �  ��      RLRBR� �  ��      RL �@0P�R� �  ��      RL �@0P�R� �  ��      RL Q�0P�R� �  ��      RLRBR� �  ��      RL P�0Q�R� � 
 ��      RLRBR� �  ��      RL  @0R?R� �  ��      RLRBR� �  ��      RLRBR� P �  ��      RLRBR� �  ��       Q�0P�RBR� � % ��      RLRBR� �  ��      RL P}0Q�R� �  ��      RLRBR� �  ��      RL P0R%R� �  ��       Q�0Pg  @Pr0Q�R� �  ��      RL  �@0Q�R� �  ��      RL  �@0Q�R� �  ��      RL  �@0Q�R� �  ��      RLRBR� �  ��      RLRB PJ0RG � 
 ��      RLRBR� �  ��      RLRB  r@0R �  ��      RLRB Ps @0R �  ��      RLRB Ps @p0R �  ��      RLRB  s@p0R �  ��      RLRB  t@p0R �  ��      RLRB  v@0R �  ��      RLRBR� �  ��      K@RAR� �  ��      RLRBR� �  ��      K@RAR� �  ��       Q-0@RAR� �  ��       +@P90P�RBR� �  ��       Qf0 �@RAR� �  ��      K@RAR� �  ��       PI0RRBR� � & ��      RLRBR� �  ��        @0R1RBR� �  ��        @0R0RBR� �  ��      RLRBR� �  ��      RLRB Qo0Q" �  ��      RLRB m@0Q  �  ��      RLRB l@0Q  �  ��      RLRB m@0Q! �  ��      RLRB n@0Q! �  ��      RLRB Qp0Q! � $ ��      RLRBR� �  ��      RLRB ~@0Q �  ��      RLRBR� �  ��      RLRB Q�0P� �  ��      RLRBR� �  ��       RKp`0RAR� �  ��      RLRBR� �  ��      RLRBR�  �@   ��      RLRBR� �  ��       PI0RRBR� �  ��      RLRBR� � + �� 
   
  T�R� �  ��       S0QwR� �  �� 
   
  T�R� �  ��       Sb0Q,R� �  �� 
   
  T�R� �  ��       P60TXR� �  �� 
   
  T�R� �  ��       SJ0QD �@0P
 �  ��      T� �@0P
 �  ��       �@0Q� �@0P
 �  ��       �@0Q�R� �  �� 
   
  T�R� �  ��      T� Q70QZ �  ��      �@-P�R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  ��       R�0@R� �  ��       �@0@R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��       T�0PR� �  ��       �@0PR� �  ��       T�p0PR� �  ��       �@0PR� �  ��       T�p0PR� �  ��       �@0P
R� �  ��       T�p0P
R� �  ��       �@0P	R� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       �@0PR� �  ��       T�p0PR� �  ��       �@`0PR� �  ��       �@0PR� �  �� 
   
  T�R� � 
 ��      �@R� � " �� 
   
  T�R� �  ��      T�R�  k@ t  �� 
   
  T�R� �  ��      T�R� P� "  �� 
   
  T�R� �  ��       TO0P?R� �  �� 
   
  T�R� �  ��      T�R� P�   �� 
   
  T�R� �  ��      T� �@0P� �  ��      T� Q�0P� �  �� 
   
  T�R� �  ��        B@0TJR� �  ��        A@0TJR� �  ��        B@0TJR� �  �� 
   
  T�R� �  ��      T�R� P� &  ��       R�0Q�R� �  �� 
   
  T�R� �  ��      T�R� P)0P�  	 �� 
   
  T�R� �  ��      T�R�  P@ �  ��      T�R�  P@ �  �� 
   
  T�R� �  ��       R0R�R� �  �� 
   
  T�R� �  ��      T� �@0P� �  ��      T� �@ @0P� �  ��      T� Q� @0P� �  ��      T� Q� @0P� �  ��      T� Q� @0P� �  ��      T� Q� @0P� �  ��      T� �@0P� �  �� 
   
  T�R� �  �� 	   	  W �  �� 
   
  T�R� �  ��      �@R� � * �� 
   
  T�R� �  ��       R�0Q�R� �  �� 
   
  T�R� �  ��      T� P0Ru �  �� 
   
  T�R� �  ��      T�  	@0R� �  �� 
   
  T�R� �  ��       R�0Q�R� � 2 �� 
   
  T�R� �  ��       R�0Q�R� �  �� 
   
  T�R� �  ��      T� Pg0R* �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  �� 
   
  T�R� �  ��      �@R� �  ��      �@PR� �  ��      T�PR� �  �� 
   
  T�R� �  ��       B@0SJR� � 
 �� 
   
  T�R� �  ��      T�R� P� &  ��      T�R�  �@ %  ��      T�R�  �@ &  �� 
   
  T�R� �  ��      T�R�  �@p  ��      T�R�  �@  ��      T�R�  �@  ��      T� P�0R  �@  ��      T�  �@0R  �@   ��      T�  �@0R �  �� 
   
  T�R� �  ��       S!0QmR� �  �� 
   
  T�R� �  ��      T� Pw0R �  ��      T�  v@Q0Q
 �  �� 
   
  T�R� �  ��      T�  \@0R3 �  ��      T� P^0R3 �  �� 
   
  T�R� �  ��        @0T�R� �  �� 
   
  T�R� �  ��      T�  d@0R+ �  �� 
   
  T�R� �  ��      T� Q�0P� �  �� 
   
  T�R� �  ��      T�  �@0Q�  A@ �  �� 
   
  T�R� �  ��      T� Q0Q �  �� 
   
  T�R� �  ��       Q�0R�R� �  ��       .@0R^R� �  �� 
   
  T�R� �  ��      T�R� P2 � 	 �� 
   
  T�R� �  ��      T� Q.0Qc �  ��      T� ,@0Qc �  ��      T� Q.0Qc �  ��        @0TR� �  �� 
   
  T�R� �  ��      T� P0R� �  �� 
   
  T�R� �  ��      T�R�  �@ 3  ��      T�R�  �@ 3  ��      T�R� P� 4  �� 
   
  T�R� �  ��       Q�0R�R� �  �� 
   
  T�R� �  ��      T�R�  �@ !  ��       �@0RR�  �@ !  ��       R�0RR�  �@ !  ��       P@0P<  @0Qo �  ��       P@0P< Q!0Qp P�   �� 
   
  T�R� �  ��       R�0RR� P� 
  ��      T�R� P�   �� 
   
  T�R� �  ��      T�R� P�   ��      T� QP0Qa PY �  ��      T�R� P�P   �� 
   
  T�R� �  ��       S�0P�R�  �@PP   ��      T� Q�0P� P�P %  ��      T�R� PGP&PP%PPP !  ��      T�R� P�PPPPP @P   ��      T�R� PPeP$PP	PPP   ��      T�R� P�PPP @P   �� #   #   Q�PpP*0R Q`PE0P� P;P2PPPPPPPPPPPPP   �� /   /   P�Q$P)0R� '@PP�0P� PPPPPP @ @P
P @ @PPPPPP @P   �� 8   8   P�QRP0P� '@P�0P� PPPP @PP%PPPPPPPPPPPPPPP @P @PPPPPP	   �� 9   9   P�P�PEPQ0Q� QzPCP0P� P	PPPPPPPP(PPPPP @PPP @PPPPPPPP @PPP   �� >   >   P�PPAP�P�PaP�0P� QBP;PPP'PP�P
P0P PPPPPP
PPPP @ 	@PPPP
PPPPPPPPP @ @   �� U   U   P6P�P
P&PP@PcP2PP|P!PTP�PP+0P� P�PePPPPPPP	P1PPP�Pp PPPPP'PP @ @P @ @P @PPPP @P
P @P @PP @ @PP @   �� a   a   PIP*P1P:P"P�PPP*PaPPPlPPP0Ql PP�P@PP-PPPPP?PPPPP	PP~ @PP0P PP @P @PPPPPPP @PPPP
 @P @ @ @ @ @PP @P	 @PP @P   �� p   p   P)PPP�PPSP7PsP�PP PPCPoP P\P;0PH QPPPPPPPPPPP	PPPPPP
PmPP 
@0P P @PPPPPPPPPPPPP @PPPPPP @ @PPPP @PPPPP @P @P @P @ @ @P @   �� �   �   P\PPPPP& @P+PPPPPPP PjP;PFPCPP PPP
P7P;PPP,P�0P9 P�PPPPPPPPPP  @P
P @PPPWPPP
PP @Pp P @ @P @ @P @PPPP @P @ @PP @ @PP @P @ @ @ @P @ @P @ @ @ @ @ @P @   �� �   �   P5PP
PP4P	PPP'PPPPP
P
PP @PPP/PPPP&PPP,PP
P  @PP2PPPPAPP~P#P�PPP,0P PpPP-PPPP"P	PPPPPPPPP P
 @PPPPPPPPbPP @PP @PPP0P  @PPPP @P	PP @P	 @P	PPPPP @PP @P @ @ @PP 	@ @ @P @ @ @
PP @ @ @   �� �   �   P;P @PPPPPAPPPP$P	PPP
PPPP	PPPP#P	P)PAPPP!P$PPPPP
PP @PPpP PPPPPP`P(0PI P/PeP @ !@PP4P @PPPPPPPP @ @PP	P @PPPPPP	PPPPP @P
PP	PP @P0P  @P @PP @PP	PP @ @ @ @PPPPPP	 @ @ @PP @ @ @PP @P @ @ @ @ @ @ @ @ @ @PPP @P @P @   �� �   �   P$P. @P&P !@P	PPPP @PPPPPPPPPPPPP
PP
PPPPP @PPP @PPPP	PPPPP @PPPPPPPPPPPP @PP
PP	P9PPP0P @P @P @PPP>PPP@0P PuP,PPP @PPP#PPPPPP @PPPPPPP @PPPPP @ @PP @P 	@PPP
PPP;PP"P
PPPP @ @0P P @PP @ @PPPPPPP @ @ @ @ @P @ @ @PP @PP @	 @P @ @ @PP @
 @ @ @ @ @ @   ��     PPPPPPPPPP @P
PPPPP @PPPPPPPPP	PP @PP
PPP @P	PP
PPP
PPPPPPPPP1PPPP @PPPPPPPPPPPP @PPP
 @PPP @PPPPPP
P(PPPPPPPP	PP @PPePPPP0P PPEP- @PPPP
PPPPP 2@ @PPP @P 	@PPPPPPPPP @PP @ @PPPPPPP @PPPPP
PPPPPPPPPPPPPPP @P @p  @PPPP @P @P @ @PPPPP @P @ @P @ @P @ @ @ @ @P @ @ @PP @ @ @ @ @ @ @   ��!  !   P7PPPPPPPPP&PPPPPP	 @PP 	@ @PP @P @PP @PPPPPPPP @PP @PPPPP	PP @PP @PP	PPPPPPPPPP @P @PP	PPP
PPP @PP
PPPPP @PPP&PPPP PP @PPPPPPPP
P. 
@P;0P	 P>PuPPPPPPPPPP @PPPPPP @ @P @P	P @ @ @PPPPP @P
 @ @ @PP @P @PPPPPP	PP	PPPP
PPPP 
@PP @ @PPPP @ @ @PP @ @ @P @ @P @ @ @ @PPPP @ @ @ @ @PP @P @ @P @ @ @ @ @ @ @ @ @ @   ��h  h   PPPPPPPP @ @PPPPPPPPP	PPPPPP @PPP @PPPP @PPPPPP @P
PP
 @PPP
PPPPPPP @PPPPPPPPP @P @PP @ 	@PPPPPPP	PPP @ @PPPPP @P @PPPP	PPPPPP @ @PPPPP @ @PP
PPP
PPPPPP @PPPP @ @PPPPPPPP @ @PPPP0P P) 2@P*PPP	PPPPPPPPPPPPPPPPPPPP @PP @P
P @P @ @P @PPP @P	PP @ @ @PPP @PPPP 
@PPPPPPPPPPP
P	PPPPPPPP	PPPPP @P @P @P0P`PP @P @P @P @ @P @ @PP @ @ @ @P @PP @P @ @ @P @ @ @ @ @P @P @ @ @ @ @P @   ���  �   P'PPPPPPPPPP @PPPPPPPPPP @ @PPPPPPPPPPPP @P @ @PP @ @ @ @PPPPPPPPP @ @PPPP @ @PPP @PP @P @P @ @PP @PPP @ @PP @PP @P @PPPPPP	PPPPPPP @PPP @ @PPP
P @P @PP @P @P @PPPP	PPPPPP @PPP
PPPPPPP @PPPP	PPP
P!P @PPPPPP0P P&PE @PPPPPPPPPP	PP @P	PPPP @P @ @P @P @ @P @ @ @P @ @PPPPPPP @PPP @P @ @P @P @PP @P	P @PP @PPPPPPPPP @PP @PP @P @PPPP @PPP @ @ @ @P @ @ @PP @ @ @p  @ @PPP @ @PPP @PP @ @ @ @ @PP @ @ @PP @ @ @P @ @ @ @ @ @ @P @ @ @	 @	 @ @ @ @ @   ���  �    @PP	P
P	P @PPPPPPP @PPPPP
PP
 @ @PPP @PPPP @PPPP @ @PPPPP	P @PPPPPPP @ @ @PPP	PPPPPPPPPP
PPP @P @PPP	PPP @PPPP	PPPPPPPPP @ @ @P @PP @ @PPPPPP	PP @PPPPPPPPP @PP @ @ @PP	 @P	P @PPPPPPPPPPPPPP
 	@P	P! @PPPPPPP 	@ @ @PPP
PPPPPPPPPPPPPPPPP @PPPPPPPP0P	 PPPPPPPPP9PP @P 	@PPPPPP @PP @ @PP 	@PPP @P @ @ @PPPP @PPPPPP @PPPPP @ @ @ @P @P @PPP	P @ @ @PP @PP @PPP @PPP @PPPP @ @PP @ @P
PPPPPPP @P @ @PPPPPPP @P @p P @ @PP @PPP @ @ @ @ @ @ @ @ @ @P @ @ @P @ @ @ @ @ @ @P   ���  �   PPPPP @P @P @P @PPP @PP @PPP @PP	PPP @P @PPPP @ @PPP @P	P @P @ @PP @P @PP	PPPPPP @P @PP @PPPP @P @P @PPPP @ @ @PP	PP @PPP @PPPPPPPP	P
 @P
 @PPP @ @PP @P @P
PPP @ @PPP 	@ @PPP @ @ @P	PP @PPPPP	P
P @P 	@PPPP @P @ @ @PPPP @P @PP @PPPP	P @ @P @PPPPPPP @PPPP
PPPPPPP @PPP @PPPPP	PP0P P	P2P! @P @P @ @ @P @ @PPPP	P @ @ @P @PP @PPPPPP @ @PP 
@PPP @P @PP @ @ @ @PPP @ @P @ @P @P @ @ @P @P @ @ @PPPPPPP @ @PPPPPPPP @PPPPPPPP @P @PPPP @P @PP @ @PP @PPPP @PP @P @ @PPP @P @P @PP @ @ @P @ @ @ @ @P @ @P @ @ @	 @ @PP @
 @ @ @ @P @  ��(  (   P+PP @PP 	@PP @P @P @P	PPPPP @P @ @ @ @ @ @PPPPPPP @P @ @P @PPP @ @PP @ @PPPPPPPPPP @ @PPP @PPPPPP @P @P
PPP @PPP @P @P @ @PPPPP	 @PPP @PPPPPP @P @ @PPP @P @P @ @P @PP @ @PPP @ @ @ @P @PPPP @ @PPPP @PPPPPPP @PP @PP @PPPPP @ @PPPPPP @P	PPPP	PP @ @ @P 	@PP @P @ @PP @ @P @ @PPPPPPPPPP @ @PPPPPPPP
 @PPPPPP @PP @P	0P PPP# @PPP 
@PPP @ @PPPPPPPPPPP @ @ @PPPPP @ @ @PP @PP @ @P @ @ @ @ @P @P @ @ @P @
P @PPPPPPP @ @P @PPPPPPPPPP @ @PP @PPP @PPPPPP @PPPP @PPPPP @ @P @ @PPPP @ @PP @ @P @P @PP @PPP @P @ @ @ @P @
 @P @P @ @ @PPPPPP @P @ @ @ @ @ @P @P @ @ @ @ @ @ @    ��X  X    @PPPP @PP @ @P @ @PP @PP @P @PP @PPPPP @PPP @P @ @ @P @ @PPPPPP @ @P @ @PP @ @P @P @ @ @PP @P @P @ @ @P @P @ @P @ @P @P @PPPPPP @P @PP @ @P @ @ @ @P @PP @PP @P @PPP @PPPP @ @ @ @PP @ @ @P @P @PPPPP @ @ @ @PPP @P @P @PP @ @PP @P @PP @PPPP @PPPPPPPPPPPPP @PP @ @P	 @P 	@ @P @PPP @P @P @PPP @ @ @PPPPP @P @ @ @PP @PP	P @PPPPPP @P @ @ @ @PPPPP @PP @ @P P @P0P P	P @PPPPPPPPPP @ @PPPPP	P @PPPP @ @PP 
@P @ @ @PPP @ @P @PP @P @P @ @ @ @ @ @ @
P @P @ @P @ @ @ @P @PP @PPP @ @ @ @ @PP @P @P @ @ @ @ @ @P @P @PPPP @PPPPPPPP @ @PPPP @PP @PP @ @PP @P @ @ @P @ @P @0P @ @P @ @P @P @ @ @PP @ @ @ @PP @ @ @ @ @ @   ��v  v   P
P @PPPP @P @P @P @ @P @
P @ @P @P @PPPP
 @P @PPPP @P @ @ 	@P @PP @PPP @P @P @PP @PPP @ @ @P @ @PPPP @ @ @P @P @ @ @ @P @ @PPPP @ @P @P @ @P @ @ @PPPP @ @ @ @ @P @PP @P @ @PP @ @ @ @P @P @P @PPP 	@P @PPP @P @ @P @ @P @ @ @ @ @P @ @PP @P @ @PP @PP @P @ @ @PP @PPPPP @P @ @P @P @P @ @PPPP @PPPP @PPPPP @P @ @ @ @ @P @ @P @ @P @PP @PP @PPPP	P
P
P @ @P @ @PPP @P @ @PPPPP @PP @P @PPp P @P	P!PPP	PPPPP @ @ @P @P @ @PPPPPP @P @PPP @P @ @P @P @P @P @ @ @ @PP 
@P @ @ @ @P @ @ @P @PP @P @ @ @PP @PP @P @ @PPP @P @ @ @ @ @ @ @ @P @ @PPPPP @ @PP @ @PP @ @ @P @ @ @PPP @PP @ @P @ @P @ @ @ @ @P @ @ @ @P @ @
 @P @ @P @PP @ @P @ @ @	 @ @
 @ @ @ @ @:   ��p  p    @PPP @PPP @P @ @ @ @P @ @ @ @ @ @P @ @ @PPP @P @ @ @P @ @ @ @ @P @P @P @ @PP @P @ @ @ @ @ @ @
PP @ @P @ @PPPP @ @PP @ @P @ @P @ @ @P @ @P @ @ @PPPP @PP @ @ @ @PP @ @ @ @ @ @P @ @ @ @PP @ @ @PP @P @PP @ @PP @ @ @ @ @ @ @ @ @P @P @ @P @ @	 @P @P @ @P @P @ @PPPPPPPP @ @P @ @P @ @PP @ @ @P @PPP @ @ @ @ @ @PP @PPPPPP @ @ @ @ @ @P @P @ @ @ @PP @PPP @ @P @PP @ @ @PPPPP @PPPPPP @PPPP @PPPP @PPP @P0P P	PPP 
@PPPP @PPP @PPPPP @PP @P @ @P @PP @PP @ @ @P @P @ @P @ @ @P @ @PP @P @ @PP @ @P @ @ @ @ @
 @ @P @ @ @ @PP @ @ @P @ @P @ @ @ @ @PP @ @PP @ @ @P @P @ @ @P @PP @ @ @ @P @PP @ @PPP @PP @ @P @ @ @ @ @P @ @ @
P @P @ @%P @M @ @
   ��y  y   PPP @ @ @ @ @P @ @PPP @P @ @P @ @PP @ @PPP @PP @P @ @ @PP @P @P @ @ @P @PP @P @P @P @PP @P @ @ @P @	 @P @ @P @P @ @P @ @ @ @PP @PP @PP @P @P @ @ @ @P @P @PP @PP @P @P @ @ @ @ @ @ @ @PPPP @P @P @ @P @P @P @ @ @ @ @ @ @ @ @ @ @ @ @PPPPPPPPP @ @ @ @P @ @ @PP @ @PP @PPP @PPP @P @ @ @P @PPP @PP @PP @ @ @ @ @P @PP @PPPP @P @ @ @P @PPPP @P @ @ @PP @ @P @P @	 @P @PP @P @PP @PPP @PP @P @PPP @PPP @PPPPPPPPPP @PP	PPPP @PPPPP @PPPPPPP @ @PPPP	 @P @PPPPP @ @PP @PPPPP @ @ @ @PP @ @ @ @ @ @PPPPPPP @ @ @ @ @ @ @P @ @ @ @	PPP @ @ @ @P @PP @ @ @P @ @ @P @PP @ @ @ @P @PPP @PP @ @PP @ @PP @PPPPP @ @ @P @ @P @ @ @ @P @ @ @ @ @P @ @$P @ @ @	 @$ @^   ���  �   P	 
@ 
@PP @ @ @P @PP @ @P @ @
P @PPPPP @ @P @ @ @ @ @P @ @ @PP @ @P @ @P @ @P @ @ @ @P @P @ @P @ @ @ @ @ @ @ @ @ @ @ @PPP @ @P @PP @ @ @ @P @ @ @P @ @ @P @P @ @ @ @ @ @
 @ @PP @ @ @
 @ @ @ @P @ @ @P @PPPP @PP @ @ @ @ @P @P @P @ @ @	 @ @ @ @P @P @ @ @PPP @ @ @ @
P @ @ @ @P @P @ @ @ @ @ @ @PPPP @PP @ @P @P @ @PP @ @ @ @ @ @ @P @ @P @P @P @P @P @P @ @ @ @ @ @P @PP @P @PPPPPPPP @PP @P @PPPP @PP @PPP @ @PP @P @P @P @PP @PPPPPP0P @PPPP @ @PPPPP @PPPPP @ @ @ @ @PP @ @P @PPPPP @P @ @PP @ @ @ @ @ @PP @ @PPP @
 @ @ @ @P @ @P @P @ @ @ @ @ @ @ @	 @ @ @	 @P @P @P @ @ @P @ @ @P @ @ @P @P @ @ @PP @PP @ @ @ @P @ @PPP @ @ @ @ @ @ @ @P @ @ @ @ @ @ @ @ @ @	 @ @ @ @U   ��q  q   P @ @ @PP @PP @ @ @ @ @ @ @ @ @ @ @ @ @ @ @PPPPP @ @ @P 	@ @ @ @ @ @ @ @ @ @PPPP @ @ @P @ @ @ @P @ @ @ @ @ @
 @ @ @ @ @ @P @ @ @ @ @	P @ @P @P @ @ @ @ @ @ @P @ @P @PP @P @ @	PPP @ @PPPP @ @ @PPP @ @ @P @ @ @P @ @ @P @ @PP @ @ @ @ @P @ @ @ @P @ @ @ @P @P @
 @ @ @P @ @ @PPP @ @PPP @ @P @PP @ @ @ @ @P @PP @PP @PPPPP @ @ @ @ @ @ @ @PPP @ @ @ @PPPP @P @ @ @P @ @P 	@P @P @ @PP @PPPPPPPPP @ @ @ @P 	@ @ @ @ @P @ 	@ @0P PP	PPP @PPP @ @P @ @PPPP @P @PPPPPPP @PPPP @	P @ @P @ @ @ @ @P @PP @P @ @ @PPP @ @ @P @P @ @ @ @P @PP @3 @ @	 @	 @ @P @ @ @ @ @ @ @ @ @ @ @ @ @ @P @ @P @ @	 @P @ @ @PPPPP @ @ @ @ @PP @ @P @P @ @ @P @ @ @* @ @ @ @ @2 @`   ��{  {   P	 @ @PP @ @ @ @ @P @ @ @P @ @ @ @ @ @ @P @PPPP @ @ @PPPP @P @ @ @ @ @ @P @ @PPPP @ @ @ @ @ @P @	 @ @ @ @P @P @ @ @ @ @P @ @ @ @ @ @ @ @ @ @ @ @ @ @ @P @ @ @ @PPP @ @PP @P @ @ @P @PPP @P @PP @ @ @ @ @ @	PPP @P @ @ @P @ @P @P @ @PP @ @P @ @ @P @ @P @P @P @ @ @ @P @PP @P @ @P @ @ @ @ @P @P @ @P @ @P @ @ @ @PP @P @ @P @P @PP @ @P @ @ @ @P @ @ @PPPP @ @ @P @PP @ @P @ @ @P @PPPPPPP @PPP @PP @ @PP @ @PPPP @PP @PP @0P` @PPP @PPPP @PPPP @PPP @PPP @P @P @PPPP @ @ @ @PPP @P	PP @ @ @ @P @ @P @PP @ @P @ @ @ @ @P @ @ @ @ @ @ @ @ @ @ @ @
 @	 @ @ @ @ @ @ @ @ @ @ @ @ @ @PP @ @ @ @P @ @PP @ @ @P @P @P @P @P @PP @PP @ @P @ @ @ @ @
 @ @  @ @ @s @   ���  �   PPP @P @ @ @P @ @P @ @ @ @ @P @ @ @ @ @ @ @ @P @ @P @P @P @ @ @ @ @ @
 @ @P @ @ @ @ @ @ @ @ @ @ @ @ @ @ @PP @ @ @ @ @ @ @ @ @ @PP @ @P @P @ @ @	 @ @ @ @ @ @ @ @ @ @ @ @ @P @P @	 @ @ @ @ @ @PP @PP @ @ @ @PP @ @ @ @ @	 @ @P @ @ @P @ @ @ @ @
 @6 @ @ @ @ @ @ @ @ @P @ @P @ @ @ @ @ @ @PP @ @ @ @P @P @PPP @P @ @ @ @0P PP @ @P @PP @ @ @P @ @P @ @ @ @ @ @ @P @ @ @P @ @ @ @	P @ @	P @ @ @
 @P @ @P @ @? @
 @ @ @. @ @ @ @ @ @ @ @ @ @ @ @ A   ���  �   P	PP @ @ @P @PP @ @
 @ @ @	 @ @ @ @ @ @P @P @ @P @ @P @ @ @ @ @ @ @ @ @P @PPP @ @P @ @ @ @ @ @ @ @ @ @ @P @ @ @ @PP @ @ @PP @ @ @P @	 @ @ @ @ @P @	 @ @ @PP @P @P @P @ @ @ @ @ @ @ @ @ @ @PP @ @P @ @ @P @ @ @ @ @ @ @ @PPP @ @ @ @ @ @ @
P @ @ @ @ @	 @ @P @ @PPPPP @ @ @ @ @PP @PP @P @	 @ @ @PP @ @P @ @P @ @ @ @P @ @P @ @PPP @P @ @PPPP @PP @P @P @ @P @ @ @P @ @P @ @ @P @P @ @ @
 @PP @ @ @ @ @ @ @P @ @
 @ @P @ @ @ @P @ @ @ @3 @ @ @ @ @ @P @ @ @
PP @ @ @ @ @P @ @ @ @ @ @ @P @ @ @ @ @ @ @4 @! @�   ���  �    @ @ @ @ @PP @ @P @P @P @ @ @	 @ @P @ @ @ @P @ @ @ @ @ @ @ @ @ @( @ @ @ @ @ @ @PP @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @P @ @ @ @ @ @P @ @ @P @ @ @P @P @ @ @ @ @ @ @P @ @ @ @ @ @ @ @ @ @ @PP @ @ @ @ @ @ @PP @ @PP @ @ @P @ @PP @P @P @ @ @PP @P @P @P @ @ @P @ @P @ @ @ @	 @ @ @ @ @ @PP @ @ @ @ @ @ @ @ @ @ @ @P @ @ @A @ @ @P @
 @ @ @ @ @ @ @
P @ @ @ @P @ @ @ @ @ @ @	 A  ��q  q   P @ @ @ @ @	P @P @ @ @!P @ @ @	P @ @ @
 @ @ @ @ @ @ @ @ @ @ @
 @ @ @ @ @ @PP @ @
 @P @ @ @ @ @ @ @. @ @ @ @ @ @ @' @ @ @ @ @ @ @ @
 @ @ @ @ @ @ @ @	 @ @ @P @P @ @ @ @ @	 @ @PP @ @ @ @ @ @ @. @ @ @ @ @ @ @ @PP @ @ @	 @PP @ @ @ @ @ @ @ @ @	 @P @ @ @PP @ @PP @ @ @ @P @P @PPPPP @ @ @PP @ @P @P @P @ @
P @ @ @ @ @P @ @ @ @ @& @ @ @ @ @ @ @P @J @	 @ @ @ @
 @- @
 @
 @ @ @ @ @ @ @ @ @	 @ A   ��9  9   PPPP @ @ @P @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @< @ @ @ @ @ @ @ @ @ @ @5 @ @ @ @ @	 @f @ @ @ @
 @ @ @	 @ @ @ @	 @ @ @ @ @P @ @ @ @) @ @ @P @ @PP @ @PP @P @P @ @ @P @
 @ @ @ @	P @
P @ @ @P @	 @ @ @	P0P  @ @ @PP @ @P @ @ @P @P @P @P @ @ @P @ @ @
P @P @P @	P @/ @ @$ @ @P @ @P @P @ @ @8 @E @ @ @ @ @ @- @	 @ @P @ @ @ A  �� �   �   PP @ @ @ @ @ @  @ @ @ @ @ @P @ @ @ @ @ @ @ @ @ @ @ @H @ @	 @P @ @ @" @& @	 @ @ @E @ @ @1 @( @ @& @ @ @ @ @ @ @ @ @ @ @ @ @Q @ @ @ @ @ @PP @ @P @ @ @ @ @ @P @P @ @ @ @ @PP @PP @ @ @ @ @ @P @P @ @ @ @ @ @ @ @0 @ @. @!P @ @ @/ @ @` @6 @ @ @ A  �� �   �    @ @ @ @	P @ @$ @ @ @ @ @ @ @ @P @ @ @ @ @# @ @ @4 @# @ @P @ @ @	 @Q @ @ @ @J @ @ @G @ @ @p @ @ @ @ @ @
 @ @K @ @	 @ @ @ @ @ @ @	P @P @P @ @ @P @	 @ @ @ @ @ @ @ @ @ @P @ @ @
 @ @ @ @ @ @P @
 @ @ @ @ @; @ @	 @ @z @	 @+ @ @N @ @ @V @�   �� �   �    @> @ @- @P @P @ @W @7 @( @$ @# @3 @A @J @ @ @ @A @  @P @ @W @ @X @ @ @ @P @PPP @ @ @ @ @ @	 @ @ @ @
 @ @ @ @P @ @ @ @ @ @ @ @ @  @ @ @ @E @ @ @� @ @! @* @ @1 A   �� y   y    @ @ @P @DP @
P @ @ @7 @ @w @
 @* @ @ Am @ @ @ @ @6P @U @ @" @/ @ @ @ @ @P @ @
 @ @ @ @ @ @	P @ @ @P @# @ @ @
 @P @ @� @ @� @ @ A�   �� L   L    @ @ @ @L A @ @� @N @t @J @& @, @y @ @ @ @ @ @ @ @ @, @P @ @, @ @3 @ @ A
 @F @% AM   �� O   O    @ @1 @ @ @ @Y @� @[ @M @ @R @O @ @ @W @ @ @ @_ @% @ @H @ @ @# @ @ @ @� @ @L @ @n @E A�   �� 4   4    @ @ @ @X AL BE @( @ @ @ @. @ @P @ @# @ @ @ @� @ B4   �� &   &    C @* @a @A @ @ @u @ @
 @ @ @[ @ @� BA  �� )   )   P @ @- Cc @� @ @& @ @ @ @& @U @ @{ @ A" A  ��        @ @l C� @f A A�  ��       P An CJ B< A  ��        Cf AC @f B�  ��        @ A� E. A  ��        DF C�  �� 
   
    G�  �� 
   
    G�  �� 	   	   H   �� 
   
    G�� �� 	   	   H 