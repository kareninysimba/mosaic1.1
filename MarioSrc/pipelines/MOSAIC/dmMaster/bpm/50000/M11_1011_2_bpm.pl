  �V  ,	  #$TITLE = "DFLATS Observation(s)"
$CTIME = 973251213
$MTIME = 973254177
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
RAWFILE = 'n501453.fits'       / Original raw file
FILENAME= 'K4M10B_20101027-IFlat-kp4m20101027T235153F' / Current filename
NEXTEND =                    8 / Number of extensions
OBSTYPE = 'dome flat'          / Observation type
PROCTYPE= 'MasterCal'          / Processing type
PRODTYPE= 'image   '           / Product type
MIMETYPE= 'application/fits'   / Mimetype of this data file
EXPTIME =                   55 / Exposure time (sec)
OBJRA   = '18:50:24.22'        / Right Ascension
OBJDEC  = '-17:00:00.0'        / Declination
OBJEPOCH=               2010.8 / [yr] epoch

          # Single exposure quantities are representative from 1st exposure
TIMESYS = 'UTC     '           / Time system
DATE-OBS= '2010-10-27T23:51:53.0' / Date and time of exposure start
TIME-OBS= '23:51:53'           / Universal time
MJD-OBS =       55496.99436343 / MJD of observation start
ST      = '18:50:24'           / Sidereal time

OBSERVAT= 'KPNO    '           / Observatory
OBS-ELEV=                2120. / [km] Observatory elevation
OBS-LAT = '31:57.8 '           / [deg] Observatory latitude
OBS-LONG= '111:36.0'           / [deg] Observatory longitude
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2010.8 / Equinox of tel coords
TELRA   = '18:50:24.22'        / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
TELFOCUS=                -8873 / Telescope focus
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off     '           / ADC Mode
ADCPAN1 =                 0.01 / [deg] MSE ADC 1 prism angle
ADCPAN2 =                 0.05 / [deg] MSE ADC 2 prism angle
CORRCTOR= 'Mayall Corrector'   / Corrector Identification

INSTRUME= 'mosaic_1_1'         / Mosaic detector
NDETS   =                    8 / Number of detectors in mosaic
FILTER  = 'I Nearly-Mould k1005' / Filter name
FILTID  = 'k1005   '           / Unique filter identification
PHOTBW  =              191.459 / [nm] Filter wavelength FWHM
PHOTFWHM=              191.459 / [nm] Filter wavelength FWHM
PHOTCLAM=              820.453 / [nm] Filter wavelength width
FILTPOS =                    5 / Instrument filter position
FILTERSN= 'k1005   '           / Filter serial number
NOCGAIN = 'normal  '           / NOCS gain setting
SHUTSTAT= 'dark    '           / Shutter status
ENVTEM  =                   13 / [celsius] MSE temp7 - Ambient
DEWAR   = 'Mosaic1.1 Dewar'    / Dewar identification
DEWTEM  =               -178.5 / [celsius] MSE temp1 - Dewar tank
DEWTEM2 =                 -9.7 / [celsius] MSE temp3 - Fill Neck
CCDTEM  =          -106.199997 / [celsius] MSE temp2 - CCD Focal Plate

OBSERVER= 'Howell, Schweiker, Mathis, Reedy' / Observer(s)
PROPOSER= 'David Sawyer'       / Proposer(s)
PROPID  = '2010B-2005'         / Proposal identification
OBSID   = 'kp4m.20101027T235153' / Observation ID
EXPID   =                    0 / Monsoon exposure ID
NOCID   =        2455497.70176 / NOCS exposure ID

CONTROLR= 'Mosaic System Electronics  Sep 2' / MSE name and revision date
DHEFILE = 'mosaic1_Seq2amp250Kpx.ucd' / Sequencer file

NOHS    = '2.0.0   '           / NOHS ID
NOCMDOF =                    0 / [arcsec] Map Dec offset
NOCMITER=                    0 / Map iteration count
NOCNO   =                   31 / observation number in this sequence
NOCPOST = 'dfs     '           / ntcs_moveto ra dec epoch
NOCDROF =                    0 / [arcsec] Dither RA offset
NOCDHS  = 'DFLATS  '           / DHS script name
NOCORA  =                    0 / [arcsec] RA offset
NOCODEC =                    0 / [arcsec] Dec offset
NOCMPOS =                    0 / Map position
NOCDITER=                    0 / Dither iteration count
NOCMPAT = '4Q      '           / Map pattern
NOCMREP =                    0 / Map repetition count
NOCDDOF =                    0 / [arcsec] Dither Dec offset
NOCTOT  =                   50 / Total number of observations in set
NOCSCR  = 'DFLATS  '           / NOHS script run
NOCFOCUS=                    0 / [um] nics_focus value
NOHS    = 'Mosaic.1.1'         / NOHS ID
NOCTIM  =                   55 / [s] Requested integration time
NOCOFFT = '0 0     '           / ntcs_offset RA Dec offset (arcsec)
NOCSYS  = 'kpno 4m '           / system ID
NOCNUM  =                   10 / observation number request
NOCLAMP = 'On      '           / Dome flat lamp status (on|off|unknown)
NOCRBIN =                    1 / CCD row binning
NOCMROF =                    0 / [arcsec] Map RA offset
NOCSKY  =                    0 / sky offset modulus
NOCNPOS =                    1 / observation number in requested number
NOCCBIN =                    1 / CCD column binning
NOCTYP  = 'DFLATS  '           / Observation type
NOCDPOS =                    0 / Dither position
NOCDPAT = '5PX     '           / Dither pattern
NOCDREP =                    0 / Dither repetition count

RAINDEX =                    0 / [arcsec] RA index
RAZERO  =                570.5 / [arcsec] RA zero
ALT     = '41:02:15.0'         / Telescope altitude
DECINST =                    0 / [arcsec] Dec instrument center
DECDIFF =                    0 / [arcsec] Dec diff
FOCUS   =                -8873 / [mm] Telescope focus
PARALL  =                  360 / [deg] parallactic angle
DECZERO =              -375.12 / [arcsec] Dec zero
AZ      = '180:00:00.0'        / Telescope azimuth
RADIFF  =                    0 / [arcsec] RA diff
RAINST  =                    0 / [arcsec] RA instrument center
DECOFF  =                    0 / [arcsec] Dec offset
DECINDEX=                    0 / [arcsec] Dec index
RAOFF   =                    0 / [arcsec] RA offset

DOMEERR =               135.85 / [deg] Dome error as distance from target
DOMEAZ  =                    0 / [deg] Dome position

MSEREADY= 'dark    '           / MSE shutter ready (none|guide|dark|restore)

TCPGDR  = 'off     '           / Guider status (on|off|lock)

DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1_1        '  /  instrument identifier
DTCALDAT= '2010-10-27        '  /  calendar date from observing schedule
ODATEOBS= '                  '  /  previous DATE-OBS
DTUTC   = '2010-10-27T23:57:30'  /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2010B-2005        '  /  observing proposal ID
DTPI    = 'David Sawyer      '  /  Principal Investigator
DTPIAFFL= 'National Optical Astronomy Observatory'  /  PI affiliation
DTTITLE = 'On-sky Commissioning of MOSA 1.1'  /  title of observing proposal
DTCOPYRI= 'AURA              '  /  copyright holder of data
DTACQUIS= 'mosaic1dhs-01-4m.kpno.noao.edu'  /  host name of data acquisition com
DTACCOUN= 'cache             '  /  observing account name
DTACQNAM= '/home/data/n501453.fits'  /  file name supplied at telescope
DTNSANAM= 'kp1247271.fits    '  /  file name in NOAO Science Archive
DTSTATUS= 'done              '  /  data transport status
SB_HOST = 'mosaic1dhs-01-4m.kpno.noao.edu'  /  iSTB client host
SB_ACCOU= 'cache             '  /  iSTB client user account
SB_SITE = 'kp                '  /  iSTB host site
SB_LOCAL= 'kp                '  /  locale of iSTB daemon
SB_DIR1 = '20101027          '  /  level 1 directory in NSA DS
SB_DIR2 = 'kp4m              '  /  level 2 directory in NSA DS
SB_DIR3 = '2010B-2005        '  /  level 3 directory in NSA DS
SB_RECNO=              1247271  /  iSTB sequence number
SB_ID   = 'kp1247271         '  /  unique iSTB identifier
SB_NAME = 'kp1247271.fits    '  /  name assigned by iSTB
RMCOUNT =                    0  /  remediation counter
RECNO   =              1247271  /  NOAO Science Archive sequence number
XTALKFIL= 'Mosaic11_xtalkdummy.txt' /

GAIN    =                  1.1 / Approx inv gain (ADU/e)
RDNOISE =                  5.0 / Approx readout noise (e)
SATURATE=               220000 / Approx saturation (ADU)

DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.001509 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         3.103876E-42 / depth of longest jump down
CCDMEAN =            149304.45
OVSCNMTD=                    3
DQGLFSAT=                   0.
IMCMB001= 'kp1247271.fits'
IMCMB002= 'kp1247272.fits'
IMCMB003= 'kp1247274.fits'
IMCMB004= 'kp1247275.fits'
IMCMB005= 'kp1247277.fits'
IMCMB006= 'kp1247278.fits'
IMCMB007= 'kp1247281.fits'
IMCMB008= 'kp1247285.fits'
IMCMB009= 'kp1247286.fits'
IMCMB010= 'kp1247287.fits'
NCOMBINE=                   10
QUALITY =                   0.
GAINMEAN=                  1.1

PIPELINE= 'NOAO Mosaic Pipeline' / Name of calibration pipeline
PLVER   = 'MOSAIC V1.1'        / Pipeline version
EFFTIME =                   55 / [s] Effective exposure time
PLPROPID= 'CAL     '

PLQUEUE = 'K4M10B  '           / PL Queue
PLQNAME = '20101027'           / PL Dataset
PLPROCID= '7ae6e75 '           / PL Processing ID
PLFNAME = 'IFlat-kp4m20101027T235153F' / PL Filename
PLOFNAME= 'n501453_pl'         / Original file name
PCOUNT  =                    0 / No 'random' parameters
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'ccd2    '           / Extension name
INHERIT =                    T / Inherits global header
DATE    = '2010-11-02T18:19:17' / Date FITS file was generated
IRAF-TLM= '2010-11-03T17:47:13' / Time of last modification
IMAGEID =                    2 / Image identification

CCDNAME = 'SN09-01 '           / CCD name
AMPNAME = 'SN09-01:A'          / Amplifier name
BUNIT   = 'adu     '           / [adu] ADU counts
CCDSUM  = '1 1     '           / CCD pixel summing
DETSEC  = '[2049:4096,1:4096]' / Detector section


XTALKCOR= 'Nov  2 11:02 No crosstalk correction required'
BIASFIL = 'K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z[ccd2]' / Bias reference
DQOVMJLR=                   5. / length of deepest jump down
DQOVMJJR=             51.97789 / amplitude of deepest jump
DQOVJPRB=                   1. / probability of a jump
DQOVMIN =               393.58 / min in overscan region
DQOVMAX =             648.6735 / max in overscan region
DQOVMEAN=             635.5618 / mean in overscan region
DQOVSIG =             22.95058 / sigma in overscan region
DQOVSMEA=             635.4491 / mean in collapsed overscan strip
DQOVSSIG=             3.379736 / sigma in collapsed overscan strip
SATPROC = 'Nov  2 11:05 Sat: 220000. ADU (242000. e-), grw=0'
TRIM    = 'Nov  2 11:05 Trim is [1:1024,1:4096]'
FIXPIX  = 'Nov  2 11:05 Fix Mosaic11_dummy_2_bpm.pl + sat' /
OVERSCAN= 'Nov  2 11:05 Overscan is [1025:1074,1:4096], function=minmax'
ZEROCOR = 'Nov  2 11:05 Zero is K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z' /
CCDPROC = 'Nov  2 11:05 CCD processing done'
AMPMERGE= 'Nov  2 11:06 Merged 2 amps'
DQGLMEAN=             152834.8
DQGLSIG =             3297.021
DQDFCRAT=             2778.814
PUPILCOR= 'Nov  2 11:07 maximum amplitude = 1.586E-7'
DCCDMEAN=             1.013431
CCDMNTMP=             149304.5
    �                          �  �     V  #      ���               ��V      d� �@ 
@p�1GPH CP31�P @P1!�P`
!�@q�!�@5hP �P3%�Psk1�P/"�P0�P5!P%! @1hP7 �@4$Pn`
#�P
1�P&#h 9@4�PC"bP"]@4PG0�P$�@4�P`	$� @ 	@1�P!� @2yP-`
`
`
12P)"RP`	`
3�P$P0zP"`
 yP2P`	 
P'# @2IP)`	`
"? @10P_0�P�!� @4P00�PZ`	 KP 
P`	 	P#`PCr�`
 �P`	# @3lPA1�P 
P0�P`
$.P0%P4 P �P`	`
#�Pp2�P`	`
#�@3�P"�P3�P&$� c@5�P%� F@3P`
`
"YP`	`
! @3�PB`
$: ;@5/P$� @3tP1P'`	#kP5!@q`
2?P	08P%1oP �P`	!P`	`
 �P/0�P2�P	#aP1�P$= 8@ 	 @5�P`	#�PB3�P`
`
#;P UP?`
2fPJ`	0oP	!�P)`
`
# '@u�%� @t�#lP!% B@4�P@0�P%� @2�PY`
1�P`	$c @1�P �P^!7 8@4FP"HP#" @ 	 
@1AP`
`4�P`
%� 1@ 	 @rQ!P!> @   ��         �� 	   	  P�  �� 
   
   )@�  �� 	   	  P*�  �� 	   	  P�  �� 
   
   6@�  �� 	   	  P�  �� 	   	  P3�  �� 	   	  P2�  �� 
   
   N@�  �� 
   
   N@�  �� 
   
   Q@�  �� 	   	  P.�  �� 	   	  P�  �� 
   
   �@k  �� 
   
   �@j  �� 	   	  P�A  �� 
   
   �@F  �� 	   	  P�6  �� 
   
   �@)  �� 	   	  P�)  �� 	   	  QA�  �� 	   	  Q(�  �� 	   	  QG�  �� 	   	  QJ�  �� 	   	  QK�  �� 	   	  P�  �� 
   
   �@  �� 	   	  Q"�  �� 
   
   @�  �� 	   	  Q"�  �� 	   	  QF�  �� 
   
  e@�  �� 	   	  Q�  �� 	   	  Q�  �� 
   
  �@i  �� 	   	  Q�G  �� 	   	  Q�H  �� 
   
  n@�  �� 
   
  r@�  �� 	   	  Q�C  �� 	   	  R�  �� 	   	  Q�  �� 	   	  Q�  �� 	   	  R8�  �� 
   
  6@�  �� 
   
  ?@�  �� 	   	  R�E  �� 	   	  R�:  �� 	   	  R�U  �� 	   	  R�T  �� 	   	  R�@  �� 	   	  R�C  �� 
   
  �@B  �� 	   	  S8�  �� 
   
  @�  �� 	   	  S�  �� 	   	  R�  �� 	   	  Sp�  �� 
   
  �@b  �� 	   	  S�}  �� 
   
  �@}  ��       @{@}  �� 	   	  S�a  �� 
   
  �@y  �� 	   	  S�z  �� 	   	  T%�  �� 
   
  PkS��  �� 
   
  �@�  �� 	   	  S�  �� 	   	  S�  �� 	   	  S�)  �� 	   	  T�  �� 
   
  @  �� 	   	  Tb�  �� 	   	  T�`  �� 
   
  �@`  �� 	   	  TI�  �� 	   	  T�I  �� 
   
  �@H  �� 	   	  T�H  �� 
   
  a@�  �� 
   
  �@\  �� 
   
  �@\  �� 	   	  T�]  �� 	   	  U/�  �� 
   
  �@&  �� 	   	  U�  �� 
   
  �@?  �� 
   
  �@>  �� 
   
  �@?  �� 	   	  T�@  �� 	   	  U�q  �� 	   	  U\�  �� 	   	  U�\  �� 
   
  �@[  �� 	   	  U�\  �� 
   
  T@�  �� 	   	  U�q  �� 	   	  UP�  �� 
   
  �@y  �� 	   	  U�y  �� 
   
  �@<  �� 
   
  �@;  �� 
   
  �@:  �� 	   	  U�  �� 	   	  U�Q  �� 	   	  V�  �� 
   
  	@�  �� 	   	  V�  �� 	   	  VU�  �� 	   	  V}�  �� 	   	  Vy�  �� 	   	  Vx�  �� 	   	  V�s  �� 
   
  �@  �� 	   	  V�  �� 	   	  W �  �� 	   	  W �  �� 	   	  V�O  �� 
   
  @W` �  �� 	   	  W� �  �� 	   	  WX �  �� 
   
  W@ �  �� 
   
  V�P� �  �� 	   	  W �  �� 	   	   G�  �� 
   
  �@   �� 	   	  W� [  �� 	   	  W� X  ��      @�@ V  �� 	   	  W� d  �� 	   	  W� 1  �� 	   	  W� M  �� 
   
  �@ L  �� 	   	  W� M  �� 
   
  �@ '  �� 
   
  �@ '  �� 	   	  W� (  �� 	   	  W� O  �� 	   	  W�   �� 
   
  �@ a  ��      V� �@ a  �� 
   
  �@ a  �� 	   	  W�   �� 
   
  @�@  �� 	   	  @�  �� 
   
  �@:  �� 
   
  �@:  �� 
   
  �@'  �� 	   	  S�  �� 
   
  @�  �� 	   	  QH�  �� 	   	  QL�  �� 	   	  QM�  �� 	   	  WM �  �� 	   	  R�&  �� 	   	  Q�X  �� 	   	  Q�8  �� 	   	  Uy�  �� 
   
   @�  �� 	   	  P�  �� 	   	  VI�  �� 	   	  W�   �� 
   
  �@B  �� 
   
  �@ &  �� 	   	  R�  �� 	   	  P�  �� 
   
   ,@�  �� 
   
  ?@�  �� 	   	  T�"  �� 	   	  T�  �� 	   	  S�!  �� 
   
  '@�  �� 	   	  U�  �� 	   	  Q�  �� 	   	  W� %  �� 
   
  �@r  �� 	   	  Rz�  �� 
   
  �@'  �� 
   
  �@ j