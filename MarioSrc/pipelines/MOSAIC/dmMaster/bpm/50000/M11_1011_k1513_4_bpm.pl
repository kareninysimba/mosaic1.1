  ŚV  +Ó  %s$TITLE = "DFLATS Observation(s)"
$CTIME = 976449188
$MTIME = 976449188
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
RAWFILE = 'n501453.fits'       / Original raw file
FILENAME= 'K4M10B_20101027-IFlat-kp4m20101027T235153F' / Current filename
NEXTEND =                    8 / Number of extensions
OBSTYPE = 'dome flat'          / Observation type
PROCTYPE= 'MasterCal'          / Processing type
PRODTYPE= 'image   '           / Product type
MIMETYPE= 'application/fits'   / Mimetype of this data file
EXPTIME =                   55 / Exposure time (sec)
OBJRA   = '18:50:24.22'        / Right Ascension
OBJDEC  = '-17:00:00.0'        / Declination
OBJEPOCH=               2010.8 / [yr] epoch

          # Single exposure quantities are representative from 1st exposure
TIMESYS = 'UTC     '           / Time system
DATE-OBS= '2010-10-27T23:51:53.0' / Date and time of exposure start
TIME-OBS= '23:51:53'           / Universal time
MJD-OBS =       55496.99436343 / MJD of observation start
ST      = '18:50:24'           / Sidereal time

OBSERVAT= 'KPNO    '           / Observatory
OBS-ELEV=                2120. / [km] Observatory elevation
OBS-LAT = '31:57.8 '           / [deg] Observatory latitude
OBS-LONG= '111:36.0'           / [deg] Observatory longitude
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2010.8 / Equinox of tel coords
TELRA   = '18:50:24.22'        / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
TELFOCUS=                -8873 / Telescope focus
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off     '           / ADC Mode
ADCPAN1 =                 0.01 / [deg] MSE ADC 1 prism angle
ADCPAN2 =                 0.05 / [deg] MSE ADC 2 prism angle
CORRCTOR= 'Mayall Corrector'   / Corrector Identification

INSTRUME= 'mosaic_1_1'         / Mosaic detector
NDETS   =                    8 / Number of detectors in mosaic
FILTER  = 'I Nearly-Mould k1005' / Filter name
FILTID  = 'k1005   '           / Unique filter identification
PHOTBW  =              191.459 / [nm] Filter wavelength FWHM
PHOTFWHM=              191.459 / [nm] Filter wavelength FWHM
PHOTCLAM=              820.453 / [nm] Filter wavelength width
FILTPOS =                    5 / Instrument filter position
FILTERSN= 'k1005   '           / Filter serial number
NOCGAIN = 'normal  '           / NOCS gain setting
SHUTSTAT= 'dark    '           / Shutter status
ENVTEM  =                   13 / [celsius] MSE temp7 - Ambient
DEWAR   = 'Mosaic1.1 Dewar'    / Dewar identification
DEWTEM  =               -178.5 / [celsius] MSE temp1 - Dewar tank
DEWTEM2 =                 -9.7 / [celsius] MSE temp3 - Fill Neck
CCDTEM  =          -106.199997 / [celsius] MSE temp2 - CCD Focal Plate

OBSERVER= 'Howell, Schweiker, Mathis, Reedy' / Observer(s)
PROPOSER= 'David Sawyer'       / Proposer(s)
PROPID  = '2010B-2005'         / Proposal identification
OBSID   = 'kp4m.20101027T235153' / Observation ID
EXPID   =                    0 / Monsoon exposure ID
NOCID   =        2455497.70176 / NOCS exposure ID

CONTROLR= 'Mosaic System Electronics  Sep 2' / MSE name and revision date
DHEFILE = 'mosaic1_Seq2amp250Kpx.ucd' / Sequencer file

NOHS    = '2.0.0   '           / NOHS ID
NOCMDOF =                    0 / [arcsec] Map Dec offset
NOCMITER=                    0 / Map iteration count
NOCNO   =                   31 / observation number in this sequence
NOCPOST = 'dfs     '           / ntcs_moveto ra dec epoch
NOCDROF =                    0 / [arcsec] Dither RA offset
NOCDHS  = 'DFLATS  '           / DHS script name
NOCORA  =                    0 / [arcsec] RA offset
NOCODEC =                    0 / [arcsec] Dec offset
NOCMPOS =                    0 / Map position
NOCDITER=                    0 / Dither iteration count
NOCMPAT = '4Q      '           / Map pattern
NOCMREP =                    0 / Map repetition count
NOCDDOF =                    0 / [arcsec] Dither Dec offset
NOCTOT  =                   50 / Total number of observations in set
NOCSCR  = 'DFLATS  '           / NOHS script run
NOCFOCUS=                    0 / [um] nics_focus value
NOHS    = 'Mosaic.1.1'         / NOHS ID
NOCTIM  =                   55 / [s] Requested integration time
NOCOFFT = '0 0     '           / ntcs_offset RA Dec offset (arcsec)
NOCSYS  = 'kpno 4m '           / system ID
NOCNUM  =                   10 / observation number request
NOCLAMP = 'On      '           / Dome flat lamp status (on|off|unknown)
NOCRBIN =                    1 / CCD row binning
NOCMROF =                    0 / [arcsec] Map RA offset
NOCSKY  =                    0 / sky offset modulus
NOCNPOS =                    1 / observation number in requested number
NOCCBIN =                    1 / CCD column binning
NOCTYP  = 'DFLATS  '           / Observation type
NOCDPOS =                    0 / Dither position
NOCDPAT = '5PX     '           / Dither pattern
NOCDREP =                    0 / Dither repetition count

RAINDEX =                    0 / [arcsec] RA index
RAZERO  =                570.5 / [arcsec] RA zero
ALT     = '41:02:15.0'         / Telescope altitude
DECINST =                    0 / [arcsec] Dec instrument center
DECDIFF =                    0 / [arcsec] Dec diff
FOCUS   =                -8873 / [mm] Telescope focus
PARALL  =                  360 / [deg] parallactic angle
DECZERO =              -375.12 / [arcsec] Dec zero
AZ      = '180:00:00.0'        / Telescope azimuth
RADIFF  =                    0 / [arcsec] RA diff
RAINST  =                    0 / [arcsec] RA instrument center
DECOFF  =                    0 / [arcsec] Dec offset
DECINDEX=                    0 / [arcsec] Dec index
RAOFF   =                    0 / [arcsec] RA offset

DOMEERR =               135.85 / [deg] Dome error as distance from target
DOMEAZ  =                    0 / [deg] Dome position

MSEREADY= 'dark    '           / MSE shutter ready (none|guide|dark|restore)

TCPGDR  = 'off     '           / Guider status (on|off|lock)

DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1_1        '  /  instrument identifier
DTCALDAT= '2010-10-27        '  /  calendar date from observing schedule
ODATEOBS= '                  '  /  previous DATE-OBS
DTUTC   = '2010-10-27T23:57:30'  /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2010B-2005        '  /  observing proposal ID
DTPI    = 'David Sawyer      '  /  Principal Investigator
DTPIAFFL= 'National Optical Astronomy Observatory'  /  PI affiliation
DTTITLE = 'On-sky Commissioning of MOSA 1.1'  /  title of observing proposal
DTCOPYRI= 'AURA              '  /  copyright holder of data
DTACQUIS= 'mosaic1dhs-01-4m.kpno.noao.edu'  /  host name of data acquisition com
DTACCOUN= 'cache             '  /  observing account name
DTACQNAM= '/home/data/n501453.fits'  /  file name supplied at telescope
DTNSANAM= 'kp1247271.fits    '  /  file name in NOAO Science Archive
DTSTATUS= 'done              '  /  data transport status
SB_HOST = 'mosaic1dhs-01-4m.kpno.noao.edu'  /  iSTB client host
SB_ACCOU= 'cache             '  /  iSTB client user account
SB_SITE = 'kp                '  /  iSTB host site
SB_LOCAL= 'kp                '  /  locale of iSTB daemon
SB_DIR1 = '20101027          '  /  level 1 directory in NSA DS
SB_DIR2 = 'kp4m              '  /  level 2 directory in NSA DS
SB_DIR3 = '2010B-2005        '  /  level 3 directory in NSA DS
SB_RECNO=              1247271  /  iSTB sequence number
SB_ID   = 'kp1247271         '  /  unique iSTB identifier
SB_NAME = 'kp1247271.fits    '  /  name assigned by iSTB
RMCOUNT =                    0  /  remediation counter
RECNO   =              1247271  /  NOAO Science Archive sequence number
XTALKFIL= 'Mosaic11_xtalkdummy.txt' /

GAIN    =                  1.1 / Approx inv gain (ADU/e)
RDNOISE =                  5.0 / Approx readout noise (e)
SATURATE=               220000 / Approx saturation (ADU)

DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.001509 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         3.103876E-42 / depth of longest jump down
CCDMEAN =            149304.45
OVSCNMTD=                    3
DQGLFSAT=                   0.
IMCMB001= 'kp1247271.fits'
IMCMB002= 'kp1247272.fits'
IMCMB003= 'kp1247274.fits'
IMCMB004= 'kp1247275.fits'
IMCMB005= 'kp1247277.fits'
IMCMB006= 'kp1247278.fits'
IMCMB007= 'kp1247281.fits'
IMCMB008= 'kp1247285.fits'
IMCMB009= 'kp1247286.fits'
IMCMB010= 'kp1247287.fits'
NCOMBINE=                   10
QUALITY =                   0.
GAINMEAN=                  1.1

PIPELINE= 'NOAO Mosaic Pipeline' / Name of calibration pipeline
PLVER   = 'MOSAIC V1.1'        / Pipeline version
EFFTIME =                   55 / [s] Effective exposure time
PLPROPID= 'CAL     '

PLQUEUE = 'K4M10B  '           / PL Queue
PLQNAME = '20101027'           / PL Dataset
PLPROCID= '7ae6e75 '           / PL Processing ID
PLFNAME = 'IFlat-kp4m20101027T235153F' / PL Filename
PLOFNAME= 'n501453_pl'         / Original file name
PCOUNT  =                    0 / No 'random' parameters
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'ccd4    '           / Extension name
INHERIT =                    T / Inherits global header
DATE    = '2010-11-02T18:19:21' / Date FITS file was generated
IRAF-TLM= '2010-11-03T17:47:13' / Time of last modification
IMAGEID =                    4 / Image identification

CCDNAME = 'SN10-02 '           / CCD name
AMPNAME = 'SN10-02:A'          / Amplifier name
BUNIT   = 'adu     '           / [adu] ADU counts
CCDSUM  = '1 1     '           / CCD pixel summing
DETSEC  = '[6145:8192,1:4096]' / Detector section


XTALKCOR= 'Nov  2 11:02 No crosstalk correction required'
BIASFIL = 'K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z[ccd4]' / Bias reference
DQOVMJLR=                   5. / length of deepest jump down
DQOVMJJR=             57.45077 / amplitude of deepest jump
DQOVJPRB=                   1. / probability of a jump
DQOVMIN =               332.44 / min in overscan region
DQOVMAX =             615.7551 / max in overscan region
DQOVMEAN=             605.6566 / mean in overscan region
DQOVSIG =             22.07127 / sigma in overscan region
DQOVSMEA=             605.6375 / mean in collapsed overscan strip
DQOVSSIG=             2.663475 / sigma in collapsed overscan strip
SATPROC = 'Nov  2 11:08 Sat: 220000. ADU (242000. e-), grw=0'
TRIM    = 'Nov  2 11:08 Trim is [1:1024,1:4096]'
FIXPIX  = 'Nov  2 11:08 Fix Mosaic11_dummy_4_bpm.pl + sat' /
OVERSCAN= 'Nov  2 11:08 Overscan is [1025:1074,1:4096], function=minmax'
ZEROCOR = 'Nov  2 11:08 Zero is K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z' /
CCDPROC = 'Nov  2 11:09 CCD processing done'
AMPMERGE= 'Nov  2 11:09 Merged 2 amps'
DQGLMEAN=             145943.5
DQGLSIG =              4811.06
DQDFCRAT=             2653.518
DCCDMEAN=            0.9675512
CCDMNTMP=             149304.5
    Ţ                          !¸  !¸       %s      ˙˙˙               ˙       F5`	`M`9`4`$`&`'````````````````````````````````````````````````````````````````````````````````````````````````````````````````````` @`
 @`
` 
@`
 
@ 
@!`
 @! 
@`
 @`
 
@
`
 
@	 
@`
 @`
 @ 
@`
 
@`
 
@`
 @ 
@`
`
``
`
`
` @`
 @`
 @`
` @ 
@`
 
@`
 @`
`
``
`
 @`
 
@`
`
 @	`
 @`
 @`
 @`
 @`
 
@`
``
`` 
@`
 @	`
``
`
`
 @`
``
``
 @ 
@`
`
`
 @`
 
@`
` @`
`
`
``
 
@ 
@`
 @`
``
` @`
`
 @`
`
```
` 
@`
``` 
@`
``
 @`
``
``
``
``
 @`
`
`
` 
@`
``
`````
 @`
`
`
``
 @`
``
 @`
``
``
`
``
````
`
`
 @`
```
````
``
` 
@`
```
``
````
`````
`
``
 @`
``
``
`
 @``
```
`` @`
``
``
`
 @```
```
````
````
`
``
`
```
``
``
`
` @`
`````
``
``
`
`
``````
 
@ 
@`
``
`````
``` @`
```
 @`
```````
``
``
````
`` @`
``
 @`
``` @`
```````
``
```
``
` 
@```
```
``
``
```
``
``
`
` @`
``
```` @`
````
` 
@`
``
````
`
 @`
` @`
` @`
``
``
`
 @`
```
``
``
``
```````
 
@`
`
` @`
`
````
`
`
` 
@ 
@`
 @`
``
``` @`
 @ 
@`
`
``
` @`
 
@`
``
 @`
 @`
 @ 
@`
 @ 
@ 
@`
 @`````````````````` @ @ @ @	`
` @`
 
@`
``
 @`
 
@ 
@	`
 @`
 @`
 
@`
`
 @`
 @`
`
`
 @`
` @`
` 
@`
 @`
 @`
 @ 
@ 
@ @	`
` 
@`
 @`
 @`
 @	`
 @`
 @`
 @`
 @`
 
@`
 
@ 
@`
 @`
 
@ 
@`
 
@`
 
@ 
@! 
@ `
 @" 
@! 
@! 
@" 
@! 
@`
 @ 
@" 
@! 
@! 
@" 
@! 
@! 
@" 
@! 
@! 
@" 
@! 
@! 
@" 
@! 
@! 
@`
` @ 
@! 
@! 
@"`
 
@ @ 
@! 
@" 
@! 
@`
 @ 
@" 
@   ˙       5 ˙ 	   	   H   ˙ M   M   PËPPPP	P!PPPP @PPPP @P @PPPPPPPP @PP @PPPPP @P @P @P @PP @ @ @ @ @ @
 @ F  ˙ 9   9   PîP"PPP	PPPPPPPPPP @PPPPPPPPP @ @ @PPPP @P @ @PP @ F  ˙ 4   4   PÎPPPP4PP	P
PPPPPPPP
 @PP @ @PPP @P 	@ @PPPPPP @ F  ˙ $   $   Q.P/ @PPPPPPPPPPP @PPPP @P @ F  ˙ &   &   Q*PPPPPPP	 @PPPPPPPPPP @PPPP @ F  ˙ '   '   QHPPPP @PPPPPPPPPPPPP @P @PP @ F  ˙       QPPP P	PPPP @ @PP @ @ @ F  ˙       QP:P
 @ @ @P F  ˙       QPP, @P @ @ @P @ F  ˙       Q2PP"P_PPP @ F  ˙       QŐPPPPPP @ @ E˙  ˙       QŢPPP @P @PP F   ˙       QÓP @ @P @ @ F  ˙       QÔPPPPPPP @P @ Eý  ˙       Q o@ @ @ Eý  ˙       QďP	 @ Eř  ˙       QÜPPP @ @P @ Eů  ˙       QňP @ @ Eú  ˙       QňP @P @PP Eř  ˙       QűP @ @ Eó  ˙       R Eü  ˙       QüPP @ @ Eö  ˙       Q˙ @ @P Eň  ˙       Q÷P @ @ Eî  ˙       R @ @ Eě  ˙       @ @PPPP Eę  ˙       @ @PP @ @ Eä  ˙       RPPP @ Eĺ  ˙       RPP @ Eí  ˙       R
P @ @ Eç  ˙       R @ @ Eč  ˙       RPPPPP Eâ  ˙       RP @
 @ EÝ  ˙       RP @P @ Eß  ˙       RPP @ EŮ  ˙       RP @ @ @ E×  ˙       R @ Eá  ˙       @P @ @ EÖ  ˙       RPPP EŘ  ˙       R @ @ EÝ  ˙       R @P @ @ @ EÎ  ˙       RPP @ E×  ˙       RP @ @ @ EË  ˙       R%P @ EŐ  ˙       R!PP @ @ @ EÎ  ˙       R!PP @ @ EË  ˙       R& @ @ EČ  ˙       !@PP @ @P EË  ˙       R& @ EŇ  ˙       R+ @ EŃ  ˙       R%PPPP EÎ  ˙       R'P EĚ  ˙       '@ @ @PP EĆ  ˙       R&P @P EË  ˙       R.P @ EĆ  ˙       3@ EČ  ˙       R) @P @P @ EŔ  ˙       R' 
@P @ EŔ  ˙       R3 @ EÂ  ˙       R*PP
PP EĂ  ˙       6@ @ EÄ  ˙       :@P @ E¸  ˙       R5PP Eż  ˙       R3P @ @P Eź  ˙       R>P Eż  ˙       R7P @ Eş  ˙       R7PP @ @P Eş  ˙       R? @ Eź  ˙       R<P @ @ E´  ˙       R8PPPP Eľ  ˙       RA @ Eś  ˙       RCP @ @ Eą  ˙       R=PP @P E˛  ˙       RC @ @ EŻ  ˙       RFPP Eł  ˙       R<PP @ E˛  ˙       RBP	 @ Eą  ˙       REP @ E­  ˙       RGP @ EŹ  ˙       RCPP E­  ˙       RL @	 E¨  ˙       RG @P EŽ  ˙       RN @ @ E¨  ˙       RMP @ E¨  ˙       RNP @ EŞ  ˙       RM @ EŞ  ˙       RK @PP EŚ  ˙       RL @ @ E¨  ˙       RM @ @ EĄ  ˙       P@PP @ EĽ  ˙       RQPP @P E   ˙       RNPP @ EŁ  ˙       N@P @P EŁ  ˙       RN @P @ E  ˙       P@PPP @PP E   ˙       R[ @ E   ˙       RS @ @	 @ E  ˙       RPP
PP EĄ  ˙       R^P @P E  ˙       RZPP E  ˙       _@P @ E  ˙       ]@ @ E  ˙       R[P @ E  ˙       a@ @P E  ˙       ]@ @P E  ˙       R\ @ E  ˙       [@PP @ E  ˙       c@P E  ˙       RY @ @ E  ˙       Ra @ E  ˙       R_PPP E  ˙       RdP E  ˙       R[ E  ˙       RcPP E  ˙       g@ E  ˙       Ri E  ˙       RfP E  ˙       Rd @ E  ˙       R_ 
E  ˙       R_PP E  ˙       RfP E  ˙       Re E  ˙       g@ E  ˙       Rd E  ˙ 
   
   jE  ˙       Rd E  ˙ 
   
   jE  ˙      P0 R2 E  ˙ 
   
   jE  ˙ 
   
   kE  ˙ 
   
   jE  ˙ 
   
   kE ! ˙ 
   
   lE  ˙      P XE ! ˙ 
   
   mE  ˙ 
   
   nE  ˙       Rm E  ˙ 
   
   nE  ˙ 
   
   mE 
 ˙ 
   
   nE  ˙ 
   
   mE 	 ˙ 
   
   nE  ˙ 
   
   oE  ˙       Rn E  ˙ 
   
   oE  ˙       Rn E  ˙ 
   
   oE  ˙ 
   
   pE  ˙ 
   
   oE  ˙ 
   
   pE  ˙ 
   
   oE  ˙ 
   
   pE  ˙       Rn E  ˙ 
   
   pE  ˙ 
   
   qE  ˙ 
   
   pE  ˙       Ro E  ˙ 
   
   qE  ˙ 
   
   pE  ˙ 
   
   qE  ˙       Rn E  ˙       Ro E  ˙ 
   
   qE  ˙       RlP E  ˙ 
   
   qE  ˙       Rl E  ˙ 
   
   qE  ˙      PÝ E  ˙       Ü@ E  ˙ 
   
   qE  ˙ 
   
   rE  ˙ 
   
   pE  ˙ 
   
   rE  ˙       Rf E  ˙ 
   
   rE  ˙ 
   
   qE  ˙       Rn E  ˙ 
   
   rE  ˙ 
   
   sE  ˙       Rn E  ˙ 
   
   sE  ˙ 
   
   rE  ˙ 
   
   sE  ˙ 
   
   rE  ˙       Rn E 	 ˙ 
   
   sE  ˙       Ro E  ˙ 
   
   sE  ˙       Ro E  ˙ 
   
   tE  ˙       Rp E  ˙ 
   
   tE  ˙       Rp E  ˙ 
   
   tE  ˙ 
   
   sE  ˙ 
   
   tE  ˙      &@  LE  ˙ 
   
   tE  ˙       RoP E  ˙       Rm E  ˙ 
   
   tE  ˙ 
   
   uE  ˙       Rt E 	 ˙ 
   
   uE  ˙       Rr E  ˙ 
   
   uE  ˙ 
   
   tE  ˙ 
   
   uE  ˙       Ro E  ˙ 
   
   uE  ˙       Rk 
E  ˙ 
   
   uE  ˙       Rr E  ˙ 
   
   uE  ˙       Rn E  ˙ 
   
   uE  ˙ 
   
   vE  ˙ 
   
   uE  ˙ 
   
   vE  ˙       Rt E  ˙ 
   
   vE  ˙ 
   
   uE  ˙ 
   
   vE  ˙       Ru E  ˙       Rr E  ˙ 
   
   vE  ˙ 
   
   uE  ˙ 
   
   vE  ˙       Ru E  ˙ 
   
   vE  ˙ 
   
   uE  ˙ 
   
   vE  ˙ 
   
   wE  ˙       Rj E  ˙ 
   
   wE  ˙       Rs E  ˙ 
   
   wE  ˙       Rv E  ˙       Rs E  ˙ 
   
   wE  ˙ 
   
   vE  ˙       Rp E  ˙ 
   
   wE  ˙ 
   
   vE  ˙      Q+ LE  ˙      Q+ QK E  ˙ 
   
   wE  ˙       Rt E  ˙ 
   
   wE  ˙ 
   
   xE  ˙       Ro 	E  ˙       Rq E  ˙       Rw E  ˙ 
   
   wE  ˙ 
   
   xE  ˙       Rv E  ˙ 
   
   xE  ˙       Rw E  ˙ 
   
   xE  ˙       RtP E  ˙ 
   
   xE  ˙       Rw E  ˙ 
   
   xE  ˙       Rw E  ˙ 
   
   xE  ˙       Rv E  ˙ 
   
   wE  ˙       Ru E  ˙ 
   
   xE  ˙ 
   
   wE  ˙ 
   
   xE  ˙       Rr E  ˙ 
   
   wE  ˙ 
   
   yE  ˙       Rw E  ˙ 
   
   yE  ˙       Rr E  ˙       Rv E  ˙       Rq E  ˙       Rw E  ˙ 
   
   yE  ˙       Rw E  ˙ 
   
   yE  ˙ 
   
   xE  ˙ 
   
   yE  ˙       Rw E  ˙ 
   
   yE  ˙       Rx E  ˙ 
   
   yE  ˙       Rp 	E  ˙ 
   
   yE  ˙       Rx E  ˙ 
   
   yE  ˙       Rp 	E  ˙ 
   
   yE  ˙       Ro 	E  ˙ 
   
   xE  ˙ 
   
   yE  ˙       RqP E  ˙ 
   
   zE  ˙       Rx E  ˙       Rx E  ˙       RuP E  ˙ 
   
   zE  ˙ 
   
   yE  ˙ 
   
   zE  ˙       Rn E  ˙ 
   
   zE  ˙       Ru E  ˙       Rs E  ˙ 
   
   zE  ˙       Rv E  ˙       Rv E  ˙       Ru E  ˙ 
   
   zE  ˙       Rr E  ˙ 
   
   zE  ˙       Rx E  ˙ 
   
   zE  ˙ 
   
   yE  ˙       RvP E  ˙       RvP E  ˙ 
   
   zE  ˙       Rw E  ˙ 
   
   zE  ˙       RqP E  ˙       Rw E  ˙       Rv E  ˙ 
   
   zE  ˙       RsP E  ˙       Rv E  ˙       w@ E  ˙       Rx E  ˙ 
   
   yE  ˙ 
   
   {E  ˙       Ry E  ˙ 
   
   {E  ˙       Rw E  ˙ 
   
   {E  ˙       Rz E  ˙ 
   
   {E  ˙       w@ E  ˙ 
   
   zE  ˙ 
   
   yE  ˙       Ry E  ˙       Rz E  ˙ 
   
   {E  ˙       x@ E  ˙       Rv E  ˙ 
   
   {E  ˙       RsP E  ˙       Rz E  ˙       Ry E  ˙ 
   
   {E  ˙       Rw E  ˙ 
   
   {E  ˙       RuP E  ˙ 
   
   {E  ˙ 
   
   zE  ˙       Ry E  ˙       Rz E  ˙       RwP E  ˙ 
   
   |E  ˙       Ro E  ˙       Rz E  ˙ 
   
   {E  ˙       Rw @ E  ˙       Rz E  ˙       Rx E  ˙ 
   
   |E  ˙       R{ E  ˙       RrP @ E  ˙       R{ E  ˙ 
   
   {E  ˙ 
   
   |E  ˙       RsPP E  ˙ 
   
   |E  ˙ 
   
   {E  ˙       R{ E  ˙       Rz E  ˙ 
   
   zE  ˙       w@ E  ˙ 
   
   |E  ˙       RsP E  ˙ 
   
   {E  ˙ 
   
   |E  ˙       Rs 	E  ˙       v@ E  ˙ 
   
   {E  ˙      R Pe E  ˙      @  b@ E  ˙      R Pf E  ˙       Rz E  ˙ 
   
   |E  ˙       Rz E  ˙ 
   
   }E  ˙       RxP E  ˙ 
   
   }E  ˙ 
   
   |E  ˙ 
   
   }E  ˙       Rv E  ˙       t@ E  ˙       Rz E  ˙      Q Qg E  ˙       Rz E  ˙ 
   
   }E  ˙ 
   
   |E  ˙ 
   
   }E  ˙ 
   
   |E  ˙       R| E  ˙ 
   
   }E  ˙       Rz E  ˙       Ry E  ˙       RzP E  ˙       Rz E  ˙ 
   
   }E  ˙       R| E  ˙       RwP E  ˙       z@ E  ˙       R{ E  ˙ 
   
   }E  ˙       RrP E  ˙       RxP E  ˙ 
   
   |E  ˙       R| E  ˙ 
   
   ~E  ˙       y@P E  ˙       RsP	 E  ˙       Rz E  ˙       RxP E  ˙       R} E  ˙       R| E  ˙ 
   
   ~E  ˙       R| E  ˙ 
   
   ~E  ˙       y@ E  ˙ 
   
   ~E  ˙       RyP E  ˙       R} E  ˙       Rt 
E  ˙ 
   
   {E  ˙       R| E  ˙       RvP E  ˙       Rx E  ˙ 
   
   ~E  ˙       R} E  ˙ 
   
   ~E  ˙       RzP E  ˙ 
   
   }E  ˙       RwP E  ˙       RzP E  ˙       RwP E  ˙       |@ E  ˙ 
   
   E  ˙      P` R E  ˙       RxP E  ˙       R| E  ˙       R| E  ˙       R|P E  ˙       w@P E  ˙ 
   
   ~E  ˙       Rw E  ˙ 
   
   E  ˙       RqP
 E  ˙       R{ E  ˙ 
   
   E  ˙       Rz E  ˙ 
   
   ~E  ˙       R} E  ˙ 
   
   E  ˙      P¸ ÇE  ˙       RwP E  ˙ 
   
   E  ˙       R} E  ˙       R} E  ˙ 
   
   E  ˙       R} E  ˙ 
   
   E  ˙       R| E  ˙ 
   
   E  ˙       R| E  ˙      Rw  E  ˙ 
   
   E  ˙       R{P E  ˙ 
   
   E  ˙       {@ E  ˙ 
   
   E  ˙ 
   
   ~E  ˙       Rz E  ˙       R E  ˙ 
   
   E  ˙       R E  ˙ 
   
   E  ˙       RqP @ E  ˙       R~ E  ˙       R} E  ˙       Ő@ ŠE  ˙       R~ E  ˙ 
   
   E  ˙       R| E  ˙       Rv 	E  ˙       R}P E  ˙ 
   
   E  ˙       R| E  ˙ 
   
   E  ˙ 
   
   E  ˙       R~ E  ˙ 
   
   E  ˙       R~ E  ˙       Rz E  ˙       Rv 
E  ˙ 
   
   E  ˙ 
   
   E  ˙       R~P E  ˙ 
   
   E  ˙       R} E  ˙       Rz E  ˙ 
   
   E  ˙       R~P E  ˙       R{ E  ˙ 
   
   E  ˙       R~ E  ˙ 
   
   E  ˙       Ry E  ˙ 
   
   E  ˙ 
   
   E  ˙       R E  ˙ 
   
   E  ˙       Ry E  ˙       R~ E  ˙ 
   
   E  ˙       R E  ˙ 
   
   E  ˙       R E  ˙ 
   
   E~  ˙       R~ E  ˙ 
   
   E~  ˙      P˙ Q E~  ˙       ý@ Q E~  ˙      Pţ E~  ˙       ü@ E~  ˙      Pý E~  ˙       R E~  ˙ 
   
   E~  ˙ 
   
   E  ˙ 
   
   E~  ˙ 
   
   E  ˙       R E  ˙       R~ E~  ˙ 
   
   E~  ˙ 
   
   E  ˙      @ E~  ˙       R} E~  ˙       R{ E~  ˙ 
   
   E~  ˙ 
   
   E  ˙ 
   
   E~  ˙       R E~  ˙ 
   
   E  ˙ 
   
   E~  ˙ 
   
   E}  ˙       R E}  ˙ 
   
   E}  ˙       R E}  ˙ 
   
   E}  ˙      RX  +E}  ˙      W@  *E}  ˙      RY P( E}  ˙      RZ  )E}  ˙ 
   
   E}  ˙       R E}  ˙ 
   
   E}  ˙ 
   
   E|  ˙ 
   
   E}  ˙       RxP E|  ˙ 
   
   E|  ˙       R} E~  ˙       @ E|  ˙ 
   
   E|  ˙ 
   
   E}  ˙ 
   
   E|  ˙       R E|  ˙ 
   
   E|  ˙       R E|  ˙ 
   
   E|  ˙       R E|  ˙ 
   
   E|  ˙       R E|  ˙ 
   
   E|  ˙ 
   
   E{  ˙       R E{  ˙ 
   
   E|  ˙ 
   
   E{  ˙ 
   
   Ez  ˙      Q Ez  ˙      @ Ez  ˙      @ Ez  ˙      @ E{  ˙      @ Ez  ˙      @ Ez  ˙      ~@ Ez  ˙      }@ Ez  ˙      |@ Ez  ˙      |@ Ez  ˙      {@ 	Ez  ˙      z@ 
Ez  ˙      y@ Ez  ˙      x@ Ez  ˙      w@ Ez  ˙      u@ Ez  ˙      t@ Ez  ˙      s@ Ez  ˙      s@ Ez  ˙      q@ Ez  ˙      p@ Ez  ˙      Qq Ez  ˙      Qq Ey 	 ˙ 
   
   Ey  ˙       R Ey  ˙      PČ żEy  ˙ 
   
   Ey  ˙ 
   
   Ez  ˙ 
   
   Ey  ˙       R Ey  ˙ 
   
   Ey  ˙      QĐ Pľ Ez  ˙ 
   
   Ex  ˙ 
   
   Ey  ˙ 
   
   Ex 	 ˙ 
   
   Ew  ˙       R Ew  ˙ 
   
   Ew  ˙       R Ew  ˙ 
   
   Ew  ˙ 
   
   Ex  ˙ 
   
   Ew  ˙ 
   
   Ev  ˙      PŹ ŢEv  ˙ 
   
   Ev  ˙       R Ev  ˙ 
   
   Ev  ˙ 
   
   Ew  ˙ 
   
   Ev  ˙       R Eu  ˙ 
   
   Eu  ˙      @  lEu  ˙      @  lEu  ˙ 
   
   Eu  ˙       R Eu  ˙ 
   
   Ev  ˙ 
   
   Eu  ˙       R Eu  ˙ 
   
   Eu  ˙       R Ev  ˙ 
   
   Eu  ˙       R Eu  ˙ 
   
   Eu  ˙ 
   
   Et  ˙       R Et 	 ˙ 
   
   Et  ˙       R Et  ˙ 
   
   Eu  ˙ 
   
   Et  ˙       R Et  ˙ 
   
   Et  ˙       R Et  ˙ 
   
   Et  ˙      PA KEt 	 ˙ 
   
   Es  ˙       R Es  ˙ 
   
   Es  ˙       R Es  ˙ 
   
   Es  ˙      Pa ,Es  ˙ 
   
   Es  ˙       R Es  ˙ 
   
   Es  ˙ 
   
   Et  ˙ 
   
   Er  ˙ 
   
   Es  ˙ 
   
   Er  ˙ 
   
   Eq  ˙       R Eq  ˙ 
   
   Eq  ˙ 
   
   Er  ˙ 
   
   Eq  ˙ 
   
   Ep  ˙ 
   
   Eq  ˙ 
   
   Ep  ˙ 
   
   Eq  ˙ 
   
   Ep ! ˙ 
   
   Eo   ˙ 
   
   En  ˙      PŞ čEn " ˙ 
   
   Em ! ˙ 
   
   El ! ˙ 
   
   Ek " ˙ 
   
   Ej ! ˙ 
   
   Ei  ˙ 
   
   Eh  ˙      R#  uEh  ˙ 
   
   Eh " ˙ 
   
   Eg ! ˙ 
   
   Ef ! ˙ 
   
   Ee " ˙ 
   
   Ed ! ˙ 
   
   Ec ! ˙ 
   
   Eb " ˙ 
   
   Ea ! ˙ 
   
    E` ! ˙ 
   
   ĄE_ " ˙ 
   
   ˘E^ ! ˙ 
   
   ŁE] ! ˙ 
   
   ¤E\ " ˙ 
   
   ĽE[ ! ˙ 
   
   ŚEZ ! ˙ 
   
   §EY  ˙ 
   
   ¨EX  ˙      @  EX  ˙      R  EX  ˙ 
   
   ¨EX ! ˙ 
   
   ŠEW ! ˙ 
   
   ŞEV " ˙ 
   
   ŤEU  ˙ 
   
   ŹET  ˙      Q ET  ˙ 
   
   ŹET ! ˙ 
   
   ­ES " ˙ 
   
   ŽER ! ˙ 
   
   ŻEQ  ˙ 
   
   °EP  ˙      Qž  ňEP  ˙ 
   
   °EP " ˙ 
   
   ąEO  ˙ 
   
   ˛EN