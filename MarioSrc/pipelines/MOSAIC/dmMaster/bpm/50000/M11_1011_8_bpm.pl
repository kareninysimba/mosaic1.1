  ¦V  +Ö  Ü$TITLE = "DFLATS Observation(s)"
$CTIME = 973252288
$MTIME = 973257136
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
RAWFILE = 'n501453.fits'       / Original raw file
FILENAME= 'K4M10B_20101027-IFlat-kp4m20101027T235153F' / Current filename
NEXTEND =                    8 / Number of extensions
OBSTYPE = 'dome flat'          / Observation type
PROCTYPE= 'MasterCal'          / Processing type
PRODTYPE= 'image   '           / Product type
MIMETYPE= 'application/fits'   / Mimetype of this data file
EXPTIME =                   55 / Exposure time (sec)
OBJRA   = '18:50:24.22'        / Right Ascension
OBJDEC  = '-17:00:00.0'        / Declination
OBJEPOCH=               2010.8 / [yr] epoch

          # Single exposure quantities are representative from 1st exposure
TIMESYS = 'UTC     '           / Time system
DATE-OBS= '2010-10-27T23:51:53.0' / Date and time of exposure start
TIME-OBS= '23:51:53'           / Universal time
MJD-OBS =       55496.99436343 / MJD of observation start
ST      = '18:50:24'           / Sidereal time

OBSERVAT= 'KPNO    '           / Observatory
OBS-ELEV=                2120. / [km] Observatory elevation
OBS-LAT = '31:57.8 '           / [deg] Observatory latitude
OBS-LONG= '111:36.0'           / [deg] Observatory longitude
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2010.8 / Equinox of tel coords
TELRA   = '18:50:24.22'        / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
TELFOCUS=                -8873 / Telescope focus
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off     '           / ADC Mode
ADCPAN1 =                 0.01 / [deg] MSE ADC 1 prism angle
ADCPAN2 =                 0.05 / [deg] MSE ADC 2 prism angle
CORRCTOR= 'Mayall Corrector'   / Corrector Identification

INSTRUME= 'mosaic_1_1'         / Mosaic detector
NDETS   =                    8 / Number of detectors in mosaic
FILTER  = 'I Nearly-Mould k1005' / Filter name
FILTID  = 'k1005   '           / Unique filter identification
PHOTBW  =              191.459 / [nm] Filter wavelength FWHM
PHOTFWHM=              191.459 / [nm] Filter wavelength FWHM
PHOTCLAM=              820.453 / [nm] Filter wavelength width
FILTPOS =                    5 / Instrument filter position
FILTERSN= 'k1005   '           / Filter serial number
NOCGAIN = 'normal  '           / NOCS gain setting
SHUTSTAT= 'dark    '           / Shutter status
ENVTEM  =                   13 / [celsius] MSE temp7 - Ambient
DEWAR   = 'Mosaic1.1 Dewar'    / Dewar identification
DEWTEM  =               -178.5 / [celsius] MSE temp1 - Dewar tank
DEWTEM2 =                 -9.7 / [celsius] MSE temp3 - Fill Neck
CCDTEM  =          -106.199997 / [celsius] MSE temp2 - CCD Focal Plate

OBSERVER= 'Howell, Schweiker, Mathis, Reedy' / Observer(s)
PROPOSER= 'David Sawyer'       / Proposer(s)
PROPID  = '2010B-2005'         / Proposal identification
OBSID   = 'kp4m.20101027T235153' / Observation ID
EXPID   =                    0 / Monsoon exposure ID
NOCID   =        2455497.70176 / NOCS exposure ID

CONTROLR= 'Mosaic System Electronics  Sep 2' / MSE name and revision date
DHEFILE = 'mosaic1_Seq2amp250Kpx.ucd' / Sequencer file

NOHS    = '2.0.0   '           / NOHS ID
NOCMDOF =                    0 / [arcsec] Map Dec offset
NOCMITER=                    0 / Map iteration count
NOCNO   =                   31 / observation number in this sequence
NOCPOST = 'dfs     '           / ntcs_moveto ra dec epoch
NOCDROF =                    0 / [arcsec] Dither RA offset
NOCDHS  = 'DFLATS  '           / DHS script name
NOCORA  =                    0 / [arcsec] RA offset
NOCODEC =                    0 / [arcsec] Dec offset
NOCMPOS =                    0 / Map position
NOCDITER=                    0 / Dither iteration count
NOCMPAT = '4Q      '           / Map pattern
NOCMREP =                    0 / Map repetition count
NOCDDOF =                    0 / [arcsec] Dither Dec offset
NOCTOT  =                   50 / Total number of observations in set
NOCSCR  = 'DFLATS  '           / NOHS script run
NOCFOCUS=                    0 / [um] nics_focus value
NOHS    = 'Mosaic.1.1'         / NOHS ID
NOCTIM  =                   55 / [s] Requested integration time
NOCOFFT = '0 0     '           / ntcs_offset RA Dec offset (arcsec)
NOCSYS  = 'kpno 4m '           / system ID
NOCNUM  =                   10 / observation number request
NOCLAMP = 'On      '           / Dome flat lamp status (on|off|unknown)
NOCRBIN =                    1 / CCD row binning
NOCMROF =                    0 / [arcsec] Map RA offset
NOCSKY  =                    0 / sky offset modulus
NOCNPOS =                    1 / observation number in requested number
NOCCBIN =                    1 / CCD column binning
NOCTYP  = 'DFLATS  '           / Observation type
NOCDPOS =                    0 / Dither position
NOCDPAT = '5PX     '           / Dither pattern
NOCDREP =                    0 / Dither repetition count

RAINDEX =                    0 / [arcsec] RA index
RAZERO  =                570.5 / [arcsec] RA zero
ALT     = '41:02:15.0'         / Telescope altitude
DECINST =                    0 / [arcsec] Dec instrument center
DECDIFF =                    0 / [arcsec] Dec diff
FOCUS   =                -8873 / [mm] Telescope focus
PARALL  =                  360 / [deg] parallactic angle
DECZERO =              -375.12 / [arcsec] Dec zero
AZ      = '180:00:00.0'        / Telescope azimuth
RADIFF  =                    0 / [arcsec] RA diff
RAINST  =                    0 / [arcsec] RA instrument center
DECOFF  =                    0 / [arcsec] Dec offset
DECINDEX=                    0 / [arcsec] Dec index
RAOFF   =                    0 / [arcsec] RA offset

DOMEERR =               135.85 / [deg] Dome error as distance from target
DOMEAZ  =                    0 / [deg] Dome position

MSEREADY= 'dark    '           / MSE shutter ready (none|guide|dark|restore)

TCPGDR  = 'off     '           / Guider status (on|off|lock)

DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1_1        '  /  instrument identifier
DTCALDAT= '2010-10-27        '  /  calendar date from observing schedule
ODATEOBS= '                  '  /  previous DATE-OBS
DTUTC   = '2010-10-27T23:57:30'  /  post exposure UTC epoch from DTS
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2010B-2005        '  /  observing proposal ID
DTPI    = 'David Sawyer      '  /  Principal Investigator
DTPIAFFL= 'National Optical Astronomy Observatory'  /  PI affiliation
DTTITLE = 'On-sky Commissioning of MOSA 1.1'  /  title of observing proposal
DTCOPYRI= 'AURA              '  /  copyright holder of data
DTACQUIS= 'mosaic1dhs-01-4m.kpno.noao.edu'  /  host name of data acquisition com
DTACCOUN= 'cache             '  /  observing account name
DTACQNAM= '/home/data/n501453.fits'  /  file name supplied at telescope
DTNSANAM= 'kp1247271.fits    '  /  file name in NOAO Science Archive
DTSTATUS= 'done              '  /  data transport status
SB_HOST = 'mosaic1dhs-01-4m.kpno.noao.edu'  /  iSTB client host
SB_ACCOU= 'cache             '  /  iSTB client user account
SB_SITE = 'kp                '  /  iSTB host site
SB_LOCAL= 'kp                '  /  locale of iSTB daemon
SB_DIR1 = '20101027          '  /  level 1 directory in NSA DS
SB_DIR2 = 'kp4m              '  /  level 2 directory in NSA DS
SB_DIR3 = '2010B-2005        '  /  level 3 directory in NSA DS
SB_RECNO=              1247271  /  iSTB sequence number
SB_ID   = 'kp1247271         '  /  unique iSTB identifier
SB_NAME = 'kp1247271.fits    '  /  name assigned by iSTB
RMCOUNT =                    0  /  remediation counter
RECNO   =              1247271  /  NOAO Science Archive sequence number
XTALKFIL= 'Mosaic11_xtalkdummy.txt' /

GAIN    =                  1.1 / Approx inv gain (ADU/e)
RDNOISE =                  5.0 / Approx readout noise (e)
SATURATE=               220000 / Approx saturation (ADU)

DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.001509 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         3.103876E-42 / depth of longest jump down
CCDMEAN =            149304.45
OVSCNMTD=                    3
DQGLFSAT=                   0.
IMCMB001= 'kp1247271.fits'
IMCMB002= 'kp1247272.fits'
IMCMB003= 'kp1247274.fits'
IMCMB004= 'kp1247275.fits'
IMCMB005= 'kp1247277.fits'
IMCMB006= 'kp1247278.fits'
IMCMB007= 'kp1247281.fits'
IMCMB008= 'kp1247285.fits'
IMCMB009= 'kp1247286.fits'
IMCMB010= 'kp1247287.fits'
NCOMBINE=                   10
QUALITY =                   0.
GAINMEAN=                  1.1

PIPELINE= 'NOAO Mosaic Pipeline' / Name of calibration pipeline
PLVER   = 'MOSAIC V1.1'        / Pipeline version
EFFTIME =                   55 / [s] Effective exposure time
PLPROPID= 'CAL     '

PLQUEUE = 'K4M10B  '           / PL Queue
PLQNAME = '20101027'           / PL Dataset
PLPROCID= '7ae6e75 '           / PL Processing ID
PLFNAME = 'IFlat-kp4m20101027T235153F' / PL Filename
PLOFNAME= 'n501453_pl'         / Original file name
PCOUNT  =                    0 / No 'random' parameters
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'ccd8    '           / Extension name
INHERIT =                    T / Inherits global header
DATE    = '2010-11-02T18:19:28' / Date FITS file was generated
IRAF-TLM= '2010-11-03T17:47:13' / Time of last modification
IMAGEID =                    8 / Image identification

CCDNAME = 'SN11-01 '           / CCD name
AMPNAME = 'SN11-01:C'          / Amplifier name
BUNIT   = 'adu     '           / [adu] ADU counts
CCDSUM  = '1 1     '           / CCD pixel summing
DETSEC  = '[6145:8192,4097:8192]' / Detector section


XTALKCOR= 'Nov  2 11:03 No crosstalk correction required'
BIASFIL = 'K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z[ccd8]' / Bias reference
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =                   0. / min in overscan region
DQOVMAX =              474.449 / max in overscan region
DQOVMEAN=             470.7435 / mean in overscan region
DQOVSIG =             22.62346 / sigma in overscan region
DQOVSMEA=             470.8597 / mean in collapsed overscan strip
DQOVSSIG=             1.643349 / sigma in collapsed overscan strip
SATPROC = 'Nov  2 11:15 Sat: 220000. ADU (242000. e-), grw=0'
TRIM    = 'Nov  2 11:15 Trim is [1:1024,1:4096]'
FIXPIX  = 'Nov  2 11:15 Fix Mosaic11_dummy_8_bpm.pl + sat' /
OVERSCAN= 'Nov  2 11:15 Overscan is [1025:1074,1:4096], function=minmax'
ZEROCOR = 'Nov  2 11:15 Zero is K4M10B_20101027_7ae6c2c-kp4m20101027T213612Z' /
CCDPROC = 'Nov  2 11:15 CCD processing done'
AMPMERGE= 'Nov  2 11:16 Merged 2 amps'
DQGLMEAN=             153475.3
DQGLSIG =             7134.798
DQDFCRAT=              2790.46
DCCDMEAN=             1.017019
CCDMNTMP=             149304.5
   Ž                          ń  ń     Ć  Ü      ’’’               ’Ć      !@3 
@ @+`
 @`
 @`
`` @Ę 
@`` @"`
 @`
 @ 
@ @`
` @%`
 @`
` @ 
@ @ 
@ @
`
``
 @`
 @`
``` @`
 @e`
 @ 
@ @`
 @r 
@ @c 
@ @`
 @`
` @`
 @ 
@ @B`
` @ 
@`` @S`
 @3`
 @
`
 @>`
``` @.`
 @`
` @B`
 @`
 @t`
` @7`
` @`
```` @`
 @+ 
@ @`
` 
@` @(`
 @3`
 @	`
 @
`
` @`````
` @	`
``
 @`
 @)`
` @ `
 @`
 @!`
 @!`
 @`
 @	`
 @`
 @`
 @`
 @&`
 @`
 @$`
`` @'`
 @+`
 @`
```` @*`
 @6`
 @£ 
@ @`
 @`
 @`
`` @`
 @%`
``
`` @` @```
 @`
 @s 
@ @f 
@ @`
 @H`
 @A`
 @: 
@ @.`
 @`
`````````` @H 
@ @U`
`````` @ @ @z­ @*­@` @ @``````` @`
 @ @ @`
 @F`
` @ 
@ @`
 @`
 @ 
@ @`
 @`
 @ 
@<ĢPW 9P9  @pe0AP ^P`
0:P`
 P 	P0{P`	 0P`
`	p_ & 0@ 
@l°;ö @p'`0p&`
Õ ’         ’ 
   
   8@Ę  ’ 	   	  PµK  ’ 	   	  Pi  ’ 
   
   @h  ’ 
   
  @u  ’ 
   
  @u  ’ 	   	  R6Ź  ’ 	   	  Si  ’ 
   
  ¹@E  ’ 	   	  S»E  ’ 	   	  SŗF  ’ 
   
  ė@  ’ 	   	  Sķ  ’ 
   
  2@Ģ  ’ 	   	  UĒ9  ’ 
   
  «@S  ’ 
   
  I@µ  ’ 	   	  VKµ  ’      „@ł@! ?  ’      „@ś@ @  ’ 
   
  @ż@  ’ 
   
  @ū@  ’ 	   	   G’  ’ 
   
  1@Ģ  ’ 	   	  @’  ’ 
   
  @ü@ 3 ’ 
   
  „@Y  ’      „@R¬ ­ + ’ 
   
  „@Y  ’      „@	@N  ’ 
   
  „@Y  ’      „@Pąy  ’ 
   
  „@Y  ’      S@ O@Y  ’      T@ O@Y  ’      TV O@Y Ę ’ 
   
  „@Y  ’      „@8@   ’      y@*@Y  ’      R{*@Y " ’ 
   
  „@Y  ’      S5p@Y  ’ 
   
  „@Y  ’      y@*@Y  ’ 
   
  „@Y  ’      „@Æ@Ø  ’ 
   
  „@Y  ’      „@Pū^  ’      „@ ł@^ % ’ 
   
  „@Y  ’      „@ @P  ’ 
   
  „@Y  ’      „@Qį ė@   ’      „@Ģ@   ’ 
   
  „@Y  ’      Si<@Y  ’ 
   
  „@Y  ’      PĪ×@Y 
 ’ 
   
  „@Y  ’      PåĄ@Y  ’ 
   
  „@Y  ’      „@Q;  ’ 
   
  „@Y  ’      Qõ°@Y  ’ 
   
  „@Y  ’      R@ Q@Y  ’      Q@ Q@Y  ’      R@ P@Y  ’      R@ Q@Y  ’ 
   
  „@Y  ’      „@ i@ī e ’ 
   
  „@Y  ’      „@Póf  ’ 
   
  „@Y  ’      „@PŅ  ’ 
   
  „@Y  ’      „@Rø ” r ’ 
   
  „@Y  ’      Pęæ@Y c ’ 
   
  „@Y  ’      Tq 4@Y  ’ 
   
  „@Y  ’      @@Y  ’ 
   
  „@Y  ’      Qy,@Y  ’      x@+@Y  ’ 
   
  „@Y  ’      Q@Y  ’ 
   
  „@Y  ’      Q{*@Y B ’ 
   
  „@Y  ’      Se@@Y  ’      R;j@Y  ’ 
   
  „@Y  ’      Pč½@Y  ’       ę@¼@Y  ’       ę@½@Y S ’ 
   
  „@Y  ’      „@ ]@ś 3 ’ 
   
  „@Y  ’      PįÄ@Y 
 ’ 
   
  „@Y  ’      Q­ų@Y > ’ 
   
  „@Y  ’      „@ś@P [  ’      „@ł@ [  ’      „@ł@ \  ’      „@ś@ ] . ’ 
   
  „@Y  ’      „@R­ ¬  ’ 
   
  „@Y  ’      „@PŹ  ’      „@PÉ B ’ 
   
  „@Y  ’      R-x@Y  ’ 
   
  „@Y  ’      „@R Õ t ’ 
   
  „@Y  ’      u@ -@Y  ’      @o@ -@Y 7 ’ 
   
  „@Y  ’      „@PR  ’      E@^@Y  ’ 
   
  „@Y  ’      „@PŠ  ’      „@ Ķ@  ’      „@ Ī@  ’      „@ Ļ@  ’      „@ Ī@  ’ 
   
  „@Y  ’      }@&@Y + ’ 
   
  „@Y  ’      „@ź@l  ’ 
   
  „@Y  ’      „@y@ Ž  ’ 
   
  „@Y  ’      PsQæs@Y  ’      R2s@Y ( ’ 
   
  „@Y  ’      P @Y 3 ’ 
   
  „@Y  ’      TO V@Y 	 ’ 
   
  „@Y  ’      R@Y 
 ’ 
   
  „@Y  ’      P£@Y  ’      §@Pł@Y  ’      §@ł@Y  ’      Ø@ł@Y  ’      Ø@ś@Y  ’      QŖū@Y  ’ 
   
  „@Y  ’      „@Qrē  ’      „@q@ę 	 ’ 
   
  „@Y  ’      P@Y  ’ 
   
  „@Y  ’      QÜÉ@Y  ’ 
   
  „@Y  ’      PĒŽ@Y ) ’ 
   
  „@Y  ’      S¢@Y  ’      ”@@Y   ’ 
   
  „@Y  ’      „@R0)  ’ 
   
  „@Y  ’      QōP(@Y ! ’ 
   
  „@Y  ’      R]H@Y ! ’ 
   
  „@Y  ’      „@Q“„  ’ 
   
  „@Y  ’      Q·ī@Y 	 ’ 
   
  „@Y  ’      ¬@÷@Y  ’ 
   
  „@Y  ’      ń@²@Y  ’ 
   
  „@Y  ’      P@Y  ’ 
   
  „@Y  ’      „@P+. & ’ 
   
  „@Y  ’      R@Y  ’ 
   
  „@Y  ’      T @Y $ ’ 
   
  „@Y  ’      „@PG  ’      =@f@ @F  ’      =@f@PG ' ’ 
   
  „@Y  ’      „@Rņ g + ’ 
   
  „@Y  ’      S?f@Y  ’ 
   
  „@Y  ’      „@QĪ  ’      „@Ī@  ’      „@Ļ@  ’      „@Š@  ’      „@Ń@ * ’ 
   
  „@Y  ’      S­ ų@Y 6 ’ 
   
  „@Y  ’      „@PH £ ’ 
   
  „@Y  ’      P¢@Y  ’ 
   
  „@Y  ’      „@ K@  ’ 
   
  „@Y  ’      T @Y  ’ 
   
  „@Y  ’      „@4@#  ’      „@4@"  ’      „@Q6#  ’ 
   
  „@Y  ’      „@P(1 % ’ 
   
  „@Y  ’      „@QĻ  ’ 
   
  „@Y  ’      „@ n@é  ’      „@ m@é  ’      „@ l@é  ’      „@ l@ź  ’      „@ l@ė  ’      „@ k@ģ  ’      „@Plķ  ’ 
   
  „@Y  ’      „@R§ ²  ’ 
   
  „@Y  ’      Sl9@Y s ’ 
   
  „@Y  ’      „@R
O f ’ 
   
  „@Y  ’      Q$@Y  ’ 
   
  „@Y  ’      „@QÓ H ’ 
   
  „@Y  ’      P`E@Y A ’ 
   
  „@Y  ’      Pf?@Y : ’ 
   
  „@Y  ’      „@Q÷b . ’ 
   
  „@Y  ’      „@Rŗ   ’ 
   
  „@Y  ’      „@Ä@  ’      „@Ć@  ’      „@Ā@  ’      „@Ā@	  ’      „@Ć@  ’      „@Ä@  ’      „@Å@  ’      „@Ę@  ’      „@Ē@  ’      „@Č@  ’      „@QŹ H ’ 
   
  „@Y  ’      „@ `@÷ U ’ 
   
  „@Y  ’      „@S
 O  ’      „@@ J  ’      „@@ H  ’      „@ @ F  ’      „@’@ E  ’      „@PÄ:@ D  ’      „@PÄ9@ C  ’      „@ü@ B  ’      „@ū@ A  ’      „@ś@ @  ’      P²ó@ś@ @  ’      P³ņ@ś@ @  ’      „@ū@ A  ’      „@ü@ B  ’      „@ż@ C  ’      „@ž@ D  ’      „@’@ E  ’      „@ @ F  ’      „@@ H  ’      „@@ J  ’      „@S
 O  ’ 
   
  „@Y  ’      „@@S  ’      %@}@Y  ’      $@}@Y  ’ 
   
  „@Y  ’      „@QĀ F ’ 
   
  „@Y  ’      P	@Y  ’       @@Y  ’ 
   
  „@Y  ’      Rī·@Y  ’ 
   
  „@Y  ’      „@ Ć@  ’ 
   
  „@Y  ’      Q=h@Y  ’ 
   
  „@Y  ’      T' ~@Y  ’ 
   
  „@Y  ’      Rņ³@Y  ’ 
   
  „@Y  ’      SŃ Ō@Y  ’ 
   
  „@Y  ’ 
   
  ¤@W  ’ 	   	  Qv