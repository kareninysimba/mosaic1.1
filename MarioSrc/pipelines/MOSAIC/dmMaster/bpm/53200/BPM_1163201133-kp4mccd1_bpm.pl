  ¦V  /  ź$TITLE = "Dflats  filter1 = B"
$CTIME = 847643888
$MTIME = 847643924
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:34:40' / Date FITS file was generated
IRAF-TLM= '16:34:47 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    1 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.475 / Total elapsed time in secs

CCDNAME = 'SITe #7298FBR06-01 (NOAO 14)' / CCD name
AMPNAME = 'SITe #7298FBR06-01 (NOAO 14), lower right (Amp12)' / Amplifier name
GAIN    =                  3.1 / gain expected for amp 112 (e-/ADU)
RDNOISE =                  7.0 / read noise expected for amp 112 (e-)
SATURATE=                36000 / Maximum good data value (ADU)
CONHWV  = 'ACEB001_AMP12'      / Controller hardware version
ARCONG  =                  3.1 / gain expected for amp 112 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[2072:25,1:4096]'   / Amplifier section
DETSEC  = '[1:2048,1:4096]'    / Detector section

ATM1_1  =                  -1. / CCD to amplifier transformation
ATM2_2  =                   1. / CCD to amplifier transformation
ATV1    =               2073.0 / CCD to amplifier transformation
ATV2    =                   0. / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                   0. / CCD to detector transformation
DTV2    =                   0. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =              4201.98 / Coordinate reference pixel
CRPIX2  =             4137.486 / Coordinate reference pixel
CD1_1   =        6.1119817e-07 / Coordinate matrix
CD2_1   =       -7.1329086e-05 / Coordinate matrix
CD1_2   =       -7.1701224e-05 / Coordinate matrix
CD2_2   =        4.5191754e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. 4.069675375994405E-4 0.295'
WAT1_002= '274600595593 0.1517720770709265 0.2996333188403072 -2.43400889604536'
WAT1_003= '4E-4 -1.863153569610771E-5 -5.482794949672218E-4 -8.085430890308042E'
WAT1_004= '-5 -6.904936574741062E-5 -5.426855463797667E-4 -3.097713139645927E-5'
WAT1_005= ' -5.100605320647182E-5 -4.608163781684982E-5 -4.778467110637049E-6"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. 4.069675375994405E-4 0.29'
WAT2_002= '5274600595593 0.1517720770709265 0.2996333188403072 -2.1214373636907'
WAT2_003= '91E-4 1.687649491394406E-5 -2.681944559664209E-4 4.133968152092598E-'
WAT2_004= '6 -9.958450986377670E-5 -3.854028908544852E-4 -7.758933668028480E-5 '
WAT2_005= '-2.141933667661954E-4 4.382513177000551E-6 -4.793137925231863E-6"'

CHECKSUM= 'PfPgQcPePcPePcPe'    /  ASCII 1's complement checksum
DATASUM = '564576666 '          /  checksum of data records
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd1.fits'
XTALKCOR= 'Oct 23 16:48 Crosstalk is 0.00162*im02'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             2462.686 / min in overscan region
DQOVMAX =             2465.045 / max in overscan region
DQOVMEAN=             2463.936 / mean in overscan region
DQOVSIG =             2.240824 / sigma in overscan region
DQOVSMEA=             2463.937 / mean in collapsed overscan strip
DQOVSSIG=            0.2921181 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 16:54 Trim is [65:2112,1:4096]'
OVERSCAN= 'Oct 23 16:54 Overscan is [1:50,1:4096], function=minmax'
ZEROCOR = 'Oct 23 16:54 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 16:54 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=                   0.
DQGLMEAN=               7315.3
DQGLSIG =             289.3057
DQDFCRAT=             406.4055
DQDFGLME=             7274.995 / DQ global mean of zero frame
DQDFGLSI=             373.7411 / DQ global sigma of zero frame
DQDFXMEA=             7274.995 / DQ mean of collapsed x bias
DQDFXSIG=             322.7278 / DQ sigma of collapsed x bias
DQDFXSLP=            0.4713564 / DQ slope of collapsed x bias
DQDFXCEF=            0.8636909 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             7274.995 / DQ mean of collapsed y bias
DQDFYSIG=             88.36041 / DQ sigma of collapsed y bias
DQDFYSLP=           0.04179804 / DQ slope of collapsed y bias
DQDFYCEF=            0.5593975 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=             7166.589 / DQ global mean of zero frame
DQADGLSI=             365.6387 / DQ global sigma of zero frame
DQADXMEA=             7166.589 / DQ mean of collapsed x bias
DQADXSIG=             317.9471 / DQ sigma of collapsed x bias
DQADXSLP=             0.464323 / DQ slope of collapsed x bias
DQADXCEF=             0.863596 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             7166.589 / DQ mean of collapsed y bias
DQADYSIG=             87.29263 / DQ sigma of collapsed y bias
DQADYSLP=           0.04141555 / DQ slope of collapsed y bias
DQADYCEF=            0.5610586 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=             0.846036
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
   Ž                                  ­  ź      ’’’               ’ ­      ``` P @ @ 3@ PĒ`
` 
R 
@	 
@0 @ @Ø` @` @F @ @ @` @_`` @ @#µ@3Ø@@ @\` @`` @ @ @` @ @* @ @?` @ @ @|` @ @ @»` @ @ @ @ @ @ @` @	`` @` @G @ @`` @`` @n @````` @G @ @ @ @ @ @ @ @ @ @m @ @Ņ @ @` @`` ’         ’       j@0 R@ %  ’       Wk0PTP
PP @ +  ’      ¾@ @ !  ’       „@Y  ’       ¤@X  ’       „@X  ’       Ā@;  ’ 
   
   Uv  ’       @u  ’ 
   
   Uv  ’ 
   
  X@¦ 	 ’ 
   
  W@„ 0 ’       VW0@`¤  ’       VW0@¤ Ø ’       VW0@`¤  ’       ü@PX0@`¤  ’       ū@PX0@`¤  ’       ū@PY0@`¤ F ’       VW0@`¤  ’      V@`¤  ’       VW0@`¤  ’      V@`¤  ’      VV`0@`¤ _ ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤ @ ’       VW0@`¤ \ ’       VW0@`¤  ’       R×S0@`¤  ’       VW0@`¤  ’      V@`¤  ’       VW0@`¤  ’      V@`¤  ’       VW0@`¤  ’      V@`¤  ’       VW0@`¤  ’      V@`¤ * ’       VW0@`¤  ’      V@`¤ ? ’       VW0@`¤  ’       VW0@`P§ ż  ’       VW0@`¤  ’       VW0@¤ | ’       VW0@`¤  ’       VW0@`Q> f  ’       VW0@`¤  ’       VW0@¤ » ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤ 	 ’       VW0@`¤  ’       TLR0@`¤  ’       I@R	0@`¤  ’       H@R	0@`¤  ’       TJ @R	0@`¤ G ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤ n ’       VW0@`¤  ’       @Tµ0@`¤  ’       @Tµ0@`¤  ’       @T¶0@`¤  ’       @Tø0@`¤  ’       @Tø0@`¤  ’       @Tø0@`¤ G ’       VW0@`¤  ’       @Sµ0@`¤  ’       VW0@`¤  ’      V@`¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’       VW0@¤  ’       VW0@`¤  ’      V@`¤ m ’       VW0@`¤  ’       VW0@¤ Ņ ’       VW0@`¤  ’       W@Qż0@`¤  ’       W@Qü0@`¤  ’       W@Qż0@`¤  ’       VW0@`¤  ’       6@@Q @ @	 Sö0@`¤  ’       2@ ć@ @ ś@
 @ \@÷@ Q|0@`¤  ’       VW0@¤