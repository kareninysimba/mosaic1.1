  �V  0~  $TITLE = "Domeflats (afternoon) filter1 = B"
$CTIME = 847980856
$MTIME = 847989328
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-14T20:02:51' / Date FITS file was generated
IRAF-TLM= '13:02:59 (14/11/2006)' / Time of last modification
OBJECT  = 'Domeflats (afternoon) filter1 = B' / Name of the object observed
FILENAME= 'dflat011'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '20:01:49.85'        / RA of observation (hr)
DEC     = '20:39:35.79'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-12-08T19:33:49.3' / Date of observation start (UTC approximate)
TIME-OBS= '19:33:49.302'       / Time of observation start
MJD-OBS =       53347.81515396 / MJD of observation start
MJDHDR  =       53347.81507176 / MJD of header creation
LSTHDR  = '20:02:03.4        ' / LST of header creation

OBSERVAT= 'CTIO    '           / Observatory
TELESCOP= 'CTIO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2000.0 / Equinox of tel coords
TELRA   = '20:01:49.85       ' / RA of telescope (hr)
TELDEC  = '20:39:35.8        ' / DEC of telescope (deg)
HA      = '00:00:01.1        ' / hour angle (H:M:S)
ZD      = '50.8              ' / Zenith distance
AIRMASS =                1.581 / Airmass
TELFOCUS= '16071             ' / Telescope focus
CORRCTOR= 'Blanco Corrector'   / Corrector Identification
ADC     = 'Blanco ADC'         / ADC Identification

DETECTOR= 'Mosaic2 '           / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                   16 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.270 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.270 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    3 / Filter position
FILTER  = 'B Harris c6002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -1.6010000000000E+00 / North TV Camera focus position
TV2FOC  = -1.9970000000000E+00 / South Camera focus position
ENVTEM  =  2.0900000000000E+01 / Ambient temperature (C)
DEWAR   = 'Mosaic2 Dewar'      / Dewar identification
DEWTEM  =  0.0000000000000E+00 / Dewar temperature (C)
DEWTEM2 = -4.9799999000000E+01 / Fill Neck temperature (C)
DEWTEM3 = 'N2  90.6'
CCDTEM  = -9.4000000000000E+01 / CCD temperature (C)
CCDTEM2 = 'CCD 169.8'

WEATDATE= 'Dec 08 19:31:02 2004' / Date and time of last update
WINDSPD = '4.7     '           / Wind speed (mph)
WINDDIR = '248     '           / Wind direction (degrees)
AMBTEMP = '23.2    '           / Ambient temperature (degrees C)
HUMIDITY= '24      '           / Ambient relative humidity (percent)
PRESSURE= '782     '           / Barometric pressure (millibars)
DIMMSEE = 'mysql_query():Unknown Error seeing=column' / Tololo DIMM seeing

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '13July99ver7_30'    / Controller software version
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Wed Dec  8 15:51:13 2004' / Date waveforms last compiled

OBSERVER= 'ESSENCE/SuperMACHO' / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'ct4m.20041208T193349' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/ctio/4meter/caldir/Mosaic2/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic2_xtalk040501.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =               135390  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



























































































































DTSITE  = 'ct                '  /  observatory location
DTTELESC= 'ct4m              '  /  telescope identifier
DTINSTRU= 'optic             '  /  instrument identifier
DTCALDAT= '2004-12-08        '  /  calendar date from observing schedule
DTPUBDAT= 'none              '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= 'noao              '  /  observing proposal ID
DTPI    = 'Christopher Stubbs'  /  Principal Investigator
DTPIAFFL= 'University of Washington'  /  PI affiliation
DTTITLE = 'A Next Generation Microlensing Survey of the LMC'  /  title of observ
DTACQUIS= 'ctioa8.ctio.noao.edu'  /  host name of data acquisition computer
DTACCOUN= 'mosaic            '  /  observing account name
DTACQNAM= '/ua80/mosaic/tonight/dflat011.fits'  /  file name supplied at telesco
DTNSANAM= 'ct135390.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041208/ct4m/noao/ct135390.fits.gz'
UPARM   = 'MarioCal$/Mosaic2_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M2_53166'
IMAGEID =                   11 / Image identification
EXPTIME =               35.000 / Exposure time in secs
DARKTIME=               43.822 / Total elapsed time in secs

CCDNAME = 'SITe #98173FABR10-02 (NOAO 25)' / CCD name
AMPNAME = 'SITe #98173FABR10-02 (NOAO 25), upper left (Amp23)' / Amplifier name
GAIN    =                  2.6 / gain expected for amp 323 (e-/ADU)
RDNOISE =                  8.0 / read noise expected for amp 323 (e-)
SATURATE=                44000 / Maximum good data value (ADU)
CONHWV  = 'ACEB003_AMP23'      / Controller hardware version
ARCONG  =                  2.6 / gain expected for amp 323 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[2049:4096,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                2048. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'ct4m.20020927T000014 (USNO N B Harris c6002) by F. Valdes 2002-10-24'
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =            300.45771 / Coordinate reference value
CRVAL2  =            20.659942 / Coordinate reference value
CRPIX1  =             2023.319 / Coordinate reference pixel
CRPIX2  =            -19.72543 / Coordinate reference pixel
CD1_1   =        2.2820792e-07 / Coordinate matrix
CD2_1   =        7.4389462e-05 / Coordinate matrix
CD1_2   =        7.4067709e-05 / Coordinate matrix
CD2_2   =        3.1225917e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. 0.001073575885090811 0.304'
WAT1_002= '8479858137544 -0.1504327533496561 0.003121176637822637 -2.0203943671'
WAT1_003= '26917E-4 3.323787653624544E-5 -6.452098302971465E-4 -9.3916031378555'
WAT1_004= '11E-5 -9.608566129932258E-5 2.226045436829402E-4 1.541840020133133E-'
WAT1_005= '5 -5.078261478124224E-5 -5.866992501777399E-5 -9.785853997717662E-6 '
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. 0.001073575885090811 0.30'
WAT2_002= '48479858137544 -0.1504327533496561 0.003121176637822637 4.6817274836'
WAT2_003= '48569E-5 -4.872228124463091E-5 1.220451115668443E-4 -1.2365995050031'
WAT2_004= '73E-5 1.342003474931889E-6 -4.318604922098636E-4 -9.897609446546496E'
WAT2_005= '-5 8.059883616646803E-5 2.818811213743012E-6 -1.862896137151850E-5 "'

CHECKSUM= 'XKKWYJIUXJIUXJIU'    /  ASCII 1's complement checksum
DATASUM = '3172231450'          /  checksum of data records
XTALKCOR= 'Oct 23 22:52 Crosstalk is 0.00121*im09+3.25E-4*im10'
ZERO    = 'MarioCal$/UBVRI_C20041208_1161662005-ct4m20041208T190047Z-ccd6.fits'
CALOPS  = 'XOTZFWSBP'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             1138.493 / min in overscan region
DQOVMAX =              1142.33 / max in overscan region
DQOVMEAN=             1140.145 / mean in overscan region
DQOVSIG =             3.662626 / sigma in overscan region
DQOVSMEA=             1140.135 / mean in collapsed overscan strip
DQOVSSIG=            0.4811625 / sigma in collapsed overscan strip
WAT1_006= '"       '
TRIM    = 'Oct 23 23:27 Trim is [25:1048,1:4096]'
OVERSCAN= 'Oct 23 23:27 Overscan is [1063:1112,1:4096], function=minmax'
ZEROCOR = 'Oct 23 23:27 Zero is MarioCal$/UBVRI_C20041208_1161662005-ct4m200412'
CCDPROC = 'Oct 23 23:27 CCD processing done'
PROCID  = 'ct4m.20041208T193349V5'
OVSCNMTD=                    3
AMPMERGE= 'Oct 23 23:28 Merged 2 amps'
DQGLFSAT=                   0.
DQGLMEAN=             13339.75
DQGLSIG =             370.8382
DQDFCRAT=             381.1357
DQDFGLME=              13344.8 / DQ global mean of zero frame
DQDFGLSI=             397.3074 / DQ global sigma of zero frame
DQDFXMEA=              13344.8 / DQ mean of collapsed x bias
DQDFXSIG=             317.4273 / DQ sigma of collapsed x bias
DQDFXSLP=             0.466611 / DQ slope of collapsed x bias
DQDFXCEF=            0.8692727 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=              13344.8 / DQ mean of collapsed y bias
DQDFYSIG=              169.453 / DQ sigma of collapsed y bias
DQDFYSLP=          -0.03942205 / DQ slope of collapsed y bias
DQDFYCEF=            0.2751136 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  708
DQADNUSD=                    9
DQADNREJ=                    0
DQADGLME=             9644.025 / DQ global mean of zero frame
DQADGLSI=             289.9589 / DQ global sigma of zero frame
DQADXMEA=             9644.024 / DQ mean of collapsed x bias
DQADXSIG=             229.9699 / DQ sigma of collapsed x bias
DQADXSLP=            0.3386767 / DQ slope of collapsed x bias
DQADXCEF=            0.8708827 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             9644.024 / DQ mean of collapsed y bias
DQADYSIG=             138.6812 / DQ sigma of collapsed y bias
DQADYSLP=          -0.05213238 / DQ slope of collapsed y bias
DQADYCEF=            0.4445414 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=             1.168685
GAINMEAN=                 2.55
PROCID01= 'ct4m.20041208T193349V4'
PROCID02= 'ct4m.20041208T202848V4'
PROCID03= 'ct4m.20041208T201149V4'
PROCID04= 'ct4m.20041208T195415V4'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53347.82
MJDMAX  =             53740.88
OBJMASK = 'bp2mkbpm6.pl'
   �                                 �        ���               ���      `g�#�@ @{�`+�@ @ @ @{�+�@7s#@ @s#'@ @s'#7@ @ @% @|R,]@ @ @|h,v@|h,v@ @|v````````,@s�``#�@s�`#�@ @ @s�``#�@ @'
@7
@s�#�@ @&�@6�@ @ @s�`#�@r�```````"Y@rJ```",@s�#�@ @ @s�#�@H @ @|�,�@|�,�@
 @ @(|�,�@|�,�@ @|�-@ @}-@
}-@= @r�"�@r�"�@r�"�@ @};``-0@ @u�%�@u�%�@u�%�@	 @ @}n``-[@  @}W`-S@0}G-P@d�ppp0@4�@"%@5@$�@4�@}M� @`p '@p4 A@0N@4D@($�@0@4�@ 	@}S-`@}S-\@}P-Y@I}M-V@}J-S@}G-P@$ 	@}N-Z@
}N-W@}L-U@}I-R@	}F-O@}C-L@}@-I@}>-G@ @"}G-P@* 	@` 	@ @*}q-z@- 	@ @}�-�@}�-�@5 	@ @ 	@ @}�-�@ 	@}�-�@ 	@}�``-�@}�-�@}�`-�@	 	@ @}�-�@ 	@ 	@ @# 	@ @;}�`m�}�-�@}�-�@H 	@ @ 	@}�-�@
 	@ @}�``-�@ 	@ @ 	@}�-�@}�-�@ 	@ @# 	@ @w�`'�@w�a�p`%�@~,.5@1~).2@	w[v�fu```'�@b�`z1jMpr�b� @ @ @` @`z�`v�.=@~1.:@~..:@vs`&p@~:`.6@ 	@ @(~@`.=@�~2.;@~0.9@~.``.@~.@~.@
~.@ 	@ @~.&@~.#@ @~#.,@ @xp(7@x*p(Q@ @2~P.Y@~M.V@8~J.S@	 	@ @Jx�)@~e.n@~b.k@~```.U@* 	@ @~_.h@>\P 
P` P`
"� #@2�P PU 
P"� -@2�P) 
P 
PN 
P`
 P 
@  @"� @r�.�P>~�.�@`<4@run�<4 @  @2�P`  @ 
PO 
P``` P 
P 
P`
 P 
P`
 P 
P 
@"nPpprI`b'pp`5pppp
1�P< 
P 
 @ P 
 @ P
`
 P!i@  ,@1iP!t @  ]@1u @` P`(`#`0`7� ��         ��       �@0 @  ,BHp A`  ��       �@0 @Ru ]@  ��       �@0 @Ru Q_�  ��      �@ P0Rd�  ��      �@Ru�  ��      �@Ru U@w  ��      �@Ru R@w  ��       n@0 9@Ru P� \@s  ��       n@0 9@Ru T@s  ��       Rp0 :@Ru T@t  ��      �@Ru V@t  ��      �@Ru X@u  ��      �@Ru W@u  ��      �@Ru W@v  ��      �@Ru [@ @w  ��      �@Ru c@x  ��      �@ QN0Q'�  ��      �@ PJ0R+�  ��      �@ Q[0Q�  ��      �@Ru Q�  ��      �@Ru�  ��       PJ0`@Ru�  ��      �@ P�0Q��  ��      �@Rt�  ��      �@Ru�  ��      �@Rt�  ��      �@  T@0R�  ��      �@  T@0R�  ��      �@  U@0R�  ��       �@0Qv�  ��       S�0Qw�  ��       SH0Q��  ��       R�0Rh�  ��       �@0Q��  ��       T�0P��  ��       S�0QI�  ��       P�0TR�  ��       S0R�  ��      U! RE �  ��      U! �@   ��      U! P�@  ��       U0P�  ��       R�0R�  ��       T:0P��  ��       TL0P��  ��      U! P��  ��       �@0P5�  ��      U! Q9�  ��       Q0T�  ��       Q0T�  ��       P�0Tm�  ��       T�0P��  ��      U! PK0R�  ��      U!  #@0R�  ��      U!  #@0R�  ��      U!  %@0R�  ��       T+0P�R�  ��       T�0PT P@0R�  ��       �@0PSR�  ��       Rm0R�R�  ��      U! Q�  ��      U! �@  ��       P�0T.�  ��      U! P�  ��       6@0P��  ��       �@0Rd�  ��       �@0Rd�  ��       R�0Re�  ��       �@0S��  ��       R�0RK�  ��       Q�0S3�  ��       T�0PQ�  ��       @	0U�  ��       @	0U�  ��      U! �@ #  ��      U! �@ #  ��       �@0P.�  ��       T�0P/�  ��       T�0P\�  ��      U! R�   ��      U! Q�*  ��      U! R� U  ��       �@0S)�  ��       �@0S(�  ��       Q�0S)�  ��       S�0Q.�  ��       R�0RY�  ��       Q0T�  ��       S�0Qo�  ��       (@0P��  ��       T0Q�  ��       P0U�  ��       Rn0R��  ��       S�0Q��  ��       Q�0SQ�  ��      U! P��  ��      U! P��  ��      U!  �@�  ��      U! P��  ��       P�0T2�  �� 
   
   V�  ��       J@�  ��       I@�  �� 
   
   S�*  �� 
   
   V�   ��       :@�  �� 
   
   Q�  �� 
   
   V�  �� 
   
   W5 �  �� 
   
   U�k  �� 
   
   PS�  �� 
   
   W8 �  ��       7@ �  �� 
   
   Q�T  ��       �@S  ��       #@�  ��       �@|  ��       �@,  ��       �@,  ��       0@ �  ��       0@ �  �� 
   
   V�n  �� 
   
   W�   ��       �@	  ��       �@	  ��       �@Q�  ��       �@�@  �� 
   
   V�c  �� 
   
   W* �  �� 
   
   R�  ��       �@~  �� 
   
   P�  �� 
   
   U�  ��       �@  �� 
   
   P�  �� 
   
   R�]  ��       �@]  ��        @�@   ��       �@ 
  �� 
   
   S�  �� 
   
   T�\  ��        t@�  �� 
   
   V}�  ��        {@�  �� 
   
   T\�  ��       Z@�  ��       @�  �� 
   
   T�  ��       i@�  ��       h@�  �� (   (    -@ @ @ @� @ @ @	 @ @ @ @P @P @P '@(  �� #   #    B> @P @ @P @ @ @ @PP @ @ @I  �� 0   0    C� @ @	 @ @
 @ @ @P @ @
 @ @ @ @ @P @P @ �@�  �� 7   7    E_ @ @ @ @	PP @ @P @P @ @
P @ @ @ @ @ @P @ @ 
@ .@�  �� 7   7    E� @ @ @ZP @P @ @ @ @ @ @ @ @P @ @P 3@ @ @ @P0 �@ 	  ��       �@0 @  ,BHp A`  ��        i@�  ��       �@)  ��       �@|  ��       �@+  ��       @ �  �� 
   
   Q/�  ��       @�  ��       `@�  ��       �@(  ��       K@�  �� 
   
   �@  �� 
   
   �@  ��       �@
   �� 
   
   �@	  ��       �@
   �� 
   
   �@  ��       �@
   �� 
   
   �@  ��        @�@   ��        @�@   ��       �@   ��       P70s@Ru�  ��      �@ P0R`�  ��       P�0"@Ru�  ��       P}0-@Ru�  ��      P�@Ru R� 6  ��      P`0�@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@ R!0PT�  ��       QG0c@Ru�  ��      �@ Q0Qq�  ��      �@ O@0Q#�  ��      �@ P@0Q#�  ��      �@Ru�  ��      �@Ru Qtk  ��        �@0�@Ru�  ��        �@0�@Ru�  ��      �@Rt�  ��      �@ R0P\�  ��      U! �@   ��      U! �@   ��      U! Q� �@   ��       SO0Q��  ��       @
0U�  ��       @0U�  ��       @0U�  ��       @0U�  ��       @E@0S��  ��       @0U�  ��       @
0U�  ��       @0U�  ��       �@0S5�  ��       �@0S1�  ��       �@
0S3�  ��       �@0S1�  ��      �@ P.0RG�  ��      �@  *@0RD�  ��      �@  )@	0RC�  ��      �@  (@0RB�  ��      �@  '@0RA�  ��       Qb0H@  '@0RA�  ��      �@  '@0RA�  ��      �@  &@0R@�  ��      �@  (@0RB�  ��      �@  )@	0RC�  ��      �@  *@0RD�  ��      �@ P.0RG�  ��       Qk0?@Rt Q� �  ��      �@Rt �@ �  ��      �@Rt Q� �  ��       S�0QM �@ %  ��      U! �@ %  ��      U! �@
   ��      U! �@   ��      U! �@   ��       �@0 @Ru ]@  ��       �@0 @Ru ]@  ��       �@0 @Ru�  ��      �@Ru�  ��      �@Ru P�  ��      �@Ru� 7 ��      �@Ru�  ��      �@Ru�  ��      P�@Ru�  ��       Pp�@Ru�  ��       Pp`0�@Ru�  ��       Pp`0�@Ru�  ��      P�@Ru� % ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@Ru `@y  ��      �@Ru S@w  ��      �@Ru R@u  ��      �@Ru R@s  ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@ ^@0P�  ��      �@Ru�  ��      �@Ru r@j  ��      �@Ru�  ��       X@0O@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@  '@0RA�  ��      �@Ru�  ��      �@Ru �@  ��      �@Ru�  ��      �@ @0P\� H ��      �@Ru�  ��        �@0�@Ru�  ��      �@Ru�  ��      �@Ru� 
 ��      �@Ru�  ��      �@Ru� ( ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@Ru�  ��      �@Rt�  ��      �@Ru� 
 ��      �@Ru� = ��      �@Ru�  ��      �@Rt�  ��      �@Rt �@ �  ��      �@Rt �@ �  ��      �@Rt�  ��      �@Ru�  ��      �@Ru �@   ��      �@Ru�  ��      �@Ru�  ��      �@Ru� 	 ��      �@Ru�  ��      �@  g@0R�  ��      �@Ru�   ��      �@Ru�  �� 	   	  U!� 0 �� 	   	  U!�  �� 	   	  U!� : �� 	   	  U!� 4 �� 	   	  U!�  ��       �@0Q��  �� 	   	  U!�  �� 	   	  U!� I �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!� $ �� 	   	  U!�  ��      U! �@  
 �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!� 	 �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!�  ��       �@0P4� " �� 	   	  U!� * �� 	   	  U!�  ��        @0U�  �� 	   	  U!�  ��        @0U� * �� 	   	  U!� - �� 	   	  U!�  ��        �@0T��  �� 	   	  U!�  �� 	   	  U!� 5 �� 	   	  U!�  ��       @0T�  �� 	   	  U!�  ��       P0U�  �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!R�  �� 	   	  U!R�  ��      U!  #@0R�  �� 	   	  U!R�  �� 	   	  U!R� 	 �� 	   	  U!R�  ��       �@0P�R�  �� 	   	  U!R�  �� 	   	  U!R�  �� 	   	  U!�  ��       �@0RR� # �� 	   	  U!�  ��        �@0T}� ; �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!� H �� 	   	  U!�  ��      U! �@ /  �� 	   	  U!�  ��       5@0P�� 
 �� 	   	  U!�  ��      U! P�  �� 	   	  U!�  �� 	   	  U!�  ��       L@0R��  �� 	   	  U!�  ��       �@0S��  �� 	   	  U!�  �� 	   	  U!�  ��        8@0T�� # �� 	   	  U!�  ��       @0P�  �� 	   	  U!�  ��      U! �@   �� 	   	  U!� 1 �� 	   	  U!� 	 �� 	   	  U!�  ��       @0U�  ��       @H@0S��  �� 	   	  U!�  ��      U! �@    ��      U! �@ %  �� 	   	  U!�  �� 	   	  U!�  ��       0@0S�� ( �� 	   	  U!� � �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!� 
 �� 	   	  U!�  �� 	   	  U!�  ��       U 0P!�  �� 	   	  U!�  �� 	   	  U!�  ��       '@0P��  �� 	   	  U!�  ��        @0Q�  �� 	   	  U!�  ��       �@
0S1�  ��       �@0S9� 2 �� 	   	  U!�  �� 	   	  U!� 8 �� 	   	  U!� 	 �� 	   	  U!�  ��       T�0P4� J �� 	   	  U!�  �� 	   	  U!�  �� 	   	  U!�  ��      U!  �@� * �� 	   	  U!�  ��      U! �@ 7  �� 	   	  U!�  �� 	   	  U!�  ��       N@0S��  ��       L@0S��  ��       M@0S��  ��       N@	0S��  ��       N@0S��  ��       L@0S��  ��       L@0S��  ��       �@0Sp�  ��       �@0Sp�  ��       �@0Sp�  ��       �@0Sr�  ��       �@0Ss�  ��       �@0S?�  ��       @0S�  ��       @0S�  ��      U! �@  ��      �@Ru �@ 4  ��      �@Ru �@   �� 
   
   S�.  ��       �@+  ��       �@	*  �� 
   
   S�.  ��       @F@0S��  ��       @F@0S��  ��       @G@0S��  ��       @E@
0S��  ��       @J@0S��  ��       @K@0S��  ��       @M@0S��  ��       @O@0S��  ��       @P@0S��  ��       @Q@0S��  ��       @R@0S��