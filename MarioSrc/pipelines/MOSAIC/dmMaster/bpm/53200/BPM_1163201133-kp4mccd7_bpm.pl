  ŚV  /P  ţ$TITLE = "Dflats  filter1 = B"
$CTIME = 847644897
$MTIME = 847645462
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:35:53' / Date FITS file was generated
IRAF-TLM= '16:36:00 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    7 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.478 / Total elapsed time in secs

CCDNAME = 'SITe #7294FBR05-01 (NOAO 11)' / CCD name
AMPNAME = 'SITe #7294FBR05-01 (NOAO 11), upper left (Amp21)' / Amplifier name
GAIN    =                  3.2 / gain expected for amp 421 (e-/ADU)
RDNOISE =                  6.0 / read noise expected for amp 421 (e-)
SATURATE=                28000 / Maximum good data value (ADU)
CONHWV  = 'ACEB004_AMP21'      / Controller hardware version
ARCONG  =                  3.2 / gain expected for amp 421 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[4097:6144,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                4096. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =           -45.916741 / Coordinate reference pixel
CRPIX2  =           -34.285392 / Coordinate reference pixel
CD1_1   =       -7.5272453e-08 / Coordinate matrix
CD2_1   =       -7.2442096e-05 / Coordinate matrix
CD1_2   =       -7.2172687e-05 / Coordinate matrix
CD2_2   =        3.0348395e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. -0.2982514086028947 -0.002'
WAT1_002= '550173057799662 -0.1516770098082775 -0.002145271740632979 3.14374087'
WAT1_003= '4902158E-4 -3.737375289092993E-4 5.477393377310946E-4 -9.11900156138'
WAT1_004= '1649E-5 -1.708151904195917E-4 1.923909344570269E-4 -1.76469725720530'
WAT1_005= '2E-5 5.602195714424349E-5 -4.395064399074159E-5 -5.5651331804882E-6"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. -0.2982514086028947 -0.00'
WAT2_002= '2550173057799662 -0.1516770098082775 -0.002145271740632979 8.8522215'
WAT2_003= '42283860E-5 -1.194539099729988E-4 8.972001217809093E-5 7.02003429310'
WAT2_004= '8476E-6 -1.643182768550488E-4 3.658108501377644E-4 -7.95894091142055'
WAT2_005= '3E-5 7.415626788485283E-5 -1.124083689528678E-5 -5.9237109061946E-6"'

CHECKSUM= 'DdepDcenDcenDcen'    /  ASCII 1's complement checksum
DATASUM = '2345260597'          /  checksum of data records
XTALKCOR= 'Oct 23 16:49 No crosstalk correction required'
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd7.fits'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =              2228.48 / min in overscan region
DQOVMAX =              2230.44 / max in overscan region
DQOVMEAN=              2229.48 / mean in overscan region
DQOVSIG =             1.749953 / sigma in overscan region
DQOVSMEA=             2229.481 / mean in collapsed overscan strip
DQOVSSIG=            0.2719579 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 17:02 Trim is [25:2072,1:4096]'
OVERSCAN= 'Oct 23 17:02 Overscan is [2087:2136,1:4096], function=minmax'
ZEROCOR = 'Oct 23 17:02 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 17:02 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=                   0.
DQGLMEAN=             7228.311
DQGLSIG =             184.7156
DQDFCRAT=             401.5728
DQDFGLME=             7227.418 / DQ global mean of zero frame
DQDFGLSI=             222.4373 / DQ global sigma of zero frame
DQDFXMEA=              7227.42 / DQ mean of collapsed x bias
DQDFXSIG=               112.67 / DQ sigma of collapsed x bias
DQDFXSLP=           -0.1309457 / DQ slope of collapsed x bias
DQDFXCEF=            0.6872718 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             7227.418 / DQ mean of collapsed y bias
DQDFYSIG=             98.27689 / DQ sigma of collapsed y bias
DQDFYSLP=          -0.06431574 / DQ slope of collapsed y bias
DQDFYCEF=            0.7739061 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=             7119.838 / DQ global mean of zero frame
DQADGLSI=             214.9934 / DQ global sigma of zero frame
DQADXMEA=             7119.838 / DQ mean of collapsed x bias
DQADXSIG=             110.9672 / DQ sigma of collapsed x bias
DQADXSLP=           -0.1290428 / DQ slope of collapsed x bias
DQADXCEF=            0.6876774 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             7119.838 / DQ mean of collapsed y bias
DQADYSIG=             97.04227 / DQ sigma of collapsed y bias
DQADYSLP=           -0.0636209 / DQ slope of collapsed y bias
DQADYCEF=            0.7752845 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
PUPILCOR= 'Oct 23 17:04 maximum amplitude = 8.188E-6'
DCCDMEAN=            0.8358589
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
   Ţ                          ;  ;       ţ      ˙˙˙               ˙      ``7`-```f``````````````bR0n@q!-@q@!S@p0á@`ô`09@ L@qŠ```!@0p@ ź@ @0ŕ@pő` @``Jadq @pK @ @ż @`` @` @0ľ@`` @p &@ps @ps`Mp: @p O@p;`pt` u@pb @pr @ @p_ @`p_` @pa`pj }@pi``px @pxp> O@p@`Q0B@pmp M@`p1p\ k@p-p/p1p1 q@0/@03@`01@0"@ @pFpH` H@`0F@`0X@p1`B0!@0@p!¸  @`%Đ@5ż@%Đ@uÁ````` @`` @`%A@ @ @ @ @5l@` @4` @h%=@ @ @ @5]@` @ @ @ @ @` @` @` @` @) @ @` @= @ @+ @` @ @ @ @` @
`` @0 @ @ @ @ @	 @``` @`` @ @ @ @Ą&<@ @	` @ @ @ @``` @` @ @ @
 @ @`` @ @ @ @ @ @ @ @ @ @@ @ @&` @` @ @ @ @ @` @ @	``` @ @ @` @` @ @ @K @` @ @ @ @` @` @ @! @ @|` @` @( @`` @1` @0 @ @` @H` @` @. @ @`` @` @
 @	` @` @ă @ @` @ @ @ @& @ @	` @ @ @
 @ @	 @ @m`
 @ 
@` 
@ @V 
@ @.`
 @ @ę`
 @	` @=č@ 
@ @` @? 
@ @! 
@ @ 
@ @ 
@` 
@` @ @` @L @ @`` @ @ @	,š@ @* 
@ @`
 @ 
@ @`
` 
@ @a`
`` @ 
@ @`` @ @`` @ @ 
@ @ 
@`` @ @`` @ @ @ 
@ @ @` @>@ @``````   ˙         ˙ 7   7   @h <@ 	@0 k@+ @ @: @ }@ Á@0 @/ 5@  P0@`0 ,@P? ô@2 !@ @6 @ @ @ @  ˙ -   -   @hP> 	@0PPćP  ź@Ě@0 @`0@ ]@ P,0@`0P{Q$PPd X@ @ #@  ˙       @gTÔp`P.0@`0P{  ˙       @fTŐp`P.0@`0P{  ˙       @eTÖp`P.0@`0P{  ˙       @cTŘp`P.0@`0P{  ˙       Uk0@`0P{  ˙       9@T00@`0P{  ˙       Uk0@`0P{  ˙        ´@Tľ0@P{  ˙       Uk0@`0P{  ˙       Uk0@P{  ˙       Uk0@`0P{  ˙       Uk0@P{  ˙       Uk0@`0P{  ˙       Uk0@P{  ˙       Uk0@ y@  ˙       Uk0@P{  ˙       Uk0@`0P{  ˙       SćQ0@`0P{  ˙       Uk0@`0P{  ˙       Uk0@`0 y@ 4 ˙       Uk0@`0P{  ˙       Uk0@`0 y@ h ˙       Uk0@`0P{  ˙       Uk0@`0P{  ˙      j@`0P{  ˙       Uk0@`0P{  ˙      j@`0P{  ˙       Uk0@`0P{  ˙      j@`0P{  ˙       Uk0@`0P{  ˙       Uk0@`0 y@  ˙       Uk0@`0P{  ˙       Uk0@P{  ˙       Uk0@`0P{  ˙       PnTý0@`0P{  ˙       Uk0@`0P{  ˙       Uk0@`0P{ C@ľ ) ˙       Uk0@`0P{  ˙       Uk0@`0 y@  ˙       Uk0@`0P{  ˙       Uk0@`0 y@ = ˙       Uk0@`0P{  ˙      j@`0P{ + ˙       Uk0@`0P{  ˙        Ţ@T0@`0P{  ˙       PßT0@`0P{  ˙      j@`0P{  ˙       Uk0@`0P{  ˙      j@`0P{  ˙       Uk0@`0P{  ˙       Uk0@`0 y@ 
 ˙       Uk0@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@P{ 0 ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{ 	 ˙      U< .@`0P{  ˙      U< .@P{  ˙      ;@ -@P{  ˙      ;@ -@`0P{  ˙      ;@ P.0@P{  ˙      ;@ -@P{  ˙      ;@ P.0@`0P{  ˙      ;@ P.0@P{  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@ Ą ˙      U< P/0@`0P{  ˙ 
   
  U<PŽ  ˙       ź@0P}PŽ  ˙       ź@0P} ­@  ˙       ź@0P}PŽ ? ˙ 
   
  U<PŽ  ˙      U< Ź@ ! ˙ 
   
  U<PŽ  ˙      ;@P­  ˙ 
   
  U<PŽ  ˙       Pď0TMPŽ  ˙ 
   
  U<PŽ  ˙      U< ­@  ˙ 
   
  U<PŽ  ˙       U;pPŽ  ˙       U;p`0 Ź@  ˙       U;p`0 Ť@  ˙       U;p`0P­  ˙       PTľp`0P­ L ˙       U;p`0P­  ˙       U;p`0 Ź@  ˙       U;p`0P­  ˙      :@`0P­  ˙      9@`0P­  ˙      :@`0P­  ˙      :@PŽ 	 ˙ 
   
  U<PŽ  ˙       P°p`R<p`J@0P­  ˙       @PŽp`R<p`J@0P­  ˙       @P­p`R<p`J@0P­  ˙       @PŹp`R<p`J@0P­  ˙       @PŤp`R<p`Ě@ {@0P­  ˙       @PŞp`R<p`Ě@ {@0P­  ˙       @ ~@P)p`R<p`Ě@ @ a@0P­  ˙       @ ~@P)p`0D@' PŃp`Ě@ @ a@0P­  ˙       8@T00@`0P{  ˙       Uk0@P{  ˙       Uk0@`0P{  ˙       Uk0@P{  ˙       Uk0@`0P{  ˙       Uk0@`0 y@  ˙       Uk0@`0P{  ˙       Q-T>0@`0P{  ˙       Uk0@`0P{  ˙       Uk0@`0 y@  ˙       Uk0@`0P{  ˙       @bPTÔp`P.0@`0P{  ˙       @b @TÓp`P.0@`0P{  ˙       @`PTÔp`P.0@`0P{  ˙       @^ @TŘp`P.0@`0P{  ˙       @\ @0TŮ P/0@`0P{  ˙       @\ @0TÚ P/0@`0P{  ˙       @Z @0TÚ P/0@`0P{  ˙       @`0TÜ P/0@`0P{  ˙       @VP @0TÜ P/0@`0P{  ˙       @V @ @0TÝ P/0@`0P{  ˙       @T @ @0TÝ P/0@`0P{  ˙       @R @ @0TÝ P/0@`0P{  ˙       @P @0Tă P/0@`0P{  ˙       @P @ @0Tä P/0@`0P{  ˙       @N @0Tä P/0@`0P{  ˙       @#0U P/0@P{  ˙       @#0U P/0@`0P{  ˙       @#0U P/0@P{  ˙       @0U P/0@P{  ˙       @0U* P/0@P{  ˙       @Ph0TÂ P/0@P{  ˙       @ h@0TÂ P/0@`0P{  ˙       @0U, P/0@P{  ˙       @90U P/0@`0P{  ˙       @C0Tů P/0@`0P{  ˙       @H0Tô P/0@`0P{  ˙       @0ń@"S P/0@`0P{  ˙       @0U6 P/0@ y@  ˙       @0U6 P/0@`0 y@  ˙       @0U9 P/0@`0 y@  ˙       @
0U2 P/0@P{  ˙       @
0U2 P/0@`0P{  ˙       @0U( P/0@P{  ˙       @0U! P/0@P{  ˙       @-0U P/0@`0P{  ˙       @<0U  P/0@`0P{  ˙       @50U P/0@`0P{  ˙       @10U P/0@`0P{  ˙       @)0U P/0@`0P{  ˙       @0U P/0@`0P{  ˙       @K0Tń P/0@`0P{  ˙       @F0Tö P/0@`0P{  ˙       @?0Tý P/0@`0P{  ˙       @0U# P/0@`0P{  ˙       @0U% P/0@P{  ˙       @0U0 P/0@P{  ˙       @M0Tď P/0@`0P{  ˙      U< P/0@`0 y@ 	 ˙      U< P/0@`0P{  ˙      ?@ RÝ P/0@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@  ˙      U< P/0@`0P{  ˙      U< P/0@`pPz  ˙      U< P/0@Pz  ˙      U< P/0@`pPz  ˙      U< P/0@Pz  ˙      U< P/0@`pPz  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`pPz 
 ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@ z@  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< P/0@P{ @ ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@ & ˙      U< P/0@`0P{  ˙      U< .@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@  ˙      U< P/0@`0 x@  ˙      U< P/0@`0PyP  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{ 	 ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`0 y@  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@P{ K ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@  ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@  ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      U< .@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< .@`0P{ ! ˙      U< P/0@`0P{  ˙      U< P/0@P{ | ˙      U< P/0@`0P{  ˙      @Q P/0@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0P{v@  ( ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@  ˙      U< P/0@`0P{  ˙      U< P/0@`0 z@ 1 ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@ 0 ˙      U< P/0@`0P{  ˙      U< .@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@`PB0P9 H ˙      U< P/0@`0P{  ˙      U< P/0@P{  ˙      U< P/0@`0P{  ˙      S@  @R! P/0@`0P{ . ˙      U< P/0@`0P{  ˙      U< .@`0P{  ˙      U< P/0@`0P{  ˙      (@T P/0@`0P{  ˙      @T
 P/0@`0P{  ˙      U< P/0@`0P{  ˙      e@PĂ P/0@`0P{ 
 ˙      U< P/0@`0P{ 	 ˙        Ż@0T P/0@`0P{  ˙        Ż@0 d@T	 P/0@`0P{  ˙      U< P/0@`0P{  ˙      \@PÂ P/0@`0P{ ă ˙      U< P/0@`0P{  ˙      U< .@`0P{  ˙      U< P/0@`0P{  ˙      U< P/0@`0 y@  ˙      U< P/0@`0P{  ˙      U< P/0@`0P{R  ˙      U< P/0@`0 y@R & ˙      U< P/0@`0P{R  ˙       Ö@0Pc P/0@`0P{R 	 ˙      U< P/0@`0P{R  ˙      U< P/0@`0 y@R  ˙      U< P/0@`0P{R  ˙      U< .@`0P{R 
 ˙      U< P/0@`0P{R  ˙      U< P/0@`0P{ 	 ˙      U< /@P|  ˙      U<P1P} m ˙ 
   
  U<PŽ  ˙      U< Ź@  ˙ 
   
  U<PŽ  ˙      U< Ź@  ˙ 
   
  U<PŽ  ˙      U< Ź@ V ˙ 
   
  U<PŽ  ˙      U< Ź@ . ˙ 
   
  U<PŽ  ˙       Rđ0RLPŽ  ˙       í@0RLPŽ ę ˙ 
   
  U<PŽ  ˙      U< PĽ0P	 	 ˙      U<  Ł@0P  ˙      U< PĽ0P	  ˙ 
   
  U<PŽ  ˙      U< P­p` * ˙ 
   
  U<PŽ  ˙      U< ­@  ˙ 
   
  U<PŽ  ˙      U< ­@  ˙ 
   
  U<PŽ  ˙      U< ­@  ˙ 
   
  U<PŽ  ˙      U< Ź@  ˙ 
   
  U<PŽ  ˙      U< Ź@ a ˙ 
   
  U<PŽ  ˙      U<  F@!0PG  ˙      U< D@%PE  ˙      U<  F@!0PG  ˙ 
   
  U<PŽ  ˙      U<PŹP  ˙      U< Ť@  ˙      U<PŹP  ˙      U< Ť@  ˙      U<PŹP  ˙      U< Ť@  ˙      U<PŹP  ˙      U< Ť@  ˙      U< Ź@  ˙ 
   
  U<PŽ  ˙      U< Ź@  ˙ 
   
  U<PŽ  ˙      U< Ź@  ˙      U< Ť@  ˙      U<PŹP  ˙      U< Ť@  ˙      U<PŹP  ˙      U< Ť@  ˙      U<PŹP  ˙      U< Ť@  ˙      U<PŹP  ˙ 
   
  U<PŽ  ˙      RďRMPŽ  ˙      PąR>RMPŽ  ˙      PąR> RLp`0P­  ˙       P°pR>`RKp`0P­  ˙      Pą R=p`RKp`0P­  ˙       @Ud0@`0P{  ˙        @UZ0@`0P{  ˙       @
Ua0@`0P{  ˙       @U]0@`0P{  ˙        @UZ0@`0P{  ˙       @UZ0@`0P{  ˙        
@Ü@Su0@`0P{  ˙        
@Ý@Sv0@`0P{  ˙        @UX0@`0P{  ˙        @UX0@`0P{  ˙        @	Ü@Su0@`0P{  ˙        
@UU0@`0P{  ˙        @UU0@`0P{  ˙        @	UQ0@`0P{  ˙        @UQ0@`0P{  ˙        @UL0@`0P{  ˙        @U?0@P{  ˙        @UG0@`0P{  ˙        @UG0@`0P{  ˙        @0>@`0P{  ˙        @UC0@`0P{  ˙        #@U80@P{  ˙        @U?0@P{  ˙        @U?0@`0P{  ˙        (@U60@P{  ˙        @U:0@P{  ˙        *@U00@P{  ˙        (@U80@P{  ˙        #@U80@`0P{  ˙        (@U60@`0P{  ˙        (@U60@P{  ˙        2@U+0@P{  ˙        2@U+0@P{  ˙        ,@U00@P{  ˙        7@U%0@`0P{  ˙        7@	U+0@`0P{  ˙        2@U+0@`0P{  ˙        ,@U+0@P{  ˙        @ @U!0@`0 y@  ˙        =@U!0@`0 y@  ˙        =@	U%0@ y@  ˙        =@	U%0@`0P{  ˙        7@U%0@P{  ˙        @ @U0@`0 y@  ˙        @ @	U!0@`0P{  ˙        @	 @U!0@`0 y@  ˙        @ @	U0@`0P{  ˙        @ @	U0@`0P{  ˙        @ @U0@`0P{  ˙        @ @	U0@`0P{  ˙        @ @U0@`0P{  ˙        @ @U0@`0P{  ˙        #@ @U0@`0P{  ˙        #@ @U0@`0P{  ˙        !@ @U0@`0P{  ˙        +@ @U0@`0P{  ˙        +@ @U0@`0P{  ˙        )@ @	U0@`0P{  ˙        &@ @U0@`0P{  ˙        /@ @U0@`0 z@  ˙        -@ @U0@`0 z@  ˙        +@ @	U0@`0 z@  ˙        )@ @U0@`0P{  ˙        -@ @U0@`0 z@  ˙        5@ @Tţ0@`0P{  ˙        1@ @U0@`0P{  ˙        1@ @U0@`0P{  ˙        /@ @U0@`0 z@  ˙        ;@ @Tú0@`0P{  ˙        1@ @U0@`0P{  ˙        8@ @Tţ0@`0P{  ˙        3@ @Tţ0@`0P{  ˙        @@ @	Tú0@`0P{  ˙        =@ @	Tú0@`0P{  ˙        =@ @	Tú0@`0P{  ˙        ;@ @Tú0@`0P{  ˙       @ h@	0TČ P/0@`0 y@  ˙       @ i@	0TČ P/0@`0 y@  ˙        k@	0TČ P/0@`0P{  ˙        B@	  @	0TČ P/0@`0P{  ˙        @@  @	T÷0@`0P{  ˙        @@ @T÷0@`0P{