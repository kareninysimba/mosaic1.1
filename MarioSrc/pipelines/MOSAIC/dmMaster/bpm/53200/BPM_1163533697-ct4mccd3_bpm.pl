  ŚV  0s  +$TITLE = "Domeflats (afternoon) filter1 = B"
$CTIME = 847979291
$MTIME = 847979600
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-14T20:02:49' / Date FITS file was generated
IRAF-TLM= '13:02:56 (14/11/2006)' / Time of last modification
OBJECT  = 'Domeflats (afternoon) filter1 = B' / Name of the object observed
FILENAME= 'dflat011'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '20:01:49.85'        / RA of observation (hr)
DEC     = '20:39:35.79'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-12-08T19:33:49.3' / Date of observation start (UTC approximate)
TIME-OBS= '19:33:49.302'       / Time of observation start
MJD-OBS =       53347.81515396 / MJD of observation start
MJDHDR  =       53347.81507176 / MJD of header creation
LSTHDR  = '20:02:03.4        ' / LST of header creation

OBSERVAT= 'CTIO    '           / Observatory
TELESCOP= 'CTIO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2000.0 / Equinox of tel coords
TELRA   = '20:01:49.85       ' / RA of telescope (hr)
TELDEC  = '20:39:35.8        ' / DEC of telescope (deg)
HA      = '00:00:01.1        ' / hour angle (H:M:S)
ZD      = '50.8              ' / Zenith distance
AIRMASS =                1.581 / Airmass
TELFOCUS= '16071             ' / Telescope focus
CORRCTOR= 'Blanco Corrector'   / Corrector Identification
ADC     = 'Blanco ADC'         / ADC Identification

DETECTOR= 'Mosaic2 '           / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                   16 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.270 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.270 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    3 / Filter position
FILTER  = 'B Harris c6002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -1.6010000000000E+00 / North TV Camera focus position
TV2FOC  = -1.9970000000000E+00 / South Camera focus position
ENVTEM  =  2.0900000000000E+01 / Ambient temperature (C)
DEWAR   = 'Mosaic2 Dewar'      / Dewar identification
DEWTEM  =  0.0000000000000E+00 / Dewar temperature (C)
DEWTEM2 = -4.9799999000000E+01 / Fill Neck temperature (C)
DEWTEM3 = 'N2  90.6'
CCDTEM  = -9.4000000000000E+01 / CCD temperature (C)
CCDTEM2 = 'CCD 169.8'

WEATDATE= 'Dec 08 19:31:02 2004' / Date and time of last update
WINDSPD = '4.7     '           / Wind speed (mph)
WINDDIR = '248     '           / Wind direction (degrees)
AMBTEMP = '23.2    '           / Ambient temperature (degrees C)
HUMIDITY= '24      '           / Ambient relative humidity (percent)
PRESSURE= '782     '           / Barometric pressure (millibars)
DIMMSEE = 'mysql_query():Unknown Error seeing=column' / Tololo DIMM seeing

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '13July99ver7_30'    / Controller software version
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Wed Dec  8 15:51:13 2004' / Date waveforms last compiled

OBSERVER= 'ESSENCE/SuperMACHO' / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'ct4m.20041208T193349' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/ctio/4meter/caldir/Mosaic2/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic2_xtalk040501.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =               135390  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



























































































































DTSITE  = 'ct                '  /  observatory location
DTTELESC= 'ct4m              '  /  telescope identifier
DTINSTRU= 'optic             '  /  instrument identifier
DTCALDAT= '2004-12-08        '  /  calendar date from observing schedule
DTPUBDAT= 'none              '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= 'noao              '  /  observing proposal ID
DTPI    = 'Christopher Stubbs'  /  Principal Investigator
DTPIAFFL= 'University of Washington'  /  PI affiliation
DTTITLE = 'A Next Generation Microlensing Survey of the LMC'  /  title of observ
DTACQUIS= 'ctioa8.ctio.noao.edu'  /  host name of data acquisition computer
DTACCOUN= 'mosaic            '  /  observing account name
DTACQNAM= '/ua80/mosaic/tonight/dflat011.fits'  /  file name supplied at telesco
DTNSANAM= 'ct135390.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041208/ct4m/noao/ct135390.fits.gz'
UPARM   = 'MarioCal$/Mosaic2_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M2_53166'
IMAGEID =                    5 / Image identification
EXPTIME =               35.000 / Exposure time in secs
DARKTIME=               43.818 / Total elapsed time in secs

CCDNAME = 'SITe #98261FACR06-02 (NOAO 28)' / CCD name
AMPNAME = 'SITe #98261FACR06-02 (NOAO 28), lower left (Amp11)' / Amplifier name
GAIN    =                  2.3 / gain expected for amp 211 (e-/ADU)
RDNOISE =                  7.9 / read noise expected for amp 211 (e-)
SATURATE=                27000 / Maximum good data value (ADU)
CONHWV  = 'ACEB002_AMP11'      / Controller hardware version
ARCONG  =                  2.3 / gain expected for amp 211 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,1:4096]'    / Amplifier section
DETSEC  = '[4097:6144,1:4096]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                   1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =                   0. / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                4096. / CCD to detector transformation
DTV2    =                   0. / CCD to detector transformation

WCSASTRM= 'ct4m.20020927T000014 (USNO N B Harris c6002) by F. Valdes 2002-10-24'
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =            300.45771 / Coordinate reference value
CRVAL2  =            20.659942 / Coordinate reference value
CRPIX1  =           -85.287953 / Coordinate reference pixel
CRPIX2  =            4127.2807 / Coordinate reference pixel
CD1_1   =        2.0483566e-07 / Coordinate matrix
CD2_1   =         7.442412e-05 / Coordinate matrix
CD1_2   =        7.4101696e-05 / Coordinate matrix
CD2_2   =        1.8213053e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. -0.3057467218389723 -0.001'
WAT1_002= '880976770565168 0.005670383336271377 0.1587623819673878 2.2186025026'
WAT1_003= '26981E-4 -2.136274059671448E-5 6.056955310638465E-4 -1.0771481894103'
WAT1_004= '95E-4 -1.469933641040793E-5 -2.045777530432725E-4 -1.826141220211157'
WAT1_005= 'E-5 4.904575138571058E-5 -6.59973152170311E-5 -5.36688946005139E-6 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. -0.3057467218389723 -0.00'
WAT2_002= '1880976770565168 0.005670383336271377 0.1587623819673878 -6.07229855'
WAT2_003= '6025636E-5 1.349931915234808E-5 -9.962270335027458E-5 7.137215117009'
WAT2_004= '710E-6 -9.748241484291803E-6 4.190418440564275E-4 -9.090823486414494'
WAT2_005= 'E-5 -8.31892496438167E-5 1.430283429468086E-5 -7.87961183596274E-6 "'

CHECKSUM= '5gaHAgVF6gaFAgUF'    /  ASCII 1's complement checksum
DATASUM = '824561188 '          /  checksum of data records
XTALKCOR= 'Oct 23 22:51 Crosstalk is 1.70E-4*im06+0.00123*im07+0.00211*im08'
ZERO    = 'MarioCal$/UBVRI_C20041208_1161662005-ct4m20041208T190047Z-ccd3.fits'
CALOPS  = 'XOTZFWSBP'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             1498.205 / min in overscan region
DQOVMAX =             1501.965 / max in overscan region
DQOVMEAN=             1500.276 / mean in overscan region
DQOVSIG =             3.195452 / sigma in overscan region
DQOVSMEA=             1500.281 / mean in collapsed overscan strip
DQOVSSIG=            0.5120155 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 23:27 Trim is [25:1048,1:4096]'
OVERSCAN= 'Oct 23 23:27 Overscan is [1063:1112,1:4096], function=minmax'
ZEROCOR = 'Oct 23 23:27 Zero is MarioCal$/UBVRI_C20041208_1161662005-ct4m200412'
CCDPROC = 'Oct 23 23:27 CCD processing done'
PROCID  = 'ct4m.20041208T193349V5'
OVSCNMTD=                    3
AMPMERGE= 'Oct 23 23:28 Merged 2 amps'
DQGLFSAT=          1.192093E-6
DQGLMEAN=             9778.148
DQGLSIG =             730.9647
DQDFCRAT=             279.3757
DQDFGLME=             9776.513 / DQ global mean of zero frame
DQDFGLSI=             733.7614 / DQ global sigma of zero frame
DQDFXMEA=             9776.512 / DQ mean of collapsed x bias
DQDFXSIG=             702.3112 / DQ sigma of collapsed x bias
DQDFXSLP=             1.080463 / DQ slope of collapsed x bias
DQDFXCEF=            0.9097573 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             9776.513 / DQ mean of collapsed y bias
DQDFYSIG=             172.4148 / DQ sigma of collapsed y bias
DQDFYSLP=           0.09288919 / DQ slope of collapsed y bias
DQDFYCEF=            0.6371078 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  708
DQADNUSD=                    9
DQADNREJ=                    0
DQADGLME=             7134.353 / DQ global mean of zero frame
DQADGLSI=             532.6181 / DQ global sigma of zero frame
DQADXMEA=             7134.353 / DQ mean of collapsed x bias
DQADXSIG=             513.0676 / DQ sigma of collapsed x bias
DQADXSLP=            0.7895502 / DQ slope of collapsed x bias
DQADXCEF=            0.9100192 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             7134.353 / DQ mean of collapsed y bias
DQADYSIG=             116.0665 / DQ sigma of collapsed y bias
DQADYSLP=           0.05518638 / DQ slope of collapsed y bias
DQADYCEF=            0.5622732 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
DCCDMEAN=            0.8646759
GAINMEAN=                 2.55
PROCID01= 'ct4m.20041208T193349V4'
PROCID02= 'ct4m.20041208T202848V4'
PROCID03= 'ct4m.20041208T201149V4'
PROCID04= 'ct4m.20041208T195415V4'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53347.82
MJDMAX  =             53740.88
OBJMASK = 'bp2mkbpm6.pl'
    Ţ                          
  
     }  +      ˙˙˙               ˙}      ``
``````C`'R (@6´P"&ż R@  9@ 
 @"&P`
 @y)@``9 @ P" @ P`
` 
P+ 
PK'0 *@w& PA 
P``'  @6űP 
 @  M@ P 
P  
P& @5ýP  
P 
P%ô @  @uő``` @%Ó @ 
@uŇ PD 
PM 
P  
P' 
P$%Š@u&3 @6)P&4 @6*PY`
`
 P 
P 
P 
P`
`` 
P  
P 
PM 
P` P 
P
% @5Pn 
P` 
 	@ P`
%\ @5QP` 
Pb` 
P 
P2 
P @` P 
P 
P: 
PZ 
P-`` P 
P, 
P 
P`
 P' 
PB`` 
Pz``````` P`
#ő 9@3ę @  @ P< 
PŠ$ÍP$ @p! @p#ppt @dpti``` P* 
PU 
P- 
P 
P 
P`` P8``` P 
@` PZ 
P 
 [@ P 
P 
P*!ö@1ě@ 
@-"˘@ @r¤"ą@ @ @ @ @rÝ``"Í@1@. @ @+qÁ``!ą@qŁ!°@nq˘!Ż@1Ą@G"@ @r"@r"@ @1@q!@1@H` @ @ @ @` @ @`` @` ˙         ˙ 
   
   Gű   ˙        Gů   ˙        Gó 	  ˙        GęP   ˙        Gĺ   ˙        GÓ @   ˙ C   C    h@PPPPZ @P @ @PP @  @ @ @ @	 @ @ @PP @ @P @ @ @ @ @
 @ @ @ @ Eó   ˙       \@ a@ @ @ @ @ @ @ 
@ @ DŽ #  ˙       S[ <@ Y@ @P1 <@ @ @P, @ @P @ @ @
P @PPPP @ @ @P @ @P @ @ @ @ @ @	 	@P @ @ @ @ @ @ @P @ @ @ @ @P @P @P @ @PP @ @PPPP	PPP @ @P @ @ 	@PP @ @ @P @ @ @P @ @ @P @ @ @ L  ˙        ź@B  ˙       @ä  ˙       @j  ˙       Ú@$@ ý  ˙       Ú@#@ ý  ˙ 
   
   Rn  ˙       @m  ˙ 
   
   Rm  ˙ 
   
   VÝ#  ˙ 
   
   Qó  ˙       @ó  ˙ 
   
   Pc  ˙       |@  ˙       {@  ˙       |@  ˙ 
   
   SŢ"  ˙       "@Ű  ˙        @ Ý  ˙ 
   
   Q:Ć  ˙ 
   
   SÔ,  ˙ 
   
   W í  ˙ 
   
   SźD  ˙ 
   
   S~  ˙ 
   
   RVŞ  ˙       @{  ˙       @{  ˙       @{  ˙       T¸@ ź  ˙       A@ ź  ˙       Qď@x  ˙ 
   
   RFş  ˙ 
   
   Sé  ˙ 
   
   S\¤  ˙ 
   
   P  ˙       H@ ´  ˙ 
   
   WK ľ  ˙ 
   
   Sn  ˙ 
   
   UWŠ  ˙ 
   
   Sß!  ˙       Ý@!  ˙ 
   
   S=Ă  ˙ 
   
   Sw  ˙ 
   
   SH¸  ˙ 
   
   Sk  ˙       @j  ˙       @j  ˙ 
   
   Sk  ˙ 
   
   Tr  ˙ 
   
   VQŻ  ˙ 
   
   U¤\  ˙       Pw@ p  ˙       @ p  ˙ 
   
   SĚ4  ˙ 
   
   T^˘  ˙ 
   
   PĄ_  ˙       PťUwÎ  ˙ 
   
   V2Î  ˙       ü@  ˙ 
   
   Rg  ˙       e@  ˙       2@Ě  ˙ 
   
   T4Ě  ˙       {@  ˙ 
   
   R|  ˙ 
   
   W v  ˙ 
   
   SÝ#  ˙       y@  ˙       x@  ˙       y@  ˙ 
   
   PBž  ˙ 
   
   V)×  ˙ 
   
   PžB  ˙ 
   
   UłM  ˙       T@ Ş  ˙       T@ Š  ˙       T@ Ş  ˙ 
   
   PÜ$  ˙ 
   
   Vk  ˙ 
   
   Tä  ˙ 
   
   V§Y  ˙       QDUcY  ˙ 
   
   UżA  ˙       ę@  ˙       é@  ˙ 
   
   Vë  ˙       ¨@U  ˙       ¨@R  ˙       ¨@Q  ˙       §@
O  ˙       ¨@
N  ˙       Ť@N  ˙       ­@O  ˙       Ż@N  ˙ 
   
   Qń  ˙       đ@  ˙       @ y  ˙        &@Ř  ˙ 
   
   P´L  ˙ 
   
   W h  ˙       ¤@ P  ˙       ¤@ P  ˙       §@ R  ˙       ¨@ R  ˙       Š@ R  ˙       Ş@ T  ˙ 
   
   Wb   ˙ 
   
   PŠW  ˙ 
   
   R5Ë  ˙ 
   
   P¸H  ˙ 
   
   Tá  ˙       @ß  ˙       @Ţ  ˙       @ß  ˙       @j  ˙       @i  ˙       @i  ˙       @j  ˙ 
   
   S?Á  ˙       >@ż  ˙       ?@ż  ˙ 
   
   PY§  ˙ 
   
   T9Ç  ˙        ů@  ˙ 
   
   Vĺ  ˙ 
   
   WO ą  ˙ 
   
  Y@Ľ  ˙ 
   
  W@Ľ - ˙       RW0@`¤  ˙       RW0@`Qz*  ˙       PR?0@`¤  ˙        @R>0@`¤  ˙        @R?0@`¤  ˙       QŻP¨0@`¤  ˙       Ž@P§0@`¤  ˙       QŻP¨0@`¤  ˙       RW0@`Qg=  ˙       RW0@`R8l G ˙       RW0@`¤  ˙      V@`Q!  ˙       RW0@`{@'  ˙       RW0@`P/u H ˙       RW0@`¤  ˙       RW0@`U( |  ˙       RW0@`¤  ˙       RW0@`Ň@ Ď  ˙       RW0@`¤  ˙       RW0@`@  ˙      V@`@  ˙       RW0@`¤  ˙       RW0@¤  ˙       RW0@`¤  ˙       RW0@¤  ˙       RW0@ §@ ú  ˙       RW0@@ @ ú  ˙       Ô@*  ˙       @ ú  ˙ 
   
   Qâ  ˙       <@Á  ˙       â@  ˙       @|  ˙       @}  ˙ 
   
   Pú  ˙       @x  ˙       H@ ľ  ˙ 
   
  W@Ľ . ˙       RW0@`¤  ˙       <@P0@`¤ + ˙       RW0@`¤  ˙       RW0@`¤ n ˙       RW0@`¤  ˙       RW0@`¤  ˙       RW0@`¤  ˙       RW0@`¤  ˙       Ř@&  ˙       s@  ˙       ş@C  ˙       ¨@U  ˙       @^  ˙        {@  ˙ 
   
   Q `  ˙       RW0@`M@U  ˙       RW0@`¤  ˙       RW0@`¤  ˙       RW0@`%@|  ˙       RW0@`$@|  ˙       RW0@`%@|  ˙       RW0@`¤  ˙       RW0@`¤  ˙       RW0@` @  ˙       RW0@`¤  ˙       RW0@`¤  ˙       RW0@`{@&  ˙       RW0@`¤  ˙       Ś@	 Q  ˙       Ś@
 P  ˙       ¤@ P  ˙       ¤@ O  ˙       ¤@ @ O  ˙       Ź@ N  ˙       Ź@ Q  ˙       Ť@ O  ˙       Ť@ N  ˙ 
   
   Vé  ˙       @ć  ˙       @	ĺ  ˙       @	ĺ  ˙       @ć  ˙ 
   
   Vé