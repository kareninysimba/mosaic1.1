  ŚV  /H  $TITLE = "Dflats  filter1 = B"
$CTIME = 849876625
$MTIME = 849876919
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
DATAMIN =           0.000000E0 / Minimum data value
DATAMAX =           0.000000E0 / Maximum data value
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
DATE    = '2006-11-10T23:35:47' / Date FITS file was generated
IRAF-TLM= '16:35:54 (10/11/2006)' / Time of last modification
OBJECT  = 'Dflats  filter1 = B' / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat007'           / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '16:25:58.92'        / RA of observation (hr)
DEC     = '29:20:15.82'        / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2004-10-08T22:39:54.2' / Date of observation start (UTC approximate)
TIME-OBS= '22:39:54.2'         / Time of observation start
MJD-OBS =       53286.94437731 / MJD of observation start
MJDHDR  =       53286.94436343 / MJD of header creation
LSTHDR  = '16:25:05          ' / LST of header creation

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2004.8 / Equinox of tel coords
TELRA   = '16:26:10.38       ' / RA of telescope (hr)
TELDEC  = '29:19:37.5        ' / DEC of telescope (deg)
ZD      = '2.6458            ' / Zenith distance
AIRMASS =                1.001 / Airmass
TELFOCUS=                -9020 / Telescope focus
CORRCTOR= 'Mayall Corrector'   / Corrector Identification
ADC     = 'Mayall ADC'         / ADC Identification
ADCSTAT = 'off               ' / ADC tracking state null-track-preset
ADCPAN1 =  0.0000000000000E+00 / ADC 1 prism angle
ADCPAN2 =  0.0000000000000E+00 / ADC 2 prism angle

DETECTOR= 'CCDMosaThin1'       / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.258 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.258 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    2 / Filter position
FILTER  = 'B Harris k1002    ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -4.0300000000000E-01 / North TV Camera focus position
TV2FOC  = -2.1000000000000E+00 / South Camera focus position
ENVTEM  =  1.9900000000000E+01 / Ambient temperature (C)
DEWAR   = 'CCDMosaThin1 Dewar' / Dewar identification
DEWTEM  = -1.6839999400000E+02 / Dewar temperature (C)
DEWTEM2 =  1.2000000000000E+00 / Fill Neck temperature (C)
DEWTEM3 = 'N2   0.0'
CCDTEM  = -9.9400002000000E+01 / CCD temperature  (C)
CCDTEM2 = 'CCD   0.0'

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '2.000  13Feb96     (add mode and group to hdrs)' / Controller softwar
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Fri Oct  8 14:16:41 2004' / Date waveforms last compiled

OBSERVER= 'Schweiker'          / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'kp4m.20041008T223954' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaicsrc.tcl (Nov02)' / Image creation software vers
KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/kpno/4meter/caldir/' / OTF calibration directory
XTALKFIL= 'MarioCal$/Mosaic1_xtalk030301.txt' / Crosstalk file
CHECKVER= 'COMPLEMENT'          /  FITS checksum version
RECNO   =                63009  /  NOAO Science Archive sequence number
ARCONGI =                    2 / Gain selection (index into Gain Table)



































































































































DTSITE  = 'kp                '  /  observatory location
DTTELESC= 'kp4m              '  /  telescope identifier
DTINSTRU= 'mosaic_1          '  /  instrument identifier
DTCALDAT= '2004-10-08        '  /  calendar date from observing schedule
DTPUBDAT= '2006-04-09        '  /  calendar date of public release
DTOBSERV= 'NOAO              '  /  scheduling institution
DTPROPID= '2004B-0321        '  /  observing proposal ID
DTPI    = 'Arlin Crotts      '  /  Principal Investigator
DTPIAFFL= 'Columbia University'  /  PI affiliation
DTTITLE = 'Microlensing in M31 at Large Distances and for Large Masses'  /  titl
DTACQUIS= 'tan.kpno.noao.edu '  /  host name of data acquisition computer
DTACCOUN= 'lp                '  /  observing account name
DTACQNAM= '/md1/4meter/dflat007.fits'  /  file name supplied at telescope
DTNSANAM= 'kp063009.fits     '  /  file name in NOAO Science Archive
DTCOPYRI= 'AURA              '  /  copyright holder of data
ENID    = '20041008/kp4m/2004B-0321/kp063009.fits.gz'
UPARM   = 'MarioCal$/CCDMosaThin1_uparm_030915'
PIPEDATA= 'MarioCal$/PipeData_M1_50000'
IMAGEID =                    6 / Image identification
EXPTIME =               18.000 / Exposure time in secs
DARKTIME=               19.490 / Total elapsed time in secs

CCDNAME = 'SITe #7233FBR09-02 (NOAO 09)' / CCD name
AMPNAME = 'SITe #7233FBR09-02 (NOAO 09), upper left (Amp23)' / Amplifier name
GAIN    =                  2.8 / gain expected for amp 323 (e-/ADU)
RDNOISE =                  5.3 / read noise expected for amp 323 (e-)
SATURATE=                30000 / Maximum good data value (ADU)
CONHWV  = 'ACEB003_AMP23'      / Controller hardware version
ARCONG  =                  2.8 / gain expected for amp 323 (e-/ADU)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,4096:1]'    / Amplifier section
DETSEC  = '[2049:4096,4097:8192]' / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                  -1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =               4097.0 / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                2048. / CCD to detector transformation
DTV2    =                4096. / CCD to detector transformation

WCSASTRM= 'kp4m.19981012T035510 (Tr 37 B) by L. Davis 19981013' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =             246.4955 / Coordinate reference value
CRVAL2  =            29.337728 / Coordinate reference value
CRPIX1  =            2069.3434 / Coordinate reference pixel
CRPIX2  =           -46.454409 / Coordinate reference pixel
CD1_1   =       -2.2512021e-07 / Coordinate matrix
CD2_1   =       -7.2337511e-05 / Coordinate matrix
CD1_2   =       -7.1990279e-05 / Coordinate matrix
CD2_2   =          2.67093e-08 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 2. -0.2982116455254548 -0.002'
WAT1_002= '950630261811555 0.00154519927216234 0.1497294592763834 2.36351129400'
WAT1_003= '2921E-4 -1.611011490899461E-5 5.427767310797452E-4 -9.80504117355443'
WAT1_004= '5E-5 7.162246317750486E-5 -1.542555741553624E-4 -3.286360524487164E-'
WAT1_005= '6 4.947126639310378E-5 -6.152945439784223E-5 -3.322021688749016E-6"'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 2. -0.2982116455254548 -0.00'
WAT2_002= '2950630261811555 0.00154519927216234 0.1497294592763834 -6.834194238'
WAT2_003= '559837E-5 1.685807046404099E-5 -8.795979007348174E-5 2.1075698487810'
WAT2_004= '46E-6 -6.100909275006465E-5 3.663506954563162E-4 -8.719178534055199E'
WAT2_005= '-5 -6.391387908349464E-5 7.414387422612762E-7 -1.245865036450428E-5"'

CHECKSUM= '1pMc4oMc1oMc1oMc'    /  ASCII 1's complement checksum
DATASUM = '1649225637'          /  checksum of data records
XTALKCOR= 'Oct 23 16:49 Crosstalk is 0.00175*im05'
ZERO    = 'MarioCal$/UBVRI_K20041008_1161645623-kp4m20041008T235941Z-ccd6.fits'
CALOPS  = 'XOTZFWSBPG'
PHOTFILT= 'B       '
BANDTYPE= 'broad   '
DQOVMLL =                   0. / length of longest jump
DQOVMLJ =             2.084816 / amplitude of longest jump
DQOVMJL =                   0. / length of highest jump
DQOVMJJ =                   0. / amplitude of highest jump
DQOVMLLR=                   0. / length of longest jump down
DQOVMLJR=         2.802597E-44 / depth of longest jump down
DQOVMJLR=                   0. / length of deepest jump down
DQOVMJJR=                   0. / amplitude of deepest jump
DQOVJPRB=                   0. / probability of a jump
DQOVMIN =             2549.939 / min in overscan region
DQOVMAX =              2554.04 / max in overscan region
DQOVMEAN=             2552.206 / mean in overscan region
DQOVSIG =             2.143053 / sigma in overscan region
DQOVSMEA=             2552.206 / mean in collapsed overscan strip
DQOVSSIG=            0.8716116 / sigma in collapsed overscan strip
TRIM    = 'Oct 23 17:02 Trim is [25:2072,1:4096]'
OVERSCAN= 'Oct 23 17:02 Overscan is [2087:2136,1:4096], function=minmax'
ZEROCOR = 'Oct 23 17:02 Zero is MarioCal$/UBVRI_K20041008_1161645623-kp4m200410'
CCDPROC = 'Oct 23 17:02 CCD processing done'
PROCID  = 'kp4m.20041008T223954V4'
OVSCNMTD=                    3
DQGLFSAT=                   0.
DQGLMEAN=             7826.255
DQGLSIG =             268.6418
DQDFCRAT=             434.7919
DQDFGLME=             7832.263 / DQ global mean of zero frame
DQDFGLSI=             391.9427 / DQ global sigma of zero frame
DQDFXMEA=             7832.263 / DQ mean of collapsed x bias
DQDFXSIG=             244.8651 / DQ sigma of collapsed x bias
DQDFXSLP=          -0.07296797 / DQ slope of collapsed x bias
DQDFXCEF=            0.1762182 / DQ correlation coeffecient of collapsed x bias
DQDFYMEA=             7832.263 / DQ mean of collapsed y bias
DQDFYSIG=             198.7333 / DQ sigma of collapsed y bias
DQDFYSLP=            0.1270409 / DQ slope of collapsed y bias
DQDFYCEF=            0.7559538 / DQ correlation coeffecient of collapsed y bias
NCOMBINE=                  356
DQADNUSD=                    7
DQADNREJ=                    0
DQADGLME=             7715.977 / DQ global mean of zero frame
DQADGLSI=             383.5801 / DQ global sigma of zero frame
DQADXMEA=             7715.977 / DQ mean of collapsed x bias
DQADXSIG=              239.793 / DQ sigma of collapsed x bias
DQADXSLP=           -0.0716671 / DQ slope of collapsed x bias
DQADXCEF=            0.1767375 / DQ correlation coeffecient of collapsed x bias
DQADYMEA=             7715.977 / DQ mean of collapsed y bias
DQADYSIG=             195.5468 / DQ sigma of collapsed y bias
DQADYSLP=            0.1249965 / DQ slope of collapsed y bias
DQADYCEF=            0.7559088 / DQ correlation coeffecient of collapsed y bias
POBSTYPE= 'dflat   '
PROCTYPE= 'MasterCal'
PUPILCOR= 'Oct 23 17:04 maximum amplitude = 1.020E-5'
DCCDMEAN=            0.9063694
GAINMEAN=                 2.75
PROCID01= 'kp4m.20041008T223954V3'
PROCID02= 'kp4m.20041008T233852V3'
PROCID03= 'kp4m.20041008T231927V3'
PROCID04= 'kp4m.20041008T230002V3'
IMCMB001= 'BP2CMB1 '
IMCMB002= 'BP2CMB2 '
IMCMB003= 'BP2CMB3 '
IMCMB004= 'BP2CMB4 '
MJDMIN  =             53286.95
MJDMAX  =             53853.01
OBJMASK = 'bp2mkbpm6.pl'
   Ţ                                 R        ˙˙˙               ˙R      ``$gţ @ @ @$p} @ąpz @lx3(A@x2(B@ @8C@``(3@ @83@
` @`(@ @ @	x'(7@ńx((6@x%(7@8%@(3@Gx%(3@ą @80@u(B@ @ËxF````(@	q!@\ @ A @
x6 @z(:@Bx,(;@ @ @ @ @xh(w@ @ @x(@ @} @ @<xą(żAxŽ(ź@xŽ @f" @y @ @xť(Ě@xź(Č@xš(Ĺ@?xś(Â@*xł``(¤@ @
 @ @ @ @xŇ``(Ä@xś$5@ @ @tC$O@tD$N@O 
@ @ 
@ @+tp$z@ @tz$@e 
@ @	 
@t$§@t$Ť@ @ 
@ @< 
@tĚ$Ö@tÉ`
`
`
`
`
 
@`
`
`
`
 
@ 
P#`
 @ 
P
 
P, @  @  @ P	 
 	@'@	w{'@	wx'@ 
@ @w'@w'@w'@w``'n@1w^ @'[@ @7_@'i@7[@`
 @'M@w@'J@k 
@ @ 
@ @wj``'U@ @wS'a@wP'b@wP```''@w``"`$`1`  ˙         ˙ $   $   @ @@ Ü@@ w@  Ą@ ś@ @PŤ0@ @
0 B@ y@  ˙        Í@ E@ ś@ @PŤ0@ @ @0 B@ @ @ @  ˙      Í@S;@ L@   ˙      Í@S<@ L@   ˙      Í@˙@<@ L@   ˙      Í@˙@;@ L@   ˙      Í@ @;@ L@  
 ˙      Í@ @;@ L@   ˙      Í@˙@;@ L@   ˙      Í@ @;@ L@   ˙      Í@ @`0:@ L@   ˙      Í@S<@ L@   ˙      Í@ P0&@ L@   ˙      Í@  @0%@ L@   ˙      Í@<@ L@   ˙      Í@=@ P0 E@  u ˙      Í@=@ L@   ˙      Í@ @0 ­@ L@   ˙      Í@ SP0 Ź@ L@   ˙      Í@ @0 Ť@ L@   ˙      Í@ @0 Ź@ L@   ˙      Í@ @0 Ź@ L@   ˙      Í@=@ L@  z ˙      Í@=@ L@   ˙      @Ë@=@ L@   ˙      @Ë@=@ L@   ˙      @Ë@=@ L@   ˙      Í@ R20@ L@   ˙      Í@=@ K@   ˙      Í@=@ K@   ˙      Í@=@ L@   ˙       6@0 Ô@ L@   ˙       Tď0@ L@   ˙       U°0]@ L@   ˙       Vą0 \@ L@   ˙       U%0č@ L@   ˙      @ L@   ˙       Tű0@ L@   ˙      @ @0 D@   ˙      @ @0 F@   ˙      @`0 G@   ˙      @ @ H@   ˙      W L@   ˙       Vé0 s@   ˙       SP0A@   ˙       Tä0x@   ˙ 
   
  \@   ˙       @P×0l@   ˙       Rđ0m@   ˙ 
   
  _@   ˙ 
   
  `@   ˙ 
   
  a@   ˙ 
   
  b@   ˙ 
   
  c@   ˙ 
   
  d@   ˙ 
   
  g@   ˙ 
   
  h@   ˙ 
   
  i@   ˙ 
   
  j@   ˙ 
   
  k@   ˙ 
   
  l@   ˙ 
   
   Qđ  ˙       ď@  ˙ 
   
   Qń  ˙ 
   
   R=Ă  ˙       đ@  ˙       ď@  ˙       č@  ˙        ł@J  ˙ 
   
   QS­  ˙ 	   	  W ń  ˙       QU0¸@ đ  ˙       Q0	@ đ  ˙       Q|0@ đ  ˙       QZ0ł@ đ  ˙       PQ0ź@ đ  ˙        @ @0Q@ đ  ˙        @ @0Q@ đ  ˙        @ @0Q@ đ  ˙       Pa0Ź@ đ  ˙ 
   
  @ đ  ˙        +@0ß@ đ  ˙ 
   
  @ đ  ˙        S@0ˇ@ đ  ˙       PU0¸@ đ  ˙       P0t@ đ  ˙       PG0P2RĂPmd@ đ  ˙       PG0P2 RÂpPmd@ đ  ˙      Py RÂpPmd@ đ  ˙      Py R`0PcPmR Ő@ đ  ˙      P-PL R`0PcPmR Ő@ đ  ˙      P-PLRĂPm`0R Ő@ đ  ˙      P-PLRĂ Plp`0R Ő@ đ  ˙      PPPLRĂ Plp`0R Ő@ đ  ˙      PPPL @0P! Plp`0R Ő@ đ  ˙      P Pp`0PK @0P! Plp`Rp`0 Ô@ đ  ˙ "   "  P PpPL @0P! Plp` @ č@ ˇ@Pdp`0 Ô@ đ  ˙ $   $  P Pp`0PK @P p Plp` @ č@ ˇ@Pdp`0 Ô@ đ  ˙ 1   1  P Pp`PJp` @ @ @PPPP )@Ŕ@P p`Pkp` @ j@ {@ ś@Pdp`0 Ô@ đ  ˙       Pp`Pp`P @ @ @ @p @ @ @ @ˇ @ @ @ @! @ @	 @P @ @ @ @ @P @P @ @ @ @P 
@ @P @PP @ @	 @ @PP @ @P @P	P
 @ E@  @P p` @PVp` @ j@ {@0P@P/P  E@Pdp`0 Ô@ đ  ˙ i   i    @ @PPP @P AP @HP @?P @) @	 @ @A @ @ @ @ @ @P @ @ @
PPP @P @ @	 @ @Pp` @ -@PP0@ Pu @ j@ {@0 4@o  @Pdp`0 @$ P)p`P î  ˙      @ @ H@   ˙      @ @ I@   ˙      @ L@   ˙ 
   
  \@  O ˙ 
   
  \@   ˙       T'05@   ˙ 
   
  \@   ˙       +@0.@  + ˙ 
   
  \@   ˙       @0A@   ˙ 
   
  \@  e ˙ 
   
  \@   ˙       :@0@  	 ˙ 
   
  \@   ˙       @0Á@   ˙       @0C@   ˙       @0C@   ˙ 
   
  \@   ˙       s@0ć@  < ˙ 
   
  \@   ˙ 
   
  ]@   ˙ 
   
  ]@   ˙       Q¨0%@=@ L@   ˙       Pň0Ű@=@ L@   ˙       P0˝@=@ L@   ˙       Q]0p@=@ L@   ˙      Í@ ˙@ @0 Ź@ @ @0 D@   ˙      Í@=@ L@   ˙      Í@ ú@0 @@ L@  $ ˙      Í@=@ L@  ą ˙      Í@=@ L@  l ˙      Í@=@ L@   ˙      Í@ @;@ L@   ˙      Í@ @;@ L@   ˙      Í@˙@;@ L@   ˙      Í@˙@;@ L@   ˙      Í@ @;@ L@   ˙      Í@ @`0:@ L@   ˙      Í@ S 0@`0:@ L@  	 ˙      Í@ @;@ L@  ń ˙      Í@=@ L@   ˙      Í@  @0%@ L@   ˙      Í@=@ L@  G ˙      Í@=@ L@  ą ˙      Í@=@ L@   ˙      Í@ @0 4@ L@   ˙      Í@ F@0ô@ L@  Ë ˙      Í@=@ L@  	 ˙      Í@=@ L@  \ ˙      Í@=@ L@   ˙       @0ź@=@ L@  ˙      Í@=@ L@  
 ˙      Í@  ×@0c@ L@  B ˙      @Ě@=@ L@   ˙      @Ě@=@ L@   ˙      @Ě@=@ L@   ˙      @Ě@=@ L@   ˙      @Ë@=@ L@   ˙      @Ě@=@ L@   ˙      @Ě@=@ L@   ˙      @Ë@=@ L@   ˙      @Ě@=@ L@   ˙      @Ě@=@ L@  } ˙      Í@=@ L@   ˙      Í@=@ L@  < ˙      Í@=@ L@  ˙      Í@=@ L@   ˙      Í@=@ L@  y ˙      Í@=@ L@   ˙      RĎ>@ L@   ˙      RĎ g@0 Ô@ L@   ˙      @ L@   ˙      @ L@  ? ˙      @ L@  * ˙      @ L@   ˙      @ L@  
 ˙      @ K@   ˙      @ J@   ˙      @P H@   ˙      @ H@   ˙      @  @0 D@   ˙      @ H@  	 ˙ 
   
  @ đ 	 ˙ 
   
  @ đ  ˙ 
   
  @ đ  ˙        w@0@ đ  ˙ 
   
  @ đ  ˙ 
   
  @ đ  ˙ 
   
  @ đ  ˙ 
   
  @ đ 1 ˙ 
   
  @ đ  ˙        z@0@ đ  ˙ 
   
  @ đ  ˙ 
   
  @ đ  ˙ 
   
  @ đ k ˙ 
   
  @ đ  ˙        @0w@ đ  ˙ 
   
  @ đ  ˙      Py@ đ  ˙      PyRĂPmd@ đ  ˙      PyRĂPmd@ đ  ˙      PyRĂPmR Ő@ đ  ˙      Py ^@0PbPmR Ő@ đ  ˙      P-PLRĂPmR Ő@ đ  ˙      PPPL @0P! Plp`Rp`0 Ô@ đ