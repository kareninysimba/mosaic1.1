  �V  �  �$TITLE = "Domeflat"
$CTIME = 830428772
$MTIME = 830428772
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
PCOUNT  =                 7756 / Heap size in bytes
GCOUNT  =                    1 / Only one group
EXTNAME = 'pl      '           / Extension name
ORIGIN  = 'NOAO-IRAF FITS Image Kernel December 2001' / FITS file originator
INHERIT =                    F / Inherits global header
DATE    = '2003-06-11T18:32:20'
IRAF-TLM= '14:40:01 (20/06/2003)'
OBJECT  = 'Domeflat'           / Name of the object observed
NEXTEND =                    8 / Number of extensions
FILENAME= 'dflat3018'          / Original host filename
OBSTYPE = 'dome flat'          / Observation type
PREFLASH=             0.000000 / Preflash time (sec)
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '18:01:17.00'        / RA of observation (hr)
DEC     = '20:41:4.10'         / DEC of observation (deg)

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2000-09-30T22:04:39.3' / Date of observation start (UTC approximate)
TIME-OBS= '22:04:39.3'         / Time of observation start
MJDHDR  =       51817.91991435 / MJD of header creation
LSTHDR  = '18:01:16.9        ' / LST of header creation

OBSERVAT= 'CTIO    '           / Observatory
TELESCOP= 'CTIO 4.0 meter telescope'
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2000.0 / Equinox of tel coords
TELRA   = '18:01:17.00       ' / RA of telescope (hr)
TELDEC  = '20:41:04.1        ' / DEC of telescope (deg)
HA      = '-00:00:01.9       ' / hour angle (H:M:S)
ZD      = '50.9              ' / Zenith distance
AIRMASS =                1.582 / Airmass
TELFOCUS= '14665             ' / Telescope focus
CORRCTOR= 'Blanco Corrector'   / Corrector Identification
ADC     = 'Blanco ADC'         / ADC Identification

DETECTOR= 'Mosaic2 '           / Detector
DETSIZE = '[1:8192,1:8192]'    / Detector size for DETSEC
NCCDS   =                    8 / Number of CCDs
NAMPS   =                    8 / Number of Amplifiers
PIXSIZE1=                  15. / Pixel size for axis 1 (microns)
PIXSIZE2=                  15. / Pixel size for axis 2 (microns)
PIXSCAL1=                0.270 / Pixel scale for axis 1 (arcsec/pixel)
PIXSCAL2=                0.270 / Pixel scale for axis 2 (arcsec/pixel)
RAPANGL =                   0. / Position angle of RA axis (deg)
DECPANGL=                  90. / Position angle of DEC axis (deg)
FILPOS  =                    3 / Filter position
FILTER  = 'V CTIO            ' / Filter name(s)
SHUTSTAT= 'dark              ' / Shutter status
TV1FOC  = -3.2000000000000E+00 / North TV Camera focus position
TV2FOC  = -2.0790000000000E+00 / South Camera focus position
ENVTEM  =  1.5000000000000E+01 / Ambient temperature (C)
DEWAR   = 'Mosaic2 Dewar'      / Dewar identification
DEWTEM  = -1.6410000600000E+02 / Dewar temperature (C)
DEWTEM2 = -5.5200001000000E+01 / Fill Neck temperature (C)
DEWTEM3 = 'N2  89.1'
CCDTEM  = -1.0080000300000E+02 / CCD temperature  (C)
CCDTEM2 = 'CCD 161.3'

WEATDATE= 'Sep 30 22:01:02 2000' / Date and time of last update
WINDSPD = '2.4     '           / Wind speed (mph)
WINDDIR = '44      '           / Wind direction (degrees)
AMBTEMP = '19.5    '           / Ambient temperature (degrees C)
HUMIDITY= '16      '           / Ambient relative humidity (percent)
PRESSURE= '784     '           / Barometric pressure (millibars)

CONTROLR= 'Mosaic Arcon'       / Controller identification
CONSWV  = '13July99ver7_30'    / Controller software version
AMPINTEG=                 3600 / (ns) Double Correlated Sample time
READTIME=                14400 / (ns) unbinned pixel read time
ARCONWD = 'Obs Sat Sep 30 17:16:42 2000' / Date waveforms last compiled
ARCONWM = 'OverlapXmit EarlyReset SplitShift ' / Waveform mode switches on
ARCONGI =                    2 / Gain selection (index into Gain Table)

OBSERVER= 'A. Clocchiatti, C. Smith' / Observer(s)
PROPOSER= '<unknown>'          / Proposer(s)
PROPOSAL= '<unknown>'          / Proposal title
PROPID  = '<unknown>'          / Proposal identification
OBSID   = 'ct4m.20000930T220439' / Observation ID

IMAGESWV= 'mosdca (Jun99), mosaic.tcl (Aug00)' / Image creation software version




KWDICT  = 'MosaicV1.dic (Sep97)' / Keyword dictionary

OTFDIR  = 'mscdb$noao/ctio/4meter/caldir/Mosaic2A/' / OTF calibration directory
XTALKFIL= 'mscdb$noao/Mosaic2/CAL0009/xtalkA0009' / Crosstalk file

CHECKSUM= '<unknown>'          / Header checksum
DATASUM = '<unknown>'          / Data checksum
CHECKVER= '<unknown>'          / Checksum version
RECNO   =                    0 / NOAO archive sequence number

EXPTIME =             18.12155 / Exposure time (sec)
DARKTIME=             26.94336 / Dark time (sec)
IMAGEID =                    1 / Image identification

CCDNAME = 'SITe #98173FABR14-02 (NOAO 26)' / CCD name
AMPNAME = 'SITe #98173FABR14-02 (NOAO 26), lower left (Amp11)' / Amplifier name
GAIN    =                  2.6 / gain expected for amp 111 (e-/ADU)
RDNOISE =                  6.1 / read noise expected for amp 111 (e-)
SATURATE=                48000 / Maximum good data value (ADU)
CONHWV  = 'ACEB001_AMP11'      / Controller hardware version
ARCONG  =                  2.6 / gain expected for amp 111 (e-/ADU)
ARCONRN =                  6.1 / read noise expected for amp 111 (e-)
CCDSIZE = '[1:2048,1:4096]'    / CCD size
CCDSUM  = '1 1     '           / CCD pixel summing
CCDSEC  = '[1:2048,1:4096]'    / CCD section
AMPSEC  = '[1:2048,1:4096]'    / Amplifier section
DETSEC  = '[1:2048,1:4096]'    / Detector section

ATM1_1  =                   1. / CCD to amplifier transformation
ATM2_2  =                   1. / CCD to amplifier transformation
ATV1    =                   0. / CCD to amplifier transformation
ATV2    =                   0. / CCD to amplifier transformation
LTM1_1  =                  1.0 / CCD to image transformation
LTM2_2  =                  1.0 / CCD to image transformation
DTM1_1  =                   1. / CCD to detector transformation
DTM2_2  =                   1. / CCD to detector transformation
DTV1    =                   0. / CCD to detector transformation
DTV2    =                   0. / CCD to detector transformation

WCSASTRM= 'ct4m.19990714T012701 (USNO-K V) by F. Valdes 1999-08-02' / WCS Source




EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =            270.32083 / Coordinate reference value
CRVAL2  =            20.684472 / Coordinate reference value
CRPIX1  =            4244.3258 / Coordinate reference pixel
CRPIX2  =            4304.2481 / Coordinate reference pixel
CD1_1   =       -6.8295807e-08 / Coordinate matrix
CD2_1   =        7.3313414e-05 / Coordinate matrix
CD1_2   =         7.374228e-05 / Coordinate matrix
CD2_2   =       -1.1927219e-06 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "3. 4. 4. 2. -0.3171856965643079 -0.015'




WAT1_002= '0652479325533 -0.3126038394350166 -0.1511955040928311 0.002318100364'




WAT1_003= '838772 0.01749134520424022 -0.01082784423020123 -0.1387962673564234 '




WAT1_004= '-4.307309762939804E-4 0.009069288008295441 0.002875265278754504 -0.0'




WAT1_005= '4487658756007625 -0.1058043162287004 -0.0686214765375767 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "3. 4. 4. 2. -0.3171856965643079 -0.01'




WAT2_002= '50652479325533 -0.3126038394350166 -0.1511955040928311 0.00553481957'




WAT2_003= '8784082 0.01258790793029932 0.01016780085575339 0.01541083298696018 '




WAT2_004= '0.03531979587941362 0.0150096457430599 -0.1086479352595234 0.0399806'




WAT2_005= '086902122 0.02341002785565408 -0.07773808393244387 "'
XTALKCOR= '03-Oct-2000 20:12:50 Crosstalk is im2 * 0.00119'
TRIM    = 'Oct  3 20:13 Trim is [25:2072,1:4096]'
OVERSCAN= 'Oct  3 20:13 Overscan is [2087:2136,1:4096], mean 1167.167'
CCDPROC = 'Oct  4  2:07 CCD processing done'
ZEROCOR = 'Oct  4  2:07 Zero is Zero[im1]'
NCOMBINE=                   21
ORIGBPM1= '0x010100.bpm.sm000000_1.pl'
ORIGBPM2= '0x020100.bpm.sm000000_2.pl'
    �                          �  �     �  �      ���               ���      #a@`	 
@`	 
@� 	@`
`	`
` 
B� 	@ @s�#�@F 	@ 
@ 	@ @	s� 
@#�@3�@`	#�@ 	@ 
@]s�``#�@s� @`	`
`	`
#�@s�`
``#�@(s�#�@ 	@ @s�#�@0s�#�@s�``	`
`	``#f@s[ @,#Y@sP#Y@sO#X@ 	@ @sb 
@`	 @`	#D@s: @#8@ @" 	@ 
@sM#V@
sK#T@ 	@ 
@-s]#f@3`	`
```` @ @ @` @ @ @`````` 
@
t:$C@t9$B@t7$B@t7$@@t6``$)@t$(@$t$&@t @`$@t`
`	#�@s�`#�@s�#�@s�#�@s�#�@ 	@ @s�`
`````#�@s�``#�@s�``#�@s� 
@ 	@#y@+sn``#a@)`	`
` 
@ @ 	@ 
@ 	@`	 
@ @`	`` @`	` 
@o`	 
@`	` @	` @ @T @ @`` @	 @ @ @ @```` @,` @@`` @`` @h @ @` @``` @` @ @``` @Q` @�``` @` @`` @` @`` @``` @}``` @O @ @` @"``` @L @x(@` @�``` @ @``` @0` @ @ @?` @(` @` @U @ @�   ��         ��      @P[P?L  �� 
   
  @Q6�  �� 	   	  @�  �� 	   	  @�  �� 
   
  @Q�d  ��      @�@   ��      @�@   �� 
   
  @W�   ��      @�@ -  �� 	   	  @�  �� 
   
  @R9�  �� 	   	  @�  �� 
   
  @R�#  ��      @�@#  �� 
   
  @W� T  ��      @ #@�  ��      @ #@�  ��      @ #@�  �� 
   
  @R��  �� 	   	  @�  �� 
   
  @Se�  ��      @�@^  �� 	   	  @�  �� 
   
  @Swo  �� 	   	  @�  ��      @�@F  ��      @�@E  ��      @�@F  ��      @�@: , �� 	   	  @�  �� 
   
  @S�T"  �� 
   
  @U�R  �� 
   
  @S�S�  �� 	   	  @W�  ��      @�@S�  �� 	   	  @W�  �� 
   
  @U�RH  ��      @>@S�  �� 	   	  @W�  ��      @�@SE  �� 
   
  @R�UR  �� 
   
  @T�S  �� 
   
  @UDR�  ��      @&@Q�  ��      @&@Q�  �� 
   
  @UjR|  ��      @k@Qy  ��      @j@Qx  ��      @Q��@Qy  ��      @�@V  �� 
   
  @VQg  ��      @�@PL  ��      @�@PK  �� 
   
  @W�PM  �� 
   
  @W�PB  �� 	   	  @W�  �� 
   
  @V�Q2  ��      @�@P�  ��      @V� X@P�  �� 
   
  @PSW�  ��      @ Q@W�  �� 
   
  @P�WU  �� 
   
  @WGP�  ��      @U]Q�P�  ��      @[@R�  ��      @[@R�  ��      @[@R�  ��      @\@R�  �� 
   
  @U^R�  ��      @+@U�  ��      @*@U�  ��      @+@U�  ��      @v@Pn  ��      @v@Pm  ��      @v@Pn  �� 
   
  @W�P@  �� 	   	  @W�  ��      @�@P  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @ �@~@ @!@Sa  �� 	   	  @�  �� 
   
  @V� �  �� 	   	  @�  �� 
   
  @P�D � �� 	   	  @�  �� 
   
  @S�1  �� 	   	  @�  �� 
   
  @S�1  ��      @P�R�1  �� 
   
  @S�1� �� 	   	  @�  ��      @�@"  �� 	   	  @� F �� 	   	  @�  �� 
   
  @P��  �� 	   	  @�  ��      @@ � 	 �� 	   	  @�  �� 
   
  @Q�R  �� 	   	  @�  �� 
   
  @Q� ] �� 	   	  @�  �� 	   	  @�  �� 	   	  @� ( �� 	   	  @�  �� 	   	  @�  ��      @5@�  �� 	   	  @� 0 �� 	   	  @�  �� 	   	  @�  �� 	   	  @�  �� 	   	  @W�  �� 	   	  @W�  �� 	   	  @W�  ��      @@S�  �� 	   	  @W�  �� 	   	  @W�  ��      @U@S� " �� 	   	  @W�  �� 
   
  @TuSq  �� 	   	  @W� 
 �� 	   	  @W�  �� 	   	  @W�  �� 
   
  @T�S$ - �� 	   	  @W� 3 �� 	   	  @W�  �� 
   
  @ST�  ��      @@T�  ��      @@T�  ��      @@T�  ��      @ @T�  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @�@T�  ��      @;@ �@T�  ��      @;@ �@T�  ��      @;@ �@T�  ��      @@T�  ��      @@T�  �� 
   
  @ST� 
 �� 	   	  @W�  �� 	   	  @W�  ��      @%@Q�  �� 	   	  @W�  �� 	   	  @W�  ��      @�@V $ �� 	   	  @W�  �� 	   	  @W�  �� 	   	  @W�  �� 	   	  @W�  �� 	   	  @W�  ��      @ Q@W�  �� 	   	  @W�  �� 	   	  @W�  ��      @�@PE  �� 	   	  @W�  �� 	   	  @W�  �� 	   	  @W�  �� 	   	  @W� + �� 	   	  @W� ) �� 	   	  @W�  �� 
   
  @P,W�  ��      @ *@W�  �� 
   
  @P,W�  ��      @�@Q�  �� 	   	  @W�  �� 
   
  @P�W   �� 	   	  @W�  �� 	   	  @W�  �� 
   
  @R�UB  ��      @�@UA  �� 	   	  @W�  ��      @ a@W�  ��      @ a@W�  ��      @ a@W�  �� 	   	  @W�  ��      @ B@W�  �� 
   
  @PCW� o �� 	   	  @W�  �� 
   
  @TXS�  �� 	   	  @W�  ��      @Qb �@ @U�  ��      @Qb �@U� 	 ��      @`@U�  ��      @Q� �@U�  ��      @�@ �@U� T ��      @`@U�  ��      @Q� �@U�  ��      @`@U�  ��      @@Y@U�  ��      @QZ@U� 	 ��      @`@U�  ��      @Q� �@U�  ��      @`@U�  ��      @9@%@U�  ��      @`@U�  ��      @ �@�@U�  ��      @ �@�@U�  ��      @ �@�@U�  ��      @P��@U� , ��      @`@U�  ��      @Q� �@U� @ ��      @`@U�  ��      @@]@U�  ��      @Q]@U�  ��      @`@U�  ��      @`@R$Se  ��      @`@!@ @Sa h ��      @`@!@Sa  ��      @ A@@!@Sa  ��      @`@!@Sa  ��      @`@!@Q�Q�  ��      @`@!@Sa  ��      @RU @!@Sa  ��      @S@ @!@Sa  ��      @RU @!@Sa  ��      @`@!@Sa  ��      @�@ �@!@Sa  ��      @�@ �@!@Sa  ��      @`@!@Sa  ��      @`@Q@Sa  ��      @`@@@Sa  ��      @`@@@Sa Q ��      @`@!@Sa  ��      @`@P�I@Sa � ��      @`@!@Sa  ��      @`@!@ �@Rs  ��      @`@!@ �@Rr  ��      @`@!@ �@Rs  ��      @`@!@Sa  ��      @`@!@@PD  ��      @`@!@Sa  ��      @`@ �@d@Sa  ��      @`@P�d@Sa  ��      @`@!@Sa  ��      @`@ @!@Sa  ��      @`@!@Sa  ��      @ �@v@!@Sa  ��      @P�w@!@Sa  ��      @`@!@Sa  ��      @`@!@P�Ri  ��      @`@!@ �@Rh  ��      @`@!@P�Ri } ��      @`@!@Sa  ��      @`@ @@Sa  ��      @`@ @@Sa  ��      @`@ @@Sa O ��      @`@!@Sa  ��      @Pr�@!@Sa  ��      @`@!@Sa  ��      @Pi�@!@Sa " ��      @`@!@Sa  ��      @`@Qe �@Sa  ��      @`@c@ �@Sa  ��      @`@Qe �@Sa L ��      @`@!@Sa  ��      @ �@~@!@Sa  ��      @ �@~@!@Sa  ��      @ �@~@!@R�P� � ��      @ �@~@!@Sa  ��      @ �@~@!@B@P  ��      @ �@~@!@A@	P  ��      @ �@~@!@@@
P  ��      @ �@~@!@?@P  ��      @ �@~@!@@@P  ��      @ �@~@!@@@
P  ��      @ �@~@!@A@	P  ��      @ �@~@!@B@P 0 ��      @ �@~@!@Sa  ��      @P0 �@~@!@Sa  ��      @ �@~@!@Sa  ��      @ �@~@!@PIS ? ��      @ �@~@!@Sa  ��      @PI �@~@!@Sa ( ��      @ �@~@!@Sa  ��      @ �@~@!@Q�Q�  ��      @ �@~@!@Sa  ��      @ �@~@P�1@Sa U ��      @ �@~@!@Sa  ��      @ �@~@PE�@Sa � ��      @ �@~@!@Sa