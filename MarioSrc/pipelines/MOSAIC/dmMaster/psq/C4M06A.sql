REPLACE INTO PSQ VALUES('C4M06A', 'C4M06A', 'C4M06AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M06A (dataset) VALUES ('20060201');
REPLACE INTO C4M06A (dataset) VALUES ('20060224');
REPLACE INTO C4M06A (dataset) VALUES ('20060228');
REPLACE INTO C4M06A (dataset) VALUES ('20060305');
REPLACE INTO C4M06A (dataset) VALUES ('20060424');
REPLACE INTO C4M06A (dataset) VALUES ('20060425');
REPLACE INTO C4M06A (dataset) VALUES ('20060428');
REPLACE INTO C4M06A (dataset) VALUES ('20060430');
REPLACE INTO C4M06A (dataset) VALUES ('20060613');
REPLACE INTO C4M06A (dataset) VALUES ('20060614');
REPLACE INTO C4M06A (dataset) VALUES ('20060620');
REPLACE INTO C4M06A (dataset) VALUES ('20060625');
REPLACE INTO C4M06A (dataset) VALUES ('20060722');
REPLACE INTO C4M06A (dataset) VALUES ('20060725');

REPLACE INTO C4M06AD VALUES ('20060201', '20060201', '20060207',
  '"startDate" between timestamp ''2006-02-01'' and timestamp ''2006-02-07''');
REPLACE INTO C4M06AD VALUES ('20060224', '20060224', '20060227',
  '"startDate" between timestamp ''2006-02-24'' and timestamp ''2006-02-27''');
REPLACE INTO C4M06AD VALUES ('20060228', '20060228', '20060304',
  '"startDate" between timestamp ''2006-02-28'' and timestamp ''2006-03-04''');
REPLACE INTO C4M06AD VALUES ('20060305', '20060305', '20060308',
  '"startDate" between timestamp ''2006-03-05'' and timestamp ''2006-03-08''');
REPLACE INTO C4M06AD VALUES ('20060424', '20060424', '20060424',
  '"startDate" between timestamp ''2006-04-24'' and timestamp ''2006-04-24''');
REPLACE INTO C4M06AD VALUES ('20060425', '20060425', '20060427',
  '"startDate" between timestamp ''2006-04-25'' and timestamp ''2006-04-27''');
REPLACE INTO C4M06AD VALUES ('20060428', '20060428', '20060429',
  '"startDate" between timestamp ''2006-04-28'' and timestamp ''2006-04-29''');
REPLACE INTO C4M06AD VALUES ('20060430', '20060430', '20060501',
  '"startDate" between timestamp ''2006-04-30'' and timestamp ''2006-05-01''');
REPLACE INTO C4M06AD VALUES ('20060613', '20060613', '20060613',
  '"startDate" between timestamp ''2006-06-13'' and timestamp ''2006-06-13''');
REPLACE INTO C4M06AD VALUES ('20060614', '20060614', '20060619',
  '"startDate" between timestamp ''2006-06-14'' and timestamp ''2006-06-19''');
REPLACE INTO C4M06AD VALUES ('20060620', '20060620', '20060624',
  '"startDate" between timestamp ''2006-06-20'' and timestamp ''2006-06-24''');
REPLACE INTO C4M06AD VALUES ('20060625', '20060625', '20060629',
  '"startDate" between timestamp ''2006-06-25'' and timestamp ''2006-06-29''');
REPLACE INTO C4M06AD VALUES ('20060722', '20060722', '20060724',
  '"startDate" between timestamp ''2006-07-22'' and timestamp ''2006-07-24''');
REPLACE INTO C4M06AD VALUES ('20060725', '20060725', '20060730',
  '"startDate" between timestamp ''2006-07-25'' and timestamp ''2006-07-30''');
