REPLACE INTO PSQ VALUES('C4M10B', 'C4M10B', 'C4M10BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M10B (dataset) VALUES ('20100805');
REPLACE INTO C4M10B (dataset) VALUES ('20100809');
REPLACE INTO C4M10B (dataset) VALUES ('20100926');
REPLACE INTO C4M10B (dataset) VALUES ('20100929');
REPLACE INTO C4M10B (dataset) VALUES ('20101003');
REPLACE INTO C4M10B (dataset) VALUES ('20101006');
REPLACE INTO C4M10B (dataset) VALUES ('20101010');
REPLACE INTO C4M10B (dataset) VALUES ('20101026');
REPLACE INTO C4M10B (dataset) VALUES ('20101030');
REPLACE INTO C4M10B (dataset) VALUES ('20101125');
REPLACE INTO C4M10B (dataset) VALUES ('20101126');
REPLACE INTO C4M10B (dataset) VALUES ('20101130');

REPLACE INTO C4M10BD VALUES ('20100805', '20100805', '20100808',
  '"startDate" between timestamp ''2010-08-05'' and timestamp ''2010-08-08''');
REPLACE INTO C4M10BD VALUES ('20100809', '20100809', '20100812',
  '"startDate" between timestamp ''2010-08-09'' and timestamp ''2010-08-12''');
REPLACE INTO C4M10BD VALUES ('20100926', '20100926', '20100928',
  '"startDate" between timestamp ''2010-09-26'' and timestamp ''2010-09-28''');
REPLACE INTO C4M10BD VALUES ('20100929', '20100929', '20101002',
  '"startDate" between timestamp ''2010-09-29'' and timestamp ''2010-10-02''');
REPLACE INTO C4M10BD VALUES ('20101003', '20101003', '20101005',
  '"startDate" between timestamp ''2010-10-03'' and timestamp ''2010-10-05''');
REPLACE INTO C4M10BD VALUES ('20101006', '20101006', '20101009',
  '"startDate" between timestamp ''2010-10-06'' and timestamp ''2010-10-09''');
REPLACE INTO C4M10BD VALUES ('20101010', '20101010', '20101013',
  '"startDate" between timestamp ''2010-10-10'' and timestamp ''2010-10-13''');
REPLACE INTO C4M10BD VALUES ('20101026', '20101026', '20101029',
  '"startDate" between timestamp ''2010-10-26'' and timestamp ''2010-10-29''');
REPLACE INTO C4M10BD VALUES ('20101030', '20101030', '20101102',
  '"startDate" between timestamp ''2010-10-30'' and timestamp ''2010-11-02''');
REPLACE INTO C4M10BD VALUES ('20101125', '20101125', '20101125',
  '"startDate" between timestamp ''2010-11-25'' and timestamp ''2010-11-25''');
REPLACE INTO C4M10BD VALUES ('20101126', '20101126', '20101129',
  '"startDate" between timestamp ''2010-11-26'' and timestamp ''2010-11-29''');
REPLACE INTO C4M10BD VALUES ('20101130', '20101130', '20101201',
  '"startDate" between timestamp ''2010-11-30'' and timestamp ''2010-12-01''');
