REPLACE INTO PSQ VALUES('K4M07B', 'K4M07B', 'K4M07BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M07B (dataset) VALUES ('20070830');
REPLACE INTO K4M07B (dataset) VALUES ('20070831');
REPLACE INTO K4M07B (dataset) VALUES ('20070904');
REPLACE INTO K4M07B (dataset) VALUES ('20070906');
REPLACE INTO K4M07B (dataset) VALUES ('20070911');
REPLACE INTO K4M07B (dataset) VALUES ('20070914');
REPLACE INTO K4M07B (dataset) VALUES ('20071001');
REPLACE INTO K4M07B (dataset) VALUES ('20071005');
REPLACE INTO K4M07B (dataset) VALUES ('20071012');
REPLACE INTO K4M07B (dataset) VALUES ('20071207');
REPLACE INTO K4M07B (dataset) VALUES ('20071211');
REPLACE INTO K4M07B (dataset) VALUES ('20071213');
REPLACE INTO K4M07B (dataset) VALUES ('20071217');

REPLACE INTO K4M07BD VALUES ('20070830', '20070830', '20070830',
  '"startDate" between timestamp ''2007-08-30'' and timestamp ''2007-08-30''');
REPLACE INTO K4M07BD VALUES ('20070831', '20070831', '20070903',
  '"startDate" between timestamp ''2007-08-31'' and timestamp ''2007-09-03''');
REPLACE INTO K4M07BD VALUES ('20070904', '20070904', '20070905',
  '"startDate" between timestamp ''2007-09-04'' and timestamp ''2007-09-05''');
REPLACE INTO K4M07BD VALUES ('20070906', '20070906', '20070910',
  '"startDate" between timestamp ''2007-09-06'' and timestamp ''2007-09-10''');
REPLACE INTO K4M07BD VALUES ('20070911', '20070911', '20070913',
  '"startDate" between timestamp ''2007-09-11'' and timestamp ''2007-09-13''');
REPLACE INTO K4M07BD VALUES ('20070914', '20070914', '20070916',
  '"startDate" between timestamp ''2007-09-14'' and timestamp ''2007-09-16''');
REPLACE INTO K4M07BD VALUES ('20071001', '20071001', '20071004',
  '"startDate" between timestamp ''2007-10-01'' and timestamp ''2007-10-04''');
REPLACE INTO K4M07BD VALUES ('20071005', '20071005', '20071008',
  '"startDate" between timestamp ''2007-10-05'' and timestamp ''2007-10-08''');
REPLACE INTO K4M07BD VALUES ('20071012', '20071012', '20071015',
  '"startDate" between timestamp ''2007-10-12'' and timestamp ''2007-10-15''');
REPLACE INTO K4M07BD VALUES ('20071207', '20071207', '20071210',
  '"startDate" between timestamp ''2007-12-07'' and timestamp ''2007-12-10''');
REPLACE INTO K4M07BD VALUES ('20071211', '20071211', '20071212',
  '"startDate" between timestamp ''2007-12-11'' and timestamp ''2007-12-12''');
REPLACE INTO K4M07BD VALUES ('20071213', '20071213', '20071216',
  '"startDate" between timestamp ''2007-12-13'' and timestamp ''2007-12-16''');
REPLACE INTO K4M07BD VALUES ('20071217', '20071217', '20071219',
  '"startDate" between timestamp ''2007-12-17'' and timestamp ''2007-12-19''');
