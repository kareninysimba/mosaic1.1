REPLACE INTO PSQ VALUES('K4M07A', 'K4M07A', 'K4M07AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M07A (dataset) VALUES ('20070209');
REPLACE INTO K4M07A (dataset) VALUES ('20070210');
REPLACE INTO K4M07A (dataset) VALUES ('20070214');
REPLACE INTO K4M07A (dataset) VALUES ('20070219');
REPLACE INTO K4M07A (dataset) VALUES ('20070409');
REPLACE INTO K4M07A (dataset) VALUES ('20070410');
REPLACE INTO K4M07A (dataset) VALUES ('20070414');
REPLACE INTO K4M07A (dataset) VALUES ('20070418');
REPLACE INTO K4M07A (dataset) VALUES ('20070510');
REPLACE INTO K4M07A (dataset) VALUES ('20070515');
REPLACE INTO K4M07A (dataset) VALUES ('20070518');
REPLACE INTO K4M07A (dataset) VALUES ('20070522');
REPLACE INTO K4M07A (dataset) VALUES ('20070607');
REPLACE INTO K4M07A (dataset) VALUES ('20070613');

REPLACE INTO K4M07AD VALUES ('20070209', '20070209', '20070209',
  '"startDate" between timestamp ''2007-02-09'' and timestamp ''2007-02-09''');
REPLACE INTO K4M07AD VALUES ('20070210', '20070210', '20070213',
  '"startDate" between timestamp ''2007-02-10'' and timestamp ''2007-02-13''');
REPLACE INTO K4M07AD VALUES ('20070214', '20070214', '20070218',
  '"startDate" between timestamp ''2007-02-14'' and timestamp ''2007-02-18''');
REPLACE INTO K4M07AD VALUES ('20070219', '20070219', '20070220',
  '"startDate" between timestamp ''2007-02-19'' and timestamp ''2007-02-20''');
REPLACE INTO K4M07AD VALUES ('20070409', '20070409', '20070409',
  '"startDate" between timestamp ''2007-04-09'' and timestamp ''2007-04-09''');
REPLACE INTO K4M07AD VALUES ('20070410', '20070410', '20070413',
  '"startDate" between timestamp ''2007-04-10'' and timestamp ''2007-04-13''');
REPLACE INTO K4M07AD VALUES ('20070414', '20070414', '20070417',
  '"startDate" between timestamp ''2007-04-14'' and timestamp ''2007-04-17''');
REPLACE INTO K4M07AD VALUES ('20070418', '20070418', '20070422',
  '"startDate" between timestamp ''2007-04-18'' and timestamp ''2007-04-22''');
REPLACE INTO K4M07AD VALUES ('20070510', '20070510', '20070514',
  '"startDate" between timestamp ''2007-05-10'' and timestamp ''2007-05-14''');
REPLACE INTO K4M07AD VALUES ('20070515', '20070515', '20070517',
  '"startDate" between timestamp ''2007-05-15'' and timestamp ''2007-05-17''');
REPLACE INTO K4M07AD VALUES ('20070518', '20070518', '20070521',
  '"startDate" between timestamp ''2007-05-18'' and timestamp ''2007-05-21''');
REPLACE INTO K4M07AD VALUES ('20070522', '20070522', '20070523',
  '"startDate" between timestamp ''2007-05-22'' and timestamp ''2007-05-23''');
REPLACE INTO K4M07AD VALUES ('20070607', '20070607', '20070612',
  '"startDate" between timestamp ''2007-06-07'' and timestamp ''2007-06-12''');
REPLACE INTO K4M07AD VALUES ('20070613', '20070613', '20070618',
  '"startDate" between timestamp ''2007-06-13'' and timestamp ''2007-06-18''');
