REPLACE INTO PSQ VALUES('K4M05B', 'K4M05B', 'K4M05BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M05B (dataset) VALUES ('20050926');
REPLACE INTO K4M05B (dataset) VALUES ('20050927');
REPLACE INTO K4M05B (dataset) VALUES ('20051001');
REPLACE INTO K4M05B (dataset) VALUES ('20051004');
REPLACE INTO K4M05B (dataset) VALUES ('20051008');
REPLACE INTO K4M05B (dataset) VALUES ('20051025');
REPLACE INTO K4M05B (dataset) VALUES ('20051026');
REPLACE INTO K4M05B (dataset) VALUES ('20051101');
REPLACE INTO K4M05B (dataset) VALUES ('20051103');
REPLACE INTO K4M05B (dataset) VALUES ('20051123');
REPLACE INTO K4M05B (dataset) VALUES ('20051130');
REPLACE INTO K4M05B (dataset) VALUES ('20051202');

REPLACE INTO K4M05BD VALUES ('20050926', '20050926', '20050926',
  'start_date between timestamp ''2005-09-26'' and timestamp ''2005-09-26''');
REPLACE INTO K4M05BD VALUES ('20050927', '20050927', '20050930',
  'start_date between timestamp ''2005-09-27'' and timestamp ''2005-09-30''');
REPLACE INTO K4M05BD VALUES ('20051001', '20051001', '20051003',
  'start_date between timestamp ''2005-10-01'' and timestamp ''2005-10-03''');
REPLACE INTO K4M05BD VALUES ('20051004', '20051004', '20051007',
  'start_date between timestamp ''2005-10-04'' and timestamp ''2005-10-07''');
REPLACE INTO K4M05BD VALUES ('20051008', '20051008', '20051009',
  'start_date between timestamp ''2005-10-08'' and timestamp ''2005-10-09''');
REPLACE INTO K4M05BD VALUES ('20051025', '20051025', '20051025',
  'start_date between timestamp ''2005-10-25'' and timestamp ''2005-10-25''');
REPLACE INTO K4M05BD VALUES ('20051026', '20051026', '20051031',
  'start_date between timestamp ''2005-10-26'' and timestamp ''2005-10-31''');
REPLACE INTO K4M05BD VALUES ('20051101', '20051101', '20051102',
  'start_date between timestamp ''2005-11-01'' and timestamp ''2005-11-02''');
REPLACE INTO K4M05BD VALUES ('20051103', '20051103', '20051106',
  'start_date between timestamp ''2005-11-03'' and timestamp ''2005-11-06''');
REPLACE INTO K4M05BD VALUES ('20051123', '20051123', '20051129',
  'start_date between timestamp ''2005-11-23'' and timestamp ''2005-11-29''');
REPLACE INTO K4M05BD VALUES ('20051130', '20051130', '20051201',
  'start_date between timestamp ''2005-11-30'' and timestamp ''2005-12-01''');
REPLACE INTO K4M05BD VALUES ('20051202', '20051202', '20051206',
  'start_date between timestamp ''2005-12-02'' and timestamp ''2005-12-06''');
