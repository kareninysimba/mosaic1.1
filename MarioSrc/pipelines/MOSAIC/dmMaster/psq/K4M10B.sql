REPLACE INTO PSQ VALUES('K4M10B', 'K4M10B', 'K4M10BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M10B (dataset) VALUES ('20101022');
REPLACE INTO K4M10B (dataset) VALUES ('20101027');
REPLACE INTO K4M10B (dataset) VALUES ('20101104');
REPLACE INTO K4M10B (dataset) VALUES ('20101105');
REPLACE INTO K4M10B (dataset) VALUES ('20101107');
REPLACE INTO K4M10B (dataset) VALUES ('20101109');
REPLACE INTO K4M10B (dataset) VALUES ('20101129');
REPLACE INTO K4M10B (dataset) VALUES ('20101130');
REPLACE INTO K4M10B (dataset) VALUES ('20101203');
REPLACE INTO K4M10B (dataset) VALUES ('20101130A');
REPLACE INTO K4M10B (dataset) VALUES ('20101206');
REPLACE INTO K4M10B (dataset) VALUES ('20101209');
REPLACE INTO K4M10B (dataset) VALUES ('20110104');
REPLACE INTO K4M10B (dataset) VALUES ('20110108');

REPLACE INTO K4M10BD VALUES ('20101022', '20101022', '20101026',
  '"startDate" between timestamp ''2010-10-22'' and timestamp ''2010-10-26''');
REPLACE INTO K4M10BD VALUES ('20101027', '20101027', '20101027',
  '"startDate" between timestamp ''2010-10-27'' and timestamp ''2010-10-27''');
REPLACE INTO K4M10BD VALUES ('20101104', '20101104', '20101104',
  '"startDate" between timestamp ''2010-11-04'' and timestamp ''2010-11-04''');
REPLACE INTO K4M10BD VALUES ('20101105', '20101105', '20101106',
  '"startDate" between timestamp ''2010-11-05'' and timestamp ''2010-11-06''');
REPLACE INTO K4M10BD VALUES ('20101107', '20101107', '20101108',
  '"startDate" between timestamp ''2010-11-07'' and timestamp ''2010-11-08''');
REPLACE INTO K4M10BD VALUES ('20101109', '20101109', '20101110',
  '"startDate" between timestamp ''2010-11-07'' and timestamp ''2010-11-10''');
REPLACE INTO K4M10BD VALUES ('20101129', '20101129', '20101129',
  '"startDate" between timestamp ''2010-11-29'' and timestamp ''2010-11-29''');
REPLACE INTO K4M10BD VALUES ('20101130', '20101130', '20101202',
  '"startDate" between timestamp ''2010-11-30'' and timestamp ''2010-12-02''');
REPLACE INTO K4M10BD VALUES ('20101203', '20101203', '20101205',
  '"startDate" between timestamp ''2010-12-03'' and timestamp ''2010-12-05''');
REPLACE INTO K4M10BD VALUES ('20101130A', '20101130', '20101205',
  '"startDate" between timestamp ''2010-11-30'' and timestamp ''2010-12-05''');
REPLACE INTO K4M10BD VALUES ('20101206', '20101206', '20101208',
  '"startDate" between timestamp ''2010-12-06'' and timestamp ''2010-12-08''');
REPLACE INTO K4M10BD VALUES ('20101209', '20101209', '20101212',
  '"startDate" between timestamp ''2010-12-09'' and timestamp ''2010-12-12''');
REPLACE INTO K4M10BD VALUES ('20110104', '20110104', '20110107',
  '"startDate" between timestamp ''2011-01-04'' and timestamp ''2011-01-07''');
REPLACE INTO K4M10BD VALUES ('20110108', '20110108', '20110111',
  '"startDate" between timestamp ''2011-01-08'' and timestamp ''2011-01-11''');
