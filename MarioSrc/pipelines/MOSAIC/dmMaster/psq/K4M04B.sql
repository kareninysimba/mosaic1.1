REPLACE INTO PSQ VALUES('K4M04B', 'K4M04B', 'K4M04BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M04B (dataset) VALUES ('20040831');
REPLACE INTO K4M04B (dataset) VALUES ('20040903');
REPLACE INTO K4M04B (dataset) VALUES ('20040907');
REPLACE INTO K4M04B (dataset) VALUES ('20040910');
REPLACE INTO K4M04B (dataset) VALUES ('20040914');
REPLACE INTO K4M04B (dataset) VALUES ('20041008');
REPLACE INTO K4M04B (dataset) VALUES ('20041012');
REPLACE INTO K4M04B (dataset) VALUES ('20041014');
REPLACE INTO K4M04B (dataset) VALUES ('20041102');
REPLACE INTO K4M04B (dataset) VALUES ('20041103');
REPLACE INTO K4M04B (dataset) VALUES ('20041108');
REPLACE INTO K4M04B (dataset) VALUES ('20041111');
REPLACE INTO K4M04B (dataset) VALUES ('20041115');
REPLACE INTO K4M04B (dataset) VALUES ('20041123');
REPLACE INTO K4M04B (dataset) VALUES ('20050103');
REPLACE INTO K4M04B (dataset) VALUES ('20050109');
REPLACE INTO K4M04B (dataset) VALUES ('20050113');

REPLACE INTO K4M04BD VALUES ('20040831', '20040831', '20040802',
  'start_date between timestamp ''2004-08-31'' and timestamp ''2004-08-02''');
REPLACE INTO K4M04BD VALUES ('20040903', '20040903', '20040906',
  'start_date between timestamp ''2004-09-03'' and timestamp ''2004-09-06''');
REPLACE INTO K4M04BD VALUES ('20040907', '20040907', '20040909',
  'start_date between timestamp ''2004-09-07'' and timestamp ''2004-09-09''');
REPLACE INTO K4M04BD VALUES ('20040910', '20040910', '20040913',
  'start_date between timestamp ''2004-09-10'' and timestamp ''2004-09-13''');
REPLACE INTO K4M04BD VALUES ('20040914', '20040914', '20040916',
  'start_date between timestamp ''2004-09-14'' and timestamp ''2004-09-16''');
REPLACE INTO K4M04BD VALUES ('20041008', '20041008', '20041011',
  'start_date between timestamp ''2004-10-08'' and timestamp ''2004-10-11''');
REPLACE INTO K4M04BD VALUES ('20041012', '20041012', '20041013',
  'start_date between timestamp ''2004-10-12'' and timestamp ''2004-10-13''');
REPLACE INTO K4M04BD VALUES ('20041014', '20041014', '20041017',
  'start_date between timestamp ''2004-10-14'' and timestamp ''2004-10-17''');
REPLACE INTO K4M04BD VALUES ('20041102', '20041102', '20041102',
  'start_date between timestamp ''2004-11-02'' and timestamp ''2004-11-02''');
REPLACE INTO K4M04BD VALUES ('20041103', '20041103', '20041107',
  'start_date between timestamp ''2004-11-03'' and timestamp ''2004-11-07''');
REPLACE INTO K4M04BD VALUES ('20041108', '20041108', '20041110',
  'start_date between timestamp ''2004-11-08'' and timestamp ''2004-11-10''');
REPLACE INTO K4M04BD VALUES ('20041111', '20041111', '20041114',
  'start_date between timestamp ''2004-11-11'' and timestamp ''2004-11-14''');
REPLACE INTO K4M04BD VALUES ('20041115', '20041115', '20041118',
  'start_date between timestamp ''2004-11-15'' and timestamp ''2004-11-18''');
REPLACE INTO K4M04BD VALUES ('20041123', '20041123', '20050103',
  'start_date between timestamp ''2004-11-23'' and timestamp ''2005-01-03''');
REPLACE INTO K4M04BD VALUES ('20050103', '20050103', '20050108',
  'start_date between timestamp ''2005-01-03'' and timestamp ''2005-01-08''');
REPLACE INTO K4M04BD VALUES ('20050109', '20050109', '20050112',
  'start_date between timestamp ''2005-01-09'' and timestamp ''2005-01-12''');
REPLACE INTO K4M04BD VALUES ('20050113', '20050113', '20050116',
  'start_date between timestamp ''2005-01-13'' and timestamp ''2005-01-16''');
