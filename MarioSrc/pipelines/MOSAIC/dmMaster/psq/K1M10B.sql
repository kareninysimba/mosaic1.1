REPLACE INTO PSQ VALUES('K1M10B', 'K1M10B', 'K1M10BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K1M10B (dataset) VALUES ('20110120');
REPLACE INTO K1M10B (dataset) VALUES ('20110123');

REPLACE INTO K1M10BD VALUES ('20110120', '20110120', '20110122',
  '"startDate" between timestamp ''2011-01-20'' and timestamp ''2011-01-22''');
REPLACE INTO K1M10BD VALUES ('20110123', '20110123', '20110126',
  '"startDate" between timestamp ''2011-01-23'' and timestamp ''2011-01-26''');
