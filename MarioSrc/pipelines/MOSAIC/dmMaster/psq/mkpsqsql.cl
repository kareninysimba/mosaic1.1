# MKPSQSQL -- Convert getrun data into sql data.
#
# This only needs to use the first three fields of the QD file from getruns.

procedure mkpsqsql (queue)

string	queue

begin
	file	fname
	string	instrument, query
	string	q, d, a, b
        a="";b="";d="";

	# Use queue name to set other parameters.
	q = queue
	if (substr(q,1,3) == "C4M") {
	    fname = "ct4m"
	    instrument = "MOSAIC"
	    query = "instrument=''mosaic_2''"
	} else if (substr(q,1,4) == "K4M1") {
	    fname = "kp4m"
	    instrument = "MOSAIC"
	    query = "instrument=''mosaic_1_1''"
	} else if (substr(q,1,4) == "K4M0") {
	    fname = "kp4m"
	    instrument = "MOSAIC"
	    query = "instrument=''mosaic_1''"
	} else if (substr(q,1,4) == "K1M1") {
	    fname = "kp09m"
	    instrument = "MOSAIC"
	    query = "instrument=''mosaic_1_1''"
	} else
	    error (1, "Unknown queue name (%s)\n", q)

	# Enter new queue.
	printf ("REPLACE INTO PSQ VALUES('%s', '%s', '%sD', '%s',\n",
	    q, q, q, instrument)
	printf ("'dir', 'disabled', '%s');\n\n", query)

	list = fname//"QD.out"
	while (fscan (list, d) != EOF)
	    printf ("REPLACE INTO %s (dataset) VALUES ('%s');\n", q, d)
	list = ""
	printf ("\n")

	list = fname//"QD.out"
	while (fscan (list, d, a, b) != EOF) {
	    printf ("REPLACE INTO %sD VALUES ('%s', '%s', '%s',\n", q, d, a, b)
	    printf ("  'start_date between timestamp ''%s-%s-%s'' and timestamp ''%s-%s-%s''');\n",
		substr(a,1,4), substr(a,5,6), substr(a,7,8),
		substr(b,1,4), substr(b,5,6), substr(b,7,8))
	}
	list = ""
end
