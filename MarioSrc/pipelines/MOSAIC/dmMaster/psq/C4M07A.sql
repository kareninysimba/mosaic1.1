REPLACE INTO PSQ VALUES('C4M07A', 'C4M07A', 'C4M07AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M07A (dataset) VALUES ('20070213');
REPLACE INTO C4M07A (dataset) VALUES ('20070217');
REPLACE INTO C4M07A (dataset) VALUES ('20070222');
REPLACE INTO C4M07A (dataset) VALUES ('20070225');
REPLACE INTO C4M07A (dataset) VALUES ('20070301');
REPLACE INTO C4M07A (dataset) VALUES ('20070415');
REPLACE INTO C4M07A (dataset) VALUES ('20070420');
REPLACE INTO C4M07A (dataset) VALUES ('20070608');
REPLACE INTO C4M07A (dataset) VALUES ('20070612');
REPLACE INTO C4M07A (dataset) VALUES ('20070616');
REPLACE INTO C4M07A (dataset) VALUES ('20070620');

REPLACE INTO C4M07AD VALUES ('20070213', '20070213', '20070216',
  '"startDate" between timestamp ''2007-02-13'' and timestamp ''2007-02-16''');
REPLACE INTO C4M07AD VALUES ('20070217', '20070217', '20070221',
  '"startDate" between timestamp ''2007-02-17'' and timestamp ''2007-02-21''');
REPLACE INTO C4M07AD VALUES ('20070222', '20070222', '20070224',
  '"startDate" between timestamp ''2007-02-22'' and timestamp ''2007-02-24''');
REPLACE INTO C4M07AD VALUES ('20070225', '20070225', '20070228',
  '"startDate" between timestamp ''2007-02-25'' and timestamp ''2007-02-28''');
REPLACE INTO C4M07AD VALUES ('20070301', '20070301', '20070305',
  '"startDate" between timestamp ''2007-03-01'' and timestamp ''2007-03-05''');
REPLACE INTO C4M07AD VALUES ('20070415', '20070415', '20070419',
  '"startDate" between timestamp ''2007-04-15'' and timestamp ''2007-04-19''');
REPLACE INTO C4M07AD VALUES ('20070420', '20070420', '20070423',
  '"startDate" between timestamp ''2007-04-20'' and timestamp ''2007-04-23''');
REPLACE INTO C4M07AD VALUES ('20070608', '20070608', '20070611',
  '"startDate" between timestamp ''2007-06-08'' and timestamp ''2007-06-11''');
REPLACE INTO C4M07AD VALUES ('20070612', '20070612', '20070615',
  '"startDate" between timestamp ''2007-06-12'' and timestamp ''2007-06-15''');
REPLACE INTO C4M07AD VALUES ('20070616', '20070616', '20070619',
  '"startDate" between timestamp ''2007-06-16'' and timestamp ''2007-06-19''');
REPLACE INTO C4M07AD VALUES ('20070620', '20070620', '20070622',
  '"startDate" between timestamp ''2007-06-20'' and timestamp ''2007-06-22''');
