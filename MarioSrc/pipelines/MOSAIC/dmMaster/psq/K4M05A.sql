REPLACE INTO PSQ VALUES('K4M05A', 'K4M05A', 'K4M05AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M05A (dataset) VALUES ('20050301');
REPLACE INTO K4M05A (dataset) VALUES ('20050304');
REPLACE INTO K4M05A (dataset) VALUES ('20050308');
REPLACE INTO K4M05A (dataset) VALUES ('20050312');
REPLACE INTO K4M05A (dataset) VALUES ('20050405');
REPLACE INTO K4M05A (dataset) VALUES ('20050409');
REPLACE INTO K4M05A (dataset) VALUES ('20050602');
REPLACE INTO K4M05A (dataset) VALUES ('20050603');
REPLACE INTO K4M05A (dataset) VALUES ('20050608');
REPLACE INTO K4M05A (dataset) VALUES ('20050609');
REPLACE INTO K4M05A (dataset) VALUES ('20050701');
REPLACE INTO K4M05A (dataset) VALUES ('20050705');
REPLACE INTO K4M05A (dataset) VALUES ('20050709');

REPLACE INTO K4M05AD VALUES ('20050301', '20050301', '20050303',
  'start_date between timestamp ''2005-03-01'' and timestamp ''2005-03-03''');
REPLACE INTO K4M05AD VALUES ('20050304', '20050304', '20050307',
  'start_date between timestamp ''2005-03-04'' and timestamp ''2005-03-07''');
REPLACE INTO K4M05AD VALUES ('20050308', '20050308', '20050311',
  'start_date between timestamp ''2005-03-08'' and timestamp ''2005-03-11''');
REPLACE INTO K4M05AD VALUES ('20050312', '20050312', '20050315',
  'start_date between timestamp ''2005-03-12'' and timestamp ''2005-03-15''');
REPLACE INTO K4M05AD VALUES ('20050405', '20050405', '20050408',
  'start_date between timestamp ''2005-04-05'' and timestamp ''2005-04-08''');
REPLACE INTO K4M05AD VALUES ('20050409', '20050409', '20050412',
  'start_date between timestamp ''2005-04-09'' and timestamp ''2005-04-12''');
REPLACE INTO K4M05AD VALUES ('20050602', '20050602', '20050602',
  'start_date between timestamp ''2005-06-02'' and timestamp ''2005-06-02''');
REPLACE INTO K4M05AD VALUES ('20050603', '20050603', '20050607',
  'start_date between timestamp ''2005-06-03'' and timestamp ''2005-06-07''');
REPLACE INTO K4M05AD VALUES ('20050608', '20050608', '20050608',
  'start_date between timestamp ''2005-06-08'' and timestamp ''2005-06-08''');
REPLACE INTO K4M05AD VALUES ('20050609', '20050609', '20050612',
  'start_date between timestamp ''2005-06-09'' and timestamp ''2005-06-12''');
REPLACE INTO K4M05AD VALUES ('20050701', '20050701', '20050704',
  'start_date between timestamp ''2005-07-01'' and timestamp ''2005-07-04''');
REPLACE INTO K4M05AD VALUES ('20050705', '20050705', '20050708',
  'start_date between timestamp ''2005-07-05'' and timestamp ''2005-07-08''');
REPLACE INTO K4M05AD VALUES ('20050709', '20050709', '20050710',
  'start_date between timestamp ''2005-07-09'' and timestamp ''2005-07-10''');
