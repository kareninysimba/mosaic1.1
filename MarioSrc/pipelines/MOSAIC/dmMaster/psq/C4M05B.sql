REPLACE INTO PSQ VALUES('C4M05B', 'C4M05B', 'C4M05BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M05B (dataset) VALUES ('20050801');
REPLACE INTO C4M05B (dataset) VALUES ('20050805');
REPLACE INTO C4M05B (dataset) VALUES ('20050807');
REPLACE INTO C4M05B (dataset) VALUES ('20050824');
REPLACE INTO C4M05B (dataset) VALUES ('20050826');
REPLACE INTO C4M05B (dataset) VALUES ('20050829');
REPLACE INTO C4M05B (dataset) VALUES ('20050901');
REPLACE INTO C4M05B (dataset) VALUES ('20050904');
REPLACE INTO C4M05B (dataset) VALUES ('20050921');
REPLACE INTO C4M05B (dataset) VALUES ('20050923');
REPLACE INTO C4M05B (dataset) VALUES ('20051002');
REPLACE INTO C4M05B (dataset) VALUES ('20051024');
REPLACE INTO C4M05B (dataset) VALUES ('20051118');
REPLACE INTO C4M05B (dataset) VALUES ('20051121');
REPLACE INTO C4M05B (dataset) VALUES ('20051126');
REPLACE INTO C4M05B (dataset) VALUES ('20051201');
REPLACE INTO C4M05B (dataset) VALUES ('20051205');
REPLACE INTO C4M05B (dataset) VALUES ('20051223');
REPLACE INTO C4M05B (dataset) VALUES ('20051229');
REPLACE INTO C4M05B (dataset) VALUES ('20060102');

REPLACE INTO C4M05BD VALUES ('20050801', '20050801', '20050804',
  'start_date between timestamp ''2005-08-01'' and timestamp ''2005-08-04''');
REPLACE INTO C4M05BD VALUES ('20050805', '20050805', '20050806',
  'start_date between timestamp ''2005-08-05'' and timestamp ''2005-08-06''');
REPLACE INTO C4M05BD VALUES ('20050807', '20050807', '20050809',
  'start_date between timestamp ''2005-08-07'' and timestamp ''2005-08-09''');
REPLACE INTO C4M05BD VALUES ('20050824', '20050824', '20050825',
  'start_date between timestamp ''2005-08-24'' and timestamp ''2005-08-25''');
REPLACE INTO C4M05BD VALUES ('20050826', '20050826', '20050828',
  'start_date between timestamp ''2005-08-26'' and timestamp ''2005-08-28''');
REPLACE INTO C4M05BD VALUES ('20050829', '20050829', '20050831',
  'start_date between timestamp ''2005-08-29'' and timestamp ''2005-08-31''');
REPLACE INTO C4M05BD VALUES ('20050901', '20050901', '20050903',
  'start_date between timestamp ''2005-09-01'' and timestamp ''2005-09-03''');
REPLACE INTO C4M05BD VALUES ('20050904', '20050904', '20050909',
  'start_date between timestamp ''2005-09-04'' and timestamp ''2005-09-09''');
REPLACE INTO C4M05BD VALUES ('20050921', '20050921', '20050930',
  'start_date between timestamp ''2005-09-21'' and timestamp ''2005-09-30'' and dtpropid=''2005B-0111''');
REPLACE INTO C4M05BD VALUES ('20050923', '20050923', '20051010',
  'start_date between timestamp ''2005-09-23'' and timestamp ''2005-10-10'' and dtpropid=''noao''');
REPLACE INTO C4M05BD VALUES ('20051002', '20051002', '20051009',
  'start_date between timestamp ''2005-10-02'' and timestamp ''2005-10-09'' and dtpropid!=''noao''');
REPLACE INTO C4M05BD VALUES ('20051024', '20051024', '20051109',
  'start_date between timestamp ''2005-10-24'' and timestamp ''2005-11-09''');
REPLACE INTO C4M05BD VALUES ('20051118', '20051118', '20051124',
  'start_date between timestamp ''2005-11-18'' and timestamp ''2005-11-24'' and dtpropid=''2005B-0043''');
REPLACE INTO C4M05BD VALUES ('20051121', '20051121', '20051129',
  'start_date between timestamp ''2005-11-21'' and timestamp ''2005-11-29'' and dtpropid=''noao''');
REPLACE INTO C4M05BD VALUES ('20051126', '20051126', '20051204',
  'start_date between timestamp ''2005-11-26'' and timestamp ''2005-12-04'' and dtpropid=''2005B-0043''');
REPLACE INTO C4M05BD VALUES ('20051201', '20051201', '20051209',
  'start_date between timestamp ''2005-12-01'' and timestamp ''2005-12-09'' and dtpropid=''noao''');
REPLACE INTO C4M05BD VALUES ('20051205', '20051205', '20051211',
  'start_date between timestamp ''2005-12-05'' and timestamp ''2005-12-11'' and dtpropid=''2005B-0043''');
REPLACE INTO C4M05BD VALUES ('20051223', '20051223', '20051228',
  'start_date between timestamp ''2005-12-23'' and timestamp ''2005-12-28''');
REPLACE INTO C4M05BD VALUES ('20051229', '20051229', '20060101',
  'start_date between timestamp ''2005-12-29'' and timestamp ''2006-01-01''');
REPLACE INTO C4M05BD VALUES ('20060102', '20060102', '20060105',
  'start_date between timestamp ''2006-01-02'' and timestamp ''2006-01-05''');

