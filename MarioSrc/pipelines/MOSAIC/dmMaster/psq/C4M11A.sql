REPLACE INTO PSQ VALUES('C4M11A', 'C4M11A', 'C4M11AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M11A (dataset) VALUES ('20110203');
REPLACE INTO C4M11A (dataset) VALUES ('20110207');
REPLACE INTO C4M11A (dataset) VALUES ('20110211');
REPLACE INTO C4M11A (dataset) VALUES ('20110220');
REPLACE INTO C4M11A (dataset) VALUES ('20110406');
REPLACE INTO C4M11A (dataset) VALUES ('20110408');
REPLACE INTO C4M11A (dataset) VALUES ('20110428');
REPLACE INTO C4M11A (dataset) VALUES ('20110429');
REPLACE INTO C4M11A (dataset) VALUES ('20110528');
REPLACE INTO C4M11A (dataset) VALUES ('20110529');
REPLACE INTO C4M11A (dataset) VALUES ('20110603');
REPLACE INTO C4M11A (dataset) VALUES ('20110605');
REPLACE INTO C4M11A (dataset) VALUES ('20110609');
REPLACE INTO C4M11A (dataset) VALUES ('20110627');
REPLACE INTO C4M11A (dataset) VALUES ('20110628');
REPLACE INTO C4M11A (dataset) VALUES ('20110701');
REPLACE INTO C4M11A (dataset) VALUES ('20110705');
REPLACE INTO C4M11A (dataset) VALUES ('20110709');

REPLACE INTO C4M11AD VALUES ('20110203', '20110203', '20110206',
  '"startDate" between timestamp ''2011-02-03'' and timestamp ''2011-02-06''');
REPLACE INTO C4M11AD VALUES ('20110207', '20110207', '20110210',
  '"startDate" between timestamp ''2011-02-07'' and timestamp ''2011-02-10''');
REPLACE INTO C4M11AD VALUES ('20110211', '20110211', '20110212',
  '"startDate" between timestamp ''2011-02-11'' and timestamp ''2011-02-12''');
REPLACE INTO C4M11AD VALUES ('20110220', '20110220', '20110221',
  '"startDate" between timestamp ''2011-02-20'' and timestamp ''2011-02-21''');
REPLACE INTO C4M11AD VALUES ('20110406', '20110406', '20110407',
  '"startDate" between timestamp ''2011-04-06'' and timestamp ''2011-04-07''');
REPLACE INTO C4M11AD VALUES ('20110408', '20110408', '20110409',
  '"startDate" between timestamp ''2011-04-08'' and timestamp ''2011-04-09''');
REPLACE INTO C4M11AD VALUES ('20110428', '20110428', '20110428',
  '"startDate" between timestamp ''2011-04-28'' and timestamp ''2011-04-28''');
REPLACE INTO C4M11AD VALUES ('20110429', '20110429', '20110502',
  '"startDate" between timestamp ''2011-04-29'' and timestamp ''2011-05-02''');
REPLACE INTO C4M11AD VALUES ('20110528', '20110528', '20110528',
  '"startDate" between timestamp ''2011-05-28'' and timestamp ''2011-05-28''');
REPLACE INTO C4M11AD VALUES ('20110529', '20110529', '20110602',
  '"startDate" between timestamp ''2011-05-29'' and timestamp ''2011-06-02''');
REPLACE INTO C4M11AD VALUES ('20110603', '20110603', '20110604',
  '"startDate" between timestamp ''2011-06-03'' and timestamp ''2011-06-04''');
REPLACE INTO C4M11AD VALUES ('20110605', '20110605', '20110608',
  '"startDate" between timestamp ''2011-06-05'' and timestamp ''2011-06-08''');
REPLACE INTO C4M11AD VALUES ('20110609', '20110609', '20110610',
  '"startDate" between timestamp ''2011-06-09'' and timestamp ''2011-06-10''');
REPLACE INTO C4M11AD VALUES ('20110627', '20110627', '20110627',
  '"startDate" between timestamp ''2011-06-27'' and timestamp ''2011-06-27''');
REPLACE INTO C4M11AD VALUES ('20110628', '20110628', '20110630',
  '"startDate" between timestamp ''2011-06-28'' and timestamp ''2011-06-30''');
REPLACE INTO C4M11AD VALUES ('20110701', '20110701', '20110704',
  '"startDate" between timestamp ''2011-07-01'' and timestamp ''2011-07-04''');
REPLACE INTO C4M11AD VALUES ('20110705', '20110705', '20110708',
  '"startDate" between timestamp ''2011-07-05'' and timestamp ''2011-07-08''');
REPLACE INTO C4M11AD VALUES ('20110709', '20110709', '20110711',
  '"startDate" between timestamp ''2011-07-09'' and timestamp ''2011-07-11''');
