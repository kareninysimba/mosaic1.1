REPLACE INTO PSQ VALUES('C4M05A', 'C4M05A', 'C4M05AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M05A (dataset) VALUES ('20050204');
REPLACE INTO C4M05A (dataset) VALUES ('20050207');
REPLACE INTO C4M05A (dataset) VALUES ('20050212');
REPLACE INTO C4M05A (dataset) VALUES ('20050214');
REPLACE INTO C4M05A (dataset) VALUES ('20050312');
REPLACE INTO C4M05A (dataset) VALUES ('20050314');
REPLACE INTO C4M05A (dataset) VALUES ('20050402');
REPLACE INTO C4M05A (dataset) VALUES ('20050406');
REPLACE INTO C4M05A (dataset) VALUES ('20050409');
REPLACE INTO C4M05A (dataset) VALUES ('20050429');
REPLACE INTO C4M05A (dataset) VALUES ('20050503');
REPLACE INTO C4M05A (dataset) VALUES ('20050507');
REPLACE INTO C4M05A (dataset) VALUES ('20050511');
REPLACE INTO C4M05A (dataset) VALUES ('20050607');
REPLACE INTO C4M05A (dataset) VALUES ('20050710');

REPLACE INTO C4M05AD VALUES ('20050204', '20050204', '20050206',
  'start_date between timestamp ''2005-02-04'' and timestamp ''2005-02-06''');
REPLACE INTO C4M05AD VALUES ('20050207', '20050207', '20050211',
  'start_date between timestamp ''2005-02-07'' and timestamp ''2005-02-11''');
REPLACE INTO C4M05AD VALUES ('20050212', '20050212', '20050213',
  'start_date between timestamp ''2005-02-12'' and timestamp ''2005-02-13''');
REPLACE INTO C4M05AD VALUES ('20050214', '20050214', '20050216',
  'start_date between timestamp ''2005-02-14'' and timestamp ''2005-02-16''');
REPLACE INTO C4M05AD VALUES ('20050312', '20050312', '20050313',
  'start_date between timestamp ''2005-03-12'' and timestamp ''2005-03-13''');
REPLACE INTO C4M05AD VALUES ('20050314', '20050314', '20050317',
  'start_date between timestamp ''2005-03-14'' and timestamp ''2005-03-17''');
REPLACE INTO C4M05AD VALUES ('20050402', '20050402', '20050405',
  'start_date between timestamp ''2005-04-02'' and timestamp ''2005-04-05''');
REPLACE INTO C4M05AD VALUES ('20050406', '20050406', '20050408',
  'start_date between timestamp ''2005-04-06'' and timestamp ''2005-04-08''');
REPLACE INTO C4M05AD VALUES ('20050409', '20050409', '20050412',
  'start_date between timestamp ''2005-04-09'' and timestamp ''2005-04-12''');
REPLACE INTO C4M05AD VALUES ('20050429', '20050429', '20050502',
  'start_date between timestamp ''2005-04-29'' and timestamp ''2005-05-02''');
REPLACE INTO C4M05AD VALUES ('20050503', '20050503', '20050506',
  'start_date between timestamp ''2005-05-03'' and timestamp ''2005-05-06''');
REPLACE INTO C4M05AD VALUES ('20050507', '20050507', '20050510',
  'start_date between timestamp ''2005-05-07'' and timestamp ''2005-05-10''');
REPLACE INTO C4M05AD VALUES ('20050511', '20050511', '20050514',
  'start_date between timestamp ''2005-05-11'' and timestamp ''2005-05-14''');
REPLACE INTO C4M05AD VALUES ('20050607', '20050607', '20050611',
  'start_date between timestamp ''2005-06-07'' and timestamp ''2005-06-11''');
REPLACE INTO C4M05AD VALUES ('20050710', '20050710', '20050710',
  'start_date between timestamp ''2005-07-10'' and timestamp ''2005-07-10''');
