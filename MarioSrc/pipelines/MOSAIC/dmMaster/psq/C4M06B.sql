REPLACE INTO PSQ VALUES('C4M06B', 'C4M06B', 'C4M06BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M06B (dataset) VALUES ('20060815');
REPLACE INTO C4M06B (dataset) VALUES ('20060820');
REPLACE INTO C4M06B (dataset) VALUES ('20060823');
REPLACE INTO C4M06B (dataset) VALUES ('20060826');
REPLACE INTO C4M06B (dataset) VALUES ('20060829');
REPLACE INTO C4M06B (dataset) VALUES ('20060831');
REPLACE INTO C4M06B (dataset) VALUES ('20060911');
REPLACE INTO C4M06B (dataset) VALUES ('20060914');
REPLACE INTO C4M06B (dataset) VALUES ('20060919');
REPLACE INTO C4M06B (dataset) VALUES ('20060924');
REPLACE INTO C4M06B (dataset) VALUES ('20061012');
REPLACE INTO C4M06B (dataset) VALUES ('20061017');
REPLACE INTO C4M06B (dataset) VALUES ('20061023');
REPLACE INTO C4M06B (dataset) VALUES ('20061029');
REPLACE INTO C4M06B (dataset) VALUES ('20061110');
REPLACE INTO C4M06B (dataset) VALUES ('20061113');
REPLACE INTO C4M06B (dataset) VALUES ('20061117');
REPLACE INTO C4M06B (dataset) VALUES ('20061121');
REPLACE INTO C4M06B (dataset) VALUES ('20061124');
REPLACE INTO C4M06B (dataset) VALUES ('20061212');
REPLACE INTO C4M06B (dataset) VALUES ('20061217');
REPLACE INTO C4M06B (dataset) VALUES ('20061224');
REPLACE INTO C4M06B (dataset) VALUES ('20070108');
REPLACE INTO C4M06B (dataset) VALUES ('20070115');

REPLACE INTO C4M06BD VALUES ('20060815', '20060815', '20060819',
  '"startDate" between timestamp ''2006-08-15'' and timestamp ''2006-08-19''');
REPLACE INTO C4M06BD VALUES ('20060820', '20060820', '20060822',
  '"startDate" between timestamp ''2006-08-20'' and timestamp ''2006-08-22''');
REPLACE INTO C4M06BD VALUES ('20060823', '20060823', '20060825',
  '"startDate" between timestamp ''2006-08-23'' and timestamp ''2006-08-25''');
REPLACE INTO C4M06BD VALUES ('20060826', '20060826', '20060828',
  '"startDate" between timestamp ''2006-08-26'' and timestamp ''2006-08-28''');
REPLACE INTO C4M06BD VALUES ('20060829', '20060829', '20060830',
  '"startDate" between timestamp ''2006-08-29'' and timestamp ''2006-08-30''');
REPLACE INTO C4M06BD VALUES ('20060831', '20060831', '20060906',
  '"startDate" between timestamp ''2006-08-31'' and timestamp ''2006-09-06''');
REPLACE INTO C4M06BD VALUES ('20060911', '20060911', '20060913',
  '"startDate" between timestamp ''2006-09-11'' and timestamp ''2006-09-13''');
REPLACE INTO C4M06BD VALUES ('20060914', '20060914', '20060918',
  '"startDate" between timestamp ''2006-09-14'' and timestamp ''2006-09-18''');
REPLACE INTO C4M06BD VALUES ('20060919', '20060919', '20060923',
  '"startDate" between timestamp ''2006-09-19'' and timestamp ''2006-09-23''');
REPLACE INTO C4M06BD VALUES ('20060924', '20060924', '20060927',
  '"startDate" between timestamp ''2006-09-24'' and timestamp ''2006-09-27''');
REPLACE INTO C4M06BD VALUES ('20061012', '20061012', '20061016',
  '"startDate" between timestamp ''2006-10-12'' and timestamp ''2006-10-16''');
REPLACE INTO C4M06BD VALUES ('20061017', '20061017', '20061022',
  '"startDate" between timestamp ''2006-10-17'' and timestamp ''2006-10-22''');
REPLACE INTO C4M06BD VALUES ('20061023', '20061023', '20061028',
  '"startDate" between timestamp ''2006-10-23'' and timestamp ''2006-10-28''');
REPLACE INTO C4M06BD VALUES ('20061029', '20061029', '20061031',
  '"startDate" between timestamp ''2006-10-29'' and timestamp ''2006-10-31''');
REPLACE INTO C4M06BD VALUES ('20061110', '20061110', '20061112',
  '"startDate" between timestamp ''2006-11-10'' and timestamp ''2006-11-12''');
REPLACE INTO C4M06BD VALUES ('20061113', '20061113', '20061116',
  '"startDate" between timestamp ''2006-11-13'' and timestamp ''2006-11-16''');
REPLACE INTO C4M06BD VALUES ('20061117', '20061117', '20061120',
  '"startDate" between timestamp ''2006-11-17'' and timestamp ''2006-11-20''');
REPLACE INTO C4M06BD VALUES ('20061121', '20061121', '20061123',
  '"startDate" between timestamp ''2006-11-21'' and timestamp ''2006-11-23''');
REPLACE INTO C4M06BD VALUES ('20061124', '20061124', '20061129',
  '"startDate" between timestamp ''2006-11-24'' and timestamp ''2006-11-29''');
REPLACE INTO C4M06BD VALUES ('20061212', '20061212', '20061216',
  '"startDate" between timestamp ''2006-12-12'' and timestamp ''2006-12-16''');
REPLACE INTO C4M06BD VALUES ('20061217', '20061217', '20061223',
  '"startDate" between timestamp ''2006-12-17'' and timestamp ''2006-12-23''');
REPLACE INTO C4M06BD VALUES ('20061224', '20061224', '20061228',
  '"startDate" between timestamp ''2006-12-24'' and timestamp ''2006-12-28''');
REPLACE INTO C4M06BD VALUES ('20070108', '20070108', '20070114',
  '"startDate" between timestamp ''2007-01-08'' and timestamp ''2007-01-14''');
REPLACE INTO C4M06BD VALUES ('20070115', '20070115', '20070118',
  '"startDate" between timestamp ''2007-01-15'' and timestamp ''2007-01-18''');
