REPLACE INTO PSQ VALUES('K4M14A', 'K4M14A', 'K4M14AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M14A (dataset) VALUES ('20140201');
REPLACE INTO K4M14A (dataset) VALUES ('20140204');
REPLACE INTO K4M14A (dataset) VALUES ('20140220');
REPLACE INTO K4M14A (dataset) VALUES ('20140223');
REPLACE INTO K4M14A (dataset) VALUES ('20140224');
REPLACE INTO K4M14A (dataset) VALUES ('20140319');
REPLACE INTO K4M14A (dataset) VALUES ('20140327');
REPLACE INTO K4M14A (dataset) VALUES ('20140328');
REPLACE INTO K4M14A (dataset) VALUES ('20140415');
REPLACE INTO K4M14A (dataset) VALUES ('20140416');
REPLACE INTO K4M14A (dataset) VALUES ('20140417');
REPLACE INTO K4M14A (dataset) VALUES ('20140418');
REPLACE INTO K4M14A (dataset) VALUES ('20140421');
REPLACE INTO K4M14A (dataset) VALUES ('20140423');
REPLACE INTO K4M14A (dataset) VALUES ('20140427');
REPLACE INTO K4M14A (dataset) VALUES ('20140429');
REPLACE INTO K4M14A (dataset) VALUES ('20140505');
REPLACE INTO K4M14A (dataset) VALUES ('20140507');
REPLACE INTO K4M14A (dataset) VALUES ('20140513');
REPLACE INTO K4M14A (dataset) VALUES ('20140527');
REPLACE INTO K4M14A (dataset) VALUES ('20140530');
REPLACE INTO K4M14A (dataset) VALUES ('20140603');
REPLACE INTO K4M14A (dataset) VALUES ('20140616');
REPLACE INTO K4M14A (dataset) VALUES ('20140617');
REPLACE INTO K4M14A (dataset) VALUES ('20140620');

REPLACE INTO K4M14AD VALUES ('20140201', '20140201', '20140203',
  'start_date between timestamp ''2014-02-01'' and timestamp ''2014-02-03''');
REPLACE INTO K4M14AD VALUES ('20140204', '20140204', '20140204',
  'start_date between timestamp ''2014-02-04'' and timestamp ''2014-02-04''');
REPLACE INTO K4M14AD VALUES ('20140220', '20140220', '20140222',
  'start_date between timestamp ''2014-02-20'' and timestamp ''2014-02-22''');
REPLACE INTO K4M14AD VALUES ('20140223', '20140223', '20140223',
  'start_date between timestamp ''2014-02-23'' and timestamp ''2014-02-23''');
REPLACE INTO K4M14AD VALUES ('20140224', '20140224', '20140226',
  'start_date between timestamp ''2014-02-24'' and timestamp ''2014-02-26''');
REPLACE INTO K4M14AD VALUES ('20140319', '20140319', '20140320',
  'start_date between timestamp ''2014-03-19'' and timestamp ''2014-03-20''');
REPLACE INTO K4M14AD VALUES ('20140327', '20140327', '20140327',
  'start_date between timestamp ''2014-03-27'' and timestamp ''2014-03-27''');
REPLACE INTO K4M14AD VALUES ('20140328', '20140328', '20140330',
  'start_date between timestamp ''2014-03-28'' and timestamp ''2014-03-30''');
REPLACE INTO K4M14AD VALUES ('20140415', '20140415', '20140415',
  'start_date between timestamp ''2014-04-15'' and timestamp ''2014-04-15''');
REPLACE INTO K4M14AD VALUES ('20140416', '20140416', '20140416',
  'start_date between timestamp ''2014-04-16'' and timestamp ''2014-04-16''');
REPLACE INTO K4M14AD VALUES ('20140417', '20140417', '20140417',
  'start_date between timestamp ''2014-04-17'' and timestamp ''2014-04-17''');
REPLACE INTO K4M14AD VALUES ('20140418', '20140418', '20140420',
  'start_date between timestamp ''2014-04-18'' and timestamp ''2014-04-20''');
REPLACE INTO K4M14AD VALUES ('20140421', '20140421', '20140422',
  'start_date between timestamp ''2014-04-21'' and timestamp ''2014-04-22''');
REPLACE INTO K4M14AD VALUES ('20140423', '20140423', '20140426',
  'start_date between timestamp ''2014-04-23'' and timestamp ''2014-04-26''');
REPLACE INTO K4M14AD VALUES ('20140427', '20140427', '20140428',
  'start_date between timestamp ''2014-04-27'' and timestamp ''2014-04-28''');
REPLACE INTO K4M14AD VALUES ('20140429', '20140429', '20140430',
  'start_date between timestamp ''2014-04-29'' and timestamp ''2014-04-30''');
REPLACE INTO K4M14AD VALUES ('20140505', '20140505', '20140506',
  'start_date between timestamp ''2014-05-05'' and timestamp ''2014-05-06''');
REPLACE INTO K4M14AD VALUES ('20140507', '20140507', '20140507',
  'start_date between timestamp ''2014-05-07'' and timestamp ''2014-05-07''');
REPLACE INTO K4M14AD VALUES ('20140513', '20140513', '20140513',
  'start_date between timestamp ''2014-05-13'' and timestamp ''2014-05-13''');
REPLACE INTO K4M14AD VALUES ('20140527', '20140527', '20140529',
  'start_date between timestamp ''2014-05-27'' and timestamp ''2014-05-29''');
REPLACE INTO K4M14AD VALUES ('20140530', '20140530', '20140602',
  'start_date between timestamp ''2014-05-30'' and timestamp ''2014-06-02''');
REPLACE INTO K4M14AD VALUES ('20140603', '20140603', '20140603',
  'start_date between timestamp ''2014-06-03'' and timestamp ''2014-06-03''');
REPLACE INTO K4M14AD VALUES ('20140616', '20140616', '20140616',
  'start_date between timestamp ''2014-06-16'' and timestamp ''2014-06-16''');
REPLACE INTO K4M14AD VALUES ('20140617', '20140617', '20140619',
  'start_date between timestamp ''2014-06-17'' and timestamp ''2014-06-19''');
REPLACE INTO K4M14AD VALUES ('20140620', '20140620', '20140622',
  'start_date between timestamp ''2014-06-20'' and timestamp ''2014-06-22''');
