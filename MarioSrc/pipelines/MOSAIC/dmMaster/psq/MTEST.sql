REPLACE INTO PSQ VALUES('MTEST', 'MTEST', 'MTESTD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

DELETE FROM MTEST;
REPLACE INTO MTEST (dataset) VALUES ('9Z4F5O');
REPLACE INTO MTEST (dataset) VALUES ('5O');

DELETE FROM MTESTD;
REPLACE INTO MTESTD VALUES ('9Z4F5O', '20110104', '20110104',
'"startDate"= timestamp ''2011-01-04'' and ((obstype=''zero'' and substr(reference,3,7) between ''1293874'' and ''1293882'') or (filter like ''%k1003%'' and ((obstype=''dome flat'' and substr(reference,3,7) between ''1293490'' and ''1293494'') or obstype=''object'')))');
REPLACE INTO MTESTD VALUES ('5O', '20110104', '20110104',
'"startDate"= timestamp ''2011-01-04'' and filter like ''%k1003%'' and obstype=''object''');
