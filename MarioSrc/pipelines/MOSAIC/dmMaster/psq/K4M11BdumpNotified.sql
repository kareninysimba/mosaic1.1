-- MySQL dump 10.11
--
-- Host: localhost    Database: pipeline
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `K4M11B`
--

DROP TABLE IF EXISTS `K4M11B`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `K4M11B` (
  `dataset` char(32) NOT NULL default '',
  `priority` int(11) default '1',
  `status` char(16) default 'pending',
  `submitted` char(19) default NULL,
  `completed` char(19) default NULL,
  `notified` char(19) default NULL,
  `comments` varchar(512) default NULL,
  PRIMARY KEY  (`dataset`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `K4M11B`
--

LOCK TABLES `K4M11B` WRITE;
/*!40000 ALTER TABLE `K4M11B` DISABLE KEYS */;
INSERT INTO `K4M11B` VALUES ('20110915',1,'completed','2011-11-09T18:40','2011-11-10T01:01','-',NULL),('20110922',1,'submitted','2011-11-10T15:55','2011-09-26T22:32','2011-10-28T18:46:55','pgr removal error'),('20110924',1,'resubmit','2011-09-30T19:36','2011-10-01T23:44','2011-10-02T08:18:09','pgr removal error'),('20111004',1,'resubmit','2011-10-07T17:16','2011-10-07T21:07','2011-10-07T14:54:42','pgr removal error'),('20111007',1,'resubmit','2011-10-18T19:05','2011-10-18T20:11','2011-10-13T10:21:20','pgr removal error.  The gain changed on the last night requiring division into separate nights and sky flats to take the place of missing dome flats.'),('20111026',1,'resubmit','2011-10-29T17:21','2011-10-29T17:34','2011-10-29T10:45:02','pgr removal error'),('20111024',1,'completed','2011-10-27T21:33','2011-10-28T02:35','2011-10-28T11:46:55',NULL),('20111028',1,'resubmit','2011-11-02T15:31','2011-11-03T10:02','2011-11-03T15:34:51','pgr removal error'),('20111031',1,'resubmit','2011-11-04T15:27','2011-11-05T07:52','2011-11-07T08:33:15','pgr removal error'),('20111220',1,'pending',NULL,NULL,NULL,NULL),('20120117',1,'pending',NULL,NULL,NULL,NULL),('20120119',1,'pending',NULL,NULL,NULL,NULL),('20111103',1,'resubmit','2011-11-07T22:20','2011-11-07T23:18','2011-11-08T09:47:59','pgr removal error'),('20111021',1,'resubmit','2011-10-26T15:39','2011-10-26T16:19','2011-10-26T10:56:36','pgr removal error. T&E');
/*!40000 ALTER TABLE `K4M11B` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-11-10 16:02:55
