REPLACE INTO PSQ VALUES('K1M11A', 'K1M11A', 'K1M11AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K1M11A (dataset) VALUES ('20110328');
REPLACE INTO K1M11A (dataset) VALUES ('20110401');
REPLACE INTO K1M11A (dataset) VALUES ('20110405');
REPLACE INTO K1M11A (dataset) VALUES ('20110408');
REPLACE INTO K1M11A (dataset) VALUES ('20110614');
REPLACE INTO K1M11A (dataset) VALUES ('2011061l');
REPLACE INTO K1M11A (dataset) VALUES ('20110619');
REPLACE INTO K1M11A (dataset) VALUES ('20110622');

REPLACE INTO K1M11AD VALUES ('20110328', '20110328', '20110331',
  '"startDate" between timestamp ''2011-03-28'' and timestamp ''2011-03-31''');
REPLACE INTO K1M11AD VALUES ('20110401', '20110401', '20110404',
  '"startDate" between timestamp ''2011-04-01'' and timestamp ''2011-04-04''');
REPLACE INTO K1M11AD VALUES ('20110405', '20110405', '20110407',
  '"startDate" between timestamp ''2011-04-05'' and timestamp ''2011-04-07''');
REPLACE INTO K1M11AD VALUES ('20110408', '20110408', '20110411',
  '"startDate" between timestamp ''2011-04-08'' and timestamp ''2011-04-11''');
REPLACE INTO K1M11AD VALUES ('20110614', '20110614', '20110614',
  '"startDate" between timestamp ''2011-06-14'' and timestamp ''2011-06-14''');
REPLACE INTO K1M11AD VALUES ('2011061l', '20110615', '20110618',
  '"startDate" between timestamp ''2011-06-15'' and timestamp ''2011-06-18''');
REPLACE INTO K1M11AD VALUES ('20110619', '20110619', '20110621',
  '"startDate" between timestamp ''2011-06-19'' and timestamp ''2011-06-21''');
REPLACE INTO K1M11AD VALUES ('20110622', '20110622', '20110626',
  '"startDate" between timestamp ''2011-06-22'' and timestamp ''2011-06-26''');
