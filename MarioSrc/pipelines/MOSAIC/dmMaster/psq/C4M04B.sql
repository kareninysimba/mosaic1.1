REPLACE INTO PSQ VALUES('C4M04B', 'C4M04B', 'C4M04BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M04B (dataset) VALUES ('20040810');
REPLACE INTO C4M04B (dataset) VALUES ('20040815');
REPLACE INTO C4M04B (dataset) VALUES ('20040908');
REPLACE INTO C4M04B (dataset) VALUES ('20040909');
REPLACE INTO C4M04B (dataset) VALUES ('20040917');
REPLACE INTO C4M04B (dataset) VALUES ('20040918');
REPLACE INTO C4M04B (dataset) VALUES ('20041004');
REPLACE INTO C4M04B (dataset) VALUES ('20041103');
REPLACE INTO C4M04B (dataset) VALUES ('20041202');
REPLACE INTO C4M04B (dataset) VALUES ('20041202');
REPLACE INTO C4M04B (dataset) VALUES ('20041214');
REPLACE INTO C4M04B (dataset) VALUES ('20050106');
REPLACE INTO C4M04B (dataset) VALUES ('20050111');

REPLACE INTO C4M04BD VALUES ('20040810', '20040811', '20040814',
  'start_date between timestamp ''2004-08-11'' and timestamp ''2004-08-14''');
REPLACE INTO C4M04BD VALUES ('20040815', '20040815', '20040819',
  'start_date between timestamp ''2004-08-15'' and timestamp ''2004-08-19''');
REPLACE INTO C4M04BD VALUES ('20040908', '20040908', '20040914',
  'start_date between timestamp ''2004-09-08'' and timestamp ''2004-09-14'' and dtpropid=''noao''');
REPLACE INTO C4M04BD VALUES ('20040909', '20040909', '20040916',
  'start_date between timestamp ''2004-09-09'' and timestamp ''2004-09-16'' and dtpropid=''2004B-0208''');
REPLACE INTO C4M04BD VALUES ('20040917', '20040917', '20040919',
  'start_date between timestamp ''2004-09-17'' and timestamp ''2004-09-19'' and dtpropid=''2004B-0437''');
REPLACE INTO C4M04BD VALUES ('20040918', '20040918', '20040918',
  'start_date between timestamp ''2004-09-18'' and timestamp ''2004-09-18'' and dtpropid=''2004B-0463''');
REPLACE INTO C4M04BD VALUES ('20041004', '20041004', '20041025',
  'start_date between timestamp ''2004-10-04'' and timestamp ''2004-10-25''');
REPLACE INTO C4M04BD VALUES ('20041103', '20041103', '20041121',
  'start_date between timestamp ''2004-11-03'' and timestamp ''2004-11-21''');
REPLACE INTO C4M04BD VALUES ('20041202', '20041202', '20041214',
  'start_date between timestamp ''2004-12-02'' and timestamp ''2004-12-14''');
REPLACE INTO C4M04BD VALUES ('20041214', '20041214', '20041220',
  'start_date between timestamp ''2004-12-02'' and timestamp ''2004-12-20''');
REPLACE INTO C4M04BD VALUES ('20050106', '20050106', '20050110',
  'start_date between timestamp ''2005-01-06'' and timestamp ''2005-01-10''');
REPLACE INTO C4M04BD VALUES ('20050111', '20050111', '20050118',
  'start_date between timestamp ''2005-01-11'' and timestamp ''2005-01-18''');
