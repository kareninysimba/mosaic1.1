REPLACE INTO PSQ VALUES('K4M11A', 'K4M11A', 'K4M11AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M11A (dataset) VALUES ('20110221');
REPLACE INTO K4M11A (dataset) VALUES ('20110222');
REPLACE INTO K4M11A (dataset) VALUES ('20110224');
REPLACE INTO K4M11A (dataset) VALUES ('20110228');
REPLACE INTO K4M11A (dataset) VALUES ('20110420');
REPLACE INTO K4M11A (dataset) VALUES ('20110421');
REPLACE INTO K4M11A (dataset) VALUES ('20110425');
REPLACE INTO K4M11A (dataset) VALUES ('20110428');
REPLACE INTO K4M11A (dataset) VALUES ('20110519');
REPLACE INTO K4M11A (dataset) VALUES ('20110520');
REPLACE INTO K4M11A (dataset) VALUES ('20110523');
REPLACE INTO K4M11A (dataset) VALUES ('20110527');
REPLACE INTO K4M11A (dataset) VALUES ('20110531');
REPLACE INTO K4M11A (dataset) VALUES ('20110603');

REPLACE INTO K4M11AD VALUES ('20110221', '20110221', '20110221',
  '"startDate" between timestamp ''2011-02-21'' and timestamp ''2011-02-21''');
REPLACE INTO K4M11AD VALUES ('20110222', '20110222', '20110223',
  '"startDate" between timestamp ''2011-02-22'' and timestamp ''2011-02-23''');
REPLACE INTO K4M11AD VALUES ('20110224', '20110224', '20110227',
  '"startDate" between timestamp ''2011-02-24'' and timestamp ''2011-02-27''');
REPLACE INTO K4M11AD VALUES ('20110228', '20110228', '20110303',
  '"startDate" between timestamp ''2011-02-28'' and timestamp ''2011-03-03''');
REPLACE INTO K4M11AD VALUES ('20110420', '20110420', '20110420',
  '"startDate" between timestamp ''2011-04-20'' and timestamp ''2011-04-20''');
REPLACE INTO K4M11AD VALUES ('20110421', '20110421', '20110424',
  '"startDate" between timestamp ''2011-04-21'' and timestamp ''2011-04-24''');
REPLACE INTO K4M11AD VALUES ('20110425', '20110425', '20110427',
  '"startDate" between timestamp ''2011-04-25'' and timestamp ''2011-04-27''');
REPLACE INTO K4M11AD VALUES ('20110428', '20110428', '20110501',
  '"startDate" between timestamp ''2011-04-28'' and timestamp ''2011-05-01''');
REPLACE INTO K4M11AD VALUES ('20110519', '20110519', '20110519',
  '"startDate" between timestamp ''2011-05-19'' and timestamp ''2011-05-19''');
REPLACE INTO K4M11AD VALUES ('20110520', '20110520', '20110522',
  '"startDate" between timestamp ''2011-05-20'' and timestamp ''2011-05-22''');
REPLACE INTO K4M11AD VALUES ('20110523', '20110523', '20110526',
  '"startDate" between timestamp ''2011-05-23'' and timestamp ''2011-05-26''');
REPLACE INTO K4M11AD VALUES ('20110527', '20110527', '20110530',
  '"startDate" between timestamp ''2011-05-27'' and timestamp ''2011-05-30''');
REPLACE INTO K4M11AD VALUES ('20110531', '20110531', '20110602',
  '"startDate" between timestamp ''2011-05-31'' and timestamp ''2011-06-02''');
REPLACE INTO K4M11AD VALUES ('20110603', '20110603', '20110605',
  '"startDate" between timestamp ''2011-06-03'' and timestamp ''2011-06-05''');
