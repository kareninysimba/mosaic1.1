REPLACE INTO PSQ VALUES('C4M07B', 'C4M07B', 'C4M07BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M07B (dataset) VALUES ('20070810');
REPLACE INTO C4M07B (dataset) VALUES ('20070813');
REPLACE INTO C4M07B (dataset) VALUES ('20070816');
REPLACE INTO C4M07B (dataset) VALUES ('20070819');
REPLACE INTO C4M07B (dataset) VALUES ('20070907');
REPLACE INTO C4M07B (dataset) VALUES ('20070911');
REPLACE INTO C4M07B (dataset) VALUES ('20070915');
REPLACE INTO C4M07B (dataset) VALUES ('20070918');
REPLACE INTO C4M07B (dataset) VALUES ('20070930');
REPLACE INTO C4M07B (dataset) VALUES ('20071004');
REPLACE INTO C4M07B (dataset) VALUES ('20071008');
REPLACE INTO C4M07B (dataset) VALUES ('20071011');
REPLACE INTO C4M07B (dataset) VALUES ('20071014');
REPLACE INTO C4M07B (dataset) VALUES ('20071018');
REPLACE INTO C4M07B (dataset) VALUES ('20071030');
REPLACE INTO C4M07B (dataset) VALUES ('20071103');
REPLACE INTO C4M07B (dataset) VALUES ('20071106');
REPLACE INTO C4M07B (dataset) VALUES ('20071109');
REPLACE INTO C4M07B (dataset) VALUES ('20071113');
REPLACE INTO C4M07B (dataset) VALUES ('20071117');
REPLACE INTO C4M07B (dataset) VALUES ('20071130');
REPLACE INTO C4M07B (dataset) VALUES ('20071203');
REPLACE INTO C4M07B (dataset) VALUES ('20071207');
REPLACE INTO C4M07B (dataset) VALUES ('20071211');
REPLACE INTO C4M07B (dataset) VALUES ('20071215');
REPLACE INTO C4M07B (dataset) VALUES ('20071229');
REPLACE INTO C4M07B (dataset) VALUES ('20080102');
REPLACE INTO C4M07B (dataset) VALUES ('20080106');

REPLACE INTO C4M07BD VALUES ('20070810', '20070810', '20070812',
  '"startDate" between timestamp ''2007-08-10'' and timestamp ''2007-08-12''');
REPLACE INTO C4M07BD VALUES ('20070813', '20070813', '20070815',
  '"startDate" between timestamp ''2007-08-13'' and timestamp ''2007-08-15''');
REPLACE INTO C4M07BD VALUES ('20070816', '20070816', '20070818',
  '"startDate" between timestamp ''2007-08-16'' and timestamp ''2007-08-18''');
REPLACE INTO C4M07BD VALUES ('20070819', '20070819', '20070822',
  '"startDate" between timestamp ''2007-08-19'' and timestamp ''2007-08-22''');
REPLACE INTO C4M07BD VALUES ('20070907', '20070907', '20070910',
  '"startDate" between timestamp ''2007-09-07'' and timestamp ''2007-09-10''');
REPLACE INTO C4M07BD VALUES ('20070911', '20070911', '20070914',
  '"startDate" between timestamp ''2007-09-11'' and timestamp ''2007-09-14''');
REPLACE INTO C4M07BD VALUES ('20070915', '20070915', '20070917',
  '"startDate" between timestamp ''2007-09-15'' and timestamp ''2007-09-17''');
REPLACE INTO C4M07BD VALUES ('20070918', '20070919', '20070919',
  '"startDate" between timestamp ''2007-09-19'' and timestamp ''2007-09-19''');
REPLACE INTO C4M07BD VALUES ('20070930', '20070930', '20071003',
  '"startDate" between timestamp ''2007-09-30'' and timestamp ''2007-10-03''');
REPLACE INTO C4M07BD VALUES ('20071004', '20071004', '20071007',
  '"startDate" between timestamp ''2007-10-04'' and timestamp ''2007-10-07''');
REPLACE INTO C4M07BD VALUES ('20071008', '20071008', '20071010',
  '"startDate" between timestamp ''2007-10-08'' and timestamp ''2007-10-10''');
REPLACE INTO C4M07BD VALUES ('20071011', '20071011', '20071013',
  '"startDate" between timestamp ''2007-10-11'' and timestamp ''2007-10-13''');
REPLACE INTO C4M07BD VALUES ('20071014', '20071014', '20071017',
  '"startDate" between timestamp ''2007-10-14'' and timestamp ''2007-10-17''');
REPLACE INTO C4M07BD VALUES ('20071018', '20071018', '20071022',
  '"startDate" between timestamp ''2007-10-18'' and timestamp ''2007-10-22''');
REPLACE INTO C4M07BD VALUES ('20071030', '20071030', '20071102',
  '"startDate" between timestamp ''2007-10-30'' and timestamp ''2007-11-02''');
REPLACE INTO C4M07BD VALUES ('20071103', '20071103', '20071105',
  '"startDate" between timestamp ''2007-11-03'' and timestamp ''2007-11-05''');
REPLACE INTO C4M07BD VALUES ('20071106', '20071106', '20071108',
  '"startDate" between timestamp ''2007-11-06'' and timestamp ''2007-11-08''');
REPLACE INTO C4M07BD VALUES ('20071109', '20071109', '20071112',
  '"startDate" between timestamp ''2007-11-09'' and timestamp ''2007-11-12''');
REPLACE INTO C4M07BD VALUES ('20071113', '20071113', '20071116',
  '"startDate" between timestamp ''2007-11-13'' and timestamp ''2007-11-16''');
REPLACE INTO C4M07BD VALUES ('20071117', '20071117', '20071118',
  '"startDate" between timestamp ''2007-11-17'' and timestamp ''2007-11-18''');
REPLACE INTO C4M07BD VALUES ('20071130', '20071202', '20071202',
  '"startDate" between timestamp ''2007-12-02'' and timestamp ''2007-12-02''');
REPLACE INTO C4M07BD VALUES ('20071203', '20071203', '20071206',
  '"startDate" between timestamp ''2007-12-03'' and timestamp ''2007-12-06''');
REPLACE INTO C4M07BD VALUES ('20071207', '20071207', '20071210',
  '"startDate" between timestamp ''2007-12-07'' and timestamp ''2007-12-10''');
REPLACE INTO C4M07BD VALUES ('20071211', '20071211', '20071214',
  '"startDate" between timestamp ''2007-12-11'' and timestamp ''2007-12-14''');
REPLACE INTO C4M07BD VALUES ('20071215', '20071215', '20071218',
  '"startDate" between timestamp ''2007-12-15'' and timestamp ''2007-12-18''');
REPLACE INTO C4M07BD VALUES ('20071229', '20071229', '20080101',
  '"startDate" between timestamp ''2007-12-29'' and timestamp ''2008-01-01''');
REPLACE INTO C4M07BD VALUES ('20080102', '20080102', '20080105',
  '"startDate" between timestamp ''2008-01-02'' and timestamp ''2008-01-05''');
REPLACE INTO C4M07BD VALUES ('20080106', '20080106', '20080108',
  '"startDate" between timestamp ''2008-01-06'' and timestamp ''2008-01-08''');
