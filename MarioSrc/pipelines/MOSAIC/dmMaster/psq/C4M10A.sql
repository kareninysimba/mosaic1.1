REPLACE INTO PSQ VALUES('C4M10A', 'C4M10A', 'C4M10AD', 'MOSAIC', 'dir', 'enabled', 'proctype=''Raw'' and dtinstru=''mosaic_2'' and obstype not in (''focus'',''test'')');

DROP TABLE IF EXISTS `C4M10A`;
CREATE TABLE `C4M10A` (
  `dataset` char(32) NOT NULL default '',
  `priority` int(11) default '1',
  `status` char(16) default 'pending',
  `submitted` char(19) default NULL,
  `completed` char(19) default NULL,
  `comments` varchar(512) default NULL,
  PRIMARY KEY  (`dataset`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


LOCK TABLES `C4M10A` WRITE;
INSERT INTO `C4M10A` VALUES
('20100211',1,'completed','2010-02-16T00:22','2010-02-17T02:54',NULL),
('20100213',1,'completed','2010-02-24T17:20','2010-02-25T01:42',NULL),
('20100216',1,'completed','2010-03-08T16:00','2010-03-08T16:32',NULL),
('20100311',1,'completed','2010-03-16T19:05','2010-03-17T03:46',NULL),
('20100315',1,'completed','2010-03-23T00:11','2010-03-23T21:54',NULL),
('20100319',1,'completed','2010-03-23T22:15','2010-03-24T08:20',NULL),
('20100410',1,'pending','NULL','NULL',NULL),
('20100414',1,'pending','NULL','NULL',NULL),
('20100419',1,'pending','NULL','NULL',NULL),
('20100423',1,'pending','NULL','NULL',NULL),
('20100428',1,'pending','NULL','NULL',NULL),
('20100508',1,'pending','NULL','NULL',NULL),
('20100512',1,'pending','NULL','NULL',NULL),
('20100606',1,'pending','NULL','NULL',NULL),
('20100608',1,'pending','NULL','NULL',NULL),
('20100611',1,'pending','NULL','NULL',NULL),
('20100708',1,'pending','NULL','NULL',NULL),
('20100712',1,'pending','NULL','NULL',NULL),
('20100716',1,'pending','NULL','NULL',NULL),
('20100720',1,'pending','NULL','NULL',NULL),
('20100723',1,'pending','NULL','NULL',NULL);
UNLOCK TABLES;


DROP TABLE IF EXISTS `C4M10AD`;
CREATE TABLE `C4M10AD` (
  `name` char(32) NOT NULL default '',
  `start` int(11) default NULL,
  `end` int(11) default NULL,
  `subquery` varchar(512) default NULL,
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


LOCK TABLES `C4M10AD` WRITE;
INSERT INTO `C4M10AD` VALUES
('20100211',20100211,20100212,'dtcaldat between \'2010-02-11%\' and \'2010-02-12%\' and sb_id not like \'ct23729%\''),
('20100213',20100213,20100215,'dtcaldat between \'2010-02-13%\' and \'2010-02-15%\''),
('20100216',20100216,20100219,'dtcaldat between \'2010-02-16%\' and \'2010-02-19%\''),
('20100311',20100311,20100314,'dtcaldat between \'2010-03-11%\' and \'2010-03-14%\''),
('20100315',20100315,20100318,'dtcaldat between \'2010-03-15%\' and \'2010-03-18%\''),
('20100319',20100319,20100320,'dtcaldat between \'2010-03-19%\' and \'2010-03-20%\''),
('20100410',20100410,20100413,'dtcaldat between \'2010-04-10%\' and \'2010-04-13%\''),
('20100414',20100414,20100418,'dtcaldat between \'2010-04-14%\' and \'2010-04-18%\''),
('20100419',20100419,20100422,'dtcaldat between \'2010-04-19%\' and \'2010-04-19%\''),
('20100423',20100423,20100425,'dtcaldat between \'2010-04-23%\' and \'2010-04-25%\''),
('20100428',20100428,20100501,'dtcaldat between \'2010-04-28%\' and \'2010-05-01%\''),
('20100508',20100508,20100511,'dtcaldat between \'2010-05-08%\' and \'2010-05-11%\''),
('20100512',20100512,20100513,'dtcaldat between \'2010-05-12%\' and \'2010-05-13%\''),
('20100606',20100606,20100607,'dtcaldat between \'2010-06-06%\' and \'2010-06-07%\''),
('20100608',20100608,20100610,'dtcaldat between \'2010-06-08%\' and \'2010-06-10%\''),
('20100611',20100611,20100612,'dtcaldat between \'2010-06-11%\' and \'2010-06-12%\''),
('20100708',20100708,20100711,'dtcaldat between \'2010-07-08%\' and \'2010-07-11%\''),
('20100712',20100712,20100715,'dtcaldat between \'2010-07-12%\' and \'2010-07-15%\''),
('20100716',20100716,20100719,'dtcaldat between \'2010-07-16%\' and \'2010-07-19%\''),
('20100720',20100720,20100722,'dtcaldat between \'2010-07-20%\' and \'2010-07-22%\''),
('20100723',20100723,20100725,'dtcaldat between \'2010-07-23%\' and \'2010-07-25%\'');
UNLOCK TABLES;
