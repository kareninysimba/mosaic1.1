REPLACE INTO PSQ VALUES('K4M15A', 'K4M15A', 'K4M15AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M15A (dataset) VALUES ('20150205');
REPLACE INTO K4M15A (dataset) VALUES ('20150209');
REPLACE INTO K4M15A (dataset) VALUES ('20150210');
REPLACE INTO K4M15A (dataset) VALUES ('20150211');
REPLACE INTO K4M15A (dataset) VALUES ('20150215');
REPLACE INTO K4M15A (dataset) VALUES ('20150217');
REPLACE INTO K4M15A (dataset) VALUES ('20150313');
REPLACE INTO K4M15A (dataset) VALUES ('20150314');
REPLACE INTO K4M15A (dataset) VALUES ('20150318');
REPLACE INTO K4M15A (dataset) VALUES ('20150319');
REPLACE INTO K4M15A (dataset) VALUES ('20150322');
REPLACE INTO K4M15A (dataset) VALUES ('20150413');
REPLACE INTO K4M15A (dataset) VALUES ('20150414');
REPLACE INTO K4M15A (dataset) VALUES ('20150424');
REPLACE INTO K4M15A (dataset) VALUES ('20150425');
REPLACE INTO K4M15A (dataset) VALUES ('20150507');
REPLACE INTO K4M15A (dataset) VALUES ('20150508');
REPLACE INTO K4M15A (dataset) VALUES ('20150511');
REPLACE INTO K4M15A (dataset) VALUES ('20150521');
REPLACE INTO K4M15A (dataset) VALUES ('20150522');
REPLACE INTO K4M15A (dataset) VALUES ('20150523');
REPLACE INTO K4M15A (dataset) VALUES ('20150525');
REPLACE INTO K4M15A (dataset) VALUES ('20150608');
REPLACE INTO K4M15A (dataset) VALUES ('20150609');
REPLACE INTO K4M15A (dataset) VALUES ('20150611');
REPLACE INTO K4M15A (dataset) VALUES ('20150612');

REPLACE INTO K4M15AD VALUES ('20150205', '20150205', '20150205',
  'start_date between timestamp ''2015-02-05'' and timestamp ''2015-02-05''');
REPLACE INTO K4M15AD VALUES ('20150209', '20150209', '20150209',
  'start_date between timestamp ''2015-02-09'' and timestamp ''2015-02-09''');
REPLACE INTO K4M15AD VALUES ('20150210', '20150210', '20150210',
  'start_date between timestamp ''2015-02-10'' and timestamp ''2015-02-10''');
REPLACE INTO K4M15AD VALUES ('20150211', '20150211', '20150214',
  'start_date between timestamp ''2015-02-11'' and timestamp ''2015-02-14''');
REPLACE INTO K4M15AD VALUES ('20150215', '20150215', '20150216',
  'start_date between timestamp ''2015-02-15'' and timestamp ''2015-02-16''');
REPLACE INTO K4M15AD VALUES ('20150217', '20150217', '20150217',
  'start_date between timestamp ''2015-02-17'' and timestamp ''2015-02-17''');
REPLACE INTO K4M15AD VALUES ('20150313', '20150313', '20150313',
  'start_date between timestamp ''2015-03-13'' and timestamp ''2015-03-13''');
REPLACE INTO K4M15AD VALUES ('20150314', '20150314', '20150317',
  'start_date between timestamp ''2015-03-14'' and timestamp ''2015-03-17''');
REPLACE INTO K4M15AD VALUES ('20150318', '20150318', '20150318',
  'start_date between timestamp ''2015-03-18'' and timestamp ''2015-03-18''');
REPLACE INTO K4M15AD VALUES ('20150319', '20150319', '20150321',
  'start_date between timestamp ''2015-03-19'' and timestamp ''2015-03-21''');
REPLACE INTO K4M15AD VALUES ('20150322', '20150322', '20150322',
  'start_date between timestamp ''2015-03-22'' and timestamp ''2015-03-22''');
REPLACE INTO K4M15AD VALUES ('20150413', '20150413', '20150413',
  'start_date between timestamp ''2015-04-13'' and timestamp ''2015-04-13''');
REPLACE INTO K4M15AD VALUES ('20150414', '20150414', '20150414',
  'start_date between timestamp ''2015-04-14'' and timestamp ''2015-04-14''');
REPLACE INTO K4M15AD VALUES ('20150424', '20150424', '20150424',
  'start_date between timestamp ''2015-04-24'' and timestamp ''2015-04-24''');
REPLACE INTO K4M15AD VALUES ('20150425', '20150425', '20150427',
  'start_date between timestamp ''2015-04-25'' and timestamp ''2015-04-27''');
REPLACE INTO K4M15AD VALUES ('20150507', '20150507', '20150507',
  'start_date between timestamp ''2015-05-07'' and timestamp ''2015-05-07''');
REPLACE INTO K4M15AD VALUES ('20150508', '20150508', '20150510',
  'start_date between timestamp ''2015-05-08'' and timestamp ''2015-05-10''');
REPLACE INTO K4M15AD VALUES ('20150511', '20150511', '20150513',
  'start_date between timestamp ''2015-05-11'' and timestamp ''2015-05-13''');
REPLACE INTO K4M15AD VALUES ('20150521', '20150521', '20150521',
  'start_date between timestamp ''2015-05-21'' and timestamp ''2015-05-21''');
REPLACE INTO K4M15AD VALUES ('20150522', '20150522', '20150522',
  'start_date between timestamp ''2015-05-22'' and timestamp ''2015-05-22''');
REPLACE INTO K4M15AD VALUES ('20150523', '20150523', '20150524',
  'start_date between timestamp ''2015-05-23'' and timestamp ''2015-05-24''');
REPLACE INTO K4M15AD VALUES ('20150525', '20150525', '20150525',
  'start_date between timestamp ''2015-05-25'' and timestamp ''2015-05-25''');
REPLACE INTO K4M15AD VALUES ('20150608', '20150608', '20150608',
  'start_date between timestamp ''2015-06-08'' and timestamp ''2015-06-08''');
REPLACE INTO K4M15AD VALUES ('20150609', '20150609', '20150610',
  'start_date between timestamp ''2015-06-09'' and timestamp ''2015-06-10''');
REPLACE INTO K4M15AD VALUES ('20150611', '20150611', '20150611',
  'start_date between timestamp ''2015-06-11'' and timestamp ''2015-06-11''');
REPLACE INTO K4M15AD VALUES ('20150612', '20150612', '20150614',
  'start_date between timestamp ''2015-06-12'' and timestamp ''2015-06-14''');
