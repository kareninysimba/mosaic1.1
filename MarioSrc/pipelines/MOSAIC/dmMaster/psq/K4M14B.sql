REPLACE INTO PSQ VALUES('K4M14B', 'K4M14B', 'K4M14BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M14B (dataset) VALUES ('20140803');
REPLACE INTO K4M14B (dataset) VALUES ('20140815');
REPLACE INTO K4M14B (dataset) VALUES ('20140816');
REPLACE INTO K4M14B (dataset) VALUES ('20140818');
REPLACE INTO K4M14B (dataset) VALUES ('20140827');
REPLACE INTO K4M14B (dataset) VALUES ('20140829');
REPLACE INTO K4M14B (dataset) VALUES ('20140831');
REPLACE INTO K4M14B (dataset) VALUES ('20140923');
REPLACE INTO K4M14B (dataset) VALUES ('20140925');
REPLACE INTO K4M14B (dataset) VALUES ('20140928');
REPLACE INTO K4M14B (dataset) VALUES ('20140929');
REPLACE INTO K4M14B (dataset) VALUES ('20141021');
REPLACE INTO K4M14B (dataset) VALUES ('20141024');
REPLACE INTO K4M14B (dataset) VALUES ('20141027');
REPLACE INTO K4M14B (dataset) VALUES ('20141126');
REPLACE INTO K4M14B (dataset) VALUES ('20141130');
REPLACE INTO K4M14B (dataset) VALUES ('20141201');
REPLACE INTO K4M14B (dataset) VALUES ('20141213');
REPLACE INTO K4M14B (dataset) VALUES ('20141222');
REPLACE INTO K4M14B (dataset) VALUES ('20141226');
REPLACE INTO K4M14B (dataset) VALUES ('20141227');
REPLACE INTO K4M14B (dataset) VALUES ('20150123');
REPLACE INTO K4M14B (dataset) VALUES ('20150125');

REPLACE INTO K4M14BD VALUES ('20140803', '20140803', '20140805',
  'start_date between timestamp ''2014-08-03'' and timestamp ''2014-08-05''');
REPLACE INTO K4M14BD VALUES ('20140815', '20140815', '20140815',
  'start_date between timestamp ''2014-08-15'' and timestamp ''2014-08-15''');
REPLACE INTO K4M14BD VALUES ('20140816', '20140816', '20140817',
  'start_date between timestamp ''2014-08-16'' and timestamp ''2014-08-17''');
REPLACE INTO K4M14BD VALUES ('20140818', '20140818', '20140821',
  'start_date between timestamp ''2014-08-18'' and timestamp ''2014-08-21''');
REPLACE INTO K4M14BD VALUES ('20140827', '20140827', '20140828',
  'start_date between timestamp ''2014-08-27'' and timestamp ''2014-08-28''');
REPLACE INTO K4M14BD VALUES ('20140829', '20140829', '20140830',
  'start_date between timestamp ''2014-08-29'' and timestamp ''2014-08-30''');
REPLACE INTO K4M14BD VALUES ('20140831', '20140831', '20140901',
  'start_date between timestamp ''2014-08-31'' and timestamp ''2014-09-01''');
REPLACE INTO K4M14BD VALUES ('20140923', '20140923', '20140924',
  'start_date between timestamp ''2014-09-23'' and timestamp ''2014-09-24''');
REPLACE INTO K4M14BD VALUES ('20140925', '20140925', '20140927',
  'start_date between timestamp ''2014-09-25'' and timestamp ''2014-09-27''');
REPLACE INTO K4M14BD VALUES ('20140928', '20140928', '20140928',
  'start_date between timestamp ''2014-09-28'' and timestamp ''2014-09-28''');
REPLACE INTO K4M14BD VALUES ('20140929', '20140929', '20140930',
  'start_date between timestamp ''2014-09-29'' and timestamp ''2014-09-30''');
REPLACE INTO K4M14BD VALUES ('20141021', '20141021', '20141023',
  'start_date between timestamp ''2014-10-21'' and timestamp ''2014-10-23''');
REPLACE INTO K4M14BD VALUES ('20141024', '20141024', '20141026',
  'start_date between timestamp ''2014-10-24'' and timestamp ''2014-10-26''');
REPLACE INTO K4M14BD VALUES ('20141027', '20141027', '20141027',
  'start_date between timestamp ''2014-10-27'' and timestamp ''2014-10-27''');
REPLACE INTO K4M14BD VALUES ('20141126', '20141126', '20141129',
  'start_date between timestamp ''2014-11-26'' and timestamp ''2014-11-29''');
REPLACE INTO K4M14BD VALUES ('20141130', '20141130', '20141130',
  'start_date between timestamp ''2014-11-30'' and timestamp ''2014-11-30''');
REPLACE INTO K4M14BD VALUES ('20141201', '20141201', '20141201',
  'start_date between timestamp ''2014-12-01'' and timestamp ''2014-12-01''');
REPLACE INTO K4M14BD VALUES ('20141213', '20141213', '20141214',
  'start_date between timestamp ''2014-12-13'' and timestamp ''2014-12-14''');
REPLACE INTO K4M14BD VALUES ('20141222', '20141222', '20141223',
  'start_date between timestamp ''2014-12-22'' and timestamp ''2014-12-23''');
REPLACE INTO K4M14BD VALUES ('20141226', '20141226', '20141226',
  'start_date between timestamp ''2014-12-26'' and timestamp ''2014-12-26''');
REPLACE INTO K4M14BD VALUES ('20141227', '20141227', '20141229',
  'start_date between timestamp ''2014-12-27'' and timestamp ''2014-12-29''');
REPLACE INTO K4M14BD VALUES ('20150123', '20150123', '20150124',
  'start_date between timestamp ''2015-01-23'' and timestamp ''2015-01-24''');
REPLACE INTO K4M14BD VALUES ('20150125', '20150125', '20150125',
  'start_date between timestamp ''2015-01-25'' and timestamp ''2015-01-25''');
