REPLACE INTO PSQ VALUES('K4M13A', 'K4M13A', 'K4M13AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M13A (dataset) VALUES ('20130204');
REPLACE INTO K4M13A (dataset) VALUES ('20130206');
REPLACE INTO K4M13A (dataset) VALUES ('20130208');
REPLACE INTO K4M13A (dataset) VALUES ('20130213');
REPLACE INTO K4M13A (dataset) VALUES ('20130218');
REPLACE INTO K4M13A (dataset) VALUES ('20130304');
REPLACE INTO K4M13A (dataset) VALUES ('20130308');
REPLACE INTO K4M13A (dataset) VALUES ('20130311');
REPLACE INTO K4M13A (dataset) VALUES ('20130314');
REPLACE INTO K4M13A (dataset) VALUES ('20130318');
REPLACE INTO K4M13A (dataset) VALUES ('20130320');
REPLACE INTO K4M13A (dataset) VALUES ('20130604');
REPLACE INTO K4M13A (dataset) VALUES ('20130605');
REPLACE INTO K4M13A (dataset) VALUES ('20130611');

REPLACE INTO K4M13AD VALUES ('20130204', '20130204', '20130205',
  'start_date between timestamp ''2013-02-04'' and timestamp ''2013-02-05''');
REPLACE INTO K4M13AD VALUES ('20130206', '20130206', '20130207',
  'start_date between timestamp ''2013-02-06'' and timestamp ''2013-02-07''');
REPLACE INTO K4M13AD VALUES ('20130208', '20130208', '20130212',
  'start_date between timestamp ''2013-02-08'' and timestamp ''2013-02-12''');
REPLACE INTO K4M13AD VALUES ('20130213', '20130213', '20130217',
  'start_date between timestamp ''2013-02-13'' and timestamp ''2013-02-17''');
REPLACE INTO K4M13AD VALUES ('20130218', '20130218', '20130218',
  'start_date between timestamp ''2013-02-18'' and timestamp ''2013-02-18''');
REPLACE INTO K4M13AD VALUES ('20130304', '20130304', '20130307',
  'start_date between timestamp ''2013-03-04'' and timestamp ''2013-03-07''');
REPLACE INTO K4M13AD VALUES ('20130308', '20130308', '20130310',
  'start_date between timestamp ''2013-03-08'' and timestamp ''2013-03-10''');
REPLACE INTO K4M13AD VALUES ('20130311', '20130311', '20130313',
  'start_date between timestamp ''2013-03-11'' and timestamp ''2013-03-13''');
REPLACE INTO K4M13AD VALUES ('20130314', '20130314', '20130317',
  'start_date between timestamp ''2013-03-14'' and timestamp ''2013-03-17''');
REPLACE INTO K4M13AD VALUES ('20130318', '20130318', '20130319',
  'start_date between timestamp ''2013-03-18'' and timestamp ''2013-03-19''');
REPLACE INTO K4M13AD VALUES ('20130320', '20130320', '20130321',
  'start_date between timestamp ''2013-03-20'' and timestamp ''2013-03-21''');
REPLACE INTO K4M13AD VALUES ('20130604', '20130604', '20130604',
  'start_date between timestamp ''2013-06-04'' and timestamp ''2013-06-04''');
REPLACE INTO K4M13AD VALUES ('20130605', '20130605', '20130610',
  'start_date between timestamp ''2013-06-05'' and timestamp ''2013-06-10''');
REPLACE INTO K4M13AD VALUES ('20130611', '20130611', '20130612',
  'start_date between timestamp ''2013-06-11'' and timestamp ''2013-06-12''');
