REPLACE INTO `PSQ` VALUES
('K4M10A','K4M10A','K4M10AD','MOSAIC','dir','enabled','proctype=\'Raw\' and dtinstru=\'mosaic_1\' and obstype not in (\'focus\',\'test\')');

DROP TABLE IF EXISTS `K4M10A`;
CREATE TABLE `K4M10A` (
  `dataset` char(32) NOT NULL default '',
  `priority` int(11) default '1',
  `status` char(16) default 'pending',
  `submitted` char(19) default NULL,
  `completed` char(19) default NULL,
  `comments` varchar(512) default NULL,
  PRIMARY KEY  (`dataset`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


LOCK TABLES `K4M10A` WRITE;
INSERT INTO `K4M10A` VALUES
('20100203',1,'nodata','NULL','NULL',NULL),
('20100205',1,'completed','2010-02-09T16:48','2010-02-09T22:49',NULL),
('20100405',1,'completed','2010-04-12T20:51','2010-04-13T16:08',NULL),
('20100406',1,'submitted','2010-04-13T16:33','NULL',NULL),
('20100409',1,'pending','NULL','NULL',NULL),
('20100413',1,'pending','NULL','NULL',NULL),
('20100417',1,'pending','NULL','NULL',NULL),
('20100510',1,'pending','NULL','NULL',NULL),
('20100514',1,'pending','NULL','NULL',NULL),
('20100517',1,'pending','NULL','NULL',NULL),
('20100604',1,'pending','NULL','NULL',NULL),
('20100607',1,'pending','NULL','NULL',NULL),
('20100609',1,'pending','NULL','NULL',NULL);
UNLOCK TABLES;

DROP TABLE IF EXISTS `K4M10AD`;
CREATE TABLE `K4M10AD` (
  `name` char(32) NOT NULL default '',
  `start` int(11) default NULL,
  `end` int(11) default NULL,
  `subquery` varchar(512) default NULL,
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


LOCK TABLES `K4M10AD` WRITE;
INSERT INTO `K4M10AD` VALUES
('20100203',20100203,20100204,'dtcaldat between \'2010-02-03%\' and \'2010-02-04%\''),
('20100205',20100205,20100207,'dtcaldat between \'2010-02-05%\' and \'2010-02-07%\''),
('20100405',20100405,20100405,'dtcaldat between \'2010-04-05%\' and \'2010-04-05%\''),
('20100406',20100406,20100408,'dtcaldat between \'2010-04-06%\' and \'2010-04-08%\''),
('20100409',20100409,20100412,'dtcaldat between \'2010-04-09%\' and \'2010-04-12%\''),
('20100413',20100413,20100416,'dtcaldat between \'2010-04-13%\' and \'2010-04-16%\''),
('20100417',20100417,20100419,'dtcaldat between \'2010-04-17%\' and \'2010-04-19%\''),
('20100510',20100510,20100513,'dtcaldat between \'2010-05-10%\' and \'2010-05-13%\''),
('20100514',20100514,20100516,'dtcaldat between \'2010-05-14%\' and \'2010-05-16%\''),
('20100517',20100517,20100518,'dtcaldat between \'2010-05-17%\' and \'2010-05-18%\''),
('20100604',20100604,20100606,'dtcaldat between \'2010-06-04%\' and \'2010-06-06%\''),
('20100607',20100607,20100608,'dtcaldat between \'2010-06-07%\' and \'2010-06-08%\''),
('20100609',20100609,20100609,'dtcaldat between \'2010-06-09%\' and \'2010-06-09%\'');
UNLOCK TABLES;
