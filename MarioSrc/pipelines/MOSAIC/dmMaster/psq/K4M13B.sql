REPLACE INTO PSQ VALUES('K4M13B', 'K4M13B', 'K4M13BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M13B (dataset) VALUES ('20130807');
REPLACE INTO K4M13B (dataset) VALUES ('20130809');
REPLACE INTO K4M13B (dataset) VALUES ('20130823');
REPLACE INTO K4M13B (dataset) VALUES ('20130826');
REPLACE INTO K4M13B (dataset) VALUES ('20130906');
REPLACE INTO K4M13B (dataset) VALUES ('20130908');
REPLACE INTO K4M13B (dataset) VALUES ('20130910');
REPLACE INTO K4M13B (dataset) VALUES ('20130911');
REPLACE INTO K4M13B (dataset) VALUES ('20131004');
REPLACE INTO K4M13B (dataset) VALUES ('20131011');
REPLACE INTO K4M13B (dataset) VALUES ('20131013');
REPLACE INTO K4M13B (dataset) VALUES ('20131126');
REPLACE INTO K4M13B (dataset) VALUES ('20131130');
REPLACE INTO K4M13B (dataset) VALUES ('20131202');
REPLACE INTO K4M13B (dataset) VALUES ('20131204');
REPLACE INTO K4M13B (dataset) VALUES ('20131231');
REPLACE INTO K4M13B (dataset) VALUES ('20140102');
REPLACE INTO K4M13B (dataset) VALUES ('20140123');
REPLACE INTO K4M13B (dataset) VALUES ('20140126');

REPLACE INTO K4M13BD VALUES ('20130807', '20130807', '20130808',
  'start_date between timestamp ''2013-08-07'' and timestamp ''2013-08-08''');
REPLACE INTO K4M13BD VALUES ('20130809', '20130809', '20130811',
  'start_date between timestamp ''2013-08-09'' and timestamp ''2013-08-11''');
REPLACE INTO K4M13BD VALUES ('20130823', '20130823', '20130825',
  'start_date between timestamp ''2013-08-23'' and timestamp ''2013-08-25''');
REPLACE INTO K4M13BD VALUES ('20130826', '20130826', '20130828',
  'start_date between timestamp ''2013-08-26'' and timestamp ''2013-08-28''');
REPLACE INTO K4M13BD VALUES ('20130906', '20130906', '20130907',
  'start_date between timestamp ''2013-09-06'' and timestamp ''2013-09-07''');
REPLACE INTO K4M13BD VALUES ('20130908', '20130908', '20130909',
  'start_date between timestamp ''2013-09-08'' and timestamp ''2013-09-09''');
REPLACE INTO K4M13BD VALUES ('20130910', '20130910', '20130910',
  'start_date between timestamp ''2013-09-10'' and timestamp ''2013-09-10''');
REPLACE INTO K4M13BD VALUES ('20130911', '20130911', '20130912',
  'start_date between timestamp ''2013-09-11'' and timestamp ''2013-09-12''');
REPLACE INTO K4M13BD VALUES ('20131004', '20131004', '20131006',
  'start_date between timestamp ''2013-10-04'' and timestamp ''2013-10-06''');
REPLACE INTO K4M13BD VALUES ('20131011', '20131011', '20131012',
  'start_date between timestamp ''2013-10-11'' and timestamp ''2013-10-12''');
REPLACE INTO K4M13BD VALUES ('20131013', '20131013', '20131013',
  'start_date between timestamp ''2013-10-13'' and timestamp ''2013-10-13''');
REPLACE INTO K4M13BD VALUES ('20131126', '20131126', '20131129',
  'start_date between timestamp ''2013-11-26'' and timestamp ''2013-11-29''');
REPLACE INTO K4M13BD VALUES ('20131130', '20131130', '20131201',
  'start_date between timestamp ''2013-11-30'' and timestamp ''2013-12-01''');
REPLACE INTO K4M13BD VALUES ('20131202', '20131202', '20131203',
  'start_date between timestamp ''2013-12-02'' and timestamp ''2013-12-03''');
REPLACE INTO K4M13BD VALUES ('20131204', '20131204', '20131204',
  'start_date between timestamp ''2013-12-04'' and timestamp ''2013-12-04''');
REPLACE INTO K4M13BD VALUES ('20131231', '20131231', '20140101',
  'start_date between timestamp ''2013-12-31'' and timestamp ''2014-01-01''');
REPLACE INTO K4M13BD VALUES ('20140102', '20140102', '20140102',
  'start_date between timestamp ''2014-01-02'' and timestamp ''2014-01-02''');
REPLACE INTO K4M13BD VALUES ('20140123', '20140123', '20140125',
  'start_date between timestamp ''2014-01-23'' and timestamp ''2014-01-25''');
REPLACE INTO K4M13BD VALUES ('20140126', '20140126', '20140126',
  'start_date between timestamp ''2014-01-26'' and timestamp ''2014-01-26''');
