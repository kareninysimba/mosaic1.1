REPLACE INTO PSQ VALUES('K4M12B', 'K4M12B', 'K4M12BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M12B (dataset) VALUES ('20121009');
REPLACE INTO K4M12B (dataset) VALUES ('20121010');
REPLACE INTO K4M12B (dataset) VALUES ('20130107');

REPLACE INTO K4M12BD VALUES ('20121009', '20121009', '20121009',
  'start_date between timestamp ''2012-10-09'' and timestamp ''2012-10-09''');
REPLACE INTO K4M12BD VALUES ('20121010', '20121010', '20121014',
  'start_date between timestamp ''2012-10-10'' and timestamp ''2012-10-14''');
REPLACE INTO K4M12BD VALUES ('20130107', '20130107', '20130113',
  'start_date between timestamp ''2013-01-07'' and timestamp ''2013-01-13''');
