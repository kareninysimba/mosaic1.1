REPLACE INTO PSQ VALUES('K4M15B', 'K4M15B', 'K4M15BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M15B (dataset) VALUES ('20150811');
REPLACE INTO K4M15B (dataset) VALUES ('20150831');
REPLACE INTO K4M15B (dataset) VALUES ('20150901');
REPLACE INTO K4M15B (dataset) VALUES ('20150902');
REPLACE INTO K4M15B (dataset) VALUES ('20150911');
REPLACE INTO K4M15B (dataset) VALUES ('20150912');
REPLACE INTO K4M15B (dataset) VALUES ('20150913');
REPLACE INTO K4M15B (dataset) VALUES ('20150914');
REPLACE INTO K4M15B (dataset) VALUES ('20151005');
REPLACE INTO K4M15B (dataset) VALUES ('20151006');
REPLACE INTO K4M15B (dataset) VALUES ('20151008');
REPLACE INTO K4M15B (dataset) VALUES ('20151010');
REPLACE INTO K4M15B (dataset) VALUES ('20151011');
REPLACE INTO K4M15B (dataset) VALUES ('20151012');
REPLACE INTO K4M15B (dataset) VALUES ('20160107');
REPLACE INTO K4M15B (dataset) VALUES ('20160108');
REPLACE INTO K4M15B (dataset) VALUES ('20160110');
REPLACE INTO K4M15B (dataset) VALUES ('20160111');
REPLACE INTO K4M15B (dataset) VALUES ('20160129');

REPLACE INTO K4M15BD VALUES ('20150811', '20150811', '20150817',
  'start_date between timestamp ''2015-08-11'' and timestamp ''2015-08-17''');
REPLACE INTO K4M15BD VALUES ('20150831', '20150831', '20150831',
  'start_date between timestamp ''2015-08-31'' and timestamp ''2015-08-31''');
REPLACE INTO K4M15BD VALUES ('20150901', '20150901', '20150901',
  'start_date between timestamp ''2015-09-01'' and timestamp ''2015-09-01''');
REPLACE INTO K4M15BD VALUES ('20150902', '20150902', '20150903',
  'start_date between timestamp ''2015-09-02'' and timestamp ''2015-09-03''');
REPLACE INTO K4M15BD VALUES ('20150911', '20150911', '20150911',
  'start_date between timestamp ''2015-09-11'' and timestamp ''2015-09-11''');
REPLACE INTO K4M15BD VALUES ('20150912', '20150912', '20150912',
  'start_date between timestamp ''2015-09-12'' and timestamp ''2015-09-12''');
REPLACE INTO K4M15BD VALUES ('20150913', '20150913', '20150913',
  'start_date between timestamp ''2015-09-13'' and timestamp ''2015-09-13''');
REPLACE INTO K4M15BD VALUES ('20150914', '20150914', '20150914',
  'start_date between timestamp ''2015-09-14'' and timestamp ''2015-09-14''');
REPLACE INTO K4M15BD VALUES ('20151005', '20151005', '20151005',
  'start_date between timestamp ''2015-10-05'' and timestamp ''2015-10-05''');
REPLACE INTO K4M15BD VALUES ('20151006', '20151006', '20151007',
  'start_date between timestamp ''2015-10-06'' and timestamp ''2015-10-07''');
REPLACE INTO K4M15BD VALUES ('20151008', '20151008', '20151009',
  'start_date between timestamp ''2015-10-08'' and timestamp ''2015-10-09''');
REPLACE INTO K4M15BD VALUES ('20151010', '20151010', '20151010',
  'start_date between timestamp ''2015-10-10'' and timestamp ''2015-10-10''');
REPLACE INTO K4M15BD VALUES ('20151011', '20151011', '20151011',
  'start_date between timestamp ''2015-10-11'' and timestamp ''2015-10-11''');
REPLACE INTO K4M15BD VALUES ('20151012', '20151012', '20151014',
  'start_date between timestamp ''2015-10-12'' and timestamp ''2015-10-14''');
REPLACE INTO K4M15BD VALUES ('20160107', '20160107', '20160107',
  'start_date between timestamp ''2016-01-07'' and timestamp ''2016-01-07''');
REPLACE INTO K4M15BD VALUES ('20160108', '20160108', '20160109',
  'start_date between timestamp ''2016-01-08'' and timestamp ''2016-01-09''');
REPLACE INTO K4M15BD VALUES ('20160110', '20160110', '20160110',
  'start_date between timestamp ''2016-01-10'' and timestamp ''2016-01-10''');
REPLACE INTO K4M15BD VALUES ('20160111', '20160111', '20160111',
  'start_date between timestamp ''2016-01-11'' and timestamp ''2016-01-11''');
REPLACE INTO K4M15BD VALUES ('20160129', '20160129', '20160131',
  'start_date between timestamp ''2016-01-29'' and timestamp ''2016-01-31''');
