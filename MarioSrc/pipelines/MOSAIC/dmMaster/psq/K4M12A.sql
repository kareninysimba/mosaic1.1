REPLACE INTO PSQ VALUES('K4M12A', 'K4M12A', 'K4M12AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M12A (dataset) VALUES ('20120213');
REPLACE INTO K4M12A (dataset) VALUES ('20120222');
REPLACE INTO K4M12A (dataset) VALUES ('20120313');
REPLACE INTO K4M12A (dataset) VALUES ('20120315');
REPLACE INTO K4M12A (dataset) VALUES ('20120320');
REPLACE INTO K4M12A (dataset) VALUES ('20120324');
REPLACE INTO K4M12A (dataset) VALUES ('20120511');
REPLACE INTO K4M12A (dataset) VALUES ('20120514');
REPLACE INTO K4M12A (dataset) VALUES ('20120517');
REPLACE INTO K4M12A (dataset) VALUES ('20120620');

REPLACE INTO K4M12AD VALUES ('20120213', '20120213', '20120221',
  'start_date between timestamp ''2012-02-13'' and timestamp ''2012-02-21''');
REPLACE INTO K4M12AD VALUES ('20120222', '20120222', '20120226',
  'start_date between timestamp ''2012-02-22'' and timestamp ''2012-02-26''');
REPLACE INTO K4M12AD VALUES ('20120313', '20120313', '20120314',
  'start_date between timestamp ''2012-03-13'' and timestamp ''2012-03-14''');
REPLACE INTO K4M12AD VALUES ('20120315', '20120315', '20120319',
  'start_date between timestamp ''2012-03-15'' and timestamp ''2012-03-19''');
REPLACE INTO K4M12AD VALUES ('20120320', '20120320', '20120323',
  'start_date between timestamp ''2012-03-20'' and timestamp ''2012-03-23''');
REPLACE INTO K4M12AD VALUES ('20120324', '20120324', '20120328',
  'start_date between timestamp ''2012-03-24'' and timestamp ''2012-03-28''');
REPLACE INTO K4M12AD VALUES ('20120511', '20120511', '20120513',
  'start_date between timestamp ''2012-05-11'' and timestamp ''2012-05-13''');
REPLACE INTO K4M12AD VALUES ('20120514', '20120514', '20120516',
  'start_date between timestamp ''2012-05-14'' and timestamp ''2012-05-16''');
REPLACE INTO K4M12AD VALUES ('20120517', '20120517', '20120521',
  'start_date between timestamp ''2012-05-17'' and timestamp ''2012-05-21''');
REPLACE INTO K4M12AD VALUES ('20120620', '20120620', '20120626',
  'start_date between timestamp ''2012-06-20'' and timestamp ''2012-06-26''');
