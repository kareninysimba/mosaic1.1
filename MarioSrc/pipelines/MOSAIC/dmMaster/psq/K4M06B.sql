REPLACE INTO PSQ VALUES('K4M06B', 'K4M06B', 'K4M06BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M06B (dataset) VALUES ('20060820');
REPLACE INTO K4M06B (dataset) VALUES ('20060826');
REPLACE INTO K4M06B (dataset) VALUES ('20060913');
REPLACE INTO K4M06B (dataset) VALUES ('20060920');
REPLACE INTO K4M06B (dataset) VALUES ('20061012');
REPLACE INTO K4M06B (dataset) VALUES ('20061017');
REPLACE INTO K4M06B (dataset) VALUES ('20061020');
REPLACE INTO K4M06B (dataset) VALUES ('20061024');
REPLACE INTO K4M06B (dataset) VALUES ('20061027');
REPLACE INTO K4M06B (dataset) VALUES ('20061214');
REPLACE INTO K4M06B (dataset) VALUES ('20061215');
REPLACE INTO K4M06B (dataset) VALUES ('20061219');
REPLACE INTO K4M06B (dataset) VALUES ('20061221');
REPLACE INTO K4M06B (dataset) VALUES ('20070109');
REPLACE INTO K4M06B (dataset) VALUES ('20070113');
REPLACE INTO K4M06B (dataset) VALUES ('20070119');

REPLACE INTO K4M06BD VALUES ('20060820', '20060820', '20060822',
  '"startDate" between timestamp ''2006-08-20'' and timestamp ''2006-08-22''');
REPLACE INTO K4M06BD VALUES ('20060826', '20060826', '20060828',
  '"startDate" between timestamp ''2006-08-26'' and timestamp ''2006-08-28''');
REPLACE INTO K4M06BD VALUES ('20060913', '20060913', '20060919',
  '"startDate" between timestamp ''2006-09-13'' and timestamp ''2006-09-19''');
REPLACE INTO K4M06BD VALUES ('20060920', '20060920', '20060924',
  '"startDate" between timestamp ''2006-09-20'' and timestamp ''2006-09-24''');
REPLACE INTO K4M06BD VALUES ('20061012', '20061012', '20061016',
  '"startDate" between timestamp ''2006-10-12'' and timestamp ''2006-10-16''');
REPLACE INTO K4M06BD VALUES ('20061017', '20061017', '20061019',
  '"startDate" between timestamp ''2006-10-17'' and timestamp ''2006-10-19''');
REPLACE INTO K4M06BD VALUES ('20061020', '20061020', '20061023',
  '"startDate" between timestamp ''2006-10-20'' and timestamp ''2006-10-23''');
REPLACE INTO K4M06BD VALUES ('20061024', '20061024', '20061026',
  '"startDate" between timestamp ''2006-10-24'' and timestamp ''2006-10-26''');
REPLACE INTO K4M06BD VALUES ('20061027', '20061027', '20061030',
  '"startDate" between timestamp ''2006-10-27'' and timestamp ''2006-10-30''');
REPLACE INTO K4M06BD VALUES ('20061214', '20061214', '20061214',
  '"startDate" between timestamp ''2006-12-14'' and timestamp ''2006-12-14''');
REPLACE INTO K4M06BD VALUES ('20061215', '20061215', '20061218',
  '"startDate" between timestamp ''2006-12-15'' and timestamp ''2006-12-18''');
REPLACE INTO K4M06BD VALUES ('20061219', '20061219', '20061220',
  '"startDate" between timestamp ''2006-12-19'' and timestamp ''2006-12-20''');
REPLACE INTO K4M06BD VALUES ('20061221', '20061221', '20061223',
  '"startDate" between timestamp ''2006-12-21'' and timestamp ''2006-12-23''');
REPLACE INTO K4M06BD VALUES ('20070109', '20070109', '20070112',
  '"startDate" between timestamp ''2007-01-09'' and timestamp ''2007-01-12''');
REPLACE INTO K4M06BD VALUES ('20070113', '20070113', '20070118',
  '"startDate" between timestamp ''2007-01-13'' and timestamp ''2007-01-18''');
REPLACE INTO K4M06BD VALUES ('20070119', '20070119', '20070123',
  '"startDate" between timestamp ''2007-01-19'' and timestamp ''2007-01-23''');
