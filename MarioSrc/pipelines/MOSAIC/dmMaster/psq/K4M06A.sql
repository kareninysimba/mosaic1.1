REPLACE INTO PSQ VALUES('K4M06A', 'K4M06A', 'K4M06AD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1''');

REPLACE INTO K4M06A (dataset) VALUES ('20060221');
REPLACE INTO K4M06A (dataset) VALUES ('20060222');
REPLACE INTO K4M06A (dataset) VALUES ('20060223');
REPLACE INTO K4M06A (dataset) VALUES ('20060227');
REPLACE INTO K4M06A (dataset) VALUES ('20060228');
REPLACE INTO K4M06A (dataset) VALUES ('20060302');
REPLACE INTO K4M06A (dataset) VALUES ('20060421');
REPLACE INTO K4M06A (dataset) VALUES ('20060425');
REPLACE INTO K4M06A (dataset) VALUES ('20060428');
REPLACE INTO K4M06A (dataset) VALUES ('20060430');
REPLACE INTO K4M06A (dataset) VALUES ('20060502');
REPLACE INTO K4M06A (dataset) VALUES ('20060505');
REPLACE INTO K4M06A (dataset) VALUES ('20060614');
REPLACE INTO K4M06A (dataset) VALUES ('20060620');
REPLACE INTO K4M06A (dataset) VALUES ('20060623');
REPLACE INTO K4M06A (dataset) VALUES ('20060626');

REPLACE INTO K4M06AD VALUES ('20060221', '20060221', '20060221',
  '"startDate" between timestamp ''2006-02-21'' and timestamp ''2006-02-21''');
REPLACE INTO K4M06AD VALUES ('20060222', '20060222', '20060222',
  '"startDate" between timestamp ''2006-02-22'' and timestamp ''2006-02-22''');
REPLACE INTO K4M06AD VALUES ('20060223', '20060223', '20060226',
  '"startDate" between timestamp ''2006-02-23'' and timestamp ''2006-02-26''');
REPLACE INTO K4M06AD VALUES ('20060227', '20060227', '20060227',
  '"startDate" between timestamp ''2006-02-27'' and timestamp ''2006-02-27''');
REPLACE INTO K4M06AD VALUES ('20060228', '20060228', '20060301',
  '"startDate" between timestamp ''2006-02-28'' and timestamp ''2006-03-01''');
REPLACE INTO K4M06AD VALUES ('20060302', '20060302', '20060302',
  '"startDate" between timestamp ''2006-03-02'' and timestamp ''2006-03-02''');
REPLACE INTO K4M06AD VALUES ('20060421', '20060421', '20060424',
  '"startDate" between timestamp ''2006-04-21'' and timestamp ''2006-04-24''');
REPLACE INTO K4M06AD VALUES ('20060425', '20060425', '20060427',
  '"startDate" between timestamp ''2006-04-25'' and timestamp ''2006-04-27''');
REPLACE INTO K4M06AD VALUES ('20060428', '20060428', '20060429',
  '"startDate" between timestamp ''2006-04-28'' and timestamp ''2006-04-29''');
REPLACE INTO K4M06AD VALUES ('20060430', '20060430', '20060501',
  '"startDate" between timestamp ''2006-04-30'' and timestamp ''2006-05-01''');
REPLACE INTO K4M06AD VALUES ('20060502', '20060502', '20060504',
  '"startDate" between timestamp ''2006-05-02'' and timestamp ''2006-05-04''');
REPLACE INTO K4M06AD VALUES ('20060505', '20060505', '20060509',
  '"startDate" between timestamp ''2006-05-05'' and timestamp ''2006-05-09''');
REPLACE INTO K4M06AD VALUES ('20060614', '20060614', '20060619',
  '"startDate" between timestamp ''2006-06-14'' and timestamp ''2006-06-19''');
REPLACE INTO K4M06AD VALUES ('20060620', '20060620', '20060622',
  '"startDate" between timestamp ''2006-06-20'' and timestamp ''2006-06-22''');
REPLACE INTO K4M06AD VALUES ('20060623', '20060623', '20060625',
  '"startDate" between timestamp ''2006-06-23'' and timestamp ''2006-06-25''');
REPLACE INTO K4M06AD VALUES ('20060626', '20060626', '20060628',
  '"startDate" between timestamp ''2006-06-26'' and timestamp ''2006-06-28''');
