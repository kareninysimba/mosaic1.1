REPLACE INTO PSQ VALUES('K4M11B', 'K4M11B', 'K4M11BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_1_1''');

REPLACE INTO K4M11B (dataset) VALUES ('20110915');
REPLACE INTO K4M11B (dataset) VALUES ('20110922');
REPLACE INTO K4M11B (dataset) VALUES ('20110924');
REPLACE INTO K4M11B (dataset) VALUES ('20111004');
REPLACE INTO K4M11B (dataset) VALUES ('20111007');
REPLACE INTO K4M11B (dataset) VALUES ('20111018');
REPLACE INTO K4M11B (dataset) VALUES ('20111019');
REPLACE INTO K4M11B (dataset) VALUES ('20111024');
REPLACE INTO K4M11B (dataset) VALUES ('20111026');
REPLACE INTO K4M11B (dataset) VALUES ('20111028');
REPLACE INTO K4M11B (dataset) VALUES ('20111031');
REPLACE INTO K4M11B (dataset) VALUES ('20111103');
REPLACE INTO K4M11B (dataset) VALUES ('20111220');
REPLACE INTO K4M11B (dataset) VALUES ('20120117');
REPLACE INTO K4M11B (dataset) VALUES ('20120119');

REPLACE INTO K4M11BD VALUES ('20110915', '20110915', '20110918',
  'start_date between timestamp ''2011-09-15'' and timestamp ''2011-09-18''');
REPLACE INTO K4M11BD VALUES ('20110922', '20110922', '20110923',
  'start_date between timestamp ''2011-09-22'' and timestamp ''2011-09-23''');
REPLACE INTO K4M11BD VALUES ('20110924', '20110924', '20110929',
  'start_date between timestamp ''2011-09-24'' and timestamp ''2011-09-29''');
REPLACE INTO K4M11BD VALUES ('20111004', '20111004', '20111006',
  'start_date between timestamp ''2011-10-04'' and timestamp ''2011-10-06''');
REPLACE INTO K4M11BD VALUES ('20111007', '20111007', '20111009',
  'start_date between timestamp ''2011-10-07'' and timestamp ''2011-10-09''');
REPLACE INTO K4M11BD VALUES ('20111018', '20111018', '20111018',
  'start_date between timestamp ''2011-10-18'' and timestamp ''2011-10-18''');
REPLACE INTO K4M11BD VALUES ('20111019', '20111019', '20111023',
  'start_date between timestamp ''2011-10-19'' and timestamp ''2011-10-23''');
REPLACE INTO K4M11BD VALUES ('20111024', '20111024', '20111025',
  'start_date between timestamp ''2011-10-24'' and timestamp ''2011-10-25''');
REPLACE INTO K4M11BD VALUES ('20111026', '20111026', '20111027',
  'start_date between timestamp ''2011-10-26'' and timestamp ''2011-10-27''');
REPLACE INTO K4M11BD VALUES ('20111028', '20111028', '20111030',
  'start_date between timestamp ''2011-10-28'' and timestamp ''2011-10-30''');
REPLACE INTO K4M11BD VALUES ('20111031', '20111031', '20111102',
  'start_date between timestamp ''2011-10-31'' and timestamp ''2011-11-02''');
REPLACE INTO K4M11BD VALUES ('20111103', '20111103', '20111103',
  'start_date between timestamp ''2011-11-03'' and timestamp ''2011-11-03''');
REPLACE INTO K4M11BD VALUES ('20111220', '20111220', '20111223',
  'start_date between timestamp ''2011-12-20'' and timestamp ''2011-12-23''');
REPLACE INTO K4M11BD VALUES ('20120117', '20120117', '20120118',
  'start_date between timestamp ''2012-01-17'' and timestamp ''2012-01-18''');
REPLACE INTO K4M11BD VALUES ('20120119', '20120119', '20120122',
  'start_date between timestamp ''2012-01-19'' and timestamp ''2012-01-22''');
