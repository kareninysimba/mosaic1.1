REPLACE INTO PSQ VALUES('C4M11B', 'C4M11B', 'C4M11BD', 'MOSAIC',
'dir', 'disabled', 'instrument=''mosaic_2''');

REPLACE INTO C4M11B (dataset) VALUES ('20110817');
REPLACE INTO C4M11B (dataset) VALUES ('20110821');
REPLACE INTO C4M11B (dataset) VALUES ('20110826');
REPLACE INTO C4M11B (dataset) VALUES ('20111016');
REPLACE INTO C4M11B (dataset) VALUES ('20111018');
REPLACE INTO C4M11B (dataset) VALUES ('20111023');
REPLACE INTO C4M11B (dataset) VALUES ('20111027');
REPLACE INTO C4M11B (dataset) VALUES ('20111116');
REPLACE INTO C4M11B (dataset) VALUES ('20111118');
REPLACE INTO C4M11B (dataset) VALUES ('20111122');
REPLACE INTO C4M11B (dataset) VALUES ('20111215');
REPLACE INTO C4M11B (dataset) VALUES ('20111217');
REPLACE INTO C4M11B (dataset) VALUES ('20120105');

REPLACE INTO C4M11BD VALUES ('20110817', '20110817', '20110820',
  'start_date between timestamp ''2011-08-17'' and timestamp ''2011-08-20''');
REPLACE INTO C4M11BD VALUES ('20110821', '20110821', '20110825',
  'start_date between timestamp ''2011-08-21'' and timestamp ''2011-08-25''');
REPLACE INTO C4M11BD VALUES ('20110826', '20110826', '20110829',
  'start_date between timestamp ''2011-08-26'' and timestamp ''2011-08-29''');
REPLACE INTO C4M11BD VALUES ('20111016', '20111016', '20111017',
  'start_date between timestamp ''2011-10-16'' and timestamp ''2011-10-17''');
REPLACE INTO C4M11BD VALUES ('20111018', '20111018', '20111022',
  'start_date between timestamp ''2011-10-18'' and timestamp ''2011-10-22''');
REPLACE INTO C4M11BD VALUES ('20111023', '20111023', '20111026',
  'start_date between timestamp ''2011-10-23'' and timestamp ''2011-10-26''');
REPLACE INTO C4M11BD VALUES ('20111027', '20111027', '20111101',
  'start_date between timestamp ''2011-10-27'' and timestamp ''2011-11-01''');
REPLACE INTO C4M11BD VALUES ('20111016', '20111116', '20111117',
  'start_date between timestamp ''2011-11-16'' and timestamp ''2011-11-17''');
REPLACE INTO C4M11BD VALUES ('20111018', '20111118', '20111121',
  'start_date between timestamp ''2011-11-18'' and timestamp ''2011-11-21''');
REPLACE INTO C4M11BD VALUES ('20111122', '20111122', '20111127',
  'start_date between timestamp ''2011-11-22'' and timestamp ''2011-11-27''');
REPLACE INTO C4M11BD VALUES ('20111215', '20111215', '20111216',
  'start_date between timestamp ''2011-12-15'' and timestamp ''2011-12-16''');
REPLACE INTO C4M11BD VALUES ('20111217', '20111217', '20111219',
  'start_date between timestamp ''2011-12-17'' and timestamp ''2011-12-19''');
REPLACE INTO C4M11BD VALUES ('20120105', '20120105', '20120108',
  'start_date between timestamp ''2012-01-05'' and timestamp ''2012-01-12''');
