/* SQL commands to (re-)init the PMAS portion of the DM database */
/* This file reinits everything but the primary data tables */
/* ProcessingProcesses and ProcessingData. */

/* indicates tables which have class information and to which table they pertain */
/* all tables indicated must have cols 'id', 'tablename' and 'class' */
DROP TABLE ClassDictionary;
CREATE TABLE ClassDictionary (
    id int not null,                  /* int id of the class */
    class varchar(255) not null,      /* str id of the class */
    viewname varchar(64) not null,  /*  view for this class */
    tablename varchar(64) not null   /* underlying table for this class */
);

/* declare Process-related classes */
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 1, 'process', 'Processes', 'ProcessingProcesses');

/* declare DataProduct-related classes */
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 1, 'dataproduct', 'DataProducts', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 2, 'obsdataproduct', 'ObservedDataProducts', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 3, 'DSInput', 'DSInput', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 4, 'STBOutput', 'STDOutput', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 5, 'duplicateimage', 'DuplicateImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 100, 'singleimage', 'SingleImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 101, 'biasimage', 'BiasImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 102, 'domeflatimage', 'DomeflatImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 103, 'objectimage', 'ObjectImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 200, 'compositeimage', 'CompositeImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 201, 'zeroimage', 'ZeroImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 202, 'avdomeflatimage', 'AvDomeflatImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 203, 'mefobjectimage', 'MEFObjectImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 204, 'skyflatimage', 'SkyflatImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 205, 'fringeimage', 'FringeImages', 'ProcessingData');
INSERT INTO ClassDictionary ( id, class, viewname, tablename ) VALUES ( 206, 'pupilghostimage', 'PupilGhostImages', 'ProcessingData');

/* Create indices */
DROP INDEX ProcIdIndx;
CREATE UNIQUE INDEX ProcIdIndx on ProcessingProcesses ( id_str );

DROP INDEX ProcStartDateIndx;
CREATE INDEX ProcStartDateIndx ON ProcessingProcesses ( startMJD );

DROP INDEX ProcClassIndx;
CREATE INDEX ProcClassIndx ON ProcessingProcesses ( class );


/* Create views of the processed data - would be a good idea to "automate" this*/

/* Process: class '0' .. all process items are this */
DROP VIEW Processes;
CREATE VIEW Processes AS
       select id_str,startMJD,runTime,nodeIP,logURL,uparmURL,success
       FROM ProcessingProcesses;

/* Create indices */
DROP INDEX DataIdIndx;
CREATE UNIQUE INDEX DataIdIndx ON ProcessingData ( id_str );

DROP INDEX DataCreateDateIndx;
CREATE INDEX DataCreateDateIndx ON ProcessingData ( createMJD );

DROP INDEX DataClassIndx;
CREATE INDEX DataClassIndx ON ProcessingData ( class );

/* Create views of the processed data - would be a good idea to "automate" this*/

/* DataProducts: class '1' .. all data items are this */
DROP VIEW DataProducts;
CREATE VIEW DataProducts AS 
       select id_str,createMJD,producedby,URL
       FROM ProcessingData;

/* ObservedDataProducts: class '2' inherits from general 'DataProduct' */
DROP VIEW ObservedDataProducts;
CREATE VIEW ObservedDataProducts AS 
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class > 0 and class < 300;

/* DuplicateImages: class '5' inherits from general 'DataProduct' */
DROP VIEW DuplicateImages;
CREATE VIEW DuplicateImages AS 
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 5;

/* SingleImages: class '100' inherits from ObsDataProd */
DROP VIEW SingleImages;
CREATE VIEW SingleImages AS 
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              dqrej, dqglmean,dqglsig,dqglfsat,
              dqovmll, dqovmlj, dqovmjl, dqovmjj, dqovmllr, dqovmljr,
              dqovmjlr, dqovmjjr, dqovjprb, dqovmin, dqovmax, dqovmean, dqovsig,
              dqovsmea, dqovssig, 
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class >= 100 and class < 200;

/* BiasImages: class '101' inherits from SingleImages */
DROP VIEW BiasImages;
CREATE VIEW BiasImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              dqrej,dqglmean,dqglsig,dqglfsat,
              dqbiesig,dqbizmns,dqbizsge,
              dqovmll, dqovmlj, dqovmjl, dqovmjj, dqovmllr, dqovmljr,
              dqovmjlr, dqovmjjr, dqovjprb, dqovmin, dqovmax, dqovmean, dqovsig,
              dqovsmea, dqovssig,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 101;

/* DomeflatImages: class '102' inherits from SingleImages */
DROP VIEW DomeflatImages;
CREATE VIEW DomeflatImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              dqrej,dqglmean,dqglsig,dqglfsat,
              dqdfcrat, dqdfxslp,dqdfyslp
              dqdfxsig,dqdfysig,dqdfglme,dqdfglsi,dqdfxmea,dqdfxcef,
              dqdfymea,dqdfycef,
              dqovmll, dqovmlj, dqovmjl, dqovmjj, dqovmllr, dqovmljr,
              dqovmjlr, dqovmjjr, dqovjprb, dqovmin, dqovmax, dqovmean, dqovsig,
              dqovsmea, dqovssig, dqistwi, 
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 102;

/* ObjectImages: class '103' inherits from SingleImages */
DROP VIEW ObjectImages;
CREATE VIEW ObjectImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              dqrej,dqglmean,dqglsig,dqglfsat,
              dqobfrbp, dqobmxa, dqseamp,
              dqphdpps, dqphdpap, dqphapsz, dqphdppx, dqphzp, dqphezp,
              dqwcccxr, dqwcccyr, dqwcccxs, dqwcccys,
              dqskfrc, dqskval, dqskvxgr, dqskvygr, dqsksig, dqskmag,
              dqsksxgr, dqsksygr, dqskeval, dqskesig,
              dqpgscal, dqpgcal,
              dqfrscal, dqfrcal,
              dqovmll, dqovmlj, dqovmjl, dqovmjj, dqovmllr, dqovmljr,
              dqovmjlr, dqovmjjr, dqovjprb, dqovmin, dqovmax, dqovmean, dqovsig,
              dqovsmea, dqovssig, 
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 103;

/* CompositeImages: class '200' inherits from ObsDataProd */
DROP VIEW CompositeImages;
CREATE VIEW CompositeImages AS 
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD
       FROM ProcessingData where class >= 200 and class < 300;

/* ZeroImages: class '201' inherits from CompositeImages */
DROP VIEW ZeroImages;
CREATE VIEW ZeroImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              parentBiases,dqzenusd,dqzenrej,dqzexslp,dqzeyslp,
              dqzexsig,dqzeysig,dqzeglme,dqzeglsi,dqzexmea,dqzexcef,
              dqzeymea,dqzeycef, 
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 201;

/* AvDomeflatImages: class '202' inherits from CompositeImages */
DROP VIEW AvDomeflatImages;
CREATE VIEW AvDomeflatImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              parentDomeflats,dqadnusd,dqadnrej,
              dqrfxslp, dqrfyslp, dqrfxsig, dqrfysig, dqrfglme, dqrfglsi,
              dqrfxmea, dqrfxcef, dqrfymea, dqrfycef,
              dqadxslp, dqadyslp, dqadxsig, dqadysig, dqadglme, dqadglsi,
              dqadxmea, dqadxcef, dqadymea, dqadycef, dqisatwi,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 202;

/* MEFObjectImages: class '203' inherits from CompositeImages */
DROP VIEW MEFObjectImages;
CREATE VIEW MEFObjectImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              dqreject, dqwcumxo, dqwcumyo, dqwcumro, dqwcumts,
              dqwcummf, dqwcummz, dqwcumme, dqwcuptx, dqwcupty,
              dqwcupsx, dqwcupsy, dqwcupax, dqwcupay, dqwcuprx,
              dqwcupry, dqwcccxr, dqwcccyr, dqwcccxs, dqwcccys,
              dqobmaxa, dqsemef, dqphazp, dqpheazp, dqphadps,
	      dqskfrac, dqskaval, dqskasig,
              dqskeavl, dqskamag,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 203;

/* SkyflatImages: class '204' inherits from CompositeImages */
DROP VIEW SkyflatImages;
CREATE VIEW SkyflatImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              parentsForSkyflat, dqsfnusd, dqsfxslp, dqsfyslp, dqsfxsig,
              dqsfysig, dqsfglme, dqsfglsi, dqsfxmea, dqsfxcef, dqsfymea,
              dqsfycef, dqsflcmn, dqsflcsi, dqsfsnr, dqsflcme, dqsflcse,
              dqsfsnre, 
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 204;

/* FringeImages: class '205' inherits from CompositeImages */
DROP VIEW FringeImages;
CREATE VIEW FringeImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              parentsForFringe, dqfrnusd, dqfrsnr,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 205;

/* PupilGhostImages: class '206' inherits from CompositeImages */
DROP VIEW PupilGhostImages;
CREATE VIEW PupilGhostImages AS
       select id_str,createMJD,producedby,URL,
              obsConfig,observeMJD,
              parentsForPupilGhost, dqpgnusd, dqpgsnr,
              enid, detector, ampname, imageid, filter, obsid, obstype, telescope,
              ra, dec, epoch, exptime, darktime, airmass, zd, saturate,
              object, ccdtem, ccdtem2, dewtem, dewtem2, dewtem3,
              envtem, telfocus, gain, rdnoise
       FROM ProcessingData where class = 206;

/* View of data service input */
DROP VIEW DSInput;
CREATE VIEW DSInput AS
	SELECT id_str,createMJD,year,month,day,hr,min,sec,producedby,size,
	       nelements
	FROM ProcessingData WHERE class=3;

/* View of STB output data products */
DROP VIEW STBOutput;
CREATE VIEW STBOutput AS
	SELECT id_str,createMJD,year,month,day,hr,min,sec,producedby,size
	FROM ProcessingData WHERE class=4;

/* Create Observation Config table */
DROP TABLE ObservationConfig;
CREATE TABLE ObservationConfig (
    id                int not null,
    telescope_id      int not null,
    detector_id       int not null,
    filter_id         int not null,
    /* table key */
    primary key (id)
);

/* declare observing configs here */
/* "unknown" configuration */
INSERT INTO ObservationConfig ( id, telescope_id, detector_id, filter_id ) VALUES ( -1, -1, -1, -1);
/* kitt peak 4m, mosaic2, no filter */
INSERT INTO ObservationConfig ( id, telescope_id, detector_id, filter_id ) VALUES ( 0, 0, 0, 0);

/* create telescopes table */
DROP TABLE Telescopes;
CREATE TABLE Telescopes (
    id                int not null,
    name              varchar (128) not null,
    description       varchar (255) not null,
    /* table key */
    primary key (id)
);

/* Create indices */
DROP INDEX TelescopeNameIndx;
CREATE UNIQUE INDEX TelescopeNameIndx ON Telescopes ( name );

/* declare telescopes here */
INSERT INTO Telescopes ( id, name, description ) VALUES (-1, 'unknown', 'unknown telescope');
INSERT INTO Telescopes ( id, name, description ) VALUES ( 0, 'KittPeak_4m', 'Kitt Peak 4 meter telescope');
INSERT INTO Telescopes ( id, name, description ) VALUES ( 1, 'CTIO_4m', 'CTIO 4 meter telescope');

/* create detectors table */
DROP TABLE Detectors;
CREATE TABLE Detectors (
    id                int not null,
    name              varchar (128) not null,
    description       varchar (255) not null,
    /* table key */
    primary key (id)
);

/* Create indices */
DROP INDEX DetectorNameIndx;
CREATE UNIQUE INDEX DetectorNameIndx ON Detectors ( name );

/* declare detectors here */
INSERT INTO Detectors ( id, name, description ) VALUES (-1, 'unknown', 'unknown detector');
INSERT INTO Detectors ( id, name, description ) VALUES ( 0, 'mosaic2', 'Mosaic CCD array');
INSERT INTO Detectors ( id, name, description ) VALUES ( 1, 'newfirm', 'NewFirm CCD array');
INSERT INTO Detectors ( id, name, description ) VALUES ( 2, 'CCDMosaThin1', 'Mosaic CCD array');

/* create filters table */
DROP TABLE Filters;
CREATE TABLE Filters (
    id                int not null,
    SN                varchar (32) not null,
    name              varchar (128) not null,
    description       varchar (255) not null,
    /* table key */
    primary key (id)
);

/* Create indices */
DROP INDEX FilterSNIndx;
CREATE UNIQUE INDEX FilterSNIndx ON Filters ( SN );

/* declare filters here */
INSERT INTO Filters ( id, SN, name, description ) VALUES (-1, 'unknown', 'unknown', 'Unknown Filter');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 0, 'k1000', 'blank', 'No Filter');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 1, 'k1001', 'U', 'LU4#1 - Liq. CuSO4+UG1');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 2, 'k1002', 'B', 'Harris B - Mosaic');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 3, 'k1003', 'V', 'Harris V - Mosaic');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 4, 'k1004', 'R', 'Harris R - Mosaic');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 5, 'k1005', 'I', 'Nearly-Mould I');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 6, 'k1006', 'C', 'Washington C Filter');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 7, 'k1007', 'M', 'Washington M (Modified)');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 8, 'k1008', 'D51', 'DDO 51 Filter');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 9, 'k1009','ha',  "H Alpha Filter  ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 10, 'k1010','ha4', "H Alpha + 4nm ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 11, 'k1011','ha8', "H Alpha + 8nm ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 12, 'k1012','ha12',"H Alpha + 12nm ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 13, 'k1013','ha16',"H Alpha + 16nm/[SII] ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 14, 'k1014','O3',  "[OIII] Filter Mosaic");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 15, 'k1015','Ooff',"[OIII] + 30nm offband #3");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 16, 'k1016','wh',  "BK-7 Glass Filter ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 17, 'k1017','g',   "SDSS g' filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 18, 'k1018','r',   "SDSS r' filter ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 19, 'k1019','i',   "SDSS i' filter ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 20, 'k1020','z',   "SDSS z' filter ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 21, 'k1021','wrc3', "Wolf-Rayet CIII filter ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 22, 'k1022','wr475',"Wolf-Rayet Cont. Filter ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 23, 'k1023','wrhe2',"Wolf-Rayet HeII filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 24, 'k1024','wrc4', "Wolf-Rayet CIV filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 25, 'k1025','Bw',   "NOAO Deep Wide-Field Survey Filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 26, 'k1026','815',  "Rhoads 820B Filter - Mosaic  (old)");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 27, 'k1027','823',  "Rhoads 820R Filter - Mosaic  (old)");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 28, 'k1028','918R', "Rhoads 918R Filter - Mosaic ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 29, 'k1029','U',    "LU3#1 spare Liq. CuSO4 Filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 30, 'k1030','Ooff', "[OIII]+30nm, offband #1 (abras.marks)");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 31, 'k1031','Ooff', "[OIII]+30nm, offband #2 ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 32, 'k1040','VR',   "Bernstein Broad VR filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 33, 'k1041','Un',   "Steidel Custom U filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 34, 'k1042','Gn',   "Steidel Custom B filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 35, 'k1043','Rs',   "Steidel Custom R filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 36, 'k1046','815',  "Rhoads 820B Filter - Mosaic");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 37, 'k1047','823',  "Rhoads 820R Filter - Mosaic");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 38, 'k1051','337',  "Windhorst 337nm BATC Medium Band 01 a");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 39, 'k1052','390',  "Windhorst 390nm BATC Medium Band 02 b ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 40, 'k1053','420',  "Windhorst 420nm BATC Medium Band 03 c ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 41, 'k1054','454',  "Windhorst 454nm BATC Medium Band 04 d");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 42, 'k1055','493',  "Windhorst 493nm BATC Medium Band 05 e ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 43, 'k1056','527',  "Windhorst 527nm BATC Medium Band 06 f ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 44, 'k1057','579',  "Windhorst 579nm BATC Medium Band 07 g ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 45, 'k1058','607',  "Windhorst 607nm BATC Medium Band 08 h");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 46, 'k1059','666',  "Windhorst 666nm BATC Medium Band 09 i ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 47, 'k1060','705',  "Windhorst 705nm BATC Medium Band 10 j ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 48, 'k1061','755',  "Windhorst 755nm BATC Medium Band 11 k");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 49, 'k1062','802',  "Windhorst 802nm BATC Medium Band 12 m ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 50, 'k1063','848',  "Windhorst 848nm BATC Medium Band 13 n ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 51, 'k1064','918',  "Windhorst 918nm BATC Medium Band 14 o");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 52, 'k1065','973',  "Windhorst 973nm BATC Medium Band 15 p ");
/* next line is a duplicate of k1000 */
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 53, 'c6000','blank',"No Filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 54, 'c6001','U',    "U CTIO set#1, UG2/1 mm +CuSO4, Johnson U");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 55, 'c6002','B',    "B CTIO set#1");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 56, 'c6003','V',    "V CTIO set#1 (cracked, Retired)");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 57, 'c6004','R',    "R CTIO set#1");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 58, 'c6005','I',    "I CTIO set#1 (has been damaged, replaced with c6028)");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 59, 'c6006','C',    "C Washington c6006  ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 60, 'c6007','M',    "M Washington c6007  ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 61, 'c6008','D51',  "D51 DDO 51 c6008  ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 62, 'c6009','ha',   "H-alpha 6563/80");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 63, 'c6011','ha8',  "H-alpha+8nm 6650/80");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 64, 'c6012','o2',   "[O II] 3727/50");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 65, 'c6013','s2',   "[S II] 6725/80 ");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 66, 'c6014','o3',   "[O III] 4990/50");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 67, 'c6015','g',    "SDSS g', set#3");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 68, 'c6016','wh',   "BK-7 Glass Filter");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 69, 'c6017','g',    "SDSS g', set#2");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 70, 'c6018','r',    "SDSS r', set#2");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 71, 'c6019','i',    "SDSS i', set#2");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 72, 'c6020','z',    "SDSS z', set#3");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 73, 'c6021','u',    "SDSS u', set#1, UG11 + an IR blocker");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 74, 'c6024','Bj',   "Bj Tyson 4350/1650");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 75, 'c6025','It',   "I Tyson 8800/2000");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 76, 'c6026','V',    "V CTIO replacement, used starting 21 Oct 2000");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 77, 'c6027','VR',   "VR SuperMacho, intended copy of VR Bernstein k1040");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 78, 'c6028','I',    "I CTIO (replacement for c6005)");
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 79, 'c9999','XX1',  "Rhoads 815 new k1046" );
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 80, 'c9998','XX2',  "Rhoads823 new k1047','keyword" );

/* Ruleset Table */
/* create processing data (data product-related object) table */
DROP TABLE ProcessingRules;
CREATE TABLE ProcessingRules (
    id_str            varchar(255) not null,     /* string rep of id, NOT unique by itself  */
    createMJD         double precision not null, /* date of creation */
    expireMJD         double precision default 99999.99, /* date of creation */
/*    obsConfig         int not null default -1,   /* applicable observation configuration */
    codeLocation      varchar (255) not null,    /* URL location of the code for this rule */
    codeType          varchar (32) not null,  /* Type of the code, any string allowed, but should obey a 'standard'. Right now we have 'CL' and 'Python' */
    parameterList     varchar (255) default null /* id's of parameters passed to the rule by the module. 
                                                    This is a space-delimited list of integers.
                                                    Used for creation of a validation script of the rule 
                                                    when run by a module. */
);

/* Create indice */
DROP INDEX RuleIdIndx;
CREATE UNIQUE INDEX RuleIdIndx on ProcessingRules ( id_str, createMJD );

/* Insert some rules to use */
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'test1', 50000.0, "test_rule.cl", "CL", "0 1");
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_biases', 50000.0, "enough_biases_v01.cl", "CL", "2"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'bias_ok', 50000.0, "bias_ok_v01.cl", "CL", "0 0 0");
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_domeflats', 50000.0, "enough_domeflats_v01.cl", "CL", "2"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'flat_ok', 50000.0, "flat_ok_v01.cl", "CL", "0 0 0 0 0");
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'flatshape_ok', 50000.0, "flatshape_ok_v01.cl", "CL", "0 0 0 0 0 0");
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'domeflat_range', 50000.0, "domeflat_range_v01.cl", "CL", "0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_for_putcal', 50000.0, "enough_for_putcal_v01.cl", "CL", "0 0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_for_fringe', 50000.0, "enough_for_fringe_v01.cl", "CL", "0 0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_usno_entries', 50000.0, "enough_usno_entries_v01.cl", "CL", "0" ); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_for_sflat', 50000.0, "enough_for_sflat_v01.cl", "CL", "0 0" ); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'use_fit_skyflat', 50000.0, "use_fit_skyflat_v01.cl", "CL", "0 0" ); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'remove_pupil_df', 50000.0, "remove_pupil_df_v01.cl", "CL", "0 0" ); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'ovscn_subtract_method', 50000.0, "ovscn_subtract_method_v01.cl", "CL", "0 0" ); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'enough_for_pupilghost', 50000.0, "enough_for_pupilghost_v01.cl", "CL", "0 0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'ftrevalrule', 50000.0, "ftrevalrule_v01.cl", "CL", "0 0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'good_for_sft', 50000.0, "good_for_sft_v01.cl", "CL", "0 0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'good_for_frg', 50000.0, "good_for_frg_v01.cl", "CL", "0 0"); 
INSERT INTO ProcessingRules ( id_str, createMJD, codeLocation, codeType, parameterList ) VALUES ( 'good_for_pgr', 50000.0, "good_for_pgr_v01.cl", "CL", "0 0"); 

/* Passed parameters for rules */
DROP TABLE ProcessingRuleParams;
CREATE TABLE ProcessingRuleParams (
    id              int not null, /* id of a parameter */
    name            varchar(64) not null, /* name of the parameter, as used in the code */
    dataType        int not null default 0, /* datatype. Values : 0 == string, 1 == integer, 2 == float/double precision */
    nullAllowed     int not null default 0/* is no value allowed for this parameter. 0 == 'no', 1 == 'yes' */
);

/* Create indice */
DROP INDEX RuleParamIdIndx;
CREATE UNIQUE INDEX RuleParamIdIndx on ProcessingRuleParams ( id );

/* Insert some common types of passed parameters rules may use */
INSERT INTO ProcessingRuleParams ( id, name, nullAllowed ) VALUES ( 0, 'OSF_DATASET', 0);
INSERT INTO ProcessingRuleParams ( id, name, nullAllowed ) VALUES ( 1, 'PROC_ID', 0);
INSERT INTO ProcessingRuleParams ( id, name, nullAllowed ) VALUES ( 2, 'numbias', 0);
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 81, 'k1044', 'Us', 'Us solid U k1044');
INSERT INTO Filters ( id, SN, name, description ) VALUES ( 82, 'k1045', 'Ud', 'Ud Dey k1045');
