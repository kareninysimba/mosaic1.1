# XTCOEFF: NOAO/IRAF V2.12.1-EXPORT sm@ctiokc Thu 12:51:13 22-Sep-2005
#             Images         Victim Bkg         Source Bkg        Victim 
# sm35.050909_0815.048.fits        Line median        Line median                  
# sm36.050909_0812.047.fits        Line median        Line median                  
# sm43.050909_0809.046.fits        Line median        Line median                  
# sm44.050909_0806.045.fits        Line median        Line median                  
# sm45.050909_0803.044.fits        Line median        Line median                  
# sm46.050909_0800.043.fits        Line median        Line median                  
# sm53.050905_1009.1063.fits        Line median        Line median                  
# sm53.050909_0757.042.fits        Line median        Line median                  
# sm57.050905_0953.1059.fits        Line median        Line median                  
# sm57.050909_0743.038.fits        Line median        Line median                  
# sm58.050905_0950.1058.fits        Line median        Line median                  
# sm58.050909_0740.037.fits        Line median        Line median                  
# sm63.050905_0947.1057.fits        Line median        Line median                  
# sm63.050909_0737.036.fits        Line median        Line median                  
# sm65.050905_0940.1055.fits        Line median        Line median                  
# sm67.050905_0933.1053.fits        Line median        Line median                  
# sm67.050909_0721.032.fits        Line median        Line median                  
# sm68.050905_0929.1052.fits        Line median        Line median                  
# sm68.050909_0718.031.fits        Line median        Line median                  
# sm69.050905_0927.1051.fits        Line median        Line median                  
# sm69.050909_0715.030.fits        Line median        Line median                  
# sm78.050905_0901.1045.fits        Line median        Line median                  
# sm78.050909_0650.024.fits        Line median        Line median                  
# sm79.050905_0858.1044.fits        Line median        Line median                  
# sm79.050909_0647.023.fits        Line median        Line median                  
# sm89.050905_0832.1037.fits        Line median        Line median                  
# sm89.050909_0936.068.fits        Line median        Line median                  
# sm8a.050905_0829.1036.fits        Line median        Line median                  
# sm8a.050909_0932.067.fits        Line median        Line median                  
# sm93.050905_0759.1028.fits        Line median        Line median                 
# sm93.050909_0901.059.fits        Line median        Line median                  
# sm99.050905_0826.1035.fits        Line median        Line median                  
# sm99.050909_0929.066.fits        Line median        Line median                  
# sm9a.050905_0823.1034.fits        Line median        Line median                  
# sm9a.050909_0926.065.fits        Line median        Line median                  
# sma4.050905_0730.1021.fits        Line median        Line median                  
# sma4.050909_0830.052.fits        Line median        Line median                  
# smaa.050905_0756.1027.fits        Line median        Line median                  
# smaa.050909_0858.058.fits        Line median        Line median                  
# smba.050905_0727.1020.fits        Line median        Line median                  
# smba.050909_0826.051.fits        Line median        Line median                  
# smc5.050905_0643.1009.fits        Line median        Line median                  
# smc5.050909_0548.010.fits        Line median        Line median                  
# smc6.050905_0646.1010.fits        Line median        Line median                  
# smc6.050909_0551.011.fits        Line median        Line median                  
# smd7.050905_0627.1004.fits        Line median        Line median                  
# smd7.050909_0532.005.fits        Line median        Line median                  
# smda.050905_0637.1007.fits        Line median        Line median                  
# smda.050909_0542.008.fits        Line median        Line median                  
# smdb.050905_0641.1008.fits        Line median        Line median                  
# smdb.050909_0545.009.fits        Line median        Line median                  
# sme8.050905_0620.1002.fits        Line median        Line median                  
# sme8.050909_0524.003.fits        Line median        Line median                  
# sme9.050905_0625.1003.fits        Line median        Line median                  
# sme9.050909_0530.004.fits        Line median        Line median                  

im01    im02     0.000775 (0.000003, 232.2)
im01    im03     0.001612 (0.000004, 381.9)
im01    im04     0.001688 (0.000004, 475.3)
im02    im01     0.000525 (0.000002, 252.8)
im02    im03     0.000999 (0.000004, 264.4)
im02    im04     0.001239 (0.000003, 391.1)
im03    im01     0.000105 (0.000003, 35.4)
im03    im02     0.000328 (0.000003, 116.9)
im03    im04     0.000318 (0.000003, 108.1)
im04    im01     0.001008 (0.000002, 450.4)
im04    im02     0.000642 (0.000003, 248.2)
im04    im03     0.000774 (0.000005, 166.0)
im05    im06     0.000323 (0.000002, 140.2)
im05    im07     0.001350 (0.000004, 338.2)
im05    im08     0.002039 (0.000003, 722.4)
im06    im05     0.000338 (0.000004, 104.0)
im06    im07     0.000322 (0.000004, 78.2)
im06    im08     0.001755 (0.000003, 579.8)
im07    im05     0.001336 (0.000004, 324.8)
im07    im06     0.000789 (0.000003, 303.9)
im07    im08     0.000762 (0.000004, 208.8)
im08    im05     0.000139 (0.000004, 33.6)
im08    im06     0.000003 (0.000002,  1.4)
im08    im07       0.000048 (0.000005, 30.8)
im09    im10      0.000147 (0.000001, 107.8)
im09    im11      0.000278 (0.000004, 73.4)
im09    im12      0.000218 (0.000004, 62.3)
im10    im09      0.000244 (0.000004, 67.9)
im10    im11     0.000656 (0.000004, 160.5)
im10    im12     0.000056 (0.000004, 98.2)
im11    im09      0.001249 (0.000003, 398.0)
im11    im10     0.000325 (0.000003, 94.8)
im11    im12     0.000250 (0.000003, 83.8)
im12    im09      0.001985 (0.000003, 620.0)
im12    im10     0.001612 (0.000006, 283.9)
im12    im11     0.000355 (0.000004, 88.2)
im13    im14     0.000762 (0.000003, 298.4)
im13    im15     0.001013 (0.000003, 339.1)
im13    im16     0.000777 (0.000004, 190.2)
im14    im13     0.000316 (0.000002, 149.3)
im14    im15     0.003365 (0.000003, 1307.6)
im14    im16     0.000318 (0.000003, 91.5)
im15    im13     0.000930 (0.000002, 520.9)
im15    im14     0.000210 (0.000002, 102.1)
im15    im16     0.000280 (0.000003, 89.0)
im16    im13     0.002076 (0.000002, 1052.8)
im16    im14     0.001619 (0.000002, 854.4)
im16    im15     0.000489 (0.000002, 198.0)
