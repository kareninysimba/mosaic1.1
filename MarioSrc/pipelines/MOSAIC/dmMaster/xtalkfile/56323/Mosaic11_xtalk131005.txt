# XTCOEFF: NOAO/IRAF V2.16 @dec01.tuc.noao.edu 
# Fri 18:32:17 04-Oct-2013 by D. Herrera
#             Images         Victim Bkg         Source Bkg        Victim Mask
#          kp1806318        Line median        Line median                   
#          kp1806344        Line median        Line median                   
#          kp1806369        Line median        Line median                   
#          kp1806381        Line median        Line median                   
#          kp1806382        Line median        Line median                   
#          kp1806392        Line median        Line median                   
#          kp1806457        Line median        Line median                   
#          kp1806492        Line median        Line median                   
#          kp1806606        Line median        Line median                   

im01	im09	 0.001859 (0.000019, 73.7) *
im01	im10	 0.000236 (0.000030,  1.2) *
im02	im10	 0.001871 (0.000016, 102.7) *
im02	im11	 0.000241 (0.000016, 102.7) *
im03	im11	 0.002090 (0.000019, 102.2) *
im03	im12	 0.000215 (0.000019, 11.2)
im11	im03	 0.002138 (0.000015, 107.4) *
im04	im12	 0.002371 (0.000016, 126.4) *
im12	im04	 0.002329 (0.000033, 52.7) *
im12	im03	 0.000215 (0.000033, 52.7) *
im05	im13	 0.001837 (0.000039, 30.4) *
im13	im05	 0.001985 (0.000018, 104.0) *
im13	im06	 0.000124 (0.000013,  9.7)
im06	im14	 0.001878 (0.000013, 132.9)
im06	im13	 0.000140 (0.000014,  9.8)
im14	im06	 0.001993 (0.000013, 134.8) *
im14	im07	 0.000161 (0.000019,  8.3)
im07	im15	 0.001642 (0.000012, 110.1) *
im07	im14	 0.000172 (0.000011, 15.9)
im15	im07	 0.001719 (0.000016, 95.6) *
im15	im08	 0.000170 (0.000014,  5.1)
im08	im16	 0.002083 (0.000013, 114.9) *
im08	im15	 0.000140 (0.000015,  1.4) *
im16	im08	 0.001983 (0.000026, 43.4) *
im09	im01	 0.001790 (0.000014, 110.2) *
im10	im02	 0.001578 (0.000012, 131.4)
im10	im01	 0.000201 (0.000014, 14.8)
