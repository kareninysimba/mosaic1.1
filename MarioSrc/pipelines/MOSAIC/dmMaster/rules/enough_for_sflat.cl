procedure enough_for_sflat_v01 (filter, numimg)

string	filter		{prompt="Filter name"}
int	numimg		{prompt="Number of images"}

begin
	int	n,l
	string	f

	n = numimg
	f = filter

	l = 4
	if (f == "default")
	    l = 4

	if (n>=l)
	    print ("1")
	else
	    print ("0")
end
