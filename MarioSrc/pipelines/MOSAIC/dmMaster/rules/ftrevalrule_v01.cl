# FTREVALRULE -- Evaluate the sky coverage histogram.
#
# The input is a file with the histogram of number of pixels excluded.
# The file is replaced by a file with the cumulative percentages.

procedure ftrevalrule (histfile)

file	histfile		{prompt="Input and output histogram file"}
string	params = ""		{prompt="Parameters"}

struct	*fd

begin
	real	x0 = 75.		# Fiducial point
	real	y0 = 10.		# Precentage at fidicial point
	real	slope = -0.055		# Slope of linear part of log/lin curve
	real	origin = 60.		# Origin
	real	pow

	bool	result
	int	nim, npix, bin, n, sum
	real	a, b, x, y, z, psum, thresh
	file	temp

	n = fscan (params, x0, y0, slope, origin)

	# Temporary file.
	temp = mktemp ("ftrev")

	# Set function parameters.
	x0 = (100 - x0)
	pow = slope * x0 / log10 (y0 / origin)

	# Determine total number of pixels and images.
	nim = 0; npix = 0
	fd = histfile
	while (fscan (fd, nim, n) != EOF)
	    npix += n

	# Convert input pixel histogram to cumulative percentages.
	sort (histfile, num+, rev+, > temp)
	delete (histfile)
	sum = 0; psum = 0; result = yes
	fd = temp
	while (fscan (fd, bin, n) != EOF) {
	    a = 100. * (nim - bin + 0.5) / nim
	    if (a > 100.)
	        break
	    b = 100. * n / npix
	    sum = sum + n
	    psum = psum + b

	    # Evaluate threshold.
	    x = 100 - a
	    if (x < x0)
		z = slope * x**pow / (pow * x0**(pow-1))
	    else
		z = slope * (x - x0) + slope * x0 / pow
	    thresh = origin * 10. ** z

	    if (psum < 0.001)
	        next
	    printf ("%3d %8.3f %8.3f %8d\n", a, psum, thresh, sum, >> histfile)
#	    printf ("%3d%% %8.3f%% %8.3f%% %8d\n", a, psum, thresh, sum,
#	        >> histfile)
	    if (a > 24. && a < 90. && psum > thresh)
	        result = no
	}
	fd = ""; delete (temp)

	print (result)
end
