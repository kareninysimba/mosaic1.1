#!/bin/env pipecl
#
# STKsetup2
#
# Description:
#
#   This module replaces the original rlist (created in stkselect.cl) with one
#   that contains full pathnames.  The full pathnames are obtained from 
#   a concatenation of the return files from STI.  Also, copy the CBPMs for
#   the resampled SIFs to the current directory.
#
# Exit Status Values:
#
#   0 = Unsuccessful
#	1 = Successful
#
# History
#
#   T. Huard   20090407	Created
#   R. Swaters          Changed name and added some code.
#   T. Huard   20090509 Clarified comment about ordering of olist (no longer
#       sorted by transparency, but not important).  Also, added check for
#       pre-existing rlist, which would occur in multiple runs of a dataset.
#   Last revised.   T. Huard    20090509    10:50am

# Declare variables
int     status = 1
string  dataset, datadir,lfile, olist, rlist, image, mask, lfile


# Set file and pathnames
names("stk",envget("OSF_DATASET"))
dataset=names.dataset
datadir=names.datadir
lfile=datadir//names.lfile
olist="stkolist.tmp"
rlist="stkrlist.tmp"
set (uparm = names.uparm)

# Load packages
images

# Log start of processing.
printf("\nSTKsetup2 (%s): ",dataset) | tee(lfile)
time | tee(lfile)

# Go to the relevant directory
cd(datadir)

# Concatenate the STI return file into one list containing only
# the resampled images and masks.
concat( "*.sti", > "stkgetmasks1.tmp" )
match( "_ss_rsp", "stkgetmasks1.tmp", > "stkgetmasks2.tmp" )
# Make a list of all the resampled masks
match( "bpm", "stkgetmasks2.tmp", >> "stkgetmasks3.tmp" )
# Make a list of all the resampled images
match( "bpm", "stkgetmasks2.tmp", stop+, >> "stkgetmasks4.tmp" )

# Loop over the input olist, find the matching cumulative bad pixel
# mask, and update the bpm keyword.
#
# THIS IS NO LONGER TRUE, BUT NOT IMPORTANT! -> Note that olist is
# sorted such
# that the image with the best transparency is first. This order is
# preserved for the list of resampled images in rlist. The loop below
# is done to make sure that all of the datasets successfully returned
# from the STI pipelines.
if (access(rlist)) delete(rlist,verify-)
;
list = olist
while ( fscan( list, s1 ) != EOF ) {

     # Reset the variables
     mask = ""
     image = ""

     # Extract the filename without the path
     s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )

     # Find the matching resampled mask for this input image
     match( s2, "stkgetmasks3.tmp" ) | scan( mask )
     # Find the matching resampled image for this input image
     match( s2, "stkgetmasks4.tmp" ) | scan( image )

     if ( ( mask != "" ) && ( image != "" ) ) {

         # Add the resampled image to rlist
         print( image, >> rlist )
         # Copy the resampled cumulative pixel mask to here
         s3 = substr( mask, strldx("/",mask)+1, 999 )
         imcopy( mask, s3 )
         # Add the name of the cumulative pixel mask to the header.
         # Normally this would be in the CBPM keyword, but imcombine
         # (in the next module) required the use of BPM
         hedit( image, "BPM", s3, add+, update+, ver- )

     } else {
         sendmsg( "WARNING",
             "No resampled image created; excluded from stack",
             image, "VRFY" )
     }

}
list = ""
#delete( "stkgetmasks?.tmp" )

logout( status )
