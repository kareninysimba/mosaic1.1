#!/bin/env pipecl
#
# STKSTISTART

int     status = 1
string  datadir, dataset, ilist, lfile, ofile, indir, olist
string  temp1, temp2, listoflists, ref
struct  *list2

# Load the packages.
images
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set the dataset name and uparm.
names( "stk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".stk"
olist = "stkolist.tmp"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSTKSELECT (%s):\n", dataset ) | tee(lfile)

# Go to the relevant directory
cd( datadir )

# Create the list of lists. This is done based on olist, not ilist,
# because olist contains only images with a WCS. Processing in the
# STI pipeline requires a WCS solution for transient detection
# and resampling.
listoflists = indir//dataset//"sti.lol"
# Read the first file from the list. This is passed on to each instance
# of the STI pipeline because a reference image is needed if 
# sciencepipeline==NO. In that case, RSPRA and RSPDEC are not set.
list = olist
i = fscan( list, ref )
list = ""
# Now loop over the list to create the list of lists.
list = olist
while ( fscan( list, s1 ) != EOF ) {
    s2 = substr( s1, strldx("/",s1)+1, strlstr("_ss",s1)-1 )
    s2 = substr( s2, 1, stridx("_",s2)-1 ) //
         substr( s2, stridx("_",s2)+1, 999 )
    printf( "%s %s\n", s1, ref, >> s2//".tmp" )
    print( s2//".tmp", >> "stkstistart1.tmp" )
}
list = ""
pathnames( "@stkstistart1.tmp", > listoflists )

# Call the STI pipelines 
temp1 = "stkstistart2.tmp"
temp2 = "stkstistart3.tmp"
callpipe( "stk", "sti", "split", listoflists, temp1, temp2 )
concat( temp2 ) | scan( status )
concat( temp1 )
printf( "Callpipe returned %d\n", status )

delete( "stkstistart?.tmp" )

logout( status )
