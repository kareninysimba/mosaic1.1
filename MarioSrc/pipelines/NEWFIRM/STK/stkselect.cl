#!/bin/env pipecl
#
# STKSELECT - Select the exposures to be stacked, and determine photometric
# scaling factors
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful (not implemented in module)
#   2 = Successful: No images to resample and stack 
#   3 = Successful: Only one image to resample and "stack"
#   4 = Successful: No images to stack, but there are images to resample.
#           This will be checked in stkstack.cl, and not taken care of in
#           stk.xml.  So, this could be changed to status 1 later.
#
# Future work
#
#   Currently, STKSCALE (and STKMZ) is computed only for those images that
#   survive rejection for WCS, keyword availability, and quality.  STKSCALE
#   is set to 0 for the images that are rejected for any of those reasons,
#   even though it could be computed, provided the MAGZERO keyword is
#   specified.  The code could be revised to provide a legitimate STKSCALE
#   in all cases possible, even if the images are not stacked.  This would
#   provide the greatest flexibility.  In that scenario, it would be 
#   possible to have STKSCALE < 1 (i.e., better transparency than the
#   reference image) if the rejection criteria are set in certain ways.
#
#   Would be nice to step back, and think whether the module can be made
#   more compact, yet straightforward and easier to follow.
#
# History
#
#   R. Swaters --------	Created.
#   T. Huard 200812--	Added code to provide weights for the SIFs depending on
#       the transparency and seeing, if the IRAF parameter
#       "stk_weight" is set to 1.
#   T. Huard 20081224	Included some error catching for cases when MAGZERO,
#       SEEING1, and/or SKYNOIS1 are not found in the FITS headers of
#       the SIFs.  If this happens, default values for MAGZERO, SEEING1,
#       SKYNOIS1 of 22., 1., 5., are assigned for now.  The default for
#       SKYNOIS1 is somewhat arbitrary (I looked at some SIFs and found
#       values of ~2.)
#   T. Huard 20090108	Following suggestions from Rob, default values for
#       MAGZERO, SEEING1, SKYNOIS1 were removed.  Any image lacking any
#       one of these keywords is removed from olist/rlist (i.e., not
#       processed further or stacked).  Also, added some documentation.
#   T. Huard 20090210	Added more information to the output table of accepted
#       images (stkselect-qual-accept.txt), information not used in
#       weighting but used in quality characterization (which has now
#       been added to the first pass).  An output list of images
#       rejected due to pipeline problems with data, such as those with
#       WCS solutions or missing keywords, is now created
#       (spkselect-pipe-reject.txt).  An output table of those images
#       rejected because of data quality, such as given by transparency
#       or seeing thresholds, is now created
#       (spkselect-qual-reject.txt).  In the current version of the 
#       module, however, there is no implementation of rejection based
#       on data quality.  Reworked some of the image loops, added more
#       documentation, and reformatted the file.
#   T. Huard 20090402   Create lists and list of lists ("stk2stiALL.list")
#       for call to STI.  The separate lists are two-column lists,
#       with the first column being the SIF filenames and the second
#       column being the reference SIF filenames (all the
#       same for each STK dataset/sequence).  The reference SIF
#       filename is included for the purpose of resampling in
#       stiresample.cl.  Finally, made sure file format conformed to
#       convention (there seemed to be some tab characters remaining).
#   T. Huard 200904--   Included call to rejection1.cl to control the rejection
#       of SIFs based on the quality (seeing, tranparency, skynoise).  The
#       rejection module is called with two inputs: the list file and 'method'
#       that controls the method used for rejection:
#       rejection(olist,stk_reject).  The stk_reject parameter in NEWFIRM.cl
#       determines the method.
#   T. Huard 20090503    olist now contains all SIFs, except for those that 
#       do not have legitimate WCS.  Previously, olist was used to reject
#       images without legitimate WCS, without certain keywords set, and of
#       poor quality.  This change was made in order to provide resampled
#       images for all images that can be resampled and to allow restacking
#       with different rejection criteria.  Also, the rejection module was
#       changed such that it no longer outputs the list of accepted images,
#       but outputs the exit status.  The rejection state of an image is saved
#       in the keyword REJECT (current state as progressing through pipeline),
#       REJECT1 (from first pass), REJECT2 (from second pass).
#   T. Huard 20090506   Included call to weighting1.cl to control the weighting
#       of SIFs based on quality (seeing, transparency, skynoise).  The
#       weighting module is called with two inputs: the list file and 'method'
#       that controls the method used for weighting.  The stk_weight parameter
#       in NEWFIRM.cl determines the method.
#   T. Huard 20090507   Added check for weighting module failure.  Also,
#       added STKMZ keywords to images and storing STKSCALE factors in PMAS
#       database; these were in previous versions, but had been lost.  STKMZ
#       is required by stkstack.
#   T. Huard 20090509   Added checks that there are still SIFs after the 
#       KW-rejection stage and the QUAL-rejection stage.  If not, then there
#       are images to be resampled, but not to be stacked.  In this case,
#       exit with status 4. Changed REJECT1->REJECT2, WTRNSNS1->WTRNSNS2, etc.
#   T. Huard 20090510   Added code to fix bugs when case of all but one image
#       is rejected due to WCS.  Also, needed to add STKMZ, STKSCALE keywords
#       to the remaining image in that case (for stkstack).
#   T. Huard 20090622   Fixed bug with calculation of scaling factors STKSCALE
#       based on relative transparencies.  Because of some code history, the
#       transparency and skynoise columns had been swapped, and the previous
#       version of this module was taking the skynoise for the transparency.
#	Last Revised:  T. Huard  200900622  2:10pm

# Declare variables.
int     status = 1
int     first = 1
int     istat
real    magzero,seeing,skynoise,skymode,trans,transREF,stkscale,stkmz
string  datadir,dataset,ilist,lfile,ofile,indir,olist,shortname
string  ref,msg,reject,cast,rejectTMP
struct  *list2

# Load the packages.
images
lists
proto
servers
dataio
task $paste = "$!paste -d\  $1 $2 > $3"
task $rejection = "$rejection1.cl $1 $2"
task $weighting = "$weighting1.cl $1 $2"

# Set the dataset name and uparm.
names( "stk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".stk"
olist = "stkolist.tmp"
ofile = dataset//"_stk"
shortname = names.shortname
set (uparm = names.uparm)

# Log the operation.
printf( "\nSTKSELECT (%s): ", dataset ) | tee(lfile)
time | tee (lfile)

# Go to the relevant directory
cd( datadir )

# *******************************************************************
# ***** PREFILTER FOR TESTING PURPOSES (only 1 SIF survive WCS) *****
# *******************************************************************
#list=ilist
#i=fscan(list,s1)
#hedit(s1,"REJECT","N!",add+,update+,verify-,show-)
#while(fscan(list,s1) != EOF) {
#    hedit(s1,"REJECT","Y!",add+,update+,verify-,show-)
#    hedit(s1,"WCSCAL","F",add+,update+,verify-,show-)
#}
#list=""
# *************************************************************************
# ***** PREFILTER FOR TESTING PURPOSES (only 1 SIF survive rejection) *****
# *************************************************************************
#list=ilist
#i=fscan(list,s1)
#hedit(s1,"REJECT","N!",add+,update+,verify-,show-)
#while(fscan(list,s1) != EOF) {
#    hedit(s1,"REJECT","Y!",add+,update+,verify-,show-)
#}
#list=""
# ********************************************************
# ***** MAKE SURE NO IMAGES ARE PRESET FOR REJECTION *****
# ********************************************************
#list=ilist
#while(fscan(list,s1) != EOF) {
#    hedit(s1,"REJECT","N!",add+,update+,verify-,show-)
#}
#list=""

# Make a table here, "stkselect-table.txt", that is composed of six columns:
# filename, relative transparency, seeing, skynoise, skymode, and magzero.
# These quantities are useful either for computing the relative weights to
# construct the stack or for assessing the quality of the stack.  The
# transparency is normalized such that it is unity for magzero=22.  This
# is fine since these will be used to compute relative weights.  Images
# that do not have values for all four keywords are written to a file,
# "stkselect-rejKW.txt" for later use in this module.  (This is because I
# want the keyword check to come after the WCS check, and the REJECT 
# values to reflect that.)
#
# Also, there are two "rejection info tables" ("stkselect-table2.txt",
# "stkselect-reject.txt") created in this module.  These first is simply
# a copy of "stkselect-table.txt", but with the images ordered as the
# REJECT keywords are set.  The second is the value of the REJECT keywords
# for the images in the same order.  In this way, the two rejection info
# tables can be pasted to remake "stkselect-table.txt" with a column 
# containing the REJECT value.
if (access("stkselect-table.txt")) delete("stkselect-table.txt",verify-)
;
if (access("stkselect-rejKW.txt")) delete("stkselect-rejKW.txt",verify-)
;
if (access("stkselect-table2.txt")) delete("stkselect-table2.txt",verify-)
;
if (access("stkselect-reject.txt")) delete("stkselect-reject.txt",verify-)
;
touch("stkselect-rejKW.txt")
list=ilist
while(fscan(list,s1) != EOF) {
    hselect(s1,"MAGZERO,SEEING1,SKYNOIS1,SKYMODE",yes) | scan(magzero,seeing,
        skynoise,skymode)
    if (nscan() == 4) {
        # If all four keywords were available, great!  Compute transparency
        # and write the quantities to the table.
        trans=10**((magzero-22.)/2.5)
    } else {
        # If all four keywords were not available, then go through the process
        # of determining which were missing.  Compute transparency if MAGZERO
        # is available.  For those keywords not available write INDEF in the 
        # table.
        magzero=INDEF ; seeing=INDEF ; skynoise=INDEF ; skymode=INDEF
        hselect(s1,"MAGZERO",yes) | scan(magzero)
        hselect(s1,"SEEING1",yes) | scan(seeing)
        hselect(s1,"SKYNOIS1",yes) | scan(skynoise)
        hselect(s1,"SKYMODE",yes) | scan(skymode)
        if (magzero != INDEF) {
            trans=10**((magzero-22.)/2.5)
        } else {
            trans=INDEF
        }
        printf("%s\n",s1,>>"stkselect-rejKW.txt")
    }
    printf("%s %s %s %s %s %s\n",s1,trans,seeing,skynoise,skymode,
        magzero,>>"stkselect-table.txt")
}
list=""

# ******************************
# *** REJECTION BASED ON WCS ***
# ******************************
# Remove images without WCS.  The list of images with WCS saved as olist.
# For images rejected based on WCS, set REJECT keyword accordingly.
if (access(olist)) delete(olist,verify-)
;
touch(olist)
list = ilist
while ( fscan( list, s1 ) != EOF ) {
    s2 = "F"
    hselect( s1, "WCSCAL", yes ) | scan( s2 )
    if ( s2 == "T" ) {
        if ( first == 1 ) {
            first = 0
            ref = s1
        }
        ;
        print( s1, >> olist )
    } else {
        # For images rejected because of lack of WCS, set REJECT=Ywcs, unless
        # it was previously set by the user to be Y!.  In that case
        # REJECT=Y!wcs.  If REJECT was previously set by the user to be N!,
        # the pipeline needs to overrule that decision.  In that case
        # REJECT=YwcsN! in order to reject the image based on WCS, but to let
        # others know that the user wanted originally to keep it.  In that
        # case, a message is sent to the message monitor to alert the operator
        # of this change.
        reject="" ; hselect(s1,"REJECT",yes) | scan(reject)
        rejectTMP=substr(reject,1,2)
        if (rejectTMP == "Y!") {
            reject=reject//"wcs"
        } else if (rejectTMP == "N!") {
            reject="YwcsN!"
            msg="Overruling rejection overrule (N! -> YwcsN!) for "//s1
            sendmsg("WARNING",msg,shortname,"PROC")
        } else {
            reject="Ywcs"
        }
        hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
        # Make sure the REJECT is copied to REJECT1
        hedit(s1,"REJECT1",reject,add+,update+,verify-,show-)
        # Update rejection info tables    
        match(s1,"stkselect-table.txt",>>"stkselect-table2.txt")
        printf("%s\n",reject,>>"stkselect-reject.txt")
        # Set the weighting keywords to 0.
        hedit(s1,"WTRNSNS",0.,add+,update+,verify-,show-)
        hedit(s1,"WSEEING",0.,add+,update+,verify-,show-)
        hedit(s1,"WTOT",0.,add+,update+,verify-,show-)
        hedit(s1,"WTRNSNS1",0.,add+,update+,verify-,show-)
        hedit(s1,"WSEEING1",0.,add+,update+,verify-,show-)
        hedit(s1,"WTOT1",0.,add+,update+,verify-,show-)
        hedit(s1,"STKSCALE",0.,add+,update+,verify-,show-)
    }
}
list = ""

# Following rejection of images based on WCS, determine number of exposures in
# the current group.  If there are no images left (exit status 2), or there is
# just one image left (exit status 3), then there is no need to proceed to the
# rejection and weighting code.
if ( access( olist ) ) {
    count( olist ) | scan( i )
} else
    i = 0
    
if (i==0)
    status = 2
else if (i==1) {
    # In the case where only one image remains, it essentially will be a stack
    # of itself.  The pipeline will set REJECT=N in this case, unless it was
    # previously set by a user to be N! or Y!.  In that case, it will retain its
    # value.
    status = 3
    fields(olist,"1",lines="1") | scan(s1)
    reject="" ; hselect(s1,"REJECT",yes) | scan(reject)  
    rejectTMP=substr(reject,1,2)
    if ((rejectTMP != "Y!") && (rejectTMP != "N!")) {
        reject="N"
        hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
        # Make sure the REJECT is copied to REJECT1
        hedit(s1,"REJECT1",reject,add+,update+,verify-,show-)
    }
    ;
    # Update rejection info tables   
    match(s1,"stkselect-table.txt",>>"stkselect-table2.txt")
    printf("%s\n",reject,>>"stkselect-reject.txt")

    # Set the weighting keywords to 1.  Set STKSCALE, STKMZ.
    hedit(s1,"WTRNSNS",1.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING",1.,add+,update+,verify-,show-)
    hedit(s1,"WTOT",1.,add+,update+,verify-,show-)
    hedit(s1,"WTRNSNS1",1.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING1",1.,add+,update+,verify-,show-)
    hedit(s1,"WTOT1",1.,add+,update+,verify-,show-)
    hedit(s1,"STKSCALE",1.,add+,update+,verify-,show-)
    stkmz=0. ; hselect(s1,"MAGZERO",yes) | scan(stkmz)
    hedit(s1,"STKMZ",stkmz,add+,update+,verify-,show-)
}
;
if (status != 1) {
    # Paste rejection info to table "stkselect-table.txt"
    paste("stkselect-table2.txt","stkselect-reject.txt","tmp.txt")
    rename("tmp.txt","stkselect-table.txt")
    # Clean up
    delete("tmp.txt",verify-)
    delete("stkselect-reject.txt",verify-)
    # Log out
    logout (status)
}
;

# ***************************************************
# *** REJECTION BASED ON AVAILABILITY OF KEYWORDS ***
# ***************************************************
# Reject images that do not have all four keywords - MAGZERO, SEEING1, SKYNOIS1,
# SKYMODE - written to the header.  It should be very rare that an image is
# rejected for this reason.  For images rejected for this reason, set their 
# REJECT keywords accordingly, and update rejection info tables.  Even if 
# stk_reject="none" and stk_weight=0 (i.e., no quality rejection is performed,
# and no weighting is done, except weighting by exposure time), we still reject
# images where one of these keywords has not been included because these are 
# important for characterizing the quality of the stack in stkqual.cl
#
# Make the list of images ("stkselect-inREJ.txt") that will be passed on to
# the quality rejection module.  These are all images in olist (which have
# already been cleaned from the WCS-problematic images), except for the images
# with keyword problems.
fields(olist,"1",>"stkselect-inREJ.txt")
list="stkselect-rejKW.txt"
while(fscan(list,s1) != EOF) {
    reject="" ; hselect(s1,"REJECT",yes) | scan(reject)    
    rejectTMP=substr(reject,1,2)
    if (rejectTMP == "Y!") {
        reject=reject//"kw"
    } else if (rejectTMP == "N!") {
        reject="YkwN!"
    } else {
        reject="Ykw"
    }
    hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
    # Make sure the REJECT is copied to REJECT1
    hedit(s1,"REJECT1",reject,add+,update+,verify-,show-)
    # Update rejection info tables    
    match(s1,"stkselect-table.txt",>>"stkselect-table2.txt")
    printf("%s\n",reject,>>"stkselect-reject.txt")
    # Filter out the keyword-rejected image from the image list that will
    # be given to the quality rejection module.
    match(s1,"stkselect-inREJ.txt",stop+,>"tmp.txt")
    rename("tmp.txt","stkselect-inREJ.txt")
    # Set the weighting keywords to 0.
    hedit(s1,"WTRNSNS",0.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING",0.,add+,update+,verify-,show-)
    hedit(s1,"WTOT",0.,add+,update+,verify-,show-)
    hedit(s1,"WTRNSNS1",0.,add+,update+,verify-,show-)
    hedit(s1,"WSEEING1",0.,add+,update+,verify-,show-)
    hedit(s1,"WTOT1",0.,add+,update+,verify-,show-)
    hedit(s1,"STKSCALE",0.,add+,update+,verify-,show-)
}
list=""

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 4 (images to resample, but no images to stack).
count("stkselect-inREJ.txt") | scan(i)
if (i==0) {
    msg="No more SIFs to reject/weight following WCS+KW rejection in STKselect"
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
    status=4
    logout(status)
}
;

# ****************************************
# *** REJECTION BASED ON IMAGE QUALITY ***
# ****************************************
# Now, reject images based on their quality (seeing, transparency, skynoise),
# if stk_reject is not equal to "none".  Apply the rejection method specified
# by stk_reject.  The rejection module creates a list ("rejection.out"), which
# is a two column list, with the first column including all images input to the
# module and the second column including the REJECT values assigned.  The output
# of the rejection module is its exit status.  If call to rejection fails, then
# report this to the message monitor and proceed with no quality rejection done.
# Finally, create the list of images that will be given to the weighting code
# and set weight keywords (WTRNSNS, WSEEING, WTOT) for those images rejected
# based on quality that will not be forwarded to the weighting code.  An
# alternative approach could be to create the weighting list and set the
# weighting keywords in a separate loop.
# 
# *** SHOULD I MAKE A CHECK HERE THAT THERE ARE SUFFICIENT IMAGES IN 
# *** stkselect-inREJ.txt TO SEND TO REJECTION?
if (access("stkselect-inWEIGHT.txt")) delete("stkselect-inWEIGHT.txt",verify-)
;
touch("stkselect-inWEIGHT.txt")
if (stk_reject != "none") {
    line="" ; rejection("stkselect-inREJ.txt",stk_reject) | scan(line)
    print(line) | scan(istat)   # line->character is necessary because of 
                                # format of the rejection output
    if (istat != 1) {
        msg="STK rejection failed. Setting all REJECT=N except where overruled."
        sendmsg("ERROR",msg,shortname,"PROC")
        printf("%s\n",line) | tee(lfile)        
        list="stkselect-inREJ.txt"
        while(fscan(list,s1) != EOF) {
            reject="" ; hselect(s1,"REJECT",yes) | scan(reject)
            rejectTMP=substr(reject,1,2)
            if ((rejectTMP != "N!") && (rejectTMP != "Y!")) {    
                reject="N"
                hedit(s1,"REJECT",reject,add+,update+,verify-,show-)
            }
            ;
            # Make sure the REJECT is copied to REJECT1
            hedit(s1,"REJECT1",reject,add+,update+,verify-,show-) 
            # Update rejection info tables   
            match(s1,"stkselect-table.txt",>>"stkselect-table2.txt")
            printf("%s\n",reject,>>"stkselect-reject.txt")        
        }
        list=""
        copy("stkselect-inREJ.txt","stkselect-inWEIGHT.txt")
    } else {
        # Update rejection info tables, making use of the output file of 
        # rejection module, which is a two-column table.  The first column
        # includes the filenames, and the second column includes the 
        # REJECT values.
        rename("rejection.out","stkselect-outREJ.txt")
        fields("stkselect-outREJ.txt","1",>"tmpF.txt")
        fields("stkselect-outREJ.txt","2",>"tmpR.txt")
        list="tmpF.txt"
        list2="tmpR.txt"
        while((fscan(list,s1) != EOF) && (fscan(list2,reject) != EOF)) {
            # Update rejection info tables.            
            match(s1,"stkselect-table.txt",>>"stkselect-table2.txt")
            printf("%s\n",reject,>>"stkselect-reject.txt")
            # If the image is not rejected based on quality, include it in
            # the list for weighting.  If the image was rejected, set the
            # weighting keywords to 0, and do not include it in the list
            # for weighting.
            if (substr(reject,1,1) == "N") {
                printf("%s\n",s1,>>"stkselect-inWEIGHT.txt")
            } else {
                hedit(s1,"WTRNSNS",0.,add+,update+,verify-,show-)
                hedit(s1,"WSEEING",0.,add+,update+,verify-,show-)
                hedit(s1,"WTOT",0.,add+,update+,verify-,show-)
                hedit(s1,"WTRNSNS1",0.,add+,update+,verify-,show-)
                hedit(s1,"WSEEING1",0.,add+,update+,verify-,show-)
                hedit(s1,"WTOT1",0.,add+,update+,verify-,show-)
                hedit(s1,"STKSCALE",0.,add+,update+,verify-,show-)
            }
            # Make sure the REJECT is copied to REJECT1
            hedit(s1,"REJECT1",reject,add+,update+,verify-,show-)
        }
        list=""
        list2=""
        delete("tmpF.txt",verify-)
        delete("tmpR.txt",verify-)
    }
}
;

# Since the REJECT keywords should all be set now, paste the rejection
# info tables to generate an updated table "stkselect-table.txt".
paste("stkselect-table2.txt","stkselect-reject.txt","tmp.txt")
rename("tmp.txt","stkselect-table.txt")
delete("stkselect-table2.txt",verify-)
delete("stkselect-reject.txt",verify-)

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 4 (images to resample, but no images to stack).
count("stkselect-inWEIGHT.txt") | scan(i)
if (i==0) {
    msg="No SIFs to weight following WCS+KW+QUAL rejection in STKselect"
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
    status=4
    logout(status)
}
;

# **********************************
# *** WEIGHTING BASED ON QUALITY ***
# **********************************
# Prepare the input list for the weighting module, and sort the entries
# in order of decreasing transparency.  Compute the relative scaling,
# based on transparency, and write these to STKSCALE.
# Note that a lesser zeropoint (MAGZERO), or lesser transparency, means
# that the image has lower throughput, and therefore it needs to be scaled
# up.  
#
# Even if stk_weight="none", this list preparation is done since it 
# enables a convenient way to set set STKSCALE in the images.
if (access("tmp.txt")) delete("tmp.txt",verify-)
;
list="stkselect-inWEIGHT.txt"
while(fscan(list,s1) != EOF) {
    match(s1,"stkselect-table.txt",>>"tmp.txt")
}
list=""
fields("tmp.txt","1,3,2,4",lines="1-",>"tmp2.txt")
sort("tmp2.txt",column=3,numeric+,reverse_sort+,>"stkselect-inWEIGHT.txt")
delete("tmp.txt",verify-)
delete("tmp2.txt",verify-) 
# At this point, the list contains all images that will be stacked,
# even if each will receive different weights.  So, compute the relative
# transparency scaling factors (not the weight!) and include in the
# STKSCALE keywords.  Also, record the MAGZERO of the reference image
# (which is also appropriate for scaled images) to STKMZ keywords.  Finally,
# create a table "stkselect-STKSCALE.txt" of filename and stkscale for info.
if (access("stkselect-STKSCALE.txt"))
    delete("stkselect-STKSCALE.txt",verify-)
;
fields("stkselect-inWEIGHT.txt","1,3",lines="1") | scan(s1,transREF)
hselect(s1,"MAGZERO",yes) | scan(stkmz) # could be recal from transREF
list="stkselect-inWEIGHT.txt"
###while(fscan(list,s1,seeing,skynoise,trans) != EOF) {
while(fscan(list,s1,seeing,trans,skynoise) != EOF) {
    stkscale=transREF/trans
    hedit(s1,"STKSCALE",stkscale,add+,update+,verify-,show-)
    hedit(s1,"STKMZ",stkmz,add+,update+,verify-,show-)
    printf("%s %s\n",s1,stkscale,>>"stkselect-STKSCALE.txt")
    # Create a short version of the dataset name; store scale factor in PMAS
    s2=substr(s1,strldx("/",s1)+1,strlstr(".fits",s1)-1)
    printf("%12.5e\n",stkscale) | scan(cast)
    setkeyval(class="objectimage",id=s2,dm=dm,keyword="dqphscal",value=cast)
}    
list=""

# If "stk_weight" is set to something other than "none", then compute weighting
# factors based on observing conditions (seeing, transparency, and skynoise)
# using the weighting1 module.  These weighting factors are stored as keywords
# WTRNSNS, WSEEING, WTOT.  These are only computed for the images that are not
# previously rejected based on WCS, availability of keywords, and quality; for 
# the rejected images, these weighting factors have already been set to 0.
# If stk_weight="none", then these weighting factors are not computed, but just
# set to unity (except for the ones that were previously rejected, where the
# weights were sets to 0). 

# It should not be necessary to explicitly
# weight by the exposure time since this dependence is implicit in the sky
# noise level.  For example, if we were combining 1-sec and 2-sec exposures
# having the same seeing and transparency, the 1-sec exposures should have
# a sky noise (per second) that is greater than the sky noise of the 2-sec
# exposures by ~sqrt(2).
#
# If "stk_weight" is not set, then weight data depending on the exposure 
# time only.  Instead of exposure time, we could instead weight by the 
# 1/(sky noise)^2, similar to what is done when weighting by seeing,
# transparency, and sky noise.
if (stk_weight != "none") {
    # Call the weighting module
    line="" ; weighting("stkselect-inWEIGHT.txt",stk_weight) | scan(line)
    print(line) | scan(istat)   # line->character is necessary because of 
                                # format of the rejection output
    if (istat != 1) {
        msg="STK weighting failed. Setting WSEEING,WTRNNS,WTOT to 1."
        sendmsg("ERROR",msg,shortname,"PROC")
        printf("%s\n",line) | tee(lfile)        
        list="stkselect-inWEIGHT.txt"
        while(fscan(list,s1) != EOF) {
            hedit(s1,"WTRNSNS",1.0,add+,update+,verify-,show-)
            hedit(s1,"WSEEING",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTOT",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTRNSNS1",1.0,add+,update+,verify-,show-)
            hedit(s1,"WSEEING1",1.0,add+,update+,verify-,show-)
            hedit(s1,"WTOT1",1.0,add+,update+,verify-,show-)
        }
        list=""
    } else {
        rename("weighting.out","stkselect-outWEIGHT.txt")
    }
} else {
    # In this case, the weights are set to unity.  Weighting by
    # the exposure times will be performed in the imcombine statement
    # in stkstack.
    list="stkselect-inWEIGHT.txt"
    while(fscan(list,s1) != EOF) {
        hedit(s1,"WTRNSNS",1.0,add+,update+,verify-,show-)
        hedit(s1,"WSEEING",1.0,add+,update+,verify-,show-)
        hedit(s1,"WTOT",1.0,add+,update+,verify-,show-)
        hedit(s1,"WTRNSNS1",1.0,add+,update+,verify-,show-)
        hedit(s1,"WSEEING1",1.0,add+,update+,verify-,show-)
        hedit(s1,"WTOT1",1.0,add+,update+,verify-,show-)
    }
	list=""
}

# Clean up.
# DO NOT REMOVE stkselect-table.txt SINCE IT IS NEEDED BY stkqual.cl!

printf("Exit code: %d\n",status)

logout(status)
