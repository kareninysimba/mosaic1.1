#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( "stk", envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".stk"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nSTKSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Check if input files are accessible.
if (defvar( "PIPEDEBUG" )) {
    type( ifile )
    list = ifile
    while( fscan( list, s1 ) != EOF ) {
        if ( !imaccess( s1 ) ) {
            sendmsg ("ERROR", "Can't access image", s1, "CAL")
            status = 0
        }
        ;
    }
    list = ""
    if (status == 0)
        logout (status)
    ;
}
;

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

logout( status )
