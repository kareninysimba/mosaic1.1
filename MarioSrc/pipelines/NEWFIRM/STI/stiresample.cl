#!/bin/env pipecl
#
# STIresample
#
# Description:
#
#   Resamples the SIF.
#
# Exit Status Values:
#
#   0 = Unsuccessful (set for error in reading SIFnamefile or CBPMnamefile,
#                   or for error in retrieving the reprojection center)
#	1 = Successful
#
# History:
#
#   R. Swaters  --------    Created.
#   T. Huard    20090406    IMPORTED TO STI PIPELINE, which works on an
#                           individual SIF instead of a list of SIFs.  Renamed
#                           stkresample.cl to stiresample.cl, and revised
#                           accordingly.  Also, renamed mask output by 
#                           mscimage from "_rsp_bpm.pl" to "_rsp_cbpm.pl" to be
#                           explicitly clear with which mask we are working,
#                           and changed BPM keyword value in the resampled SIF
#                           accordingly.
#   Last Revised:  T. Huard  20090407  12:45pm
#
#  WHAT DOES mscimage USE FOR BPM?  VALUE OF BPM KEYWORD??

int     status = 1
real    ra,dec
string  dataset,datadir,shortname,lfile
string  SIFnamefile,REFnamefile,CBPMnamefile
string	sifFULL,sifREFFULL,sif,interpolant

# Load the packages.
redefine mscred=mscred$mscred.cl
images
lists
noao
nproto
mscred
#cache mscred
#Why is the "cache mscred" necessary?

# Set the dataset name and uparm.
names( "sti", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
lfile = datadir // names.lfile
SIFnamefile="stisetup-SIF.tmp"
REFnamefile="stisetup-REF.tmp"
CBPMnamefile="stisetup-CBPM.tmp"
set (uparm = names.uparm)
#mscred.logfile=lfile -- NOT NECESSARY?
#mscred.instrument="pipedata$mosaic.dat" -- NOT NECESSARY?

# Log start of processing.
printf ("\n%s (%s): ",envget("NHPPS_MODULE_NAME"),dataset) | tee (lfile)
time | tee (lfile)

# Go to the relevant directory
cd( datadir )

# Read SIFnamefile and REFnamefile to get full pathname of SIF (sifFULL) and
# the reference SIF (sifREFFULL).  Extract basename (sif).
fields(SIFnamefile,"1",lines="1") | scan(sifFULL)
fields(REFnamefile,"1",lines="1") | scan(sifREFFULL)
sif=substr(sifFULL,strldx("/",sifFULL)+1,strlstr(".fits",sifFULL)-1)

# Resample the SIF
if (sciencepipeline) {

    hselect( sifREFFULL, "RSPRA,RSPDEC", yes ) | scan( ra, dec )
    if ( nscan() != 2 ) {
        sendmsg( "ERROR", "Could not retrieve reprojection center", 
            shortname, "PROC" )
        status = 0
    } else {
        interpolant = "linear"
        mscimage( sif, sif//"_rsp", format="image", verbose+,
            ra=ra, dec=dec, scale=rsp_scale, rotation=0,
            wcssource="parameters", interpolant=interpolant )
    }
    
} else {

    interpolant = "linear"
    mscimage( sif, sif//"_rsp", format="image", verbose+,
        scale=rsp_scale, rotation=0, wcssource="parameters",
        reference=sifREFFULL, interpolant=interpolant )
        
}

hedit(sif//"_rsp","BPM",sif//"_rsp_cbpm.pl",add+,verify-,update+)
imrename(sif//"_rsp_bpm.pl",sif//"_rsp_cbpm.pl")
nhedit (sif//"_rsp", "PIXSCALE", rsp_scale, "[arc/pix] Pixel scale", add+)
nhedit (sif//"_rsp", "PIXSCAL1", rsp_scale, "[arc/pix] Pixel scale", add+)
nhedit (sif//"_rsp", "PIXSCAL2", rsp_scale, "[arc/pix] Pixel scale", add+)

logout( status )
