#!/bin/env pipecl
#
# STItrdet
#
# Description:
#
#   This module runs craverage on a list of non-resampled SIFs.  The results are
#   "fixed" SIFs (present in the working directory) and transient-flagged pixel
#   masks merged with the previous cumulative bad pixel masks.  The transient-
#   flagged pixels are given a value 8 in the merged masks, except for pixels
#   that were already flagged in the cumulative mask, in which case they retain 
#   their original values.  The merged masks are present in the current working
#   directory, but also copied to the archived mask location.
#
# Exit Status Values:
#
#   0 = Unsuccessful (set for error in reading SIFnamefile or CBPMnamefile)
#	1 = Successful
#
# History:
#
#   T. Huard  20081027	The 20081002 version of this module was copied from
#			MOSAIC's SRS pipeline and slightly revised to be
#			appropriate for NEWFIRM's STK pipeline.  Revisions
#			included using 'olist' instead of 'ifile' as the input
#			list and reading only the filenames from this list (and
#			not the ra and dec tangent points).  IT MAY BE POSSIBLE
#			AND BETTER TO REVISE PROCEDURE FURTHER TO HAVE
#			CRAVERAGE WORK ON A LIST RATHER THAN INDIVIDUAL
#			IMAGES (FOR BOTH MOSAIC AND NEWFIRM).
#   T. Huard  20081027	Use getcal to retrieve the stored location of masks,
#			and copy the revised masks to this location.
#   T. Huard  20081113	Cleaned up the code.  Now removing extra backup files
#			that are created, and removed development code that
#			took care of module crashes and multiple runs.
#			Removed exit status value of 3, replacing that case
#			with a WARNING in the logs and allowing pipeline to
#			continue running without intervention.
#   T. Huard  20081121	Making some minor corrections, as suggested by Rob.
#			Among these changes...module is no longer checking that
#			BPM keyword value exists and exiting with status of 2.
#   T. Huard  20081125	Transient-flagged pixels are given a value of 8 in the
#			masks.
#   T. Huard  20081126	Limited module width to 80 characters. Changed
#			"exitval" to "status" for consistency with other
#			modules. Replaced some messages for log with
#			'sendmsg' statements.
#   T. Huard  200904--    IMPORTED TO STI PIPELINE, which works on an individual
#           SIF instead of a list of SIFs.  Renamed stktr.cl to stitrdet.cl,
#           and revised accordingly.  With the creation of stisetup.cl, some
#           of the original code found in stktr.cl has been removed in
#           stitrdet.cl.  For example, the CBPM (and SIF) is now stored in a
#           CBPMnamefile (SIFnamefile).  So, a getcal is no longer necessary
#           in this module.
#   Last Revised:  T. Huard  20090407  8:30am

# Declare variables
int     status = 1
string  dataset,datadir,shortname,lfile,SIFnamefile,CBPMnamefile
string  msg,sifFULL,sif,cbpmFULL,cbpm,cbpmORIG,sifORIG,sifcrav

# Load packages
redefine mscred=mscred$mscred.cl
noao
imred
crutil
nproto
servers
mscred
cache mscred

# Set file and path names
names("sti",envget("OSF_DATASET"))
dataset=names.dataset
datadir=names.datadir
shortname=names.shortname
lfile=datadir//names.lfile
SIFnamefile="stisetup-SIF.tmp"
CBPMnamefile="stisetup-CBPM.tmp"
set (uparm=names.uparm)
set (pipedata=names.pipedata)
cd (datadir)
#mscred.logfile=lfile
#mscred.instrument="pipedata$mosaic.dat"

# Log start of processing.
printf ("\n%s (%s): ",envget("NHPPS_MODULE_NAME"),dataset) | tee (lfile)
time | tee (lfile)

# Check sti_trrej to determine whether craverage should be used.  If not, log
# out with exit status of 1 (successful).
if ((sciencepipeline == no) || (sti_trrej != "craverage")) {
    if (sciencepipeline == no) {
        msg="STITR: NEWFIRM running QRP, not science pipeline"
        printf("%s\n",msg) | tee(lfile)
    }
    ;
    if (sti_trrej != "craverage") {
        msg="STITR: sti_trrej not set to craverage"
        printf("%s\n",msg) | tee(lfile)
    }
    ;
    msg="STITR: No CR/transient rejection done in this module."
    printf("\n%s",msg) | tee(lfile)
    logout(1)
}
;

# Read sifnamefile to get full pathname of SIF (sifFULL).
# Extract basename (sif).
fields(SIFnamefile,"1",lines="1") | scan(sifFULL)
sif=substr(sifFULL,strldx("/",sifFULL)+1,strlstr(".fits",sifFULL)-1)
sifcrav=sif//"_crav"

# Read CBPMnamefile to retrieve full pathname of CBPM.  Extract the basename
# of the CBPM.  The working version of the CBPM has been copied over to the
# current directory by stisetup, but the full pathname, as given by getcal,
# is necessary to update the CBPM.
# Rename non-craveraged CBPM (append "_NOCRMED").  Then, later this original
# CBPM can be merged with the craverage-output mask and given the original
# CBPM filename in current directory.
fields(CBPMnamefile,"1",lines="1") | scan(cbpmFULL)
cbpm=substr(cbpmFULL,strldx("/",cbpmFULL)+1,999)
cbpmORIG=substr(cbpm,1,strlstr(".pl",cbpm)-1)//"_NOCRMED"//".pl"
rename(cbpm,cbpmORIG)

# Make a copy of the SIF (_NOCRMED), prior to fixpix-ing it in craverage.
# This isn't really necessary since the pre-fixpix'd SIF will be saved
# in the original full pathname version (sifFULL) and the fixpix'd is 
# saved in the current working directory (sif).
sifORIG=sif//"_NOCRMED"
imcopy(sif,sifORIG)

# REMOVE COSMIC RAYS AND ARTIFACTS.
# Run craverage on SIF.  Output is a fixpix'd image (sif) saved to the current
# working directory (the original pre-fixpix'd image is saved with "_NOCRMED"
# tag).  Also output is a mask (_crmask) flagging the pixels identified by
# craverage.
craverage(sif,sifcrav,crmask=sif//"_crmask",average="",sigma="",
    navg=5,nrej=1,nbkg=5,nsig=25,var0=0.,var1=0.,var2=0.,
    crval=8,lcrsig=5,hcrsig=5,crgrow=0.,objval=0,lobjsig=10.,
    hobjsig=3.,objgrow=0.)
if (imaccess(sifcrav) == NO) {
    msg="craverage failed; non-cravg propagated instead"
    sendmsg("WARNING",msg,shortname,"PROC")
    # Copy non-craverage-corrected SIF; CBPM has already been copied
    # in stisetup
    imcopy(sif,sifcrav)
} else {
    # Merge the mask from craverage with the original mask.  Pixels
    # identified in the craverage mask are assigned a value of 8 in
    # the merged mask, while pixels identified as bad in the
    # original mask are assigned their original values.  If a pixel
    # is identified both in craverage mask and the original mask,
    # that pixel is flagged in the merged mask with the original bad
    # pixel value!  In other words, if a pixel is identified as
    # "bad", we do not trust that craverage has identified a cosmic
    # ray hit, transient, or artifact affecting that pixel.
    mskexpr("m>0?m:(i>0? i: 0)",cbpm,sif//"_crmask"//".fits[pl]",
        refmask=cbpmORIG)

    # Update the CBPM
    delete(cbpmFULL,verify-)
    imcopy(cbpm,cbpmFULL)
}
# Rename sifcrav to sif
imdel( sif )
imrename( sifcrav, sif )

# Redefine sifFULL to the version in current working directory.
# Update SIFnamefile.
pathnames(datadir//sif//".fits") | scan(sifFULL)
delete(SIFnamefile,verify-)
printf("%s\n",sifFULL,>SIFnamefile)

# Clean up.
#delete("*_NOCRMED*",verify-)

logout(status)
