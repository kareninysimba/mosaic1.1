#!/bin/env pipecl
#
# NFDFEED -- Feed NEWFIRM data to MTD

int	seqno, seqtot
string	dataset, indir, datadir, ilist, lfile, mtd, mtdid
string	queue, ds

int	ominseq = 2	# Minimum number in object sequence

# Tasks and packages.
task pipeselect = $!pipeselect
task setseqid = NHPPS_PIPESRC$/NEWFIRM/DIR/setseqid.cl
images
utilities

cache setseqid
unlearn setseqid

# Set filenames.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // "." // names.pipe
lfile = datadir // names.lfile

# Log start of processing.
    printf ("\n%s (%s): ", strupr(envget("NHPPS_MODULE_NAME")), dataset) | tee (lfile)
time | tee (lfile)

# Find MTD pipeline.
mtd = ""; pipeselect ("NEWFIRM", "mtd", 1, 0) | scan (mtd)
if (mtd == "") {
    sendmsg ("WARNING", "No pipeline running", "mtd", "PIPE")
    logout 2
}
;

# Loop through files and send them to the MTD pipeline.
sort (ilist, > "nfdfeed.tmp")
rename ("nfdfeed.tmp", ilist)
list = ilist
while (fscan( list, s1) != EOF) {
    s2 = "none"; s3 = "unknown_unknown"; seqno = 1; seqtot = 1
    hselect (s1//"[0]", "OBSID,SEQID,NOCNO,NOCTOT", yes) |
	scan (s2, s3, seqno, seqtot)

    print (s3) | translit ("STDIN",
	"`-=[]\;',./~!@#$%^&*()+{}|:<>?"//'"', del+) | scan (s3)

    # Reset the sequence ID.
    queue = ""; ds = ""
    print (s3) | translit ("STDIN", "_", " ") | scan (queue, ds)

    setseqid (queue, ds, seqno, seqtot, s2)

    if (s2 == "none" || s3 == "unknown_unknown" ||  seqtot < ominseq)
        next
    ;

    if (setseqid.mtdend1 != "") {
        mtdid = setseqid.mtdend1
        printf ("Sequence end trigger: %s\n", mtdid)
	touch (mtd//mtdid)
	sleep (1)
    }
    ;

    if (setseqid.mtdend2 != "") {
        mtdid = setseqid.mtdend2
        printf ("Sequence end trigger: %s\n", mtdid)
	touch (mtd//mtdid)
	sleep (1)
    }
    ;

    mtdid = setseqid.mtd
    printf ("Dataset Trigger: %s\n", mtdid)
    print (s1, > mtd//mtdid)
    touch (mtd//mtdid//"trig")
    sleep (15)
}
list = ""

logout 1
