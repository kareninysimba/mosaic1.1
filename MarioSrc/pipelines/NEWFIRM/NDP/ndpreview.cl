#!/bin/env pipecl
# 
# NDPREVIEW -- Review data products.
#
# This routine makes a review HTML.

string  dataset, datadir, ilist, lfile, pngdir, pngfits, html, prophtml
string	root, lastroot, propid

# Load tasks and packages
task $resize = "$!convert -resize"
task $ln = "$!ln -sf"
task $readlink = "$!readlink"
task ndpprophdr = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndpprophdr.cl"
task ndppropseq = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndppropseq.cl"
task ndpsummary = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndpsummary.cl"
task ndpsummary1 = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndpsummary1.cl"
cache ndppropseq
unlearn ndppropseq
dppkg

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = names.indir // dataset // ".ndp"
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Log start of module.
printf ("\n%s (%s): ", strupr(envget("NHPPS_MODULE_NAME")), dataset) |
    tee (lfile)
time | tee (lfile)

# Create data subdirectories.  Note that this may be called more than once
# so we delete any old data.
cd (names.rootdir//"output")
if (access("html")==NO)
    mkdir ("html")
;
if (access("png")==NO)
    mkdir ("png")
;
if (access("observer")==NO)
    mkdir ("observer")
;
if (access("logs")==NO)
    mkdir ("logs")
;
cd ("png")
pngdir = names.shortname//"/"
if (access(pngdir))
    delete (pngdir//"*")
else
    mkdir (pngdir)
cd (pngdir)
pngdir = "../png/"//names.shortname//"/"
html = "../../html/"//names.shortname // ".html"
prophtml = "dummy"
if (access(html))
    delete (html)
;

# Extract lists of PNGs.
concat (datadir//"*.mkd") | match ("_png", > "ndprev.tmp")
match ("_raw_png", "ndprev.tmp", > "ndprev_raw.tmp")
match ("_press_png", "ndprev.tmp", > "ndprev_press.tmp")
match ("_ss_png", "ndprev.tmp", > "ndprev_ss.tmp")
match ("_r_png", "ndprev.tmp", > "ndprev_rsp.tmp")
match ("_stk_png", "ndprev.tmp", > "ndprev_stk.tmp")
match ("_spk_png", "ndprev.tmp", > "ndprev_spk.tmp")

# Eliminate unpopulated columns.
count ("ndprev_ss.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_ss.tmp")
;
count ("ndprev_press.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_press.tmp")
;
count ("ndprev_raw.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_raw.tmp")
;
count ("ndprev_rsp.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_rsp.tmp")
;
count ("ndprev_stk.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_stk.tmp")
;
count ("ndprev_spk.tmp") | scan (i)
if (i == 0)
    delete ("ndprev_spk.tmp")
;

# Create list of datasets.
list = "ndprev.tmp"; lastroot = ""
while (fscan (list, s1) != EOF) {
    root = substr (s1, strldx("/",s1)+1, strstr("_png.fits",s1)-1)
    root = substr (root, 1, strldx("_",root)-1)
    s2 = ""
    match (root//"_ss", "ndprev.tmp") | scan (s2)
    if (s2 == "")
	match (root//"_press", "ndprev.tmp") | scan (s2)
    if (s2 == "")
	match (root//"_r", "ndprev.tmp") | scan (s2)
    if (s2 == "")
	match (root//"_spk", "ndprev.tmp") | scan (s2)
    if (s2 == "")
	match (root//"_stk", "ndprev.tmp") | scan (s2)
    if (s2 != "")
        s1 = s2
    ;
    if (root == lastroot)
        next
    ;
    lastroot = root

    # Get PNG locally for more efficient header access.
    copy (s1, ".", verbose-)
    s1 = substr (s1, strldx("/",s1)+1, 1000)
    s2 = s1//"[0]"

    # Set the PROPID so we can later separate by it.
    propid = ""
    hselect (s2, "DTPROPID,PROPID,PLPROPID", yes) | scan (propid)
    if (propid == "")
       propid = "unknown"
    ;

    # Create the HTML file and initialize the table.
    if (access(html)==NO) {
	printf ('<HTML><TITLE>%s</TITLE><BODY><CENTER><H1>%s</H1></CENTER>\n',
	    names.shortname, names.shortname, > html)
	printf ('\n<TABLE BORDER="1">\n', >> html)
    } else
	printf ('</TR>\n', >> html)

    # Create the proposal HTML file.
    prophtml = "../../logs/"// propid // ".html"
    if (access(prophtml) == NO)
        ndpprophdr (s2, > prophtml)
    ;
    ndppropseq (s2)

    # Start a row.  Put in any comments.
    printf ("<!-- PROPID %s -->\n", propid, >> html)
    printf ("<TR>\n", >> html)

    # Put summary information.
    printf ('<TD ALIGN="LEFT" VALIGN="CENTER">\n', >> html)
    ndpsummary (s2, >> html)
    printf ('</A></TD>\n', >> html)
    #printf ('<TD ALIGN="LEFT" VALIGN="CENTER">\n', >> html)
    #ndpsummary1 (s2, >> html)
    #printf ('</A></TD>\n', >> html)

    if (access("ndprev_raw.tmp")) {
        pngfits = ""
	match (root//"_raw_png.fits", "ndprev_raw.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, large-, >> html)
    }
    ;
    if (access("ndprev_press.tmp")) {
        pngfits = ""
	match (root//"_press_png.fits", "ndprev_press.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, large-, >> html)
    }
    ;
    if (access("ndprev_ss.tmp")) {
	pngfits = ""
	match (root//"_ss_png.fits", "ndprev_ss.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, >> html)
    }
    ;
    if (access("ndprev_rsp.tmp")) {
	pngfits = ""
	match (root//"_r_png.fits", "ndprev_rsp.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, large-, >> html)
    }
    ;
    if (access("ndprev_stk.tmp")) {
	pngfits = ""
	match (root//"_stk_png.fits", "ndprev_stk.tmp") | scan (pngfits)
	if (access("ndprev_spk.tmp"))
	    dpcol (pngfits, pngdir, propid, >> html)
	else
	    dpcol (pngfits, pngdir, propid, >> html)
    }
    ;
    if (access("ndprev_spk.tmp")) {
	pngfits = ""
	match (root//"_spk_png.fits", "ndprev_spk.tmp") | scan (pngfits)
	dpcol (pngfits, pngdir, propid, >> html)
    }
    ;

    # Finish row.
    printf ('</TD>\n', >> html)
}
list = ""
delete ("ndprev*.tmp")

# Finish table and HTML.
if (access(html)) {
    printf ('</TR>\n', >> html)
    printf ('\n</TABLE></BODY></HTML>\n', >> html)
}
;

# Finish prop table.
if (access(prophtml)) {
    cd ("../../observer")
    html = "../html/" // names.shortname // ".html"
    prophtml = "../logs/" // propid // ".html"
    ndppropseq (html, >> prophtml)
    if (access("index.html")) {
        readlink ("index.html") | scan (s1)
	if (s1 != prophtml)
	    ln (prophtml, "index.html")
        ;
    } else
	ln (prophtml, "index.html")
}
;

logout 1
