# NDPPROPSEQ -- Accumulate sequence data.
# This assumes that somethings cannot change within a sequence.

procedure ndppropseq (image)

file	image			{prompt="Image for header information"}

string	froot = "&nbsp;"
int	fnum1 = INDEF
int	fnum2 = INDEF
int	ncomb = INDEF
int	nexp = 0
real	exptot = 0
string	ut = "&nbsp;"
string	seqid = "&nbsp;"
string	type = "&nbsp;"
string	exptime = "&nbsp;"
string	filter = "&nbsp;"
real	focus1 = INDEF
real	focus2 = INDEF
string	dafs = "&nbsp;"
real	skybg1 = INDEF
real	skybg2 = INDEF
real	skymag1 = INDEF
real	skymag2 = INDEF
real	seeing1 = INDEF
real	seeing2 = INDEF
real	magz1 = INDEF
real	magz2 = INDEF
real	depth1 = INDEF
real	depth2 = INDEF
real	airmass1 = INDEF
real	airmass2 = INDEF
real	ra = 0
real	dec = 0
struct	title = "&nbsp;"

begin
	int	i
	file	im
	real	rval
	struct	sval

	# Tasks and packages.
	images
	utilities

	im = image

	i = strstr (".html", im)
	if (i == 0)
	    i = strstr (".hdr", im)

	if (i > 0) {
	    printf ('<TR>\n')
	    if (substr(im,i,1000) == ".hdr") {
	        sval = substr (im, 1, i-1) // "_x2.png"
		printf ('<TD><A HREF="%s">%s%d</A><BR>', im, froot, fnum1)
		printf ('<A HREF="%s">png</A></TD>\n', sval)
	    } else {
		printf ('<TD><A HREF="%s">%s', im, froot)
		if (!isindef(fnum1)) {
		    if (isindef(ncomb))
			printf ('%5d-%5d</A><BR>%d / %.0h</TD>\n',
			    fnum1, fnum2, nexp, exptot)
		    else
			printf ('%5dx%d</A><BR></TD>\n', fnum1, ncomb)
		}
	    }
	    printf ('<TD>%s</TD>\n', ut)
	    printf ('<TD>%s<BR>%s</TD>\n', seqid, type)
	    printf ('<TD>%s</TD>\n', exptime)
	    printf ('<TD>%s</TD>\n', filter)
	    if (isindef(focus1))
		printf ('<TD>&nbsp;</TD>\n')
	    else if (abs(focus1-focus2)<5)
		printf ('<TD>%d</TD>\n', focus1)
	    else
		printf ('<TD>%d-%d</TD>\n', focus1, focus2)
	    printf ('<TD>%s</TD>\n', dafs)

	    if (isindef(skybg1))
		printf ('<TD>&nbsp;<BR>')
	    else if (abs(skybg1-skybg2)<0.1)
		printf ('<TD>%.2f<BR>', skybg1)
	    else
		printf ('<TD>%.2f-%.2f<BR>', skybg1, skybg2)
	    if (isindef(skymag1))
		printf ('&nbsp;</TD>\n')
	    else if (abs(skymag1-skymag2)<0.01)
		printf ('%.2f</TD>\n', skymag1)
	    else
		printf ('%.2f-%.2f</TD>\n', skymag1, skymag2)

	    if (isindef(seeing1))
		printf ('<TD>&nbsp;</TD>\n')
	    else if (abs (seeing1-seeing2)<0.01)
		printf ('<TD>%.2f</TD>\n', seeing1)
	    else
		printf ('<TD>%.2f-%.2f</TD>\n', seeing1, seeing2)

	    if (isindef(magz1))
		printf ('<TD>&nbsp;<BR>')
	    else if (abs(magz1-magz2)<0.01)
		printf ('<TD>%.2f<BR>', magz1)
	    else
		printf ('<TD>%.2f-%.2f<BR>', magz1, magz2)
	    if (isindef(depth1))
		printf ('&nbsp;</TD>\n')
	    else if (abs(depth1-depth2)<0.01)
		printf ('%.2f</TD>\n', depth1)
	    else
		printf ('%.2f-%.2f</TD>\n', depth1, depth2)

	    if (isindef(airmass1))
		printf ('<TD>&nbsp;</TD>\n')
	    else if (abs(airmass1-airmass2)<0.05)
		printf ('<TD>%.2f</TD>\n', airmass1)
	    else
		printf ('<TD>%.2f-%.2f</TD>\n', airmass1, airmass2)

	    if (nexp == 0)
		printf ('<TD>INDEF<BR>INDEF</TD>\n')
	    else {
		ra /= nexp; dec /= nexp
		if (dec < 0.)
		    printf ('<TD>%08.0H<BR>-%08.0h</TD>\n', ra, abs(dec))
		else
		    printf ('<TD>%08.0H<BR>%08.0h</TD>\n', ra, dec)
	    }

	    printf ('<TD>%s</TD>\n', title)
	    printf ('</TR>\n')
	    return
	}

	nexp += 1
	hselect (im, "PLOFNAME", yes) | scan (sval)
	if (sval != "")
	    i = strldx ("_", sval)
	else {
	    hselect (im, "FILENAME", yes) | scan (sval)
	    i = strstr (".fits", sval)
	}
	if (sval != "") {
	    if (i > 6) {
		froot = substr (sval, 1, i-6)
		i = int (substr(sval,i-5,i-1))
		if (isindef(fnum1)) {
		    fnum1 = i
		    fnum2 = i
		} else {
		    fnum1 = min (fnum1, i)
		    fnum2 = max (fnum2, i)
		}
	    }
	    rval = 1
	    hselect (im, "$NCOMBINE", yes) | scan (rval)
	    ncomb = rval
	}
	if (ut == "&nbsp;") {
	    hselect (im, "DATE-OBS", yes) | scan (sval)
	    if (sval != "") {
		i = stridx ("T", sval)
		ut = substr(sval,1,i-1) // "<BR>" // substr(sval,i+1,1000)
	    }
	}
	hselect (im, "SEQID", yes) | scan (sval)
	if (sval != "" && (seqid == "&nbsp;" || sval < seqid))
	    seqid = sval
	if (type == "&nbsp;") {
	    hselect (im, "NOCDHS", yes) | scan (sval)
	    if (sval != "")
	        type = sval
	}
	rval=INDEF; hselect (im, "OEXPTIME", yes) | scan (rval)
	if (isindef(rval))
	    hselect (im, "EXPTIME", yes) | scan (rval)
	if (!isindef(rval))
	    exptot += rval / 3600.
	if (exptime == "&nbsp;") {
	    if (isindef(rval))
	        sval = ""
	    else
		printf ("%d\n", rval) | scan (sval)
	    if (sval != "")
		exptime = sval // "s<BR>"
	    hselect (im, "EXPCOADD", yes) | scan (sval)
	    if (sval != "")
		exptime += sval // "s"
	    hselect (im, "NCOADD", yes) | scan (sval)
	    if (sval != "")
		exptime += " x " // sval
	}
	if (filter == "&nbsp;") {
	    hselect (im, "FILTER", yes) | scan (sval)
	    if (sval != "")
	        filter = sval
	}
	rval=INDEF; hselect (im, "TELFOCUS", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(focus1)) {
		focus1 = rval
		focus2 = rval
	    } else {
		focus1 = min (focus1, rval)
		focus2 = max (focus2, rval)
	    }
	}
	if (dafs == "&nbsp;") {
	    hselect (im, "DIGAVGS", yes) | scan (sval)
	    dafs = sval
	    hselect (im, "FSAMPLE", yes) | scan (sval)
	    dafs += "<BR>" // sval
	}
	rval=INDEF; hselect (im, "SKYBG", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(skybg1)) {
		skybg1 = rval
		skybg2 = rval
	    } else {
		skybg1 = min (skybg1, rval)
		skybg2 = max (skybg2, rval)
	    }
	}
	rval=INDEF; hselect (im, "SKYMAG", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(skymag1)) {
		skymag1 = rval
		skymag2 = rval
	    } else {
		skymag1 = min (skymag1, rval)
		skymag2 = max (skymag2, rval)
	    }
	}
	rval=INDEF; hselect (im, "SEEING", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(seeing1)) {
		seeing1 = rval
		seeing2 = rval
	    } else {
		seeing1 = min (seeing1, rval)
		seeing2 = max (seeing2, rval)
	    }
	}
	rval=INDEF; hselect (im, "MAGZERO", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(magz1)) {
		magz1 = rval
		magz2 = rval
	    } else {
		magz1 = min (magz1, rval)
		magz2 = max (magz2, rval)
	    }
	}
	rval=INDEF; hselect (im, "PHOTDPTH", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(depth1)) {
		depth1 = rval
		depth2 = rval
	    } else {
		depth1 = min (depth1, rval)
		depth2 = max (depth2, rval)
	    }
	}
	rval=INDEF; hselect (im, "AIRMASS", yes) | scan (rval)
	if (!isindef(rval)) {
	    if (isindef(airmass1)) {
		airmass1 = rval
		airmass2 = rval
	    } else {
		airmass1 = min (airmass1, rval)
		airmass2 = max (airmass2, rval)
	    }
	}
	rval=INDEF; hselect (im, "CRVAL1", yes) | scan (rval)
	if (!isindef(rval))
	    ra += rval
	rval=INDEF; hselect (im, "CRVAL2", yes) | scan (rval)
	if (!isindef(rval))
	    dec += rval
	if (title == "&nbsp;") {
	    hselect (im, "TITLE", yes) | scan (sval)
	    if (sval != "")
	        title = sval
	}
end
