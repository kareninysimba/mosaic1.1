#!/bin/env pipecl
#
# NDPHDRLOG -- Make log from hdr files.

bool	bydate = YES

images
noao
artdata

task ndppropseq = "NHPPS_PIPESRC$/NEWFIRM/NDP/ndppropseq.cl"
cache ndppropseq

if (cl.args == "") {
    printf ('<HTML><TITLE>NOAO PIPELINE LOG</TITLE><BODY>\n')
    printf ('<CENTER><H2>NOAO PIPELINE LOG</H2></CENTER>\n')

    printf ('\n<P>\n<TABLE BORDER=1><TR>\n')
    printf ('<TH>Filename</TH>\n')
    printf ('<TH>UT Start</TH>\n')
    printf ('<TH>Type</TH>\n')
    printf ('<TH>ExpTime</TH>\n')
    printf ('<TH>Filter</TH>\n')
    printf ('<TH>Focus</TH>\n')
    printf ('<TH>DA<BR>FS</TH>\n')
    printf ('<TH>Sky ADU<BR>Sky Mag</TH>\n')
    printf ('<TH>FWHM<BR>arcsec</TH>\n')
    printf ('<TH>Mag zero<BR>Depth</TH>\n')
    printf ('<TH>Airmass</TH>\n')
    printf ('<TH>Title</TH>\n')
    printf ('</TR>\n')

    logout 1
}
;

list = cl.args
while (fscan (list, s1) != EOF) {
    unlearn ndppropseq
    mkpattern ("tmpim", ndim=0, header=s1)

    if (bydate) {
	s2 = ""; hselect ("tmpim", "DATE-OBS", yes) | scan (s2)
	if (s2 == "")
	    next
	;
	s2 = substr (s2, 1, 10) // ".html"
	if (access(s2) == NO) {
	    printf ('<HTML><TITLE>NOAO PIPELINE LOG</TITLE><BODY>\n', > s2)
	    printf ('<CENTER><H2>NOAO PIPELINE LOG</H2></CENTER>\n', >> s2)

	    printf ('\n<P>\n<TABLE BORDER=1><TR>\n', >> s2)
	    printf ('<TH>Filename</TH>\n', >> s2)
	    printf ('<TH>UT Start</TH>\n', >> s2)
	    printf ('<TH>Type</TH>\n', >> s2)
	    printf ('<TH>ExpTime</TH>\n', >> s2)
	    printf ('<TH>Filter</TH>\n', >> s2)
	    printf ('<TH>Focus</TH>\n', >> s2)
	    printf ('<TH>DA<BR>FS</TH>\n', >> s2)
	    printf ('<TH>Sky ADU<BR>Sky Mag</TH>\n', >> s2)
	    printf ('<TH>FWHM<BR>arcsec</TH>\n', >> s2)
	    printf ('<TH>Mag zero<BR>Depth</TH>\n', >> s2)
	    printf ('<TH>Airmass</TH>\n', >> s2)
	    printf ('<TH>Title</TH>\n', >> s2)
	    printf ('</TR>\n', >> s2)
	}
	;
	    
	ndppropseq ("tmpim")
	ndppropseq (s1, >> s2)
    } else {
	ndppropseq ("tmpim")
	ndppropseq (s1)
    }

    imdel ("tmpim")
}
list = ""

logout 1
