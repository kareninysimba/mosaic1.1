# DSPSUMMARY -- Make HTML summary info from an image header.
# This is an HTML fragment written to the STDOUT to be used by
# the calling program.  The image argument may be  wildcard but only
# the first image will be processed.

procedure dspsummary (image)

file	image				{prompt="Image"}

begin
	file	im
	int	ival, rejected
	real	rval
	string	strval
	struct	sval, obstype, proctype

	# Tasks and packages.
	images
	utilities

	im = image

	# Observation type and processing type.
	obstype = ""; hselect (im, "OBSTYPE", yes) | scan (obstype)
	proctype = ""; hselect (im, "PROCTYPE", yes) | scan (proctype)

	# Is this image rejected?
	sval=""; hselect( im, "REJECT", yes ) |
	    translit( "STDIN", '"', delete+) | scan( sval )
	if ( substr( sval, 1, 1 ) == "Y" )
	    printf( "<font color=\"red\">\n" )
        else 
	    printf( "<font color=\"green\">\n" )
	
	# Date of observation
	sval=""; hselect (im, "DATE-OBS", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	if (sval != "")
	    printf ("%s<BR>\n", sval)

	# Image Title
	sval=""; hselect (im, "TITLE", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	ival = strstr ("- DelRA",sval)
	if (ival > 0)
	    sval = substr (sval, 1, ival-1)
	printf ("%s<BR>\n", sval)

	# Original filename or number combined.
	ival=INDEF; hselect (im, "NCOMBINE", yes) | scan (ival)
	if (isindef(ival)) {
	    sval=""; hselect (im, "PLOFNAME", yes) |
		translit ("STDIN", '"', delete+) | scan (sval)
	    if (sval != "") {
	        ival = strstr ("_pl_png", sval)
		if (ival > 0)
		    sval = substr (sval, 1, ival-1)
		printf ("%s<BR>\n", sval)
	    }
	} else
	    printf ("NCOMBINE = %d<BR>\n", ival)

	if (proctype == "MasterCal") {
	    rval=INDEF; hselect (im, "QUALITY", yes) | scan (rval)
	    if (!isindef(rval)) {
	        printf ("QUALITY = %g<BR>\n", rval)
		if (rval < 0.)
		    printf ("<B>For Evaluation Only</B><BR>\n")
	    }
	}
	
	if (obstype == "zero")
	    return

	# Filter
	sval=""; hselect (im, "FILTER", yes) |
	    translit ("STDIN", '"', delete+) | scan (sval)
	if (sval != "")
	    printf ("%s<BR>\n", sval)

	if (proctype == "MasterCal")
	    return

	# Original exposure time.
	rval=INDEF; hselect (im, "OEXPTIME", yes) | scan (rval)
	if (isindef(rval)) {
	    rval=INDEF; hselect (im, "EXPTIME", yes) | scan (rval)
	}
	if (!isindef(rval))
	    printf ("OEXPTIME = %d<BR>\n", rval)

	# Proposal title
	sval=""; hselect (im, "DTTITLE", yes) | scan (sval)
	if (sval != "") {
	    if (strlen (sval) > 20)
		printf ("%.30s...<BR>\n", sval)
	    else
		printf ("%s<BR>\n", sval)
	}

	# Sky level
	rval=INDEF; hselect (im, "SKY", yes) |
	    translit ("STDIN", '"', delete+) | scan (rval)
	if (!isindef(rval))
	    printf ("SKY = %.3g<BR>\n", rval)

	# Airmass.
	rval=INDEF; hselect (im, "AIRMASS", yes) | scan (rval)
	if (!isindef(rval))
	    printf ("AIRMASS = %.2g<BR>\n", rval)

	# WCS and Photometry.
	sval="F"; hselect (im, "WCSCAL", yes) | scan (sval)
	if (sval == "T") { 
	    # Magnitude zero point
	    strval =""; hselect (im, "FILTER", yes) |
		translit ("STDIN", '"', delete+) | scan (strval)
	    rval=INDEF; hselect (im, "MAGZERO", yes) | scan (rval)
	    if (!isindef(rval))
		printf ("MAGZERO(%s) = %.2f<BR>\n", strval, rval)

	    # Magnitude depth.
	    rval=INDEF; hselect (im, "PHOTDPTH", yes) | scan (rval)
	    if (!isindef(rval))
		printf ("PHOTDPTH(%s) = %.2f<BR>\n", strval, rval)

	    # Seeing
	    rval=INDEF; hselect (im, "SEEING", yes) | scan (rval)
	    if (!isindef(rval))
		printf ("FWHM = %.2f<BR>\n", rval)
	} else {
	    hselect (im, "TELRA,TELDEC", yes) | scan (rval, rval)
	    if (nscan() < 2)
		printf ("<B>Telescope Coordinates Missing</B><BR>\n")
	    else
		printf ("<B>WCS/Phot Calibration Failed</B><BR>\n")
	}
	printf( "</font>\n" )
end
