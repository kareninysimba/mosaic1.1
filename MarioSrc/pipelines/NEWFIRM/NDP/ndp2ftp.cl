#!/bin/env pipecl
#
# DPS2FTP -- Move html files to ftp.

if (dts_ftp == "no")
    logout 1
;

string	dataset, dataset1, dataset2, datadir, ilist, olist, odir, lfile
string	propid, ftpdir, nvodir, s4
file	ihtml, html
struct	*fd, *fd1, line1

# Load tasks and packages.
proto
task $ftpdb = "$! echo ""$1"" | mysql -N -pMosaic_DHS $USER"
task $ltar = "$!(cd ..; tar -c `cat $2`) | (mkdir -p $3/Html; cd $3; cat > Html/$1.tar)"
task $rtar = "$!(cd ..; tar -c `cat $2`) | ssh $3 '(mkdir -p $4/Html; cd $4; cat > Html/$1.tar)'"
task $nvoln = "$!ssh $1 '(mkdir -p $2; cd $2; ln -s ../../../../pipeline/protected/$3/Html .)'"

# Set names and directories.
names (envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET"))
dataset = names.dataset
dataset1 = substr (dataset, 1, stridx("-",dataset)-1)
dataset1 = substr (dataset1, 1, strldx("_",dataset1)-1)
datadir = names.datadir
odir = names.rootdir // "output/"
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Log start of module.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Work in the output directory and determine PROPIDs and ftp directories.
# Exit if none are found.

cd (odir)
ilist = dataset // "_dps2ftp1.tmp"
olist = dataset // "_dps2ftp2.tmp"
ihtml = odir // "html/" // names.shortname // ".html"
if (access(ihtml)==NO)
    logout 1
;
if (access("Html")==NO)
    mkdir ("Html")
;
cd ("Html")
match ("PROPID", ihtml) | fields ("STDIN", 3) | sort (reverse+) | uniq (> "dps2ftp.tmp")

list = "dps2ftp.tmp"
while (fscan (list, propid) != EOF) {
    if (dts_ftp != "yes")
	s1 = dts_ftp
    else
	s1 = propid
    printf ("select ftp_dir, nvo_dir from FTP where proposal='%s';\n", s1) |
        scan (line)
    ftpdir = ""; nvodir = ""
    ftpdb (line) | scan (ftpdir, nvodir)
    if (ftpdir == "")
        sendmsg ("ERROR", "No ftp information", line, "PROC")
    ;
    printf ("  PROPID = '%s', ftp_dir = '%s'\n", propid, ftpdir) | tee (lfile)
    ftpdir += "/Reduced"
    nvodir += "/Reduced"
    printf ("%s %s %s\n", propid, ftpdir, nvodir, >> ilist)
}
list = ""; delete ("dps2ftp.tmp")

# Loop through PROPIDs.
list = ilist
while (fscan (list, propid, ftpdir, nvodir) != EOF) {
    dataset2 = names.shortname
    i = strldx ("_", dataset2)
    if (i > 0) {
	j = stridx ("-", dataset2)
	if (j == 0)
	    dataset2 = substr (dataset2, 1, i-1) // "Z"
	else
	    dataset2 = substr (dataset2, 1, i-1) // substr (dataset2, j, 1000)
    }
    ;
    if (propid != "")
	dataset2 = propid // "_" // dataset2
    ;
    html = dataset2 // ".html"
    print ("Html/"//html, > olist)

    fd = ihtml
    if (fscan (fd, line) == EOF)
        next
    ;
    printf ("<HTML><TITLE>%s</TITLE><BODY><CENTER><H1>%s</H1></CENTER>\n",
        dataset2, dataset2, > html)

    while (fscan (fd, line) != EOF) {
        i = strstr ("PROPID", line)
	if (i == 0) {
	    s2 = line
	    while (YES) {
		i = strstr ("SRC=", s2)
		if (i == 0)
		    break
		;
		s2 = substr (s2, i+5, 1000)
		i = stridx ('"', s2)
		s1 = substr (s2, 1, i-1)
		s2 = substr (s2, i+1, 1000)
		print (substr(s1,4,1000), >> olist)
	    }
	    s2 = line
	    while (YES) {
		i = strstr ("HREF=", s2)
		if (i == 0)
		    break
		;
		s2 = substr (s2, i+6, 1000)
		i = stridx ('"', s2)
		s1 = substr (s2, 1, i-1)
		s2 = substr (s2, i+1, 1000)
		print (substr(s1,4,1000), >> olist)

		if (strstr(".html",s1) > 0) {
		    s4 = substr (s1, 4, strldx("/",s1))
		    match ("SRC=", s1, > "dps2ftp3.tmp")
		    fd1 = "dps2ftp3.tmp"
		    while (fscan (fd1, line1) != EOF) {
			s3 = line1
			while (YES) {
			    i = strstr ("SRC=", s3)
			    if (i == 0)
				break
			    ;
			    s3 = substr (s3, i+5, 1000)
			    i = stridx ('"', s3)
			    s1 = substr (s3, 1, i-1)
			    s3 = substr (s3, i+1, 1000)
			    print (s4//s1, >> olist)
			}
		    }
		    fd1 = ""; delete ("dps2ftp3.tmp")
		}
	    }
	    print (line, >> html)
	    next
	}
	;
	i = fscan (line, s1, s1, s2)
	if (s2 != propid) {
	    while (fscan (fd, line) != EOF) {
	        if (line == "</TR>")
		    break
		;
	    }
	} else
	    print (line, >> html)
    }
    fd = ""

    # Copy to ftp directory if specified.
    if (ftpdir == "") {
        delete (olist)
	next
    }

    # Log operation
    printf ("  Make tar file for %s in %s\n", dataset2, ftpdir) | tee (lfile)

    # Make the tar file.
    move (olist, odir)
    i = strldx (":", ftpdir)
    j = strldx (":", nvodir)
    if (i == 0)
	ltar (dataset2, olist, ftpdir)
    else {
	rtar (dataset2, olist, substr(ftpdir,1,i-1), substr(ftpdir,i+1,1000))
	printf ("Set NVO directory: %s %s %s\n", substr(nvodir,1,j-1),
	    substr(nvodir,j+1,999), substr(ftpdir,i+1,999))
	nvoln (substr(nvodir,1,j-1), substr(nvodir,j+1,999),
	    substr(ftpdir,i+1,999))
    }

    delete (odir//olist)
}
list = ""

# Clean up.
delete (ilist)

logout 1
