#!/bin/env pipecl
#
# SDKPROC

int     status = 1
int     namps
string  datadir, dataset, ifile, image, indir, lfile, trimsec, cal, bpm

file    caldir = "MC$"

# Tasks and packages
images
mscred
proto

# Set file and path names.
sdknames (envget("OSF_DATASET"))
dataset = sdknames.dataset

# Set filenames.
indir = sdknames.indir
datadir = sdknames.datadir
ifile = indir // dataset // ".sdk"
lfile = datadir // sdknames.lfile
set (uparm = sdknames.uparm)
set (pipedata = sdknames.pipedata)
cal = sdknames.cal

# Log start of processing.
printf ("\nSDKCOMBINE (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# First check whether persistence may have affected the darks
# in the input list.
if (do_persistence) {
    imcombine( "@sdkmasks.list", "sdkmasks", offset="physical",
        combine="sum" )
    # If there are any pixels in the stack to which no darks contributed
    # valid data, then reject this stacked dark.
    imstat( "sdkmasks", upper=0, format-, fields="npix" ) | scan( i )
    if (i>0)
        sendmsg( "WARNING", "Found undefined pixels in dark", dataset, "VRFY" )
        #status = 0
    ;
    delete( "sdkmasks.list" )
    imdel( "sdkmasks" )
}
;

if ( status>0 ) {
    count (ifile) | scan (i)
    if (i > 3)
	imcombine( "@"//ifile, cal, offset="physical", reject="minmax", nlow=0 )
    else
	imcombine( "@"//ifile, cal, offset="physical", reject="none" )

    # Retrieve the BPM keyword from the first image in the list ...
    list = ifile
    i = fscan( list, image )
    list = ""
    hselect( image, "BPM", yes ) | scan( bpm )
    # ... and add it to the combined calibration 
    hedit( cal, "BPM", bpm, add+, ver- )

    # Interpolate over all bad pixels to produce cosmetically nice
    # images.
    fixpix( cal, "BPM" )
}
;

logout 1
