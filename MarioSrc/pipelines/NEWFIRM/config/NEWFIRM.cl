# MARIO.CL -- Pipeline configuration script.
# This may do any valid IRAF setup but it is mostly intended for defining
# global variables.  Note that these become package parameters.

bool    sciencepipeline = yes   {prompt="Science pipeline mode?"}
bool	do_cleanup = yes        {prompt="Clean up intermediate results?"}
bool	do_persistence = yes    {prompt="Apply persistence masks?"}
bool	do_saturation = yes	{prompt="Mask saturation?"}
string  lincoeftype = "linimage"  {prompt="Linearity coef type (linconst|linimage)"}
int     num_trigger = 2         {prompt="Number of pipelines to trigger"}
string  whenflatfield = "after" {prompt="Flatfield before|after sky subtraction?"}

bool	dir_stkgrp = yes	{prompt="Group sequences for stacking?"}

bool	sfl_nodarkcont = yes    {prompt="Continue if no dark available?"}
bool	sfl_nolincont = yes     {prompt="Continue if no linearity available?"}

bool	ogc_fixchannels = yes   {prompt="Fix noisy channels?"}
bool	ogc_nodarkcont = yes    {prompt="Continue if no dark available?"}
bool	ogc_nolincont = yes     {prompt="Continue if no linearity available?"}
bool	ogc_noflatcont = yes    {prompt="Continue if no dome flat available?"}

int     per_numexps = 5         {prompt="Number of exposures to mask persistence"}

int     sky_minrunmedwin = 4    {prompt="Min window size for running median"}
int     sky_maxrunmedwin = 9    {prompt="Max window size for running median"}
bool	sky_pairwiseskysub = no {prompt="Do pairwise sky subtraction?"}
real	sky_4qradius = 100      {prompt="4Q mask circle radius in percent"}
real    wcs_fracmatch = 0.5     {prompt="WCS matching fraction"}
bool	wcs_wcsglobal = no      {prompt="Do only global WCS update"}
real    gos_ditherstep = 1200   {prompt="Maximum dither step for grouping"}
bool    gos_byarray = no        {prompt="Stack data per array"}

string	psq_order = "asc"       {prompt="PSQ selection order"}
int     psq_limit = 999999      {prompt="PSQ data limit"}
string  ds_cache = ""   {prompt="Data service cache"}
string  psq_redo = "no"         {prompt="Redo previous processing?",enum="yes|no|cal|obj"}

string	cal_skyflat = "flat"	{prompt="Sky flat type (ignore|flat|object)"}
int     cal_nzero = 3           {prompt="Min zero combine for putcal"}
int     cal_nflat = 3           {prompt="Min flat combine for putcal"}
real	cal_mjdmin = -3         {prompt="Min MJD for putcal"}
real	cal_mjdmax = 3          {prompt="Max MJD for putcal"}

string  stk_reject = "badrmagzero,0.5"  {prompt="Type of STK rejection"}
string  stk_weight = "default"  {prompt="Type of STK weighting"}
string  spk_reject = "badrmagzero,0.5"  {prompt="Type of SPK rejection"}
string  spk_weight = "default"  {prompt="Type of SPK weighting"}
string	sti_trrej = "craverage"	{prompt="Type of CR rejection performed within image before 1-pass stacking (none|craverage)"}
string	spi_trrej = "acediff"	{prompt="Type of CR rejection performed during 2-pass stacking (none|acediff)"}

real	rsp_grid = 1            {prompt="Tangent point grid (deg)"}
real	rsp_scale = 0.4         {prompt="Resampling scale"}
string	rsp_interp = "lsinc17"  {prompt="Resampling interpolant"}
int     png_blk = 2             {prompt="PNG version block size"}
int     png_pix = 120           {prompt="PNG preview pixel size"}
int     verbose = 0             {prompt="Verbosity level"}

string	email = "operator"      {prompt="Address to send email"}
string	dps_archive = "no"      {prompt="Archive data products?"}
string	dts_ftp = "no"          {prompt="FTP data products (yes|no|proposal)"}
string  dts_save = ".dps MKD?*fits S[PT]K?*_s[pt]k?*fits log enids cat" {prompt="Pattern to save"}
string	all_erract = "return"   {prompt="Error action (none|return)"}

bool	mkd_press = yes		{prompt="Make pre-ss data product?"}
bool	mkd_ss = yes		{prompt="Make ss data product?"}
bool	mkd_ssrsp = yes		{prompt="Make ss resampled data product?"}
bool	mkd_stk = yes		{prompt="Make stacked data product?"}

keep
