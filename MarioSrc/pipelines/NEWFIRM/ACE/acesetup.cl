#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
string  dataset, indir, datadir, ifile, im, lfile, nfile

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".ace"
nfile = dataset // ".needed"

set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf( "\nACESETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

cd (datadir)

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head (ifile, nl=1) | scan (s1, s2)
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

logout( status )

