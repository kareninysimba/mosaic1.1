#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
string  datadir, ilist, indir, lfile, image, mask, dataset
string  sky, sig, obm, cat

# Load packages.
images
utilities
noao
nproto
nfextern
ace
dataqual

# Set file and path names.
names( envget("NHPPS_SUBPIPE_NAME"), envget("OSF_DATASET") )
dataset = names.dataset

# Set filenames.
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ace"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf( "\nACEDETECT (%s): ", dataset ) | tee (lfile)
time | tee( lfile )

cd (datadir)

# Read the exposure and its bad pixel mask from the input list
mask = ""
head (ilist, nl=1) | scan (image, mask)

# Create the output file names
s1 = substr( image, strldx("/",image)+1, strlstr(".fits",image)-1 )
sky = "a" // s1 // "_sky"
sig = "a" // s1 // "_sig"
obm = "a" // s1 // "_obm"
cat = "a" // s1 // "_cat"

imacedq (image, mask, "", sky, sig, obm, cat, lfile, "", "acedetect",
    convolve="bilinear 3 3", hsigma=4, minpix=6, ngrow=0, agrow=0.,
    catdefs="pipedata$swcace.def", magzero="!MAGZERO", verbose=2, mosaic-)

if (imacedq.status != 1)
    printf( "ACE failed on dataset %s\n", image )
else
    concat( lfile )

logout (imacedq.status)
