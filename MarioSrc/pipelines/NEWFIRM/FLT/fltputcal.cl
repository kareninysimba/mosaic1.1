#!/bin/env pipecl
#
# MTDSTATS

file	caldir = "MC$"
int	status = 1
real	quality = 1.0
real	mjd, mjdstart, mjdend, x1, x2, namp, nflat, dqglmafs, dqglmmfs
string	datadir, dataset, lfile, obstype

# Load the packages.
images
lists
servers
proto

# Set the dataset name and uparm.
names( "flt", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)

# Log the operation.
printf( "\nDRKPUTCAL (%s):\n", dataset )
print( datadir )
# Go to the relevant directory
cd( datadir )

# Get the list of flat images by selecting images with the
# string -imX.fits in the return files from the SFL pipeline.
# Note that the original flat files have _imX.fits. The on and
# off flats are not selected because these end in
# -im[0-9][on|off].fits. Also, require that all SFL return 
# successfully; this is done by ensuring that each of the 
# files in the return file has the expected contents.
list = dataset//".sfl"; touch( "putcal.lst" )
while ( fscan( list, s1 ) != EOF ) {
    s2 = ""
    match( "-im[0-9]*.fits", s1 ) | scan( s2 )
    if (s2=="") {
        # This file does not have the expected contents, therefore
        # not all data are returned, a complete flat cannot
        # be put in the database, and hence this set is rejected,
        # which is accomplished by deleting putcal.list
	delete( "putcal.lst" )
	touch( "putcal.lst" )
        break
    } else {
        # This file does have the expected contents, so add this
        # file to putcal.lst
        print( s2, >> "putcal.lst" )
    }
}
list = ""

# Get the first file from the list
s1 = ''; head ("putcal.lst", nl=1) | scan (s1)
if (s1 == "") { # Error if there are no files
    sendmsg( "WARNING", "No calibration file to put", "", "CAL" )
    logout 2
} else {
    hselect( s1, "MJD-OBS,OBSTYPE", yes ) |
	scan( mjd, obstype )
    mjdstart = mjd + cal_mjdmin
    mjdend = mjd + cal_mjdmax
    nflat = 1; hselect( s1, "NCOMBINE", yes ) | scan( nflat )
}

# Normalize the flat field across all extensions, and store
# the normalization factor in the headers
# First loop over all extensions to determine the mean for 
# each, and also determine the mean of dqavfsat and dqmxfsat
list = "putcal.lst"
x2 = 0
dqglmafs = 0
dqglmmfs = 0
namp = 0
while ( fscan( list, s1 ) != EOF ) {

    getcal (s1, "bpm", cm, caldir,
        obstype="!obstype", detector="!instrume", imageid="!detector",
        filter="!filter", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
        sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
            "CAL")
        status = 0
        break
    }
    ;
    mimstat( s1, imasks="!BPM", fields="mean", nclip=3, lsigma=3, usigma=3,
        format- ) | scan( x1 )
    x2 = x2+x1
    namp = namp+1

    hsel( s1, "DQAVFSAT", yes ) | scan( x )
    if (x>dqglmafs)
        dqglmafs = x
    ;
    hsel( s1, "DQMXFSAT", yes ) | scan( x )
    if (x>dqglmmfs)
        dqglmmfs = x
    ;

}
list = ""

if (dqglmmfs>0.01) {
    sendmsg( "WARNING", "Fraction of saturated pixels exceeds 1%", "", "CAL" )
    status = 2
#    logout (status)
    quality -= dqglmmfs
}

# Now determine the average scale factor
x1 = x2/namp
# Finally, apply the average scale factor to all extensions,
# and also store it in the header for all extensions
list = "putcal.lst"
while ( fscan( list, s1 ) != EOF ) {
    imarith( s1, "/", x1, s1 )
    hedit( s1, "FLATNORM", x1, add+, ver- )
}

# Put the calibration files in the Data Manager.
if (nflat < cal_nflat) {
    sendmsg( "WARNING", "Too few dome flat exposures",
	str(i) // " - not put in DM", "CAL" )
    status = 2
} else {
    hedit ("@putcal.lst", "PROCTYPE", "MasterCal", add+, ver-)
    hedit ("@putcal.lst", "QUALITY", quality, add+, ver-)
    putcal( "@putcal.lst", "image", dm=cm, class="dflat", detector="!instrume",
	imageid="!detector", filter="!filter", exptime="", mjdstart=mjdstart,
	mjdend=mjdend, quality=quality ) | scan( i, line )
    if (i != 0)
	status = 0
    ;
}

delete ("putcal.lst")
if (status == 0) {
    sendmsg ("ERROR", "Putcal failed", line, "CAL")
    logout 1
} else
    logout (status)
