#!/bin/env pipecl
# 
# MKDsetup
#
# Description:
#
#   This modules sets up the list files for the MKD pipeline. 
#
# Exit Status Values:
#
#   1 = Successful, input data represent an object
#   2 = Successful, input data represent a dark or flat
#   3 = Unsuccessful, no data found
#   4 = Unsuccessful, getcal failed to retrieve bpm
#
# History
#
#   T. Huard 20090302   Created
#   T. Huard 20090313   Moved "calledby" to mkdnames.cl, and ensured that the
#               format of the file conformed to convention.
#   T. Huard 20090325   Restructured code somewhat.  Added code to construct
#               lists of sky-subtracted non-resampled SIFs.
#   Last Revised:   T. Huard    20090326    6:45AM

# Declare some variables
int     status=1
string  dataset,datadir,ifile,lfile,rfile,uparmS
string  calledby,bpmCHECK
string  ilist2,lst1,lst1bpm,lst2,lst2bpm,lst3,lst3bpm,lst4,lst4bpm
struct  statusstr1
file    temp1
file    caldir="MC$"

# Load tasks and packages
images
servers
proto
noao
artdata
fitsutil
mscred
task $cp = "$!cp -r $(1) $(2)" # necessary for "setdirs" statement
task $ln = "$!ln -s $(1) $(2)" # necessary for "setdirs" statement

# Set names and directories.
mkdnames(envget("OSF_DATASET"))
dataset=mkdnames.dataset
datadir=mkdnames.datadir
ifile=mkdnames.indir//dataset//".mkd"
lfile=mkdnames.lfile
rfile=mkdnames.indir//"return/"//dataset//".mkd"
calledby=mkdnames.calledby
uparmS=mkdnames.uparm
set(uparm=mkdnames.uparm)
set(pipedata=mkdnames.pipedata)
cd(datadir)

# Log start of module.
printf ("\n%s (%s): ", envget ("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)
print(dataset//" dataset is "//calledby)

# Depending on which pipeline called the NDP/MKD pipeline, the ifile is
# searched for the appropriate SIFs to put together into images (composite
# images and MEFs) representing the processed version of original MEFs.  In
# the case of darks or flats, the SIFs collected are not resampled (and
# obviously not sky-subtracted).  In the case of an object, the resampled,
# sky-subtracted SIFs are collected to combine into a composite image, the
# non-resampled pre-sky-subtracted SIFs are collected to combine into an MEF,
# and the non-resampled sky-subtracted SIFs are collected to combine into
# an MEF.  If the second-pass sky subtraction has been run
# (with or without the transient removal), the resampled (and non-resampled)
# SIFs correspond to the *ss2_rsp.fits (and *ss2.fits) files from the SPI
# pipeline.  If the second-pass sky subtraction was not run, then no such
# files will be found, and the *ss_rsp.fits (and *ss.fits) first-pass files
# from the STK pipeline should be used for the sky-subtracted resampled (and
# non-resampled) SIFs.  If no SIFs are found, then an error message is written
# to the log and the module is exited with status of 3.
#
# For an object, the SIF may not make it into the SPI pipeline due to a WCS
# problem or because it was rejected by a transparency/seeing threshold.
# In these cases, we expected that all four SIFs will not make it into SPI.
# However, it is conceivable that a SIF will not make it to SPI, but the
# other SIFs of the original MEF do make it to SPI.  Specifically, this could
# happen in the case of seeing thresholding since one of the quadrants may
# show more distortion than the others and the exposure may already be
# marginally passable by the seeing threshold.
#
# Normally, when the pipeline is run in production, products will be
# constructed from second-pass files.  Products created from first-pass files
# will be distinguished from those created from second-pass files.
if (access("mkdsetup-raw.list")) delete("mkdsetup-raw.list",verify-)
;
if (access("mkdsetup-press.list")) delete("mkdsetup-press.list",verify-)
;
if (access("mkdsetup-ssrsp.list")) delete("mkdsetup-ssrsp.list",verify-)
;
if (access("mkdsetup-ss1rsp.list")) delete("mkdsetup-ss1rsp.list",verify-)
;
if ((calledby == "dark") || (calledby == "flat")) {
    # For calibrations, construct a list of pre-sky-subtracted (not resampled)
    # SIF filenames.
    status=2
    lst1="mkdsetup-press.list"
    lst1bpm="mkdsetup-pressBPM.list"
    match("-im[0-9].fits",ifile,>lst1)
} else {
    # For an object, construct a list of sky-subtracted resampled SIF 
    # filenames.
    lst1="mkdsetup-ssrsp.list"
    lst1bpm="mkdsetup-ssrspBPM.list"
    lst2="mkdsetup-press.list"
    lst2bpm="mkdsetup-pressBPM.list"
    match("SPI?*im[0-9]*_ss2_rsp.fits",ifile,>lst1)
    count(lst1) | scan(i)
    if (i == 0) {
        lst1="mkdsetup-ss1rsp.list"
        lst1bpm="mkdsetup-ss1rspBPM.list"
        lst2="mkdsetup-press1.list"
        lst2bpm="mkdsetup-press1BPM.list"
        delete(lst1,verify-)
        match("im[0-9]*_ss_rsp.fits",ifile,>lst1)
    }
    ;
    # For an object, construct also a list of pre-sky-subtracted (not
    # resampled) SIF filenames associated with the sky-subtracted, resampled
    # SIF filenames.  Currently, to do this requires use of the return file
    # to track down these pre-sky-subtracted SIF filenames.  The NEWFIRM
    # pipeline should later be revised so that the necessary information is
    # passed on to ilist.
    fields(rfile,"1",lines="1") | scan(ilist2)
    ilist2=substr(ilist2,1,strlstr("-mkd",ilist2)-1)//".ndp"
    list=lst1
    touch( lst2 )
    while (fscan(list,s1) != EOF) {
        s2=substr(s1,strlstr("/",s1)+1,strlstr("_ss",s1)-1)//".fits"
        match("NEWFIRM_OGC?*"//s2,ilist2,>>lst2)
    }
    list=""
    # Include a check that the number of sky-subtracted, resampled SIFs is the
    # same as the number of pre-sky-subtracted SIFs found.
    count(lst1) | scan(i)
    count(lst2) | scan(j)
    if (i != j) {
        sendmsg("ERROR","Number of pre-SS and SS SIFs not the same!",
            dataset,"PROC")
    }
    ;
    # For an object, construct also a list of sky-subtracted (not resampled)
    # SIF filenames.  Instead of matching for these here
    # and later sorting, perhaps it's better to use the sky-subtracted,
    # resampled list after sorting to generate this list?  I will keep
    # the code for generating the list in this way, for now, for consistency
    # with previous matching.
    # *** NOTE THAT "SPI" MUST BE INCLUDED IN THE MATCH SINCE THE SAME
    # *** FILENAMES RESULTING FROM SPS ARE INCLUDED IN ifile
    lst3="mkdsetup-ss.list"
    lst3bpm="mkdsetup-ssBPM.list"
    touch( lst3 )
    match("SPI?*im[0-9]*_ss2.fits",ifile,>lst3)
    count(lst3) | scan(i)
    if (i == 0) {
        lst3="mkdsetup-ss1.list"
        lst3bpm="mkdsetup-ss1BPM.list"
        delete(lst3,verify-)
        match("im[0-9]*_ss.fits",ifile,>lst3)
    }
    ;
    # *** Include sanity check here too? ***

    # For an object, construct also a list of raw SIF filenames.
    lst4="mkdsetup-raw.list"
    lst4bpm="mkdsetup-rawBPM.list"
    touch( lst4 )
    match("OGC?*im[0-9]*_raw.fits",ifile,>lst4)
    # *** Include sanity check here too? ***
}
# Check that some data were found.
count (lst1) | scan (i)
if (i == 0) {
    delete (lst1)
    sendmsg ("ERROR", "No data found", "", "VRFY")
    status=3
    logout(status)
}
;

# Record in log file the filenames of SIFs to combine, before sorting.
if (calledby == "object") {
    print("")
    print("===== SKY-SUB RESAMP SIFs TO COMBINE (PRESORTED) =====")
    type(lst1)
    print("======================================================")
    print("")
    print("")
    print("===== PRE-SKY-SUB NONRESAMP SIFs TO COMBINE (PRESORTED) =====")
    type(lst2)
    print("=============================================================")
    print("")
    print("")
    print("===== SKY-SUB NONRESAMP SIFs TO COMBINE (PRESORTED) =====")
    type(lst3)
    print("=========================================================")
    print("")
    print("")
    print("===== RAW SIFs TO COMBINE (PRESORTED) =====")
    type(lst4)
    print("=========================================================")
    print("")
} else {
    print("")
    print("===== PRE-SKY-SUB NONRESAMP SIFs TO COMBINE (PRESORTED) =====")
    type(lst1)
    print("=============================================================")
    print("")
}

# Setup UPARM and PIPEDATA
delete(substr(uparmS,strlstr("/",uparmS)+1,strlen(uparmS)-1),verify-)
s1 = ""; head (lst1) | scan (s1)
iferr {
    setdirs(s1)
} then {;d
    logout 0
} else
    ;

# Sort SIFs by extension in each list.  There is really no need to do this
# here, but I do it anyway since it was in the original MKD modules (and just
# in case these files are listed more than once in the ilist).
list=lst1
while (fscan(list, s1) != EOF) {
    s2=substr(s1,strlstr("im",s1),strlstr("im",s1)+2)
    printf ("%s %s\n", s2, s1, >> "mkdsetup1.tmp")
}
list=""
delete(lst1,verify-)
sort("mkdsetup1.tmp") | uniq (>"mkdsetup2.tmp")
fields("mkdsetup2.tmp",2,lines="1-",>lst1)

if (calledby == "object") {
    list=lst2; touch ("mkdsetup3.tmp")
    while (fscan(list,s1) != EOF) {
        s2=substr(s1,strlstr("im",s1),strlstr("im",s1)+2)
        printf ("%s %s\n", s2, s1, >> "mkdsetup3.tmp")
    }
    list=""
    delete(lst2,verify-)
    sort("mkdsetup3.tmp") | uniq (>"mkdsetup4.tmp")
    fields("mkdsetup4.tmp",2,lines="1-",>lst2)

    list=lst3; touch ("mkdsetup5.tmp")
    while (fscan(list,s1) != EOF) {
        s2=substr(s1,strlstr("im",s1),strlstr("im",s1)+2)
        printf ("%s %s\n", s2, s1, >> "mkdsetup5.tmp")
    }
    list=""
    delete(lst3,verify-)
    sort("mkdsetup5.tmp") | uniq (>"mkdsetup6.tmp")
    fields("mkdsetup6.tmp",2,lines="1-",>lst3)

    list=lst4; touch ("mkdsetup7.tmp")
    while (fscan(list,s1) != EOF) {
        s2=substr(s1,strlstr("im",s1),strlstr("im",s1)+2)
        printf ("%s %s\n", s2, s1, >> "mkdsetup7.tmp")
    }
    list=""
    delete(lst4,verify-)
    sort("mkdsetup7.tmp") | uniq (>"mkdsetup8.tmp")
    fields("mkdsetup8.tmp",2,lines="1-",>lst4)
    
    delete("mkdsetup3.tmp",verify-)
    delete("mkdsetup4.tmp",verify-)
    delete("mkdsetup5.tmp",verify-)
    delete("mkdsetup6.tmp",verify-)
    delete("mkdsetup7.tmp",verify-)
    delete("mkdsetup8.tmp",verify-)
}
;
delete("mkdsetup1.tmp",verify-)
delete("mkdsetup2.tmp",verify-)

# Record in log file the filenames of SIFs to combine, before sorting.
if (calledby == "object") {
    print("")
    print("===== SKY-SUB RESAMP SIFs TO COMBINE (SORTED) =====")
    type(lst1)
    print("===================================================")
    print("")
    print("")
    print("===== PRE-SKY-SUB NONRESAMP SIFs TO COMBINE (SORTED) =====")
    type(lst2)
    print("==========================================================")
    print("")
    print("")
    print("===== SKY-SUB NONRESAMP SIFs TO COMBINE (SORTED) =====")
    type(lst3)
    print("======================================================")
    print("")
    print("")
    print("===== RAW SIFs TO COMBINE (SORTED) =====")
    type(lst4)
    print("======================================================")
    print("")
} else {
    print("")
    print("===== PRE-SKY-SUB NONRESAMP SIFs TO COMBINE (SORTED) =====")
    type(lst1)
    print("==========================================================")
    print("")
}

# At this point, for datasets that represent objects:
#
#   lst1 (mkdsetup-ssrsp.list or mkdsetup-ss1rsp.list) is the list of
#   sky-subtracted resampled SIF filenames, ordered by extension, to be
#   combined into a composite image
#
#   lst2 (mkdsetup-press.list) is the list of pre-sky-subtracted
#   non-resampled SIF filenames, ordered by extension, to be combined into
#   an MEF
#
#   lst3 (mkdsetup-ss.list or mkdsetup-ss1.list) is the list of
#   sky-subtracted non-resampled SIF filenames, ordered by extension, to
#   be combined into an MEF
#
#   lst4 (mkdsetup-raw.list) is the list of raw SIF filenames, ordered
#   by extension, to be combined into an MEF
#
# At this point, for datasets that represent darks or flats:
#
#   lst1 (mkdsetup-press.list) is the list of pre-sky-subtracted
#   non-resampled SIF filenames, ordered by extension, to be combined
#   into an MEF
#

# Construct list(s) of BPMs (lst1bpm; also lst2bpm and lst3bpm if object).
if (calledby == "object") {
    # For sky-subtracted, resampled SIFs, the list of BPMs is constructed from
    # the full SIF pathnames.
    list=lst1
    while (fscan(list,s1) != EOF) {
        bpm=substr(s1,1,strlstr(".fits",s1)-1)//"_bpm.pl"
        printf("%s\n",bpm,>>lst1bpm)
        # As a check, I compare the BPM filename derived from the
        # SIF filename with that in the SIF header to be sure they are equal.
        hselect(s1,"BPM",yes) | scan(bpmCHECK)
        if (bpmCHECK != substr(bpm,strlstr("/",bpm)+1,999)) {
            print("WARNING: bpm not consistent with BPM from header") |
            tee(lfile)
            print("       image: "//s1) | tee(lfile)
            print("       bpm(ilist): "//bpm) | tee(lfile)
            print("       bpm(header): "//bpmCHECK) | tee(lfile)
        }
        ;
    }
    list=""
    # For pre-sky-subtracted non-resampled SIFs, the list of BPMs is
    # derived from getcals.
    list=lst2
    while (fscan(list,s1) != EOF) {
        hselect(s1,"OBSID",yes) | scan(s2)
        getcal(s1,"masks",cm,caldir,imageid="!detector",mjd="!mjd-obs",
            match="%"//s2,obstype="",detector="",filter="") | scan(i,statusstr)
        if (i != 0) {
            sendmsg("ERROR","Getcal failed for bpm",str(i)//" "//statusstr,
                "CAL")
            status=4
            break
        } else {
            bpm=getcal.value
            printf("%s\n",bpm,>>lst2bpm)
        }
        hselect(s1,"BPM",yes) | scan(bpmCHECK)
        if (bpmCHECK != substr(bpm,strlstr("/",bpm)+1,999)) {
            print("WARNING: bpm not consistent with BPM from header") |
            tee(lfile)
            print("       image: "//s1) | tee(lfile)
            print("       bpm(getcal): "//bpm) | tee(lfile)
            print("       bpm(header): "//bpmCHECK) | tee(lfile)
        }
        ;
    }
    list=""
    # For sky-subtracted non-resampled SIFs, the list of BPMs is
    # derived from getcals.
    list=lst3
    while (fscan(list,s1) != EOF) {
        hselect(s1,"OBSID",yes) | scan(s2)
        getcal(s1,"masks",cm,caldir,imageid="!detector",mjd="!mjd-obs",
            match="%"//s2,obstype="",detector="",filter="") | scan(i,statusstr)
        if (i != 0) {
            sendmsg("ERROR","Getcal failed for bpm",str(i)//" "//statusstr,
                "CAL")
            status=4
            break
        } else {
            bpm=getcal.value
            printf("%s\n",bpm,>>lst3bpm)
        }
        hselect(s1,"BPM",yes) | scan(bpmCHECK)
        if (bpmCHECK != substr(bpm,strlstr("/",bpm)+1,999)) {
            print("WARNING: bpm not consistent with BPM from header") |
            tee(lfile)
            print("       image: "//s1) | tee(lfile)
            print("       bpm(getcal): "//bpm) | tee(lfile)
            print("       bpm(header): "//bpmCHECK) | tee(lfile)
        }
        ;
    }
    list=""    
    # For raw SIFs, the list of BPMs is derived from getcals.
    list=lst4
    while (fscan(list,s1) != EOF) {
        hselect(s1,"OBSID",yes) | scan(s2)
        getcal(s1,"masks",cm,caldir,imageid="!detector",mjd="!mjd-obs",
            match="%"//s2,obstype="",detector="",filter="") | scan(i,statusstr)
        if (i != 0) {
            sendmsg("ERROR","Getcal failed for bpm",str(i)//" "//statusstr,
                "CAL")
            status=4
            break
        } else {
            bpm=getcal.value
            printf("%s\n",bpm,>>lst4bpm)
        }
        hselect(s1,"BPM",yes) | scan(bpmCHECK)
        if (bpmCHECK != substr(bpm,strlstr("/",bpm)+1,999)) {
            print("WARNING: bpm not consistent with BPM from header") |
            tee(lfile)
            print("       image: "//s1) | tee(lfile)
            print("       bpm(getcal): "//bpm) | tee(lfile)
            print("       bpm(header): "//bpmCHECK) | tee(lfile)
        }
        ;
    }
    list=""    
} else {
    # For darks and flats, the list of BPMs is derived from getcals.
    list=lst1
    while (fscan(list,s1) != EOF) {
        getcal(s1,"bpm",cm,caldir,obstype="!obstype",detector="!instrume",
            imageid="!detector",filter="!filter",exptime="",mjd="!mjd-obs") |
            scan(i,statusstr1)
        if (i != 0) {
            sendmsg("ERROR","Getcal failed for bpm",str(i)//" "//statusstr,
                "CAL")
            status=4
            break
        } else {
            bpm=getcal.value
            printf("%s\n",bpm,>>lst1bpm)
        }
        hselect(s1,"BPM",yes) | scan(bpmCHECK)
        if (bpmCHECK != substr(bpm,strlstr("/",bpm)+1,999)) {
            print("ERROR: bpm not consistent with BPM from header") | tee(lfile)
            print("       image: "//s1) | tee(lfile)
            print("       bpm(getcal): "//bpm) | tee(lfile)
            print("       bpm(header): "//bpmCHECK) | tee(lfile)
         }
        ;
    }
}

logout(status)
