#!/bin/env pipecl
#
# DRKSETUP

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $psqlog = "$!psqlog"
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( "drk", envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ifile = indir // dataset // ".drk"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Record in PSQ.
psqlog (names.shortname, "submitted")

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1//"[0]" )
} then {
    logout 0
} else
    ;

# Write the list of ENIDs.
list = ifile
while (fscan (list, s1) != EOF)
    hselect (s1//"[0]", "ENID*", yes) | words (>> dataset//".enids")
list = ""
concat (dataset//".enids")

logout( status )
