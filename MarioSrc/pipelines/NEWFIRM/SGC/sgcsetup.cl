#!/bin/env pipecl
#
# SGCsetup
#
# History
#
# R. Swaters, F. Valdes? --------	Created.
# T. Huard  20090318    Added code to check sgc_lincoeftype (in NEWFIRM.cl) to
#               prepare to use constant linearity coefficients or 2D
#               linearity coefficients.  If sgc_lincoeftype="linconst",
#               then the constant coefficients are used and the value
#               of LINCONST keyword is written to the LINCOEFF keyword.
#               If sgc_lincoeftype="linimage", then the 2D coefficients
#               are used and the value of LINIMAGE keyword is written
#               to LINCOEFF.  It is *IMPORTANT* that LINCOEFF has the
#               appropriate value because that keyword is read in 
#               sgcproc.cl to help identify saturated pixels.  It
#               is possible to rework sgcproc.cl so that it does not
#               rely on LINCOEFF, but that could be done at a later
#               date, if desired.  Documentation revised.  Ensured
#               that format of code conforms to convention.
# T. Huard  20090326    Changed sgc_lincoeftype to lincoeftype.
# Last Revised: T. Huard    20090326    9:30AM

int     status = 1
struct  statusstr, mask_name
string  dataset, indir, datadir, ifile, lfile, host
real	dexptime

string	subdir
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
sgcnames (envget ("OSF_DATASET"))
dataset = sgcnames.dataset
datadir = sgcnames.datadir
indir   = sgcnames.indir
ifile   = indir // dataset // ".sgc"
lfile   = datadir // sgcnames.lfile

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == NO)
    caldir = caldir // subdir

set (uparm = sgcnames.uparm)
set (pipedata = sgcnames.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nSGCSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Setup uparm and pipedata
delete( substr( sgcnames.uparm, 1, strlen(sgcnames.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# Check type of linearization to be done.  ltype="linconst" for constant
# linearity coefficients, while ltype="linimage" for 2D linearity coefficients.
if ((lincoeftype != "linconst") && (lincoeftype != "linimage")) {
    s2="lincoeftype: "//lincoeftype
    sendmsg("WARNING",s2,dataset,"VRFY")
    s2="Defaulting to constant linearity coeff"
    sendmsg("WARNING",s2,dataset,"VRFY")
    lincoeftype="linconst"
} else {
    if (lincoeftype == "linconst") {
        printf("\nUsing constant linearity coefficient.\n") | tee(lfile)
    } else {
        printf("\nUsing 2D linearity coefficient.\n") | tee(lfile)
    }
}

# Get the calibrations (i.e., darks and flats) from the calibration database
list = ifile
while (fscan (list, s1) != EOF) {
    getcal (s1, "bpm", cm, caldir,
	obstype="", detector="!instrume", imageid="!detector",
	filter="", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
	sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
	    "CAL")
	status = 0
	break
    }
    ;

    # Try to find a dark with similar exposure time
    dexptime = 0.05
    getcal( s1, "dark", cm, caldir,
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="!exptime", dexptime=dexptime,
	mjd="!mjd-obs" ) | scan( i, statusstr )
    # If none were found, and sgc_nodarkcont is set to YES, then
    # try again with a larger dexptime.  The value for 
    # sgc_nodarkcont is set in NEWFIRM.cl
    if ( (i != 0) && (sgc_nodarkcont == YES) ) {
#        dexptime = 1.0
        dexptime = 10000.0
        getcal( s1, "dark", cm, caldir,
            obstype="", detector="!instrume", imageid="!detector",
            filter="", exptime="!exptime", dexptime=dexptime,
	    mjd="!mjd-obs" ) | scan( i, statusstr )
    }
    ;
    if (i != 0) {
        if (sgc_nodarkcont == NO) {
            sendmsg( "ERROR", "Getcal failed for dark", s1, "CAL" )
            status = 0
            break
        }
    }
    ;

    # Try to retrieve the required flat from the database
    getcal( s1, "dflat", cm, caldir,
        obstype="object", detector="!instrume", imageid="!detector",
        filter="!filter", exptime="", mjd="!mjd-obs" ) |
            scan( i, statusstr )

    if (i != 0) {
	if (sgc_noflatcont == NO) { # value set in NEWFIRM.cl
	    # No dome flat available, not OK to continue, so throw error
            sendmsg( "ERROR", "Getcal failed for flat",
	        str(i)//" "//statusstr, "CAL" )
            status = 0
            break
	}
    }
    ;

    # Check whether the getcal works for retrieving linearity coefficient
    getcal(s1,lincoeftype,cm,caldir,obstype="",detector="!instrume",
        imageid="!detector",filter="",exptime="",mjd="!mjd-obs")
    if (getcal.statcode>0) {
        # Getcal was not successful
        s2="Getcal failed - no linearity coefficient retrieved"
        sendmsg("ERROR",s2,s1,"CAL")
        status=0
        break
    } else {
        # Getcal was successful
        # If constant linearity coefficient is used, write value of LINCONST
        # keyword to LINCOEFF keyword.  If 2D linearity coefficient is used,
        # write value of LINIMAGE keyword to LINCOEFF keyword.
        # It is *IMPORTANT* that LINCOEFF has the appropriate value because
        # that keyword is read in sflproc.cl to help identify saturated
        # pixels.
        s2=""; hselect(s1,lincoeftype,yes) | scan(s2)
        if (s2 != "") {     
            hedit(s1,"LINCOEFF",s2,add+,addonly-,delete-,verify-,show+,update+)
         } else {
            s2=lincoeftype//" keyword not found"
            sendmsg("ERROR",s2,s1,"CAL")
            status=0
            break
        }
    }
    
}
list = ""

logout( status )
