# MKPSQSQL -- Convert getrun data into sql data.

procedure mkpsqsql (queue)

string	queue
string	instrument = "NEWFIRM"
string	query = "dtinstru=''newfirm''"

struct	*fd

begin
	string	q, d, a, b

	q = queue

#	printf ("DROP TABLE IF EXISTS %s;\n", q)
#	printf ("CREATE TABLE IF NOT EXISTS %s (\n", q)
#	printf ("    dataset     char(32),\n")
#	printf ("    priority    int             DEFAULT 1,\n")
#	printf ("    status      char(16)        DEFAULT 'pending',\n")
#	printf ("    submitted   char(16),\n")
#	printf ("    completed   char(16),\n")
#	printf ("    PRIMARY KEY (dataset)\n")
#	printf ("    );\n\n")
#
#	printf ("#DROP TABLE IF EXISTS %sD;\n", q)
#	printf ("CREATE TABLE IF NOT EXISTS %sD (\n", q)
#	printf ("    name        char(32),\n")
#	printf ("    start       int,\n")
#	printf ("    end         int,\n")
#	printf ("    subquery    varchar(512),\n")
#	printf ("    PRIMARY KEY (name)\n")
#	printf ("    );\n\n")

	printf ("REPLACE INTO PSQ VALUES('%s', '%s', '%sD', '%s',\n",
	    q, q, q, instrument)
	printf ("'dir', 'disabled', '%s');\n\n", query)
	
	fd = q//".Q"
	while (fscan (fd, d) != EOF)
	    printf ("REPLACE INTO %s (dataset) VALUES ('%s');\n", q, d)
	fd = ""
	printf ("\n")

	fd = q//".QD"
	while (fscan (fd, d, a, b) != EOF) {
	    printf ("REPLACE INTO %sD VALUES ('%s', '%s', '%s',\n", q, d, a, b)
	    printf ("  'dtcaldat between ''%s-%s-%s%%'' and ''%s-%s-%s%%''');\n",
		substr(a,1,4), substr(a,5,6), substr(a,7,8),
		substr(b,1,4), substr(b,5,6), substr(b,7,8))
	}
	fd = ""
end
