  ĻV  #D  1$TITLE = "OBJECT"
$CTIME = 878304539
$MTIME = 878318033
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
NEXTEND =                    4 / Number of extensions
FILENAME= 'dflatJ0461.fits'    / Original host filename
OBSTYPE = 'object  '           / Observation type
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '5:26:51.79'         / RA of observation (hr)
DEC     = '-17:00:00.0'        / DEC of observation (deg)
OBJRA   = '5:26:51.79'         / right ascension [HH:MM:SS.SS]
OBJDEC  = '-17:00:00.0'        / declination [DD:MM:SS.S]
OBJEPOCH=               2007.8 / epoch [YYYY]

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2007-10-31T10:15:55.0' / Date of observation start (UTC)
TIME-OBS= '10:15:55'           / universal time [HH:MM:SS]
MJD-OBS =       54404.42771991 / MJD of observation start
ST      = '5:26:53 '           / sidereal time [HH:MM:SS]

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2007.8 / Equinox of tel coords
TELRA   = '5:26:51.79'         / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
HA      = '0:00:00.13'         / telescope ha [No Units]
ZD      =              48.9625 / Zenith distance
AIRMASS =                1.521 / Airmass
TELFOCUS=                10975 / Telescope focus

DETECTOR= 'NEWFIRM '           / Mosaic detector
MOSSIZE = '[1:4096,1:4096]'    / Mosaic detector size
NDETS   =                    4 / Number of detectors in mosaic
FILTER  = 'J       '           / Filter name(s)

OBSERVER= 'Hal_Halbedel'       / Observer(s)
PROPOSER= 'Ron_Probst'         / Proposer(s)
PROPID  = '2007A-TnE'          / Proposal identification
OBSID   = 'kp4m.20071031T101555' / Observation ID
SEQID   = 'NFQR_2007ATnE_0027' / Sequence ID
SEQNUM  =                   27 / Sequence Number
EXPID   =                    0 / Monsoon exposure ID
NOCID   =      2454405.1359522 / NEWFIRM ID

NOHS    = '1.1.1   '           / NOHS ID [No Units]
NOCNO   =                    1 / observation number in this sequence [No Units]
NOCUTC  =             20071031 / NOCUTC ID [YYYYMMDD]
NOCRSD  =      2454405.1215835 / recipe date [MSD]
NOCORA  =                    0 / RA offset [Arcseconds]
NOCDITER=                    0 / dither iteration count [No Units]
NOCSKY  =                    0 / sky offset modulus [No Units]
NOCDPAT = '5PX     '           / dither pattern [No Units]
NOCGID  =      2454405.1215835 / group identification number [MSD]
NOCDROF =                    0 / dither RA offset [Arcseconds]
NOCDHS  = 'STARE   '           / DHS script name [No Units]
NOCNAME = 'dflatJ5s.fits'      / file name for data set [No Units]
NOCMPOS =                    0 / map position [No Units]
NOCTIM  =                    5 / requested integration time [Seconds]
NOCMDOF =                    0 / map Dec offset [Arcminutes]
NOCMREP =                    0 / map repetition count [No Units]
NOCTOT  =                    5 / total number of observations in set [No Units]
NOCFOCUS=                11200 / ntcs_focus value [micron]
NOCSYS  = 'kpno_4m '           / system ID [No Units]
NOCLAMP = 'off     '           / dome flat lamp status (on|off)
NOCDPOS =                    0 / dither position [No Units]
NOCMITER= 'arcminutes'         / map iteration count [No Units]
NOCID   =      2454405.1359522 / observation ID a.k.a expID [MSD]
NOCODEC =                    0 / Dec offset [Arcseconds]
NOCMPAT = '5PX     '           / map pattern [No Units]
NOCDDOF =                    0 / dither Dec offset [Arcseconds]
NOCNUM  =                    5 / observation number request [No Units]
NOCMROF =                    0 / map RA offset [Arcminutes]
NOCTYP  = 'OBJECT  '           / observation type [No Units]
NOCDREP =                    0 / dither repetition count [No Units]

NFOSSTMP=            64.988998 / oss temp measured [K]
NFDETTMP=                   30 / detector array temp measured [K]

NFFILPOS= 'J       '           / detected position name [No Units]
NFFW2POS=                    1 / wheel 2 actual position (0|1|2|3|4|5|6|7|8) [No
NFFW1POS=                    8 / wheel 1 actual position (0|1|2|3|4|5|6|7|8) [No

DECDIFF =                    0 / dec diff [Arcsecond]
AZ      = '180:00:02.4'        / telescope azimuth [DD:MM:SS]
DECOFF  =                    0 / dec offset [Arcsecond]
RAOFF   =                    0 / ra offset [Arcsecond]
RAINDEX =              -221.89 / ra index [Arcsecond]
ALT     = '41:02:15.0'         / telescope altitude [HH:MM:SS]
RAZERO  =                33.62 / ra zero [Arcsecond]
RADIFF  =                    0 / ra diff [Arcsecond]
DECZERO =                91.12 / dec zero [Arcsecond]
DECINDEX=                   35 / dec index [Arcsecond]

DOMEERR =              -135.32 / dome error as distance from target [Degrees]
DOMEAZ  =                359.3 / dome position [Degrees]

NFC1POS = '00:00:00.00 00:00:00.0 2007 [s]' / camera 1 target in RA Dec Epoch [H
NFC1GDR = 'off [s] '           / camera 1 guider mode [No Units]
NFC1FILT= '0 [s]   '           / camera 1 filter [No Units]

TCPGDR  = 'off     '           / guider status (on|off) [No Units]

NFC2FILT= '0 [s]   '           / camera 2 filter [No Units]
NFC2POS = '00:00:00.00 00:00:00.0 2007 [s]' / camera 2 target in RA Dec Epoch [H
NFC2GDR = 'off [s] '           / camera 2 guider mode [No Units]

NFECPOS = 'open    '           / detected position (open|close|between) [No Unit
PROCMEAN=             1973.472
PCOUNT  =                    0 / No 'random' parameters
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'im2     '           / Extension name
EXTVER  =                    2 / Extension version
INHERIT =                    T / Inherits global header
DATE    = '2007-10-31T20:02:32' / Date FITS file was generated
IRAF-TLM= '13:20:43 (31/10/2007)' / Time of last modification
OBJECT  = 'OBJECT  '           / Name of the object observed
IMAGEID =                    2 / Image identification
EXPTIME =                    5 / Actual integration time in seconds
MJDSTART=        54404.6359817 / MJD of observation start
MJDEND  =        54404.6360396 / MJD of observation end

DETNAME = 'Raytheon InSb #2 (NOAO 2)' / Array name
DETSIZE = '[1:2046,1:2046]'    / Detector size
DETSEC  = '[2047:4092,1:2046]' / Detector section

LTM1_1  =                  1.0 / Detector to image transformation
LTM2_2  =                  1.0 / Detector to image transformation

WCSASTRM= 'image1399 (USNO N J) by F. Valdes 2007-06-29' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =      81.715791666667 / Coordinate reference value
CRVAL2  =                -17.0 / Coordinate reference value
CRPIX1  =     -29.163260286592 / Coordinate reference pixel
CRPIX2  =      2162.2692847543 / Coordinate reference pixel
CD1_1   = -5.1147689850518e-07 / Coordinate matrix
CD2_1   =  0.00010987882059486 / Coordinate matrix
CD1_2   = -0.00010985960236195 / Coordinate matrix
CD2_2   =   3.272714354659e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 1. 0.01171189932174763 0.2374'
WAT1_002= '207564093686 0.002606981764336732 0.2279784399663589 -1.401604116952'
WAT1_003= '021E-4 -3.936577357846626E-5 -2.976925006198533E-4 2.239952792040140'
WAT1_004= 'E-6 8.680333404602644E-5 -1.837781850368252E-4 1.517676062202822E-4 '
WAT1_005= '2.513111858786038E-5 -4.188598056252738E-5 -7.136997948722332E-6 3.5'
WAT1_006= '84997215855048E-5 4.493177238217266E-6 2.619251517739210E-5 2.782258'
WAT1_007= '896691837E-5 -7.193441255813327E-6 -4.343861370939314E-6 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 1. 0.01171189932174763 0.237'
WAT2_002= '4207564093686 0.002606981764336732 0.2279784399663589 -1.12172936271'
WAT2_003= '3350E-4 6.366732306022806E-5 -3.440491012919562E-5 3.128928605225737'
WAT2_004= 'E-5 -2.699644307419462E-5 -2.027187570974867E-4 -7.877716146459297E-'
WAT2_005= '6 3.017602406323044E-5 -2.768789902917035E-4 1.593048134610645E-4 3.'
WAT2_006= '330658702881534E-5 -5.471784519107507E-6 2.698404181058821E-6 2.3638'
WAT2_007= '36933637852E-5 1.268344218130272E-5 3.244156533024538E-6 "'

PROC0001= 'Oct 31 12:56 Trim $I'
PROC0002= 'Oct 31 12:56 trimsec = [2:2047,2:2047]'
PROCDONE= 'T       '
PROCAVG =              1902.85
PROCSIG =               148.84
PROCID  = 'kp4m.20071031T101555V1'
PROCID01= 'kp4m.20071031T101555'
PROCID02= 'kp4m.20071031T101604'
PROCID03= 'kp4m.20071031T101613'
PROCID04= 'kp4m.20071031T101622'
PROCID05= 'kp4m.20071031T101634'
IMCMB001= '        '
IMCMB002= '        '
IMCMB003= '        '
IMCMB004= '        '
IMCMB005= '        '
NCOMBINE=                    5
BPM     = 'bpm071031_im2.pl'
   Ū     ū  ū                 *Ø  *Ø  ū    1      ˙˙˙               ˙        |@ @`	`	`	`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
`
`
`
`
 
@`
`
`
`
`
`
`
 
@ 
@`
`
`
`
`
`
`
 
@ 
@`
`
`
`
`
`
`
 
@ 
@`
`
`
`
`
 
@ 
@ 
@`
`
`
`
`
 
@ 
@ 
@`
`
`
`
`
 
@ 
@ 
@`
`
`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
`
`
 
@ 
@ 
@ 
@`
`
`
 
@ 
@ 
@ 
@`
`
`
 
@ 
@ 
@`
`
`
`
`
 
@`
`
``````` @ @ @``````` @````````````````````````````` @````````````
`
`
`
`
`
`
`
 
@`
`
`
`
`
````` 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@`
`
` 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@`
> @` @`````=  @`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 
@ 
@`
 @ @` @ @ 
@ 
P÷×   @ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@`
`	 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
` 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@`
``` @` @`` @````` 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
`
```````````````` 
@ 
@ 
@`
`
`
 @ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
`
````` @ @``` @`` @``````` 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@`
`
 @ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@`
` 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
`
`
 @ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
`
g˙```````````````````xÚ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
 
@ 
@ 
@ 
@`
`
```
``
 
@ 
@`
 
@ 
@ 
@ 
@ 
@`
 
@ 
@ 
@`
```````` @ 
@`
 
@ 
@ 
@ 
@`
``````````````````````````````` ˙      ū  ˙      Wū  ˙ 	   	  ü@  ˙ 	   	  û@  ˙ 	   	  ú@  ˙ 
   
  ú@   ˙ 
   
  ų@   ˙ 
   
  ų@   ˙ 
   
  ø@   ˙ 
   
  ø@   ˙ 
   
  ÷@   ˙ 
   
  ÷@   ˙ 
   
  ö@   ˙ 
   
  ö@   ˙ 
   
  õ@   ˙ 
   
  ô@   ˙ 
   
  ô@   ˙ 
   
  ķ@   ˙ 
   
  ķ@   ˙ 
   
  ō@   ˙ 
   
  ō@ 	  ˙ 
   
  ņ@ 	  ˙ 
   
  ņ@ 
  ˙ 
   
  đ@ 
  ˙ 
   
  đ@   ˙ 
   
  ī@   ˙ 
   
  î@   ˙ 
   
  î@   ˙ 
   
  í@   ˙ 
   
  í@   ˙ 
   
  ė@   ˙ 
   
  ė@   ˙ 
   
  ë@   ˙ 
   
  ë@   ˙ 
   
  ę@   ˙ 
   
  é@   ˙ 
   
  č@   ˙ 
   
  č@   ˙ 
   
  į@   ˙ 
   
  į@   ˙ 
   
  æ@   ˙ 
   
  æ@   ˙ 
   
  å@   ˙ 
   
  å@   ˙ 
   
  ä@   ˙ 
   
  ã@   ˙ 
   
  â@   ˙ 
   
  â@   ˙ 
   
  á@   ˙ 
   
  á@   ˙ 
   
  ā@   ˙ 
   
  ā@   ˙ 
   
  ß@   ˙ 
   
  ß@   ˙ 
   
  Ū@   ˙ 
   
  Ũ@   ˙ 
   
  Ü@   ˙ 
   
  Ü@   ˙ 
   
  Û@   ˙ 
   
  Û@    ˙ 
   
  Ú@    ˙ 
   
  Ú@ !  ˙ 
   
  Ų@ "  ˙ 
   
  Ø@ #  ˙ 
   
  ×@ $  ˙ 
   
  Ö@ $  ˙ 
   
  Ö@ %  ˙ 
   
  Õ@ %  ˙ 
   
  Õ@ &  ˙ 
   
  Ô@ &  ˙ 
   
  Ô@ '  ˙ 
   
  Ķ@ (  ˙ 
   
  Ō@ )  ˙ 
   
  Ņ@ *  ˙ 
   
  Đ@ *  ˙ 
   
  Đ@ +  ˙ 
   
  Ī@ +  ˙ 
   
  Ī@ ,  ˙ 
   
  Î@ ,  ˙ 
   
  Î@ -  ˙ 
   
  Í@ .  ˙ 
   
  Ė@ /  ˙ 
   
  Ë@ 0  ˙ 
   
  Ę@ 0  ˙ 
   
  Ę@ 1  ˙ 
   
  É@ 1  ˙ 
   
  É@ 2  ˙ 
   
  Č@ 3  ˙ 
   
  Į@ 4  ˙ 
   
  Æ@ 5  ˙ 
   
  Å@ 6  ˙ 
   
  Ä@ 6  ˙ 
   
  Ä@ 7  ˙ 
   
  Ã@ 7  ˙ 
   
  Ã@ 8  ˙ 
   
  Â@ 9  ˙ 
   
  Á@ :  ˙ 
   
  Ā@ ;  ˙ 
   
  ŋ@ <  ˙ 
   
  ž@ <  ˙ 
   
  ž@ =  ˙ 
   
  Ŋ@ =  ˙ 
   
  Ŋ@ >  ˙ 
   
  ŧ@ ?  ˙ 
   
  ģ@ @  ˙ 
   
  ē@ A  ˙ 
   
  š@ B  ˙ 
   
  ¸@ B  ˙ 
   
  ¸@ C  ˙ 
   
  ˇ@ D  ˙ 
   
  ļ@ E  ˙ 
   
  ĩ@ F  ˙ 
   
  ´@ G  ˙ 
   
  ŗ@ H  ˙ 
   
  ˛@ I  ˙ 
   
  ą@ J  ˙ 
   
  °@ K  ˙ 
   
  ¯@ L  ˙ 
   
  Ž@ M  ˙ 
   
  ­@ N  ˙ 
   
  Ŧ@ O  ˙ 
   
  Ģ@ P  ˙ 
   
  Ē@ Q  ˙ 
   
  Š@ R  ˙ 
   
  ¨@ S  ˙ 
   
  §@ T  ˙ 
   
  Ļ@ T  ˙ 
   
  Ļ@ U  ˙ 
   
  Ĩ@ V  ˙ 
   
  ¤@ W  ˙ 
   
  Ŗ@ X  ˙ 
   
  ĸ@ Y  ˙ 
   
  Ą@ Z  ˙ 
   
   @ Z  ˙ 
   
   @ [  ˙ 
   
  @ \  ˙ 
   
  @ ]  ˙ 
   
  @ ^  ˙ 
   
  @ _  ˙ 
   
  @ `  ˙ 
   
  @ `  ˙ 
   
  @ a  ˙ 
   
  @ b  ˙ 
   
  @ c  ˙ 
   
  @ d  ˙ 
   
  @ e  ˙ 
   
  @ e  ˙ 
   
  @ f  ˙ 
   
  @ f  ˙ 
   
  @ g  ˙ 
   
  @ h  ˙ 
   
  @ i  ˙      TņĄ@ i  ˙      ę@@ j  ˙      č@@ j  ˙      æ@@ k  ˙      ä@@ k  ˙      ã@@ l  ˙      â@@ l  ˙      á@@ m  ˙      ā@!@ n  ˙      ß@#@ o  ˙      Ū@%@ p  ˙      Ũ@'@ q  ˙      Ũ@'@ q  ˙      Ũ@'@ r  ˙      Ũ@'@ r  ˙      Ũ@'@ s  ˙      Ũ@'@ t  ˙      Ü@)@ t  ˙      Ũ@'@ u  ˙      Ũ@'@ v  ˙      Ũ@'@ v  ˙      Ũ@'@ w  ˙      Ũ@'@ w  ˙      Ū@%@ x  ˙      Ū@%@ x  ˙      ß@#@ y  ˙      ß@#@ z  ˙      ā@!@ z  ˙      ā@!@ {  ˙      á@@ {  ˙      â@@ |  ˙      ã@@ |  ˙      ä@@ }  ˙      æ@@ }  ˙      č@@ ~  ˙      T< Ž@@ ~  ˙      7@	Pą@   ˙      5@9@   ˙      4@8@   ˙      3@6@   ˙      2@5@   ˙      1@3@   ˙      1@2@   ˙      0@1@   ˙      0@0@   ˙      0@0@   ˙      0@/@   ˙      /@.@   ˙      0@.@   ˙      0@-@   ˙      0@,@   ˙      1@-@   ˙      1@,@   ˙      2@-@   ˙      3@-@   ˙      4@.@   ˙      5@.@   ˙      7@	0@   ˙      T<3@   ˙ 
   
  o@   ˙ 
   
  n@   ˙ 
   
  m@   ˙ 
   
  m@   ˙ 
   
  l@   ˙ 
   
  l@   ˙ 
   
  k@   ˙ 
   
  k@   ˙ 
   
  j@   ˙ 
   
  g@   ˙ 
   
  e@   ˙ 
   
  7@5   ˙ 
   
  @f   ˙ 
   
  Ø@   ˙      ¨@ .@   ˙      y@ _@   ˙      s@e @   ˙      s@5 Â@   ˙      s@ ņ@   ˙ 
   
  k@   ˙ 
   
  l@   ˙ 
   
  m@   ˙ 
   
  n@   ˙ 
   
  o@   ˙ 
   
  p@   ˙ 
   
  q@   ˙ 
   
  r@   ˙ 
   
  s@   ˙ 
   
  t@   ˙ 
   
  u@   ˙ 
   
  v@   ˙ 
   
  w@   ˙ 
   
  x@   ˙ 
   
  y@   ˙ 
   
  z@   ˙ 
   
  {@   ˙ 
   
  |@   ˙ 
   
  }@ ~  ˙ 
   
  ~@ }  ˙ 
   
  @ |  ˙ 
   
  @ {  ˙ 
   
  @ z  ˙ 
   
  @ y  ˙ 
   
  @ x  ˙ 
   
  @ w  ˙ 
   
  @ v  ˙ 
   
  @ u  ˙ 
   
  @ t  ˙ 
   
  @ s  ˙ 
   
  @ r  ˙ 
   
  @ q  ˙ 
   
  @ p  ˙ 
   
  @ o  ˙ 
   
  @ n  ˙ 
   
  @ m  ˙ 
   
  @ l  ˙ 
   
  @ k  ˙ 
   
  @ j  ˙ 
   
  @ i  ˙ 
   
  @ h  ˙ 
   
  @ g  ˙ 
   
  @ f  ˙ 
   
  @ e  ˙ 
   
  @ d  ˙ 
   
  @ c  ˙ 
   
  @ b  ˙ 
   
  @ a  ˙ 
   
  @ `  ˙ 
   
  @ _  ˙ 
   
  @ ^  ˙ 
   
  @ ]  ˙ 
   
  @ \  ˙ 
   
   @ [  ˙ 
   
  Ą@ Z  ˙ 
   
  ĸ@ Y  ˙ 
   
  Ŗ@ X  ˙ 
   
  ¤@ W  ˙ 
   
  Ĩ@ V  ˙ 
   
  Ļ@ U  ˙ 
   
  §@ T  ˙ 
   
  ¨@ S  ˙ 
   
  Š@ R  ˙ 
   
  Ē@ Q  ˙ 
   
  Ģ@ P  ˙ 
   
  Ŧ@ O  ˙ 
   
  ­@ N  ˙ 
   
  Ž@ M  ˙ 
   
  ¯@ L  ˙ 
   
  °@ K  ˙ 
   
  ą@ J  ˙ 
   
  ˛@ I  ˙      Gū  ˙ 
   
  ŗ@ H  ˙ 
   
  ´@ G  ˙ 
   
  ĩ@ F  ˙ 
   
  ļ@ E  ˙ 
   
  ˇ@ D  ˙ 
   
  ¸@ C  ˙ 
   
  š@ B  ˙ 
   
  ē@ A  ˙ 
   
  ģ@ @  ˙ 
   
  ŧ@ ?  ˙ 
   
  Ŋ@ >  ˙ 
   
  ž@ =  ˙ 
   
  ŋ@ <  ˙ 
   
  Ā@ ;  ˙ 
   
  Á@ :  ˙ 
   
  Â@ 9  ˙ 
   
  Ã@ 8  ˙ 
   
  Ä@ 7  ˙ 
   
  Å@ 6  ˙ 
   
  Æ@ 5  ˙ 
   
  Į@ 4  ˙ 
   
  Č@ 3  ˙ 
   
  É@ 2  ˙ 
   
  Ę@ 1  ˙ 
   
  Đ@ +  ˙ 
   
  Ņ@ *  ˙ 
   
  Ō@ )  ˙ 
   
  Ķ@ (  ˙ 
   
  Ô@ '  ˙ 
   
  Õ@ &  ˙ 
   
  Ö@ %  ˙ 
   
  ×@ $  ˙ 
   
  Ø@ #  ˙ 
   
  Ų@ "  ˙ 
   
  Ú@ !  ˙ 
   
  Û@    ˙ 
   
  Ü@   ˙ 
   
  Ũ@   ˙ 
   
  Ū@   ˙ 
   
  ß@   ˙ 
   
  ā@   ˙ 
   
  á@   ˙ 
   
  â@   ˙ 
   
  ã@   ˙ 
   
  ä@   ˙ 
   
  å@   ˙ 
   
  æ@   ˙ 
   
  į@   ˙ 
   
  č@   ˙ 
   
  é@   ˙ 
   
  ę@   ˙ 
   
  ë@   ˙ 
   
  ė@   ˙ 
   
  í@   ˙ 
   
  î@   ˙ 
   
  ī@   ˙ 
   
  đ@   ˙ 
   
  ņ@ 
  ˙ 
   
  ō@ 	  ˙ 
   
  ķ@   ˙ 
   
  ô@   ˙ 
   
  õ@   ˙ 
   
  ö@   ˙ 
   
  ÷@   ˙ 
   
  ø@   ˙ 
   
  L@¯  ˙ 
   
  K@°  ˙ 
   
  J@ą  ˙ 
   
  I@˛  ˙ 
   
  H@ŗ  ˙ 
   
  G@´  ˙ 
   
  F@ĩ  ˙ 
   
  E@ļ  ˙ 
   
  D@ˇ  ˙ 
   
  C@¸  ˙ 
   
  B@š  ˙ 
   
  A@ē  ˙ 
   
  @@ģ  ˙ 
   
  ?@ŧ  ˙ 
   
  >@Ŋ  ˙ 
   
  =@ž  ˙ 
   
  <@ŋ  ˙ 
   
  ;@Ā  ˙ 
   
  :@Á  ˙ 
   
  9@Â  ˙ 
   
  8@Ã  ˙ 
   
  7@Ä  ˙ 
   
  6@Å  ˙ 
   
  5@Æ  ˙ 
   
  4@Į  ˙ 
   
  3@Č  ˙ 
   
  2@É  ˙ 
   
  1@Ę  ˙ 
   
  0@Ë  ˙ 
   
  /@Ė  ˙ 
   
  .@Í  ˙ 
   
  -@Î  ˙ 
   
  ,@Ī  ˙ 
   
  +@Đ  ˙ 
   
  *@Ņ  ˙ 
   
  )@Ō  ˙ 
   
  (@Ķ  ˙ 
   
  '@Ô  ˙ 
   
  &@Õ  ˙ 
   
  %@Ö  ˙ 
   
  $@×  ˙ 
   
  #@Ø  ˙ 
   
  "@Ų  ˙ 
   
  !@Ú  ˙ 
   
   @Û  ˙ 
   
  @Ü  ˙ 
   
  @Ũ  ˙ 
   
  @Ū  ˙ 
   
  @ß  ˙ 
   
  @ā  ˙ 
   
  @á  ˙ 
   
  @â  ˙ 
   
  @ã  ˙ 
   
  @ä  ˙ 
   
  @å  ˙ 
   
  @æ  ˙ 
   
  @į  ˙ 
   
  @č  ˙ 
   
  @é  ˙ 
   
  @ę  ˙ 
   
  @ë  ˙ 
   
  @ė  ˙ 
   
  @í  ˙ 
   
  @î  ˙ 
   
  @ī  ˙ 
   
  @đ  ˙ 
   
  
@ņ  ˙ 
   
  	@ō  ˙ 
   
  @ķ  ˙ 
   
  @ô  ˙ 
   
  @õ  ˙ 
   
  @ö  ˙ 
   
  @÷  ˙ 
   
  @ø  ˙ 
   
  @ų  ˙ 
   
  @ú  ˙ 
   
   @û  ˙ 
   
  ˙@ü  ˙ 
   
  ū@ũ  ˙ 
   
  ũ@ū  ˙ 
   
  ü@˙  ˙ 
   
  û@   ˙ 
   
  ú@  ˙ 	   	   Gũ  ˙ 
   
  ų@  ˙ 
   
  ø@  ˙ 
   
  ÷@  ˙ 
   
  ö@  ˙ 
   
  õ@  ˙ 
   
  ô@  ˙ 
   
  ķ@  ˙ 
   
  ō@	  ˙ 
   
  ņ@
  ˙ 
   
  đ@  ˙ 
   
  ī@  ˙ 
   
  î@  ˙ 
   
  í@  ˙ 
   
  ė@  ˙ 
   
  ë@  ˙ 
   
  ę@  ˙      Gū  ˙ 
   
  é@  ˙ 
   
  č@  ˙ 
   
  į@  ˙ 
   
  æ@  ˙ 
   
  å@  ˙ 
   
  ä@  ˙ 
   
  ã@  ˙ 
   
  â@  ˙ 
   
  á@  ˙ 
   
  ā@  ˙ 
   
  ß@  ˙ 
   
  Ū@  ˙ 
   
  Ũ@  ˙ 
   
  Ü@  ˙ 
   
  Û@   ˙ 
   
  Ú@!  ˙ 
   
  Ų@"  ˙ 
   
  Ø@#  ˙ 
   
  ×@$  ˙ 
   
  Ö@%  ˙ 
   
  Õ@&  ˙ 
   
  Ô@'  ˙ 
   
  Ķ@(  ˙ 
   
  Ō@)  ˙ 
   
  Ņ@*  ˙ 
   
  Đ@+  ˙ 
   
  Ī@,  ˙ 
   
  Î@-  ˙ 
   
  Í@.  ˙ 
   
  Ė@/  ˙ 
   
  Ë@0  ˙ 
   
  Ę@1  ˙ 
   
  É@2  ˙ 
   
  Č@3  ˙ 
   
  Į@4  ˙ 
   
  Æ@5  ˙ 
   
  Å@6  ˙ 
   
  Ä@7  ˙ 
   
  Ã@8  ˙ 
   
  Â@9  ˙ 
   
  Á@:  ˙ 
   
  Ā@;  ˙ 
   
  ŋ@<  ˙ 
   
  ž@=  ˙ 
   
  Ŋ@>  ˙ 
   
  ŧ@?  ˙ 
   
  ģ@@  ˙ 
   
  ē@A  ˙ 
   
  š@B  ˙      QōÆ@C  ˙      î@Ã@C  ˙      í@	Á@D  ˙      ė@Ā@D  ˙      ë@ž@E  ˙      ë@Ŋ@F  ˙      ę@ģ@G  ˙      ę@ē@H  ˙      ë@ģ@H  ˙      ë@ē@I  ˙      ė@ē@J  ˙      í@	ģ@J  ˙      î@ģ@K  ˙      QōŊ@L  ˙ 
   
  ¯@L  ˙ 
   
  Ž@M  ˙ 
   
  ­@N  ˙ 
   
  Ŧ@O  ˙ 
   
  Ģ@P  ˙ 
   
  Ē@Q  ˙ 
   
  Š@R  ˙ 
   
  ¨@S  ˙ 
   
  §@T  ˙ 
   
  Ļ@U  ˙ 
   
  Ĩ@V  ˙ 
   
  ¤@W  ˙ 
   
  Ŗ@X  ˙ 
   
  ĸ@Y  ˙ 
   
  Ą@Z  ˙ 
   
   @[  ˙ 
   
  @\  ˙ 
   
  @]  ˙ 
   
  @^  ˙ 
   
  @_  ˙ 
   
  @`  ˙ 
   
  @a  ˙ 
   
  @b  ˙ 
   
  @c  ˙ 
   
  @d  ˙      S( o@d  ˙      $@ k@e  ˙      #@	 j@e  ˙      "@ h@f  ˙      !@ f@g  ˙       @ f@g  ˙       @ e@h  ˙       @ d@h  ˙      @ d@i  ˙       @ d@i  ˙       @ c@j  ˙       @ d@j  ˙      !@ d@k  ˙      "@	 e@k  ˙      #@ e@l  ˙      S' g@m  ˙ 
   
  @m  ˙ 
   
  @n  ˙ 
   
  @o  ˙ 
   
  @p  ˙ 
   
  @q  ˙ 
   
  @r  ˙      Gū  ˙ 
   
  @s  ˙ 
   
  @t  ˙ 
   
  @u  ˙ 
   
  @v  ˙ 
   
  @w  ˙ 
   
  @x  ˙ 
   
  @y  ˙ 
   
  @z  ˙ 
   
  @{  ˙ 
   
  @|  ˙ 
   
  ~@}  ˙ 
   
  }@~  ˙ 
   
  |@  ˙ 
   
  {@  ˙ 
   
  z@  ˙ 
   
  y@  ˙ 
   
  x@  ˙ 
   
  w@  ˙ 
   
  v@  ˙ 
   
  u@  ˙ 
   
  t@  ˙ 
   
  s@  ˙ 
   
  r@  ˙ 
   
  q@  ˙ 
   
  p@  ˙ 
   
  o@  ˙      QĀ¯@  ˙      ģ@	Ē@  ˙      š@§@  ˙      ¸@Ļ@  ˙      ˇ@¤@  ˙      ļ@Ŗ@  ˙      ĩ@Ą@  ˙      ´@@  ˙      ´@@  ˙      ´@@  ˙      ŗ@@  ˙      ´@@  ˙      Gū  ˙      ´@@  ˙      ĩ@@  ˙      ļ@@  ˙      ˇ@@  ˙      ¸@@  ˙      š@@  ˙      ģ@	@  ˙      QĀĄ@  ˙ 
   
  a@  ˙ 
   
  `@  ˙ 
   
  _@  ˙ 
   
  ^@  ˙ 
   
  ]@  ˙ 
   
  \@  ˙ 
   
  [@   ˙ 
   
  Z@Ą  ˙ 
   
  Y@ĸ  ˙ 
   
  X@Ŗ  ˙ 
   
  W@¤  ˙ 
   
  V@Ĩ  ˙ 
   
  U@Ļ  ˙ 
   
  T@§  ˙ 
   
  S@¨  ˙ 
   
  R@Š  ˙ 
   
  Q@Ē  ˙ 
   
  P@Ģ  ˙ 
   
  O@Ŧ  ˙ 
   
  N@­  ˙ 
   
  M@Ž  ˙ 
   
  L@¯  ˙ 
   
  K@°  ˙ 
   
  J@ą  ˙ 
   
  I@˛  ˙ 
   
  H@ŗ  ˙ 
   
  G@´  ˙ 
   
  F@ĩ  ˙ 
   
  E@ļ  ˙ 
   
  D@ˇ  ˙ 
   
  C@¸  ˙ 
   
  B@š  ˙ 
   
  A@ē  ˙ 
   
  @@ģ  ˙      Gū  ˙ 
   
  ?@ŧ  ˙ 
   
  >@Ŋ  ˙ 
   
  =@ž  ˙ 
   
  <@ŋ  ˙ 
   
  ;@Ā  ˙ 
   
  :@Á  ˙ 
   
  9@Â  ˙ 
   
  8@Ã  ˙ 
   
  7@Ä  ˙ 
   
  6@Å  ˙ 
   
  5@Æ  ˙ 
   
  4@Į  ˙ 
   
  3@Č  ˙ 
   
  2@É  ˙ 
   
  1@Ę  ˙ 
   
  0@Ë  ˙ 
   
  /@Ė  ˙ 
   
  .@Í  ˙ 
   
  -@Î  ˙ 
   
  ,@Ī  ˙ 
   
  +@Đ  ˙ 
   
  *@Ņ  ˙ 
   
  )@Ō  ˙ 
   
  (@Ķ  ˙ 
   
  '@Ô  ˙ 
   
  &@Õ  ˙ 
   
  %@Ö  ˙ 
   
  $@×  ˙ 
   
  #@Ø  ˙ 
   
  "@Ų  ˙ 
   
  !@Ú  ˙ 
   
   @Û  ˙ 
   
  @Ü  ˙ 
   
  @Ũ  ˙ 
   
  @Ū  ˙ 
   
  @ß  ˙ 
   
  @ā  ˙ 
   
  @á  ˙ 
   
  @â  ˙ 
   
  @ã  ˙ 
   
  @ä  ˙ 
   
  @å  ˙ 
   
  @æ  ˙ 
   
  @į  ˙ 
   
  @č  ˙ 
   
  @é  ˙ 
   
  @ę  ˙ 
   
  @ë  ˙ 
   
  @ė  ˙ 
   
  @í  ˙ 
   
  @î  ˙ 
   
  @ī  ˙ 
   
  @đ  ˙ 
   
  
@ņ  ˙ 
   
  	@ō  ˙ 
   
  @ķ  ˙ 
   
  @ô  ˙ 
   
  @õ  ˙ 
   
  @ö  ˙ 
   
  @÷  ˙ 
   
  @ø  ˙ 
   
  @ų  ˙ 
   
  @ú  ˙ 
   
   @û  ˙ 
   
  ˙@ü  ˙ 
   
  ū@ũ  ˙ 
   
  ũ@ū  ˙ 
   
  ü@˙  ˙ 
   
  û@   ˙ 
   
  ú@  ˙ 
   
  ų@  ˙ 
   
  ø@  ˙ 
   
  ÷@  ˙ 
   
  ö@  ˙ 
   
  õ@  ˙ 
   
  ô@  ˙ 
   
  ķ@  ˙ 
   
  ō@	  ˙ 
   
  ņ@
  ˙ 
   
  đ@  ˙ 
   
  ī@  ˙ 
   
  î@  ˙ 
   
  í@  ˙ 
   
  ė@  ˙ 
   
  ë@  ˙ 
   
  ę@  ˙ 
   
  é@  ˙ 
   
  č@  ˙ 
   
  į@  ˙ 
   
  æ@  ˙ 
   
  å@  ˙ 
   
  ä@  ˙ 
   
  ã@  ˙ 
   
  â@  ˙ 
   
  á@  ˙ 
   
  ā@  ˙ 
   
  ß@  ˙ 
   
  Ū@  ˙ 
   
  Ũ@  ˙ 
   
  Ü@  ˙ 
   
  Û@   ˙ 
   
  Ú@!  ˙ 
   
  Ų@"  ˙ 
   
  Ø@#  ˙      Gū  ˙ 
   
  ×@$  ˙ 
   
  Ö@%  ˙ 
   
  Õ@&  ˙ 
   
  Ô@'  ˙ 
   
  Ķ@(  ˙ 
   
  Ō@)  ˙ 
   
  Ņ@*  ˙ 
   
  Đ@+  ˙ 
   
  Ī@,  ˙ 
   
  Î@-  ˙ 
   
  Í@.  ˙ 
   
  Ė@/  ˙ 
   
  Ë@0  ˙ 
   
  Ę@1  ˙ 
   
  É@2  ˙ 
   
  Č@3  ˙ 
   
  Į@4  ˙ 
   
  Æ@5  ˙ 
   
  Å@6  ˙ 
   
  Ä@7  ˙ 
   
  Ã@8  ˙ 
   
  Â@9  ˙ 
   
  Á@:  ˙ 
   
  Ā@;  ˙ 
   
  ŋ@<  ˙ 
   
  ž@=  ˙ 
   
  Ŋ@>  ˙ 
   
  ŧ@?  ˙ 
   
  ģ@@  ˙ 
   
  ē@A  ˙ 
   
  š@B  ˙ 
   
  ¸@C  ˙ 
   
  ˇ@D  ˙ 
   
  ļ@E  ˙ 
   
  ĩ@F  ˙ 
   
  ´@G  ˙ 
   
  ŗ@H  ˙ 
   
  ˛@I  ˙ 
   
  ą@J  ˙ 
   
  °@K  ˙ 
   
  ¯@L  ˙ 
   
  Ž@M  ˙ 
   
  ­@N  ˙ 
   
  Ŧ@O  ˙ 
   
  Ģ@P  ˙ 
   
  Ē@Q  ˙ 
   
  Š@R  ˙ 
   
  ¨@S  ˙ 
   
  §@T  ˙ 
   
  Ļ@U  ˙ 
   
  Ĩ@V  ˙ 
   
  ¤@W  ˙ 
   
  Ŗ@X  ˙ 
   
  ĸ@Y  ˙      Gū  ˙ 
   
  Ą@Z  ˙ 
   
   @[  ˙ 
   
  @\  ˙ 
   
  @]  ˙ 
   
  @^  ˙ 
   
  @_  ˙ 
   
  @`  ˙ 
   
  @a  ˙ 
   
  @b  ˙ 
   
  @c  ˙ 
   
  @d  ˙ 
   
  @e  ˙ 
   
  @f  ˙ 
   
  @g  ˙ 
   
  @h  ˙ 
   
  @i  ˙ 
   
  @j  ˙ 
   
  @k  ˙ 
   
  @l  ˙ 
   
  @m  ˙ 
   
  @n  ˙ 
   
  @o  ˙ 
   
  @p  ˙ 
   
  @q  ˙ 
   
  @r  ˙ 
   
  @s  ˙ 
   
  @t  ˙ 
   
  |@  ˙ 
   
  {@  ˙ 
   
  z@  ˙ 
   
  y@  ˙ 
   
  x@  ˙ 
   
  w@  ˙ 
   
  v@  ˙ 
   
  u@  ˙ 
   
  t@  ˙ 
   
  s@  ˙ 
   
  r@  ˙ 
   
  q@  ˙ 
   
  p@  ˙ 
   
  o@  ˙ 
   
  n@  ˙ 
   
  m@  ˙ 
   
  l@  ˙ 
   
  k@  ˙ 
   
  j@  ˙ 
   
  i@  ˙ 
   
  h@  ˙ 
   
  g@  ˙ 
   
  f@  ˙ 
   
  e@  ˙ 
   
  d@  ˙ 
   
  c@  ˙ 
   
  b@  ˙ 
   
  a@  ˙ 
   
  `@  ˙ 
   
  _@  ˙ 
   
  ^@  ˙ 
   
  ]@  ˙ 
   
  \@  ˙ 
   
  [@   ˙ 
   
  Z@Ą  ˙ 
   
  Y@ĸ  ˙ 
   
  X@Ŗ  ˙ 
   
  W@¤  ˙ 
   
  V@Ĩ  ˙ 
   
  U@Ļ  ˙ 
   
  T@§  ˙ 
   
  S@¨  ˙ 
   
  R@Š  ˙ 
   
  Q@Ē  ˙ 
   
  P@Ģ  ˙ 
   
  O@Ŧ  ˙ 
   
  N@­  ˙ 
   
  M@Ž  ˙ 
   
  L@¯  ˙ 
   
  K@°  ˙ 
   
  J@ą  ˙ 
   
  I@˛  ˙ 
   
  H@ŗ  ˙ 
   
  G@´  ˙ 
   
  F@ĩ  ˙ 
   
  E@ļ  ˙ 
   
  D@ˇ  ˙ 
   
  C@¸  ˙ 
   
  B@š  ˙ 
   
  A@ē  ˙ 
   
  @@ģ  ˙ 
   
  ?@ŧ  ˙ 
   
  >@Ŋ  ˙ 
   
  =@ž  ˙ 
   
  <@ŋ  ˙ 
   
  ;@Ā  ˙ 
   
  :@Á  ˙ 
   
  9@Â  ˙ 
   
  8@Ã  ˙ 
   
  7@Ä  ˙ 
   
  6@Å  ˙ 
   
  5@Æ  ˙ 
   
  4@Į  ˙ 
   
  3@Č  ˙ 
   
  2@É  ˙ 
   
  1@Ę  ˙ 
   
  0@Ë  ˙ 
   
  /@Ė  ˙ 
   
  .@Í  ˙ 
   
  -@Î  ˙ 
   
  ,@Ī  ˙ 
   
  +@Đ  ˙ 
   
  *@Ņ  ˙ 
   
  )@Ō  ˙ 
   
  (@Ķ  ˙ 
   
  '@Ô  ˙ 
   
  &@Õ  ˙ 
   
  %@Ö  ˙ 
   
  $@×  ˙ 
   
  #@Ø  ˙ 
   
  "@Ų  ˙ 
   
  !@Ú  ˙ 
   
   @Û  ˙ 
   
  @Ü  ˙ 
   
  @Ũ  ˙ 
   
  @Ū  ˙ 
   
  @ß  ˙ 
   
  @ā  ˙ 
   
  @á  ˙ 
   
  @â  ˙ 
   
  @ã  ˙ 
   
  @ä  ˙ 
   
  @å  ˙ 
   
  @æ  ˙ 
   
  @į  ˙ 
   
  @č  ˙ 
   
  @é  ˙ 
   
  @ę  ˙ 
   
  @ë  ˙      @ A@¤  ˙      @  @(¤  ˙ 
   
  @L¤  ˙      @IP¨  ˙ 
   
  @'Ė  ˙ 
   
  @í  ˙ 
   
  @ī  ˙ 
   
  @đ  ˙ 
   
  
@ņ  ˙ 
   
  	@ō  ˙ 
   
  @ķ  ˙ 
   
  @ô  ˙ 
   
  @õ  ˙ 
   
  @ö  ˙ 
   
  @÷  ˙ 
   
  @ø  ˙ 
   
  @ų  ˙ 
   
  @ú  ˙       @ų@  ˙       @õ@  ˙      ˙@ō@
  ˙      ū@ī@   ˙      ū@ę@   ˙      ũ@ę@ 	  ˙      ũ@é@	   ˙      ü@ę@   ˙      ü@ę@   ˙ 
   
  û@   ˙ 
   
  ú@  ˙ 
   
  ų@  ˙ 
   
  ø@  ˙ 
   
  ÷@  ˙ 
   
  ö@  ˙ 
   
  õ@  ˙      ô@Ė@;  ˙      ķ@@u  ˙      ķ@Y@¯  ˙      ō@ @Ž ;  ˙      ō@æ@Ž u  ˙      ņ@­@Ž ¯  ˙      ņ@s@Ž é  ˙      ė@:@Ž#  ˙      ē@9 @Ž]  ˙      @iĮ@Ž  ˙      X@@ŽŅ  ˙      '@ 1@Q@¯  ˙       ö@ a@
@¯E  ˙       Å@ @ @×@¯  ˙       @ Ã@ @@Žē  ˙       c@ ķ@ ˙@b@Žô  ˙       2@#@@'@Ž.  ˙       @S@@ ė@Žh  ˙      @c@@ Ŧ@Žĸ  ˙      @2ĩ@ Ô@6 r@ŽÜ  ˙      @į@ Ĩ@f 8@Ž  ˙      é@PPP wABP  ˙      Që QA8  ˙      @Ā .@@Ä  ˙      Ü@Ā ^@ũ  ˙      Ŧ@Ā @ũ  ˙      |@Ā Ã@RōP  ˙      L@Āå@	  ˙      @Ā@  ˙       ė@ĀB@  ˙       æ@r@  ˙       æ@fĄ@   ˙      W˛ @ 1  ˙      ¯@ @ 0  ˙      Ž@ @ /  ˙      ­@	 @ .  ˙      Ž@ @ .  ˙      Ž@ @ -  ˙      ¯@ @ ,  ˙      W˛ @ ,  ˙      @&<@   ˙      @&=@   ˙      @&>@  ˙      @&?@  ˙ 
   
  @&QA  ˙ 
   
  ŧ@?  ˙ 	   	   Gũ  ˙      @ @@/  ˙      @ @@0  ˙      @ @@P+  ˙      @ @@+  ˙      @ ?@+  ˙      @ @@+  ˙      @ ?@+  ˙      @ ?@,  ˙      @ @@.  ˙      @ ?@
0  ˙      @ >@
2  ˙      @ >@	3  ˙      @ ?@5  ˙      @ ?@6  ˙      @ @@7  ˙      @ ?@8  ˙      ~@ ?@9  ˙      ~@ >@:  ˙      }@ ?@;  ˙      |@ @@<