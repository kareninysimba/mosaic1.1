  �V  #@  PL$TITLE = "OBJECT"
$CTIME = 887396587
$MTIME = 887396587
$LIMTIME = 0
$MINPIXVAL = 0.
$MAXPIXVAL = 0.
NEXTEND =                    4 / Number of extensions
FILENAME= 'dflatJ0461.fits'    / Original host filename
OBSTYPE = 'object  '           / Observation type
RADECSYS= 'FK5     '           / Default coordinate system
RADECEQ =                2000. / Default equinox
RA      = '5:26:51.79'         / RA of observation (hr)
DEC     = '-17:00:00.0'        / DEC of observation (deg)
OBJRA   = '5:26:51.79'         / right ascension [HH:MM:SS.SS]
OBJDEC  = '-17:00:00.0'        / declination [DD:MM:SS.S]
OBJEPOCH=               2007.8 / epoch [YYYY]

TIMESYS = 'UTC approximate'    / Time system
DATE-OBS= '2007-10-31T10:15:55.0' / Date of observation start (UTC)
TIME-OBS= '10:15:55'           / universal time [HH:MM:SS]
MJD-OBS =       54404.42771991 / MJD of observation start
ST      = '5:26:53 '           / sidereal time [HH:MM:SS]

OBSERVAT= 'KPNO    '           / Observatory
TELESCOP= 'KPNO 4.0 meter telescope' / Telescope
TELRADEC= 'FK5     '           / Telescope coordinate system
TELEQUIN=               2007.8 / Equinox of tel coords
TELRA   = '5:26:51.79'         / RA of telescope (hr)
TELDEC  = '-17:00:00.0'        / DEC of telescope (deg)
HA      = '0:00:00.13'         / telescope ha [No Units]
ZD      =              48.9625 / Zenith distance
AIRMASS =                1.521 / Airmass
TELFOCUS=                10975 / Telescope focus

DETECTOR= 'NEWFIRM '           / Mosaic detector
MOSSIZE = '[1:4096,1:4096]'    / Mosaic detector size
NDETS   =                    4 / Number of detectors in mosaic
FILTER  = 'J       '           / Filter name(s)

OBSERVER= 'Hal_Halbedel'       / Observer(s)
PROPOSER= 'Ron_Probst'         / Proposer(s)
PROPID  = '2007A-TnE'          / Proposal identification
OBSID   = 'kp4m.20071031T101555' / Observation ID
SEQID   = 'NFQR_2007ATnE_0027' / Sequence ID
SEQNUM  =                   27 / Sequence Number
EXPID   =                    0 / Monsoon exposure ID
NOCID   =      2454405.1359522 / NEWFIRM ID

NOHS    = '1.1.1   '           / NOHS ID [No Units]
NOCNO   =                    1 / observation number in this sequence [No Units]
NOCUTC  =             20071031 / NOCUTC ID [YYYYMMDD]
NOCRSD  =      2454405.1215835 / recipe date [MSD]
NOCORA  =                    0 / RA offset [Arcseconds]
NOCDITER=                    0 / dither iteration count [No Units]
NOCSKY  =                    0 / sky offset modulus [No Units]
NOCDPAT = '5PX     '           / dither pattern [No Units]
NOCGID  =      2454405.1215835 / group identification number [MSD]
NOCDROF =                    0 / dither RA offset [Arcseconds]
NOCDHS  = 'STARE   '           / DHS script name [No Units]
NOCNAME = 'dflatJ5s.fits'      / file name for data set [No Units]
NOCMPOS =                    0 / map position [No Units]
NOCTIM  =                    5 / requested integration time [Seconds]
NOCMDOF =                    0 / map Dec offset [Arcminutes]
NOCMREP =                    0 / map repetition count [No Units]
NOCTOT  =                    5 / total number of observations in set [No Units]
NOCFOCUS=                11200 / ntcs_focus value [micron]
NOCSYS  = 'kpno_4m '           / system ID [No Units]
NOCLAMP = 'off     '           / dome flat lamp status (on|off)
NOCDPOS =                    0 / dither position [No Units]
NOCMITER= 'arcminutes'         / map iteration count [No Units]
NOCID   =      2454405.1359522 / observation ID a.k.a expID [MSD]
NOCODEC =                    0 / Dec offset [Arcseconds]
NOCMPAT = '5PX     '           / map pattern [No Units]
NOCDDOF =                    0 / dither Dec offset [Arcseconds]
NOCNUM  =                    5 / observation number request [No Units]
NOCMROF =                    0 / map RA offset [Arcminutes]
NOCTYP  = 'OBJECT  '           / observation type [No Units]
NOCDREP =                    0 / dither repetition count [No Units]

NFOSSTMP=            64.988998 / oss temp measured [K]
NFDETTMP=                   30 / detector array temp measured [K]

NFFILPOS= 'J       '           / detected position name [No Units]
NFFW2POS=                    1 / wheel 2 actual position (0|1|2|3|4|5|6|7|8) [No
NFFW1POS=                    8 / wheel 1 actual position (0|1|2|3|4|5|6|7|8) [No

DECDIFF =                    0 / dec diff [Arcsecond]
AZ      = '180:00:02.4'        / telescope azimuth [DD:MM:SS]
DECOFF  =                    0 / dec offset [Arcsecond]
RAOFF   =                    0 / ra offset [Arcsecond]
RAINDEX =              -221.89 / ra index [Arcsecond]
ALT     = '41:02:15.0'         / telescope altitude [HH:MM:SS]
RAZERO  =                33.62 / ra zero [Arcsecond]
RADIFF  =                    0 / ra diff [Arcsecond]
DECZERO =                91.12 / dec zero [Arcsecond]
DECINDEX=                   35 / dec index [Arcsecond]

DOMEERR =              -135.32 / dome error as distance from target [Degrees]
DOMEAZ  =                359.3 / dome position [Degrees]

NFC1POS = '00:00:00.00 00:00:00.0 2007 [s]' / camera 1 target in RA Dec Epoch [H
NFC1GDR = 'off [s] '           / camera 1 guider mode [No Units]
NFC1FILT= '0 [s]   '           / camera 1 filter [No Units]

TCPGDR  = 'off     '           / guider status (on|off) [No Units]

NFC2FILT= '0 [s]   '           / camera 2 filter [No Units]
NFC2POS = '00:00:00.00 00:00:00.0 2007 [s]' / camera 2 target in RA Dec Epoch [H
NFC2GDR = 'off [s] '           / camera 2 guider mode [No Units]

NFECPOS = 'open    '           / detected position (open|close|between) [No Unit
PROCMEAN=             1973.472
PCOUNT  =                39156 / Heap size in bytes
GCOUNT  =                    1 / Only one group
ORIGIN  = 'NOAO-IRAF FITS Image Kernel July 2003' / FITS file originator
EXTNAME = 'im2     '           / Extension name
EXTVER  =                    2 / Extension version
INHERIT =                    T / Inherits global header
DATE    = '2008-02-13T19:49:14' / Date FITS file was generated
IRAF-TLM= '12:49:13 (13/02/2008)' / Time of last modification
OBJECT  = 'OBJECT  '           / Name of the object observed
IMAGEID =                    2 / Image identification
EXPTIME =                    5 / Actual integration time in seconds
MJDSTART=        54404.6359817 / MJD of observation start
MJDEND  =        54404.6360396 / MJD of observation end

DETNAME = 'Raytheon InSb #2 (NOAO 2)' / Array name
DETSIZE = '[1:2046,1:2046]'    / Detector size
DETSEC  = '[2047:4092,1:2046]' / Detector section

LTM1_1  =                  1.0 / Detector to image transformation
LTM2_2  =                  1.0 / Detector to image transformation

WCSASTRM= 'image1399 (USNO N J) by F. Valdes 2007-06-29' / WCS Source
EQUINOX =                2000. / Equinox of WCS
WCSDIM  =                    2 / WCS dimensionality
CTYPE1  = 'RA---TNX'           / Coordinate type
CTYPE2  = 'DEC--TNX'           / Coordinate type
CRVAL1  =      81.715791666667 / Coordinate reference value
CRVAL2  =                -17.0 / Coordinate reference value
CRPIX1  =     -29.163260286592 / Coordinate reference pixel
CRPIX2  =      2162.2692847543 / Coordinate reference pixel
CD1_1   = -5.1147689850518e-07 / Coordinate matrix
CD2_1   =  0.00010987882059486 / Coordinate matrix
CD1_2   = -0.00010985960236195 / Coordinate matrix
CD2_2   =   3.272714354659e-07 / Coordinate matrix
WAT0_001= 'system=image'       / Coordinate system
WAT1_001= 'wtype=tnx axtype=ra lngcor = "1. 4. 4. 1. 0.01171189932174763 0.2374'
WAT1_002= '207564093686 0.002606981764336732 0.2279784399663589 -1.401604116952'
WAT1_003= '021E-4 -3.936577357846626E-5 -2.976925006198533E-4 2.239952792040140'
WAT1_004= 'E-6 8.680333404602644E-5 -1.837781850368252E-4 1.517676062202822E-4 '
WAT1_005= '2.513111858786038E-5 -4.188598056252738E-5 -7.136997948722332E-6 3.5'
WAT1_006= '84997215855048E-5 4.493177238217266E-6 2.619251517739210E-5 2.782258'
WAT1_007= '896691837E-5 -7.193441255813327E-6 -4.343861370939314E-6 "'
WAT2_001= 'wtype=tnx axtype=dec latcor = "1. 4. 4. 1. 0.01171189932174763 0.237'
WAT2_002= '4207564093686 0.002606981764336732 0.2279784399663589 -1.12172936271'
WAT2_003= '3350E-4 6.366732306022806E-5 -3.440491012919562E-5 3.128928605225737'
WAT2_004= 'E-5 -2.699644307419462E-5 -2.027187570974867E-4 -7.877716146459297E-'
WAT2_005= '6 3.017602406323044E-5 -2.768789902917035E-4 1.593048134610645E-4 3.'
WAT2_006= '330658702881534E-5 -5.471784519107507E-6 2.698404181058821E-6 2.3638'
WAT2_007= '36933637852E-5 1.268344218130272E-5 3.244156533024538E-6 "'

PROC0001= 'Oct 31 12:56 Trim $I'
PROC0002= 'Oct 31 12:56 trimsec = [2:2047,2:2047]'
PROCDONE= 'T       '
PROCAVG =              1902.85
PROCSIG =               148.84
PROCID  = 'kp4m.20071031T101555V1'
PROCID01= 'kp4m.20071031T101555'
PROCID02= 'kp4m.20071031T101604'
PROCID03= 'kp4m.20071031T101613'
PROCID04= 'kp4m.20071031T101622'
PROCID05= 'kp4m.20071031T101634'
IMCMB001= '        '
IMCMB002= '        '
IMCMB003= '        '
IMCMB004= '        '
IMCMB005= '        '
NCOMBINE=                    5
BPM     = 'bpm071031_im2.pl'
   �     �  �                 L�  L�  �  �  PL      ���               ���      ````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````` @`` @ @ @" @ @, @ @ @` @ @ @
 @ @ @ @: @/` @` @` @` @` @` @4 @: @; @: @``````` @ @ @ @` @ @ @ @``````` @ @: @: @`````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````` @```````` @ @`````` @ @` @```````` @ @# @ @$ @ @% @ @& @
 @ @! @1 @1 @1 @1`   ��      �  ��       @ @Q&PaP�PFP?P�PP� �@PyP� �@ |  ��       @ @Q&PaP�PFP?P�PP� �@PwP� �@ ~  ��       @ @Q'PaP�PFP?P�PP� �@PuP� �@ �  ��       @ @Q'PaP�PFP?P�PP� �@PtP� �@ �  ��       @ @Q'PaP�PFP?P�PP� �@PrP� �@ �  ��       @ @Q'PaP�PFP?P�PP�@PpP� �@ �  ��       @ @Q(PaP�PFP?P�PP�@PoP� �@ �  ��       @ @Q(PaP�PFP?P�PP�@PmP� �@ �  ��       @ @Q(PaP�PFP?P�PP�@PkP� �@ �  ��       @ @Q(PaP�PFP?P�PP�@PjP� �@ �  ��       @ @Q)PaP�PFP?P�PP�	@PhP� �@ �  ��       @ @Q)PaP�PFP?P�PP�@PfP� �@ �  ��       @ @Q)PaP�PFP?P�PP�@PeP� �@ �  ��       @ @Q)PaP�PFP?P�PP�@PcP� �@ �  ��       @ @Q*PaP�PFP?P�PP�@PaP� �@ �  ��       @ @Q*PaP�PFP?P�PP�@P`P� �@ �  ��       @ @Q*PaP�PFP?P�PP�@P^P� �@ �  ��       @ @Q*PaP�PFP?P�PP�@P\P� �@ �  ��       @ @Q*PaP�PFP?P�PP�@P[P� �@ �  ��       @ @Q+PaP�PFP?P�PP�@PYP� �@ �  ��       @ @Q,PaP�PFP?P�PP�@PWP� �@ �  ��       @Q<PaP�PFP?P�PP�@PVP� �@ �  ��       @Q<PaP�PFP?P�PP�@PTP� �@ �  ��       @Q<PaP�PFP?P�PP�@PRP� �@ �  ��       @Q<PaP�PFP?P�PP� @PQP� �@ �  ��       @Q<PaP�PFP?P�PP�"@POP� �@ �  ��       @Q<PaP�PFP?P�PP�$@PMP� �@ �  ��       @Q<PaP�PFP?P�PP�%@PLP� �@ �  ��       @Q<PaP�PFP?P�PP�'@PJP� �@ �  ��       @Q<PaP�PFP?P�PP�)@PHP� �@ �  ��       @Q<PaP�PFP?P�PP�*@PGP� �@ �  ��       @Q<PaP�PFP?P�PP�,@PEP� �@ �  ��       @Q<PaP�PFP?P�PP�.@PCP� �@ �  ��       @Q<PaP�PFP?P�PP�/@PBP� �@ �  ��       @Q<PaP�PFP?P�PP�1@P@P� �@ �  ��       @Q<PaP�PFP?P�PP�3@P>P� �@ �  ��       @Q<PaP�PFP?P�PP�4@P=P� �@ �  ��       @Q<PaP�PFP?P�PP�6@P;P� �@ �  ��       @Q<PaP�PFP?P�PP�8@P9P� �@ �  ��       @Q<PaP�PFP?P�PP�9@P8P� �@ �  ��       @Q<PaP�PFP?P�PP�;@P6P� �@ �  ��       @Q<PaP�PFP?P�PP�=@P4P� �@ �  ��       @Q<PaP�PFP?P�PP�>@P3P� �@ �  ��       @Q<PaP�PFP?P�PP�@@P1P� �@ �  ��       @Q<PaP�PFP?P�PP�B@P/P� �@ �  ��       @Q<PaP�PFP?P�PP�C@P.P� �@ �  ��       @Q<PaP�PFP?P�PP�E@P,P� @ �  ��       @Q<PaP�PFP?P�PP�G@P*P� }@ �  ��       @Q<PaP�PFP?P�PP�H@P)P� {@ �  ��       @Q<PaP�PFP?P�PP�J@P'P� y@ �  ��       @Q<PaP�PFP?P�PP�L@P%P� x@ �  ��       @Q<PaP�PFP?P�PP�M@P$P� v@ �  ��       @Q<PaP�PFP?P�PP�O@P"P� t@ �  ��       @Q<PaP�PFP?P�PP�Q@P P� r@ �  ��       @Q<PaP�PFP?P�PP�R@PP� p@ �  ��       @Q<PaP�PFP?P�PP�T@PP� n@ �  ��       @Q<PaP�PFP?P�PP�V@PP� m@ �  ��       @Q<PaP�PFP?P�PP�W@PP� k@ �  ��       @Q<PaP�PFP?P�PP�Y@PP� i@ �  ��       @Q=PaP�PFP?P�PP�[@PP� g@ �  ��       @Q=PaP�PFP?P�PP�\@PP� e@ �  ��       @Q=PaP�PFP?P�PP�^@PP� c@ �  ��       @Q=PaP�PFP?P�PP�`@PP� b@ �  ��       @Q=PaP�PFP?P�PP�a@PP� `@ �  ��       @Q=PaP�PFP?P�PP�c@PP� ^@ �  ��       @Q=PaP�PFP?P�PP�e@PP� \@ �  ��       @Q=PaP�PFP?P�PP�f@PP� Z@ �  ��       @Q=PaP�PFP?P�PP�h@P	P� Y@ �  ��       @Q=PaP�PFP?P�PP�j@PP� W@ �  ��       @Q=PaP�PFP?P�PP�k@PP� U@ �  ��       @Q=PaP�PFP?P�PP�m@PP� S@ �  ��       @Q=PaP�PFP?P�PP�o@PP� Q@ �  ��       @Q=PaP�PFP?P�PP�QJ &@P� O@   ��       @Q=PaP�PFP?P�PP�G@ &@P� N@  ��       @Q=PaP�PFP?P�PP�F@ '@P� L@  ��       @Q=PaP�PFP?P�PP�F@ (@P� J@  ��       @Q=PaP�PFP?P�PP�E@	P( @P� H@  ��       @Q=PaP�PFP?P�PP�F@P) @P� F@	  ��       @Q=PaP�PFP?P�PP�F@P) @P� D@  ��       @Q=PaP�PFP?P�PP�G@P* @P� C@  ��       @Q=PaP�PFP?P�PP�QJP, @P� A@  ��       @Q=PaP�PFP?P�PP�Qv 	@P� ?@  ��       @Q=PaP�PFP?P�PP�Qv @P� =@  ��       @Q=PaP�PFP?P�PP�Qv @P� ;@  ��       @Q=PaP�PFP?P�PP�Qv @P� 9@  ��       @Q=PaP�PFP?P�PP�Qv @P� 8@  ��       @Q=PaP�PFP?P�PP�Qv @P� 6@  ��       @Q=PaP�PFP?P�PP�Qv @P� 4@  ��       @Q=PaP�PFP?P�PP�Qv @P� 2@  ��       @Q=PaP�PFP?P�PP�Qv @P� 0@  ��       @Q=PaP�PFP?P�PP�Qv @P� .@!  ��       @Q=PaP�PFP?P�PP�Qv @P� -@#  ��       @Q=PaP�PFP?P�PP�Qv @P� +@%  ��       @Q=PaP�PFP?P�PP�Qv @P� )@&  ��       @Q=PaP�PFP?P�PP�Qv @P� '@(  ��       @Q=PaP�PFP?P�PP�Qv !@P� %@*  ��       @Q=PaP�PFP?P�PP�Qv "@P� #@,  ��       @Q=PaP�PFP?P�PP�Qv $@P� "@.  ��       @Q=PaP�PFP?P�PP�Qv &@P�  @0  ��       @Q=PaP�PFP?P�PP�Qv '@P� @1  ��       @Q=PaP�PFP?P�PP�Qv )@P� @3  ��       @Q=PaP�PFP?P�PP�Qv +@P� @5  ��       @Q=PaP�PFP?P�PP�Qv ,@P� @7  ��       @Q=PaP�PFP?P�PP�Qv .@P� @9  ��       @Q=PaP�PFP?P�PP�Qv 0@P� @:  ��       @Q=PaP�PFP?P�PP�Qv 1@P� @<  ��       @Q=PaP�PFP?P�PP�Qv 3@P @>  ��       @Q=PaP�PFP?P�PP�Qv 5@P} @@  ��       @Q=PaP�PFP?P�PP�Qv 6@P| @B  ��       @Q=PaP�PFP?P�PP�Qv 8@Pz @D  ��       @Q=PaP�PFP?P�PP�Qv :@Px 
@E  ��       @Q=PaP�PFP?P�PP�Qv ;@Pw @G  ��       @Q=PaP�PFP?P�PP�Qv =@Pu @I  ��       @Q=PaP�PFP?P�PP�Qv ?@Ps @K  ��       @Q=PaP�PFP?P�PP�Qv @@Pr @M  ��       @Q=PaP�PFP?P�PP�Qv B@Pp @O  ��       @Q=PaP�PFP?P�PP�Qv D@ m@P  ��       @Q>PaP�PFP?P�PP�Qv E@ j@R  ��       @Q>PaP�PFP?P�PP�Qv G@ f@T  ��       @Q>PaP�PFP?P�PP�Qv I@ b@U  ��       @Q>PaP�PFP?P�PP�Qv J@ _@PU  ��       @Q>PaP�PFP?P�PP�Qv L@ \@PU  ��       @Q>PaP�PFP?P�PP�Qv N@ X@PU  ��       @Q>PaP�PFP?P�PP�Qv O@ U@PU  ��       @Q>PaP�PFP?P�PP�Qv Q@ Q@P
U  ��       @Q>PaP�PFP?P�PP�Qv S@ M@PU  ��       @Q>PaP�PFP?P�PP�Qv T@ J@PU  ��       @Q>PaP�PFP?P�PP�Qv V@ G@PU  ��       @Q>PaP�PFP?P�PP�Qv X@ C@PU  ��       @Q>PaP�PFP?P�PP�Qv Y@ @@PU  ��       @Q>PaP�PFP?P�PP�Qv [@ <@PU  ��       @Q>PaP�PFP?P�PP�Qv ]@ 8@PU  ��       @Q>PaP�PFP?P�PP�Qv ^@ 5@PU  ��       @Q>PaP�PFP?P�PP�Qv `@ 2@PU  ��       @Q>PaP�PFP?P�PP�Qv b@ .@PU  ��       @Q>PaP�PFP?P�PP�Qv c@ +@PU  ��       @Q>PaP�PFP?P�PP�Qv e@ '@P U  ��       @Q>PaP�PFP?P�PP�Qv g@ #@P"U  ��       @Q>PaP�PFP?P�PP�Qv h@  @P#U  ��       @Q>PaP�PFP?P�PP�Qv j@ @P%U  ��       @Q>PaP�PFP?P�PP�Qv l@ @P'U  ��       @Q>PaP�PFP?P�PP�Qv m@ @P)U  ��       @Q>PaP�PFP?P�PP�Qv o@ @P+U  ��       @Q>PaP�PFP?P�PP�Qv q@ @P-U  ��       @Q>PaP�PFP?P�PP�Qv r@ @P.U  ��       @Q>PaP�PFP?P�PP�Qv t@ @ @P0U  ��       @Q>PaP�PFP?P�PP�Qv v@P2U  ��       @Q>PaP�PFP?P�PP�Qv w@P4U  ��       @Q>PaP�PFP?P�PP�Qv y@	P5U  ��       @Q>PaP�PFP?P�PP�Qv z@P5U  ��       @Q>PaP�PFP?P�PP�Qv z@P7U  ��       @Q>PaP�PFP?P�PP�Qv {@P7U  ��       @Q>PaP�PFP?P�PP�Qv |@P8U  ��       @Q?PaP�PFP?P�PP�Qv |@P8U " ��       @Q?PaP�PFP?P�PP�Qv {@P9U  ��       @Q@PaP�PFP?P�PP�Qv {@P9U , ��       @Q@PaP�PFP?P�PP�Qv z@P:U  ��       @QAPaP�PFP?P�PP�Qv z@P:U  ��       @QAPaP�PFP?P�PP�Qv y@P;U  ��       @QAPaP�PFP?P�PP� �@P~ y@P;U  ��       @QAPaP�PFP?P�PP� �@Pu y@P;U  ��       @QAPaP�PFP?P�PP� �@	Pu y@P;U  ��       @QAPaP�PFP?P�PP� �@	Pu x@P<U 
 ��       @QBPaP�PFP?P�PP� �@	Pu x@P<U  ��       @QBPaP�PFP?P�PP�Qv x@P<U  ��       @QBPaP�PFP?P�PP�Qv w@P=U  ��       @QBPaP�PFP?P�PP�QvP�U : ��       @QCPaP�PFP?P�PP�QvP�U / ��       @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QDPaP�PFP?P�PP�QvP�U  ��      @ @QEPaP�PFP?P�PP�QvP�U  ��      @ @QEPaP�PFP?P�PP�QvP�U  ��      @ @QEPaP�PFP?P�PP�QvP�U  ��      @ @QEPaP�PFP?P�PP�QvP�U 4 ��       @QEPaP�PFP?P�PP�QvP�U : ��       @QFPaP�PFP?P�PP�QvP�U ; ��       @QGPaP�PFP?P�PP�QvP�U : ��       @QHPaP�PFP?P�PP�QvP�U  ��       @QIPaP�PFP?P�PP�QvP�U  ��       @QIPaP�PFP?P�PP�Qv �@O  ��       @QIPaP�PFP?P�PP�Qv �@M  ��       @QIPaP�PFP?P�PP�Qv �@K  ��       @QIPaP�PFP?P�PP�Qv �@I  ��       @QIPaP�PFP?P�PP�Qv �@H  ��       @QIPaP�PFP?P�PP�Qv �@G  ��       @QIPaP�PFP?P�PP�Qv �@F  ��       @QIPaP�PFP?P�PP�Qv �@!E  ��       @QIPaP�PFP?P�PP�Qv �@#D  ��       @QIPaP�PFP?P�PP�Qv �@%C  ��       @QIPaP�PFP?P�PP�Qv �@'B  ��       @QIPaP�PFP?P�PP�Qv �@)A  ��       @QIPaP�PFP?P�PP�Qv �@'B  ��       @QIPaP�PFP?P�PP�Qv �@%C  ��       @QIPaP�PFP?P�PP�Qv �@#D  ��       @QIPaP�PFP?P�PP�Qv �@!E  ��       @QIPaP�PFP?P�PP�Qv �@F  ��       @QIPaP�PFP?P�PP�Qv �@G  ��       @QIPaP�PFP?P�PP�Qv �@H  ��       @QIPaP�PFP?P�PP�Qv �@I  ��       @QIPaP�PFP?P�PP�Qv �@K  ��       @QIPaP�PFP?P�PP�Qv �@M  ��       @QIPaP�PFP?P�PP�Qv �@O  ��       @QIPaP�PFP?P�PP�QvP�U : ��       @QJPaP�PFP?P�PP�QvP�U : ��       @QKPaP�PFP?P�PP�QvP�U  ��       @QLPaP�PFP?P�PP�QvP�U  ��       @QLPaP�PFP?P�P �@PQvP�U  ��       @QLPaP�PFP?P�P �@PQvP�U  ��       @QLPaP�PFP?P�P �@PQvP�U  ��       @QLPaP�PFP?P�P �@PQvP�U  ��       @QLPaP�PFP?P�P �@PQvP�U  ��       @QLPaP�PFP?P�P �@PQvP�U  ��       @QLPaP�PFP?P�P �@P QvP�U  ��       @QLPaP�PFP?P�P �@P"QvP�PU  ��       @QLPaP�PFP?P�P �@P$Qv �@	PU  ��       @QLPaP�PFP?P�P �@P&Qv �@PU  ��       @QLPaP�PFP?P�P �@P(Qv �@PU  ��       @QLPaP�PFP?P�P �@P)Qv �@PU  ��       @QLPaP�PFP?P�P @P+Qv �@PU  ��       @QLPaP�PFP?P�P ~@P-Qv �@PU  ��       @QLPaP�PFP?P�P |@P/Qv �@PU  ��       @QLPaP�PFP?P�P z@P1Qv �@PU  ��       @QLPaP�PFP?P�P x@P2Qv �@PU  ��       @QLPaP�PFP?P�P v@P4Qv �@PU  ��       @QLPaP�PFP?P�P t@P6Qv �@PU  ��       @QLPaP�PFP?P�P s@P8Qv �@PU  ��       @QLPaP�PFP?P�P q@P:Qv �@PU  ��       @QLPaP�PFP?P�P o@P<Qv �@PU  ��       @QLPaP�PFP?P�P m@P=Qv �@PU  ��       @QLPaP�PFP?P�P k@P?Qv �@PU  ��       @QLPaP�PFP?P�P j@PAQv �@PU  ��       @QLPaP�PFP?P�P h@PCQv �@PU  ��       @QLPaP�PFP?P�P f@PEQv �@PU  ��       @QLPaP�PFP?P�P d@PFQv �@PU  ��       @QLPaP�PFP?P�P b@PHQv �@PU  ��       @QLPaP�PFP?P�P `@PJQv �@PU  ��       @QLPaP�PFP?P�P _@PLQv �@	PU  ��       @QLPaP�PFP?P�P ]@PNQvP�PU  ��       @QLPaP�PFP?P�P [@PPQvP�U  ��       @QLPaP�PFP?P�P Y@PQQvP�U  ��       @QLPaP�PFP?P�P W@PSQvP�U  ��       @QLPaP�PFP?P�P V@PUQvP�U  ��       @QLPaP�PFP?P�P T@PWQvP�U  ��       @QLPaP�PFP?P�P R@PYQvP�U  ��       @QLPaP�PFP?P�P P@PZQvP�U  ��       @QLPaP�PFP?P�P N@P\QvP�U  ��       @QLPaP�PFP?P�P L@P^QvP�U  ��       @QLPaP�PFP?P�P K@P`QvP�U  ��       @QLPaP�PFP?P�P I@PbQvP�U  ��       @QLPaP�PFP?P�P G@PdQvP�U  ��       @QLPaP�PFP?P�P E@PeQvP�U  ��       @QMPaP�PFP?P�P C@PgQvP�U  ��       @QMPaP�PFP?P�P B@PiQvP�U  ��       @QMPaP�PFP?P�P @@PkQvP�U  ��       @QMPaP�PFP?P�P >@PmQvP�U  ��       @QMPaP�PFP?P�P <@PnQvP�U  ��       @QMPaP�PFP?P�P :@PpQvP�U  ��       @QMPaP�PFP?P�P 8@PrQvP�U  ��       @QMPaP�PFP?P�P 7@PtQvP�U  ��       @QMPaP�PFP?P�P 5@PvQvP�U  ��       @QMPaP�PFP?P�P 3@PxQvP�U  ��       @QMPaP�PFP?P�P 1@PyQvP�U  ��       @QMPaP�PFP?P�P /@P{QvP�U  ��       @QMPaP�PFP?P�P .@P}QvP�U  ��       @QMPaP�PFP?P�P ,@PQvP�U  ��       @QMPaP�PFP?P�P *@P�QvP�U  ��       @QMPaP�PFP?P�P (@P�QvP�U  ��       @QMPaP�PFP?P�P &@P�QvP�U  ��       @QMPaP�PFP?P�P $@P�QvP�U  ��       @QMPaP�PFP?P�P #@P�QvP�U  ��       @QMPaP�PFP?P�P !@P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P 	@P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P�P @P�QvP�U  ��       @QMPaP�PFP?P� @P�QvP�U  ��       @QMPaP�PFP?P� @P�QvP�U  ��       @QMPaP�PFP?P� @P�QvP�U  ��       @QMPaP�PFP?P� @P�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @P	P�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� 	@PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP?P� @PP�QvP�U  ��       @QMPaP�PFP? �@PP�QvP�U  ��       @QMPaP�PFP? �@PP�QvP�U  ��       @QMPaP�PFP? �@PP�QvP�U  ��       @QMPaP�PFP? �@PP�QvP�U  ��       @QMPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@P	PP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@PPP�QvP�U  ��       @QNPaP�PFP? �@P!PP�QvP�U  ��       @QNPaP�PFP? �@P#PP�QvP�U  ��       @QNPaP�PFP? �@P$PP�QvP�U  ��       @QNPaP�PFP? �@P&PP�QvP�U  ��       @QNPaP�PFP? �@P(PP�QvP�U  ��       @QNPaP�PFP? �@P*PP�QvP�U  ��       @QNPaP�PFP? ~@P,PP�QvP�U  ��       @QNPaP�PFP? |@P-PP�QvP�U  ��       @QNPaP�PFP? z@P/PP�QvP�U  ��       @QNPaP�PFP? x@P1PP�QvP�U  ��       @QNPaP�PFP? w@P3PP�QvP�U  ��       @QNPaP�PFP? u@P5PP�QvP�U  ��       @QNPaP�PFP? s@P7PP�QvP�U  ��       @QNPaP�PFP? q@P8PP�QvP�U  ��       @QNPaP�PFP? o@P:PP�QvP�U  ��       @QNPaP�PFP? n@P<PP�QvP�U  ��       @QNPaP�PFP? l@P>PP�QvP�U  ��       @QNPaP�PFP? j@P@PP�QvP�U  ��       @QNPaP�PFP? h@PAPP�QvP�U  ��       @QNPaP�PFP? f@PCPP�QvP�U  ��       @QNPaP�PFP? d@PEPP�QvP�U  ��       @QNPaP�PFP? c@PGPP�QvP�U  ��       @QNPaP�PFP? a@PIPP�QvP�U  ��       @QNPaP�PFP? _@PKPP�QvP�U  ��       @QNPaP�PFP? ]@PLPP�QvP�U  ��       @QNPaP�PFP? [@PNPP�QvP�U  ��       @QNPaP�PFP? Z@PPPP�QvP�U  ��       @QNPaP�PFP? X@PRPP�QvP�U  ��       @QNPaP�PFP? V@PTPP�QvP�U  ��       @QNPaP�PFP? T@PUPP�QvP�U  ��       @QNPaP�PFP? R@PWPP�QvP�U  ��       @QNPaP�PFP? P@PYPP�QvP�U  ��       @QNPaP�PFP? O@P[PP�QvP�U  ��       @QNPaP�PFP? M@P]PP�QvP�U  ��       @QNPaP�PFP? K@P_PP�QvP�U  ��       @QNPaP�PFP? I@P`PP�QvP�U  ��       @QNPaP�PFP? G@PbPP�QvP�U  ��       @QNPaP�PFP? F@PdPP�QvP�U  ��       @QNPaP�PFP? D@PfPP�QvP�U  ��       @QNPaP�PFP? B@PhPP�QvP�U  ��       @QNPaP�PFP? @@PiPP�QvP�U  ��       @QNPaP�PFP? >@PkPP�QvP�U  ��       
@QOPaP�PFP? =@PmPP�QvP�U  ��       
@QOPaP�PFP? ;@PoPP�QvP�U  ��       
@QOPaP�PFP? 9@PqPP�QvP�U  ��       
@QOPaP�PFP? 7@PrPP�QvP�U  ��       
@QOPaP�PFP? 5@PtPP�QvP�U  ��       
@QOPaP�PFP? 3@PvPP�QvP�U  ��       
@QOPaP�PFP? 2@PxPP�QvP�U  ��       
@QOPaP�PFP? 0@PzPP�QvP�U  ��       
@QOPaP�PFP? .@P|PP�QvP�U  ��       
@QOPaP�PFP? ,@P}PP�QvP�U  ��       
@QOPaP�PFP? *@PPP�QvP�U  ��       
@QOPaP�PFP? )@P�PP�QvP�U  ��       
@QOPaP�PFP? '@P�PP�QvP�U  ��       
@QOPaP�PFP? %@P�PP�QvP�U  ��       
@QOPaP�PFP? #@P�PP�QvP�U  ��       
@QOPaP�PFP? !@P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? 
@P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PFP? @P�PP�QvP�U  ��       
@QOPaP�PF >@P�PP�QvP�U  ��       
@QOPaP�PF <@P�PP�QvP�U  ��       
@QOPaP�PF :@P�PP�QvP�U  ��       
@QOPaP�PF 8@P�PP�QvP�U  ��       
@QOPaP�PF 6@PP�PP�QvP�U  ��       
@QOPaP�PF 5@PP�PP�QvP�U  ��       
@QOPaP�PF 3@PP�PP�QvP�U  ��       
@QOPaP�PF 1@P	P�PP�QvP�U  ��       
@QOPaP�PF /@P
P�PP�QvP�U  ��       
@QOPaP�PF -@PP�PP�QvP�U  ��       
@QOPaP�PF ,@PP�PP�QvP�U  ��       
@QOPaP�PF *@PP�PP�QvP�U  ��       
@QOPaP�PF (@PP�PP�QvP�U  ��       
@QOPaP�PF &@PP�PP�QvP�U  ��       
@QOPaP�PF $@PP�PP�QvP�U  ��       
@QOPaP�PF "@PP�PP�QvP�U  ��       
@QOPaP�PF !@PP�PP�QvP�U  ��       
@QOPaP�PF @PP�PP�QvP�U  ��       
@QOPaP�PF @PP�PP�QvP�U  ��       
@QOPaP�PF @PP�PP�QvP�U  ��       
@QOPaP�PF @P P�PP�QvP�U  ��       
@QOPaP�PF @P"P�PP�QvP�U  ��       
@QOPaP�PF @P$P�PP�QvP�U  ��       
@QOPaP�PF @P&P�PP�QvP�U  ��       	@QPPaP�PF @P'P�PP�QvP�U  ��       	@QPPaP�PF @P)P�PP�QvP�U  ��       	@QPPaP�PF @P+P�PP�QvP�U  ��       	@QPPaP�PF @P-P�PP�QvP�U  ��       	@QPPaP�PF @P/P�PP�QvP�U  ��       	@QPPaP�PF 	@P1P�PP�QvP�U  ��       	@QPPaP�PF @P2P�PP�QvP�U  ��       	@QPPaP�PF @P4P�PP�QvP�U  ��       	@QPPaP�PF @P6P�PP�QvP�U  ��       	@QPPaP�PF @P8P�PP�QvP�U  ��       	@QPPaP� E@P:P�PP�QvP�U  ��       	@QPPaP� D@P;P�PP�QvP�U  ��       	@QPPaP� B@P=P�PP�QvP�U  ��       	@QPPaP� @@P?P�PP�QvP�U  ��       	@QPPaP� ?@PP?P�PP�QvP�U  ��       	@QPPaP� =@PP?P�PP�QvP�U  ��       	@QPPaP� ;@PP?P�PP�QvP�U  ��       	@QPPaP� 9@PP?P�PP�QvP�U  ��       	@QPPaP� 7@P	P?P�PP�QvP�U  ��       	@QPPaP� 6@PP?P�PP�QvP�U  ��       	@QPPaP� 4@PP?P�PP�QvP�U  ��       	@QPPaP� 2@PP?P�PP�QvP�U  ��       	@QPPaP� 0@PP?P�PP�QvP�U  ��       	@QPPaP� .@PP?P�PP�QvP�U  ��       	@QPPaP� ,@PP?P�PP�QvP�U  ��       	@QPPaP� +@PP?P�PP�QvP�U  ��       	@QPPaP� )@PP?P�PP�QvP�U  ��       	@QPPaP� '@PP?P�PP�QvP�U  ��       	@QPPaP� %@PP?P�PP�QvP�U  ��       	@QPPaP� #@PP?P�PP�QvP�U  ��       	@QPPaP� "@PP?P�PP�QvP�U  ��       	@QPPaP�  @P!P?P�PP�QvP�U  ��       	@QPPaP� @P#P?P�PP�QvP�U  ��       	@QPPaP� @P$P?P�PP�QvP�U  ��       	@QPPaP� @P&P?P�PP�QvP�U  ��       	@QPPaP� @P(P?P�PP�QvP�U  ��       	@QPPaP� @P*P?P�PP�QvP�U  ��       	@QPPaP� @P,P?P�PP�QvP�U  ��       	@QPPaP� @P.P?P�PP�QvP�U  ��       	@QPPaP� @P/P?P�PP�QvP�U  ��       	@QPPaP� @P1P?P�PP�QvP�U  ��       	@QPPaP� @P3P?P�PP�QvP�U  ��       	@QPPaP� @P5P?P�PP�QvP�U  ��       	@QPPaP� 
@P7P?P�PP�QvP�U  ��       	@QPPaP� @P8P?P�PP�QvP�U  ��       	@QPPaP� @P:P?P�PP�QvP�U  ��       	@QPPaP� @P<P?P�PP�QvP�U  ��       	@QPPaP� @P>P?P�PP�QvP�U  ��       	@QPPaP� @P@P?P�PP�QvP�U  ��       	@QPPa �@PBP?P�PP�QvP�U  ��       	@QPPa �@PCP?P�PP�QvP�U  ��       	@QPPa �@PEP?P�PP�QvP�U  ��       	@QPPa �@PFP?P�PP�QvP�U  ��       	@QPPa �@PPFP?P�PP�QvP�U  ��       	@QPPa �@PPFP?P�PP�QvP�U  ��       	@QPPa �@PPFP?P�PP�QvP�U  ��       	@QPPa �@PPFP?P�PP�QvP�U  ��       	@QPPa �@P
PFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@PPFP?P�PP�QvP�U  ��       @QQPa �@P PFP?PP�PP�QvP�U  ��       @QQPa �@P"PFP? @P�PP�QvP�U  ��       @QQPa �@P$PFP? @	P�PP�QvP�U  ��       @QQPa �@P%PFP? @P�PP�QvP�U  ��       @QQPa �@P'PFP? @P�PP�QvP�U  ��       @QQPa �@P)PFP? 
@P�PP�QvP�U  ��       @QQPa �@P+PFP? 
@P�PP�QvP�U  ��       @QQPa �@P-PFP? 
@P�PP�QvP�U  ��       @QQPa �@P.PFP? 	@P�PP�QvP�U  ��       @QQPa �@P0PFP? 
@P�PP�QvP�U  ��       @QQPa �@P2PFP? 
@P�PP�QvP�U  ��       @QQPa �@P4PFP? 
@P�PP�QvP�U  ��       @QQPa �@P6PFP? @P�PP�QvP�U  ��       @QQPa �@P8PFP? @	P�PP�QvP�U  ��       @QQPa @P9PFP? @P�PP�QvP�U  ��       @QQPa }@P;PFP?PP�PP�QvP�U  ��       @QQPa |@P=PFP?P�PP�QvP�U  ��       @QQPa z@P?PFP?P�PP�QvP�U  ��       @QQPa x@PAPFP?P�PP�QvP�U  ��       @QQPa v@PBPFP?P�PP�QvP�U  ��       @QQPa t@PDPFP?P�PP�QvP�U  ��       @QQPa s@PFPFP?P�PP�QvP�U  ��       @QQPa q@PHPFP?P�PP�QvP�U  ��       @QQPa o@PJPFP?P�PP�QvP�U  ��       @QQPa m@PKPFP?P�PP�QvP�U  ��       @QQPa k@PMPFP?P�PP�QvP�U  ��       @QQPa i@POPFP?P�PP�QvP�U  ��       @QQPa h@PQPFP?P�PP�QvP�U  ��       @QQPa f@PSPFP?P�PP�QvP�U  ��       @QQPa d@PUPFP?P�PP�QvP�U  ��       @QQPa b@PVPFP?P�PP�QvP�U  ��       @QQPa `@PXPFP?P�PP�QvP�U  ��       @QQPa _@PZPFP?P�PP�QvP�U  ��       @QQPa ]@P\PFP?P�PP�QvP�U  ��       @QQPa [@P^PFP?P�PP�QvP�U  ��       @QQPa Y@P_PFP?P�PP�QvP�U  ��       @QQPa W@PaPFP?P�PP�QvP�U  ��       @QQPa U@PcPFP?P�PP�QvP�U  ��       @QQPa T@PePFP?P�PP�QvP�U  ��       @QQPa R@PgPFP?P�PP�QvP�U  ��       @QQPa P@PiPFP?P�PP�QvP�U  ��       @QQPa N@PjPFP?P�PP�QvP�U  ��       @QQPa L@PlPFP?P�PP�QvP�U  ��       @QQPa K@PnPFP?P�PP�QvP�U  ��       @QQPa I@PpPFP?P�PP�QvP�U  ��      P @QNPa G@PrPFP?P�PP�QvP�U  ��       @	QNPa E@PsPFP?P�PP�QvP�U  ��       @	QNPa C@PuPFP?P�PP�QvP�U  ��       @QNPa A@PwPFP?P�PP�QvP�U  ��       @QNPa @@PyPFP?P�PP�QvP�U  ��       @QNPa >@P{PFP?P�PP�QvP�U  ��       @QMPa <@P}PFP?P�PP�QvP�U  ��       @	QLPa :@P~PFP?P�PP�QvP�U  ��       	@ @QJPa 8@P�PFP?P�PP�QvP�U  ��       	@ @QIPa 7@P�PFP?P�PP�QvP�U  ��       	@ @QIPa 5@P�PFP?P�PP�QvP�U  ��       	@PQIPa 3@P�PFP?P�PP�QvP�U  ��       	@QOPa 1@P�PFP?P�PP�QvP�U  ��       	@QOPa /@P�PFP?P�PP�QvP�U  ��       	@QOPa -@P�PFP?P�PP�QvP�U  ��       	@QOPa ,@P�PFP?P�PP�QvP�U  ��       	@QOPa *@P�PFP?P�PP�QvP�U  ��       	@QOPa (@P�PFP?P�PP�QvP�U  ��       	@QOPa &@P�PFP?P�PP�QvP�U  ��       	@QOPa $@P�PFP?P�PP�QvP�U  ��       	@QOPa #@P�PFP?P�PP�QvP�U  ��       	@QOPa !@P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa 	@P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QOPa @P�PFP?P�PP�QvP�U  ��       	@QO `@P�PFP?P�PP�QvP�U  ��       	@QO _@P�PFP?P�PP�QvP�U  ��       	@QO ]@P�PFP?P�PP�QvP�U  ��       	@QO \@P�PFP?P�PP�QvP�U  ��       	@QO Z@PP�PFP?P�PP�QvP�U  ��       	@QO X@PP�PFP?P�PP�QvP�U  ��       	@QO V@PP�PFP?P�PP�QvP�U  ��       	@QO T@PP�PFP?P�PP�QvP�U  ��       	@@P3 R@P	P�PFP?P�PP�QvP�U  ��       	@@P4 Q@PP�PFP?P�PP�QvP�U  ��       	@@P4 O@PP�PFP?P�PP�QvP�U  ��       	@@P4 M@PP�PFP?P�PP�QvP�U  ��       	@@PP1 K@PP�PFP?P�PP�QvP�U  ��       	@@
P1 I@PP�PFP?P�PP�QvP�U  ��       	@@
P1 H@PP�PFP?P�PP�QvP�U  ��       @@P1 F@PP�PFP?P�PP�QvP�U  ��       @@P1 D@PP�PFP?P�PP�QvP�U  ��       @@P2 B@PP�PFP?P�PP�QvP�U  ��       @@
P4 @@PP�PFP?P�PP�QvP�U  ��       @@
P5 >@PP�PFP?P�PP�QvP�U  ��       @@	P7 =@PP�PFP?P�PP�QvP�U  ��       @@	P8 ;@P!P�PFP?P�PP�QvP�U  ��       @@P: 9@P#P�PFP?P�PP�QvP�U  ��       @@P; 7@P$P�PFP?P�PP�QvP�U  ��       @@	P; 5@P&P�PFP?P�PP�QvP�U  ��       @@P@ 4@P(P�PFP?P�PP�QvP�U  ��       @@PA 2@P*P�PFP?P�PP�QvP�U  ��       @@PB 0@P,P�PFP?P�PP�QvP�U  ��       @QP .@P-P�PFP?P�PP�QvP�U  ��       @QP ,@P/P�PFP?P�PP�QvP�U  ��       @QP *@P1P�PFP?P�PP�QvP�U  ��       @QP )@P3P�PFP?P�PP�QvP�U  ��       @QP '@P5P�PFP?P�PP�QvP�U  ��       @QP %@P7P�PFP?P�PP�QvP�U  ��       @QP #@P8P�PFP?P�PP�QvP�U  ��       @QP !@P:P�PFP?P�PP�QvP�U  ��       @QP  @P<P�PFP?P�PP�QvP�U  ��       @QP @P>P�PFP?P�PP�QvP�U  ��       @QP @P@P�PFP?P�PP�QvP�U  ��       @QP @PAP�PFP?P�PP�QvP�U  ��       @QP @PCP�PFP?P�PP�QvP�U  ��       @QP @PEP�PFP?P�PP�QvP�U  ��       @QP @PGP�PFP?P�PP�QvP�U  ��       @QP @PIP�PFP?P�PP�QvP�U  ��       @QP @PKP�PFP?P�PP�QvP�U  ��       @QP @PLP�PFP?P�PP�QvP�U  ��       @QP @PNP�PFP?P�PP�QvP�U  ��       @QP @PPP�PFP?P�PP�QvP�U  ��       @QP 
@PRP�PFP?P�PP�QvP�U  ��       @QP @PTP�PFP?P�PP�QvP�U  ��       @QP @PUP�PFP?P�PP�QvP�U  ��       @QP @PWP�PFP?P�PP�QvP�U  ��       @QP @PYP�PFP?P�PP�QvP�U  ��       @QP @P[P�PFP?P�PP�QvP�U  ��       @O@P]P�PFP?P�PP�QvP�U  ��       @M@P_P�PFP?P�PP�QvP�U  ��       @K@P`P�PFP?P�PP�QvP�U  ��       @I@PaP�PFP?P�PP�QvP�U  ��       @H@PPaP�PFP?P�PP�QvP�U  ��       @F@PPaP�PFP?P�PP�QvP�U  ��       @D@PPaP�PFP?P�PP�QvP�U  ��       @B@PPaP�PFP?P�PP�QvP�U  ��       @@@P
PaP�PFP?P�PP�QvP�U  ��       @?@PPaP�PFP?P�PP�QvP�U  ��       @>@PPaP�PFP?P�PP�QvP�U  ��       @<@PPaP�PFP?P�PP�QvP�U  ��       @:@PPaP�PFP?P�PP�QvP�U  ��       @8@PPaP�PFP?P�PP�QvP�U  ��       @6@PPaP�PFP?P�PP�QvP�U  ��       @5@PPaP�PFP?P�PP�QvP�U  ��       @3@PPaP�PFP?P�PP�QvP�U  ��       @1@PPaP�PFP?P�PP�QvP�U  ��       @/@PPaP�PFP?P�PP�QvP�U  ��       @-@PPaP�PFP?P�PP�QvP�U  ��       @+@P PaP�PFP?P�PP�QvP�U  ��       @*@P"PaP�PFP?P�PP�QvP�U  ��       @(@P$PaP�PFP?P�PP�QvP�U  ��       @&@P&PaP�PFP?P�PP�QvP�U  ��       @$@P'PaP�PFP?P�PP�QvP�U  ��       @"@P)PaP�PFP?P�PP�QvP�U  ��       @!@P+PaP�PFP?P�PP�QvP�U  ��       @@P-PaP�PFP?P�PP�QvP�U  ��       @@P/PaP�PFP?P�PP�QvP�U  ��       @@P0PaP�PFP?P�PP�QvP�U  ��       @@P2PaP�PFP?P�PP�QvP�U  ��       @@P4PaP�PFP?P�PP�QvP�U  ��       @@P6PaP�PFP?P�PP�QvP�U  ��       @@P8PaP�PFP?P�PP�QvP�U  ��       @@P9PaP�PFP?P�PP�QvP�U  ��       @@P;PaP�PFP?P�PP�QvP�U  ��       @@P=PaP�PFP?P�PP�QvP�U  ��       @@P?PaP�PFP?P�PP�QvP�U  ��       @@PAPaP�PFP?P�PP�QvP�U  ��       @	@PCPaP�PFP?P�PP�QvP�U  ��       @@PDPaP�PFP?P�PP�QvP�U  ��       @@PFPaP�PFP?P�PP�QvP�U  ��       @@PHPaP�PFP?P�PP�QvP�U  ��       @@PJPaP�PFP?P�PP�QvP�U  ��       @ @PLPaP�PFP?P�PP�QvP�U  ��       @ �@PMPaP�PFP?P�PP�QvP�U  ��       @ �@POPaP�PFP?P�PP�QvP�U  ��       @ �@PQPaP�PFP?P�PP�QvP�U  ��       @ �@PSPaP�PFP?P�PP�QvP�U  ��       @ �@PUPaP�PFP?P�PP�QvP�U  ��       @ �@PWPaP�PFP?P�PP�QvP�U  ��       @ �@PXPaP�PFP?P�PP�QvP�U  ��       @ �@PZPaP�PFP?P�PP�QvP�U  ��       @ �@P\PaP�PFP?P�PP�QvP�U  ��       @ �@P^PaP�PFP?P�PP�QvP�U  ��       @ �@P`PaP�PFP?P�PP�QvP�U  ��       @ �@PaPaP�PFP?P�PP�QvP�U  ��       @ �@PcPaP�PFP?P�PP�QvP�U  ��       @ �@PePaP�PFP?P�PP�QvP�U  ��       @ �@PgPaP�PFP?P�PP�QvP�U  ��       @ �@PiPaP�PFP?P�PP�QvP�U  ��       @ �@PkPaP�PFP?P�PP�QvP�U  ��       @ �@PlPaP�PFP?P�PP�QvP�U  ��       @ �@PnPaP�PFP?P�PP�QvP�U  ��       @ �@PpPaP�PFP?P�PP�QvP�U  ��       @ �@PrPaP�PFP?P�PP�QvP�U  ��       @ �@PtPaP�PFP?P�PP�QvP�U  ��       @ �@PuPaP�PFP?P�PP�QvP�U  ��       @ �@PwPaP�PFP?P�PP�QvP�U  ��       @ �@PyPaP�PFP?P�PP�QvP�U  ��       @ �@P{PaP�PFP?P�PP�QvP�U  ��       @ �@P}PaP�PFP?P�PP�QvP�U  ��       @ �@PPaP�PFP?P�PP�QvP�U  ��       @ �@P�PaP�PFP?P�PP�QvP�U  ��       @ �@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ �@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ @P�PaP�PFP?P�PP�QvP�U  ��       @ E@ }@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ {@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ y@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ w@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ w@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ u@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ s@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ q@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ o@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ n@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ l@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ j@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ h@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ f@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ d@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ c@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ a@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ _@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ ]@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ [@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ Z@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ X@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ V@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ T@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ R@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ P@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ O@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ M@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ K@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ I@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ G@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ F@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ D@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ B@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ @@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ >@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ <@P�PaP�PFP?P�PP�QvP�U  ��       @ E@ ;@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ :@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ 8@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ 6@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ 4@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ 3@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ 1@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ /@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ -@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ +@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ )@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ (@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ &@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ $@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ "@P�PaP�PFP?P�PP�QvP�U  ��       @ D@  @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @P�PaP�PFP?P�PP�QvP�U  ��       @ D@ 	@P�PaP�PFP?P�PP�QvP�U  ��       @ D@ @Q PaP�PFP?P�PP�QvP�U  ��       @ D@ @QPaP�PFP?P�PP�QvP�U  ��       @ D@ @QPaP�PFP?P�PP�QvP�U  ��       @ D@ @QPaP�PFP?P�PP�QvP�U  ��       @ D@QPaP�PFP?P�PP�QvP�U  ��       @ C@Q	PaP�PFP?P�PP�QvP�U  ��       @ C@QPaP�PFP?P�PP�QvP�U  ��       @ A@QPaP�PFP?P�PP�QvP�U  ��       @ ?@QPaP�PFP?P�PP�QvP�U  ��       @ >@QPaP�PFP?P�PP�QvP�U  ��       @ =@ @QPaP�PFP?P�PP�QvP�U  ��       @ ;@QPaP�PFP?P�PP�QvP�U  ��       @ 9@QPaP�PFP?P�PP�QvP�U  ��       @ 7@QPaP�PFP?P�PP�QvP�U  ��       @ 5@QPaP�PFP?P�PP�QvP�U  ��       @ 4@QPaP�PFP?P�PP�QvP�U  ��       @ 2@QPaP�PFP?P�PP�QvP�U  ��       @ 0@QPaP�PFP?P�PP�QvP�U  ��       @ .@Q PaP�PFP?P�PP�QvP�U  ��       @ ,@Q"PaP�PFP?P�PP�QvP�U  ��       @ +@Q$PaP�PFP?P�PP�QvP�U  ��       @ )@Q&PaP�PFP?P�PP�QvP�U  ��       @ '@Q(PaP�PFP?P�PP�QvP�U  ��       @ %@Q)PaP�PFP?P�PP�QvP�U  ��       @ #@Q+PaP�PFP?P�PP�QvP�U  ��       @ !@Q-PaP�PFP?P�PP�QvP�U  ��       @  @Q/PaP�PFP?P�PP�QvP�U  ��       @ @Q1PaP�PFP?P�PP�QvP�U  ��       @ @Q3PaP�PFP?P�PP�QvP�U  ��       @P @Q4PaP�PFP? L@P`PP�QvP�U  ��       @ @P @Q6PaP�PFP? I@	P]PP�QvP�U  ��       @ @ @Q8PaP�PFP? H@P\PP�QvP�U  ��       @ @ 
@Q:PaP�PFP? G@P[PP�QvP�U  ��       @ @	 @Q<PaP�PFP? F@PZPP�QvP�U  ��       @ @	 @Q=PaP�PFP? F@PZPP�QvP�U  ��       @ @
 @Q?PaP�PFP? F@PZPP�QvP�U  ��       @ @QAPaP�PFP? E@PYPP�QvP�U  ��       @ @QCPaP�PFP? F@PZPP�QvP�U  ��       @ @QCPaP�PFP? F@PZPP�QvP�U  ��       @ @QCPaP�PFP? G@P[PP�QvP�U  ��       @ @QCPaP�PFP? H@P\PP�QvP�U  ��       @ @QDPaP�PFP? I@	P]PP�QvP�U  ��       @ @ @QDPaP�PFP? L@P`PP�QvP�U  ��       @ @P @QDPaP�PFP?P�PP�QvP�U  ��       @ @P @QDPaP�PFP?P�PP�QvP�U  ��       @PP @QDPaP�PFP?P�PP�QvP�U  ��       @ @QDPaP�PFP?P�PP�QvP�U  ��       @ @QDPaP�PFP?P�PP�QvP�U  ��       @ @QDPaP�PFPP<P�PP�QvP�U  ��       @ @QDPaP� D@	P8P�PP�QvP�U  ��       @ @QDPaP� B@P6P�PP�QvP�U  ��       @ @QDPaP� A@P5P�PP�QvP�U  ��       @ @QDPaP� @@P4P�PP�QvP�U  ��       @ @QDPaP� ?@P3P�PP�QvP�U  ��       @ @QDPaP� >@P2P�PP�QvP�U  ��       @ @QDPaP� =@P1P�PP�QvP�U  ��       @ @QDPaP� <@P0P�PP�QvP�U  ��       @ @QDPaP� =@P1P�PP�QvP�U  ��       @ @QDPaP� >@P2P�PP�QvP�U  ��       @ @QEPaP� >@P2P�PP�QvP�U  ��       @ @QEPaP� ?@P3P�PP�QvP�U  ��       @ @QEPaP� @@P4P�PP�QvP�U  ��       @ @QEPaP� A@P5P�PP�QvP�U  ��       @ @QEPaP� B@P6P�PP�QvP�U  ��       @ @QEPaP� D@	P8P�PP�QvP�U  ��       @ @QEPaP�PFPP<P�PP�QvP�U  ��       @ @QEPaP�PFP?P�PP�QvP�U # ��       @ @QEPaP�PFP?P�PP�QvP�U  ��       @ @QFPaP�PFP?P�PP�QvP�U $ ��       @ @QFPaP�PFP?P�PP�QvP�U  ��       @ @QGPaP�PFP?P�PP�QvP�U % ��      @ @QGPaP�PFP?P�PP�QvP�U  ��      @ @QHPaP�PFP?P�PP�QvP�U & ��      @ @QHPaP�PFP?P�PP�QvP�U 
 ��      @ @QIPaP�PFP?P�PP�QvP�U  ��      @ @QIPaP�PFP?P�PP�QvP�U ! ��       @QIPaP�PFP?P�PP�QvP�U 1 ��       @QJPaP�PFP?P�PP�QvP�U 1 ��       @QKPaP�PFP?P�PP�QvP�U 1 ��       @QLPaP�PFP?P�PP�QvP�U 1 ��       @QMPaP�PFP?P�PP�QvP�U  ��       @QNPaP�PFP?P�RCP�U