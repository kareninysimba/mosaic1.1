#!/bin/env pipecl
#
# MTDSTATS

int	status = 1
int     namps,numon,numoff
real	mean, x1, x2, diff
real    dqavfsat,dqavfsat1,dqavfsat2,dqmxfsat,dqmxfsat1,dqmxfsat2
string  datadir, dataset, iliston, ilistoff, image, indir, lfile, ifile
string	cal, calon, caloff, ilistb, bpm

# Tasks and packages
images
mscred
tables
proto

# Set file and path names.
sflnames (envget("OSF_DATASET"))
dataset = sflnames.dataset

# Set filenames.
indir = sflnames.indir
datadir = sflnames.datadir
iliston = indir // dataset // "on.tmp"
ilistoff = indir // dataset // "off.tmp"
ilistb = indir // dataset // "both.tmp"
lfile = datadir // sflnames.lfile
set (uparm = sflnames.uparm)
set (pipedata = sflnames.pipedata)
cal = sflnames.cal
calon = sflnames.calon
caloff = sflnames.caloff

# Log start of processing.
printf ("\nSFLCOMBINE (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

numon = 0
if ( access(iliston) ) {
    count( iliston ) | scan(numon)
    if (numon>0) 
        imcombine( "@"//iliston, calon, offset="physical",
            reject="avsigclip" )
    ;
    hsel( "@"//iliston, "DQARFSAT", yes, >> "sflcombine1.tmp" )
    tstat( "sflcombine1.tmp", "c1", low=INDEF, high=INDEF )
    dqavfsat1 = tstat.mean
    dqmxfsat1 = tstat.vmax
}
;
numoff = 0
if ( access(ilistoff) ) {
    count( ilistoff ) | scan(numoff)
    if (numoff>0)
        imcombine( "@"//ilistoff, caloff, offset="physical",
            reject="avsigclip" )
    ;
    hsel( "@"//ilistoff, "DQARFSAT", yes, >> "sflcombine2.tmp" )
    tstat( "sflcombine2.tmp", "c1", low=INDEF, high=INDEF )
    dqavfsat2 = tstat.mean
    dqmxfsat2 = tstat.vmax
}
;
if ( numon>0 && numoff>0 ) {
    print "DATA FOR BOTH"
    x1 = 0
    x2 = 0
    imstat( calon, fields="mode", nclip=3, lsigma=2,
        usigma=2, format- ) | scan( x1 )
    imstat( caloff, fields="mode", nclip=3, lsigma=2,
        usigma=2, format- ) | scan( x2 )
    diff = abs(x1 - x2)
    if ( diff<200 ) {
        print "DATA ARE TOO SIMILAR"
        status = 2
    } else {
        if ( x1>x2 ) {
            imarith( calon, "-", caloff, cal )
            dqavfsat = dqavfsat1
            dqmxfsat = dqmxfsat1
        } else {
            imarith( caloff, "-", calon, cal )
            dqavfsat = dqavfsat2
            dqmxfsat = dqmxfsat2
        }
    }
} else {
    if (numon>0) {
	print "CALON only"
        imcopy( calon, cal )
        dqavfsat = dqavfsat1
        dqmxfsat = dqmxfsat1
    }
    ;
    if (numoff>0) {
	print "CALOFF only"
        imcopy( caloff, cal )
        dqavfsat = dqavfsat2
        dqmxfsat = dqmxfsat2
    }
    ;
    if ( numon==0 && numoff==0 ) {
	status = 2
	print "NO DATA"
    }
    ;
}

if (status==1) {
    hedit( cal, "DQAVFSAT", dqavfsat, add+, ver- )
    hedit( cal, "DQMXFSAT", dqmxfsat, add+, ver- )
}
;

# Combine the on and off lists to make sure there is a valid entry
copy( iliston, ilistb )
concat( ilistoff, ilistb, append+ )
# Retrieve the BPM keyword from the first image in the list ...
list = ilistb
i = fscan( list, image )
list = ""
hselect( image, "BPM", yes ) | scan( bpm )
# ... and add it to the combined calibration 
hedit( cal, "BPM", bpm, add+, ver- )

# Interpolate over all bad pixels to produce cosmetically nice
# images.
fixpix( cal, "BPM" )

logout( status )
