#!/bin/env pipecl
#
# SFLproc
#
#
# History
#
# R. Swaters --------   Created.
# T. Huard  20081224    Changed linearize task from linearize_db.cl to
#			    linearize_im.cl to apply 2D linearity coefficients
#			    instead of constant coefficients.  "lincoeff" is
#			    defined as a string instead of a real.
# T. Huard  20090318    Changed linearize task to linearize_flex.cl to 
#               optionally apply either constant or 2D linearity coefficients.
#               The type of coefficients to be used depends on the value of
#               sfl_lincoeftype ("linconst" or "linimage") in NEWFIRM.cl.
#               sfl_lincoeftype is the second argument of linearity_flex.cl.
#               The reason linearity_flex.cl needs this second argument is
#               due to the getcal in that procedure, and the necessity of
#               having separate entries in DMData/catalog.db for the constant
#               and 2D coefficients.  Note that even though the linearity
#               coefficient information is included in DMData/catalog.db,
#               and retrieved by getcal and the value of sfl_lincoeftype,
#               the code to identify saturated pixels makes use of the 
#               LINCOEFF keyword value.  So, it is important that this keyword
#               has been set properly (in sflsetup.cl) for the saturation
#               part of the code in this procedure to work properly.  It is 
#               possible to rework this procedure so that it does not rely on
#               LINCOEFF, but that could be done at a later date, if desired.
#               Documentation has been revised.  Ensured that format of code 
#               conforms to convention.
# T. Huard  20090326    Changed sfl_lincoeftype to lincoeftype.
# Last Revised:	T. Huard  20090326  9:30AM

int	status = 1
int     namps, founddark, linerr, nsat, dx, dy
real	dqarfsat, x1, x2, x3, scale
string  dark, datadir, dataset, ilist, oliston, olistoff
string	image, indir, lfile, obstype, trimsec, lampstat, sateq, lincoeff

# Tasks and packages
images
proto
task $linearize = "$linearize_flex.cl $1"

# Set file and path names.
sflnames (envget("OSF_DATASET"))
dataset = sflnames.dataset

# Set filenames.
indir = sflnames.indir
datadir = sflnames.datadir
ilist = indir // dataset // ".sfl"
lfile = datadir // sflnames.lfile
set (uparm = sflnames.uparm)
set (pipedata = sflnames.pipedata)

# Log start of processing.
printf ("\nSFLPROC (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

        


list = ilist
oliston = indir // dataset // "on.tmp"
olistoff = indir // dataset // "off.tmp"
if (access(oliston))
    delete (oliston)
;
if (access(olistoff))
    delete (olistoff)
;
touch( oliston )
touch( olistoff )

for (namps=1; fscan(list,s1)!=EOF; namps+=1) {
    # Get the part of the filename after the last /, stripping the .fits
    s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    # Set the sfl variables for this image
    sflnames( s2 )
    # Retrieve the trim section, location of the dark, and the
    # obstype from the header
    # TODO rewrite into separate test of dark and other two keywords
    # and check against NEWFIRM.cl to see whether missing dark is OK.
    founddark = 1
    dark = ""
    hsel( s1, "DARK", yes ) | scan( dark )
    if ( nscan()==0 ) {
        founddark = 0
    }
    ;
    #
    obstype = ""
    hsel( s1, "TRIMSEC,NOCTYP,LAMPSTAT", yes ) |
	scan( trimsec, obstype, lampstat )
    #
    if ( nscan()!=3 ) {
        sendmsg( "ERROR", "Could not retrieve all required keywords",
	    s2, "VRFY" )
        status = 0
        break
    }
    ;
    # Set the name of the image
    image = sflnames.image
    # Copy the trimsec only, i.e., remove the reference pixels and
    # delete the section keywords.
    imcopy( s1//trimsec, image )
    hedit (image, "DETSEC,DATASEC,TRIMSEC,BIASSEC", del+, verify-, show-)

    # Read image size keywords from header.
    # It is *IMPORTANT* that the LINCOEFF keyword is correct since it is
    # the value that is used to help determine the saturated pixels.
    # Alternatively, one could obtain the coefficient from DMData/catalog.db
    # by doing a getcal, as is done later in this procedure to help with
    # linearization.
    hsel( image, "NAXIS1,NAXIS2,LINCOEFF,SATEQ", yes ) |
        scan( dx, dy, lincoeff, sateq )
    # Calculate the number of pixels above saturation. This is done by
    # using the saturation imexpression database. This was intended
    # to combine a mask with a thresholded image (i.e., 
    # (a==1 ? 1 : b>(satvalue(c)) ? 3 : 0). Here, the same expression
    # is used, but now a is set to 0. Sateq and saturate are 
    flpr
    imexpr( sateq, "cummask.pl", 0, image, lincoeff,
        exprdb="pipedata/saturation.db" )
    mimstat( "cummask.pl", imasks="!BPM", fields="npix", lower=2, nclip=0,
        format- ) | scan(nsat)
    imdel( "cummask.pl" )

    # Calculate the fraction of pixels that are saturated
    dqarfsat = real(nsat)/real(dx*dy)
    printf ("Fraction of saturated pixels: %.2f\n", dqarfsat)
    # Write fraction of saturated pixels to the header
    hedit( image, "DQARFSAT", dqarfsat, add+, ver- )
    # Send warning if fraction is high
    if (dqarfsat>0.01)
        sendmsg( "WARNING", "Fraction of saturated pixels exceeds 1%",
	    s2, "OBSV" )
    ;

    # Subtract the dark
    if ( founddark == 1 ) {
        imarith( image, "-", dark, image )
    }
    ;

    linerr = 0
    iferr {
        linearize( image )
    } then {
        linerr = 1
    } else
        ;

    if (linerr==1) {
        if (sgc_nolincont) {
            # No nonlinearity correction available, but OK to continue
	    sendmsg( "WARNING", 
                "No nonlinearity correction available, data not linearized",
	        "", "PROC" )
	} else {
            # No nonlinearity correction available, not OK to continue
	    sendmsg( "ERROR", 
                "No nonlinearity correction available, data not linearized",
	        "", "PROC" )
            status = 0
	    break
        }
    } else {
        # Data has been linearized, and output has been scaled to
        # nocadds=nfowler=1, and exptime=1s. This is desired in the SGC
        # pipeline, but not here, because the sgccomb.cl will compare the 
        # the absolute count levels. Scaling can be done either here or
        # in sgccombine.cl. Note that in sflcombine the scaling applies
        # to a number, not to an image, so it is more efficient.
        # TODO: move scaling to sflcombine.cl.
        hselect( image, "EXPTIME,NCOADD,FSAMPLE", yes ) | scan( x1, x2, x3 )
        scale = x1*x2*x3
        imarith( image, "*", scale, image )
    }

    # Write name of output image to oliston or olistoff
    print( obstype )
    if ((obstype=="dflaton")||(obstype=="DFLATON"))
        print( image, >> oliston )
    else if ((obstype=="dflatoff")||(obstype=="DFLATOFF"))
        print( image, >> olistoff )
    else if ((obstype=="dflats")||(obstype=="DFLATS")) {
        if ((lampstat=="a")||(lampstat=="A"))
            print( image, >> oliston )
        else if ((lampstat=="b")||(lampstat=="B"))
            print( image, >> olistoff )
	else
            print( image, >> oliston )
    } else
	print( image, >> oliston )
}
list = ""

logout( status )
