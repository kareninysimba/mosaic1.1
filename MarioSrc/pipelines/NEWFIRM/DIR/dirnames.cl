#!/bin/env pipecl
#
# DIRNAMES -- Collect all the input and output names into a final form.

string  dataset
file	indir, datadir, ilist, lfile, nlist

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ds"
lfile = datadir // names.lfile
nlist = datadir // names.shortname // "_names.log"
if (access (ilist) == NO)
    logout 1
;

# Work in data directory.
cd (datadir)

# Log start of processing.
printf ("\nDIRNAMES (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Get all the names log files.
print (nlist, > "dirnames.tmp")
match ("names.log", dataset//".list", >> "dirnames.tmp")
concat ("@dirnames.tmp", "STDOUT") | sort (> nlist)

logout 1
