#!/bin/env pipecl
#
# DIRGROUP -- Group by observation types and sequence number.

int	l
bool	verb = yes
string	dataset, indir, datadir
string	inlist, lfile, dlist, flist, olist, fname
string	seqnum, nocdhs, grp, dtcaldat
struct	filter
struct	*fd

int	ominseq = 2	# Minimum number in object sequence

# Define packages and tasks.
task setseqid = NHPPS_PIPESRC$/NEWFIRM/DIR/setseqid.cl
util
proto
images
servers
unlearn setseqid
cache	setseqid

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
lfile = datadir // names.lfile
inlist = indir // dataset // ".dir"
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

#delete ("dirgroup*.tmp,[dfo]list.tmp")
delete ("*.tmp")
dlist = "dlist.tmp"
flist = "flist.tmp"
olist = "olist.tmp"

# Log start of processing.
printf ("\nDIRGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Append "[0]" to all files in the list.  We need to sort the files because
# the order retrieved from the archive may not be related to the actual
# order of the exposures.
list = inlist
while (fscan( list, fname) != EOF)
    print (fname//"[0]", >> "dirgroup1.tmp")

# Set the sequence IDs.  If we trust the KTM sequences we could skip this step.
if (YES) {
    hselect ("@dirgroup1.tmp", "$I,NOCID", yes) |
	sort (col=2, num+) | fields ("STDIN", "1", > "dirgroup1.tmp")
    print (dataset) | translit ("STDIN", "_", " ") | scan (s2, s3)
    list = "dirgroup1.tmp"
    while (fscan (list, fname) != EOF) {
	hselect (fname, "NOCNO,NOCTOT", yes) | scan (i, j)
	if (nscan() < 2)
	    next
	;
	setseqid (s2, s3, i, j, "none")
	hedit (fname, "SEQID", setseqid.seqid, add+, show=verb)
	hedit (fname, "SEQNUM", setseqid.seqnum, add+, show-)
	print (fname, >> "dirgroup.tmp")
    }
    list = ""; delete ("dirgroup1.tmp")
} else
    rename ("dirgroup1.tmp", "dirgroup.tmp")

# Separate by obstype.

hselect ("@dirgroup.tmp", "$I", "(obstype='dark'||obstype='zero')", > "z.tmp")
hselect ("@dirgroup.tmp", "$I", "(obstype='flat')", > "f.tmp")
hselect ("@dirgroup.tmp", "$I", "(obstype='object'||obstype='sky')", > "o.tmp")
delete ("dirgroup.tmp")

## Hack to skip calibration processing.
#delete z.tmp,f.tmp
#touch z.tmp,f.tmp

# Separate into sequence lists.
hselect ("@z.tmp", "$I,seqnum", yes) | sort (col=2, > "dirgroup.tmp")
delete ("z.tmp")
s1 = ""; list = "dirgroup.tmp"
while (fscan (list, fname, i) != EOF) {
    printf ("%04d\n", i) | scan (seqnum)
    if (seqnum != s1) {
	print (datadir // dataset // "_" // seqnum // ".tmp", >> dlist)
	s1 = seqnum
    }
    ;

    # Add to sequence file.
    fname = substr (fname, 1, stridx("[",fname)-1)
    printf ("%s -> %s\n", fname, dataset//"_"//seqnum//".tmp", >> lfile)
    print (fname, >> dataset//"_"//seqnum//".tmp")
}
list = ""; delete ("dirgroup.tmp")
if (access(dlist)) {
    sort (dlist) | uniq (> "dirgroup.tmp")
    rename ("dirgroup.tmp", dlist)
}
;

# Separate into sequence lists.
hselect ("@f.tmp", "$I,seqnum,nocdhs", yes) | sort (col=2, > "dirgroup.tmp")
delete ("f.tmp")
s1 = ""; list = "dirgroup.tmp"
while (fscan (list, fname, i, nocdhs) != EOF) {
    nocdhs = strupr (nocdhs)
    if (nocdhs=="SFLAT" || nocdhs=="SFLATO" || nocdhs=="TFLAT")
        next
    ;
    printf ("%04d\n", i) | scan (seqnum)
    if (seqnum != s1) {
	print (datadir // dataset // "_" // seqnum // ".tmp", >> flist)
	s1 = seqnum
    }
    ;

    # Add to sequence file.
    fname = substr (fname, 1, stridx("[",fname)-1)
    printf ("%s -> %s\n", fname, dataset//"_"//seqnum//".tmp", >> lfile)
    print (fname, >> dataset//"_"//seqnum//".tmp")
}
list = ""; delete ("dirgroup.tmp")
if (access(flist)) {
    sort (flist) | uniq (> "dirgroup.tmp")
    rename ("dirgroup.tmp", flist)
}
;

# Separate into sequence lists.  The sequence lists may be merged by
# a group name.  A group name is a concatentation of all the grouping
# critera.  For reasons of possible large sequences we currently make the
# group name include the calendar date.  To allow sequences to cross nights
# this would be changed.

hselect ("@o.tmp", "$I,seqnum,nocdhs,rspgrp,dtcaldat,filter", yes) |
    sort (col=2, > "dirgroup.tmp")
delete ("o.tmp")

s1 = ""; s2 = ""; grp = ""
list = "dirgroup.tmp"
while (fscan (list, fname, i, nocdhs, grp, dtcaldat, filter) != EOF) {
    nocdhs = strupr (nocdhs)
    if (nocdhs=="FOCUS" || nocdhs=="TEST" || nocdhs=="SFLATO")
        next
    ;
    
    # For readability add the short filter to the dataset name.
    s3=""; match (filter, "pipedata$subsets.dat") |
        fields ("STDIN", "2") | scan (s3)
    if (s3 == "") {
	sendmsg ("ERROR", "Filter not in subset file", filter, "VRFY")
	next
    }
    ;
    filter = s3
    
    printf ("%04d%s\n", i, filter) | scan (seqnum)
    if (seqnum != s1) {
	s1 = seqnum
	s2 = ""
	j = 0
    }
    ;

    j += 1
    if (j == ominseq)
	print (datadir // dataset // "_" // seqnum // ".tmp", >> olist)
    ;

    # Add to sequence file.
    fname = substr (fname, 1, stridx("[",fname)-1)
    printf ("%s -> %s\n", fname, dataset//"_"//seqnum//".tmp", >> lfile)
    print (fname, >> dataset//"_"//seqnum//".tmp")

    # Add group name to grouping file if desired.
    printf ("%s%s%s\n", grp, dtcaldat, filter) |
        translit ("STDIN", " \t", delete+) | scan (grp)
    if (s2 != grp && dir_stkgrp) {
        s2 = grp
	printf ("%s %s\n", grp, dataset//"_"//seqnum//".tmp",
	    >> "dirgroup1.tmp")
    }
    ;
}
list = ""; delete ("dirgroup.tmp")

# Merge sequences with the same group.  Avoid making overly large sequences.
# 120 is a nice number because it has so many factors.

if (access("dirgroup1.tmp")) {
    sort ("dirgroup1.tmp", > "dirgroup.tmp"); delete ("dirgroup1.tmp")
    list = "dirgroup.tmp"
    i = fscan (list, grp, fname)
    count (fname) | scan (j)
    printf ("Group %s (%d)\n", fname, j) | tee (lfile)
    while (fscan (list, s2, s1) != EOF) {
	if (s1 == fname || access(s1)==NO)
	    next
	;
	count (s1) | scan (k)
        if (s2 == grp) {
	    count (fname) | scan (j)
	    l = j + k
	    if (l <= 120) {
		printf ("  Merge %s (%d) -> %s (%d)\n",
		    s1, k, fname, l) | tee (lfile)
		concat (s1, fname, append+)
		delete (s1)
	    } else {
		printf ("Group %s (%d)\n", s1, k) | tee (lfile)
	        fname = s1
	    }

	} else {
	    printf ("Group %s (%d)\n", s1, k) | tee (lfile)
	    fname = s1
	    grp = s2
	}
    }
    list = ""; delete ("dirgroup.tmp")

    rename (olist, "dirgroup.tmp")
    list = "dirgroup.tmp"
    while (fscan (list, fname) != EOF) {
        if (access(fname))
	    print (fname, >> olist)
	;
    }
    list = ""; delete ("dirgroup.tmp")
}
;

if (access(olist)) {
    sort (olist) | uniq (> "dirgroup.tmp")
    rename ("dirgroup.tmp", olist)
}
;

logout 1
