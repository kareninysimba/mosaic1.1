#!/usr/bin/env python

# This simple version of dirverify simply rejects exposures with
# nocdhs=="test" from the input list. If the output list has
# zero length, this module exits with exitcode=2.

# It also translates the detector/detname keyword pair (when present)
# to instrume/detector, which are the prefered values starting 2008B.

# Import python modules
import datetime
import time
import os
import sys
import pyfits as p
import random

VERBOSE      = True

# Wrapper function around send_message
def sendmsg( status, message, submsg, id, dset ):
    omsg = message
    if (len(submsg)>0):
        message += ' ('+submsg+')'
    print message
    return omsg

def proc( indir, dataset, hostname ):

    print '\nDIRVERIFY %s: ' % ( dataset ),
    print datetime.datetime.today()
    # Open the input file list
    fName  = os.path.join( indir, '%s.dir' % (dataset) )
    infile = open( fName, 'r' )
    # Read the file into memory...
    lines = infile.readlines()
    # Close the input file...
    infile.close()

    # Open the output file for writing
    fnOutfile  = os.path.join( indir, '%s.out' % (dataset) )
    #fnOutfile = "%s.out"
    outfile = open( fnOutfile, 'w' )

    rejected = 0

    # Loop over all files in the list
    # Note that it is assumed that all data sets are local
    for line in lines:
        datasetok = 1
        writeheader = 0

        thisline  = line.strip()
        if VERBOSE:
            print '=== Processing: %s' % (thisline)
        
        # Make sure file is on local host
        fields = thisline.split('!')
        if len(fields) == 2:
            # The part before the ! is the hostname, the rest is the filename
            # TODO next line can cause trouble if there is a . in the filename
            filehost = fields[0].split( '.', 1)[0]
            thisfile = fields[1]
        elif len( fields ) == 1:
            thisfile = thisline
            filehost = hostname
        else:
            # More than one ! in the file name
            sendmsg( 'ERROR', 'Input file name garbled', thisline, 'VRFY',
                     dataset )
            sys.exit(1)
        
        if filehost != hostname:
            sendmsg( 'ERROR', 'Host name in input list is not this host',
                     filehost, 'VRFY', dataset )
            sys.exit(1)
        
        # Make sure the file ends in .fits
        thisfile, ext = os.path.splitext( thisfile )
        if ext != '.fits':
            ext = '.fits'
        thisfile += ext
        # Get the short file name
        shortfile = os.path.basename( thisfile )
        
        # Read the relevant keywords from the header
        # Open the fits file
        try:
            hdu = p.open( thisfile, 'update' )
        except:
            msg = sendmsg( 'WARNING', 'Could not open fits file',
                           thisfile, 'VRFY', shortfile )
            datasetok = False
        else:
            try:
                # Read the header
                prihdr = hdu[0].header
            except:
                msg = sendmsg( 'WARNING', 'Could not open fits header',
                               thisfile, 'VRFY', shortfile )
                datasetok = False
    
        if datasetok:
            try:
                nocdhs  = prihdr['nocdhs']
            except:
                datasetok = False

        # Check for the presence of noclamp, replace with
        # lampstat A or B when present.
        if datasetok:
            try:
                detector = prihdr['lampstat']
            except:
                # No lampstat in the primary header, meaning the
                # keyword needs to be generated based on the contents
                # of noclamp.
                try:
                    noclamp = prihdr['noclamp'].lower()
                except:
                    msg = sendmsg( 'WARNING', 'noclamp has invalid value',
                                   noclamp, 'VRFY', shortfile )
                    datasetok = 0
                else:
                    if noclamp == 'on':
                        lampstat = 'A'
                    elif noclamp == 'off':
                        lampstat = 'B'
                    else:
                        msg = sendmsg( 'WARNING', 'noclamp has invalid value',
                                       noclamp, 'VRFY', shortfile )
                        datasetok = 0
                    if datasetok:
                        prihdr.update( 'lampstat', lampstat )
                        writeheader = 1

        # Check for the presence of the detname/detector keywords.
        # If they are present switch them out for the detector/instrume
        # keywords.
        if datasetok:
            try:
                detector = prihdr['detector']
            except:
                # No detector keyword in the primary header, meaning
                # the new instrument/detector keyword pair is used
                pass
            else:
                # The old detname/detector keyword pair is used.
                if detector == 'NEWFIRM':
                    del prihdr['detector']
                    prihdr.update( 'instrume', 'NEWFIRM' )
                    writeheader = 1
                    for i in range(1,5):
                        try:
                            name = hdu[i].header['DETNAME']
                        except:
                            # No detname found, so dataset is rejected
                            # TODO: remediate
                            msg = sendmsg( 'WARNING', 'detname not found',
                                           '', 'VRFY', shortfile )
                            datasetok = 0
                        else:
                            head = hdu[i].header
                            del head['DETNAME']
                            head.update( 'detector', name )

        if datasetok:
            try:
                dtinstru = prihdr['dtinstru']
            except:
                try:
                    instrume = prihdr['instrume']
                except:
                    prihdr.update( 'dtinstru', 'NEWFIRM' )
                else:
                    prihdr.update( 'dtinstru', instrume )
                writeheader = 1
            try:
                dtpropid = prihdr['dtpropid']
            except:
                prihdr.update( 'dtpropid', 'FakeID' )
                writeheader = 1
            try:
                dtpi = prihdr['dtpi']
            except:
                prihdr.update( 'dtpi', 'FakePI' )
                writeheader = 1

        if datasetok:
            try:
                nocmpat = prihdr['nocmpat']
            except:
                pass
            else:
                if nocmpat == '4Q':
                    try:
                        nocmdof = abs( prihdr['nocmdof'] )
                        nocmrof = abs( prihdr['nocmrof'] )
                    except:
                        pass
                    else:
                        if (nocmdof<10) and (nocmrof<10):
                            prihdr.update( 'nocmpat', 'MOD_4Q' )
                            writeheader = 1
            try:
                nocdpat = prihdr['nocdpat']
            except:
                pass
            else:
                if nocdpat == '4Q':
                    try:
                        nocddof = abs( prihdr['nocddof'] )
                        nocdrof = abs( prihdr['nocdrof'] )
                    except:
                        pass
                    else:
                        if (nocddof<10) and (nocdrof<10):
                            prihdr.update( 'nocdpat', 'MOD_4Q' )
                            writeheader = 1
            
        # Check for trimsec, define if not present
        if datasetok:

            printmsg = 1
            for i in range(1,5):

                # First, make sure the header can be read
                try:
                    thisheader = hdu[i].header
                except:
                    if printmsg==1:
                        msg = sendmsg( 'ERROR',
                                       'Could not open subset header. Is the fits file corrupt?',
                                       '', 'VRFY', shortfile )
                        printmsg = 0
                        break

                # In some cases, pyfits capitalizes the extname
                # keyword, which causes problems in the pipeline.
                # So the keywords are read, converted to lowercase
                # and then written again.
                try:
                    ext = hdu[i].header['extname'].lower()
                except:
                    pass
                else:
                    hdu[i].header.update( 'extname', ext )

                try:
                    trimsec = hdu[i].header['trimsec']
                except:
                    hdu[i].header.update( 'trimsec', '[2:2047,2:2047]' )
                    writeheader = 1

                try:
                    detector = hdu[i].header['detector']
                except:
                    # TODO: use getcal for this
                    if i==1:
                        hdu[i].header.update( 'detector', 'SN019' )
                    elif i==2:
                        hdu[i].header.update( 'detector', 'SN022' )
                    elif i==3:
                        hdu[i].header.update( 'detector', 'SN013' )
                    elif i==4:
                        hdu[i].header.update( 'detector', 'SN011' )
                    writeheader = 1
                    print 'UPDATE DETECTOR'
                else:
                    pass

                try:
                    inherit = hdu[i].header['inherit']
                except:
                    hdu[i].header.update( 'inherit', True )
                else:
                    if inherit == False:
                        hdu[i].header.update( 'inherit', True )
                        writeheader = 1

                # Remove duplicate entries of CD1_1
                head = hdu[i].header
                ncd11 = 0
                for keyname in head.ascardlist().keys():
                    if ( keyname == 'CD1_1' ):
                        ncd11 += 1
                if ncd11 > 1:
                    cd11 = hdu[i].header['CD1_1']
                    del head['CD1_1']
                    head.update( 'CD1_1', cd11 )

                # Check for FSAMPLE, NCOADD etc. keywords, which
                # may be missing for 2007B data
                try:
                    mjdobs = hdu[i].header['mjd-obs']
                except:
                    try:
                        mjdobs = prihdr['mjd-obs']
                    except:
                        msg = sendmsg( 'WARNING', 'mjd-obs undefined',
                                       '', 'VRFY', shortfile )
                        datasetok = 0
                        mjdobs = 0
                    
                if datasetok and mjdobs < 54497:
                    try:
                        value = hdu[i].header['digavgs']
                    except:
                        hdu[i].header.update( 'digavgs', 4 )
                        writeheader = 1
                    try:
                        value = hdu[i].header['ncoadd']
                    except:
                        hdu[i].header.update( 'ncoadd', 1 )
                        writeheader = 1
                    try:
                        value = hdu[i].header['fsample']
                    except:
                        hdu[i].header.update( 'fsample', 1 )
                        writeheader = 1
                    try:
                        value = hdu[i].header['expcoadd']
                    except:
                        exptime = hdu[i].header['exptime']
                        hdu[i].header.update( 'expcoadd', exptime )
                        writeheader = 1
                    try:
                        value = hdu[i].header['title']
                    except:
                        hdu[i].header.update( 'title',
                                              'Pipeline Generated Title' )
                        writeheader = 1
                

        if datasetok:
            try:
                obsid = prihdr['obsid']
            except:
                # TODO: make a fake obsid more like the real one
                # Note that this is only done for the persistence
                # Pipeline
                print 'FIXING OBSID'
                rnd = '%s' % random.random()
                # ljust(12,'0') was used initially, but this does not
                # work in python 2.3. TODO: replace ' ' with '0'.
                newobsid = 'RND%s' % rnd[2:14].ljust(12)
                prihdr.update( 'obsid', newobsid )
                writeheader = 1

        if writeheader:
            # Write the updated header to disk
            hdu.close( output_verify='ignore' )

        if datasetok:
            writeout = 1
            if nocdhs.lower() == 'test' or nocdhs.lower() == 'focus':
                writeout = 0
        else:
            writeout = 0

        if writeout:
            outfile.write( '%s' % ( line ) )
        else:
            rejected += 1
            print "Test exposure %s is rejected" % shortfile

    # Close the output file
    outfile.flush()
    outfile.close()

    if (rejected>0):
        status = 0
        # The list has changed, so rename the new list to the old list
        os.system( 'mv %s %s' % (fnOutfile, fName) )
    else:
        status = 0

    return( status )

if __name__ == '__main__':
    
    print "============ START OF DIRVERIFY.PY ============"
    
    # Set filenames and directories
    dataset = os.environ['OSF_DATASET']
    dirdata = os.environ['NEWFIRM_DIR']
    indir   = os.path.join( dirdata, 'input' ) + os.path.sep
    datadir = os.path.join( dirdata, 'data', dataset ) + os.path.sep

    print "Working on dataset: %s" % (dataset)

    # Get the node name
    thishost = os.environ['HOST'].split('.')[0]

    # Get the node name
    thishost = os.environ['HOST'].split('.')[0]
    
    status = proc( indir, dataset, thishost )

    sys.exit( status )
