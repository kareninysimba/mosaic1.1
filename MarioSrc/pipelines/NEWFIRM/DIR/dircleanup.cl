#!/bin/env pipecl
#
# DIRCLEANUP -- Clean up staged files.

string  dataset
file	indir, datadir, ilist, lfile

# Load tasks and packages
servers

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
ilist = indir // dataset // ".ds"
lfile = datadir // names.lfile

# Log start of processing.
printf ("\nDIRCLEANUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

if (access (ilist) == YES) {

    # Clean up staged files.
    delete ("@"//ilist)

    # Remove tmp files from data directory.
    cd (datadir)
    delete ("*.tmp")

    # Remove any DUPOBSID files.
    delete ("DUPOBSID*")

    # Remove files from indir.  This is also done in the done stage but
    # we can call this module for error recover.
    cd (indir)
    delete (dataset//".*")

} else {
    
    printf( "Input ilist is empty\n" )

}

print "START"
# Remove cumulative pixel masks from the database.
delcal( cm, class="masks", value="like %"//dataset//"%" )
print "END"

logout 1
