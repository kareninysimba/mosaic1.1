# SETSEQID -- Set the sequence ID as done in the KTM.

procedure setseqid (queue, dataset, seqno, seqtot, obsid)

string	queue			{prompt="Queue"}
string	dataset			{prompt="Dataset"}
int	seqno			{prompt="Sequence number"}
int	seqtot			{prompt="Sequence total"}
string	obsid			{prompt="Observation ID"}

int	seqnum = 0		{prompt="Sequence number"}
string	seqid = ""		{prompt="Sequence ID"}

file	mtdend1 = ""		{prompt="MTDEND file"}
file	mtd = ""		{prompt="MTD file"}
file	mtdend2 = ""		{prompt="MTDEND file"}

# This takes the place of the .ktm.dat file.
string	fqueue = ""
string	fdataset = ""
int	fseqnum = -1
string	fexpid = ""
int	fseqno = 0
int	fseqtot = 0
string	fseqflag = "last"

begin
	string	trig, seqend, seqflag, expid, val
	string	lastdataset, lastexpid, lastseqflag, lastseqid
	int	lastseqnum, lastseqno, lastseqtot

	print (obsid) |
	    translit ("STDIN", "-_.:", del+) | scan (expid)

	#printf ("%s %s %d %d\n", queue, dataset, seqno, seqtot, expid)

	# Get/set the last information.
	lastdataset = "unknown"
	lastseqnum = 0
	lastexpid = "unknown"
	lastseqno = 0
	lastseqtot = seqtot
	lastseqflag = "last"
	lastseqid = "unknown"
	if (fseqnum != -1) {
	    queue = fqueue
	    lastdataset = fdataset
	    lastseqnum = fseqnum
	    lastexpid = fexpid
	    lastseqno = fseqno
	    lastseqtot = fseqtot
	    lastseqflag = fseqflag

	    # Format the last sequence ID string.
	    if (lastexpid == "none")
		printf ("%s_%s_%04d\n", queue, lastdataset,
		    lastseqnum) | scan (lastseqid)
	    else
		printf ("%s_%s_%04d-%s\n", queue, lastdataset,
		    lastseqnum, lastexpid) | scan (lastseqid)
	}

	# Apply heuristics to determine when to change the sequence ID.
	seqflag = "none"

	# If the queue or dataset names change trigger the last dataset.
	if (dataset != lastdataset) {
	    seqflag = "last"
	}

	# If the sequence total has been reached trigger the dataset(s).
	if (seqno == seqtot) {
	    if ((seqtot == 1 || seqtot != lastseqtot) &&
	        lastseqflag == "none") {
	        seqflag = "both"
	    } else {
		seqflag = "current"
	    }

	# If there is a break in the sequence number trigger the last dataset.
	} else if (seqno <= lastseqno || seqno > lastseqno+1) {
	    seqflag = "last"
	}

	# If the last dataset has already been triggered don't trigger again.
	if (lastseqflag == "current" || lastseqflag == "both") {
	    if (seqflag == "last") {
	        seqflag = "none"
	    } else if ( seqflag == "both") {
	        seqflag = "current"
	    }
	} else if (lastseqflag == "last" && seqflag == "current") {
	    seqflag = "both"
	}

	# Advance the sequence number and reset the last sequence number
	seqnum = lastseqnum
	if (seqflag == "both") {
	    lastseqnum += 1
	    seqnum = lastseqnum
	    seqno = 0
	} else if (seqflag == "current" && lastseqflag == "last") {
	    lastseqnum += 1
	    seqnum = lastseqnum
	    seqno = 0
	} else if (seqflag == "last") {
	    lastseqnum += 1
	    seqnum = lastseqnum
	} else if (lastseqflag == "current" || lastseqflag == "both") {
	    lastseqnum += 1
	    seqnum = lastseqnum
	}

	# Format the current sequence ID string.
	if (expid == "none")
	    printf ("%s_%s_%04d\n",
		queue, dataset, lastseqnum) | scan (val)
	else
	    printf ("%s_%s_%04d-%s\n",
		queue, dataset, lastseqnum, expid) | scan (val)
	seqid = val

	# Update/create data file.
	fqueue = queue
	fdataset = dataset
	fseqnum = seqnum
	fexpid = expid
	fseqno = seqno
	fseqtot = seqtot
	fseqflag = seqflag

	# === PLTrigger ===

	# Create end of sequence for last sequence.
	mtdend1 = ""
	if (lastseqid != "unknown" &&
	    (seqflag == "last" || seqflag == "both")) {
	    printf ("%d.mtdend\n", lastseqid) | scan (val)
	    mtdend1 = val
	}

	# Create trigger file for current exposure.
	printf ("%s.mtd\n", seqid) | scan (val)
	mtd = val

	# Create end of sequence for current sequence.
	mtdend2 = ""
	if (seqflag == "current" || seqflag == "both") {
	    printf ("%d.mtdend\n", seqid) | scan (val)
	    mtdend2 = val
	}
end
