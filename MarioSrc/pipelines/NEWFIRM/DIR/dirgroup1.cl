#!/bin/env pipecl
#
# DIRGROUP1 -- Set list of zeros for call to NGT pipeline.
# If there are no zeros return a status flag for no data.
#
# 	STATUS	MEANING
#	0	Untrapped or trapped error
#	1	Found data
#	2	Found no data

string dataset, indir, datadir, inlist, lfile, dlist, pattern

# Set filenames.
names ("dir", envget("OSF_DATASET"))
dataset = names.dataset
indir = names.indir
datadir = names.datadir
inlist = indir // dataset // ".ddir"
lfile = datadir // names.lfile
dlist = datadir // "dlist.tmp"
cd (datadir)

# Log start of processing.
printf ("\nDIRGROUP1 (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Set list or return no data status.
if (access (dlist)) {
    rename (dlist, inlist)
    type (inlist)
    logout 1
} else
    logout 2
