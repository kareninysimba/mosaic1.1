# SKYNAMES -- Directory and filenames for the SKY pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure skynames (name)

string  name                    {prompt = "Name"}

string  dataset                 {prompt = "Dataset name"}
string  pipe                    {prompt = "Pipeline name"}
string  shortname               {prompt = "Short name"}
file    rootdir                 {prompt = "Root directory"}
file    indir                   {prompt = "Input directory"}
file    datadir                 {prompt = "Dataset directory"}
file    uparm                   {prompt = "Uparm directory"}
file    pipedata                {prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file    lfile                   {prompt = "Log file"}
file    image                   {prompt = "Primary image name"}
file	sky			{prompt = "Sky image"}
file	skysub			{prompt = "Sky subtracted image"}
file	mask			{prompt = "Mask image"}
file	holes			{prompt = "Holes mask"}
file	cbpmloc			{prompt = "Reference to location of CBPM"}
string  pattern = ""            {prompt = "Pattern"}
bool    base = no               {prompt = "Child part of dataset"}

begin
        # Set generic names.
        names ("sky", name, pattern=pattern, base=base)
        dataset = names.dataset
        pipe = names.pipe
        shortname = names.shortname
        rootdir = names.rootdir
        indir = names.indir
        datadir = names.datadir
        uparm = names.uparm
        pipedata = names.pipedata
        parent = names.parent
        child = names.child
        lfile = names.lfile
	sky = shortname // "_sky"
	skysub = shortname // "_ss"
	mask = shortname // "_mask"
	holes = shortname // "_holes"
	cbpmloc = shortname // "_cbpmloc"

        # Set pipeline specific names.
        image = shortname

        if (pattern != "") {
            image += ".fits"
        }
end
