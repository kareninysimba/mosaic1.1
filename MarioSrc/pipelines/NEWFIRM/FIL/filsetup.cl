#!/bin/env pipecl
#
# FILSETUP -- Setup FIL processing data including calibrations.

int     status = 1
struct  statusstr, mask_name
string  dataset, indir, datadir, ifile, im, lfile
string	subdir
file	caldir = "MarioCal$/"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( "fil", envget("OSF_DATASET") )
dataset = names.dataset

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
	caldir = caldir // subdir

# Set paths and files.
indir = names.indir
ifile = indir // dataset // ".fil"
datadir = names.datadir
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf( "\nFILSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1//"[0]" )
} then {
    logout 0
} else
    ;

logout 1
