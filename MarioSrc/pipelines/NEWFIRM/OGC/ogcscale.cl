#!/bin/env pipecl
#
# OGCproc

int	status = 1
real	exptime, rdnoise, gain, fowler, saturate, mjd, dqfracok, coadds
string	dark, datadir, dataset, flat, ilist, obpm, s4, maskname
string	image, indir, lfile, trimsec, obstype, shortname, sateq, lincoeff

# Tasks and packages
images
imutil
servers
proto
dataqual
task $linearize = "$linearize_flex.cl $1 $2"

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCSCALE (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )
if (imaccess(image)==NO)
    logout (status)
;

# Get the exposure time, Fowler sampling, and number off
# coadds from the header
hselect( image, "EXPTIME,FSAMPLE,NCOADD", yes ) |
    scan( exptime, fowler, coadds )
# Read the original bad pixel mask from the header
hsel( image, "BPM", yes ) | scan( obpm )

# Store the original exposure time
hedit( image, "OEXPTIME", exptime, add+, ver- )
# Set the exposure time to 1 s
hedit( image, "EXPTIME", 1, update+, ver- )
    
# Store the original Fowler sampling
hedit( image, "OFSAMPLE", '(fsample)', add+, ver- )
# Set the Fowler sampling to 1
hedit( image, "FSAMPLE", 1, update+, ver- )

# Retrieve saturate keyword from the header. Note that this 
# value has been put in by a getcal, meaning that the original
# value put in by the KTM has been overwritten. The value from
# the KTM was scaled up by ncoadds and nfowler. Saturate
# as retrieved by getcal is for ncoadds=1 and nfowler=1.
# Saturate still needs to be scaled down by exptime. It
# is important to realize that this value of saturate is not
# a true threshold. To find out which pixels are saturated,
# the saturation.db imexpression should be used.
if (do_saturation) {
    hselect( image, "SATURATE", yes ) | scan( saturate )
    # Store the modified saturation value
    saturate = saturate/(exptime)
    hedit( image, "SATURATE", saturate, update+, ver- )
}
;
    
# Clean up
imdel( "cummask2.pl" )

logout( status )
