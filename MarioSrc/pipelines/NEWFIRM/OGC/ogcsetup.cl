#!/bin/env pipecl
#
# ogcsetup

int     status = 1
int     fsample, digavgs, ncoadd
struct  statusstr, mask_name
string  dataset, indir, datadir, ilist, lfile, host, image
string  trimsec, darkinfo, match
real	dexptime

string	subdir
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
ogcnames (envget ("OSF_DATASET"))
dataset = ogcnames.dataset
datadir = ogcnames.datadir
indir   = ogcnames.indir
ilist   = indir // dataset // ".ogc"
lfile   = datadir // ogcnames.lfile

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == NO)
    caldir = caldir // subdir

set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nOGCSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Setup uparm and pipedata
delete( substr( ogcnames.uparm, 1, strlen(ogcnames.uparm)-1 ) )
s1 = ""; head( ilist, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# Extract the image to be worked on from the input list
head( ilist, nlines=1 ) | scan( s1 )
s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
ogcnames( s2 )
image = ogcnames.image

print "AAA"
print( s1 )
# Retrieve the trim section from the header
hsel( s1, "TRIMSEC", yes ) | scan( trimsec )
if ( nscan()!=1 ) {
    sendmsg( "ERROR", "Could not retrieve trimsec",
        image, "VRFY" )
    logout 0
}
;

# Copy the image to here, trimming it at the same time
# save it as the "raw" image for possible later use and review.
imcopy( s1//trimsec, image )
imcopy( image, ogcnames.raw )

# Delete the section keywords
hedit (image, "DETSEC,DATASEC,TRIMSEC,BIASSEC", del+, verify-, show-)
# Write the name of the image to image_p for the downstream
# modules to use
print( image, > "image_p" )

# Check type of linearization to be done.  ltype="linconst" for constant
# linearity coefficients, while ltype="linimage" for 2D linearity coefficients.
if ((lincoeftype != "linconst") && (lincoeftype != "linimage")) {
    s2="lincoeftype "//lincoeftype//" not recognized, using linimage"
    sendmsg("WARNING",s2,dataset,"VRFY")
    lincoeftype="linimage"
} else {
    if (lincoeftype == "linconst") {
        printf("\nUsing constant linearity coefficient.\n") | tee(lfile)
    } else {
        printf("\nUsing 2D linearity coefficient.\n") | tee(lfile)
    }
}

getcal (image, "bpm", cm, caldir,
    obstype="", detector="!instrume", imageid="!detector",
    filter="", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
if (i != 0) {
    sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
        "CAL")
    status = 0
}
;

if (status==1) {

    # Finding a suitable dark is done in three stages. First, an attempt
    # is made to find an exact match in coadds, Fowler sampling,
    # digital averages, and exposure time. If that does not result in
    # a match, then only a match in the exposure time is required.
    # If that also fails, and ogc_nodarkcont is set to yes, then
    # the dark closest in exposure time will be used.

    # Set the permissible deviation from the nominal exposure time
    dexptime = 0.05
    darkinfo = ""

    # Construct the match string
    hselect( image, "FSAMPLE,DIGAVGS,NCOADD,EXPTIME", yes ) |
        scan( fsample, digavgs, ncoadd, s1 )
    match = "CO="//ncoadd//" DA="//digavgs//" FS="//fsample
    getcal( image, "dark", cm, caldir,
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="!exptime", dexptime=dexptime,
        mjd="!mjd-obs", match=match ) | scan( i, statusstr )

    if ( i == 0 ) {
        darkinfo = "Exact match: "//match//" exptime: "//s1
    } else {
    
        # Try to find a dark with similar exposure time
        getcal( image, "dark", cm, caldir,
            obstype="", detector="!instrume", imageid="!detector",
            filter="", exptime="!exptime", dexptime=dexptime,
            mjd="!mjd-obs" ) | scan( i, statusstr )

        if ( i == 0 ) {
            darkinfo = "Exptime only match, exptime: "// s1
        } else {

            # If none were found, and ogc_nodarkcont is set to YES, then
            # try again with a larger dexptime.  The value for 
            # ogc_nodarkcont is set in NEWFIRM.cl
            if ( (i != 0) && (ogc_nodarkcont == YES) ) {
                #dexptime = 1.0
                dexptime = 10000.0
                getcal( image, "dark", cm, caldir,
                    obstype="", detector="!instrume", imageid="!detector",
                    filter="", exptime="!exptime", dexptime=dexptime,
                    mjd="!mjd-obs" ) | scan( i, statusstr )
                if ( i == 0 ) {
                    darkinfo = "Closest exptime match, exptime: "// s1
                }
                ;
            }
            ;
            if (i != 0) {
                if (ogc_nodarkcont == YES) {
                    darkinfo = "Data not dark subtracted"
                } else {
                    sendmsg( "ERROR", "Getcal failed for dark", image, "CAL" )
                    status = 0
                }
            }
            ;
        }
        ;
    }
    ;
}
;

if ( status == 1 ) {
    hedit( image, "DARKINFO", darkinfo, add+, ver-, update+ ) | tee( lfile )
}
;

if ( status == 1 ) {
    # Try to retrieve the required flat from the database
    getcal( image, "dflat", cm, caldir,
        obstype="object", detector="!instrume", imageid="!detector",
        filter="!filter", exptime="", mjd="!mjd-obs" ) |
            scan( i, statusstr )
    if (i != 0) {
        if (ogc_noflatcont == NO) { # value set in NEWFIRM.cl
            # No dome flat available, not OK to continue, so throw error
            sendmsg( "ERROR", "Getcal failed for flat",
                str(i)//" "//statusstr, "CAL" )
            status = 0
        }
        ;
    }
    ;
}
;

if (status==1) {
    # Check whether the getcal works for retrieving linearity coefficient
    getcal(image,lincoeftype,cm,caldir,obstype="",detector="!instrume",
        imageid="!detector",filter="",exptime="",mjd="!mjd-obs")
    if (getcal.statcode>0) {
        # Getcal was not successful
        s2="Getcal failed - no linearity coefficient retrieved"
        sendmsg("ERROR",s2,image,"CAL")
        status=0
    } else {
        # Getcal was successful
        # If constant linearity coefficient is used, write value of LINCONST
        # keyword to LINCOEFF keyword.  If 2D linearity coefficient is used,
        # write value of LINIMAGE keyword to LINCOEFF keyword.
        # It is *IMPORTANT* that LINCOEFF has the appropriate value because
        # that keyword is read in sflproc.cl to help identify saturated
        # pixels.
        s2=""; hselect(image,lincoeftype,yes) | scan(s2)
        if (s2 != "") {     
            hedit(image,"LINCOEFF",s2,add+,addonly-,verify-,show+,update+)
        } else {
            s2=lincoeftype//" keyword not found"
            sendmsg("ERROR",s2,image,"CAL")
            status=0
        }
    }
}
;

if (status==1) {
    if (do_saturation) {
	getcal( image, "saturate", cm, caldir, 
	    obstype="", detector="!instrume", imageid="!detector",
	    filter="", exptime="", mjd="!mjd-obs")
	if ( getcal.statcode == 0 ) {
	    getcal( image, "sateq", cm, caldir, 
		obstype="", detector="!instrume", imageid="!detector",
		filter="", exptime="", mjd="!mjd-obs")
	}
	;
	if (getcal.statcode>0) { # Getcal was not successful
	    sendmsg( "ERROR",
		"Getcal failed - no saturation equation retrieved", image,
		    "CAL" )
	    status = 0
	}
	;
    } else
        hedit (image, "saturate,sateq", del+, show-, verify-)
}
;

if (status==1) {
    getcal( image, "gain", cm, caldir, 
        obstype="", detector="!instrume", imageid="!detector",
        filter="", exptime="", mjd="!mjd-obs")
    if (getcal.statcode>0) { # Getcal was not successful
        sendmsg( "ERROR",
            "Getcal failed - no gain retrieved", image, "CAL" )
        status = 0
    }
    ;
}
;

logout( status )
