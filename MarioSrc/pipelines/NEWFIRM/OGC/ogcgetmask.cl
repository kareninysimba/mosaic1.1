#!/bin/env pipecl
#
# OGCproc

# The saturation keyword as put in the header by the KTM is
# set as follows (from F. Valdes' 3/19/08 email):
# exptime = ncoadds * exptime for one readout,
# saturation = nfowler * ncoadds * saturation for one readout

int	status = 1
int	namps, dx, dy, npix
real	dqfracok, mjd
string	datadir, dataset, ilist, obpm
string	image, indir, lfile, sateq, lincoeff

file    caldir = "MC$"

# Tasks and packages
images
imutil
servers
proto
dataqual

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCGETMASK (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )

# Read the original bad pixel mask from the header
hsel( image, "BPM", yes ) | scan( obpm )

# Set lincoeff from the LINCOEFF keyword value
lincoeff = ""; hselect( image, "LINCOEFF", yes ) | scan( lincoeff )
if (lincoeff == "") { 
    sendmsg( "ERROR", "LINCOEFF keyword not found", image, "CAL" )
    status = 0
}
;    

# Get the saturation equation from the header
if (do_saturation) {
    hsel( image, "$SATEQ", yes ) | scan( sateq )
    if ( sateq == "INDEF" ) {
	sendmsg( "ERROR",
	    "No sateq in header - no saturation equation retrieved",
	    image, "CAL" )
	status = 0
    } else {
	# Create a cumulative mask of saturated pixels and original bpm
	flpr
	imexpr( sateq, "cummask1.pl", obpm, image, lincoeff,
	    exprdb="pipedata/saturation.db" )
    }
} else
    imcopy (obpm, "cummask1.pl")

if (do_persistence) {

    # Read keywords needed needed for the getcal
    hsel( image, "DETECTOR,OBSID,MJD-OBS", yes ) | scan( s2, s3, mjd )
    # Retrieve the location of the cumulative mask
    getcal( image, "masks", cm, caldir, imageid=s2, mjd=mjd,
        match="%"//s3, obstype="", detector="", filter="" )
    if (getcal.statcode>0) {
        sendmsg( "ERROR", "Getcal failed with message: "//getcal.statstr,
            image, "PROC" )
        status = 0
    }
    ;
    # Write path of the mask to file for ogcmask to retrieve
    print( getcal.value, >> "cbpm_p" )

    if (status==1) {
        # Merge the saturated/bad pixel mask with the persistence mask
        mskexpr( "(i>0)?i:m", "cummask2.pl", "cummask1.pl",
            refmasks=getcal.value )
        # These are useful for debugging perposes
        #imcopy( "cummask1.pl", image//"cm1.pl" )
        #imcopy( "cummask2.pl", image//"cm2.pl" )
        #imcopy( getcal.value, image//"gc.pl" )
    }
    ;

} else
    imcopy( "cummask1.pl", "cummask2.pl" )

if (status==0) {
    logout 0
}
;

# Clean up intermediate results
imdel( "cummask1.pl" )

# Reject this dataset from further processing if the mask
# is mostly filled.
# First get the number of pixels in the image
hselect( image, "NAXIS1,NAXIS2", yes ) | scan( dx, dy )
npix = dx*dy
# Now get the number of unmasked pixels
i = 0
mimstat( image, imasks="cummask2.pl", fields="npix", format- ) | scan( i )
# Determine the fraction of unmasked pixels
dqfracok = real(i)/real(npix)
printf ("Mask fraction: %.2f\n", dqfracok)
# Reject the image if the fraction of ok pixels is too small
#if ( dqfracok < 0.25 ) {
if ( dqfracok < 0.05 ) {
    sendmsg( "ERROR", "Mask fraction too low", str(dqfracok), "PROC")
    imdel( image )
    status = 2
}
;

logout( status )
