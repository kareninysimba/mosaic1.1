#!/bin/env pipecl
#
# OGCproc

# The saturation keyword as put in the header by the KTM is
# set as follows (from F. Valdes' 3/19/08 email):
# exptime = ncoadds * exptime for one readout,
# saturation = nfowler * ncoadds * saturation for one readout

int	status = 1
string	datadir, dataset, ilist, image, indir, lfile

# Tasks and packages
images
imutil
servers
proto
dataqual

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCBADCHANS (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )

if (imaccess(image)==NO)
    logout (status)
;

if (ogc_fixchannels) {
    fixnoisychan( image )
    if ( imaccess( image//"_fnp" ) ) {
        imdel( image )
        imrename( image//"_fnp", image )
        # Get the name of the new mask from the header
        hselect( image, "BPM", yes ) | scan( s3 )
        # Following is useful for debugging
        #imcopy( s3, image//"fnp.pl" )
        # Merge the bad channels into the cumulative mask
        mskexpr( "(i<=2)? i: m", "tmpmask.pl", s3, refmasks="cummask2.pl" )
        imdel( "cummask2.pl" )
        imcopy( "tmpmask.pl", "cummask2.pl" )
    } else {
        sendmsg( "WARNING", "Bad channel detection failed", image, "PROC" )
    }
}
;

logout( status )
