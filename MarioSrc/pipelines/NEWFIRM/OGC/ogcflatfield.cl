#!/bin/env pipecl
#
# OGCFLATFIELD

int	status = 1
string	datadir, dataset, flat, ilist, image, indir, lfile

# Tasks and packages
images
imutil
servers
proto
dataqual

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCFLATFIELD (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

if ( whenflatfield == "after" ) {
    print( "Flat field will be applied AFTER sky subtraction." )
    logout( 1 )
}
;

cd( datadir )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )

# Retrieve the location of the flat field from the header, if available
flat = "" ; hselect( image, "FLAT", yes ) | scan( flat )
if (flat == "") {
    printf( "No flat field was applied!\n" ) | tee( lfile )
} else {
    # Dome flat available, so divide data by flat
    imarith( image, "/", flat, image )
    printf( "Applied flat field: %s\n", flat ) | tee( lfile )
}

logout( status )
