#!/bin/env pipecl
#
# OGCproc

# The saturation keyword as put in the header by the KTM is
# set as follows (from F. Valdes' 3/19/08 email):
# exptime = ncoadds * exptime for one readout,
# saturation = nfowler * ncoadds * saturation for one readout

int	status = 1
int	linerr
real	exptime, fowler, coadds
string	datadir, dataset, ilist
string	image, indir, lfile

# Tasks and packages
images
imutil
servers
proto
dataqual
task $linearize = "$linearize_flex.cl $1"

# Set file and path names.
ogcnames( envget("OSF_DATASET") )
dataset = ogcnames.dataset

# Set filenames.
indir = ogcnames.indir
datadir = ogcnames.datadir
ilist = indir // dataset // ".ogc"
lfile = datadir // ogcnames.lfile
set (uparm = ogcnames.uparm)
set (pipedata = ogcnames.pipedata)

# Log start of processing.
printf( "\nOGCLINEARIZE (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Make sure no persistence masking is done if not in sciencepipeline
if ( sciencepipeline == NO )
    do_persistence = NO
;

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
ogcnames( image )

linerr = 0
iferr {
    # Note that linearize scales the image to ncoadds=1, Fowler 
    # sampling of 1, and to exptime=1s.
    linearize( image )
} then {
    linerr = 1
} else
    ;

if (linerr==1) {
    if (ogc_nolincont) {
        # No nonlinearity correction available, but OK to continue
        sendmsg( "WARNING", 
            "No nonlinearity correction available, data not linearized",
            "", "PROC" )
        # Scale the image to ncoadds=1, Fowler sampling of 1, and
        # exptime=1s
        x = exptime*fowler*coadds
        imarith( image, "/", x, image )
    } else {
        # No nonlinearity correction available, not OK to continue
        sendmsg( "ERROR", 
            "No nonlinearity correction available, data not linearized",
            "", "PROC" )
        status = 0
    }
}
;

logout( status )
