#!/bin/env pipecl
#
# MTDSTATS

int	status = 1
string	dataset, indir, datadir, ifile, im, lfile, maskname, shortname
string	statusstr

string	subdir
file	caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
swcnames( envget("OSF_DATASET") )
dataset = swcnames.dataset

# Set paths and files.
indir = swcnames.indir
ifile = indir // dataset // ".swc"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
    caldir = caldir // subdir
;

datadir = swcnames.datadir
lfile = datadir // swcnames.lfile
set (uparm = swcnames.uparm)
set (pipedata = swcnames.pipedata)
cd( datadir )

# Log start of processing.
printf( "\nSWCSETUP (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

if ( access( "wcsgcl" ) ) {
    # This only happens if this module is rerun, i.e., during
    # development. If wcsgcl already exists, the code after the else
    # should not be executed again.
} else {
    # Copy the wcsgcl file to here
    match( "wcsgcl", ifile, print- ) | scan( s1 )
    copy( s1, "wcsgcl" )
    # Remove the wcsgcl return file from the list of lists
    match( "wcsgcl", ifile, print-, stop+, >> "swcsetup1.tmp" )
    delete( ifile )
    rename( "swcsetup1.tmp", ifile )
}

# Set secondary return files.
cd (indir//"return")
if (access (dataset//".swc")) {
    head( dataset//".swc" ) | scan( s1 )
    s1 = substr( s1, 1, strstr("/input/",s1) ) 
    s2 = dataset // ".ace"
    print( s1//"input/"//s2, > dataset//".ace" )
    print( s2, >> s1//"data/"//swcnames.parent//"/"//swcnames.parent//".ace" )
}
;
# Return to the data directory
cd( datadir )

# Setup uparm and pipedata
delete( substr( swcnames.uparm, 1, strlen(swcnames.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# Get the calibrations.
list = ifile
while (fscan (list, s1) != EOF) {

    shortname = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )

    # Retrieve the bad pixel mask
    getcal (s1, "bpm", cm, caldir,
        obstype="!obstype", detector="!instrume", imageid="!detector",
        filter="!filter", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
        sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
            "CAL")
        status = 0
        break
    }
    ;

    if ( whenflatfield == "after" ) {
        # Try to retrieve the required flat from the database. This was
        # done in ogcsetup as well, but there is no guarantee that the
        # same extension runs on the same node. To ensure that the 
        # required flat is present, the getcal is done here as well.
        hedit( s1, "flat", del+, ver- )
        getcal( s1, "dflat", cm, caldir,
            obstype="object", detector="!instrume", imageid="!detector",
            filter="!filter", exptime="", mjd="!mjd-obs" ) |
                scan( i, statusstr )
        if (i != 0) {
            if (ogc_noflatcont == NO) {
                # No dome flat available, not OK to continue, so throw error
                sendmsg( "ERROR", "Getcal failed for flat",
                    str(i)//" "//statusstr, "CAL" )
                status = 0
            }
            ;
        }
        ;
    }
    ;

    # Retrieve the cumulative pixel mask
    # Read keywords needed needed for the getcal
    hsel( s1, "OBSID", yes ) | scan( s3 )
    # Retrieve the location of the cumulative mask
    getcal( s1, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
        match="%"//s3, obstype="", detector="", filter="" )
    if (getcal.statcode>0) {
        sendmsg( "ERROR", "Getcal failed with message: "//getcal.statstr,
	    shortname, "PROC" )
        status = 0
        break
    }
    ;
    # Extract the file name from the full path
    maskname = substr( getcal.value, strldx("/",getcal.value)+1, 999 )
    # Copy the cumulative mask to here ...
    imcopy( getcal.value, maskname )
    # ... and add it to the header
    hedit( s1, "CBPM", maskname, add+, update+, ver- )

}
list = ""

# Clean up
delete( "swcsetup?.tmp" )

logout 1
