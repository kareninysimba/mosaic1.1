#!/bin/env pipecl

int     status
string  datadir, dataset, extname, lol
string  image, indir, lfile, listoflists, temp1, temp2
struct  *fd

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set file and path names.
gosnames (envget("OSF_DATASET"))
dataset = gosnames.dataset

# Set filenames.
datadir = gosnames.datadir
indir = gosnames.indir
lfile = datadir // gosnames.lfile
set (uparm = gosnames.uparm)
set (pipedata = gosnames.pipedata)

# Log start of processing.
printf ("\nGOSWCSSTART (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Create the filename to be passed on to the WCS pipeline
lol = indir//dataset//"wcs"//".lol"

# Pass on the GCL return file to the WCS pipeline
head( dataset//".gcl" ) | scan( s1 )
pathnames( s1, >> lol )

# The output from the SKY pipeline will be fed to the WCS pipeline,
# after the relevant fits files have been extracted. The dataset.sky
# is a list of lists, each of the lists containing the output of
# the SKY pipeline for one of the extensions and one group as created
# by goswhichskysub.cl.
if (access (dataset//".sky")) {
    # First, merge the extensions back together, because these may
    # have been regrouped for sky subtraction. Note that it is not
    # "wrong" to leave the extensions split in different groups. If 
    # these are left in different groups, it only means that more 
    # instances of the swc pipeline will run. 
    list = "split.ext"
    while ( fscan( list, s1, s2 ) != EOF ) {
print( s1 )
print( s2 )
        # Create the matching string
        s3 = "*"//s2//".sky"
        # Select the matches from gosstkgroup2.tmp
        match( s3, dataset//".sky", print-, > "goswcsstart2.tmp" )
        # Get the name of the first file
        fd = "goswcsstart2.tmp"
        i = fscan( fd, s1 )
        # Add the contents of the other files to the first
        while ( fscan( fd, s2 ) != EOF ) {
            concat( s2, >> s1 )
        }
        fd = ""
        delete( "goswcsstart2.tmp" )
        # Write the name of the first file, now containing all
        # exposures, to the new list
        print( s1, >> "goswcsstart3.tmp" )
    }
    list = ""
    # Rename the new list to the original input list
    delete( dataset//".sky" )
    copy( "goswcsstart3.tmp", dataset//".sky" )
    list = dataset//".sky"
    # Loop over all lists, s1 will contain filename of individual lists
    while (fscan (list, s1) != EOF) { 
        # Select files matching _ss.fits
        match( "_ss.fits", s1, >> "goswcsstart1.tmp" )
        # Keep the original file ...
        rename( s1, s1//".orig" )
        # ... and replace it with the filtered one
        rename( "goswcsstart1.tmp", s1 ) 
    }
    # Create full IRAF pathnames for the contents of dataset.sky
    pathnames( "@"//dataset//".sky", >> lol )
    temp1 = "goswcs1.tmp"
    temp2 = "goswcs2.tmp"
    # Call the WCS pipeline
    callpipe( "gos", "wcs", "copy", lol, temp1, temp2 )
    concat (temp2) | scan (status)
    concat (temp1)
    printf("Callpipe returned %d\n", status)
} else {
    sendmsg ("WARNING", "No data found", "", "VRFY")
    status = 0
}

logout( status )
