#!/bin/env pipecl

int	status
string	datadir, dataset, extname
string	image, indir, lfile, listoflists, outfile
string	temp1, temp2

# Tasks and packages.
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set file and path names.
gosnames (envget("OSF_DATASET"))
dataset = gosnames.dataset

# Set filenames.
indir = gosnames.indir
datadir = gosnames.datadir
lfile = datadir // gosnames.lfile
set (uparm = gosnames.uparm)
set (pipedata = gosnames.pipedata)

# Log start of processing.
printf ("\nGOSSTKGROUP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

listoflists = indir//dataset//"stk.lol"
type( listoflists )
# Call the STK pipelines 
temp1 = "gosstk1.tmp"
temp2 = "gosstk2.tmp"
callpipe( "gos", "stk", "split", listoflists, temp1, temp2 )
concat (temp2) | scan (status)
concat (temp1)
printf("Callpipe returned %d\n", status)

logout( status )
