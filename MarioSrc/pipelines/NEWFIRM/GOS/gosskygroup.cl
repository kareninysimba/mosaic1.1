# SKYGROUP -- Group coordinate list on the sky.
#
# The input is list of ra (0h-24h or 0d-360d) and dec (-90d to 90d) in the
# first two columns followed by arbitrary data (usually a filename).
# This is complicated by the periodicities at 0h.

procedure gosskygroup( input, output )

file	input			{prompt="Input list"}
string	output			{prompt="Output rootname"}
string	extn = ""		{prompt="Optional output extension"}
real	sep = 60		{prompt="Separation between groups (arcsec)"}
string	raunit = "hr"		{prompt="RA unit (hr|deg)"}
bool	keepcoords = yes	{prompt="Keep coordinates in output?"}
string	raformat = "%.2h"	{prompt="Output RA format"}
string	decformat = "%.1h"	{prompt="Output DEC format"}

struct	*fd1, *fd2

begin
	file	in, out, fname, temp1, temp2, temp3
	int	i, j, n, n1
	real	dra, r1, d1, r2, d2, r3, d3, r4, d4
	struct	fmtstr, data1, data2, data3
	int	result

	# Temporary files.
	fname = mktemp ("tmp")
	in = fname // "in"
	temp1 = fname // "1"
	temp2 = fname // "2"
	temp3 = fname // "3"

	# Set parameters.
	fname = input
	out = output

	# Check for existing output files.
	files (out//"_[0-9][0-9][0-9]*"//extn, > temp1)
	count (temp1) | scan (n); delete (temp1, verify-)
	if (access(out) || n > 0)
	    error (1, "Output files already exist")

	# Set dra (point of wrap-around, i.e., ra+dra=ra) 
	if (raunit == "hr")
	    dra = 24
	else
	    dra = 360

	# Set the format string for the output files
	if (keepcoords)
	    fmtstr = "%d " // raformat // " " // decformat // " %s"
	else
	    fmtstr = "%d %s"
	fmtstr += "\n"

	# We start by sorting in dec.
	sort (fname, col=2, num+, > in)

	# Set the output file counter
	n = 1

	# Find jumps in dec bigger than the separation and then sort
	# in ra and find jumps in ra bigger than separation.  Handle
	# the wrap around at 0h by duplicating to extend beyond 24h.
	# The duplicates will be eliminated during the merging process.
	fd1 = in
	# Get the first entry from the input file
	if (fscan (fd1, r1, d1, data1) == EOF)
	    error (1, "No data or badly formated data")
	# Loop over all lines in the input file
	while (fscan (fd1, r2, d2, data2) != EOF) {

	    # Add the current entry to the current Dec-group
	    print (r1, d1, data1, >> temp1)
	    # If the previous RA is only sep larger than
	    # 0, add dra and add it to the Dec-group again
	    if (r1 / max (0.001, dcos(d1)) * 3600 <= sep) {
		r4 = r1 + dra
		print (r4, d1, data1, >> temp1)
	    }
	    # If the difference in Dec between the current
	    # entry and the previous is smaller than sep, then
	    # set the r1 (previous entry) to the current and move
	    # on to the next entry in the input file
	    if (abs(d2-d1)*3600 <= sep) {
		r1 = r2; d1 = d2; data1 = data2
		next
	    }

	    # Store the entry that was most recently read from
	    # the Dec sorted list
	    r3 = r2; d3 = d2; data3 = data2

	    # If we reach this point, there has been a jump
	    # in Dec. Now we will sort the Dec group built
	    # so far in RA and look for groups in RA.
	    # Sort the Dec-group in RA
	    sort (temp1, col=1, num+, > temp2)
	    # Delete the unsorted file
	    delete (temp1, verify-)
	    fd2 = temp2
	    # Get the first entry from the input file
	    result = fscan (fd2, r1, d1, data1)
	    # Keep the Dec of the first entry in the list
	    d4 = d1
	    # Loop over all entries in the sorted Dec-group
	    while (fscan (fd2, r2, d2, data2) != EOF) {
	        # Write the previous entry to file in
		# user specified format
		if (keepcoords)
		    printf (fmtstr, n, r1, d1, data1, >> temp3)
		else
		    printf (fmtstr, n, data1, >> temp3)
		# Calculate the separation between the
		# current and the previous point. Note that
		# the declination is fixed to d4 because all
		# entries in this list are in the same Dec
		# group
		gosskyangle (r1, d4, r2, d4, raunit=raunit, verb-)
	        # If the difference in RA between the current
	        # entry and the previous one smaller than sep,
		# then set the r1 (previous entry) to the
		# current and move on to the next entry
		if (gosskyangle.sep <= sep) {
		    r1 = r2; d1 = d2; data1 = data2
		    next
		}

		# If we reach this point, there has been
		# a jump in RA, so we increase the output
		# file counter ...
		n += 1
		# ... and reset r1 to contain the previous
		# entry at the start of the next iteration
		r1 = r2; d1 = d2; data1 = data2
	    }
	    # Close the input file and delete the sorted list
	    fd2 = ""; delete (temp2, verify-)

	    # Make sure the last point also gets written
	    if (keepcoords)
		printf (fmtstr, n, r1, d1, data1, >> temp3)
	    else
		printf (fmtstr, n, data1, >> temp3)
	    n += 1

	    # Reset r1 to contain the previous entry of the
	    # Dec-grouped list at the start of the next
	    # iteration
	    r1 = r3; d1 = d3; data1 = data3
	}
	# Close the input file and delete the Dec sorted list
	fd1 = ""; delete (in, verify-)

	# Finish up the last Dec-group
	# Add the current entry to the list
	print (r1, d1, data1, >> temp1)
	# If the current RA is only sep larger than
	# 0, add dra and add it to the Dec-group again
	if (r1 / max (0.001, dcos(d1)) * 3600 <= sep) {
	    r4 = r1 + dra
	    print (r4, d1, data1, >> temp1)
	}
	# Sort the Dec-group in RA
	sort (temp1, col=1, num+, > temp2)
	# Delete the unsorted file
	delete (temp1, verify-)
	fd2 = temp2
	# Get the first entry from the input file
	result = fscan (fd2, r1, d1, data1)
	# Keep the Dec of the first entry in the list
	d4 = d1
	# Loop over all entries in the sorted Dec-group
	while (fscan (fd2, r2, d2, data2) != EOF) {
	    # Write the previous entry to file in
	    # user specified format
	    if (keepcoords)
		printf (fmtstr, n, r1, d1, data1, >> temp3)
	    else
		printf (fmtstr, n, data1, >> temp3)
	    # Calculate the separation between the
	    # current and the previous point. Note that
	    # the declination is fixed to d4 because all
	    # entries in this list are in the same Dec
	    # group
	    gosskyangle (r1, d4, r2, d4, raunit=raunit, verb-)
	    # If the difference in RA between the current
	    # entry and the previous one smaller than sep,
	    # then set the r1 (previous entry) to the
	    # current and move on to the next entry
	    if (gosskyangle.sep <= sep) {
		r1 = r2; d1 = d2; data1 = data2
		next
	    }

	    # If we reach this point, there has been
	    # a jump in RA, so we increase the output
	    # file counter ...
	    n += 1
	    # ... and reset r1 to contain the previous
	    # entry at the start of the next iteration
	    r1 = r2; d1 = d2; data1 = data2
	}
	# Close the input file and delete the sorted list
	fd2 = ""; delete (temp2, verify-)

	# Make sure the last point also gets written
	if (keepcoords)
	    printf (fmtstr, n, r1, d1, data1, >> temp3)
	else
	    printf (fmtstr, n, data1, >> temp3)

	# Now write out the lists and check for duplicates which must be
	# merged.

	# Sort the file containing the grouping information
	# by the output file counter, and delete the unsorted file
	sort (temp3, col=1, num+, > temp1); delete (temp3, verify-)
	# Create an empty file
	touch (temp2)
	# Open the file sorted by file counter
	fd1 = temp1
	# Read the first line
	result = fscan (fd1, i, data1)
	# Loop over the rest of the file
	while (fscan (fd1, j, data2) != EOF) {
	    # 
	    if (data1 == data2) {
	        print (j, i, >> temp2)
		next
	    }
	    printf ("%s_%03d%s\n", out, i, extn) | scan (fname)
	    print (data1, >> fname)
	    i = j; data1 = data2
	}
	fd1 = ""; delete (temp1, verify-)
	printf ("%s_%03d%s\n", out, i, extn) | scan (fname)
	print (data1, >> fname)
	sort (temp2, col=1, num+, rev+) | unique (> temp1)
	delete (temp2, verify-)

	# Merge the lists.
	n1 = n
	fd1 = temp1
	while (fscan (fd1, j, i) != EOF) {
	    printf ("%s_%03d%s\n", out, j, extn) | scan (fname)
	    if (access (fname)) {
		printf ("%s_%03d%s\n", out, i, extn) | scan (in)
		concat (in, fname, append+)
		delete (in, verify-)
		n1 -= 1
	    }
	}
	fd1 = ""; delete (temp1, verify-)

	# Renumber if needed.
	if (n1 != n) {
	    i = 1
	    for (j=1; j<=n; j+=1) {
		printf ("%s_%03d%s\n", out, j, extn) | scan (fname)
	        if (access(fname)) {
		    if (i != j) {
			printf ("%s_%03d%s\n", out, i, extn) | scan (in)
			rename (fname, in)
		    }
		    i += 1
		}
	    }
	}

	# Create the final output list of lists.
	files (out//"_[0-9]*", > out//extn)
end
