# MEFGETCAT -- Get a catalog of coordinates.
# This routine is an interface to whatever catalog access method is used.
# The returned catalog consists of RA, DEC, K, H, K

# TODO: include limit on e.g., K magnitude

procedure wcsgetcat (ra, dec, radius, cat)

real	ra			{prompt="RA center (hr)"}
real	dec			{prompt="DEC center (deg)"}
real	radius			{prompt="Radius (deg)"}
file	cat			{prompt="Output catalog"}

real	jmax = 100.		{prompt="Maximum J magnitude"}
real	hmax = 100.		{prompt="Maximum H magnitude"}
real	kmax = 100.		{prompt="Maximum K magnitude"}
int	nmax = INDEF		{prompt="Maximum number of sources"}
real	equinox = 2000.		{prompt="Equinox (yr)"}
real	epoch = 2000.		{prompt="Epoch (yr)"}
struct	*list2

begin
	file	out, temp1, temp2
	string	rstr, dstr
	real	r, d, rad, x1, x2, x3

	# Set temporary file.
	temp1 = mktemp ("tmp$wcsgetcat")
	temp2 = temp1 // "1"

	# Define packages and tasks.
	task $scat = "$!scat -c tmc -n -1 -r $3 $1 $2"

	# Set input parameters.
	r = ra * 15
	d = dec
	rad = radius * 3600.
	out = cat

	# Get the catalog data.  In this case we use USNO-B1.0 with SCAT.
	printf( "%H\n", r ) | scan( s1 )
	printf( "%h\n", d ) | scan( s2 )
	scat (s1, s2, rad) | fields ("STDIN", "2-6", lines="11-", > temp1)

	list2 = temp1
	while (fscan (list2, rstr, dstr, x1, x2, x3) != EOF) {
	    if ( x1<jmax && x2<hmax && x3<kmax ) {
		printf ("%s %s %5.2f %5.2f %5.2f\n", rstr, dstr, x1, x2, x3,
		    >> temp2 )
	    }
	    ;
	}
	list2 = ""; delete (temp1)

	if (isindef(nmax))
	    rename (temp2, out)
	else {
	    sort (temp2, col=4, num+, rev-) | head (nl=nmax, > out)
	    delete (temp1)
	}

end
