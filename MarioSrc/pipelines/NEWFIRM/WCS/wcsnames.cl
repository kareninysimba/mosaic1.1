# WCSNAMES -- Directory and filenames for the WCS pipeline.
#
# Directories will be terminated with '/'.
# Filenames will be without paths.
# Filenames that are images, or potentially images, do not include extensions
# except when a pattern is requested.

procedure wcsnames (name)

string  name                    {prompt = "Name"}

string  dataset                 {prompt = "Dataset name"}
string  pipe                    {prompt = "Pipeline name"}
string  shortname               {prompt = "Short name"}
file    rootdir                 {prompt = "Root directory"}
file    indir                   {prompt = "Input directory"}
file    datadir                 {prompt = "Dataset directory"}
file    uparm                   {prompt = "Uparm directory"}
file    pipedata                {prompt = "Pipeline data directory"}
file    parent                  {prompt = "Parent part of dataset"}
file    child                   {prompt = "Child part of dataset"}
file    lfile                   {prompt = "Log file"}
file    image                   {prompt = "Primary image name"}
file    hdr                     {prompt = "MEF header"}
file    sky                     {prompt = "MEF sky"}
file    sig                     {prompt = "MEF sky sigma"}
file	mcat			{prompt = "Merged 2MASS catalog"}
string  pattern = ""            {prompt = "Pattern"}
bool    base = no               {prompt = "Child part of dataset"}

begin
        # Set generic names.
        names ("wcs", name, pattern=pattern, base=base)
        dataset = names.dataset
        pipe = names.pipe
        shortname = names.shortname
        rootdir = names.rootdir
        indir = names.indir
        datadir = names.datadir
        uparm = names.uparm
        pipedata = names.pipedata
        parent = names.parent
        child = names.child
        lfile = names.lfile

        # Set pipeline specific names.
        image = shortname
        hdr = shortname // "_00.fits"
	mcat = shortname // "_2mass.mcat"
        sky = shortname // "_sky.fits"
        sig = shortname // "_sig.fits"

        if (pattern != "") {
            image += ".fits"
        }
end
