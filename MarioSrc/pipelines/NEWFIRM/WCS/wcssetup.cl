#!/bin/env pipecl
#
# MTDSTATS

int     status = 1
string  dataset, indir, datadir, ifile, im, lfile, gclimage, s4
struct  *fd

string	subdir
file	caldir = "MarioCal$/"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
wcsnames( envget("OSF_DATASET") )
dataset = wcsnames.dataset

# Set paths and files.
indir = wcsnames.indir
ifile = indir // dataset // ".wcs"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
	caldir = caldir // subdir

datadir = wcsnames.datadir
lfile = datadir // wcsnames.lfile
set (uparm = wcsnames.uparm)
set (pipedata = wcsnames.pipedata)
cd (datadir)

# Log start of processing.
printf( "\nWCSSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# The .wcs file presented to wcssetup.cl is a list of lists, 
# containing the return files from the GCL pipeline and from the
# SKY pipeline.
# First, extract the GCL return list, which is needed by wcsgcat,
# and write the contents to wcsgcl.
match( ".gcl", ifile, print- ) | scan( s1 )
concat( s1, >> "wcsgcl" )
# Remove the gcl return file from the list of list
match( ".gcl", ifile, print-, stop+, >> "wcssetup1.tmp" )
# Also update the original ifile
delete( ifile )
copy( "wcssetup1.tmp", ifile )
# Next, select only the fits files from the input lists
match( "*.fits", "@"//ifile, print-, >> "wcssetup2.tmp" )

# Setup uparm and pipedata
delete( substr( wcsnames.uparm, 1, strlen(wcsnames.uparm)-1 ) )
s1 = ""; head( "wcssetup2.tmp", nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

list = " wcssetup2.tmp"
while ( fscan( list, s1 ) != EOF ) {

    # Translate filter to best JHK passband and then get the
    # JHK to passband coefficients.  It is not a fatal error if
    # not found.

    s2 = substr( s1, strldx("/",s1)+1, strlen(s1) )

    getcal (s1//"[0]", "photindx", cm, "",
        obstype="", detector="!instrume", imageid="", filter="!filter", exptime="",
        mjd="!mjd-obs") | scan (i, line)
    if (i != 0)
        sendmsg ("WARNING", "Getcal failed for PHOTIDX", s2//" - "//line,
            "CAL")
    ;

    getcal (s1//"[0]", "photfilt", cm, "",
        obstype="", detector="!instrume", imageid="", filter="!photindx", exptime="",
        mjd="!mjd-obs") | scan (i, line)
    if (i != 0)
        sendmsg ("WARNING", "Getcal failed for PHOTFILT",
            s2//" - "//line, "CAL")
    ;
    getcal (s1//"[0]", "photcoef", cm, "",
        obstype="", detector="!instrume", imageid="", filter="!photfilt", exptime="",
        mjd="!mjd-obs") | scan (i, line)
    if (i != 0)
        sendmsg ("WARNING", "Getcal failed for PHOTCOEF",
            s2//" - "//line, "CAL")
    ;

    getcal (s1//"[0]", "MAGZREF", cm, "",
        obstype="", detector="!instrume", imageid="", filter="!filter",
        exptime="", mjd="!mjd-obs")
    if (getcal.statcode != 0) {
        getcal (s1//"[0]", "MAGZREF", cm, "",
            obstype="", detector="!instrume", imageid="", filter="!photindx",
            exptime="", mjd="!mjd-obs")
        if (getcal.statcode != 0)
            getcal (s1//"[0]", "MAGZREF", cm, "",
                obstype="", detector="!instrume", imageid="", filter="",
                exptime="", mjd="!mjd-obs")
        ;
    }
    ;
    if (getcal.statcode != 0)
        sendmsg ("WARNING", "Getcal failed for MAGZREF",
            s2//" - "//getcal.statstr, "CAL")
    ;


    s2 = substr( s1, strldx("/",s1)+1, strlstr("_ss",s1)-1 )
    if (sciencepipeline) { 
        # Get the name of the corresponding header in the wcsgcl file
        match( "_OGC?*"//s2, "wcsgcl" )
        match( "_OGC?*"//s2, "wcsgcl" ) | scan( gclimage )
        # Copy header keywords that were set by getcals above
        # to the OGC headers as well
        print( "magzref\nphotcoef\nphotfilt\nphotindx", > "wcssetup3.tmp" )
        fd = "wcssetup3.tmp"
        while( fscan( fd, s3 ) != EOF ) {
            hselect( s1//"[0]", s3, yes ) | scan( line )
            if ( nscan()==1 )
                hedit( gclimage, s3, line, add+, ver-, show+ )
            ;
        }
        fd = ""
    }
    ;



}
list = ""

delete( "wcssetup?.tmp" )

logout 1
