#!/bin/env python

"""
Capture data quality information from the output of ccmap and create
a .cl script to update the dataset and PMAS with.
"""

import re
import sys

outfile = open( 'wcswcs_ccmap_proc.cl', 'w' )

onwhitespace = re.compile('\s+')
infile = file( sys.argv[1], 'r')
for line in infile.readlines():
    thisline = line.strip()
fields = onwhitespace.split(thisline)
infile.close()

infile = file( fields[0] )
thedir = fields[1]
theid = fields[2]

thefile = thedir+theid

for line in infile.readlines():
    thisline = line.strip()

    m = re.match( 'Ra/Dec or Long/Lat fit rms:\s+(\d.*\d)\s+(\d.*\d)\s+\(arcsec', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"objectimage\", id=\"%s\", dm=dm," % (theid)
        print >> outfile, "    keyword=\"dqwcccxr\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"objectimage\", id=\"%s\", dm=dm," % (theid)
        print >> outfile, "    keyword=\"dqwcccyr\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( \"%s\", \"dqwcccxr\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (thefile, m.group(1))
        print >> outfile, "hedit( \"%s\", \"dqwcccyr\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (thefile, m.group(2))

    m = re.match( 'X and Y scale:\s+(\d.*\d)\s+(\d.*\d)\s+\(arcsec', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"objectimage\", id=\"%s\", dm=dm," % (theid)
        print >> outfile, "    keyword=\"dqwcccxs\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"objectimage\", id=\"%s\", dm=dm," % (theid)
        print >> outfile, "    keyword=\"dqwcccys\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( \"%s\", \"dqwcccxs\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (thefile, m.group(1))
        print >> outfile, "hedit( \"%s\", \"dqwcccys\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (thefile, m.group(2))

infile.close()
outfile.close()


