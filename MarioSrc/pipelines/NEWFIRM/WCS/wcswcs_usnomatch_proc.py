#!/bin/env python

"""
Capture data quality information from the output of usnomatch and create
a .cl script to update the dataset and PMAS with.
"""

import re
import sys

outfile = open( 'wcswcs_usnomatch_proc.cl', 'w' )

infile = file( sys.argv[1], 'r')
for line in infile.readlines():
    thisline = line.strip()
    m = re.match( 'x offset is (.*) pixels', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumxo\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcumxo\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
    m = re.match( 'y offset is (.*) pixels', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumyo\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcumyo\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
    m = re.match( 'rotation is (.*) degrees', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumro\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcumro\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
    m = re.match( 'total matched (.*) out of (.*) detections and (.*) references$', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumts\", value=\"%s\" )" % (m.group(2))
        frac = float(m.group(1))/float(m.group(3))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcummf\", value=\"%f\" )" % (frac)
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcumts\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(3))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcummf\", \"%f\", add+, update+, verify-, show+, >> lfile)" % (frac)
    print thisline
    m = re.match( '\w+ dmagzero = (.*) \+- (.*)$', thisline )
    if m:
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcummz\", value=\"%s\" )" % (m.group(1))
        print >> outfile, "setkeyval( class=\"mefobjectimage\", id=wcsnames.shortname, dm=dm,"
        print >> outfile, "    keyword=\"dqwcumme\", value=\"%s\" )" % (m.group(2))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcummz\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(1))
        print >> outfile, "hedit( wcsnames.hdr, \"dqwcumme\", \"%s\", add+, update+, verify-, show+, >> lfile)" % (m.group(2))

infile.close()
outfile.close()


