#!/bin/env pipecl
#
# strcrop
#
# Exit status:
#
#   1: Successful
#   0: Unsuccessful (image does not lie within first-pass stack)
#
# Description:
# Extract subimage from the first-pass (harsh, median) STK stack to
# match the field of a non-resampled SIF.  After extracting the
# subimage, the cropped stack is scaled such that its magzero will
# equal that of the non-resampled SIF.  Since the stack and
# non-resampled SIF are already sky subtracted in the NEWFIRM
# pipeline, the sky levels are already matched.  In this way, the
# cropped stack may be directly compared with the non-resampled SIF in
# order to identify cosmic rays, artifacts, and transients.

# History:
#			
# T. Huard  200808--    Copied over from the MOSAIC pipeline, and made
#			changes necessary for NEWFIRM.
# T. Huard  20081125    Cleaned up code following suggestions from Rob.
#                       These includes using SPKSCALE instead of MAGZERO
#                       to scale the cropped stack.  (It should yield
#                       the same result, but can be verified by looking
#                       at the log file.)
# T. Huard  20081126    Limited module width to 80 chars.  Changed "exitval"
#                       to "status" for consistency with other modules.
# T. Huard  20090510    The scaling factor to match the cropped first-pass
#                       stack to the second-pass sky subtracted image was
#                       revised.  Previously, this was taken to be SPKSCALE,
#                       which was fine before the rejection procedures were
#                       implemented.  Since, in principle, the first pass
#                       and second pass may have a different set of images
#                       comprising the stacks (since they have independent
#                       rejection procedures run), spkscale is not the
#                       necessary scaling factor to use here.  Furthermore,
#                       since now SPKSCALE is set to 0 for images not to be
#                       included in the second-pass stack (see spkselect.cl)
#                       but these images may be resampled (and go through
#                       transient-removal process) here in SPI, it is
#                       important to use the MAGZERO values in the headers
#                       of the image and first-pass stack to derive the
#                       scaling factor appropriate in this module.
# T. Huard  20090510    Images that are resampled in the second pass may not
#                       necessarily fall within the field of the first-pass
#                       stack since criteria for resampling and rejection
#                       are different.  For this reason, it is necessary
#                       to check whether the cropped stack is legitimate.
#                       For now, spicrop simply checks whether at least one
#                       pixel in the cropped stack is legitimate.  Perhaps
#                       in the future, the requirement should be a certain
#                       fraction of the SIF be covered by the stack to
#                       ensure success at the spitrdet module.  If the
#                       crop is not legitimate, exit with status 0.  Also,
#                       some restructuring of the module was done.
# T. Huard  20090510    Encountered a few cases where it was not sufficient to
#                       check for at least one legitimate pixel in the crop.
#                       So, now the module requires that at least 1/4 of the 
#                       SIF be covered by the first-pass stack.  This fraction
#                       could be changed, depending on future tests.
# Last Revised:  T. Huard  20090510  9:10pm

# Declare variables
int     status = 1
string  dataset, datadir, lfile, image, fpstack, croppedim, flat
#real    stackmagzero,skymean0,skymean,I0
real    magzero,magzeroFP,scale

# Load packages
redefine mscred = mscred$mscred.cl
noao
nproto
#nfextern
#ace
mscred

# Set file and path names
print(envget("OSF_DATASET"))
spinames(envget("OSF_DATASET"))
dataset = spinames.dataset
datadir = spinames.datadir
lfile = spinames.lfile
mscred.logfile = lfile
mscred.instrument = "pipedata$mosaic.dat"
set ( uparm = spinames.uparm )
set ( pipedata = spinames.pipedata )

print( datadir )
cd( datadir )

# Log start of processing.
printf ("\nSTRCROP (%s): ", dataset) | tee (lfile)
time | tee (lfile)

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
concat( "fpstack_p" ) | scan( fpstack )
spinames( image )

if ( whenflatfield == "after" ) {
    # Retrieve the location of the flat field from the header, if available
    flat = "" ; hselect( image, "FLAT", yes ) | scan( flat )
    if (flat == "") {
        printf( "No flat field was applied!\n" ) | tee( lfile )
    } else {
        # Dome flat available, so divide data by flat
        imarith( image, "/", flat, image )
        printf( "Applied flat field: %s\n", flat ) | tee( lfile )
    }
}
;

# Use mscimage to extract appropriate subimage from harsh (median) first-pass
# STK stack for the non-resampled SIF.
mscimage( fpstack, "tmp1.fits", format="image", pixmask+,
    wcssource="match", reference=image, ra=INDEF, dec=INDEF, scale=INDEF,
    rotation=INDEF, blank=0., interpol="linear", minterpol="linear",
    boundary="reflect", constant=0., fluxcon-, ntrim=8, nxblock=INDEF,
    nyblock=INDEF, interact-, nx=10, ny=20, fitgeom="general", xxorder=4,
    xyorder=4, xxterms="half", yxorder=4, yyorder=4, yxterms= "half" )

# Rename the mask created by mscimage
imrename( "tmp1_bpm.pl" , spinames.croppedbpm )
# Add the BPM created by mscimage to the cropped image
hedit( "tmp1.fits", "BPM", spinames.croppedbpm, show-, ver- )

# Check that the sky-subtracted image intersected at least some part of
# the first-pass stack.  If not, the cropped stack will be all 0's and
# its bad pixel mask will be all 2's.  I check that at least 25% of the
# bad pixel mask are 0's.  If so, then it is a legitimate crop; if not, 
# then it isn't.
imstat(spinames.croppedbpm,lower=0,upper=0,format-,fields="npix") | scan(i)
imstat(spinames.croppedbpm,lower=INDEF,upper=INDEF,format-,
    fields="npix") | scan(j)
x=real(i)/j
    
if (x >= 0.25) {

    # Determine scaling factor for cropped first-pass stack image (fpstack)
    # in order for its transparency (or magzero) to match that of the
    # second-pass sky subtracted image (image).  At this point, image has
    # not been scaled.  In spkselect, a scaling factor (spkscale) has been
    # computed for image for it to match the best transparency of all images
    # that will be stacked in the second pass stack.  Since, in principle,
    # the first pass and second pass may have a different set of images
    # comprising the stacks (since they have independent rejection procedures
    # run), spkscale is not the necessary scaling factor to use here.
    # Furthermore, since now SPKSCALE is set to 0 for images not to be included
    # in the second-pass stack (see spkselect.cl) but these images may be 
    # resampled (and go through transient-removal process) here in SPI,
    # it is important to use the MAGZERO values in the headers of the
    # image and first-pass stack to derive the scaling factor appropriate
    # here.
    magzero=INDEF ; hselect(image,"MAGZERO",yes) | scan(magzero)
    magzeroFP=INDEF ; hselect(fpstack,"MAGZERO",yes) | scan(magzeroFP)
    scale=10**((magzero-magzeroFP)/2.5)

    # The result of mscimage is a subimage extracted from the first-pass
    # stack, reprojected back to the unresampled SIF used here. However, it
    # still needs to be scale back to the same transparency / magzero of
    # the orginal image.
    croppedim = spinames.croppedim
    imarith( "tmp1.fits", "*", scale, croppedim )

    # Make sure the magzero is the same as in the original unresampled
    # and unscaled image
    hedit( croppedim, "MAGZERO", magzero, show-, ver- )

} else {

    # Clean up and set status to 0.
    imdelete(spinames.croppedbpm,verify-)
    status=0
    
}

# Clean up.
delete("tmp*.fits",verify-)

logout( status )
