#!/bin/env pipecl
#
# SPISTACK

real	ra, dec
string  datadir, dataset, ilist, lfile, indir, image
string	interpolant
struct  *list2

# Load the packages.
redefine mscred=mscred$mscred.cl
images
lists
noao
nproto
mscred
#cache mscred
#Why is the "cache mscred" necessary?

# Set the dataset name and uparm.
names( "spi", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spi"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPIRESAMPLE (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
spinames( image )

# Only consider images with a WCS solution
s2 = "F"
hselect( image, "WCSCAL", yes ) | scan( s2 )
if ( s2 == "F" ) {
    sendmsg( "WARNING", "Not resampling image without WCS", s1, "VRFY" )
    logout 2
}
;

if (sciencepipeline)
    interpolant = rsp_interp
else
    interpolant = "linear"
hselect( image, "RSPRA,RSPDEC", yes ) | scan( ra, dec )
if ( nscan() != 2 ) {
    sendmsg( "ERROR", "Could not retrieve reprojection center", 
        s1, "PROC" )
    logout 0
}
;
# Copy the contents of the CBPM keyword to the BPM keyword, because
# the latter is read by mscimage.
hedit( image, "BPM", "(cbpm)", update+, ver- )
mscimage( image, spinames.resampled, ra=ra, dec=dec, verbose+,
    scale=rsp_scale, rotation=0, wcssource="parameters",
    interpolant=interpolant )
print "DEBUG2"
imstat( image )
print "DEBUG3"
imstat( spinames.resampled )

logout 1
