#!/bin/env pipecl

int	status = 1
int     namps, ir, ix, iy, rad, continue
real    mjd, x1, x2, skymode
string  datadir, dataset, image, indir, lfile, rtnfile, rfile
string	sig, sky, obm, cat, nocmpat, nocdpat, fsobject, shortname
string  wcs, cast
struct  *list2

# Load packages.
servers
utilities
proto
tables
ttools
images
noao
nproto
nfextern
ace
dataqual

# Set file and path names.
spinames (envget("OSF_DATASET"))
dataset = spinames.dataset

# Set filenames.
indir = spinames.indir
datadir = spinames.datadir
lfile = datadir // spinames.lfile
set (uparm = spinames.uparm)
set (pipedata = spinames.pipedata)

# Log start of processing.
printf( "\nSPIACE (%s): ", dataset ) | tee (lfile)
time | tee( lfile )

cd( datadir )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations
concat( "image_p" ) | scan( image )
spinames( image )

sky   = spinames.sky
sig   = spinames.sig
obm   = spinames.obm
cat   = spinames.cat
imcopy( image, spinames.image )

delete (sky//"*")
delete (sig//"*")
delete (obm//"*")
delete (cat//"*")
delete ("spiaceimg.fits*,objmask.fits*")

imacedq (spinames.image, "!CBPM", "spiaceimg", sky, sig, obm, cat, lfile,
    "", "spiace", convolve="", hsigma=4, minpix=6, ngrow=2, agrow=2.,
    catdefs="pipedata$swcace.def", magzero="!MAGZERO", verbose=2, mosaic+)
if (imacedq.status != 1) {
    # Delete the local copy so that it will not be passed on
    # to downstream pipelines if ACE failed
    # TODO downstream such that deletion here is not needed
#    imdel( image )
    logout 0
}
;

# Analyze results and write to header.
s3 = mktemp ("spiace.")
if (imaccess(sky)) {

    # In four-shooter mode (or 4Q or Q mode) ACE's sky image will be
    # contaminated with emission from the extended source. To get around
    # this, sky is only considered in the area outside a circle with a
    # radius given by sky_4qradius (a percentage of half the diagonal).

    nocmpat = "" ; hsel( image, "NOCMPAT", yes ) | scan( nocmpat )
    nocdpat = "" ; hsel( image, "NOCDPAT", yes ) | scan( nocdpat )
    fsobject = "" ; hsel( image, "FSOBJECT", yes ) | scan( fsobject )
    if ( ( (nocmpat=="4Q") || (nocdpat=="4Q") || (nocdpat=="Q") ) && (fsobject=="yes") ) {
	# Get the size of the sky image and determine the radius of the
	# circle in pixels.

	hsel( image, "NAXIS1,NAXIS2", yes ) | scan( ix, iy )
	ir = ix
	if ( iy < ir )
	    ir = iy
	;
	# Divide by 2 (diameter to radius) and 100 (percent to fraction)
	rad = ir*sky_4qradius/200
	ix = ix/2
	iy = iy/2
	printf( "%d %d %d\n", ix, iy, rad ) | scan( line )
	print( line )
	# Fit linear surface to the region outside that circle
	imsurfit( sky, "fitsky", xorder=2, yorder=2, cross_terms+,
	    function="legendre", type_output="fit",
	    regions="invcircle", circle=line )
	# Subtract it from the original image.
	imdel ("spiaceimg")
	printf( "Run aceall for sky subtraction\n", >> lfile )
	aceall (image, skyotype="subsky", skyimages="spiaceimg",
	    skies="fitsky", masks="", exps="", gains="", scales="",
	    extnames="", logfiles=lfile, verbose=2, rimages="",
	    rmasks="", rexps="", rgains="", rscales="", roffset="",
	    rcatalogs="", catalogs="", catdefs="", catfilter="",
	    order="", nmaxrec=INDEF, magzero="INDEF", gwtsig=INDEF,
	    gwtnsig=INDEF, objmasks="+_obm", omtype="all",
	    sigimages="", sigmas="", rskies="", rsigmas="",
	    fitstep=100, fitblk1d=10, fithclip=2., fitlclip=3.,
	    fitxorder=1, fityorder=1, fitxterms="half", blkstep=1,
	    blksize=-10, blknsubblks=2, hdetect=yes, ldetect=no,
	    updatesky=yes, bpdetect="1-100", bpflag="1-100",
	    convolve="bilinear 3 3", hsigma=3., lsigma=10.,
	    neighbors="8", minpix=6, sigavg=4., sigmax=4.,
	    bpval=INDEF, rfrac=0.5, splitstep=0.4, splitmax=INDEF,
	    splitthresh=5., sminpix=8, ssigavg=10., ssigmax=5.,
	    ngrow=2, agrow=2.)
	imdel( "fitsky" )
    }
    ;

    # Replace image with sky subtracted version.
    imdel( image ); imrename( "spiaceimg", image )
}
;

logout 1
