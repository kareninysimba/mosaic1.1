#!/bin/env pipecl
#
# SPISETUP

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ilist, lfile, maskname, fpstack
string  image

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
spinames( envget("OSF_DATASET") )

# Set paths and files.
dataset = spinames.dataset
datadir = spinames.datadir
lfile = datadir // spinames.lfile
indir = spinames.indir
ilist = indir // dataset // ".spi"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = spinames.uparm)
set (pipedata = spinames.pipedata)

# Log start of processing.
printf( "\nSPISETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

cd (datadir)

# Remove the name of the first-pass stack from the input list and
# store its location in a file. This makes the file fpstack_p 
# reminiscent of pointer, hence the _p.
match( "_stk", ilist, print-, > "fpstack_p" )
match( "_stk", ilist, print-, stop+, > "spisetup1.tmp" )
# Update the original ilist
delete( ilist )
rename( "spisetup1.tmp", ilist )

# Extract the image to be worked on from the input list
head( ilist, nlines=1 ) | scan( s1 )
s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
spinames( s2 )
image = spinames.shortname
# Copy the image to here
imcopy( s1, image )
# Write the name of the image to image_p for the downstream
# modules to use
print( image, > "image_p" )

# Retrieve the location of the first pass stack
concat( "fpstack_p" ) | scan( fpstack )
# The bad pixel mask for the stack is assumed to be in the same
# location as the stack itself. Retrieve the name of the BPM from
# the header, and construct the full path based on the fpstack.
hsel( fpstack, "BPM", yes ) | scan( s1 )
printf ("bpm='%s'\nfpstack='%s'\n", s1, fpstack)
s2 = substr( fpstack, 1, strldx("/",fpstack) ) // s1
# Copy the stack bad pixel mask to here. This is needed because the
# full path to the original location contained in s2 cannot be written
# to the BPM keyword.
imcopy( s2, s1 )

# Setup uparm and pipedata
delete( substr( spinames.uparm, 1, strlen(spinames.uparm)-1 ) )
iferr {
    setdirs( image )
} then {
    logout 0
} else
    ;

getcal( image, "bpm", cm, caldir,
    obstype="!obstype", detector="!instrume", imageid="!detector",
    filter="!filter", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
if (i != 0) {
    sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
        "CAL")
    logout 0
}
;

if ( whenflatfield == "after" ) {
    # Try to retrieve the required flat from the database. This was
    # done in ogcsetup as well, but there is no guarantee that the
    # same extension runs on the same node. To ensure that the 
    # required flat is present, the getcal is done here as well.
    hedit( image, "flat", del+, ver- )
    getcal( image, "dflat", cm, caldir,
        obstype="object", detector="!instrume", imageid="!detector",
        filter="!filter", exptime="", mjd="!mjd-obs" ) |
            scan( i, statusstr )
    if (i != 0) {
        if (ogc_noflatcont == NO) {
            # No dome flat available, not OK to continue, so throw error
            sendmsg( "ERROR", "Getcal failed for flat",
                str(i)//" "//statusstr, "CAL" )
            status = 0
        }
        ;
    }
    ;
}
;

# Retrieve the cumulative pixel mask
# Read keywords needed needed for the getcal
hsel( image, "OBSID", yes ) | scan( s3 )
# Retrieve the location of the cumulative mask
getcal( image, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
    match="%"//s3, obstype="", detector="", filter="" )
if (getcal.statcode>0) {
    sendmsg( "ERROR", "Getcal failed with message: "//getcal.statstr,
        shortname, "PROC" )
    logout 0
}
;
# Extract the file name from the full path
maskname = substr( getcal.value, strldx("/",getcal.value)+1, 999 )
# Copy the cumulative mask to here ...
iferr {
    imcopy( getcal.value, maskname )
} then {
    printf ("Failed to copy cumulative mask...try again\n")
    sleep 5
    imcopy( getcal.value, maskname )
} else
    ;
# ... and add it to the header
hedit( image, "CBPM", maskname, add+, update+, ver- )
# Write the full path of the cumulative bad pixel mask to file
# for later modules to use. This eliminates the need for downstream
# getcals.
print( getcal.value, >> "cbpm_p" )

logout( status )
