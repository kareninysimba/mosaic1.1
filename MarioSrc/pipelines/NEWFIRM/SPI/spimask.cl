#!/bin/env pipecl
#
# SPIMASK

int	first = 1
string  datadir, dataset, ilist, lfile, indir, image
string  bpm, bpm1, bpm2
struct  *list2

# Load the packages.
redefine mscred=mscred$mscred.cl
images
lists
noao
nproto
mscred
cache mscred

# Set the dataset name and uparm.
names( "spi", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spi"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPISTACK (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

# Retrieve the names of the image and the stack being processed
# from the files containing their locations.
concat( "image_p" ) | scan( image )
spinames( image )

# Set the name of the mask created by mscimage in spiresample.cl.
# This mask is single-valued.
bpm = spinames.resampled // "_bpm.pl"
# Read the value of the bpm keyword (set by spisetup) from the
# original (not reprojected) image. This mask is multiple-valued.
hselect( image, "CBPM", yes ) | scan( bpm1 )	
# Set a temporary name ...
bpm2 = "srsrsp_tmp.pl"
# ... and rename the mscimage-created mask, so bpm2 now
# contains the mask created by mscimage
imrename( bpm, bpm2 )
# Define how mskexpr should work
printf ("reset pmmatch = \"world 9\"\nkeep\n") | cl
# Match the multiple-valued mask (bpm1) to the reprojected mask
# (bpm2 after rename), and write the result over the original
# output of mscimage
mskexpr( "m>0?m:(i>0?i: 0)", bpm, bpm2, refmask=bpm1 )
# Update the value of the bpm keyword in the header
hedit( spinames.resampled, "BPM", bpm, add+, ver-, update+ )
# Clean up
imdelete( bpm2 )

if ( do_cleanup ) {
    imdelete( bpm1 )
    imdel( spinames.croppedim )
    imdel( spinames.croppedbpm )
}
;

logout 1
