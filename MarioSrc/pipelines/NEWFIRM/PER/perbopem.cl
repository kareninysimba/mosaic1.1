#!/bin/env pipecl
#
# PERBOPEM - Bright Object and PErsistence Masks

int status = 1
int founddir,nlines,prevavail
real scale,fowler,coadd,bright
string dataset,datadir,ilist,lfile,prevmask,s4,trimsec,headerfile,extname
string shortname
struct *fd1

file    caldir = "MC$"

# Tasks and packages.
images
lists
noao
imred
crutil
proto
servers

# Set paths and files.
names ("per", envget("OSF_DATASET"))
dataset = names.dataset
datadir = names.datadir
ilist = names.indir // dataset // ".per"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Work in data directory.
founddir = 0
while (founddir==0) {
    if (access (datadir)) {
        founddir = 1
    } else {
        sleep 1
        printf("Retrying to access %s\n", datadir)
    }
}

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

cd (datadir)

# Start with the simplest possible implementation for persistence 
# masking: simply mask object brighter than a certain threshold in the
# N preceding images.

# Check whether the persistence masks already exist. The code below can
# be optimized quite a bit by doing an sql query to the database that
# returns all or some relevant subset of the persistence masks, rather
# than doing a getcal for all entries in the list.
# There is another gotcha that needs to be worked out. The masks created
# here are added to later in the pipeline, and hence become the 
# cumulative pixel masks. This means that great care will have to be taken
# in the downstream pipeline to use only the appropriate pixels from the
# masks. Otherwise, when masks are combined, e.g., transient pixels may
# be flagged when they are not yet supposed to be, and they may also
# be fixpixed. Until this has been addressed, the code below is 
# not used.
# Another thing to note is that dirper1.cl will remove all mask entries
# from the database. So probably a configuration parameter needs to be
# introduced to control whether or not existing masks are intended to be
# reused, and if so, that these should not be deleted.
# 
#list = ilist
#prevavail = 0
#i = 1
#touch( "perbopem1.tmp" )
#while ( fscan( list, s1 ) != EOF ) {
#
#    hselect( s1, "OBSID", yes ) | scan( s2 )
#    # Retrieve the location of the cumulative mask
#    getcal( s1, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
#        match="%"//s2, obstype="", detector="", filter="" )
#
#    if ( getcal.statcode == 0 ) {
#        # Persistence mask is available in the database, check whether
#        # the file also exists.
#       
#        if ( imaccess( getcal.value ) ) {
#            # The file exists, so do not add the current image
#            # to the list of images without persistent masks.
#            prevavail = 1
#        } else {
#            # The file does not exist, let the next block of code
#            # add it to the list
#            getcal.statcode = 1
#        }
#    }
#
#    if (getcal.statcode>0) {
#        # Persistence mask is not yet available
#
#       if ( prevavail == 1 ) {
#            # Previous entry already had a persistence mask, so it had
#            # not been added to the list. Add the previous per_numexps
#            # entries. Duplications will be sorted out later. Note that
#            # it is not necessary to add the current image to the list
#            # for the persistence mask, but it is added because of how
#            # the code below works.
#
#            if (i>per_numexps) {
#                nlines = per_numexps
#            } else {
#                nlines = i-1
#            }
#            j = i-1
#	    head( ilist, nlines=j, > "perbopem2.tmp" )
#            tail( "perbopem2.tmp", nlines=nlines, >> "perbopem1.tmp" )
#
#        } else {
#            # Previous entry did not have a persistence mask, so it
#            # has already been added to the list.
#
#            print( s1, >> "perbopem1.tmp" )
#
#        }
#        prevavail = 0
#
#    }
#    ;
#
#    i = i + 1
#
#}
#list = ""
#sort( "perbopem1.tmp" ) | uniq( > "perbopem3.tmp" )
#delete( ilist )
#rename( "perbopem3.tmp", ilist )
#delete( "perbopem?.tmp" )
#
#count( ilist ) | scan( i )
#if ( i == 0 )
#    logout 1
#;

# Create bright object masks for all images. Adjust counts for
# digital averages, coadds, and Fowler sampling
list = ilist
while ( fscan( list, s1 ) != EOF ) {

    extname = substr( s1, strlstr("[",s1)+1, strlstr("]",s1)-1 )
    s2 = substr(s1,strldx("/",s1)+1,strlstr("[im",s1)-1 )//"_"//extname//"bom"
    s3 = substr(s1,strldx("/",s1)+1,strlstr("[im",s1)-1 )//"_"//extname//"per"
    shortname = substr(s1,strldx("/",s1)+1,999 )

    # Determine the scale factor
    fowler = 0
    hselect( s1, "FSAMPLE", yes ) | scan( fowler )
    coadd = 0
    hselect( s1, "NCOADD", yes ) | scan( coadd )
    scale = 1.*fowler*coadd

    # Detect bright objects
    bright = 8000.*scale
    printf( "i>%f", bright, > "perbopem1.tmp" )
    mskexpr( "@perbopem1.tmp", s2//".pl", s1, refmask="" )
    delete( "perbopem1.tmp" )
    prevmask = s2

    # Grow the mask
    crgrow( s2, "crgrow.pl", radius=5 )

    # Retrieve the trim section from the header
    hsel( s1, "TRIMSEC", yes ) | scan( trimsec )
    if ( nscan()!=1 ) {
        sendmsg( "ERROR", "Could not retrieve trimsec",
	    shortname, "VRFY" )
        status = 0
        break
    }
    ;
    # Copy the trimsec only, i.e., remove the reference pixels and
    # delete the section keywords. Reuse the old file name (s2).
    imdel( s2 )
    imcopy( "crgrow.pl"//trimsec, s2//".pl" )
    # Delete temporary file
    imdel( "crgrow.pl" )
    # Remove the various "section" keywords
    hedit( s2, "DETSEC,DATASEC,TRIMSEC,BIASSEC", del+, verify-, show- )

    # Write the bright object and persistence mask file names to file
    print( s2, >> "perbopem2.tmp" )
    print( s3, >> "perbopem3.tmp" )

}
list = ""

# Create the persistence masks
fd1 = "perbopem2.tmp"
list = "perbopem3.tmp"
i = 1
while ( fscan( list, s1 ) != EOF ) {

    # Also read the bright object mask, because that file already
    # exists, whereas the persistence mask is created here and does
    # not have the correct keywords. Before this change, the information
    # putcalled into the calibration manager was incorrect: the
    # persistence mask for an image should be the sum over the
    # previous per_numexps exposures. However, because the
    # information for the putcal was read from the persistence
    # mask created with imsum (see below), the putcal used
    # the header from the last image in the sequence, thus effectively
    # making the persistence mask include the current image as well.
    if ( fscan( fd1, headerfile ) == EOF ) {
        sendmsg( "ERROR", "Mismatch in number of files", headerfile, "VRFY" )
        status = 0
        break
    } 

    # Extract the relevant bright object masks from the input list
    if (i>per_numexps) {
        nlines = per_numexps
    } else {
        nlines = i-1
    }
    if (i==1) {

        # Make an all ok mask (because there was no previous exposure
        # to create a persistence mask from)
        mskexpr( "1==0", s1//".pl", prevmask )

    } else {

        # Extract the relevant lines from the list of bright object masks
        # First, get all the previous masks
        j = i-1
	head( "perbopem2.tmp", nlines=j, > "perbopem4.tmp" )
        # And now only get the last nlines
        tail( "perbopem4.tmp", nlines=nlines, > "perbopem5.tmp" )
        # Create the persistence mask. First sum over all the
	# contributing bright object masks ...
        imsum( "@perbopem5.tmp", s1//"tmp.pl" )
	# ... and then set those that have a positive value to 6,
	# which designates pixels affected by persistence
        mskexpr( "i>0 ? 6 : 0", s1//".pl", s1//"tmp.pl" )

    }

    # Store the location of this mask in the calibration manager
    # First get the full location hostname and path
    pathname( s1 ) | scan( s2 )
    # In the putcal below the !keyword syntax cannot be used
    # because the "value=" keyword is used, so the keywords 
    # have to be read separately and supplied to putcal.
    hsel( headerfile, "OBSID,DETECTOR", yes ) | scan( s3, s4 )
    putcal( "", "keyword", cm, value=s2//".pl", class="masks",
        imageid=s4, match=names.parent//" "//s3,
        detector="", filter="", exptime="" )

    # Clean up
    imdel( s1//"tmp.pl" )
    delete( "perbopem4.tmp,perbopem5.tmp" )

    i = i+1

}
list = ""

logout( status )
