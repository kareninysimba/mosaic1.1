#!/bin/env pipecl
#
# MTDSETUP -- Setup MTD processing. 
# 
# This is to setup the link for pipedata which contains files used
# by mtdverify.

string  dataset, datadir, ifile, lfile

# Packages and tasks.
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# Set paths and files.
names ("mtd", envget("OSF_DATASET"))
dataset = names.dataset
print( dataset )
datadir = names.datadir
ifile = names.indir // dataset // ".mtd"
lfile = datadir // names.lfile
set (uparm = names.uparm)
set (pipedata = names.pipedata)
cd (datadir)

# Log start of processing.
printf ("\n%s (%s): ", envget("NHPPS_MODULE_NAME"), dataset) | tee (lfile)
time | tee (lfile)

# Set directories using the first input image.
delete (substr(names.uparm, 1, strlen(names.uparm)-1))
s1 = ""; head (ifile) | scan (s1)
print( s1 )
if (s1 == "")
    logout 1
iferr {
    setdirs (s1//"[0]")
} then 
    logout 0
else
    ;

logout 1
