#!/bin/env pipecl
#
# SPKGETMASKS
#
# History:
#
#   T. Huard  20090509  Clarified comment about ordering of olist (no longer
#       sorted by transparency, but not important).  Also, added check for
#       pre-existing rlist, which would occur in multiple runs of a dataset.
#   Last revised.   T. Huard    20090509    10:50am


int     status = 1
struct  statusstr
string  dataset, indir, datadir, ilist, im, lfile, maskname, shortname
string  olist, rlist, image

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
names( "spk", envget("OSF_DATASET") )

# Set paths and files.
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spk"
olist = "spkolist.tmp"
rlist = "spkrlist.tmp"
shortname = names.shortname

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

cd (datadir)

# Log start of processing.
printf( "\nSPKGETMASKS (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

# Concatenate the SPI return file into one list containing only
# the resampled images and masks.
concat( "*.spi", > "spkgetmasks1.tmp" )
match( "_ss2_rsp", "spkgetmasks1.tmp", > "spkgetmasks2.tmp" )
# Make a list of all the resampled masks
match( "bpm", "spkgetmasks2.tmp", >> "spkgetmasks3.tmp" )
# Make a list of all the resampled images
match( "bpm", "spkgetmasks2.tmp", stop+, >> "spkgetmasks4.tmp" )

# Loop over the input olist, find the matching cumulative bad pixel
# mask, and update the bpm keyword.
#
# THIS IS NO LONGER TRUE, BUT NOT IMPORTANT! -> Note that olist is
# sorted such
# that the image with the best transparency is first. This order is
# preserved for the list of resampled images in rlist.
if (access(rlist)) delete(rlist,verify-)
;
list = olist
while ( fscan( list, s1 ) != EOF ) {

     # Reset the variables
     mask = ""
     image = ""

     # Extract the filename without the path
     s2 = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )

     # Find the matching resampled mask for this input image
     match( s2, "spkgetmasks3.tmp" ) | scan( mask )
     # Find the matching resampled image for this input image
     match( s2, "spkgetmasks4.tmp" ) | scan( image )

     if ( ( mask != "" ) && ( image != "" ) ) {

         # Add the resampled image to rlist
         print( image, >> rlist )
         # Copy the resampled cumulative pixel mask to here
         s3 = substr( mask, strldx("/",mask)+1, 999 )
         imcopy( mask, s3 )
         # Add the name of the cumulative pixel mask to the header.
         # Normally this would be in the CBPM keyword, but imcombine
         # (in the next module) required the use of BPM
         hedit( image, "BPM", s3, add+, update+, ver- )

     } else {
         sendmsg( "WARNING",
             "No resampled image created; excluded from stack",
             image, "VRFY" )
     }

}
list = ""
delete( "spkgetmasks?.tmp" )

logout( status )
