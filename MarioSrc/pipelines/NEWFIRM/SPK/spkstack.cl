#!/bin/env pipecl
#
# SPKSTACK -- Stack exposures with weighting and rejection.
#
#
# Exit Status Values:
#
#   1 = Successful
#   0 = Unsuccessful: Stack was not created, even though accepted SIFs found
#   2 = Successful?: No acceptable SIFs found; stack not created.  This is 
#               not necessarily "unsuccessful" since it is possible that 
#               all SIFs are legitimately rejected - see spkselect.
#   3 = Successful?: One acceptable SIF found; stack created.
#
# History
#
# R. Swaters --------	Created.
# T. Huard   20081219	Added code to consider weighting by transparency
# 			and seeing, if the IRAF parameter "spk_weighting"
#			is set to 1.  Otherwise, the weighting is done
#			by exposure time.
# T. Huard   20090509   Duplicated recent changes to stkstack.cl to this
#                       module, including accounting for olist/resampling
#                       changes, creation of slist, the addition of REJECT
#                       keyword to SIFs, and the addition of WMETHOD keyword
#                       to the stack.  The declaration of variables has been
#                       cleaned up.  This module is essentially the same as
#                       stkstack.cl, with the exception of a bit more
#                       documentation in stkstack.cl and the important
#                       combine="median" in stkstack.cl and combine="average"
#                       here.  Changed exit status of 2 from ERROR to WARNING.
#   T. Huard  20090510  Add NCOMBINE to header in case of only one exposure
#               in list so that spkqual will work properly.  Also, reorganize
#               somewhat so that the WMETHOD keyword will be written in
#               such a case.  In the case of only one exposure to stack,
#               change exit status to 3 to tell spk.xml to skip spkqual.
#               Ideally, we want spkqual to run in this case, but I have not
#               yet gotten it to work.
#   Last Revised:  T. Huard  20090510  2:30pm

# Declare variables.
string  dataset,datadir,lfile,indir,ilist,rlist,slist,ofile,bpmfile,expmap
string  rule,method,wkey,msg
string	shortname
int	ncombine

# Load the packages.
images
proto

# Set the dataset name and uparm.
names( "spk", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
shortname = names.shortname
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".spk"
rlist = "spkrlist.tmp"
slist = "spkslist.tmp"
ofile = shortname //"_spk"
bpmfile = shortname//"_spkbpm"
expmap = shortname//"_spkexpmap"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSPKSTACK (%s): ", dataset ) | tee (lfile)
time | tee (lfile)

# Go to the relevant directory
cd( datadir )

# Create slist, a sorted list of images to be stacked.
if (access(slist)) delete(slist,verify-)
;
touch(slist)
hselect("@"//rlist,"$I,REJECT",yes,>"spkstack.tmp")
list="spkstack.tmp"
while(fscan(list,s1,s2) != EOF) {
    s3=substr(s2,1,1)
    if (s3 != "N")
        next
    ;
    s2 = s1
    i = strldx ("/", s2)
    if (i > 0)
        s2 = substr (s2, i+1, 999)
    ;
    printf("%s %s\n", s2, s1, >>slist)
}
list=""
delete("spkstack.tmp",verify-)
sort (slist) | fields ("STDIN", "2", > slist)
count(slist) | scan(ncombine)

# Check that the number of SIFs to combine is not 0. 
# If it is, then send message to monitor and logout with
# status of 2.
if (ncombine==0) {
    msg="No SIFs to stack, based on REJECT keyword."
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
    logout 2
}
;

# First, parse spk_weight to determine which weighting keyword to
# use for weighting SIFs.
rule=spk_weight
j=stridx(",",rule)
if (j != 0) {
    method=substr(rule,1,j-1)
} else {
    method=rule
}
wkey=""
if ((method == "total") || (method =="default")) {
    wkey="WTOT"
} else if (method == "see") {
    wkey="WSEEING"
} else if (method == "transns") {
    wkey="WTRNSNS"
}
if (wkey == "") {
    wkey="OEXPTIME"
    method="none"
    msg="spk_weight not recognized by spkstack. setting it to 'none'."
    print(msg)
    sendmsg("WARNING",msg,dataset,"PROC")
}
;
    
if (ncombine==1) {

    # Only one exposure in the list, so copy the input data
    imcopy( "@"//slist, ofile )
    hsel( "@"//slist, "BPM", yes ) | scan( s1 )
    imcopy( s1, bpmfile//".pl" )
    hselect(ofile,"OEXPTIME",yes) | scan(x)
    imcopy(ofile,ofile//"expmap.pl")
    imreplace(ofile//"expmap.pl",x,lower=INDEF,upper=INDEF)
        
} else {

    # Combine the exposures. Although it is expected that the 
    # exposure times are the same for all exposures in a sequence,
    # the imcombine below is set up such that this is not a
    # requirement, i.e., the exposure times may be different.
    # 
    # If the images are not scaled to exptime=1, one would
    # expect that imcombine would give a straight sum (or average)
    # of all the contributing exposures (at least in the skynoise
    # dominated regime). In the pipeline, the images are scaled
    # back to exposure time of 1s. 
    #
    # To combine the images properly, one could first scale the
    # image back to the original exposure time (with the scale= 
    # keyword). However, when using the scale keyword, the output
    # in the combined image is also scaled (by the average over all
    # images of the scale factor with respect the first image).
    # It can be complicating to calculate this scale factor 
    # because the number of images contributing to each pixel
    # in the output of combine can vary, 
    # 
    # Fortunately, the images can also be scaled properly by using
    # weights, and that is done here.
    # 
    # The scale keyword is also used to photometrically scale
    # the exposures before combining them.
    
    # Run imcombine.  The imcombine will use the masks defined in the
    # BPM keyword to identify bad pixels.    
    imcombine( "@"//slist, ofile, imcmb="",
        headers="", bpmasks=bpmfile//".pl", rejmasks="", nrejmasks="",
        expmasks=expmap//".pl", sigmas="", logfile=lfile, combine="average",
        reject="none", project=no, outtype="real", outlimits="",
        offsets="wcs", masktype="novalue", maskvalue="2", blank=0.,
        scale="!spkscale", zero="none", weight="!"//wkey, statsec="",
        expname="oexptime",
        lthreshold=INDEF, hthreshold=INDEF, nlow=1, nhigh=1, nkeep=1,
        mclip=yes, lsigma=3., hsigma=3., rdnoise="0.", gain="1.",
        snoise="0.", sigscale=0.1, pclip=-0.5, grow=0., mode="ql" )

} 

# Check if the stack was successful.
if (imaccess(ofile) == NO)
    logout 0
;

# Now set the ICMB keywords from the parent exposures.
list = slist; s2 = ""
for (ncombine=0; fscan(list,s1)!=EOF; ) {
    i = strldx ("/", s1) + 1
    if (i > 1)
	s1 = substr (s1, i, 999)
    ;
    i = stridx ("_", s1) - 1
    if (i > 1)
	s1 = substr (s1, 1, i)
    ;
    if (s1 == s2)
        next
    ;
    ncombine += 1
    printf ("IMCMB%03d\n", ncombine) | scan (s3)
    nhedit (ofile, s3, s1, "Contributing exposure (archive name)", add+)
    s2 = s1
}
list = ""

# Undate keywords.
nhedit (ofile, "NCOMBINE", ncombine, "Number stacked")
nhedit (ofile, "MAGZERO", "(SPKMZ)", ".")
nhedit (ofile, "WMETHOD", wkey, "Weighting method", add+)
nhedit (ofile, "DATASEC", del+)

# If there is only one exposure set exit status to 3 to to skip spkqual.
# Ideally, we want spkqual to run but I have not yet gotten it to work.
if (ncombine == 1)
    logout 3
;

logout 1
