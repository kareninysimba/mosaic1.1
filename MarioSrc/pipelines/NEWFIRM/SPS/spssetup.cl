#!/bin/env pipecl
#
# SPSSETUP

int     status = 1
struct  statusstr
string  dataset, indir, datadir, ifile, im, lfile, maskname, shortname

string  subdir
file    caldir = "MC$"

# Tasks and packages
task $cp = "$!cp -r $(1) $(2)"
task $ln = "$!ln -s $(1) $(2)"
images
servers

# get the dataset name
spsnames( envget("OSF_DATASET") )

# Set paths and files.
dataset = spsnames.dataset
datadir = spsnames.datadir
lfile = datadir // spsnames.lfile
indir = spsnames.indir
ifile = indir // dataset // ".sps"

subdir = "/" // envget ("NHPPS_SYS_NAME") // "/"
if (strstr (subdir, osfn (caldir)) == 0)
        caldir = caldir // subdir

set (uparm = names.uparm)
set (pipedata = names.pipedata)

# Log start of processing.
printf( "\nSPSSETUP (%s): ", dataset ) | tee( lfile )
time | tee (lfile)

cd (datadir)

# Setup uparm and pipedata
delete( substr( names.uparm, 1, strlen(names.uparm)-1 ) )
s1 = ""; head( ifile, nlines=1 ) | scan( s1 )
iferr {
    setdirs( s1 )
} then {
    logout 0
} else
    ;

# Get the calibrations.
list = ifile
while (fscan (list, s1, s2) != EOF) {

    shortname = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    spsnames( shortname )

    getcal (s1, "bpm", cm, caldir,
        obstype="!obstype", detector="!instrume", imageid="!detector",
        filter="!filter", exptime="", mjd="!mjd-obs") | scan (i, statusstr)
    if (i != 0) {
        sendmsg ("ERROR", "Getcal failed for bpm", str(i)//" "//statusstr,
            "CAL")
        status = 0
        break
    }
    ;

    # Retrieve the cumulative pixel mask
    # Read keywords needed needed for the getcal
    hsel( s1, "OBSID", yes ) | scan( s3 )
    # Retrieve the location of the cumulative mask
    getcal( s1, "masks", cm, caldir, imageid="!detector", mjd="!mjd-obs",
        match="%"//s3, obstype="", detector="", filter="" )
    if (getcal.statcode>0) {
        sendmsg( "ERROR", "Getcal failed with message: "//getcal.statstr,
	    shortname, "PROC" )
        status = 0
        break
    }
    ;
    # Extract the file name from the full path
    maskname = substr( getcal.value, strldx("/",getcal.value)+1, 999 )
    # Copy the cumulative mask to here ...
    imcopy( getcal.value, maskname )
    # ... and add it to the header
    hedit( s1, "CBPM", maskname, add+, update+, ver- )

    # Write the full path of the cumulative bad pixel mask to file
    # for later modules to use. This eliminates the need for downstream
    # getcals.
    print( getcal.value, >> spsnames.cbpmloc )

}
list = ""

logout 1
