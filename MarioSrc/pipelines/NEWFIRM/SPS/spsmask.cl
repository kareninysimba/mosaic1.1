#!/bin/env pipecl
#
# SPSMASK - Look for holes masks, and update the existing masks to
# include the holes, and fixpix sky subtracted images if the sky
# image has holes.

int	status = 1
string	dataset, indir, lfile, datadir, cbpm, cbpmloc
string  ilist, shortname

# Tasks and packages
images
proto
imfilter
noao
nproto
task $findmatch = "$!findmatch $1 $2"
task $paste = "$!paste -d\  $1 $2 > $3"

# Set file and path names.
spsnames( envget("OSF_DATASET") )
dataset = spsnames.dataset

# Set filenames.
indir = spsnames.indir
datadir = spsnames.datadir
ilist = indir // dataset // ".sps"
lfile = datadir // spsnames.lfile
set (uparm = spsnames.uparm)
set (pipedata = spsnames.pipedata)

# Log start of processing.
printf( "\nSPSSUB (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Loop over all images in the list

list = ilist
#set pmmatch = "logical"

while ( fscan( list, s1, s2 ) != EOF ) {
    
    shortname = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    spsnames( shortname )
   
    # Check whether the "holes" keyword is present. If it is, that means
    # that runmed produced a "holes" masks, which shows the number of 
    # contributing images to each pixel in the sky image. If that number
    # is zero, no sky could be determined at that position, i.e., there
    # is a hole in the sky image. Consequently, the sky in the sky-subtracted
    # image is not correct. Any pixel for which the hole mask contains a 0
    # should be masked in the sky-subtracted image as well.

    # Check whether the "holes" keyword is set. The holes keyword was
    # copied from the sky image to the sky-subtracted image in spssub.cl.
    s2 = ""
    hselect( spsnames.skysub, "HOLES", yes ) | scan( s2 )
    if ( s2 != "") {

        # The holes keyword is set. Merge it into the cumulative pixel
        # mask. First retrieve the cumulative pixel mask.
        hselect( spsnames.skysub, "CBPM", yes ) | scan( cbpm )
        # Merge the masks. Per PL015, no contributing pixels for the
        # sky image can either be seen as "no data", or as "poor/bad
        # calibration". For the time being, the latter, with code 5,
        # is used. 
        mskexpr( "((m>=1)&&(m<5))?m:(i==0? 5: m)", "tmpmask.pl", s2,
            refmask=cbpm )
        # Store the new cumulative pixel mask
	concat( spsnames.cbpmloc ) | scan( cbpmloc )
        imdel( cbpmloc )
        imrename( "tmpmask.pl", cbpmloc )

        # "Invert" the mask, i.e., 0 usually means good but for the
        # holes mask it means no data.
        mskexpr( "i==0?1 : 0", "tmpmask.pl", s2 )
        # Interpolate over the areas that were not sky-subtracted.
	fixpix( spsnames.skysub, masks="tmpmask.pl" )
        imdel( "tmpmask.pl" )
	
    } else {
        # The holes keyword is not set. This happens when the sky
        # is subtracted through other means than runmed.
        ;
    }

}
list = ""

logout( status )
