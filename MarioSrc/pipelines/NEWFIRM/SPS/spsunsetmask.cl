#!/bin/env pipecl
#
# SPSMASK - Reset the hole mask as found in the first-pass sky
# subtraction. Although it is likely that the hole mask that will
# be found in the second pass is similar to or larger than the one
# found in the first pass, it is best not to bias the result by
# excluding points a priori.

int	status = 1
string	dataset, indir, lfile, datadir, cbpm, cbpmloc
string  ilist, shortname

# Tasks and packages
images
proto
imfilter
noao
nproto
task $findmatch = "$!findmatch $1 $2"
task $paste = "$!paste -d\  $1 $2 > $3"

# Set file and path names.
spsnames( envget("OSF_DATASET") )
dataset = spsnames.dataset

# Set filenames.
indir = spsnames.indir
datadir = spsnames.datadir
ilist = indir // dataset // ".sps"
lfile = datadir // spsnames.lfile
set (uparm = spsnames.uparm)
set (pipedata = spsnames.pipedata)

# Log start of processing.
printf( "\nSPSUNSETMASK (%s): ", dataset ) | tee( lfile )
time | tee( lfile )

cd( datadir )

# Loop over all images in the list

list = ilist

while ( fscan( list, s1, s2 ) != EOF ) {
    
    shortname = substr( s1, strldx("/",s1)+1, strlstr(".fits",s1)-1 )
    spsnames( shortname )

    # The cumulative bad pixel mask has already been copied by
    # the spssetup.cl stage. Assuming that there typically are few
    # holes in the first-pass sky-subtracted data, the data are
    # first checked to see whether holes are present. The latter
    # is a fast operation, because this is a read of a local file.
    # Updating the mask can be slow, because that involves writing
    # a file that could be in a remote location.
    # First, get the name of the CBPM from the header:
    hselect( s1, "CBPM", yes ) | scan( s2 )
    # Determine the number of pixels with value 5
    imstat( s2, upper=5, lower=5, format-, fields="npix" ) | scan( i )
    if ( i > 0 ) {
        # Holes from the first pass were found, delete these
        # from the mask, i.e., replace 5 with 0.
        mskexpr( "i==5? 0: i", "tmpmask.pl", s2 )
        # Store the updated cumulative pixel mask
	concat( spsnames.cbpmloc ) | scan( cbpmloc )
        imdel( cbpmloc )
        imrename( "tmpmask.pl", cbpmloc )
    }
    ;

}
list = ""

logout( status )
