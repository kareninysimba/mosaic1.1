#!/bin/env pipecl
#
# SECSPKSTART
#
#
# History
#
# 	R. Swaters --------	Created.
#	T. Huard   20081114	The first-pass stack has been added
#				to the input lists.

int	status = 1
int	nmasks, ngroups
string	datadir, dataset, indir, ilist, lfile, outfile, listoflists
string  stacklist, skysublist, groupname, grouplist, stack1
string  temp1, temp2
struct  *list2

# Tasks and packages.
images
task $join = "$!awk \'{print \$1,\"$2\"}\' $1"
task $callpipe = "$!call.cl $1 $2 $3 '$4' 2>&1 > $5; echo $? > $6"

# Set the dataset name and uparm.
names( "sec", envget("OSF_DATASET") )
dataset = names.dataset
datadir = names.datadir
lfile = datadir // names.lfile
indir = names.indir
ilist = indir // dataset // ".sec"
set (uparm = names.uparm)

# Log the operation.
printf( "\nSECSPKSTART (%s):\n", dataset )

# Go to the relevant directory
cd( datadir )

# Now that the second-pass sky subtraction is completed, it is time
# to stack the data again. For this, the same groups that have been
# determined for the first-pass sky subtraction will be used.
#
# First, the original groups need to be found, then these should
# serve as templates to create the new lists with the second-pass
# sky-subtracted data, and finally these need to be sent off to the
# stacking pipeline. The first part is easy, because the secspsstart
# module has already created a list named secspkinput. The only
# thing left to do is to create a list of all the SPS products.

if (access("secspkinput")==NO)
    logout (1)
;

# Create the list of all SPS sky-subtracted images
concat( dataset // "*.sps", >> "secspkstart1.tmp" )
# Select only the sky-subtracted images
skysublist = "secspkstart2.tmp"
match( "_ss2.fits", "secspkstart1.tmp", >> skysublist )

# Loop over entries in the file containing the input groups, as set
# by secspsstart. Each entry in this file is a list of files that
# were stacked by the STK pipeline into one stack. This list will be
# used to create new lists from the SPS output.
grouplist = "secspkstart4.tmp"
list = "secspkinput"
while ( fscan( list, stacklist ) != EOF ) {

    # Extract the group name. Although it is possible
    # to rederive the group name from the first exposure
    # in the sequence like was done in gosstkgroup.cl, it
    # is extracted from the filename created there just in
    # case that what was the first file went missing.
    groupname = substr( stacklist, strldx("_",stacklist)+1,
        strldx(".",stacklist)-1 )

    # Match groupname against entries in the list of stacks
    # to identify which stack is associated with the the current
    # group.
    match(groupname//"_stk.fits",dataset//".stacked") | scan(stack1)

    # Redefine groupname
    groupname = "stack_" // groupname // ".grp"
          
    # Loop over all entries in this stacklist
    list2 = stacklist
    while ( fscan( list2, s1 ) != EOF ) {
        
        # Extract the root part of the filename
        s2 = substr (s1, strldx("/",s1)+1, strlstr(".fits",s1)-1)
        # Find the matching file in skysublist
	match( s2, skysublist ) | scan( s3 )
        # Print this name to a new group file
        print( s3, >> "secspkstart3.tmp" )

    }
    print(stack1,>>"secspkstart3.tmp")
    list2 = ""

    # Rename the temporary file to its proper group name
    rename( "secspkstart3.tmp", groupname )
    # Add the groupname to the grouplist
    print( groupname, >> grouplist )

}
list = ""

# Make sure the pathames are full IRAF path, 
pathnames( "@"//grouplist, > "secspkstart5.tmp" )
# Create the list of lists
listoflists = indir//dataset//"spk.lol"
# Rename the temporary file to the listoflists
rename( "secspkstart5.tmp", listoflists )

concat( listoflists )

# Call the SPK pipelines 
temp1 = "secspkstart6.tmp"
temp2 = "secspkstart7.tmp"
callpipe( "sec", "spk", "split", listoflists, temp1, temp2 )
concat (temp2) | scan (status)
concat (temp1)
printf("Callpipe returned %d\n", status)

logout( status )
