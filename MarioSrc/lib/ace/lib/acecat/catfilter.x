include	<acecat.h>
include	<acecat1.h>
include	<aceobjs.h>
include	<evvexpr.h>
include	"filter.h"


bool procedure filter (catptr, rec, filt)

pointer	catptr			#I Catalog pointer
pointer	rec			#I Record structure
char	filt[ARB]		#I Filter string
bool	match			#O Filter return value

int	type, locpr()
pointer	o, evvexpr()
extern	filt_op(), filt_func()
errchk	evvexpr

pointer	cat
common	/filtcom/ cat

begin
	if (catptr == NULL || rec == NULL)
	    return (false)
	if (filt[1] == EOS)
	    return (true)

	# Evaluate filter.
	cat = catptr
	o = evvexpr (filt, locpr (filt_op), rec, locpr (filt_func), rec, 0)
	if (o == NULL)
	    return (false)

	type = O_TYPE(o)
	if (O_TYPE(o) == TY_BOOL)
	    match = (O_VALI(o) == YES)

	call mfree (o, TY_STRUCT)
	if (type != TY_BOOL)
	    call error (1, "Filter expression is not boolean")

	return (match)
end


procedure filt_op (rec, name, o)

pointer	rec			#I Record structure
char	name[ARB]		#I Field name
pointer	o			#O Pointer to output operand

int	id, func, itype, otype
pointer	stp, ufunc, optr, sym
pointer	stfind()

pointer	cat
common	/filtcom/ cat

begin
	id = -1
	if (cat != NULL) {
	    ufunc = CAT_UFUNC(cat)
	    stp = CAT_STP(cat)
	    if (stp != NULL) {
		call strcpy (name, CAT_STR(cat), CAT_SZSTR)
		call strupr (CAT_STR(cat))
		sym = stfind (stp, CAT_STR(cat))
		if (sym != NULL) {
		    id = ENTRY_ID(sym)
		    itype = ENTRY_TYPE(sym)
		    func = ENTRY_FUNC(sym)
		}
	    }
	}

	if (id == -1) {
	    #call xvv_error1 ("Field `%s' not found", name)
	    call xvv_initop (o, 0, TY_INT)
	    O_TYPE(o) = 0
	    return
	}

	if (ufunc != NULL) {
	    optr = CAT_BUF(cat)
	    call zcall6 (ufunc, cat, rec, id, itype, optr, otype)
	    call catwfunc (cat, func, optr, otype, optr, otype)
	} else
	    call catwfunc (cat, func, rec+id, itype, optr, otype)

	switch (otype) {
	case TY_INT:
	    call xvv_initop (o, 0, TY_INT)
	    O_VALI(o) = RECI(optr,0)
	case TY_REAL:
	    call xvv_initop (o, 0, TY_REAL)
	    O_VALR(o) = RECR(optr,0)
	case TY_DOUBLE:
	    call xvv_initop (o, 0, TY_DOUBLE)
	    O_VALD(o) = RECD(optr,0)
	default:
	    #call xvv_error1 ("Field `%s' not numeric", name)
	    call xvv_initop (o, -otype, TY_CHAR)
	    call strcpy (RECT(optr,0), O_VALC(o), -otype)
	}
end



procedure filt_func (rec, func, args, nargs, o)

pointer	rec			#I Record structure
char	func[ARB]		#I Function
pointer	args[ARB]		#I Arguments
int	nargs			#I Number of arguments
pointer	o			#O Function value operand

int	ifunc, strdic()
pointer	sp, buf
bool	strne()

begin
	call smark (sp)
	call salloc (buf, SZ_LINE, TY_CHAR)

	ifunc = strdic (func, Memc[buf], SZ_LINE, FILT_FUNCS)
	if (ifunc == 0 || strne (func, Memc[buf]))
	    call xvv_error1 ("unknown function `%s'", func)
end
