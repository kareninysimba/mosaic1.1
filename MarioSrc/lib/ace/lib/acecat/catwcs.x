include	<imhdr.h>
include	<acecat.h>
include	<acecat1.h>


# CATWCS -- Set catalog WCS information.

procedure catwcs (cat, im, idx, idy)

pointer	cat			#I Catalog pointer
pointer	im			#I IMIO pointer
int	idx			#I Field ID for longitude
int	idy			#I Field ID for latitude

int	i
pointer	sp, axtype, label, units, format
pointer	hdr, mw, tbl, tp, stp, sym, cdef

bool	streq()
pointer	mw_openim(), sthead(), stnext()
errchk	mw_openim

begin
	if (cat == NULL)
	    return
	if (CAT_OUTTBL(cat) == NULL)
	    return

	call smark (sp)
	call salloc (axtype, SZ_FNAME, TY_CHAR)
	call salloc (label, SZ_FNAME, TY_CHAR)
	call salloc (units, SZ_FNAME, TY_CHAR)
	call salloc (format, SZ_FNAME, TY_CHAR)

	hdr = CAT_OHDR(cat)
	tbl = CAT_OUTTBL(cat)
	tp = TBL_TP(tbl)
	stp = TBL_STP(tbl)

	mw = mw_openim (im)
	do i = 1, 2 {
	    iferr (call mw_gwattrs (mw, i, "axtype", Memc[axtype], SZ_FNAME))
		Memc[axtype] = EOS
	    iferr (call mw_gwattrs (mw, i, "label", Memc[label], SZ_FNAME)) {
		if (streq (Memc[axtype], "ra"))
		    call strcpy ("RA", Memc[label], SZ_FNAME)
		else if (streq (Memc[axtype], "dec"))
		    call strcpy ("DEC", Memc[label], SZ_FNAME)
		else
		    Memc[label] = EOS
	    }
	    iferr (call mw_gwattrs (mw, i, "units", Memc[units], SZ_FNAME)) {
		if (streq (Memc[axtype], "ra"))
		    call strcpy ("hr", Memc[units], SZ_FNAME)
		else if (streq (Memc[axtype], "dec"))
		    call strcpy ("deg", Memc[units], SZ_FNAME)
		else
		    Memc[units] = EOS
	    }
	    iferr (call mw_gwattrs (mw, i, "format", Memc[format], SZ_FNAME)) {
		if (streq (Memc[axtype], "ra"))
		    call strcpy ("%.2h", Memc[format], SZ_FNAME)
		else if (streq (Memc[axtype], "dec"))
		    call strcpy ("%.1h", Memc[format], SZ_FNAME)
		else
		    Memc[format] = EOS
	    }

	    if (i == 1) {
		for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
		    if (ENTRY_ID(sym) != idx)
			next
#		    if (!(streq (Memc[stname(stp,sym)], "WX") ||
#			streq (Memc[stname(stp,sym)], "wx")))
			Memc[label] = EOS
		    break
		}
	    } else {
		for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
		    if (ENTRY_ID(sym) != idy)
			next
#		    if (!(streq (Memc[stname(stp,sym)], "WY") ||
#			streq (Memc[stname(stp,sym)], "wy")))
			Memc[label] = EOS
		    break
		}
	    }

	    if (sym != NULL) {
		cdef = ENTRY_CDEF(sym)
		if (cdef != NULL) {
		    if (Memc[label] != EOS)
			call tbcnam (tp, cdef, Memc[label])
		    if (Memc[units] != EOS)
			call tbcnit (tp, cdef, Memc[units])
		    if (Memc[format] != EOS)
			call tbcfmt (tp, cdef, Memc[format])
		}
	    }
	}

	# Save MWCS in table header.
	# Use mw_saveim to format keywords in a dummy image header.
	call mw_saveim (mw, hdr)

	call mw_close (mw)

	call sfree (sp)
end
