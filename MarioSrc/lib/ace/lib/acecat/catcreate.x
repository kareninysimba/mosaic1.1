include	<acecat.h>
include	<acecat1.h>


procedure catcreate (cat)

pointer	cat			#I Catalog structure

pointer	tbl, tp

begin
	if (cat == NULL)
	    return
	tbl = CAT_OUTTBL(cat)
	if (tbl == NULL)
	    return
	tp = TBL_TP(tbl)
	if (tp == NULL)
	    return
	if (CAT_INTBL(cat) != NULL) {
	    if (tp == TBL_TP(CAT_INTBL(cat)))
		return
	}
	call tbtcre (tp)
end
