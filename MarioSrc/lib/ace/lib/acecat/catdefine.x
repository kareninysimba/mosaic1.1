include	<acecat.h>
include	<acecat1.h>


# CATDEF -- Read catalog definition file and create symbol table.

procedure catdefine (cat, tbl, mode, catdef, structdef)

pointer	cat			#I Catalog pointer
pointer	tbl			#I Table pointer
int	mode			#I Table access mode
char	catdef[ARB]		#I Catalog definition file
char	structdef[ARB]		#I Application structure definition file

int	i, j, k, n, fd, func, ncols, reclen, fnt
real	rap
pointer	sp, fname, name, label, str, entry, sym
pointer	stp1, stp2, tp

bool	streq()
int	open(), stropen(), fscan(), nscan(), fntopn(), fntgfn()
int	stridxs(), strldxs(), strdic(), strlen(), ctor()
pointer	stopen(), stenter(), stfind(), sthead(), stnext(), stname()
errchk	catdefine1, catdefine2,  open, stropen, stopen, tbcdef1, tbcfnd1

define	err_	10

begin
	call smark (sp)
	call salloc (fname, SZ_FNAME, TY_CHAR)
	call salloc (name, SZ_FNAME, TY_CHAR)
	call salloc (label, SZ_LINE, TY_CHAR)
	call salloc (str, SZ_LINE, TY_CHAR)
	call salloc (entry, ENTRY_LEN, TY_STRUCT)
	call aclri (Memi[entry], ENTRY_LEN)

	if (tbl != NULL)
	    tp = TBL_TP(tbl)

	# Set the application entry structure.
	if (structdef[1] != EOS)
	    call catdefine1 (cat, structdef, stp1, reclen)
	else if (CAT_INTBL(cat) != NULL)
	    call catdefine2 (cat, TBL_TP(CAT_INTBL(cat)), stp1, reclen)
	else if (tp != NULL && mode != NEW_FILE)
	    call catdefine2 (cat, tp, stp1, reclen)
	else
	    call error (1, "catdefine: no catalog structure definition")

	# Read the catalog definitions.
	fd = NULL
	fnt = NULL
	if (catdef[1] == '@')
	    fd = open (catdef[2], READ_ONLY, TEXT_FILE)
	else if (catdef[1] == '#')
	    fd = stropen (catdef[2], strlen(catdef[2]), READ_ONLY)
	else if (stridxs (",", catdef) > 0)
	    fnt = fntopn (catdef)

	stp2 = stopen ("catdefine", 100, ENTRY_LEN, SZ_LINE)
	ncols = 0
	func = 0
	rap = INDEFR
	n = 1
	repeat {
	    # Parse catalog field translation.
	    if (fd == NULL && fnt == NULL) {
		if (ncols == 0)
		    sym = sthead(stp1)
		else
		    sym = stnext(stp1,sym)
		if (sym == NULL)
		    break
		call strcpy (Memc[stname(stp1,sym)], Memc[name], SZ_FNAME)
		if (catdef[1] != EOS && !streq (catdef, structdef)) {
		    if (ncols == 0)
		        call sscan (catdef)
		    if (ENTRY_READ(sym) == YES || ENTRY_WRITE(sym) == YES) {
			call gargwrd (Memc[str], SZ_LINE)
			if (Memc[str] != EOS)
			   call strcpy (Memc[str], Memc[name], SZ_FNAME)
		    }
		}

	    } else {
		if (fd != NULL) {
		    if (fscan(fd) == EOF)
			break

		    call gargwrd (Memc[name], SZ_FNAME)
		    call gargwrd (Memc[label], SZ_LINE)
		    n = nscan()
		    if (n == 0)
			next
		    if (Memc[name] == '#')
			next
		} else {
		    if (fntgfn (fnt, Memc[name], SZ_FNAME) == EOF)
		        break
		}

		# Parse the name.
		call strcpy (Memc[name], Memc[str], SZ_LINE)
		call strupr (Memc[str])
		func = 0
		rap = INDEFR
		j = stridxs ("(", Memc[str]) + 1
		if (j > 1) {
		    # Check if name is a function.
		    i = strldxs (")", Memc[str])
		    Memc[str+j-2] = EOS
		    Memc[str+i-1] = EOS
		    func = strdic (Memc[str], Memc[fname], SZ_FNAME, FUNCS)
		    if (func == 0) {
			call strcpy (Memc[name], Memc[str], SZ_LINE)
			call strupr (Memc[str])
		    } else
			call strcpy (Memc[str+j-1], Memc[str], SZ_LINE)

		    # Get field name and any arguments.
		    k = stridxs ("(", Memc[str]) + 1
		    if (k > 1) {
			i = strldxs (")", Memc[str])
			Memc[str+k-2] = EOS
			Memc[str+i-1] = EOS
			if (ctor (Memc[str], k, rap) == 0)
			    goto err_
		    }
		}
		
		sym = stfind (stp1, Memc[str])
	    }

	    if (sym == NULL) {
err_
		call stclose (stp1)
		call stclose (stp2)
		call close (fd)
		call sprintf (Memc[label], SZ_LINE,
	"Unknown or ambiguous catalog quantity `%s' in definition file `%s'")
		    call pargstr (Memc[name])
		    call pargstr (Memc[fname])
		call error (1, Memc[label])
	    }
	    if (tbl == NULL) {
		ncols = ncols + 1
		next
	    }

	    if (n == 1) {
		call strcpy (Memc[name], Memc[label], SZ_LINE)

		# Replace '()' with '[]' in name.
		if (j > 1) {
		    for (i=0; Memc[label+i]!=EOS; i=i+1) {
			if (Memc[label+i] == '(')
			    Memc[label+i] = '['
			else if (Memc[label+i] == ')')
			    Memc[label+i] = ']'
		    }
		}
	    }

	    if ((mode!=NEW_FILE&&mode!=NEW_COPY) && ENTRY_READ(sym) == YES) {
		call tbcfnd1 (tp, Memc[label], ENTRY_CDEF(sym))
		#if (ENTRY_CDEF(sym) == NULL)
		#    next
	    }

	    entry = stenter (stp2, Memc[label], ENTRY_LEN)
	    call amovi (Memi[sym], Memi[entry], ENTRY_LEN)
	    ENTRY_FUNC(entry) = func
	    ENTRY_RAP(entry) = rap

	    switch (ENTRY_FUNC(entry)) {
	    case FUNC_MAG:
		ENTRY_CTYPE(entry) = TY_REAL
		call strcpy ("magnitudes", ENTRY_UNITS(entry), ENTRY_ULEN)
		ENTRY_FORMAT(entry) = EOS
	    }
	    
	    if ((mode==NEW_FILE||mode==NEW_COPY) && ENTRY_WRITE(entry) == YES)
		call tbcdef1 (tp, ENTRY_CDEF(entry), Memc[label],
		    ENTRY_UNITS(entry), ENTRY_FORMAT(entry),
		    ENTRY_CTYPE(entry), 1)
	    #else
	#	call tbcfnd1 (tp, Memc[label], ENTRY_CDEF(entry))

	    ncols = ncols + 1
	}
	if (fd != NULL)
	    call close (fd)
	if (fnt != NULL)
	    call fntcls (fnt)

	if (cat == NULL)
	    call stclose (stp1)
	else {
	    if (CAT_STP(cat) != NULL)
	        call stclose (CAT_STP(cat))
	    CAT_RECLEN(cat) = reclen
	    CAT_STP(cat) = stp1
	}

	if (tbl == NULL) {
	    call stclose (stp2)
	    return
	}

	if (ncols == 0) {
	    call stclose (stp2)
	    call sprintf (Memc[label], SZ_LINE,
		"No catalog quantity definitions in file `%s'")
		call pargstr (Memc[fname])
	    call error (1, Memc[label])
	}

	# Reverse order of symbol table.
	stp1 = stopen ("catdef", ncols, ENTRY_LEN, SZ_LINE)
	for (sym=sthead(stp2); sym!=NULL; sym=stnext(stp2,sym)) {
	    entry = stenter (stp1, Memc[stname(stp2,sym)], ENTRY_LEN)
	    call amovi (Memi[sym], Memi[entry], ENTRY_LEN)
	}
	call stclose (stp2)

	TBL_STP(tbl) = stp1

	call sfree (sp)
end


# CATDEF1 -- Create record symbol table from a structure definition.

procedure catdefine1 (cat, structdef, stp, reclen)

pointer	cat			#I Catalog pointer
char	structdef[ARB]		#I Application structure definition file
pointer	stp			#O Symbol table
int	reclen			#O Record length

int	i, j, n, nalloc, id, fd
pointer	sp, fname, name, label, entry
pointer	stp1, sym

bool	streq()
int	open(), fscan(), nscan(), strncmp(), ctoi()
pointer	stopen(), stenter(), sthead(), stnext(), stname()
errchk	open, stopen

begin
	stp = NULL
	reclen = 0

	if (cat != NULL) {
	    if (CAT_DEFS(cat) != NULL) {
		do i = 1, CAT_RECLEN(cat)
		    call mfree (CAT_DEF(cat,i), TY_STRUCT)
		call mfree (CAT_DEFS(cat), TY_POINTER)
	    }
	    CAT_NF(cat) = 0
	}

	call smark (sp)
	call salloc (fname, SZ_FNAME, TY_CHAR)
	call salloc (name, SZ_FNAME, TY_CHAR)
	call salloc (label, SZ_LINE, TY_CHAR)
	call salloc (entry, ENTRY_LEN, TY_STRUCT)
	call aclri (Memi[entry], ENTRY_LEN)

	# Read the application structure definitions.
	fd = open (structdef, READ_ONLY, TEXT_FILE)

	stp1 = stopen ("catdefine", 100, ENTRY_LEN, SZ_LINE)
	n = 0
	nalloc = 0
	while (fscan(fd) != EOF) {
	    Memc[fname] = EOS
	    call gargwrd (Memc[fname], SZ_FNAME)
	    if (!streq (Memc[fname], "define"))
		next
	    call gargwrd (Memc[name], SZ_FNAME)
	    call strupr (Memc[name])
	    if (strncmp (Memc[name], "ID_", 3) != 0)
		next
	    call gargi (ENTRY_ID(entry))
	    id = ENTRY_ID(entry)
	    call gargwrd (Memc[label], SZ_LINE)
	    if (Memc[label] != '#')
		next
	    call gargwrd (Memc[label], SZ_LINE)
	    call gargwrd (ENTRY_UNITS(entry), ENTRY_ULEN)
	    call gargwrd (ENTRY_FORMAT(entry), ENTRY_FLEN)
	    call gargwrd (Memc[fname], SZ_FNAME)

	    if (ENTRY_UNITS(entry) == '/')
	        j = 5
	    else if (ENTRY_FORMAT(entry) == '/')
	        j = 6
	    else
		j = nscan()

	    switch (j) {
	    case 5:
	        ENTRY_UNITS(entry) = EOS
	        ENTRY_FORMAT(entry) = EOS
	    case 6:
	        ENTRY_FORMAT(entry) = EOS
	    }

	    # Decode type string.
	    i = 1
	    switch (Memc[label]) {
	    case 'i':
		ENTRY_TYPE(entry) = TY_INT
		i = 2
	    case 'r':
		ENTRY_TYPE(entry) = TY_REAL
		i = 2
	    case 'd':
		ENTRY_TYPE(entry) = TY_DOUBLE
		i = 2
	    default:
		if (ctoi (Memc[label], i, ENTRY_TYPE(entry)) == 0)
		    next
		ENTRY_TYPE(entry) = -ENTRY_TYPE(entry)
	    }
	    ENTRY_CTYPE(entry) = ENTRY_TYPE(entry)

	    switch (Memc[label+i-1]) {
	    case 'i':
		ENTRY_READ(entry) = NO
		ENTRY_WRITE(entry) = NO
	    case 'r':
		ENTRY_READ(entry) = YES
		ENTRY_WRITE(entry) = NO
	    case 'w':
		ENTRY_READ(entry) = NO
		ENTRY_WRITE(entry) = YES
	    default:
		ENTRY_READ(entry) = YES
		ENTRY_WRITE(entry) = YES
	    }

	    sym = stenter (stp1, Memc[name+3], ENTRY_LEN)
	    call amovi (Memi[entry], Memi[sym], ENTRY_LEN)

	    if (id > 1000)
	        next

	    switch (ENTRY_TYPE(entry)) {
	    case TY_INT, TY_REAL:
		n = max (n, id+1)
	    case TY_DOUBLE:
		n = max (n, id+2)
	    default:
		n = max (n, id-(ENTRY_TYPE(entry)/2)+1)
	    }

	    if (cat != NULL) {
		if (nalloc == 0) {
		    nalloc = 100
		    call calloc (CAT_DEFS(cat), nalloc, TY_POINTER)
		} else if (id >= nalloc) {
		    call realloc (CAT_DEFS(cat), id+100, TY_POINTER)
		    call aclri (CAT_DEF(cat,nalloc), id+100-nalloc)
		    nalloc = id + 100
		}
		if (CAT_DEF(cat,id) == NULL)
		    call calloc (CAT_DEF(cat,id), CAT_DEFLEN, TY_STRUCT)
		CAT_TYPE(cat,id) = ENTRY_TYPE(entry)
		CAT_READ(cat,id) = ENTRY_READ(entry)
		CAT_WRITE(cat,id) = ENTRY_WRITE(entry)
		call strcpy (Memc[name+3], CAT_NAME(cat,id), CAT_SLEN)
		call strcpy (ENTRY_UNITS(entry), CAT_FORMAT(cat,id), CAT_SLEN)
		call strcpy (ENTRY_FORMAT(entry), CAT_FORMAT(cat,id), CAT_SLEN)
		CAT_NF(cat) = CAT_NF(cat) + 1
	    }
	}
	call close (fd)

	if (n == 0) {
	    call stclose (stp1)
	    stp = NULL
	    call error (1, "catdefine1: No field definitions found")
	}

	# Reverse order of symbol table.
	stp = stopen ("catdef", n, ENTRY_LEN, SZ_LINE)
	for (sym=sthead(stp1); sym!=NULL; sym=stnext(stp1,sym)) {
	    entry = stenter (stp, Memc[stname(stp1,sym)], ENTRY_LEN)
	    call amovi (Memi[sym], Memi[entry], ENTRY_LEN)
	}
	call stclose (stp1)

	reclen = n
	if (cat != NULL) {
	    call realloc (CAT_DEFS(cat), reclen, TY_POINTER)
	    CAT_RECLEN(cat) = reclen
	}

	call sfree (sp)
end


# CATDEF2 -- Create catalog definition symbol table from a table.

procedure catdefine2 (cat, tp, stp, reclen)

pointer	cat			#I Catalog pointer
pointer	tp			#I Table pointer
pointer	stp			#O Symbol table
int	reclen			#O Record length

int	i, n, nalloc, id, colnum, nelem, lenfmt
pointer	sp, name, entry
pointer	cdef, stp1, sym

pointer	tbcnum()
pointer	stopen(), stenter(), sthead(), stnext(), stname()
errchk	stopen, tbcnum, tbcinf

begin
	stp = NULL
	reclen = 0

	if (cat != NULL) {
	    if (CAT_DEFS(cat) != NULL) {
		do i = 1, CAT_RECLEN(cat)
		    call mfree (CAT_DEF(cat,i), TY_STRUCT)
		call mfree (CAT_DEFS(cat), TY_POINTER)
	    }
	    CAT_NF(cat) = 0
	}

	call smark (sp)
	call salloc (name, SZ_FNAME, TY_CHAR)
	call salloc (entry, ENTRY_LEN, TY_STRUCT)
	call aclri (Memi[entry], ENTRY_LEN)

	stp1 = stopen ("catdefine", 100, ENTRY_LEN, SZ_LINE)
	n = 0
	nalloc = 0
	do i = 1, ARB {
	    cdef = tbcnum (tp, i)
	    if (cdef == NULL)
	        break

	    call tbcinf (cdef, colnum, Memc[name], ENTRY_UNITS(entry),
	        ENTRY_FORMAT(entry), ENTRY_TYPE(entry), nelem, lenfmt)
	    if (nelem > 1)
	        next

	    id = n
	    switch (ENTRY_TYPE(entry)) {
	    case TY_INT, TY_REAL:
		n = id + 1
	    case TY_DOUBLE:
	        id = (id + 1) / 2 * 2
		n = id + 2
	    default:
		n = id - (ENTRY_TYPE(entry) / 2) + 1
	    }

	    ENTRY_ID(entry) = id
	    ENTRY_CTYPE(entry) = ENTRY_TYPE(entry)
	    ENTRY_READ(entry) = YES
	    ENTRY_WRITE(entry) = YES

	    call strupr (Memc[name])
	    sym = stenter (stp1, Memc[name], ENTRY_LEN)
	    call amovi (Memi[entry], Memi[sym], ENTRY_LEN)

	    if (cat != NULL) {
		if (nalloc == 0) {
		    nalloc = 100
		    call calloc (CAT_DEFS(cat), nalloc, TY_POINTER)
		} else if (id >= nalloc) {
		    call realloc (CAT_DEFS(cat), id+100, TY_POINTER)
		    call aclri (CAT_DEF(cat,nalloc), id+100-nalloc)
		    nalloc = id + 100
		}
		if (CAT_DEF(cat,id) == NULL)
		    call calloc (CAT_DEF(cat,id), CAT_DEFLEN, TY_STRUCT)
		CAT_TYPE(cat,id) = ENTRY_TYPE(entry)
		CAT_READ(cat,id) = ENTRY_READ(entry)
		CAT_WRITE(cat,id) = ENTRY_WRITE(entry)
		call strcpy (Memc[name], CAT_NAME(cat,id), CAT_SLEN)
		call strcpy (ENTRY_UNITS(entry), CAT_FORMAT(cat,id), CAT_SLEN)
		call strcpy (ENTRY_FORMAT(entry), CAT_FORMAT(cat,id), CAT_SLEN)
		CAT_NF(cat) = CAT_NF(cat) + 1
	    }
	}

	if (n == 0) {
	    call stclose (stp1)
	    stp = NULL
	    call error (1, "catdefine2: No table field definitions found")
	}

	# Reverse order of symbol table.
	stp = stopen ("catdef", n, ENTRY_LEN, SZ_LINE)
	for (sym=sthead(stp1); sym!=NULL; sym=stnext(stp1,sym)) {
	    entry = stenter (stp, Memc[stname(stp1,sym)], ENTRY_LEN)
	    call amovi (Memi[sym], Memi[entry], ENTRY_LEN)
	}
	call stclose (stp1)

	reclen = n
	if (cat != NULL) {
	    call realloc (CAT_DEFS(cat), reclen, TY_POINTER)
	    CAT_RECLEN(cat) = reclen
	}

	call sfree (sp)
end
