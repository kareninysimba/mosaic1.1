include	<acecat.h>
include	<acecat1.h>


procedure catwrecs (cat, filt)

pointer	cat			#I Catalog pointer
char	filt[ARB]		#I Filter string

int	i, j
pointer	recs, rec

bool	filter()

begin
	if (cat == NULL)
	    return
	if (CAT_OUTTBL(cat) == NULL)
	    return
	if (CAT_RECS(cat) == NULL)
	    return

	recs = CAT_RECS(cat)
	j = 0
	do i = 1, CAT_NRECS(cat) {
	    rec = Memi[recs+i-1]
	    if (rec == NULL)
		next
	    if (!filter (cat, rec, filt))
		next
	    j = j + 1
	    call catwrec (cat, rec, j)
	}
end


procedure catwrec (cat, rec, row)

pointer	cat			#I Catalog pointer
pointer	rec			#I Record pointer
int	row			#I Table row

int	id, func, itype, otype
pointer	tbl, tp, stp, ufunc, optr
pointer	sym, cdef, sthead(), stnext()

begin
	if (rec == NULL)
	    return

	tbl = CAT_OUTTBL(cat)
	if (tbl == NULL)
	    return
	tp = TBL_TP(tbl)
	stp = TBL_STP(tbl)
	ufunc = CAT_UFUNC(cat)

	for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
	    if (ENTRY_WRITE(sym) == NO)
	        next
	    id = ENTRY_ID(sym)
	    itype = ENTRY_TYPE(sym)
	    cdef = ENTRY_CDEF(sym)
	    func = ENTRY_FUNC(sym)
	    if (cdef == NULL)
	        next

	    # Apply function.
	    if (ufunc != NULL) {
		optr = CAT_BUF(cat)
		call zcall6 (ufunc, cat, rec, id, itype, optr, otype)
		call catwfunc (cat, func, optr, otype, optr, otype)
	    } else if (id <= 1000)
		call catwfunc (cat, func, rec+id, itype, optr, otype)
	    else {
	        optr = CAT_BUF(cat)
		otype = itype
		switch (otype) {
		case TY_INT:
		    RECI(optr,0) = INDEFI
		case TY_REAL:
		    RECR(optr,0) = INDEFR
		case TY_DOUBLE:
		    RECD(optr,0) = INDEFD
		default:
		    RECT(optr,0) = EOS
		}
	   }

	    # Output value.
	    switch (otype) {
	    case TY_INT:
		call tbepti (tp, cdef, row, RECI(optr,0))
	    case TY_REAL:
		call tbeptr (tp, cdef, row, RECR(optr,0))
	    case TY_DOUBLE:
		call tbeptd (tp, cdef, row, RECD(optr,0))
	    default:
		call tbeptt (tp, cdef, row, RECT(optr,0))
	    }
	}
end


# CATWFUNC -- Apply a function.
# The output pointer will be the same as the input pointer if the value
# is not changed otherwise it will be a pointer to the catalog working buffer.

procedure catwfunc (cat, func, iptr, itype, optr, otype)

pointer	cat			#I Catalog structure
int	func			#I Function ID
pointer	iptr			#I Pointer to input value
int	itype			#I Datatype of input value
pointer	optr			#U Pointer to output value
int	otype			#U Datatype of output value

pointer	ptr

begin
	switch (func) {
	case FUNC_MAG:
	    switch (itype) {
	    case TY_INT:
		ptr = iptr
		optr = CAT_BUF(cat)
		otype = TY_REAL
		if (RECI(ptr,0) > 0 && !IS_INDEFI(RECI(ptr,0)))
		    RECR(optr,0) = -2.5 * log10 (real(RECI(ptr,0))) +
		        CAT_MAGZERO(cat)
		else
		    RECR(optr,0) = INDEFR
	    case TY_REAL:
		ptr = iptr
		optr = CAT_BUF(cat)
		otype = TY_REAL
		if (RECR(ptr,0) > 0. && !IS_INDEFR(RECR(ptr,0)))
		    RECR(optr,0) = -2.5 * log10 (RECR(ptr,0)) +
		        CAT_MAGZERO(cat)
		else
		    RECR(optr,0) = INDEFR
	    case TY_DOUBLE:
		ptr = iptr
		optr = CAT_BUF(cat)
		otype = TY_DOUBLE
		if (RECD(ptr,0) > 0. && !IS_INDEFD(RECD(ptr,0)))
		    RECD(optr,0) = -2.5 * log10 (RECD(ptr,0)) +
		        CAT_MAGZERO(cat)
		else
		    RECD(optr,0) = INDEFD
	    default:
		optr = iptr
		otype = itype
	    }
	default:
	    optr = iptr
	    otype = itype
	}
end
