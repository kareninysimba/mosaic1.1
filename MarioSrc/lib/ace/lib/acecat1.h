# ACECAT1.H -- Internal catalog structures.

# Catalog internal data structure.

define	CAT_SZSTR	99		# Length of catalog string
define	CAT_SLEN	19		# Length of strings
define	CAT_DEFLEN	33		# Length of entry structure
define	CAT_LEN		215		# Length of catalog structure

#define	CAT_NUMMAX	Memi[$1+6]	# Maximum record number
define	CAT_NUMMAX	CAT_NRECS($1)	# Maximum record number
define	CAT_FLAGS	Memi[$1+7]	# Catalog flags
define	CAT_STP		Memi[$1+8]	# Record structure symbol table
define	CAT_INTBL	Memi[$1+9]	# Input table structure
define	CAT_OUTTBL	Memi[$1+10]	# Output table structure
define	CAT_UFUNC	Memi[$1+11]	# User transformation function
define	CAT_APFLUX	Memi[$1+12]	# Array of aperture fluxes (ptr)
define	CAT_MAGZERO	Memr[$1+13]	# Magnitude zero
define	CAT_CUR		Memi[$1+14]	# Current index (for cathead/catnext)
define	CAT_CATALOG	Memc[P2C($1+15)]	# Catalog name
define	CAT_RECID	Memc[P2C($1+65)]	# Default ID
define	CAT_STRPTR	P2C($1+115)		# String pointer
define	CAT_STR		Memc[CAT_STRPTR($1)]	# String value
define	CAT_BUF		($1+165)		# Working buffer (50)

# Table structure.
define	TBL_SZBUF	99		# BUF size in chars
define	TBL_LEN		52		# Structure length
define	TBL_TP		Memi[$1]	# Table pointer
define	TBL_STP		Memi[$1+1]	# Symbol table of entries
define	TBL_BUF		($1+2)		# Working buffer

# Catalog entry structure.
define	ENTRY_ULEN	19			# Length of units string
define	ENTRY_FLEN	19			# Length of format string
define	ENTRY_DLEN	99			# Length of description string
define	ENTRY_LEN	100			# Length of entry structure
define	ENTRY_CDEF	Memi[$1]		# Column descriptor
define	ENTRY_ID	Memi[$1+1]		# Entry id
define	ENTRY_READ	Memi[$1+2]		# Read from catalog?
define	ENTRY_WRITE	Memi[$1+3]		# Read to catalog?
define	ENTRY_TYPE	Memi[$1+4]		# Datatype in record
define	ENTRY_CTYPE	Memi[$1+5]		# Datatype in catalog
define	ENTRY_FUNC	Memi[$1+6]		# Entry function
define	ENTRY_RAP	Memr[$1+7]		# Entry aperture radius
define	ENTRY_UNITS	Memc[P2C($1+8)]		# Entry units (19)
define	ENTRY_FORMAT	Memc[P2C($1+28)]	# Entry format (19)
define	ENTRY_DESC	Memc[P2C($1+48)]	# Entry description (99)

define	FUNCS		"|MAG|"
define	FUNC_MAG	1		# Magnitude

# Catalog extensions.
define	CATEXTNS	"|fits|tab|"

# Catalog Parameters.
define	CATPARAMS	"|image|mask|objid|catalog|nobjects|magzero|irows|orows|"
