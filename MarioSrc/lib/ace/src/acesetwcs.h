# ACESETWCS -- Catalog information used by the task.

define	ID_WX		 0 # d pixels %.2h	/ X world coordinate
define	ID_WY		 2 # d pixels %.2h	/ Y world coordinate
define	ID_PX		 4 # d pixels %.2f	/ X aperture coordinate
define	ID_PY		 5 # d pixels %.2f	/ Y aperture coordinate

define	OBJ_WX		RECD($1,ID_WX)		# X world coordinate
define	OBJ_WY		RECD($1,ID_WY)		# Y world coordinate
define	OBJ_PX		RECD($1,ID_PX)		# X aperture coordinate
define	OBJ_PY		RECD($1,ID_PY)		# Y aperture coordinate
