include	<evvexpr.h>
include	"ace.h"
include	<aceobjs.h>


procedure t_filter ()

pointer	catalog			#I Catalog name
pointer	filt			#I Filter

pointer	sp, cat, obj, cathead(), catnext()
errchk	catopen

begin
	call smark (sp)
	call salloc (catalog, SZ_FNAME, TY_CHAR)
	call salloc (filt, SZ_LINE, TY_CHAR)

	call clgstr ("catalog", Memc[catalog], SZ_FNAME)
	call clgstr ("filter", Memc[filt], SZ_FNAME)

	call catopen (cat, Memc[catalog], Memc[catalog], "")

	for (obj=cathead(cat); obj!=NULL; obj=catnext(cat,obj)) {
#	    call printf ("%d\n")
#		call pargi (OBJ_ROW(obj))
	}

	call catclose (cat)

	call sfree (sp)
end
