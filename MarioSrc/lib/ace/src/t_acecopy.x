include	<error.h>
include	<acecat.h>


# T_ACECOPY -- Copy catalogs.
#
# This task does more than just copy catalogs.  It allows changing formats,
# filtering, and appending.

procedure t_acecopy ()

int	inlist			# List of input catalogs
int	outlist			# List of output catalogs
pointer	catdef			# Catalog definitions
pointer	filt			# Catalog filter
bool	append			# Append to existing catalog?
bool	verbose			# Verbose?

int	i, j, k, nrecs
pointer	cat, recs, rec, ptr
pointer	sp, input, output, str

bool	clgetb()
int	imtopenp(), imtlen(), imtgetim(), nowhite(), catacc()
errchk	catopen()

begin
	call smark (sp)
	call salloc (input, SZ_FNAME, TY_CHAR)
	call salloc (output, SZ_FNAME, TY_CHAR)
	call salloc (catdef, SZ_FNAME, TY_CHAR)
	call salloc (filt, SZ_LINE, TY_CHAR)
	call salloc (str, SZ_LINE, TY_CHAR)

	# Get task parameters.
	inlist = imtopenp ("input")
	outlist = imtopenp ("output")
	call clgstr ("catdef", Memc[catdef], SZ_LINE)
	call clgstr ("filter", Memc[filt], SZ_LINE)
	append = clgetb ("append")
	verbose = clgetb ("verbose")

	i = nowhite (Memc[catdef], Memc[catdef], SZ_LINE)

	# Check lists.
	i = imtlen (inlist)
	j = imtlen (outlist)
	if (j != i) {
	    if (j != 1)
		call error (1, "Input and output lists don't match")
	    else if (!append)
		call error (1, "Append must be set for this operation")
	}

	# Copy catalogs.
	while (imtgetim (inlist, Memc[input], SZ_FNAME) != EOF) {
	    if (imtgetim (outlist, Memc[str], SZ_LINE) != EOF) {
	        call strcpy (Memc[str], Memc[output], SZ_FNAME)
		k = 0
	    }
	    k = k + 1

	    iferr {
		cat = NULL; rec = NULL

		# Check if for output if not appending.
		if (!append && catacc (Memc[output]) == YES) {
		    call sprintf (Memc[str], SZ_LINE,
		        "Output catalog exists (%s)")
			call pargstr (Memc[output])
		    call error (1, Memc[output])
		}

		# Open the catalogs.
		ptr = NULL
		call catopen (ptr, Memc[input], Memc[output], Memc[catdef],
		    "", NULL)
		cat = ptr

		if (verbose) {
		    call printf ("%s -> %s\n")
			call pargstr (Memc[input])
			call pargstr (Memc[output])
		    call flush (STDOUT)
		}

		# Copy the entries.
		call catrrecs (cat, Memc[filt], -1)
		recs = CAT_RECS(cat)
		nrecs = CAT_NRECS(cat)
		call catgeti (cat, "orows", j)
		do i = 0, nrecs-1 {
		    rec = Memi[recs+i]
		    j = j + 1
		    call catwrec (cat, rec, j)
		}

	    } then
	        call erract (EA_WARN)

	    if (cat != NULL) {
		# Don't update the header when appending.
		if (k > 1)
		    call mfree (CAT_OHDR(cat), TY_STRUCT)
		call catclose (cat)
	    }
	}

	call imtclose (outlist)
	call imtclose (inlist)
	call sfree (sp)
end
