task	acecatmatch = t_acecatmatch,
	acecopy = t_acecopy,
	detect = t_acedetect,
	diffdetect = t_diffdetect,
	evaluate = t_aceevaluate,
	overlay = t_acedisplay,
	skyimages = t_acesky,
	acegeomap = t_acegeomap,
	acesetwcs = t_acesetwcs
