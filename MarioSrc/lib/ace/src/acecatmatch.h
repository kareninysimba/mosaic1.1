# CATMATCH.H -- Catalog information required by the task.
# The mapping from actual catalogs to these quantities is handled by ACECAT.

define	ID_RA		0 # d hr %.2h
define	ID_DEC		2 # d deg %.1h
define	ID_MAG		4 # d magnitudes %15.7g
define	ID_X		6 # d pixels %.2f
define	ID_Y		8 # d pixels %.2f
define	ID_PTR		10 # ii		/ Pointer/integer for internal use

define	ACM_RA		RECD($1,ID_RA)
define	ACM_DEC		RECD($1,ID_DEC)
define	ACM_MAG		RECD($1,ID_MAG)
define	ACM_X		RECD($1,ID_X)
define	ACM_Y		RECD($1,ID_Y)
define	ACM_PTR		RECI($1,ID_PTR)
