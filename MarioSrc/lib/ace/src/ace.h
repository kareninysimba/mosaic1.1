define	NUMSTART		11	# First object number

# Mask Flags.
define	MASK_NUM	0017777777B	# Mask number
define	MASK_BP		0020000000B	# Bad pixel
define	MASK_BPFLAG	0040000000B	# Bad pixel flag
define	MASK_BNDRY	0100000000B	# Boundary flag
define	MASK_SPLIT	0200000000B	# Split flag
define	MASK_DARK	0400000000B	# Dark flag

define	MSETFLAG	ori($1,$2)
define	MUNSETFLAG	andi($1,noti($2))

define	MNUM		(andi($1,MASK_NUM))
define	MNOTBP		(andi($1,MASK_BP)==0)
define	MBP		(andi($1,MASK_BP)!=0)
define	MNOTBPFLAG	(andi($1,MASK_BPFLAG)==0)
define	MBPFLAG		(andi($1,MASK_BPFLAG)!=0)
define	MNOTBNDRY	(andi($1,MASK_BNDRY)==0)
define	MBNDRY		(andi($1,MASK_BNDRY)!=0)
define	MNOTSPLIT	(andi($1,MASK_SPLIT)==0)
define	MSPLIT		(andi($1,MASK_SPLIT)!=0)
define	MNOTDARK	(andi($1,MASK_DARK)==0)
define	MDARK		(andi($1,MASK_DARK)!=0)

# Output object masks types.
define	OM_TYPES		"|boolean|numbers|colors|all|"
define	OM_BOOL		1	# Boolean (0=sky, 1=object+bad)
define	OM_ONUM		2	# Object number only
define	OM_COLORS	3	# Bad=1, Objects=2-9
define	OM_ALL		4	# All values
