include	<acecat.h>
include	<aceobjs.h>


procedure catfunc (cat, obj, id, type, val, otype)

pointer	cat			#I Catalog pointer
pointer	obj			#I Object pointer
int	id			#I Field ID
int	type			#O Data type
pointer	val			#O Pointer for return value
int	otype			#O Output data type

int	napr
real	a, b, theta, elong, ellip, r, cxx, cyy, cxy
real	aerr, berr, thetaerr, cxxerr, cyyerr, cxyerr
bool	doshape
pointer	objlast

begin
	if (obj == NULL)
	    return

	if (id <= 1000) {
	    val = obj + id
	    otype = type
	    return
	}

	# Initialize for new object.
	if (obj != objlast) {
	    napr = 0
	    doshape = false
	}
	
	otype = TY_REAL
	switch (id) {
	case ID_A, ID_B, ID_THETA, ID_ELONG, ID_ELLIP, ID_R2, ID_CXX,
	    ID_CYY, ID_CXY:
	    if (!doshape) {
		call catshape (obj, a, b, theta, elong, ellip, r,
		    cxx, cyy, cxy, aerr, berr, thetaerr, cxxerr,
		    cyyerr, cxyerr)
		doshape = true
	    }
	    switch (id) {
	    case ID_A:
		Memr[val] = a
	    case ID_B:
		Memr[val] = b
	    case ID_THETA:
		Memr[val] = theta
	    case ID_ELONG:
		Memr[val] = elong
	    case ID_ELLIP:
		Memr[val] = ellip
	    case ID_R2:
		Memr[val] = r
	    case ID_CXX:
		Memr[val] = cxx
	    case ID_CYY:
		Memr[val] = cyy
	    case ID_CXY:
		Memr[val] = cxy
	    }
	case ID_FLUXERR, ID_XERR, ID_YERR:
	    switch (id) {
	    case ID_FLUXERR:
		Memr[val] = OBJ_FLUXVAR(obj)
	    case ID_XERR:
		Memr[val] = OBJ_XVAR(obj)
	    case ID_YERR:
		Memr[val] = OBJ_YVAR(obj)
	    }
	    if (IS_INDEFR(Memr[val]) || Memr[val] < 0.)
		Memr[val] = INDEFR
	    else
		Memr[val] = sqrt (Memr[val])
	case ID_AERR, ID_BERR, ID_THETAERR, ID_CXXERR, ID_CYYERR,
	    ID_CXYERR:
	    if (!doshape) {
		call catshape (obj, a, b, theta, elong, ellip, r,
		    cxx, cyy, cxy, aerr, berr, thetaerr, cxxerr,
		    cyyerr, cxyerr)
		doshape = true
	    }
	    switch (id) {
	    case ID_AERR:
		Memr[val] = aerr
	    case ID_BERR:
		Memr[val] = aerr
	    case ID_THETAERR:
		Memr[val] = aerr
	    case ID_CXXERR:
		Memr[val] = aerr
	    case ID_CYYERR:
		Memr[val] = aerr
	    case ID_CXYERR:
		Memr[val] = aerr
	    }
	}

	objlast = obj
end
