include	<error.h>
include	<imhdr.h>
include	<pmset.h>
include	"ace.h"
include	<acecat.h>
include	<acecat1.h>
include	<aceobjs.h>
include	<math.h>
include	"evaluate.h"

# Parameters for FWHM normalization function and histogram.
define	FW_NI		100		# Normalization function sample
define	FW_I1		0.40		# Low FWHM point
define	FW_I2		0.60		# High FWHM point
define	FW_BIN1		1.0		# First bin
define	FW_BIN2		21.0		# Last bin
define	FW_BINWIDTH	0.02		# Bin width
define	FW_BINMODE	21		# Mode smoothing bin (odd)


# EVALUATE -- Evaluate object parameters.
#
# This only evaluates non-DARK detections.
#
# The structure of this routine has up to three passes depending on which
# centers are defined.  If the aperture centers are defined then only one
# pass is needed.  If they are not defined and the peak centers are defined
# then two passes are needed with the aperture centers first computed based
# on a core centroid.  If the peak centers are also not defined then three
# passes are needed; 1) compute the peak center, 2) compute the aperture
# center based on the core centroid, and 3) evaluate the quantities that
# depend on the aperture center.

procedure evaluate (evl, cat, im, om, skymap, sigmap, gainmap, expmap, logfd)

pointer	evl			#I Parameters
pointer	cat			#I Catalog structure
pointer	im			#I Image pointer
pointer	om			#I Object mask pointer
pointer	skymap			#I Sky map
pointer	sigmap			#I Sigma map
pointer	gainmap			#I Gain map
pointer	expmap			#I Exposure map
int	logfd			#I Logfile

bool	newpeaks, newaps, bndry
int	i, j, n, c, l, nc, nl, c1, c2, nummax, num, nobjsap, fw_nbins
real	x, x2, y, y2, r, s, s2, f, f2, wt, val, sky, pval, ssig, s2x, s2y, dfw
real	magzero, fwhm, nfwhm
pointer	objs, obj, rlptr
pointer	data, skydata, ssigdata, gaindata, expdata, sigdata
pointer	sp, v, rl, nthresh, sum_f2, sum_s2x, sum_s2y, dopeaks, doaps
pointer	fw_norm, sum_fw, fw_bins
pointer	hdr

int	andi(), ctor()
real	imgetr()
bool	pm_linenotempty()
errchk	salloc, evgdata

begin
	call smark (sp)

	if (logfd != NULL)
	    call fprintf (logfd, "  Evaluate objects:\n")

	# Get evaluation parameters.
	if (EVL_MAGZERO(evl,1) == '!') {
	    iferr (magzero = imgetr (im, EVL_MAGZERO(evl,2))) {
		call erract (EA_WARN)
		magzero = INDEFR
	    }
	} else {
	    i = 1
	    if (ctor (EVL_MAGZERO(evl,1), i, magzero) == 0)
		magzero = INDEFR
	}
	if (!IS_INDEFR(magzero))
	    call catputr (cat, "magzero", magzero) 

	# Initialize.
	objs = CAT_RECS(cat)
	nummax = CAT_NUMMAX(cat)

	nc = IM_LEN(im,1)
	nl = IM_LEN(im,2)
	fw_nbins = 1 + (FW_BIN2 - FW_BIN1) / FW_BINWIDTH

	# Allocate work arrays.
	call salloc (v, PM_MAXDIM, TY_LONG)
	call salloc (rl, 3+3*IM_LEN(im,1), TY_INT)

	call salloc (sigdata, nc, TY_REAL)
	call salloc (fw_norm, FW_NI, TY_REAL)
	call salloc (fw_bins, fw_nbins, TY_REAL)

	call salloc (dopeaks, nummax, TY_SHORT)
	call salloc (doaps, nummax, TY_SHORT)
	call salloc (nthresh, nummax, TY_INT)
	call salloc (sum_fw, nummax, TY_REAL)
	call salloc (sum_f2, nummax, TY_REAL)
	call salloc (sum_s2x, nummax, TY_REAL)
	call salloc (sum_s2y, nummax, TY_REAL)

	call aclrr (Memr[fw_bins], fw_nbins)
	call aclri (Memi[nthresh], nummax)
	call aclrr (Memr[sum_fw], nummax)
	call aclrr (Memr[sum_f2], nummax)
	call aclrr (Memr[sum_s2x], nummax)
	call aclrr (Memr[sum_s2y], nummax)

	# Initialize profile normalization function.
	dfw = (FW_I2 - FW_I1) / (FW_NI - 1)
	y = -4 * LN_2
	do i = 0, FW_NI-1 {
	    x = i * dfw + FW_I1
	    Memr[fw_norm+i] = sqrt (log (x) / y)
	}

	# Initialize values for accumulation or checking.
	newpeaks = false; newaps = false
	do i = NUMSTART-1, nummax-1 {
	    Mems[dopeaks+i] = NO
	    Mems[doaps+i] = NO

	    obj = Memi[objs+i]
	    if (obj == NULL)
		next
	    if (OBJ_FLAG(obj,DARK) == 'D')
	        next

	    # Parameters related to the peak value and center.
	    if (IS_INDEFR(OBJ_PEAK(obj))) {
		newpeaks = true
	        Mems[dopeaks+i] = YES
		OBJ_PEAK(obj) = 0.
	    }

	    # Parameters related to aperture center.
	    if (IS_INDEFR(OBJ_FCORE(obj))) {
		newaps = true
	        Mems[doaps+i] = YES
		OBJ_FCORE(obj) = 0.
		OBJ_XAP(obj) = 0.
		OBJ_YAP(obj) = 0.
	    }
	    OBJ_R(obj) = 0.
	    OBJ_RI2(obj) = 0.
	    OBJ_FWHM(obj) = 0.
	    do j = ID_APFLUX, ID_APFLUX9
	        RECR(obj,j) = INDEFR

	    # Parameters that are accumulated based on source area.
	    OBJ_NPIX(obj) = 0
	    OBJ_SKY(obj) = 0.
	    OBJ_THRESH(obj) = 0.
	    OBJ_X1(obj) = 0.
	    OBJ_Y1(obj) = 0.
	    OBJ_X2(obj) = 0.
	    OBJ_Y2(obj) = 0.
	    OBJ_XY(obj) = 0.
	    OBJ_FLUX(obj) = 0.
	    OBJ_SIG(obj) = 0.
	    OBJ_ISIGAVG(obj) = 0.
	    OBJ_ISIGAVG2(obj) = INDEFR
	    OBJ_FLUXVAR(obj) = 0.
	    OBJ_XVAR(obj) = 0.
	    OBJ_YVAR(obj) = 0.
	    OBJ_XYCOV(obj) = 0.
	}

	# --- PASS1 ---
	#
	# Compute all parameters that do not depend on a source center.
	# Compute parameters for sources with defined centers.
	# Compute those centers that can be computed in this pass.

	# Check and initialze aperture fluxes.
	call evapinit (cat, nobjsap)

	Memi[v] = 1
	do l = 1, nl {
	    Memi[v+1] = l

	    # Note the data is read the first time it is needed.
	    data = NULL

	    # Do circular aperture photometry.
	    if (nobjsap > 0)
		call evapeval (l, im, skymap, sigmap, gainmap, expmap,
		    data, skydata, ssigdata, gaindata, expdata, sigdata)

	    # Check if there are any object regions in this line.
	    if (!pm_linenotempty (om, Memi[v]))
		next
	    call pmglri (om, Memi[v], Memi[rl], 0, nc, 0)

	    # Go through pixels which are parts of objects.
	    rlptr = rl
	    do i = 2, Memi[rl] {
		rlptr = rlptr + 3
		c1 = Memi[rlptr]
		c2 = c1 + Memi[rlptr+1] - 1
		num = MNUM(Memi[rlptr+2]) - 1
		bndry = MBNDRY(Memi[rlptr+2])

		# Do all unevaluated objects and their parents.
		while (num >= NUMSTART-1) {
		    obj = Memi[objs+num]
		    if (obj == NULL)
			break
		    if (OBJ_FLAG(obj,DARK) == 'D')
			break

		    if (data == NULL)
			call evgdata (l, im, skymap, sigmap, gainmap, expmap,
			    data, skydata, ssigdata, gaindata, expdata, sigdata)

		    if (OBJ_NPIX(obj) == 0) {
			val = Memr[data+c1-1]
			sky = Memr[skydata+c1-1]
			ssig = Memr[ssigdata+c1-1]

			OBJ_XMIN(obj) = c1
			OBJ_XMAX(obj) = c1
			OBJ_YMIN(obj) = l
			OBJ_YMAX(obj) = l
			OBJ_ISIGMAX(obj) =  (val - sky) / ssig
		    }

		    pval = OBJ_PEAK(obj)
		    s2x = Memr[sum_s2x+num]
		    s2y = Memr[sum_s2y+num]
		    do c = c1, c2 {
			sky = Memr[skydata+c-1]
			val = Memr[data+c-1] - sky
			ssig = Memr[ssigdata+c-1]
			s = Memr[sigdata+c-1]
			f2 = val * val

			# Do things concerning the peak pixel.
			if (Mems[dopeaks+num] == YES) {
			    if (pval < val) {
				OBJ_PEAK(obj) = val
				OBJ_XPEAK(obj) = c
				OBJ_YPEAK(obj) = l
			    }
			} else {
			    if (!newpeaks && Mems[doaps+num] == YES) {
				r = sqrt ((c - OBJ_XPEAK(obj)) ** 2 +
				    (l - OBJ_YPEAK(obj)) ** 2)
				if (r <= RCORE) {
				    OBJ_FCORE(obj) = OBJ_FCORE(obj) + val
				    OBJ_XAP(obj) = OBJ_XAP(obj) + c * val
				    OBJ_YAP(obj) = OBJ_YAP(obj) + l * val
				}
			    }
			}

			# Do things concerning aperture center.
			if (!newaps) {
			    r = sqrt ((c - OBJ_XAP(obj)) ** 2 +
			        (l - OBJ_YAP(obj)) ** 2)

			    if (pval > 0.) {
				x = val / pval
				if (x > FW_I1 && x < FW_I2) {
				    j = int ((x - FW_I1) / dfw) 
				    y = r / Memr[fw_norm+j]
				    wt = (1 - 2 * abs (x - 0.5)) / sqrt(y)
				    OBJ_FWHM(obj) = OBJ_FWHM(obj) + y * wt
				    Memr[sum_fw+num] = Memr[sum_fw+num] + wt
				    if (y > FW_BIN1 && y < FW_BIN2) {
					j = 1 + (y-FW_BIN1)/FW_BINWIDTH
					Memr[fw_bins+j] = Memr[fw_bins+j] + wt
				    } else
					Memr[fw_bins] = Memr[fw_bins] + wt
				}
			    }

			    OBJ_R(obj) = OBJ_R(obj) + r * val
			    OBJ_RI2(obj) = OBJ_RI2(obj) + r * f2
			    Memr[sum_f2+num] = Memr[sum_f2+num] + f2
			}

			# Do other evaluations.
			x = c - OBJ_XMIN(obj)
			y = l - OBJ_YMIN(obj)
			x2 = x * x
			y2 = y * y
			s2 = s * s

			OBJ_NPIX(obj) = OBJ_NPIX(obj) + 1
			OBJ_SKY(obj) = OBJ_SKY(obj) + sky
			OBJ_SIG(obj) = OBJ_SIG(obj) + ssig

			if (bndry) {
			    OBJ_THRESH(obj) = OBJ_THRESH(obj) + val
			    Memi[nthresh+num] = Memi[nthresh+num] + 1
			}

			OBJ_FLUX(obj) = OBJ_FLUX(obj) + val
			OBJ_FLUXVAR(obj) = OBJ_FLUXVAR(obj) + s2

			OBJ_XMIN(obj) = min (OBJ_XMIN(obj), c)
			OBJ_XMAX(obj) = max (OBJ_XMAX(obj), c)
			OBJ_X1(obj) = OBJ_X1(obj) + x * val
			OBJ_X2(obj) = OBJ_X2(obj) + x2 * val
			OBJ_XVAR(obj) = OBJ_XVAR(obj) + x2 * s2
			s2x = s2x + x * s2

			OBJ_YMIN(obj) = min (OBJ_YMIN(obj), l)
			OBJ_YMAX(obj) = max (OBJ_YMAX(obj), l)
			OBJ_Y1(obj) = OBJ_Y1(obj) + y * val
			OBJ_Y2(obj) = OBJ_Y2(obj) + y2 * val
			OBJ_YVAR(obj) = OBJ_YVAR(obj) + y2 * s2
			s2y = s2y + y * s2

			OBJ_XY(obj) = OBJ_XY(obj) + x * y * val
			OBJ_XYCOV(obj) = OBJ_XYCOV(obj) + x * y * s2

			val = val / ssig
			OBJ_ISIGAVG(obj) = OBJ_ISIGAVG(obj) + val
			OBJ_ISIGMAX(obj) = max (OBJ_ISIGMAX(obj), val)

		    }
		    Memr[sum_s2x+num] = s2x
		    Memr[sum_s2y+num] = s2y

		    num = OBJ_PNUM(obj) - 1
		}
	    }
	}

	# Finish up the evaluations.
	do i = NUMSTART-1, nummax-1 {
	    obj = Memi[objs+i]
	    if (obj == NULL)
		next
	    if (OBJ_FLAG(obj,DARK) == 'D')
		next

	    n = OBJ_NPIX(obj)
	    f = OBJ_FLUX(obj)

	    if (Mems[dopeaks+i] == YES) {
		# I don't think OBJ_PEAK can ever be less than or equal to zero.
		if (OBJ_PEAK(obj) == 0.) {
		    OBJ_XPEAK(obj) = (OBJ_XMAX(obj) + OBJ_XMIN(obj)) / 2.
		    OBJ_YPEAK(obj) = (OBJ_YMAX(obj) + OBJ_YMIN(obj)) / 2.
		}

	    } else if (!newpeaks && Mems[doaps+i] == YES) {
		if (OBJ_FCORE(obj) > 0.) {
		    OBJ_XAP(obj) = OBJ_XAP(obj) / OBJ_FCORE(obj)
		    OBJ_YAP(obj) = OBJ_YAP(obj) / OBJ_FCORE(obj)
		} else {
		    OBJ_XAP(obj) = OBJ_XPEAK(obj)
		    OBJ_YAP(obj) = OBJ_YPEAK(obj)
		}

	    } else if (!newaps) {
		x = Memr[sum_fw+i]
		if (x > 0.) {
		    fwhm = fwhm + OBJ_FWHM(obj)
		    nfwhm = nfwhm + x
		    OBJ_FWHM(obj) = OBJ_FWHM(obj) / x
		} else
		    OBJ_FWHM(obj) = INDEFR

		if (f > 0)
		    OBJ_R(obj) = OBJ_R(obj) / f
		else
		    OBJ_R(obj) = INDEFR

		f2 = Memr[sum_f2+i]
		if (f2 > 0)
		    OBJ_RI2(obj) = SQRTOF2 * OBJ_RI2(obj) / f2
		else
		    OBJ_RI2(obj) = INDEFR
	    }

	    if (f > 0.) {
		f2 = f * f
		x = OBJ_X1(obj) / f
		s2x = Memr[sum_s2x+i]
		s2y = Memr[sum_s2y+i]

		OBJ_X1(obj) = x + OBJ_XMIN(obj)
		OBJ_X2(obj) = OBJ_X2(obj) / f - x * x
		OBJ_XVAR(obj) = (OBJ_XVAR(obj) - 2 * x * s2x + 
		    x * x * OBJ_FLUXVAR(obj)) / f2

		y = OBJ_Y1(obj) / f
		OBJ_Y1(obj) = y + OBJ_YMIN(obj)
		OBJ_Y2(obj) = OBJ_Y2(obj) / f - y * y
		OBJ_YVAR(obj) = (OBJ_YVAR(obj) - 2 * y * s2y + 
		    y * y * OBJ_FLUXVAR(obj)) / f2

		OBJ_XY(obj) = OBJ_XY(obj) / f - x * y
		OBJ_XYCOV(obj) = (OBJ_XYCOV(obj) - x * s2x -
		    y * s2y + x * y * OBJ_FLUXVAR(obj)) / f2
	    } else {
		OBJ_X1(obj) = INDEFR
		OBJ_X2(obj) = INDEFR
		OBJ_XVAR(obj) = INDEFR
		OBJ_Y1(obj) = INDEFR
		OBJ_Y2(obj) = INDEFR
		OBJ_YVAR(obj) = INDEFR
		OBJ_XY(obj) = INDEFR
		OBJ_XYCOV(obj) = INDEFR
		OBJ_FLUXVAR(obj) = INDEFR
	    }

	    if (Memi[nthresh+i] > 0)
		OBJ_THRESH(obj) = OBJ_THRESH(obj) / Memi[nthresh+i]
	    else
		OBJ_THRESH(obj) = INDEFR
	    OBJ_SKY(obj) = OBJ_SKY(obj) / n
	    OBJ_SIG(obj) = OBJ_SIG(obj) / n
	    OBJ_ISIGAVG(obj) = OBJ_ISIGAVG(obj) / sqrt(real(n))
	    
	    OBJ_FLAG(obj,EVAL) = 'E'
	}

	call evapfree ()

	# --- PASS2 ---
	#
	# If aperture centers were not defined we can now find the aperture
	# center from the peak just measured.

	if (newaps && newpeaks) { 
	    Memi[v] = 1
	    do l = 1, nl {
		Memi[v+1] = l

		# Note the data is read the first time it is needed.
		data = NULL

		# Check if there are any object regions in this line.
		if (!pm_linenotempty (om, Memi[v]))
		    next
		call pmglri (om, Memi[v], Memi[rl], 0, nc, 0)

		# Go through pixels which are parts of objects.
		rlptr = rl
		do i = 2, Memi[rl] {
		    rlptr = rlptr + 3
		    c1 = Memi[rlptr]
		    c2 = c1 + Memi[rlptr+1] - 1
		    num = MNUM(Memi[rlptr+2]) - 1

		    # Compute aperture center.
		    while (num >= NUMSTART-1) {
			obj = Memi[objs+num]
			if (obj == NULL)
			    break
			if (OBJ_FLAG(obj,DARK) == 'D')
			    break
			if (Mems[doaps+num] == NO) {
			    num = OBJ_PNUM(obj) - 1
			    next
			}

			if (data == NULL)
			    call evgdata (l, im, skymap, sigmap, gainmap,
				expmap, data, skydata, ssigdata, gaindata,
				expdata, sigdata)

			do c = c1, c2 {
			    sky = Memr[skydata+c-1]
			    val = Memr[data+c-1] - sky

			    r = sqrt ((c - OBJ_XPEAK(obj)) ** 2 +
				(l - OBJ_YPEAK(obj)) ** 2)
			    if (r <= RCORE) {
				OBJ_FCORE(obj) = OBJ_FCORE(obj) + val
				OBJ_XAP(obj) = OBJ_XAP(obj) + c * val
				OBJ_YAP(obj) = OBJ_YAP(obj) + l * val
			    }

			}

			num = OBJ_PNUM(obj) - 1
		    }
		}
	    }

	    # Finish up the evaluations.
	    do i = NUMSTART-1, nummax-1 {
	        if (Mems[doaps+i] == NO)
		    next

		obj = Memi[objs+i]
		if (obj == NULL)
		    next
		if (OBJ_FCORE(obj) > 0.) {
		    OBJ_XAP(obj) = OBJ_XAP(obj) / OBJ_FCORE(obj)
		    OBJ_YAP(obj) = OBJ_YAP(obj) / OBJ_FCORE(obj)
		} else {
		    OBJ_XAP(obj) = OBJ_XPEAK(obj)
		    OBJ_YAP(obj) = OBJ_YPEAK(obj)
		}
	    }
	}

	# --- PASS3 ---
	#
	# If aperture centers were defined in pass1 or pass2 apply them.

	if (newaps) {
	    call evapinit (cat, nobjsap)

	    Memi[v] = 1
	    do l = 1, nl {
		Memi[v+1] = l

		# Note the data is read the first time it is needed.
		data = NULL

		# Do circular aperture photometry.  Check nobjsap to avoid
		# subroutine call.
		if (nobjsap > 0)
		    call evapeval (l, im, skymap, sigmap, gainmap, expmap,
			data, skydata, ssigdata, gaindata, expdata, sigdata)

		# Check if there are any object regions in this line.
		if (!pm_linenotempty (om, Memi[v]))
		    next
		call pmglri (om, Memi[v], Memi[rl], 0, nc, 0)

		# Go through pixels which are parts of objects.
		rlptr = rl
		do i = 2, Memi[rl] {
		    rlptr = rlptr + 3
		    c1 = Memi[rlptr]
		    c2 = c1 + Memi[rlptr+1] - 1
		    num = MNUM(Memi[rlptr+2]) - 1

		    # Do all unevaluated objects and their parents.
		    while (num >= NUMSTART-1) {
			obj = Memi[objs+num]
			if (obj == NULL)
			    break
			if (OBJ_FLAG(obj,DARK) == 'D')
			    break
			if (Mems[doaps+num] == NO) {
			    num = OBJ_PNUM(obj) - 1
			    next
			}

			if (data == NULL)
			    call evgdata (l, im, skymap, sigmap, gainmap,
				expmap, data, skydata, ssigdata, gaindata,
				expdata, sigdata)

			pval = OBJ_PEAK(obj)
			do c = c1, c2 {
			    sky = Memr[skydata+c-1]
			    val = Memr[data+c-1] - sky
			    f2 = val * val

			    r = sqrt ((c - OBJ_XAP(obj)) ** 2 +
				(l - OBJ_YAP(obj)) ** 2)

			    if (pval > 0. && r > 0.) {
				x = val / pval
				if (x > FW_I1 && x < FW_I2) {
				    j = int ((x - FW_I1) / dfw) 
				    y = r / Memr[fw_norm+j]
				    wt = (1 - 2 * abs (x - 0.5)) / sqrt(y)
				    OBJ_FWHM(obj) = OBJ_FWHM(obj) + y * wt
				    Memr[sum_fw+num] = Memr[sum_fw+num] + wt
				    if (y > FW_BIN1 && y < FW_BIN2) {
					j = 1 + (y-FW_BIN1)/FW_BINWIDTH
					Memr[fw_bins+j] = Memr[fw_bins+j] + wt
				    } else
					Memr[fw_bins] = Memr[fw_bins] + wt
				}
			    }

			    OBJ_R(obj) = OBJ_R(obj) + r * val
			    OBJ_RI2(obj) = OBJ_RI2(obj) + r * f2
			    Memr[sum_f2+num] = Memr[sum_f2+num] + f2
			}

			num = OBJ_PNUM(obj) - 1
		    }
		}
	    }

	    # Finish up the evaluations.
	    do i = NUMSTART-1, nummax-1 {
		if (Mems[doaps+i] == NO)
		    next

		obj = Memi[objs+i]
		if (obj == NULL)
		    next
		if (OBJ_FLAG(obj,DARK) == 'D')
		    next

		x = Memr[sum_fw+i]
		if (x > 0.) {
		    fwhm = fwhm + OBJ_FWHM(obj)
		    nfwhm = nfwhm + x
		    OBJ_FWHM(obj) = OBJ_FWHM(obj) / x
		} else
		    OBJ_FWHM(obj) = INDEFR

		f = OBJ_FLUX(obj)
		if (f > 0.)
		    OBJ_R(obj) = OBJ_R(obj) / f
		else
		    OBJ_R(obj) = INDEFR

		f2 = Memr[sum_f2+i]
		if (f2 > 0.)
		    OBJ_RI2(obj) = SQRTOF2 * OBJ_RI2(obj) / f2
		else
		    OBJ_RI2(obj) = INDEFR
	    }
	}

	call evapfree ()

	# Finish global evaluations.

	# Find the FWHM histogram peak.
	f = 0; f2 = 0
	num = FW_BINMODE
	n = num / 2
	do i = 0, num-1
	    f = f + Memr[fw_bins+i]
	do i = num, fw_nbins-1 {
	    f = f - Memr[fw_bins+i-num] + Memr[fw_bins+i]
	    x = (i-n-0.5)*FW_BINWIDTH + FW_BIN1
	   if (f >= f2) {
	       j = i - n
	       f2 = f
	    }
	}

	# Record FWHM
	if (Memr[fw_bins] < 0.5 * nfwhm)
	    fwhm = (j-0.5)*FW_BINWIDTH + FW_BIN1
	else if (nfwhm > 0.)
	    fwhm = fwhm / nfwhm
	else
	    fwhm = INDEFR
	if (!IS_INDEFR(fwhm)) {
            hdr = CAT_OHDR(cat)
            if (hdr == NULL)
                hdr = CAT_IHDR(cat)
	    if (hdr != NULL)
		call imaddr (hdr, "FWHM", fwhm)
	}

	# Set apportioned fluxes.
	call evapportion (cat, Memr[sum_s2x])

	# Set WCS coordinates.
	call evalwcs (cat, im)

	call sfree (sp)
end


# EVAPINIT -- Initialize aperture photometry.  nobjsap will signal whether
# there are any aperture fluxes requested.

procedure evapinit (cat, nobjsap)

pointer	cat			#I Catalog
int	nobjsap			#O Number of objects for aperture evaluation

bool	doobj
int	i, j, nummax
pointer	tbl, stp, sym, obj, sthead(), stnext()

int	ycompare()
extern	ycompare
errchk	calloc, malloc

int	nobjs			# Number of objects to evaluate
int	naps			# Number of apertures per object
real	rmax			# Maximum aperture radius
pointer	r2aps			# Array of aperture radii squared (ptr)
pointer	ysort			# Array of Y sorted object number indices (ptr)
int	ystart			# Index of first object to consider
pointer	objs			# Array of object structure (ptr)
int	ids			# Array of entry ids
common	/evapcom/ nobjs, naps, rmax, r2aps, ysort, ystart, objs, ids

begin
	nobjsap = 0
	nobjs = 0
	naps = 0
	ids = NULL
	r2aps = NULL
	ysort = NULL

	tbl = CAT_OUTTBL(cat)
	if (tbl == NULL)
	    return
	stp = TBL_STP(tbl)

	# Determine number of apertures.
	naps = 0
	for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
	    if (ENTRY_ID(sym) < ID_APFLUX || ENTRY_ID(sym) > ID_APFLUX9)
		next
	    naps = naps + 1
	}
	if (naps == 0)
	    return

	objs = CAT_RECS(cat)
	nummax = CAT_NUMMAX(cat)

	# Allocate memory.
	call malloc (ids, naps, TY_INT)
	call malloc (r2aps, naps, TY_REAL)
	call malloc (ysort, nummax, TY_INT)

	# Get the maximum radius since that will define the line
	# limits needed for each object.  Compute array of radius squared
	# for the apertures.  Pixels are checked for being in the aperture
	# in r^2 to avoid square roots.
	rmax = 0.
	naps = 0
	for (sym=sthead(stp); sym!=NULL; sym=stnext(stp,sym)) {
	    if (ENTRY_ID(sym) < ID_APFLUX || ENTRY_ID(sym) > ID_APFLUX9)
		next
	    Memi[ids+naps] = ENTRY_ID(sym)
	    Memr[r2aps+naps] = ENTRY_RAP(sym) ** 2
	    rmax = max (ENTRY_RAP(sym), rmax)
	    naps = naps + 1
	}

	# For the objects create a sorted index array by YAP so that we
	# can quickly find objects which include a particular line in
	# their apertures.

	do i = NUMSTART-1, nummax-1 {
	    obj = Memi[objs+i]
	    if (obj == NULL)
		next
	    if (OBJ_FLAG(obj,DARK) == 'D')
	        next
	    if (IS_INDEFR(OBJ_FCORE(obj)))
	        next
	    doobj = false
	    do j = 0, naps-1 {
		if (IS_INDEFR(RECR(obj,Memi[ids+j]))) {
		    doobj = true
		    RECR(obj,Memi[ids+j]) = 0.
		}
	    }
	    if (doobj) {
		Memi[ysort+nobjsap] = i
		nobjsap = nobjsap + 1
	    }
	}
	ystart = 1
	nobjs = nobjsap

	if (nobjsap == 0)
	    call evapfree ()
	else if (nobjsap > 1)
	    call gqsort (Memi[ysort], nobjsap, ycompare, objs)

end


# EVAPFREE -- Free aperture photometry memory.

procedure evapfree ()

int	nobjs			# Number of objects to evaluate
int	naps			# Number of apertures per object
real	rmax			# Maximum aperture radius
pointer	r2aps			# Array of aperture radii squared (ptr)
pointer	ysort			# Array of Y sorted object number indices (ptr)
int	ystart			# Index of first object to consider
pointer	objs			# Array of object structure (ptr)
int	ids			# Array of entry ids
common	/evapcom/ nobjs, naps, rmax, r2aps, ysort, ystart, objs, ids

begin
	call mfree (ids, TY_INT)
	call mfree (r2aps, TY_REAL)
	call mfree (ysort, TY_INT)
end


# EVAPEVAL -- Do circular aperture photometry.  Maintain i1 as the
# first entry in the sorted index array to be considered.  All
# earlier entries will have all aperture lines less than the
# current line.  Break on the first object whose minimum aperture
# line is greater than the current line.

procedure evapeval (l, im, skymap, sigmap, gainmap, expmap, data, skydata,
	ssigdata, gaindata, expdata, sigdata)

int	l			#I Line
pointer	im			#I Image
pointer	skymap			#I Sky map
pointer	sigmap			#I Sigma map
pointer	gainmap			#I Gain map
pointer	expmap			#I Exposure map
pointer	data			#O Image data
pointer	skydata			#O Sky data
pointer	ssigdata		#O Sky sigma data
pointer	gaindata		#O Gain data
pointer	expdata			#O Exposure data
pointer	sigdata			#O Total sigma data

int	i, j, id, nc, c
real	x, y, l2, r2, val, sky
pointer	obj

int	nobjs			# Number of objects to evaluate
int	naps			# Number of apertures per object
real	rmax			# Maximum aperture radius
pointer	r2aps			# Array of aperture radii squared (ptr)
pointer	ysort			# Array of Y sorted object number indices (ptr)
int	ystart			# Index of first object to consider
pointer	objs			# Array of object structure (ptr)
int	ids			# Array of entry ids
common	/evapcom/ nobjs, naps, rmax, r2aps, ysort, ystart, objs, ids

begin
	nc = IM_LEN(im,1)
	do i = ystart, nobjs {
	    obj = Memi[objs+Memi[ysort+i-1]]
	    if (IS_INDEFR(OBJ_FCORE(obj)))
	        next

	    y = OBJ_YAP(obj)
	    if (y - rmax > l)
		break
	    if (y + rmax < l) {
		ystart = ystart + 1
		next
	    }
	    x = OBJ_XAP(obj)
	    if (data == NULL)
		call evgdata (l, im, skymap, sigmap, gainmap, expmap,
		    data, skydata, ssigdata, gaindata, expdata, sigdata)

	    # Accumulate data within in the apertures using the r^2
	    # values.  Currently partial pixels are not considered and
	    # errors are not evaluated.
	    # Note that bad pixels or object overlaps are not excluded
	    # in the apertures.
	    l2 = (l - y) ** 2
	    do c = max (0, int(x-rmax)), min (nc, int(x+rmax+1)) {
		r2 = (c - x) ** 2 + l2
		do j = 0, naps-1 {
		    if (r2 < Memr[r2aps+j]) {
			val = Memr[data+c-1]
			sky = Memr[skydata+c-1]
			id = Memi[ids+j]
			RECR(obj,id) = RECR(obj,id) + (val - sky)
		    }
		}
	    }
	}
end


# EVAPPORTION -- Compute apportioned fluxes after the object isophotoal
# fluxes have been computed.

procedure evapportion (cat, sum_flux)

pointer	cat			#I Catalog
real	sum_flux[ARB]		#I Work array of size NUMMAX

int	nummax, num, pnum, nindef
pointer	objs, obj, pobj

begin
	objs = CAT_RECS(cat)
	nummax = CAT_NUMMAX(cat)

	call aclrr (sum_flux, nummax)
	do num = NUMSTART, nummax {
	    obj = Memi[objs+num-1]
	    if (obj == NULL)
		next
	    pnum = OBJ_PNUM(obj)
	    if (pnum == 0) {
		OBJ_FRAC(obj) = 1.
		OBJ_FRACFLUX(obj) = OBJ_FLUX(obj)
		next
	    }

	    sum_flux[pnum] = sum_flux[pnum] + max (0., OBJ_FLUX(obj))
	    OBJ_FRACFLUX(obj) = INDEFR
	}

	nindef = 0
	do num = NUMSTART, nummax {
	    obj = Memi[objs+num-1]
	    if (obj == NULL)
		next
	    pnum = OBJ_PNUM(obj)
	    if (pnum == 0)
		next
	    pobj = Memi[objs+pnum-1]

	    if (sum_flux[pnum] > 0.) {
		OBJ_FRAC(obj) = max (0., OBJ_FLUX(obj)) / sum_flux[pnum]
		if (IS_INDEFR(OBJ_FRACFLUX(pobj)))
		    nindef = nindef + 1
		else
		    OBJ_FRACFLUX(obj) = OBJ_FRACFLUX(pobj) * OBJ_FRAC(obj)
	    } else {
		OBJ_FRAC(obj) = INDEFR
		OBJ_FRACFLUX(obj) = OBJ_FLUX(obj)
	    }
	}

	while (nindef > 0) {
	    nindef = 0
	    do num = NUMSTART, nummax {
		obj = Memi[objs+num-1]
		if (obj == NULL)
		    next
		pnum = OBJ_PNUM(obj)
		if (pnum == 0)
		    next

		pobj = Memi[objs+pnum-1]
		if (IS_INDEFR(OBJ_FRACFLUX(pobj)))
		    nindef = nindef + 1
		else {
		    if (IS_INDEFR(OBJ_FRAC(obj)))
			OBJ_FRACFLUX(obj) = OBJ_FLUX(obj)
		    else
			OBJ_FRACFLUX(obj) = OBJ_FRACFLUX(pobj) * OBJ_FRAC(obj)
		}
	    }
	}
end


# EVALWCS -- Set WCS coordinates.

procedure evalwcs (cat, im)

pointer	cat			#I Catalog structure
pointer	im			#I IMIO pointer

int	i
double	xscale, yscale
pointer	sp, str, mw, ctw, ctp, objs, obj, mw_openim(), mw_sctran()
bool	streq()
errchk	mw_openim

begin
	call smark (sp)
	call salloc (str, SZ_FNAME, TY_CHAR)

	mw = mw_openim (im)
	ctw = mw_sctran (mw, "logical", "world", 03B)
	ctp = mw_sctran (mw, "logical", "physical", 03B)

	# Set conversion.
	xscale = 1.
	yscale = 1.
	ifnoerr (call mw_gwattrs (mw, 1, "axtype", Memc[str], SZ_FNAME)) {
	    if (streq (Memc[str], "ra"))
	        xscale = 15.
	}
	ifnoerr (call mw_gwattrs (mw, 2, "axtype", Memc[str], SZ_FNAME)) {
	    if (streq (Memc[str], "ra"))
	        yscale = 15.
	}

	objs = CAT_RECS(cat)
	do i = NUMSTART-1, CAT_NUMMAX(cat)-1 {
	    obj = Memi[objs+i]
	    if (obj == NULL)
		next
	    if (IS_INDEFR(OBJ_XAP(obj)) || IS_INDEFR(OBJ_YAP(obj))) {
		OBJ_WX(obj) = INDEFD
		OBJ_WY(obj) = INDEFD
		OBJ_PX(obj) = INDEFD
		OBJ_PY(obj) = INDEFD
	    } else {
		call mw_c2trand (ctw, double(OBJ_XAP(obj)),
		    double(OBJ_YAP(obj)), OBJ_WX(obj), OBJ_WY(obj))
		OBJ_WX(obj) = OBJ_WX(obj) / xscale
		OBJ_WY(obj) = OBJ_WY(obj) / yscale
		call mw_c2trand (ctp, double(OBJ_XAP(obj)),
		    double(OBJ_YAP(obj)), OBJ_PX(obj), OBJ_PY(obj))
	    }
	}

	call mw_ctfree (ctw)
	call mw_ctfree (ctp)
	call mw_close (mw)
end


# YCOMPARE -- Compare Y values of two objects for sorting.

int procedure ycompare (objs, i1, i2)

pointer	objs			#I Pointer to array of objects
int	i1			#I Index of first object to compare
int	i2			#I Index of second object to compare

real	y1, y2

begin
	y1 = OBJ_YAP(Memi[objs+i1])
	y2 = OBJ_YAP(Memi[objs+i2])
	if (y1 < y2)
	    return (-1)
	else if (y1 > y2)
	    return (1)
	else
	    return (0)
end


# EVGDATA -- Get evaluation data for an image line.

procedure evgdata (l, im, skymap, sigmap, gainmap, expmap, data, skydata,
	ssigdata, gaindata, expdata, sigdata)

int	l			#I Line
pointer	im			#I Image
pointer	skymap			#I Sky map
pointer	sigmap			#I Sigma map
pointer	gainmap			#I Gain map
pointer	expmap			#I Exposure map
pointer	data			#O Image data
pointer	skydata			#O Sky data
pointer	ssigdata		#O Sky sigma data
pointer	gaindata		#O Gain data
pointer	expdata			#O Exposure data
pointer	sigdata			#O Total sigma data

int	nc
pointer	imgl2r(), map_glr()
errchk	imgl2r, map_glr, noisemodel

begin
	nc = IM_LEN(im,1)
	data = imgl2r (im, l)
	skydata = map_glr (skymap, l, READ_ONLY)
	ssigdata = map_glr (sigmap, l, READ_ONLY)
	if (gainmap == NULL && expmap == NULL)
	    sigdata = ssigdata
	else if (expmap == NULL) {
	    gaindata = map_glr (gainmap, l, READ_ONLY)
	    call noisemodel (Memr[data], Memr[skydata],
		Memr[ssigdata], Memr[gaindata], INDEFR,
		Memr[sigdata], nc)
	} else if (gainmap == NULL) {
	    expdata = map_glr (expmap, l, READ_WRITE)
	    call noisemodel (Memr[data], Memr[skydata],
		Memr[ssigdata], INDEFR, Memr[expdata],
		Memr[sigdata], nc)
	} else {
	    gaindata = map_glr (gainmap, l, READ_ONLY)
	    expdata = map_glr (expmap, l, READ_WRITE)
	    call noisemodel (Memr[data], Memr[skydata],
		Memr[ssigdata], Memr[gaindata],
		Memr[expdata], Memr[sigdata], nc)
	}
end
