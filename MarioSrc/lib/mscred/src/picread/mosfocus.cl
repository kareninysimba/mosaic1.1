# MOSFOCUS -- Mosaic focus measuring task (raw ARCON format).
# This is customized to the NOAO header keywords.

procedure mscfocus (image)

file	image		{prompt="Mosaic focus image (rootname)"}
int	frame = 1	{prompt="Display frame to use"}
real	level = 0.5	{prompt="Measurement level (fraction or percent)"}
string	size = "FWHM"	{prompt="Size to display",
			 enum="Radius|FWHM|GFWHM|MFWHM"}
real	scale = 0.25	{prompt="Pixel scale"}
real	radius = 10.	{prompt="Measurement radius (pixels)"}
real	sbuffer = 5.	{prompt="Sky buffer (pixels)"}
real	swidth = 5.	{prompt="Sky width (pixels)"}
real	saturation = INDEF {prompt="Saturation level"}
bool	ignore_sat = no {prompt="Ignore objects with saturated pixels?"}
int	iterations = 2	{prompt="Number of radius adjustment iterations",
			 min=1}
string	logfile = "logfile" {prompt="Logfile"}

begin
	string	ims

	ims = image

	# Fix keywords.
	hedit (ims//"_![34]![19]![19].*",
	    "focshift", "(abs(focshift))", add-, verify-, show+, update+

#print ("\nMOCFOCUS: Estimate best focus from Mosaic focus image.")
#print ("  Mark the top star in each sequence unless the display is flipped.")
#print ("  More precisely mark the star with the largest y value.")
print ("Mark the top star (in unflipped display).")

	mscstarfocus (ims, focus="FOCSTART", fstep="FOCSTEP",
	    nexposures="FOCNEXPO", step="FOCSHIFT", direction="+line",
	    gap="beginning", coords="markall", display=yes, frame=frame,
	    imagecur="", graphcur="", level=level, size=size, beta=INDEF,
	    scale=scale, radius=radius, sbuffer=sbuffer, swidth=swidth,
	    saturation=saturation, ignore_sat=ignore_sat, xcenter=INDEF,
	    ycenter=INDEF, logfile=logfile, iterations=iterations)
end
