# MKFITS -- Make Mosaic FITS file from engineering Arcon images.

procedure mkfits (images)

string	images = ""		{prompt="Images to be converted"}
bool	delete = no		{prompt="Delete converted images?"}
string	bp = ""			{prompt="Bad pixel database directory"}

struct	*fd

begin
	file	imlist
	string	image, fits, amp, extname, database, ccdsec
	struct	title
	int	c1, c2, l1, l2
	real	ra, dec, ra_offset, dec_offset

	artdata
	mkpattern.ndim.p_min = 0

	imlist = mktemp ("tmp$iraf")

	# Expand input list.
	sections (images, option="root", > imlist)

	# Set WCS parameters.  This is like MSCWCS except that it is
	# applied to the individual images before appending to the FITS file.
	database = mscwcs.database
	ra_offset = mscwcs.ra_offset
	dec_offset = mscwcs.dec_offset
	ra = 0
	dec = 0

	# Read through input list converting the images.
	fd = imlist
	while (fscan (fd, image) != EOF) {
	    i = stridx ("_", image)
	    if (i == 0)
		next
	    fits = substr (image, 1, i-1) // ".fits"
	    amp = substr (image, i+1, i+3)
	    extname = "amp" // amp

	    if (!access (fits)) {
		hselect (image, "title", yes) | scan (title)
		mkpattern (fits, output="", title=title, pixtype="short",
		    ndim=0, header="")
	    }

	    # Update the header as much as possible before appending to
	    # FITS file to avoid later rewrites.
	    hedit (image, "detsec", "(ccdsec)", add+, update+, verify-, show-)
	    if (int(amp) > 300) {
		hedit (image, "FOCSHIFT", "(-FOCSHIFT)", add-, update+,
		    verify-, show-)
		hedit (image, "LTV1", -60., add+, update+, verify-, show-)
	    }
	    if (bp != "")
		hedit (image, "bpm", bp//"BPM_amp"//amp, add+, update+,
		    verify-, show-)
	    if (database != "") {
		ccsetwcs (image, database, extname, xref=INDEF, yref=INDEF,
		    xmag=INDEF, ymag=INDEF, xrotation=INDEF, yrotation=INDEF,
		    lngref=INDEF, latref=INDEF, lngunits="", latunits="",
		    transpose=no, projection="tan", coosystem="j2000",
		    update=yes, verbose=no)
		hedit (image, "wcssol", database // " " // extname,
		    add+, update+, show-, verify-)

		hselect (image, "ra,dec", yes) | scan (ra, dec)
		if (nscan() == 2) {
		    ra = ra * 15. + ra_offset / 3600.
		    dec = dec + dec_offset / 3600.
		    hedit (image, "crval1", ra, add+, update+, show-, verify-)
		    hedit (image, "crval2", dec, add+, update+, show-, verify-)
		}
	    }

	    imcopy (image, fits//"["//extname//",append,inherit,expand]",
		verbose=verbose)
	    hselect (image, "datasec", yes) |
		scanf ("[%d:%d,%d:%d", c1, c2, l1, l2)
	    c2 = c2 - c1 + 1 
	    c1 = c1 - c1 + 1 
	    l2 = l2 - l1 + 1 
	    l1 = l1 - l1 + 1 
	    printf ("[%d:%d,%d:%d]\n", c1, c2, l1, l2) | scan (ccdsec)
	    hedit (fits//"["//extname//",expand]", "ccdsec", ccdsec,
		add+, update+, verify-, show-)

	    if (delete)
		imdelete (image, verify=no)
	}
	fd = ""

	delete (imlist, verify=no)
end
