include	<error.h>
include	<imhdr.h>
include	<mach.h>


# T_TOSHORT -- Convert images to scaled short.

procedure t_toshort ()

int	inlist			# List of input images
int	outlist			# List of output images
int	inbpmlist		# List of bad pixel masks
int	outbpmlist		# List of bad pixel masks
pointer	sigma_key		# Sigma to sample
pointer	dmin_key		# Minimum data value for scaling
pointer	dmax_key		# Maximum data value for scaling
int	bpval			# Bad pixel value to add
pointer	maxbscale_key		# Maximum bscale allowed
bool	docopy			# Copy if can't convert?
int	logfile			# Logfile descriptor	

real	sigma			# Sigma to sample
real	datamin			# Minimum data value for scaling
real	datamax			# Maximum data value for scaling
real	maxbscale		# Maximum bscale allowed

int	i
real	bscale, bzero, r2igain, r2isat, r2imin
pointer	sp, input, output, bpmask, ibpmask, inbpmask, outbpmask, temp, bpmtemp
pointer	in, out, inbpm, outbpm, tmp

bool	clgetb(), streq()
int	clgeti(), imtopenp(), imtlen(), imtgetim()
int	nowhite(), open(), fnextn(), strlen(), ctor()
real	imgetr()
pointer	immap(), yt_pmmap()
errchk	open, immap, yt_pmmap, imgetr, toshort

begin
	call smark (sp)
	call salloc (input, SZ_FNAME, TY_CHAR)
	call salloc (output, SZ_FNAME, TY_CHAR)
	call salloc (bpmask, SZ_FNAME, TY_CHAR)
	call salloc (ibpmask, SZ_FNAME, TY_CHAR)
	call salloc (inbpmask, SZ_FNAME, TY_CHAR)
	call salloc (outbpmask, SZ_FNAME, TY_CHAR)
	call salloc (sigma_key, SZ_FNAME, TY_CHAR)
	call salloc (dmin_key, SZ_FNAME, TY_CHAR)
	call salloc (dmax_key, SZ_FNAME, TY_CHAR)
	call salloc (maxbscale_key, SZ_FNAME, TY_CHAR)
	call salloc (temp, SZ_FNAME, TY_CHAR)
	call salloc (bpmtemp, SZ_FNAME, TY_CHAR)

	# Get input parameters.
	inlist = imtopenp ("input")
	outlist = imtopenp ("output")
	inbpmlist = imtopenp ("inmasks")
	outbpmlist = imtopenp ("outmasks")
	call clgstr ("sigma", Memc[sigma_key], SZ_FNAME)
	call clgstr ("datamin", Memc[dmin_key], SZ_FNAME)
	call clgstr ("datamax", Memc[dmax_key], SZ_FNAME)
	bpval = clgeti ("bpval")
	call clgstr ("maxbscale", Memc[maxbscale_key], SZ_FNAME)
	docopy = clgetb ("docopy")
	call clgstr ("logfile", Memc[temp], SZ_FNAME)

	i = nowhite (Memc[sigma_key], Memc[sigma_key], SZ_FNAME)
	i = nowhite (Memc[dmin_key], Memc[dmin_key], SZ_FNAME)
	i = nowhite (Memc[dmax_key], Memc[dmax_key], SZ_FNAME)
	i = nowhite (Memc[maxbscale_key], Memc[maxbscale_key], SZ_FNAME)
	if (nowhite (Memc[temp], Memc[temp], SZ_FNAME) == 0)
	    logfile = NULL
	else
	    logfile = open (Memc[temp], APPEND, TEXT_FILE)

	# Check lists match.
	Memc[output] = EOS
	Memc[bpmask] = EOS
	Memc[ibpmask] = EOS
	Memc[outbpmask] = EOS
	if ((imtlen (outlist) > 0 && imtlen(inlist) != imtlen(outlist)) ||
	    (imtlen (inbpmlist) > 1 && imtlen(inlist) != imtlen(inbpmlist)) ||
	    (imtlen (outbpmlist) > 1 && imtlen(inlist) != imtlen(outbpmlist))) {
	    call sfree (sp)
	    call error (1, "Input and output lists do not match")
	}
	if (imtlen (outbpmlist) == 1 && imtlen(inlist) > 1) {
	    i = imtgetim (outbpmlist, Memc[bpmask], SZ_FNAME)
	    if (!streq (Memc[bpmask], "BPM")) {
		call sfree (sp)
		call error (1, "Bad pixel file must be 'BPM'")
	    }
	}

	while (imtgetim (inlist, Memc[input], SZ_FNAME) != EOF) {
	    if (imtgetim (inbpmlist, Memc[temp], SZ_FNAME) != EOF)
		call strcpy (Memc[temp], Memc[ibpmask], SZ_FNAME)
	    if (imtgetim (outlist, Memc[temp], SZ_FNAME) != EOF)
		call strcpy (Memc[temp], Memc[output], SZ_FNAME)
	    if (imtgetim (outbpmlist, Memc[temp], SZ_FNAME) != EOF) {
		call strcpy (Memc[temp], Memc[bpmask], SZ_FNAME)
		if (!streq (Memc[bpmask], "BPM")) {
		    if (fnextn (Memc[bpmask], Memc[temp], SZ_FNAME) > 0) {
			i = strlen (Memc[bpmask])
			if (streq (Memc[temp], "pl"))
			    Memc[bpmask+i-3] = EOS
			else if (streq (Memc[temp], "fits"))
			    Memc[bpmask+i-4] = EOS
		    }
		    call strcat (".pl", Memc[bpmask], SZ_FNAME)
		    call strcpy (Memc[bpmask], Memc[outbpmask], SZ_FNAME)
		}
	    }

	    if (Memc[output] == EOS)
		call strcpy (Memc[input], Memc[output], SZ_FNAME)
	    call xt_mkimtemp (Memc[input], Memc[output], Memc[temp], SZ_FNAME)

	    iferr {
		in = NULL; out = NULL; inbpm = NULL; outbpm = NULL

		tmp = immap (Memc[input], READ_ONLY, 0); in = tmp

		if (Memc[sigma_key] == EOS)
		        sigma = INDEFR
		else if (Memc[sigma_key] == '!') {
		    iferr (sigma = imgetr (in, Memc[sigma_key+1]))
		        sigma = INDEFR
		} else {
		    i = 1
		    if (ctor (Memc[sigma_key], i, sigma) == 0)
		        call error (1, "Bad sigma parameter")
		}
		if (Memc[dmin_key] == EOS)
		        datamin = INDEFR
		else if (Memc[dmin_key] == '!') {
		    iferr (datamin = imgetr (in, Memc[dmin_key+1]))
		        datamin = INDEFR
		} else {
		    i = 1
		    if (ctor (Memc[dmin_key], i, datamin) == 0)
		        call error (1, "Bad datamin parameter")
		}
		if (Memc[dmax_key] == EOS)
		        datamax = INDEFR
		else if (Memc[dmax_key] == '!') {
		    iferr (datamax = imgetr (in, Memc[dmax_key+1]))
		        datamax = INDEFR
		} else {
		    i = 1
		    if (ctor (Memc[dmax_key], i, datamax) == 0)
		        call error (1, "Bad datamax parameter")
		}
		if (Memc[maxbscale_key] == EOS)
		        maxbscale = INDEFR
		else if (Memc[maxbscale_key] == '!') {
		    iferr (maxbscale = imgetr (in, Memc[maxbscale_key+1]))
		        maxbscale = INDEFR
		} else {
		    i = 1
		    if (ctor (Memc[maxbscale_key], i, maxbscale) == 0)
		        call error (1, "Bad maxbscale parameter")
		}

		if (Memc[ibpmask] != EOS) {
		    tmp = yt_pmmap (Memc[ibpmask], in, Memc[inbpmask], SZ_FNAME)
		    inbpm = tmp
		}
		tmp = immap (Memc[output], NEW_COPY, in); out = tmp
		if (streq (Memc[bpmask], "BPM")) {
		    if (IS_INDEFR(datamin) && IS_INDEFR(datamax))
			Memc[outbpmask] = EOS
		    else if (Memc[inbpmask] == EOS)
			call error (1, "BPM not defined in the header")
		    else
			call strcpy (Memc[inbpmask], Memc[outbpmask], SZ_FNAME)
		}
		if (Memc[outbpmask] != EOS) {
		    if (Memc[inbpmask] == EOS) {
			call strcpy (Memc[outbpmask], Memc[bpmtemp], SZ_FNAME)
			tmp = immap (Memc[outbpmask], NEW_COPY, in)
		    } else {
			call xt_mkimtemp (Memc[inbpmask], Memc[outbpmask],
			    Memc[bpmtemp], SZ_FNAME)
			tmp = immap (Memc[outbpmask], NEW_COPY, inbpm)
		    }
		    outbpm = tmp
		    IM_PIXTYPE(outbpm) = TY_SHORT
		}

		call toshort (in, out, inbpm, outbpm, sigma,
		    datamin, datamax, bpval, maxbscale, bscale, bzero,
		    r2igain, r2isat, r2imin, docopy, logfile)

		if (outbpm != NULL)
		    call imunmap (outbpm)
		if (out != NULL)
		    call imunmap (out)
		if (inbpm != NULL)
		    call imunmap (inbpm)
		if (in != NULL)
		    call imunmap (in)

		# Update header.
		if (!IS_INDEFR(bscale) || Memc[outbpmask] != EOS) {
		    tmp = immap (Memc[output], READ_WRITE, 0); out = tmp
		    if (!IS_INDEFR(bscale)) {
			call imaddr (out, "BSCALE", bscale)
			call imaddr (out, "BZERO", bzero)
			call imaddr (out, "R2IGAIN", r2igain)
			call imaddr (out, "R2ISAT", r2isat)
			call imaddr (out, "R2IMIN", r2imin)
		    }
		    if (Memc[outbpmask] != EOS)
			call imastr (out, "BPM", Memc[bpmtemp])
		    call imunmap (out)
		}

		call xt_delimtemp (Memc[output], Memc[temp])
		if (Memc[outbpmask] != EOS)
		    call xt_delimtemp (Memc[outbpmask], Memc[bpmtemp])
	    } then {
		call eprintf ("%s - ")
		    call pargstr (Memc[input])
		call flush (STDERR)
		call erract (EA_WARN)
		if (outbpm != NULL) {
		    call imunmap (outbpm)
		    call imdelete (Memc[bpmtemp])
		}
		if (out != NULL) {
		    call imunmap (out)
		    call imdelete (Memc[output])
		}
		if (inbpm != NULL)
		    call imunmap (inbpm)
		if (in != NULL)
		    call imunmap (in)
	    }
	}

	if (logfile != NULL)
	    call close (logfile)
	call imtclose (outbpmlist)
	call imtclose (inbpmlist)
	call imtclose (outlist)
	call imtclose (inlist)
	call sfree (sp)
end


# TOSHORT -- Scale input data and output scaled data and optional mask.

procedure toshort (in, out, inbpm, outbpm, sigma, datamin, datamax, bpval,
	maxbscale, bscale, bzero, r2igain, r2isat, r2imin, docopy, logfile)

pointer	in		#I Input image pointer
pointer	out		#I Output image pointer
pointer	inbpm		#I Input mask pointer
pointer	outbpm		#I Output imask pointer
real	sigma		#I Sigma to sample
real	datamin		#I Data minimum for scaling
real	datamax		#I Data maximum for scaling
int	bpval		#I Mask value for data range overflow
real	maxbscale	#I Maximum bscale allowed
real	bscale		#O Scaling value
real	bzero		#O Scaling value
real	r2igain		#O Scaling value
real	r2isat		#O Scaling value
real	r2imin		#O Scaling value
bool	docopy		#I Copy if not scaling?
int	logfile		#I Logfile

bool	doscale
int	i, nc, npix
real	mean, sig, val
pointer	sp, vin, vout, vinbpm, voutbpm, str
pointer	datain, dataout, datainbpm, dataoutbpm

int	imgnlr(), imgnls(), impnls(), impnlr()
errchk	toshort_stats

begin
	call smark (sp)
	call salloc (vin, IM_MAXDIM, TY_LONG)
	call salloc (vout, IM_MAXDIM, TY_LONG)
	call salloc (vinbpm, IM_MAXDIM, TY_LONG)
	call salloc (voutbpm, IM_MAXDIM, TY_LONG)
	call salloc (str, SZ_FNAME, TY_CHAR)

	# Set data range if not already specified.
	if (IS_INDEFR (datamin) || IS_INDEFR (datamax) ||
	    (!IS_INDEFR(maxbscale) && maxbscale < 0 && IS_INDEFR(sigma)))
	    call toshort_stats (in, inbpm, npix, mean, sig, r2imin, r2isat)
	if (!IS_INDEFR (sigma))
	    sig = sigma
	if (!IS_INDEFR (datamin))
	    r2imin = datamin
	if (!IS_INDEFR (datamax))
	    r2isat = datamax

	# Set scaling parameters.
	r2igain = (r2isat - r2imin) / 65520
	bscale = r2igain
	bzero = (r2isat + r2imin) / 2

	if (logfile != NULL) {
	    call fprintf (logfile, "  BSCALE = %.4g, BZERO = %.4g\n")
	        call pargr (bscale)
		call pargr (bzero)
	    call fprintf (logfile, "  MEAN = %.4g, SIGMA = %.4g\n")
	        call pargr (mean)
		call pargr (sig)
	    call fprintf (logfile, "  DATAMIN = %.4g, DATAMAX = %.4g\n")
	        call pargr (r2imin)
		call pargr (r2isat)
	    if (!IS_INDEFR(maxbscale)) {
	        if (maxbscale > 0.) {
		    call fprintf (logfile, "  MAXBSCALE = %.4g\n")
		        call pargr (maxbscale)
		} else if (maxbscale < 0.) {
		    call fprintf (logfile, "  MAXBSCALE = %.4g\n")
		        call pargr (sig / -maxbscale)
		}
	    }
	    call flush (logfile)
	}
		
	# Check bscale limit.
	doscale = true
	if (!IS_INDEFR(maxbscale)) {
	    if (maxbscale > 0. && bscale > maxbscale) {
		call sprintf (Memc[str], SZ_FNAME,
		    "Maximum bscale exceeded (%.4g > %.4g)")
		    call pargr (bscale)
		    call pargr (maxbscale)
		if (docopy) {
		    call fprintf (logfile, "%s: Copy image\n")
			call pargstr (Memc[str])
		    doscale = false
		} else
		    call error (1, Memc[str])
	    } else if (maxbscale < 0. && bscale > sig / -maxbscale) {
		call sprintf (Memc[str], SZ_FNAME,
		    "Maximum bscale exceeded (%.4g > %.4g / %.4g)")
		    call pargr (bscale)
		    call pargr (sig)
		    call pargr (maxbscale)
		if (docopy) {
		    call fprintf (logfile, "%s: Copy image\n")
			call pargstr (Memc[str])
		    doscale = false
		} else
		    call error (1, Memc[str])
	    }
	}

	# Initialize I/O.
	nc = IM_LEN(in,1)
	call amovkl (long (1), Meml[vin], IM_MAXDIM)
	call amovkl (long (1), Meml[vout], IM_MAXDIM)
	call amovkl (long (1), Meml[vinbpm], IM_MAXDIM)
	call amovkl (long (1), Meml[voutbpm], IM_MAXDIM)

	if (doscale) {
	    IM_PIXTYPE(out) = TY_SHORT
	    while (imgnlr (in, datain, Meml[vin]) != EOF) {
		i  = impnls (out, dataout, Meml[vout])

		if (outbpm == NULL) {
		    do i = 1, nc {
			val = max (r2imin, min (r2isat, Memr[datain]))
			val = (val - bzero) / bscale
			if (val >= 0.)
			    val = val + 0.5
			else
			    val = val - 0.5
			Mems[dataout] = val
			datain = datain + 1
			dataout = dataout + 1
		    }
		} else {
		    i = impnls (outbpm, dataoutbpm, Meml[voutbpm])
		    if (inbpm == NULL)
			call aclrs (Mems[dataoutbpm], nc)
		    else {
			i = imgnls (inbpm, datainbpm, Meml[vinbpm])
			call amovs (Mems[datainbpm], Mems[dataoutbpm], nc)
		    }
		    do i = 1, nc {
			val = Memr[datain]
			if (val < r2imin || val > r2isat) {
			    val = max (r2imin, min (r2isat, Memr[datain]))
			    if (Mems[dataoutbpm] == 0)
				Mems[dataoutbpm] = bpval
			}
			val = (val - bzero) / bscale
			if (val >= 0.)
			    val = val + 0.5
			else
			    val = val - 0.5
			Mems[dataout] = val
			datain = datain + 1
			dataout = dataout + 1
			dataoutbpm = dataoutbpm + 1
		    }
		}
	    }
	} else {
	    bscale = INDEFR
	    while (imgnlr (in, datain, Meml[vin]) != EOF) {
		i  = impnlr (out, dataout, Meml[vout])
		call amovr (Memr[datain], Memr[dataout], nc)
		if (outbpm != NULL) {
		    i = impnls (outbpm, dataoutbpm, Meml[voutbpm])
		    if (inbpm == NULL)
			call aclrs (Mems[dataoutbpm], nc)
		    else {
			i = imgnls (inbpm, datainbpm, Meml[vinbpm])
			call amovs (Mems[datainbpm], Mems[dataoutbpm], nc)
		    }
		}
	    }
	}

	call sfree (sp)
end


# TOSHORT_STATS -- Find range and statistics of data which is not masked.

procedure toshort_stats (in, bpm, npix, mean, sig, dmin, dmax)

pointer	in			#I Input image pointer
pointer	bpm			#I Input mask pointer
int	npix			#O Number of good pixels
real	mean			#O Mean value
real	sig			#O Sigma (STD)
real	dmin			#O Data minimum
real	dmax			#O Data maximum

pointer	sp, v, vbpm, data, databpm
int	i, nc, ng, imgnlr(), imgnls()
real	x
double	sum1, sum2

begin
	call smark (sp)
	call salloc (v, IM_MAXDIM, TY_LONG)
	call salloc (vbpm, IM_MAXDIM, TY_LONG)

	# Initialize
	nc = IM_LEN(in,1)
	npix = 0
	mean = INDEFR
	sig = INDEFR
	dmin = MAX_REAL
	dmax = -MAX_REAL
	sum1 = 0D0
	sum2 = 0D0
	call amovkl (long (1), Meml[v], IM_MAXDIM)
	call amovkl (long (1), Meml[vbpm], IM_MAXDIM)

	# Find possibly masked minimum and maximum and STD.
	while (imgnlr (in, data, Meml[v]) != EOF) {
	    if (bpm != NULL) {
		if (imgnls (bpm, databpm, Meml[vbpm]) != EOF) {
		    do i = 1, nc {
			if (Mems[databpm] == 0) {
			    x = Memr[data]
			    dmin = min (dmin, x)
			    dmax = max (dmax, x)
			    npix = npix + 1
			    sum1 = sum1 + x 
			    sum2 = sum2 + x * x 
			}
			data = data + 1
			databpm = databpm + 1
		    }
		} else {
		    do i = 1, nc {
			x = Memr[data]
			dmin = min (dmin, x)
			dmax = max (dmax, x)
			npix = npix + 1
			sum1 = sum1 + x 
			sum2 = sum2 + x * x 
			data = data + 1
		    }
		}
	    } else {
		do i = 1, nc {
		    x = Memr[data]
		    dmin = min (dmin, x)
		    dmax = max (dmax, x)
		    npix = npix + 1
		    sum1 = sum1 + x 
		    sum2 = sum2 + x * x 
		    data = data + 1
		}
	    }
	}

	if (npix == 0)
	    call error (1, "No unmasked data")

	mean = sum1 / npix
	sig = sqrt (sum2 * npix - sum1 * sum1) / npix

	call sfree (sp)
end
