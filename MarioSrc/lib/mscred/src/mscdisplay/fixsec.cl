procedure fixsec (images)

char	images		{prompt="List of images to be fixed"}
int	xgap		{85, prompt="gap between CCD's on same controller"}

struct	*fdtmp

begin
	struct	buffer, ccdsum, fixsec
	string	in, tmp, ampid, ccdsec
	int	xsum, ysum, brk1, brk2, brk3, brk4, x1, x2

	tmp = mktemp ("uparm$tmp")
	files (images, > tmp)

	fdtmp = tmp
	while (fscan (fdtmp, in) != EOF) {

	    # Don't process images twice
	    hselect (in, "fixsec", yes) | scan (fixsec)
	    if (fixsec != "") {
	       next
	    }

	    # Get header keywords
	    hselect (in, "ampid,ccdsec,ccdsum", "yes") |
	    scan (ampid, ccdsec, ccdsum)

	    # nothing to do for LH amps
	    if (ampid=="111" || ampid=="211" || ampid=="322" || ampid=="422") {
		next
	    }

	    # Parse CCDSUM
	    print (ccdsum) | scan (xsum, ysum)

	    # Parse CCDSEC
	    brk1 = stridx ("[", ccdsec) + 1
	    brk2 = stridx (":", ccdsec) 
	    brk3 = stridx (",", ccdsec)
	    brk4 = strlen (ccdsec)
	    print (substr (ccdsec, brk1,   brk2-1)) | scan (x1)
	    print (substr (ccdsec, brk2+1, brk3-1)) | scan (x2)
	    print (substr (ccdsec, brk3,   brk4))   | scan (buffer)

	    # Modify CCDSEC
	    x1 = x1 + xgap / xsum
	    x2 = x2 + xgap / xsum

	    printf ("[%d:%d%s\n", x1, x2, buffer) | scan (ccdsec)
	    hedit (in, "ccdsec", ccdsec, add+, ver-, show-, update+)
	    hedit (in, "fixsec", "ccdsec fixed", add+, ver-, show-, update+)
	}

	delete (tmp, ver-)
end
