# FLATCOMPRESS -- Make a flat field for display by compressing to a pixel list
# with a specified resolution.

procedure flatcompress (input, output)

file	input			{prompt="Input image"}
file	output			{prompt="Output image"}
int	bin = 4			{prompt="Binning size"}
real	resolution=0.005		{prompt="Resolution"}

begin
	file	in, out, tmp1, tmp2
	real	res, ccdmean
	string	expr

	# Get input parameters.
	in = input
	out = output
	res = resolution

	# Get CCDMEAN keyword which is required.
	ccdmean = 1
	hselect (in, "ccdmean", yes) | scan (ccdmean)
	if (nscan() != 1)
	    error (1, "CCDMEAN keyword not found")

	# Do binning.
	if (bin > 1) {
	    tmp1 = mktemp ("tmp") // ".pl"
	    tmp2 = mktemp ("tmp") // ".pl"
	    blkavg (in, tmp2, bin, bin, option="average")
	    blkrep (tmp2, tmp1, bin, bin)
	    imdelete (tmp2, verify-)
	} else
	    tmp1 = in

	# Do digitization.
	if (res > 0) {
	    res = ccdmean * res
	    printf ("nint(a/%g)*%g\n", res, res) | scan (expr)
	    imexpr (expr, out, tmp1, dims="auto", intype="auto", outtype="int",
		refim="auto", rangecheck=no, verbose=no)
	    if (tmp1 != in)
		imdelete (tmp1, verify-)
	} else
	    imrename (tmp1, out, verbose=no)
end
