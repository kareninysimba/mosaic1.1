.help meffeed Sep04 MarioSrc/bin
.ih
NAME
meffeed -- feed MEF files
.ih
SUMMARY
Given a file containing a list of MEF images the images are feed to the
.hr mef MEF
pipeline with an optional sleep time between each one.
.ih
USAGE
meffeed file sleep
.ih
PARAMETERS
.ls file
File containing MEF images to be fed to the
.hr mef MEF
pipeline.  The image names
may include an IRAF node prefix since the pipelines understand this.  The
images will be fed in the order given in the file.
.le
.ls sleep
Number of seconds to sleep between each image.  A value of zero may be
used to feed all the images at once.
.le
.ih
DESCRIPTION
This host-callable CSH script reads through a file of images and feeds
each image to the MEF pipeline.  Feeding the
.hr mef MEF
pipeline means that a file
containing the image name is written to $MEFDATA/input/imagename.mef,
where imagename is the rootname of the image.  Then the pipeline trigger
file $MEFDATA/input/imagename.meftrig is touched.
.ih
EXAMPLES
1.  Feed a list of images with a 1 minute delay between images.

.nf
    % head -3 sm030927I.mef
    pipen1!/pipelines/SMSN/sm030927/waa1.030928_0157.051.fits
    pipen1!/pipelines/SMSN/sm030927/waa2.030928_0131.048.fits
    pipen1!/pipelines/SMSN/sm030927/waa3.030928_0115.046.fits
    % meffeed sm030927I.mef 60
    Feed pipen1!/pipelines/SMSN/sm030927/waa1.030928_0157.051.fits ...
    Feed pipen1!/pipelines/SMSN/sm030927/waa2.030928_0131.048.fits ...
    Feed pipen1!/pipelines/SMSN/sm030927/waa3.030928_0115.046.fits ...
    ...
.fi
.ih
SEE ALSO
.hr mjdfeed mjdfeed
,
.hr mef mef
,
.hr ngt ngt
.endhelp
