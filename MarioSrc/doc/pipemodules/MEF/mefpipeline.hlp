.help mefpipeline Sep04 mario
.ih
NAME
mef -- pipeline to process MEF exposures with library calibrations
.ih
SUMMARY
The MEF pipeline takes single MEF raw science exposures and does first pass
calibrations using library calibrations.
.ih
INPUT
This pipeline has two types of input.  The first is an @file containing
the IRAF URL for the MEF exposure to process.  The @file has the form
$MEFDATA/input/<dataset>.mef where <dataset> is any name to be used to
identify the dataset.  The trigger to run the pipeline on the @file is a
file of the same name with the string "trig" appended.  The contents of
the trigger file is ignored.

The second input is an @file, again containing the URL of the exposure
to process, with the extension ".tel" instead of ".mef".  The trigger
is similarly a file with "trig" appended.  In this case the MEF file
is examined to determine the type of exposure based on the OBSTYPE
keyword.  Zero and dome flat exposures are converted to triggers for
the
.hr cal CAL
pipeline while other types are converted to the ".mef" type of input.
.sp 2
.in -8
.ih
OUTPUT
.ih
CALLED BY
.ih
RETURNS TO
.ih
CALLS
.ih
STAGES
.hr mefstart mefstart
.br
.hr mefsetup mefsetup
.br
.hr mefxtalk mefxtalk
.br
.hr meffrsif meffrsif
.br
.hr mefgcat mefgcat
.br
.hr mefwace mefwace
.br
.hr mefacedq mefacedq
.br
.hr mefwcs mefwcs
.br
.hr mefphot mefphot
.br
.hr mefhdr mefhdr
.br
.hr mefraw mefraw
.br
.hr mefmkmef mefmkmef
.br
.hr mefmos mefmos
.br
.hr mefdone mefdone
.br
.hr meftel meftel
.br
.hr meferr meferr
.br
.hr mefcalck mefcalck
.br
.hr mefobs mefobs
.ih
DESCRIPTION
.ih
SEE ALSO
.hr meffeed meffeed
.hr mjdfeed mjdfeed
.endhelp
