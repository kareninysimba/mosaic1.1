#{ LOGIN.CL -- Pipeline login file.

# Common variables. 
string	telescope = ""	{prompt="Telescope name or reference"}
string	instrument = "" {prompt="Instrument name or reference"}
string	cm = ""		{prompt="Calibration manager connection"}
string	dm = ""		{prompt="Data manager connection"}

# Read the command line variables.
cache nhpps
nhpps

# Make more unique pipe names.  We still use /tmp for efficiency.
reset pipes = "/tmp/" // nhpps.dataset

# Load standard packages.
mario
proto
servers
clpackage

# Cache common parameters and override defaults.
cache dir, delete, tee, getcal
dir.max = 0
delete.verify = no
tee.append = yes
nhedit.verify = no

# Do application, module, and dataset dependent setups.
if (nhpps.application != "None") {

    # Set application configuration parameters and override
    # with user application and dataset specific values.

    plconfig

    # Set the tmp directory.
    printf ("names (nhpps.pipeline, nhpps.dataset); keep\n") | cl

    # Do things that depend on the datadir being present.
    # For more see mario$plmodule use in login2.cl.
    if (access(names.datadir))
        reset (tmp = names.datadir)
    ;
}
;

keep
