#{ LOGIN.CL -- Pipeline login file.

# Common variables. 
string	telescope = ""	{prompt="Telescope name or reference"}
string	instrument = "" {prompt="Instrument name or reference"}
string	cm = ""		{prompt="Calibration manager connection"}
string	dm = ""		{prompt="Data manager connection"}

# Read the command line variables.
cache nhpps
nhpps

# Make more unique pipe names.  We still use /tmp for efficiency.
reset pipes = "/tmp/" // nhpps.dataset

# Load standard packages.
images
utilities
proto
servers
mario
clpackage

# Cache common parameters and override defaults.
cache dir, delete, tee, hedit, nhedit
dir.max = 0
delete.verify = no
tee.append = yes
hedit.verify = no
hedit.show = no
hedit.update = yes 
nhedit.verify = no
nhedit.show = no
nhedit.update = yes 

# Do application, module, and dataset dependent setups.
if (nhpps.application != "None") {

    # Set application configuration parameters and override
    # with user application and dataset specific values.

    plconfig

    # Do standard module initialization and logging.

    if (nhpps.module != "None" && nhpps.dataset != "None")
	plmodule
    ;
}
;

keep
