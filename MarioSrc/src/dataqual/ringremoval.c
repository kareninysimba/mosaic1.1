/*

  ringremoval.c

  How to compile with graphical output:
  g77 ringremoval.c -o ringremoval -Wall -lcfitsio -lm

  Description:

  HISTORY:
  02/09/10   Document created
  02/10/10   Version 1.0
  02/16/10   Added adding a skylevel after ring subtraction, added
             an optional 5 sigma rejection pass for statistics
  02/19/10   Make rejection parameters specifiable from the command line,
             included mimumum inner ring radius (everything within that
	     radius ends up in one wide ring)
  02/21/10   Included averaging over pixels and rejection of high-noise
             areas. Removed averaging (see ringremoval2.c) and included
	     percentile clipping.

*/

#include <fitsio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Definitions */
#define  CHANNELWIDTH 32
#define  DETWIDTH 2048
#define  NARROWWIDTH 4

/* declare global variables */
char programname[120];           /* name under which this program is run */
int verbose = 0;                 /* default level for verbose */

/* declare functions */
void flag(int num);
void error(char *message, int giveusage);
void usage( void );
void pop(char **elements, int num, int *max);
float sqr(float x);
void keyname( char *pre, char *name, char keyword[80] );
int savefits( float *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits );
int savefits_i( int *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits );
void getstats( float *array, int numitems, float *mean, float *sigma, int offset );
void getstatsfilter(float *array, int numitems, float low, float high, int niter, float *mean, float *sigma, int offset );
void heapsort( float *arr, unsigned int N );

/* main body of the program */
int main(int argc, char **argv) {

   /* general variables */
   char message[256];            /* message string */
   char *stop;                   /* string needed for string-to-float conversion */
   int i,j,k;                    /* counters */
   float mean,sigma;
   
   /* variables related to command line options */
   int xdef = 0, ydef = 0;       /* variables related to ring center */
   float xcenter, ycenter;       
   int ndef = 0, nrings = 100;   /* variables related to number of rings */
   int sdef = 0;                 /* variables related to added sky */
   float sky = 0;
   int fdef = 0;                 /* variables related to filter application */
   int npass = 3;                /* default rejection parameters */
   float lo=3, hi=3;
   int inner = 1;                /* starting ring radius */
   int pdef = 0;                 /* variablres related to percentile clipping */
   float pclip;

   /* variables related to reading of fits files */
   int status = 0;               /* status must always be initialized = 0  */
   int bitpix;                   /* bytes per pixel */
   int naxis;                    /* number of axes in FITS file */
   long naxes[2] = {1,1};        /* lower limit of pixels to be read */

   /* variables related to input and output fits file */
   fitsfile *INFITS;             /* fits file pointers defined in fitsio.h */
   fitsfile *BPMFITS;            /* fits file pointers defined in fitsio.h */
   long nx,ny;                   /* variables related to image size */
   char fitsfilename[120];       /* fits file name */
   char outfilename[120];        /* fits file name */
   char bpmfilename[120];        /* bpm file name */
   float *data;                  /* array for fits data */
   int *bpmdata;                 /* array for bpm data */
   long ndat;                    /* number of data elements in array */

   /* variables related to the ring removal */
   float xo, yo;                 /* x and y offsets */
   float rmin, rmax, rdiff;      /* radial distances */
   float *theta;                 /* array for angles */
   int *ringno;                  /* array for ring numbers */
   float *ring_i,*ring_t;        /* arrays for ring intensity and theta */
   int *datapos;                 /* array for pixel position in data array */
   int nok;                      /* number of unmasked pixels in a ring */
   int ntot;                     /* total number of pixels in a ring */

   strcpy( programname, argv[0] ); /* store program name in global variable */

   /* Process command line options */
   i = argc-1;
   while (i>0) { /* Make a first loop to find -v arguments to determine verbose level */
      if ( strcmp(argv[i],"-v") == 0 ) {
	 verbose++; /* increase verbosity */
	 pop(argv,i,&argc); /* remove from argument list */
      }
      i--;
   }
   if (verbose>=1) printf("Verbose level: %i (out of 4)\n", verbose);
   if (verbose>=2) printf("Processing command line options:\n");
   if (verbose>=2) printf("   Number of command line arguments: %i\n", argc);
   i = argc-1;
   while (i>=1) { /* begin loop over command line arguments */
      if ( strcmp(argv[i], "-h")==0) /* help is asked, so give it and exit */
	 usage();
      if ( strcmp(argv[i], "-f")==0) {
	if (verbose>=2) printf("   5 sigma filter will be applied to statistics\n");
	fdef = 1;
	pop(argv,i,&argc);
      }
      if ( strcmp(argv[i], "-hi")==0 ) {
         if (i+1<argc) { /* Need one more argument for high rejection threshold */
            hi = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -hi\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   High rejection threshold supplied by user: %f\n", hi);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-hi is missing its argument",0);
      }
      if ( strcmp(argv[i], "-i")==0 ) {
         if (i+1<argc) { /* Need one more argument for starting ring number */
            inner = (int) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -i\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Inner ring radius requested by user: %i\n", nrings);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-i is missing its argument",0);
      }
      if ( strcmp(argv[i], "-lo")==0 ) {
         if (i+1<argc) { /* Need one more argument for low rejection threshold */
            lo = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -lo\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Low rejection threshold supplied by user: %f\n", lo);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-lo is missing its argument",0);
      }
      if ( strcmp(argv[i], "-n")==0 ) {
         if (i+1<argc) { /* Need one more argument for number of rings */
            nrings = (int) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -n\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Number of rings requested by user: %i\n", nrings);
            ndef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-n is missing its argument",0);
      }
      if ( strcmp(argv[i], "-nr")==0 ) {
         if (i+1<argc) { /* Need one more argument for number of rejection cycles */
            npass = (int) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -nr\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Number of rejection cycles requested by user: %i\n", npass);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-nr is missing its argument",0);
      }
      if ( strcmp(argv[i], "-p")==0 ) {
         if (i+1<argc) { /* Need one more argument for sky level */
            pclip = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -p\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for percentile clipping supplied by user: %f\n", pclip);
            pdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-p is missing its argument",0);
      }
      if ( strcmp(argv[i], "-s")==0 ) {
         if (i+1<argc) { /* Need one more argument for sky level */
            sky = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -s\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for added sky supplied by user: %f\n", sky);
            sdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-s is missing its argument",0);
      }
      if ( strcmp(argv[i], "-x")==0 ) {
         if (i+1<argc) { /* Need one more argument for x position */
            xcenter = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -x\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for x center supplied by user: %f\n", xcenter);
            xdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-x is missing its argument",0);
      }
      if ( strcmp(argv[i], "-y")==0 ) {
         if (i+1<argc) { /* Need one more argument for y position */
            ycenter = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -y\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for y center supplied by user: %f\n", ycenter);
            ydef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-y is missing its argument",0);
      }
      i--;
   } /* end loop over command line arguments */

   /* After the known options have been processed, one argument, the  
      input fits file, should remain. */
   /* List the remaining arguments */
   if (verbose>=2) printf("   Remaining command line arguments: %i ", argc-1);
   if ((verbose>=2)&&(argc>1)) {
      for(i=1;i<argc;i++) { printf("%s ",argv[i]); }
      printf("\n");
   }
   if (argc>3) { /* Check whether two arguments remain */
      error("too many or unknown command line arguments given",1);
   } else if (argc<3) error("too few file names given",1);
   /* Two command line arguments left, copy to strings */
   strcpy(fitsfilename, argv[1]);
   strcpy(bpmfilename, argv[2]);
   if (verbose>=2) {
      printf("   Input fits file: %s\n", fitsfilename );
      printf("   Input bpm file: %s\n", bpmfilename );
   }

   /* Check whether all the necessary information is available. 
      Provide defaults where not, or give an error message. */
   if (xdef==0) error("no x center has been supplied",1);
   if (ydef==0) error("no y center has been supplied",1);

   /* Open data fits file and obtain basic information */
   fits_open_file(&INFITS, fitsfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(INFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(INFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(INFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",fitsfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* set array dimensions and number of elements */
   nx = naxes[0];
   ny = naxes[1];
   ndat = nx*ny;
   if (verbose>=3) printf("nx: %li  ny: %li\n", nx, ny );
   /* Allocate memory for data array */
   data = (float *)calloc((unsigned) (ndat), sizeof(float));
   if (data == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(INFITS, TFLOAT, naxes, ndat, NULL, data, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Open BPM fits file and obtain basic information */
   fits_open_file(&BPMFITS, bpmfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(BPMFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(BPMFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(BPMFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",bpmfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* set array dimensions and number of elements */
   if ((naxes[0]!=nx)||(naxes[1]!=ny)) {
     error("Input fits and bpm images do not have the same size\n",0);
   }
   /* Allocate memory for data array */
   bpmdata = (int *)calloc((unsigned) (ndat), sizeof(int));
   if (bpmdata == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(BPMFITS, TINT, naxes, ndat, NULL, bpmdata, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Start of main code */

   /* Find the minimum ring radius ... */ 
   if (xcenter<1) {
     xo = 1-xcenter;
   } else if (xcenter>nx) {
     xo = xcenter-nx;
   } else {
     xo = 0;
   }
   if (ycenter<1) {
     yo = 1-ycenter;
   } else if (ycenter>ny) {
     yo = ycenter-ny;
   } else {
     yo = 0;
   }
   rmin = sqrt( xo*xo+yo*yo );
   /* ... and the maximum ring radius */
   if ( (xcenter-1) > (nx-xcenter) ) {
     xo = xcenter-1;
   } else {
     xo = nx-xcenter;
   }
   if ( (ycenter-1) > (ny-ycenter) ) {
     yo = ycenter-1;
   } else {
     yo = ny-ycenter;
   }
   rmax = sqrt( xo*xo+yo*yo );
   rdiff = rmax-rmin;
   /*
   printf( "%d %d\n", naxes[0], naxes[1] );
   printf( "%f %f\n", xcenter, ycenter );
   printf( "%f %f\n", xo, yo );
   printf( "%f %f\n", rmin, rmax );
   */

   /* Create array with theta, ring */
   theta = (float *)calloc((unsigned) (ndat), sizeof(float));
   ringno = (int *)calloc((unsigned) (ndat), sizeof(int));
   /* Loop over all points in the image */
   for(i=0;i<ndat;i++) {
     /* Convert array index into (j,k) position */
     k = (int)(i/nx);
     j = i-k*nx;
     /* Calculate distance to ring center */
     xo = j-xcenter;
     yo = k-ycenter;
     theta[i] = (float) atan2( xo, yo );
     ringno[i] = (int)(nrings*(sqrt(xo*xo+yo*yo)-rmin)/rdiff)+1;
   }

   /* 
      To do the next step efficiently, it is best to loop over the
      arrays once, and count the number of pixels in each ring. Next,
      the arrays should be rearranged by ringnumber. This should be
      fairly fast if one reads the first pixel, move its content to
      where it is supposed to be based on its ringno, move the content
      of that pixel to its next location, etc. For example, take the
      following array:
      4 3 4 2 1 4 3 3 4 2 4 1 1 2 3 4 3 4 2
      There are 3 1s, 4 2s, 5 3s, and 7 4s. So the 1s should start
      at position 1, the 2s at 4, the 3s at 8, and the 4s at 13. Now
      read the first pixel, a 4. This should go to position 13. Read
      position 13, which contains a 1, which should go to 1. Simply
      swap these two, move on to position 2. This contains a 3, which
      should go to position 8. This already contains a 3, so use
      position 9, which contains a 4. Put the 3 at 9, and then the
      4 should go to 14, which contains a 2. Put the 4 at 14, the
      2 should go to position 4, etc. 
      Along with the ringno, the theta and data arrays should also
      be rearranged.
      When the rearranging is complete, one may simply loop over all
      the known ringnos and make the relevant fits or determine the
      relevant averages.
      For the time being, however, the brute-force approach is 
      taken.
   */

   /* Allocate space for the points within each ring. The maximum
      possible number of points is a circle that is just enclosed
      within the SQUARE image with s pixels on each side, i.e.,
      Nmax = 2*pi*(s/2)*w = pi*s*w. Here, w is the width of each
      ring, which is s*sqrt(2)/nrings. So N = pi*sqrt(2)*s^2/nrings.
      Also, increase the amount by a small fudge factor to create
      some extra headroom, so N = 5*s^2/nrings. 
      A further scale factor is needed when a larger inner ring is
      specified. In extreme cases, inner could be the same as nrings,
      in which case the number of points in each "ring" would be the 
      same as the image size. Multiply with inner, and cap at ndat.
   */
   j = (int)(inner*5*nx*nx/nrings);
   if (j>ndat) j = ndat;
   ring_i = (float *)calloc((unsigned) (j), sizeof(float));
   ring_t = (float *)calloc((unsigned) (j), sizeof(float));
   datapos = (int *)calloc((unsigned) (j), sizeof(int));

   /* Loop over all rings */ 
   for(j=inner;j<=nrings;j++) {
     nok = ntot = 0;
     for(i=0;i<ndat;i++) {
       /* Select all points for the current ring that are not masked */
       if ( ((j==inner)&&(ringno[i]<=inner)) || ((j>inner)&&(ringno[i]==j)) ) {
	 /* if (ringno[i]==j) { */
	 if (bpmdata[i]==0) {
	   ring_i[nok] = data[i];
	   ring_t[nok] = theta[i];
	   nok++;
	 }
	 datapos[ntot] = i;
	 ntot++;
       }
     }
     if (pdef) {
       /* Sort the array */
       if ( nok > 0 ) {
	 heapsort( ring_i, nok );
       }
       /* Determine start and end points of data included within 
	  the percentile clipping range */ 
       i = (int)((1-pclip)*nok/2);
       k = (int)((1+pclip)*nok/2);
       /* Calculate clipping range */
       k = k-i+1;
       /* Determine the statistics in the pclipped range */
       getstats( &ring_i[i], k, &mean, &sigma, 0 );
     } else {
       /* Determine statistics within the ring */
       getstats( ring_i, nok, &mean, &sigma, 0 );
       if (fdef&&(verbose>=3)) printf("Stats before\t%f %f\t", mean, sigma );
       /* Include one 5 sigma rejection pass if needed */
       if (fdef) {
	 getstatsfilter( ring_i, nok, lo, hi, npass, &mean, &sigma, 0 );
	 if (verbose>=3) printf("after\t%f %f\n", mean, sigma );
       }
     }
     /* Subtract the mean from the data, add in sky level */
     for(i=0;i<ntot;i++) {
       data[datapos[i]] -= mean-sky;
     }
   }

   i = strlen(fitsfilename)-5;
   strncpy( outfilename, fitsfilename, i );
   outfilename[i] = '\0';
   strcat( outfilename, "_out.fits" );
   if (verbose>=1) printf( "Creating outfile: %s\n", outfilename );
   savefits( data, nx, ny, INFITS, outfilename );

   /* Close the fits file */
   fits_close_file( INFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   free( (float *)data );
   free( (float *)theta );
   free( (float *)ring_i );
   free( (float *)ring_t );
   free( (int *)ringno );
   free( (int *)datapos );

   exit(1);

} /* end main */

/* Start of function definitions */

void flag(int num) {
  printf ( "Flag %i\n", num );
}

void error(char *message, int giveusage) {
   printf("ERROR: %s\n", message);
   if (giveusage) usage();
   else exit(0);
}

void usage( void ) {
   printf("Usage: %s [options] <data file> <template file> <continuum file>\n", programname);
   printf("    [-h] (provide this help)\n");
   printf("    [-v (increase verbosity by 1)]\n");
   exit(0);
}

void pop(char **elements, int num, int *max) {
   int i;
   
   if (num<*max) {
      for(i=num+1;i<*max;i++) {
         elements[i-1] = elements[i];
      }
   }
   *max = *max-1;
}

float sqr(float x){
   return x*x;
} 

void keyname( char *pre, char *name, char keyword[80] ) {
  strcpy(keyword,"dq");
  strcat(keyword,pre);
  strcat(keyword,name);
}

int savefits( float *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits ) {
   int status=0;
   fitsfile *OUTFITS;    /* FITS file pointers defined in fitsio.h */
   char overwriteout[125];

   /* Make output file overwritable by prefixing a ! */
   strcpy( overwriteout, "!" );
   strcat( overwriteout, outfits );

   fits_create_file(&OUTFITS, overwriteout, &status); /* Open the output file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_copy_hdu(ORIGFITS, OUTFITS, 0, &status); /* Copy the original header */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   /* Write the fits file to disk */
   status = fits_write_img(OUTFITS, TFLOAT, 1, nx*ny, data, &status);
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_close_file( OUTFITS, &status ); /* Close the fits file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   return(1);
}

int savefits_i( int *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits ) {
   int status=0;
   fitsfile *OUTFITS;    /* FITS file pointers defined in fitsio.h */
   char overwriteout[125];

   /* Make output file overwritable by prefixing a ! */
   strcpy( overwriteout, "!" );
   strcat( overwriteout, outfits );

   fits_create_file(&OUTFITS, overwriteout, &status); /* Open the output file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_copy_hdu(ORIGFITS, OUTFITS, 0, &status); /* Copy the original header */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   /* Write the fits file to disk */
   status = fits_write_img(OUTFITS, TINT, 1, nx*ny, data, &status);
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_close_file( OUTFITS, &status ); /* Close the fits file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   return(1);
}

void getstats( float *array, int numitems, float *mean, float *sigma, int offset ) {
  int i;
  double _mean=0,_sigma=0;

  for(i=offset;i<numitems+offset;i++) {
    _mean += array[i];
    _sigma += array[i]*array[i];
  }
  if (numitems>1)
    _sigma = sqrt( (_sigma-_mean*_mean/numitems)/(numitems-1) );
  else
    _sigma = 0;
  _mean /= (float)numitems;
  
  *mean = (float)_mean;
  *sigma = (float)_sigma;
}

void getstatsfilter(float *array, int numitems, float low, float high, int niter, float *mean, float *sigma, int offset ) {
  int i,j,count,prevcount;
  double _mean,_sigma;
  float *data;

  data = (float *)calloc((unsigned) numitems, sizeof(float));
  for(i=0;i<numitems;i++) data[i] = array[i+offset];

  count = numitems;
  _mean = (double)(*mean);
  _sigma = (double)(*sigma);
  if (numitems>1) {
    for(i=1;i<=niter;i++) {
      prevcount = count;
      count = 0;
      for(j=0;j<prevcount;j++) {
	if ((data[j]>=_mean-low*_sigma)&&(data[j]<=_mean+high*_sigma)) {
	  data[count] = data[j];
	  count++;
	}
      }
      _mean = _sigma = 0;
      for(j=0;j<count;j++) {
	_mean += (double)data[j];
	_sigma += (double)(data[j]*data[j]);
      }
      _sigma = sqrt( (_sigma-_mean*_mean/count)/(count-1) );
      _mean /= count;
    }
  }
  *mean = (float)_mean;
  *sigma = (float)_sigma;
  free( (float *)data );
}

void heapsort( float *arr, unsigned int N ) {
  unsigned int n = N, i = n/2, parent, child;
  float t;
 
  for (;;) { /* Loops until arr is sorted */
    if (i > 0) { /* First stage - Sorting the heap */
      i--;           /* Save its index to i */
      t = arr[i];    /* Save parent value to t */
    } else {     /* Second stage - Extracting elements in-place */
      n--;           /* Make the new heap smaller */
      if (n == 0) return; /* When the heap is empty, we are done */
      t = arr[n];    /* Save last value (it will be overwritten) */
      arr[n] = arr[0]; /* Save largest value at the end of arr */
    }
 
    parent = i; /* We will start pushing down t from parent */
    child = i*2 + 1; /* parent's left child */
 
    /* Sift operation - pushing the value of t down the heap */
    while (child < n) {
      if (child + 1 < n  &&  arr[child + 1] > arr[child]) {
	child++; /* Choose the largest child */
      }
      if (arr[child] > t) { /* If any child is bigger than the parent */
	arr[parent] = arr[child]; /* Move the largest child up */
	parent = child; /* Move parent pointer to this child */
	child = parent*2 + 1; /* Find the next child */
      } else {
	break; /* t's place is found */
      }
    }
    arr[parent] = t; /* We save t in the heap */
  }
}
