package dataqual

task $biasstruct = "$!biasstruct $(1).fits"
task $flatstruct = "$!biasstruct -f $(1).fits"
task $rflatstruct = "$!biasstruct -ref $(1).fits"
task $aflatstruct = "$!biasstruct -ad $(1).fits"
task $sflatstruct = "$!biasstruct -sf $(1).fits"
task $verifyheader = "$!verifyheader $1.fits $2.fits"
task overscan = dataqual$overscan.cl
task fixnoisychan = "$!fixnoisychan.cl $(1).fits"
task removerings = dataqual$removerings.cl
task removestripes = dataqual$removestripes.cl
task storekeywords = dataqual$storekeywords.cl
task imacedq = dataqual$imacedq.cl
task dqgsky = dataqual$dqgsky.cl
task dqsifhdr = dataqual$dqsifhdr.cl

cache	imacedq
if (defpac("tables"))
    cache tstat
;

clbye
