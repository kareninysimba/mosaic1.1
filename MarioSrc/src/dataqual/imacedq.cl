# IMACEDQ -- Generate catalog and sky information from an image.
#
# This routine creates files, updates the image header and PMASS,
# and returns values in task parameters.

procedure imacedq (image, dqmask, skysub, sky, sig, obm, cat, lfile, dm, tmp)

file	image			{prompt="Input image"}
string	dqmask			{prompt="Input data quality mask"}
file	skysub			{prompt="Output sky subtracted image"}
file	sky			{prompt="Output sky map"}
file	sig			{prompt="Output sigma map"}
file	obm			{prompt="Output object segmentation mask"}
file	cat			{prompt="Output catalog"}
file	lfile			{prompt="Output logfile"}
string	dm			{prompt="PMASS address"}
file	tmp			{prompt="Temporary file rootname"}

string	convolve = ""		{prompt="ACE convolution kernel"}
real	hsigma = 3.		{prompt="ACE high detection sigma"}
int	minpix = 6		{prompt="ACE minimum pixel area"}
int	ngrow = 2		{prompt="ACE ring grow"}
real	agrow = 2.		{prompt="ACE area grow"}
file	catdefs = "imacedq.def"	{prompt="ACE catalog definitions"}
string	magzero = "!MAGZERO"	{prompt="ACE magnitude zeropoint"}
int	verbose = 2		{prompt="ACE verbose level"}

bool	mosaic = yes		{prompt="Image part of mosaic?"}
real	seeingp			{prompt="Seeing (pix)"}
real	dqskfrc			{prompt="Sky fraction"}
int	dqobmxa			{prompt="Maximum object area (pix)"}
real	skyadu			{prompt="Mean sky level (ADU)"}
real	skynoise		{prompt="Mean sky sigma (ADU)"}
real	skymode			{prompt="Sky mode for residual sky (ADU)"}
real	skybg			{prompt="Sky background for residual sky (ADU)"}
real	dpthadu			{prompt="Photometric depth (ADU)"}
int	status			{prompt="Return status"}

real	cafwhm = 3.		{prompt="Default circular ap diameter (FWHM)"}

struct	*list

begin
	int	stat
	real	mz, p1, p2
	real	eskyadu, skyxgrad, skyygrad, eskynoise, sigxgrad, sigygrad
	string	wcs, obmextn, sval1, sval2
	string	seeingp_key, skyadu_key, skynoise_key, dpthadu_key
	string	skybg_key, dqskeadu_key
	struct	dqrej, skyline, skylinebg, sigline

	# Initialize.
	if (mosaic) {
	    seeingp_key = "SEEINGP1"
	    skyadu_key = "SKYADU1"
	    skynoise_key = "SKYNOIS1"
	    dpthadu_key = "DPTHADU1"
	    skybg_key = "SKYBG1"
	    dqskeadu_key = "DQSKEAD1"
	} else {
	    seeingp_key = "SEEINGP"
	    skyadu_key = "SKYADU"
	    skynoise_key = "SKYNOISE"
	    dpthadu_key = "DPTHADU"
	    skybg_key = "SKYBG"
	    dqskeadu_key = "DQSKEADU"
	}
	obmextn = obm // "[pl]"
	seeingp = INDEF; dqskfrc = INDEF; dqobmxa = INDEF
	skyadu = INDEF; skynoise = INDEF; dpthadu = INDEF
	skymode = INDEF; skybg = INDEF; skylinebg = "INDEF"
	status = 1

	# Check if image has been previously rejected.
	hselect (image, "$DQREJ", yes) | scan (dqrej)
	dqrej = trim (dqrej)

	# Create sky, segmentation, and catalog files.
	if (dqrej == "INDEF") {
	    # Detect and catalog sources, make sky and sigma maps.
	    iferr {
		aceall (image, masks=dqmask, skyotype="subsky", skies=sky,
		    sigmas=sig, objmasks=obm, catalogs=cat, skyimage=skysub,
		    exps="", gains="", omtype="all", extnames="",
		    catdefs=catdefs, catfilter="", logfiles=lfile,
		    verbose=verbose, order="", nmaxrec=INDEF,
		    gwtsig=INDEF, gwtnsig=INDEF, fitstep=100, fitblk1d=10,
		    fithclip=2., fitlclip=3., fitxorder=1, fityorder=1,
		    fitxterms="half", blkstep=1, blksize=-10,
		    blknsubblks=2, updatesky=yes, bpdetect="100",
		    bpflag="1-5", convolve=convolve, hsigma=hsigma, lsigma=10.,
		    hdetect=yes, ldetect=no, neighbors="8", minpix=6,
		    sigavg=4., sigmax=4., bpval=INDEF, splitmax=INDEF,
		    splitstep=0., splitthresh=5., sminpix=8, ssigavg=10.,
		    ssigmax=5., ngrow=ngrow, agrow=agrow, magzero=magzero,
                    cafwhm=cafwhm)
		rename (cat//".txt", cat)
	    } then {
	        sendmsg ("WARNING", "ACE failed", image, "PROC")
		status = 2
	    } else
	        ;
	} else {
	    sendmsg ("WARNING", dqrej, image, "PROC") 
	    status = 2
	}

	# Analyze object segmentation mask for sky fraction.
	if (imaccess(obmextn)) {
	    imstat (obmextn, fields="npix", format-) | scan (x)
	    imstat (obmextn, fields="npix", format-, upper=0.5) | scan (y)
	    printf ("%.3f\n", y/x) | scan (dqskfrc)
	} else  {
	    sendmsg ("WARNING", "Object mask not created", image, "PROC")
	    status = 2
	}

	# Analyze catalog for seeing and maximum object area.
	if (access(cat)) {
	    thselect (cat, "FWHM", yes) | scan (seeingp)
	    tstat.vmax = INDEF
	    tstat (cat, "NPIX", >> lfile)
	    dqobmxa = tstat.vmax
	} else {
	    sendmsg ("WARNING", "Catalog not created", image, "PROC")
	    status = 2
	}

	# Analyze sky map for mean and gradients.
	skyline = "INDEF"; eskyadu = INDEF; skyxgrad = INDEF; skyygrad = INDEF
	if (imaccess(sky)) {
	    listpix (sky, wcs="physical", verbose-) |
		surfit ("STDIN", func="chebyshev", xorder=2, yorder=2,
		    xterms="half", weight="user") |
		fields ("STDIN", "3-4", lines="16-18", > tmp)
	    list = tmp
	    stat = fscan (list, skyline)
	    stat = fscan (skyline, skyadu, eskyadu)
	    stat = fscan (list, skyxgrad)
	    stat = fscan (list, skyygrad)
	    list = ""; delete (tmp)
	    hselect (image, "$SKYMODE", yes) | scan (skymode)
	    if (isindef(skymode) == NO) {
	        skybg = skyadu + skymode
		printf ("%g %g\n", skybg, eskyadu) | scan (skylinebg)
	    }
	} else {
	    sendmsg ("WARNING", "Sky map not created", image, "PROC")
	    status = 2
	}

	# Analyze sigma map for sky noise and gradients.
	sigline = "INDEF"; eskynoise = INDEF; sigxgrad = INDEF; sigygrad = INDEF
	if (imaccess(sig)) {
	    listpix (sig, wcs="physical", verbose-) |
		surfit ("STDIN", func="chebyshev", xorder=2, yorder=2,
		    xterms="half", weight="user") |
		fields ("STDIN", "3-4", lines="16-18", >> tmp)
	    list = tmp
	    stat = fscan (list, sigline)
	    stat = fscan (sigline, skynoise, eskynoise)
	    stat = fscan (list, sigxgrad)
	    stat = fscan (list, sigygrad)
	    list = ""; delete (tmp)
	} else {
	    sendmsg ("WARNING", "Sigma map not created", image, "PROC")
	    status = 2
	}

	# Estimate photometric depth (we only care about the ADU.
	# Set photdth as 5 sigma detection with an optimal aperture of
	# 1.35 FWHM (which is formally optimal for pure Gaussian FWHM).
	# Later an average will define the global photometric depth and
	# adjust the zero point.

	hselect (image, "$MAGZERO,$PIXSCAL1,$PIXSCAL2", yes) | scan (mz, p1, p2)
	if (isindef(mz))
	    hselect (image, "$MAGZREF", yes) | scan (mz)
	;
	photdpth (skynoise, seeingp, mz, sqrt(p1*p2), verbose=0)
	dpthadu =  photdpth.dpthadu
	if (isindef(dpthadu)) {
	    sendmsg ("WARNING", "Photometric depth cannot be determined",
	        image, "PROC")
	    #status = 2
	} else
	    printf ("%.5g\n", dpthadu) | scan (dpthadu)

	# Update image headers.
	if (imaccess(image)) {
	    if (imaccess(sky))
		nhedit (image, "SKYIM", sky, "Sky map", add+, verify-, show-)
	    if (imaccess(sig))
		nhedit (image, "SIGIM", sig, "Sigma map", add+, verify-, show-)
	    if (isindef(seeingp) == NO)
		nhedit (image, seeingp_key, seeingp, "[pix] seeing", add+, verify-, show-)
	    if (isindef(dpthadu) == NO)
		nhedit (image, dpthadu_key, dpthadu,
		    "Photometric depth (BUNIT)", add+, verify-, show-)
	    if (isindef(dqskfrc) == NO)
		nhedit (image, "DQSKFRC", dqskfrc,
		    "Frac of pixels which are sky", add+, verify-, show-)
	    if (isindef(dqobmxa) == NO)
		nhedit (image, "DQOBMXA", dqobmxa,
		    "Max num of pixels in one object", add+, verify-, show-)
	    if (skyline != "INDEF") {
		nhedit( image, skyadu_key, skyadu, "Mean sky (BUNIT)", add+, verify-, show-)
		nhedit (image, "SKYXGRAD", skyxgrad,
		    "Sky X gradient (BUNIT/pix)", add+, verify-, show-)
		nhedit (image, "SKYYGRAD", skyygrad,
		     "Sky Y gradient (BUNIT/pix)", add+, verify-, show-)
		if (isindef(skymode))
		    nhedit (image, "SKY", skyline,
			"Mean and StdDev of sky (BUNIT)", add+, verify-, show-)
		else {
		    nhedit (image, "SKY", skylinebg,
			"Mean and StdDev of sky (BUNIT)", add+, verify-, show-)
		    nhedit (image, skybg_key,
		        skybg, "Sky background (BUNIT)", add+, verify-, show-)
		    nhedit (image, dqskeadu_key, eskyadu,
		        "Error in sky background (BUNIT)", add+, verify-, show-)
		}
	    }

	    if (sigline != "INDEF") {
		nhedit (image, "SIG", sigline,
		    "Mean and StdDev of sky sigma (BUNIT)", add+, verify-, show-)
		nhedit( image, skynoise_key, skynoise,
		    "Mean sky noise (BUNIT)", add+, verify-, show-)
		nhedit (image, "SIGXGRAD", sigxgrad,
		    "Sky sigma X gradient (BUNIT/pix)", add+, verify-, show-)
		nhedit (image, "SIGYGRAD", sigygrad,
		    "Sky sigma Y gradient (BUNIT/pix)", add+, verify-, show-)
	    }
	}
	if (imaccess(skysub)) {
	    if (imaccess(sky))
		nhedit (skysub, "SKYIM", sky, "Sky map", add+, verify-, show-)
	    if (imaccess(sig))
		nhedit (skysub, "SIGIM", sig, "Sigma map", add+, verify-, show-)
	    if (isindef(seeingp) == NO)
		nhedit (skysub, seeingp_key, seeingp, "[pix] seeing", add+, verify-, show-)
	    if (isindef(dpthadu) == NO)
		nhedit (skysub, dpthadu_key, dpthadu,
		    "Photometric depth (BUNIT)", add+, verify-, show-)
	    if (isindef(dqskfrc) == NO)
		nhedit (skysub, "DQSKFRC", dqskfrc,
		    "Frac of pixels which are sky", add+, verify-, show-)
	    if (isindef(dqobmxa) == NO)
		nhedit (skysub, "DQOBMXA", dqobmxa,
		    "Max num of pixels in one object", add+, verify-, show-)
	    if (skyline != "INDEF") {
		nhedit( skysub, skyadu_key, skyadu, "Mean sky (BUNIT)", add+, verify-, show-)
		nhedit (skysub, "SKYXGRAD", skyxgrad,
		    "Sky X gradient (BUNIT/pix)", add+, verify-, show-)
		nhedit (skysub, "SKYYGRAD", skyygrad,
		     "Sky Y gradient (BUNIT/pix)", add+, verify-, show-)
		if (isindef(skymode))
		    nhedit (skysub, "SKY", skyline,
			"Mean and StdDev of sky (BUNIT)", add+, verify-, show-)
		else {
		    nhedit (skysub, "SKY", skylinebg,
			"Mean and StdDev of sky (BUNIT)", add+, verify-, show-)
		    nhedit (skysub, skybg_key, skybg,
		        "Sky background (BUNIT)", add+, verify-, show-)
		    nhedit (skysub, dqskeadu_key, eskyadu,
		        "Error in sky background (BUNIT)", add+, verify-, show-)
		}
	    }

	    if (sigline != "INDEF") {
		nhedit (skysub, "SIG", sigline,
		    "Mean and StdDev of sky sigma (BUNIT)", add+, verify-, show-)
		nhedit( skysub, skynoise_key, skynoise,
		    "Mean sky noise (BUNIT)", add+, verify-, show-)
		nhedit (skysub, "SIGXGRAD", sigxgrad,
		    "Sky sigma X gradient (BUNIT/pix)", add+, verify-, show-)
		nhedit (skysub, "SIGYGRAD", sigygrad,
		    "Sky sigma Y gradient (BUNIT/pix)", add+, verify-, show-)
	    }
	}
	if (imaccess(obmextn)) {
	    if (imaccess(sky))
		nhedit (obmextn, "SKYIM", sky, "Sky map", add+, verify-, show-)
	    if (imaccess(sig))
		nhedit (obmextn, "SIGIM", sig, "Sigma map", add+, verify-, show-)
	    if (isindef(seeingp) == NO)
		nhedit (obmextn, seeingp_key, seeingp, "[pix] seeing", add+, verify-, show-)
	    if (isindef(dpthadu) == NO)
		nhedit (obmextn, dpthadu_key, dpthadu,
		    "Photometric depth (BUNIT)", add+, verify-, show-)
	    if (isindef(dqskfrc) == NO)
		nhedit (obmextn, "DQSKFRC", dqskfrc,
		    "Frac of pixels which are sky", add+, verify-, show-)
	    if (isindef(dqobmxa) == NO)
		nhedit (obmextn, "DQOBMXA", dqobmxa,
		    "Max num of pixels in one object", add+, verify-, show-)
	    if (skyline != "INDEF") {
		nhedit( obmextn, skyadu_key, skyadu, "Mean sky (BUNIT)", add+, verify-, show-)
		nhedit (obmextn, "SKYXGRAD", skyxgrad,
		    "Sky X gradient (BUNIT/pix)", add+, verify-, show-)
		nhedit (obmextn, "SKYYGRAD", skyygrad,
		     "Sky Y gradient (BUNIT/pix)", add+, verify-, show-)
		if (isindef(skymode))
		    nhedit (obmextn, "SKY", skyline,
			"Mean and StdDev of sky (BUNIT)", add+, verify-, show-)
		else {
		    nhedit (obmextn, "SKY", skylinebg,
			"Mean and StdDev of sky (BUNIT)", add+, verify-, show-)
		    nhedit (obmextn, skybg_key, skybg,
		        "Sky background (BUNIT)", add+, verify-, show-)
		    nhedit (obmextn, dqskeadu_key, eskyadu,
		        "Error in sky background (BUNIT)", add+, verify-, show-)
		}
	    }

	    if (sigline != "INDEF") {
		nhedit (obmextn, "SIG", sigline,
		    "Mean and StdDev of sky sigma (BUNIT)", add+, verify-, show-)
		nhedit( obmextn, skynoise_key, skynoise,
		    "Mean sky noise (BUNIT)", add+, verify-, show-)
		nhedit (obmextn, "SIGXGRAD", sigxgrad,
		    "Sky sigma X gradient (BUNIT/pix)", add+, verify-, show-)
		nhedit (obmextn, "SIGYGRAD", sigygrad,
		    "Sky sigma Y gradient (BUNIT/pix)", add+, verify-, show-)
	    }
	}

	# Update catalog header.  This is used later for global values.
	if (access(cat)) {
	    if (isindef(seeingp) == NO)
		thedit (cat, seeingp_key, seeingp, show-)
	    if (isindef(dpthadu) == NO)
		thedit (cat, dpthadu_key, dpthadu, show-)
	    if (isindef(dqskfrc) == NO)
		thedit (cat, "DQSKFRC", dqskfrc, show-)
	    if (isindef(dqobmxa) == NO)
		thedit (cat, "DQOBMXA", dqobmxa, show-)
	    if (skyline != "INDEF") {
		thedit (cat, "SKY", skyline, show-)
		thedit( cat, skyadu_key, skyadu, show-)
		thedit (cat, "SKYXGRAD", skyxgrad, show-)
		thedit (cat, "SKYYGRAD", skyygrad, show-)
	    }
	    if (skylinebg != "INDEF") {
		thedit (cat, "SKY", skylinebg, show-)
		thedit( cat, skybg_key, skybg, show-)
		thedit (cat, dqskeadu_key, eskyadu, show-)
	    }
	    if (sigline != "INDEF") {
		thedit (cat, "SIG", sigline, show-)
		thedit( cat, skynoise_key, skynoise, show-)
		thedit (cat, "SIGXGRAD", sigxgrad, show-)
		thedit (cat, "SIGYGRAD", sigygrad, show-)
	    }
	}

	# Send DQ to PMASS.
	if (dm != "") {
	    if (isindef(seeingp) == NO) {
		printf ("%.2f\n", seeingp) | scan (sval1)
		setkeyval (dm, "objectimage", image, "dqseamp", sval1)
	    }
	    if (isindef(dqskfrc) == NO) {
		printf ("%.3f\n", dqskfrc) | scan (sval1)
		setkeyval (dm, "objectimage", image, "dqskfrc", sval1)
	    }
	    if (isindef(dqobmxa) == NO) {
		printf ("%d\n", dqobmxa) | scan (sval1)
		setkeyval (dm, "objectimage", image, "dqobmxa", sval1)
	    }
	    if (skyline != "INDEF") {
		stat = fscan (skyline, sval1, sval2)
		setkeyval (dm, "objectimage", image, "dqskval", sval1 )
		setkeyval (dm, "objectimage", image, "dqskeval", sval2 )
		if (!isindef(skymode)) {
		    setkeyval (dm, "objectimage", image, "dqskrval", sval1 )
		    setkeyval (dm, "objectimage", image, "dqskervl", sval2 )
		}
		printf("%.4g\n",skyxgrad) | scan( sval1 )
		setkeyval (dm, "objectimage", image, "dqskvxgr", sval1 )
		printf("%.4g\n",skyygrad) | scan( sval1 )
		setkeyval (dm, "objectimage", image, "dqskvygr", sval1 )
	    }
	    if (sigline != "INDEF") {
		stat = fscan (sigline, sval1, sval2)
		setkeyval (dm, "objectimage", image, "dqsksig", sval1 )
		setkeyval (dm, "objectimage", image, "dqskesig", sval2 )
		printf("%.4g\n",sigxgrad) | scan( sval1 )
		setkeyval (dm, "objectimage", image, "dqsksxgr", sval1 )
		printf("%.4g\n",sigygrad) | scan( sval1 )
		setkeyval (dm, "objectimage", image, "dqsksygr", sval1 )
	    }
	}

	# Set error flag.
	if (status != 1) {
	    dqrej = ""; hselect (image, "SIFERR", yes) | scan (dqrej)
	    dqrej = dqrej // "A"
	    nhedit (image, "SIFERR", dqrej, "DQ analysis error", add+, verify-, show+)
	}
end
