/*

  ringremoval.c

  How to compile with graphical output:
  g77 striperemoval.c -o striperemoval -Wall -lcfitsio -lm

  Description:

  HISTORY:
  02/21/10   Document created

*/

#include <fitsio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Definitions */
#define  CHANNELWIDTH 32
#define  DETWIDTH 2048
#define  NARROWWIDTH 4

/* declare global variables */
char programname[120];           /* name under which this program is run */
int verbose = 0;                 /* default level for verbose */

/* declare functions */
void flag(int num);
void error(char *message, int giveusage);
void usage( void );
void pop(char **elements, int num, int *max);
float sqr(float x);
void keyname( char *pre, char *name, char keyword[80] );
int savefits( float *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits );
int savefits_i( int *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits );
void getstats( float *array, int numitems, float *mean, float *sigma, int offset );
void getstatsfilter(float *array, int numitems, float low, float high, int niter, float *mean, float *sigma, int offset );
void heapsort( float *arr, unsigned int N );

/* main body of the program */
int main(int argc, char **argv) {

   /* general variables */
   char message[256];            /* message string */
   char *stop;                   /* string needed for string-to-float conversion */
   int i,j,k;                    /* counters */
   float mean,sigma;
   
   /* variables related to command line options */
   int direction = 1;            /* variables related to stripe direction */
   int sdef = 0;                 /* variables related to added sky */
   float sky = 0;
   int fdef = 0;                 /* variables related to filter application */
   int npass = 3;                /* default rejection parameters */
   float lo=3, hi=3;
   int pdef = 0;                 /* variables related to percentile clipping */
   float pclip;

   /* variables related to reading of fits files */
   int status = 0;               /* status must always be initialized = 0  */
   int bitpix;                   /* bytes per pixel */
   int naxis;                    /* number of axes in FITS file */
   long naxes[2] = {1,1};        /* lower limit of pixels to be read */

   /* variables related to input and output fits file */
   fitsfile *INFITS;             /* fits file pointers defined in fitsio.h */
   fitsfile *BPMFITS;            /* fits file pointers defined in fitsio.h */
   long nx,ny;                   /* variables related to image size */
   char fitsfilename[120];       /* fits file name */
   char outfilename[120];        /* fits file name */
   char bpmfilename[120];        /* bpm file name */
   float *data;                  /* array for fits data */
   int *bpmdata;                 /* array for bpm data */
   long ndat;                    /* number of data elements in array */

   /* variables related to the stripe removal */
   float *stripe_i;              /* array for current stripe */
   int *stripe_bpm,*stripe_gbpm; /* arrays for stripe BPMs */
   int istart,pos;               /* position in array */
   int nok;                      /* number of unmasked pixels in a ring */
   int nmasked;                  /* number of masked pixels in a group */
   int gf;                       /* area growth factor */
   float peak;                   /* peak in masked area */
   int window,nwindow,nlast,kstart; /* variables related to window fraction */

   strcpy( programname, argv[0] ); /* store program name in global variable */

   /* Process command line options */
   i = argc-1;
   while (i>0) { /* Make a first loop to find -v arguments to determine verbose level */
      if ( strcmp(argv[i],"-v") == 0 ) {
	 verbose++; /* increase verbosity */
	 pop(argv,i,&argc); /* remove from argument list */
      }
      i--;
   }
   if (verbose>=1) printf("Verbose level: %i (out of 4)\n", verbose);
   if (verbose>=2) printf("Processing command line options:\n");
   if (verbose>=2) printf("   Number of command line arguments: %i\n", argc);
   i = argc-1;
   while (i>=1) { /* begin loop over command line arguments */
      if ( strcmp(argv[i], "-d")==0 ) {
         if (i+1<argc) { /* Need one more argument for number of rings */
            direction = (int) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -d\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Stripe direction requested by user: %i\n", direction);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-d is missing its argument",0);
      }
      if ( strcmp(argv[i], "-f")==0) {
	if (verbose>=2) printf("   5 sigma filter will be applied to statistics\n");
	fdef = 1;
	pop(argv,i,&argc);
      }
      if ( strcmp(argv[i], "-h")==0) /* help is asked, so give it and exit */
	 usage();
      if ( strcmp(argv[i], "-hi")==0 ) {
         if (i+1<argc) { /* Need one more argument for high rejection threshold */
            hi = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -hi\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   High rejection threshold supplied by user: %f\n", hi);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-hi is missing its argument",0);
      }
      if ( strcmp(argv[i], "-lo")==0 ) {
         if (i+1<argc) { /* Need one more argument for low rejection threshold */
            lo = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -lo\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Low rejection threshold supplied by user: %f\n", lo);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-lo is missing its argument",0);
      }
      if ( strcmp(argv[i], "-nr")==0 ) {
         if (i+1<argc) { /* Need one more argument for number of rejection cycles */
            npass = (int) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -nr\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Number of rejection cycles requested by user: %i\n", npass);
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-nr is missing its argument",0);
      }
      if ( strcmp(argv[i], "-p")==0 ) {
         if (i+1<argc) { /* Need one more argument for sky level */
            pclip = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -p\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for percentile clipping supplied by user: %f\n", pclip);
            pdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-p is missing its argument",0);
      }
      if ( strcmp(argv[i], "-s")==0 ) {
         if (i+1<argc) { /* Need one more argument for sky level */
            sky = (float) strtod(argv[i+1], &stop);
            if ( stop==argv[i+1] ) { /* check for conversion error */
               sprintf(message, "%s is not a valid argument for -s\n", stop);
               error(message,0);
            }
            if (verbose>=2) printf("   Value for added sky supplied by user: %f\n", sky);
            sdef = 1;
            pop(argv,i,&argc);
            pop(argv,i,&argc);
         } else error("-s is missing its argument",0);
      }
      i--;
   } /* end loop over command line arguments */

   /* After the known options have been processed, one argument, the  
      input fits file, should remain. */
   /* List the remaining arguments */
   if (verbose>=2) printf("   Remaining command line arguments: %i ", argc-1);
   if ((verbose>=2)&&(argc>1)) {
      for(i=1;i<argc;i++) { printf("%s ",argv[i]); }
      printf("\n");
   }
   if (argc>3) { /* Check whether two arguments remain */
      error("too many or unknown command line arguments given",1);
   } else if (argc<3) error("too few file names given",1);
   /* Two command line arguments left, copy to strings */
   strcpy(fitsfilename, argv[1]);
   strcpy(bpmfilename, argv[2]);
   if (verbose>=2) {
      printf("   Input fits file: %s\n", fitsfilename );
      printf("   Input bpm file: %s\n", bpmfilename );
   }

   /* Check whether all the necessary information is available. 
      Provide defaults where not, or give an error message. */

   /* Open data fits file and obtain basic information */
   fits_open_file(&INFITS, fitsfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(INFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(INFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(INFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",fitsfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* set array dimensions and number of elements */
   nx = naxes[0];
   ny = naxes[1];
   ndat = nx*ny;
   if (verbose>=3) printf("nx: %li  ny: %li\n", nx, ny );
   /* Allocate memory for data array */
   data = (float *)calloc((unsigned) (ndat), sizeof(float));
   if (data == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(INFITS, TFLOAT, naxes, ndat, NULL, data, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Open BPM fits file and obtain basic information */
   fits_open_file(&BPMFITS, bpmfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(BPMFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(BPMFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(BPMFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",bpmfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* set array dimensions and number of elements */
   if ((naxes[0]!=nx)||(naxes[1]!=ny)) {
     error("Input fits and bpm images do not have the same size\n",0);
   }
   /* Allocate memory for data array */
   bpmdata = (int *)calloc((unsigned) (ndat), sizeof(int));
   if (bpmdata == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",0);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(BPMFITS, TINT, naxes, ndat, NULL, bpmdata, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Start of main code */

   /* Allocate space for the current stripe */
   stripe_i = (float *)calloc((unsigned) (nx), sizeof(float));
   stripe_bpm = (int *)calloc((unsigned) (nx), sizeof(int));
   stripe_gbpm = (int *)calloc((unsigned) (nx), sizeof(int));

   /* Loop over all stripes */
   for(j=0;j<nx;j++) {

     /* Extract data and mask for the current stripe */
     for(i=0;i<nx;i++) {
       if (direction==1) { /* rows */
	 pos = j*nx+i;
       } else { /* columns */
	 pos = i*nx+j;
       }
       stripe_i[i] = data[pos];
       stripe_bpm[i] = bpmdata[pos];
       stripe_gbpm[i] = bpmdata[pos];
     }

     /* If the density of masked area is high, mask the entire area */
     window = 60;
     nlast = nwindow = 0;
     for(i=0;i<window;i++) {
       if (stripe_bpm[i]>0) {
	 nwindow++;
       }
     }
     for(i=window;i<nx;i++) {
       if (nwindow>50) {
	 if (nlast>i-window) {
	   kstart = nlast;
	 } else {
	   kstart = i-window;
	 }
	 for(k=kstart;k<=i;k++) {
	   stripe_gbpm[k] = 1;
	 }
	 nlast = k;
       }
       nwindow += stripe_bpm[i]-stripe_bpm[i-window];
     }

     /* Grow large areas in the mask */
     nmasked = 0;
     peak = 0;
     istart = 0;
     for(i=0;i<nx;i++) {
       if (stripe_bpm[i]>0) {
	 nmasked++;
	 if (stripe_i[i]>peak) {
	   peak = stripe_i[i];
	 }
       } else {
	 gf = 0;
	 if ((nmasked>5)&&(nmasked<=75)) {
	   gf = 10+(nmasked-5)/5;
	 }
	 if (nmasked>75) {
	   if ( peak < 3 ) { peak = 3; }
	   gf = 4*(i-istart)/(2*(int)(log(peak)));
	 }
	 if (gf>0) {
	   for(k=istart-gf-1;k<=istart;k++) {
	     stripe_gbpm[k] = 1;
	   }
	   for(k=i-1;k<=i+gf+1;k++) {
	     stripe_gbpm[k] = 1;
	   }
	 }
	 nmasked = 0;
	 peak = 0;
	 istart = i;
       }
     }

     nok = 0;
     for(i=0;i<nx;i++) {
       if (stripe_gbpm[i]==0) {
	 stripe_i[nok] = stripe_i[i];
	 nok++;
       }
     }

     if (nok==0) {
       for(i=0;i<nx;i++) {
	 if (direction==1) { /* rows */
	   pos = j*nx+i;
	 } else { /* columns */
	   pos = i*nx+j;
	 }
	 data[pos] = sky;
       }
     } else {
       
       if (pdef) {
	 /* Sort the array */
	 heapsort( stripe_i, nok );
	 /* Determine start and end points of data included within 
	    the percentile clipping range */ 
	 i = (int)((1-pclip)*nok/2);
	 k = (int)((1+pclip)*nok/2);
	 /* Calculate clipping range */
	 k = k-i+1;
	 /* Determine the statistics in the pclipped range */
	 getstats( &stripe_i[i], k, &mean, &sigma, 0 );
       } else {
	 /* Determine statistics within the ring */
	 getstats( stripe_i, nok, &mean, &sigma, 0 );
	 if (fdef&&(verbose>=3)) printf("Stats before\t%f %f\t", mean, sigma );
	 /* Include one 5 sigma rejection pass if needed */
	 if (fdef) {
	   getstatsfilter( stripe_i, nok, lo, hi, npass, &mean, &sigma, 0 );
	   if (verbose>=3) printf("after\t%f %f\n", mean, sigma );
	 }
       }

       /* Subtract the mean from the data, add in sky level */
       for(i=0;i<nx;i++) {
	 if (direction==1) { /* rows */
	   pos = j*nx+i;
	 } else { /* columns */
	   pos = i*nx+j;
	 }
	 data[pos] -= mean-sky;
       }
     }
   }

   i = strlen(fitsfilename)-5;
   strncpy( outfilename, fitsfilename, i );
   outfilename[i] = '\0';
   strcat( outfilename, "_out.fits" );
   if (verbose>=1) printf( "Creating outfile: %s\n", outfilename );
   savefits( data, nx, ny, INFITS, outfilename );

   /* Close the fits file */
   fits_close_file( INFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   free( (float *)data );
   free( (float *)stripe_i );

   exit(1);

} /* end main */

/* Start of function definitions */

void flag(int num) {
  printf ( "Flag %i\n", num );
}

void error(char *message, int giveusage) {
   printf("ERROR: %s\n", message);
   if (giveusage) usage();
   else exit(0);
}

void usage( void ) {
   printf("Usage: %s [options] <data file> <template file> <continuum file>\n", programname);
   printf("    [-h] (provide this help)\n");
   printf("    [-v (increase verbosity by 1)]\n");
   exit(0);
}

void pop(char **elements, int num, int *max) {
   int i;
   
   if (num<*max) {
      for(i=num+1;i<*max;i++) {
         elements[i-1] = elements[i];
      }
   }
   *max = *max-1;
}

float sqr(float x){
   return x*x;
} 

void keyname( char *pre, char *name, char keyword[80] ) {
  strcpy(keyword,"dq");
  strcat(keyword,pre);
  strcat(keyword,name);
}

int savefits( float *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits ) {
   int status=0;
   fitsfile *OUTFITS;    /* FITS file pointers defined in fitsio.h */
   char overwriteout[125];

   /* Make output file overwritable by prefixing a ! */
   strcpy( overwriteout, "!" );
   strcat( overwriteout, outfits );

   fits_create_file(&OUTFITS, overwriteout, &status); /* Open the output file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_copy_hdu(ORIGFITS, OUTFITS, 0, &status); /* Copy the original header */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   /* Write the fits file to disk */
   status = fits_write_img(OUTFITS, TFLOAT, 1, nx*ny, data, &status);
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_close_file( OUTFITS, &status ); /* Close the fits file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   return(1);
}

int savefits_i( int *data, long nx, long ny, fitsfile *ORIGFITS, char *outfits ) {
   int status=0;
   fitsfile *OUTFITS;    /* FITS file pointers defined in fitsio.h */
   char overwriteout[125];

   /* Make output file overwritable by prefixing a ! */
   strcpy( overwriteout, "!" );
   strcat( overwriteout, outfits );

   fits_create_file(&OUTFITS, overwriteout, &status); /* Open the output file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_copy_hdu(ORIGFITS, OUTFITS, 0, &status); /* Copy the original header */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   /* Write the fits file to disk */
   status = fits_write_img(OUTFITS, TINT, 1, nx*ny, data, &status);
   if (status>0) printf( "FITSIO error number: %i\n", status );
   fits_close_file( OUTFITS, &status ); /* Close the fits file */
   if (status>0) printf( "FITSIO error number: %i\n", status );
   return(1);
}

void getstats( float *array, int numitems, float *mean, float *sigma, int offset ) {
  int i;
  double _mean=0,_sigma=0;

  for(i=offset;i<numitems+offset;i++) {
    _mean += array[i];
    _sigma += array[i]*array[i];
  }
  if (numitems>1)
    _sigma = sqrt( (_sigma-_mean*_mean/numitems)/(numitems-1) );
  else
    _sigma = 0;
  _mean /= (float)numitems;
  
  *mean = (float)_mean;
  *sigma = (float)_sigma;
}

void getstatsfilter(float *array, int numitems, float low, float high, int niter, float *mean, float *sigma, int offset ) {
  int i,j,count,prevcount;
  double _mean,_sigma;
  float *data;

  data = (float *)calloc((unsigned) numitems, sizeof(float));
  for(i=0;i<numitems;i++) data[i] = array[i+offset];

  count = numitems;
  _mean = (double)(*mean);
  _sigma = (double)(*sigma);
  if (numitems>1) {
    for(i=1;i<=niter;i++) {
      prevcount = count;
      count = 0;
      for(j=0;j<prevcount;j++) {
	if ((data[j]>=_mean-low*_sigma)&&(data[j]<=_mean+high*_sigma)) {
	  data[count] = data[j];
	  count++;
	}
      }
      _mean = _sigma = 0;
      for(j=0;j<count;j++) {
	_mean += (double)data[j];
	_sigma += (double)(data[j]*data[j]);
      }
      _sigma = sqrt( (_sigma-_mean*_mean/count)/(count-1) );
      _mean /= count;
    }
  }
  *mean = (float)_mean;
  *sigma = (float)_sigma;
  free( (float *)data );
}

void heapsort( float *arr, unsigned int N ) {
  unsigned int n = N, i = n/2, parent, child;
  float t;
 
  for (;;) { /* Loops until arr is sorted */
    if (i > 0) { /* First stage - Sorting the heap */
      i--;           /* Save its index to i */
      t = arr[i];    /* Save parent value to t */
    } else {     /* Second stage - Extracting elements in-place */
      n--;           /* Make the new heap smaller */
      if (n == 0) return; /* When the heap is empty, we are done */
      t = arr[n];    /* Save last value (it will be overwritten) */
      arr[n] = arr[0]; /* Save largest value at the end of arr */
    }
 
    parent = i; /* We will start pushing down t from parent */
    child = i*2 + 1; /* parent's left child */
 
    /* Sift operation - pushing the value of t down the heap */
    while (child < n) {
      if (child + 1 < n  &&  arr[child + 1] > arr[child]) {
	child++; /* Choose the largest child */
      }
      if (arr[child] > t) { /* If any child is bigger than the parent */
	arr[parent] = arr[child]; /* Move the largest child up */
	parent = child; /* Move parent pointer to this child */
	child = parent*2 + 1; /* Find the next child */
      } else {
	break; /* t's place is found */
      }
    }
    arr[parent] = t; /* We save t in the heap */
  }
}
