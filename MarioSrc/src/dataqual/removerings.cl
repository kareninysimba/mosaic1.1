procedure removerings(infile)

file infile		 {prompt="Image to subtract rings from"}
file mask = ""		 {prompt="bad pixel/source mask"}
real xcenter = 0         {prompt="x position of ring center"}
real ycenter = 0         {prompt="y position of ring center"}
int nrings = 100         {prompt="Number of rings to use"}
int inner = 10           {prompt="Number of inner rings to average over"}
real skymode = 0         {prompt="Sky level to add back into image"}
real low = 3             {prompt="Lower side clipping factor in sigma"}
real high = 3            {prompt="Upper side clipping factor in sigma"}
real niter = 0           {prompt="Number of clipping iterations"}
real pclip = -1          {prompt="Percentile clipping fraction"}

begin

    bool f
    int n,in,nit
    real s,x,y,lo,hi,p
    string ifile,mfile,options

    task $_removerings = "$ringremoval -v -v -x $3 -y $4 -n $5 -s $6 $7 $(1).fits $(2)"

    #Get parameters
    ifile = infile
    mfile = mask
    x = xcenter
    y = ycenter
    n = nrings
    in = inner 
    s = skymode
    nit = niter
    lo = low
    hi = high
    p = pclip

    # Check whether the ifile ends in .fits already, and if so, drop
    # the extension
    if (strlstr(".fits",ifile)>0) {
        s1 = substr (ifile, 1, strlstr(".fits",ifile)-1)
    } else {
        s1 = ifile
    }

    # Check whether the mfile ends in .pl already, and if so, drop
    # the extension
    if (strlstr(".pl",mfile)>0) {
        s2 = substr (mfile, 1, strlstr(".pl",mfile)-1)
    } else {
        s2 = mfile
    }

    # Create temporary file name
    s2 = "tmp"//s2//".fits"
    # Convert the input mask to fits
    imcopy( mfile, s2 )

    # Set the filter flag if needed
    if ( nit>0 ) {
	options = "-f"
    } else {
	options = ""
    }
    # Add other options
    options = options // " -hi " // hi
    options = options // " -lo " // lo
    options = options // " -nr " // nit
    options = options // " -i " // in

    # Add pclip option if valid
    if ( (pclip>0) && (pclip<=1) ) {
	options = options // " -p " // p
    }
    ;

    # Perform ring removal
    _removerings( s1, s2, x, y, n, s, options )

    # Write output over original file
    imdel( s1 )
    imrename( s1//"_out", s1 )
    
    # Clean up
    imdel( s2 )

end
