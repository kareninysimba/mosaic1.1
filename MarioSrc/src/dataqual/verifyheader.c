/*

  biasstruct.c

  How to compile with graphical output:
  g77 verifyheader.c -o verifyheader -Wall -lcfitsio -lm


  Description:

  HISTORY:
  1/10/05    Document created
  1/11/05    Version 1.0 ready

*/

#include <fitsio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* declare global variables */
char programname[120];           /* name under which this program is run */
int verbose = 0;                 /* default level for verbose */

/* Structure for header information */
typedef struct {
  char name[9];
  char content[73];
} header;

/* declare functions */
void flag(int num);
void error(char *message, int errorcode);
void usage( void );
void pop(char **elements, int num, int *max);
float sqr(float x);

/* main body of the program */
int main(int argc, char **argv) {

   /* general variable */
   char buffer[80];              /* character buffer */
   char message[256];            /* message string */
   int i,j;                      /* counters */

   /* variables related to reading of fits files */
   int status = 0;               /* status must always be initialized = 0  */
   int bitpix;                   /* bytes per pixel */
   int naxis;                    /* number of axes in FITS file */
   long naxes[2] = {1,1};        /* lower limit of pixels to be read */

   /* variables related to the template fits file */
   fitsfile *TEMPLATE;           /* fits file pointers defined in fitsio.h */
   long tnx,tny;                 /* variables related to template image size */
   char templatename[512];       /* template fits file name */

   /* variables related to target fits files */
   fitsfile *INFITS;             /* fits file pointers defined in fitsio.h */
   long nx,ny;                   /* variables related to image size */
   char fitsfilename[512];       /* fits file name */
   float *data;                  /* array for overscan data */
   long ndat;                    /* number of data elements in array */

   /* variables related to reading and checking of header keywords */
   int tnumkeys,numkeys;         /* number of keywords */
   int morekeys;                 /* closure of header */
   header *tkey,*key;            /* names and content of template and target keywords */
   int check;                    /* result of checks */

   strcpy( programname, argv[0] ); /* store program name in global variable */

   /* Process command line options */
   i = argc-1;
   while (i>0) { /* Make a first loop to find -v arguments to determine verbose level */
      if ( strcmp(argv[i],"-v") == 0 ) {
	 verbose++; /* increase verbosity */
	 pop(argv,i,&argc); /* remove from argument list */
      }
      i--;
   }
   if (verbose>=1) printf("Verbose level: %i (out of 4)\n", verbose);
   if (verbose>=3) printf("Processing command line options:\n");
   if (verbose>=3) printf("   Number of command line arguments: %i\n", argc);
   i = argc-1;
   while (i>=1) { /* begin loop over command line arguments */
      if ( strcmp(argv[i], "-h")==0) /* help is asked, so give it and exit */
	 usage();
      i--;
   } /* end loop over command line arguments */

   /* After the known options have been processed, two argument, the  
      template and the input fits file, should remain. */
   /* List the remaining arguments */
   if (verbose>=3) printf("   Remaining command line arguments: %i ", argc-1);
   if ((verbose>=3)&&(argc>1)) {
      for(i=1;i<argc;i++) { printf("%s ",argv[i]); }
      printf("\n");
   }
   if (argc>3) { /* Check whether two arguments remains */
      error("too many or unknown command line arguments given",1);
   } else if (argc<3) error("input file(s) missing",1);
   /* One command line argument left, copy to string */
   strcpy(templatename, argv[1]);
   strcpy(fitsfilename, argv[2]);
   if (verbose>=3) {
      printf("   Template fits file: %s\n", templatename );
      printf("   Input fits file: %s\n", fitsfilename );
   }

   /* Open data template fits file and obtain basic information */
   fits_open_file(&TEMPLATE, templatename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(TEMPLATE, &bitpix, &status); /* get data type */
   fits_get_img_dim(TEMPLATE, &naxis, &status); /* read dimensions */
   fits_get_img_size(TEMPLATE, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   tnx = naxes[0] ; tny = naxes[1]; /* store image size */
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",templatename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }

   /* Open data target fits file and obtain basic information */
   fits_open_file(&INFITS, fitsfilename, READWRITE, &status); /* Open the input file */
   fits_get_img_type(INFITS, &bitpix, &status); /* get data type */
   fits_get_img_dim(INFITS, &naxis, &status); /* read dimensions */
   fits_get_img_size(INFITS, 2, naxes, &status); /* get image size */
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   nx = naxes[0] ; ny = naxes[1]; /* store image size */
   /* Report basic fits file information to user */
   if (verbose>=2) {
     printf("Fits file: %s\nDimensions: %li x %li pixels\n",fitsfilename,naxes[0],naxes[1]);
     printf("Bytes per pixel: %i\n", bitpix);
   }
   /* Check whether sizes are the same */
   if ((nx!=tnx)||(ny!=tny)) {
     error( "images not the same size", -10 );
   }

   /* Get number of header keywords in the template and target */
   fits_get_hdrspace(TEMPLATE, &tnumkeys, &morekeys, &status);
   fits_get_hdrspace(INFITS, &numkeys, &morekeys, &status);
   /* If an error occured, print error message */
   if (status) { 
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=1) {
     printf( "Template has %i keywords\n", tnumkeys );
     printf( "Target has %i keywords\n", numkeys );
   }
   /* target must have at least as many keywords as template */
   if (tnumkeys>numkeys) {
     error( "target has too few keywords", -20 );
   }

   /* Allocate memory for keyword arrays */
   tkey = (header *)calloc((unsigned) (tnumkeys), sizeof(header) );
   key = (header *)calloc((unsigned) (numkeys), sizeof(header) );
   /* Read all keywords from template and store contents into an array */
   for(i=0;i<tnumkeys;i++) {
     fits_read_keyn(TEMPLATE, i+1, tkey[i].name, tkey[i].content, buffer, &status );
   }
   /* Read all keywords from target and store contents into an array */
   for(i=0;i<numkeys;i++) {
     fits_read_keyn(INFITS, i+1, key[i].name, key[i].content, buffer, &status );
   }
   
   /* Check whether all keywords from template exist in target */
   for(i=0;i<tnumkeys;i++) {
     check = 0;
     for(j=0;j<numkeys-i;j++) {
       if (strcmp(tkey[i].name,key[j].name)==0) { /* strings are identical */
	 check = 1;
	 strcpy(key[j].name,key[numkeys-i-1].name); /* copy last entry to present location */
	 j = numkeys; /* end loop */
       }
     }
     if (!check) {
       sprintf( message, "keyword %s not in target header", tkey[i].name );
       error( message, -21 );
     }
   }

   /* set array dimensions and number of elements */
   nx = naxes[0];
   ny = naxes[1];
   ndat = nx*ny;
   if (verbose>=3) printf("nx: %li  ny: %li\n", nx, ny );
   /* Allocate memory for data array */
   data = (float *)calloc((unsigned) (ndat), sizeof(float));
   if (data == NULL) { /* Check for memory allocation error */
      error("memory could not be allocated for data\n",-1);
   }
   /* Read the fits file into an array */
   naxes[0] = naxes[1] = 1;
   fits_read_pix(INFITS, TFLOAT, naxes, ndat, NULL, data, NULL, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }
   if (verbose>=2)
     printf("Number of pixels read: %li\n", ndat );

   /* Close the fits files */
   fits_close_file( TEMPLATE, &status );
   fits_close_file( INFITS, &status );
   if (status) { /* if error occured, print error message and exit */
      fits_report_error(stderr, status);
      return(status);
   }

   free( (float *)data );

   exit(1);

} /* end main */

/* Start of function definitions */

void flag(int num) {
  printf ( "Flag %i\n", num );
}

void error(char *message, int errorcode) {
   printf("%i ERROR: %s\n", errorcode, message);
   if (errorcode>0) usage();
   else exit(errorcode);
}

void usage( void ) {
   printf("Usage: %s [options] <template file> <target file>\n", programname);
   printf("    [-h] (provide this help)\n");
   printf("    [-v (increase verbosity by 1)]\n");
   exit(0);
}

void pop(char **elements, int num, int *max) {
   int i;
   
   if (num<*max) {
      for(i=num+1;i<*max;i++) {
         elements[i-1] = elements[i];
      }
   }
   *max = *max-1;
}

float sqr(float x){
   return x*x;
} 
