procedure removestripes(infile)

file infile		 {prompt="Image to subtract stripes from"}
file mask = ""		 {prompt="bad pixel/source mask"}
int axis = 1             {prompt="Axis to be fit"}
real skymode = 0         {prompt="Sky level to add back into image"}
real low = 3             {prompt="Lower side clipping factor in sigma"}
real high = 3            {prompt="Upper side clipping factor in sigma"}
real niter = 0           {prompt="Number of clipping iterations"}
real pclip = -1          {prompt="Percentile clipping fraction"}

begin

    bool f
    int a,n,nit
    real s,lo,hi,p
    string ifile,mfile,options

    task $_removestripes = "$striperemoval -v -v -s $3 $4 $(1).fits $(2)"

    #Get parameters
    ifile = infile
    mfile = mask
    a = axis
    s = skymode
    nit = niter
    lo = low
    hi = high
    p = pclip
print "A1"

    # Check whether the ifile ends in .fits already, and if so, drop
    # the extension
    if (strlstr(".fits",ifile)>0) {
        s1 = substr (ifile, 1, strlstr(".fits",ifile)-1)
    } else {
        s1 = ifile
    }
print "A2"

    # Check whether the mfile ends in .pl already, and if so, drop
    # the extension
    if (strlstr(".pl",mfile)>0) {
        s2 = substr (mfile, 1, strlstr(".pl",mfile)-1)
    } else {
        s2 = mfile
    }
print "A3"

    # Create temporary file name
    s2 = "tmp"//s2//".fits"
    # Convert the input mask to fits
    imcopy( mfile//"[pl]", s2 )
print "A4"

    # Set the filter flag if needed
    if ( nit>0 ) {
	options = "-f"
    } else {
	options = ""
    }
    # Add other options
    options = options // " -hi " // hi
    options = options // " -lo " // lo
    options = options // " -nr " // nit
    options = options // " -d " // a
print( options )

    # Add pclip option if valid
    if ( (pclip>0) && (pclip<=1) ) {
	options = options // " -p " // pclip
    }
    ;
print( options )

    # Perform ring removal
print "A5"
    _removestripes( s1, s2, s, options )
print "A6"

    # Write output over original file
    imdel( s1 )
    imrename( s1//"_out", s1 )
    
    # Clean up
    imdel( s2 )

end
