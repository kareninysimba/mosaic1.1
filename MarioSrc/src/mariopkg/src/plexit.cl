# PLEXIT -- Logout Interface Procedure
#
## PLEXIT
## """"""
## module exit interface
## '''''''''''''''''''''
## 
## :Manual group:   mariopkg 
## :Manual section: mario
## 
## USAGE
## =====
## 
## | cl> plexit [parameter=value ...]
## 
## PARAMETERS
## ==========
## 
## identifier = 'PLEXIT:'
##   Single word identifying the line as being an exit line.
## 
## code = 1
##   Exit status code for the exit line AND the IRAF exit value.
## 
## name = 'SUCCESS'
##   Single word mnemonic equivalent to the exit code.
## 
## class = 'SUCCESS'
##   Single word providing a class for the exit.  The suggested classes
##   are SUCCESS, ERROR, WARN, FATAL, and OTHER.  Note that FATAL is an
##   error that is unrecoverable while ERROR is an exit that might be
##   trapped.
## 
## description = 'Success'
##   Any desired description about the exit status.
## 
## arg = ''
##   Optional argument which is appended to the description enclosed
##   in parenthesis.
## 
## exitstring = ''
##   If a string with an identifier and code is supplied this is passed
##   on as the output and code is used for the IRAF exit value.  This
##   parameter allows one task to capture the output of this task as a
##   string and then reinstansiate the exit.
## 
## output = 'STDOUT'
##   The output file to which the exit line will be appended.  The special
##   name 'STDOUT' or the null string '' may be used to write to the
##   standard output (which may be redirected at a higher level by the
##   calling procedure).
## 
## 
## DESCRIPTION
## ===========
## 
## The task provides a standardized exit or logout for IRAF pipeline modules.
## The consists of writing an exit line to a specified file followed by the
## IRAF logout with the specified exit status value.  This provides
## extended information to external tools beyond an exit status value.
## 
## The task is written in a configurable way so that the output information may
## be to written to any file, including the standard output, and the exit
## line to be extracted based on any identifier.
## 
## The exit line consists of five space separated fields.  While
## nothing prevents use of strings with whitespace for the string fields,
## calling procedures should use only single words except for the
## final description.  Only the description is quoted on the output.
## 
## The five fields are specified by task parameters which are then
## used in the calling arguments in parameter=value form.  The parameters,
## with their default values, are described in the PARAMETERS section.
## 
## As a alternate output the parameter ``exitstring`` can be used to supply
## the desired output.  The only requirement is that the second word is the
## integer exit code which must be parsed by the task to turn it into the
## IRAF logout status.  This is used when the output of a separate (stand-alone)
## command using ``plexit`` is captured by another IRAF session and is then
## passed on as the exit for that session.
## 
## EXAMPLES
## ========
## 
## | cl> plexit code=2 name=NODATA class=WARN description="No data"
## | PLEXIT: 2 NODATA WARN "No data"
## | csh> echo $status
## | 2


procedure plexit ()

string	identifier = "PLEXIT:"		{prompt="Exit identifier"}
int	code = 1			{prompt="Exit status code"}
string	name = "SUCCESS"		{prompt="Exit status name"}
string	class = "SUCCESS"		{prompt="Exit status class"}
string	description = "Success"		{prompt="Description string"}
string	arg = ""			{prompt="Description argument"}
string	exitstring = ""			{prompt="Exit string"}
file	output = "STDOUT"		{prompt="Output file"}

begin
	int	c
	string	ag, dummy

	ag = arg
	if (ag != "")
	   ag = " (" // arg // ")"

	c = code
	if (output == "" || output == "STDOUT") {
	    if (fscan (exitstring, dummy, c) == 2)
	        print (exitstring)
	    else
		printf ('%s %d %s %s "%s%s"\n',
		    identifier, code, name, class, description, ag)
	} else {
	    if (fscan (exitstring, dummy, c) == 2)
	        print (exitstring, >> output)
	    else
		printf ('%s %d %s %s "%s%s"\n',
		    identifier, code, name, class, description, ag, >> output)
	}
	logout (c)
end
