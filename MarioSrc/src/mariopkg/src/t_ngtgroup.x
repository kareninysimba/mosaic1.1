include	<error.h>

# Arrays.
define	SZ_OBSID	32
define	SZ_FILTER	68
define	SZ_CCDSUM	8

define	INDEX	Memi[index+$1]
define	IMAGE	Memc[image+SZ_PATHNAME*$1]
define	MJD	Memd[mjd+$1]
define	LJD	Memi[ljd+$1]
define	EXPTIME	Memd[exptime+$1]
define	OBSID	Memc[obsid+SZ_OBSID*$1]
define	OBSTYPE	Memi[obstype+$1]
define	FILTER	Memc[filter+SZ_FILTER*$1]
define	CCDSUM	Memc[ccdsum+SZ_CCDSUM*$1]
define	NEXTEND	Memi[nextend+$1]

# Observation types recognized.
define	OBSTYPES	"|zero|dark|dome flat|sky flat|object|"
define	UNKNOWN		0
define	ZERO		1
define	DARK		2
define	FLAT		3
define	SFLAT		4
define	OBJECT		5


# T_NGTGROUP -- Group input list of exposures.
#
# This is specialized for NOAO Mosaic keywords.
#
# The input template is expanded and sorted by MJD-OBS.  The image names are
# stripped of any kernel or image sections to allow categorization of MEF
# files using one extension.  They are expanded to a full path including
# the node so that other nodes may access the files.
#
# Each exposure is recorded in a file with name based on the OBSID.
# Any periods in the OBSID have to be removed because of an OPUS bug.
# All further references to an exposure are through this file.
#
# A calibration sequence consists of successive exposures in this list which
# have the same OBSTYPE and FILTER and differ in MJD-OBS by less than "dtmax".
# Sequences are recorded in file with the OBSID of the first exposure and
# postfixed with Z, F, or T.
#
# The returned calibration and object lists are lists of sequence or OBSID
# reference files.
#
# Exposure names with "test" are ignored.
# OBSTYPEs of "sky flat" are treated as objects.
# OBSTYPEs of "focus" are ignored.
#
# Any problem exposures are written to a list.  The problems include
# non-existent input, error opening the first extension, missing keywords,
# test exposure, and unknown OBSTYPE.


procedure t_ngtgroup ()

pointer	inlist			# Input list of exposures
pointer	zfname			# Output file for list of zeros
pointer	ffname			# Output file for list of flats
pointer	ofname			# Output file for list of objects
pointer	ifname			# Output file for list of ignored exposures
double	dtmax			# Max interval for exposures in sequence
int	skyflat			# Type to map sky flat
bool	verbose			# Verbose?

int	i, j, k, nim
int	fd, zfd, ffd, ofd, sfd
bool	bval
double	dt
pointer	sp, pat, fname, fname1, sfname, im, obs, tmp
pointer	index, image, mjd, ljd, exptime, obsid, obstype, filter, ccdsum, nextend

bool	clgetb(), streq(), strne()
int	imtgetim(), imtlen(), patmake(), patmatch(), strdic(), imgeti()
double	clgetd(), imgetd(), obsgetd()
pointer	imtopenp(), immap()
errchk	immap, imgetd, imgstr, obsimopen

int	mjdcompare()
extern	mjdcompare

begin
	# Get input list and determine number of images.
	inlist = imtopenp ("input")
	nim = imtlen (inlist)
	if (nim < 1)
	    return

	# Allocate memory.
	call smark (sp)
	call salloc (zfname, SZ_FNAME, TY_CHAR)
	call salloc (ffname, SZ_FNAME, TY_CHAR)
	call salloc (ofname, SZ_FNAME, TY_CHAR)
	call salloc (ifname, SZ_FNAME, TY_CHAR)
	call salloc (index, nim, TY_INT)
	call salloc (image, nim*SZ_PATHNAME, TY_CHAR)
	call salloc (mjd, nim, TY_DOUBLE)
	call salloc (ljd, nim, TY_INT)
	call salloc (exptime, nim, TY_DOUBLE)
	call salloc (obsid, nim*SZ_OBSID, TY_CHAR)
	call salloc (obstype, nim, TY_INT)
	call salloc (filter, nim*SZ_FILTER, TY_CHAR)
	call salloc (ccdsum, nim*SZ_CCDSUM, TY_CHAR)
	call salloc (nextend, nim, TY_INT)
	call salloc (pat, SZ_LINE, TY_CHAR)
	call salloc (fname, SZ_PATHNAME, TY_CHAR)
	call salloc (fname1, SZ_PATHNAME, TY_CHAR)
	call salloc (sfname, SZ_PATHNAME, TY_CHAR)

	# Get other input parameters.
	call clgstr ("zerolist", Memc[zfname], SZ_FNAME)
	call clgstr ("flatlist", Memc[ffname], SZ_FNAME)
	call clgstr ("objlist", Memc[ofname], SZ_FNAME)
	call clgstr ("ignore", Memc[ifname], SZ_FNAME)
	dtmax = clgetd ("dtmax")
	call clgstr ("skyflat", Memc[fname], SZ_FNAME)
	if (streq (Memc[fname], "flat"))
	    skyflat = SFLAT
	else if (streq (Memc[fname], "object"))
	    skyflat = OBJECT
	else
	    skyflat = UNKNOWN
	verbose = clgetb ("verbose")

	# Make pattern to detect test exposures.
	j = patmake ("test[^/]*$", Memc[pat], SZ_LINE)

	# Loop through input and extract required information.
	# Skip test exposures and unknown observation types.
	# Map sky flat to object.

	i = 0; fd = NULL
	while (imtgetim (inlist, IMAGE(i), SZ_PATHNAME) != EOF) {
	    iferr {
		call imgcluster (IMAGE(i), IMAGE(i), SZ_PATHNAME)
		call fpathname (IMAGE(i), IMAGE(i), SZ_PATHNAME-1)

		call sprintf (Memc[fname], SZ_PATHNAME, "%s[1]")
		    call pargstr (IMAGE(i))
		tmp = immap (Memc[fname], READ_ONLY, 0); im = tmp

		# Skip test exposures.
		if (patmatch (IMAGE(i), Memc[pat]) > 0)
		    call error (1, "test")

		INDEX(i) = i
		MJD(i) = imgetd (im, "mjd-obs")
		EXPTIME(i) = imgetd (im, "exptime")
		call imgstr (im, "obsid", OBSID(i), SZ_OBSID-1)
		call imgstr (im, "filter", FILTER(i), SZ_FILTER-1)
		call imgstr (im, "ccdsum", CCDSUM(i), SZ_CCDSUM-1)
		NEXTEND(i) = imgeti (im, "nextend")
		call imgstr (im, "obstype", Memc[fname], SZ_LINE)
		OBSTYPE(i) = strdic (Memc[fname], Memc[fname], SZ_PATHNAME,
		    OBSTYPES)
		if (OBSTYPE(i) == SFLAT)
		    OBSTYPE(i) = skyflat
		if (OBSTYPE(i) == 0)
		    call error (2, Memc[fname])

		# Get timezone from OBSERVAT keyword and observatory database.
		call obsimopen (obs, im, "", NO, bval, bval)
		if (obs != NULL) {
		    dt = obsgetd (obs, "timezone")
		    LJD(i) = int (MJD(i) + 0.5 - dt/24.)
		    call obsclose (obs)
		} else
		    LJD(i) = int (MJD(i) + 0.5)

		# Remove periods from OBSID.
		call strcpy (OBSID(i), Memc[fname], SZ_PATHNAME)
		j = fname
		for (k=j; Memc[k]!=EOS; k=k+1) {
		    if (Memc[k] != '.') {
		        Memc[j] = Memc[k]
			j = j + 1
		    }
		}
		Memc[j] = EOS
		
		# Format final OBSID string.
		call strcpy (Memc[fname], OBSID(i), SZ_PATHNAME) 
		#call strcpy (OBSID(i), Memc[fname1], 4) 
		#call sprintf (OBSID(i), SZ_PATHNAME, "%s%05d_%s")
		#    call pargstr (Memc[fname1])
		#    call pargi (LJD(i))
		#    call pargstr (Memc[fname+4])

		i = i + 1
	    } then {
		call eprintf ("%s:\n")
		    call pargstr (IMAGE(i))
		call erract (EA_WARN)
	        call ngtwrite (fd, Memc[ifname], IMAGE(i), APPEND, verbose)
	    }

	    if (im != NULL)
	        call imunmap (im)
	}

	# We're done with the input list.
	call imtclose (inlist)
	if (fd != NULL)
	    call close (fd)

	# Return if there are no exposures.
	nim = i
	if (nim == 0) {
	    call sfree (sp)
	    return
	}

	# Sort by MJD to detect observing sequences.
	call gqsort (INDEX(0), nim, mjdcompare, mjd)

	# Initialize output.
	fd = NULL; zfd = NULL; ffd = NULL; ofd = NULL; sfd = NULL

	# Start the first sequence.
	i = INDEX(0)
	call fpathname (OBSID(i), Memc[fname], SZ_PATHNAME)
	call ngtwrite (fd, Memc[fname], IMAGE(i), NEW_FILE, verbose)
	if (OBSTYPE(i)==ZERO) {
	    call sprintf (Memc[sfname], SZ_PATHNAME, "%sZ")
		call pargstr (Memc[fname])
	} else if (OBSTYPE(i)==DARK) {
	    call sprintf (Memc[sfname], SZ_PATHNAME, "%sD")
		call pargstr (Memc[fname])
	} else if (OBSTYPE(i)==FLAT) {
	    call sprintf (Memc[sfname], SZ_PATHNAME, "%sF")
		call pargstr (Memc[fname])
	} else if (OBSTYPE(i)==SFLAT) {
	    call sprintf (Memc[sfname], SZ_PATHNAME, "%sS")
		call pargstr (Memc[fname])
	} else {
	    call sprintf (Memc[sfname], SZ_PATHNAME, "%sO")
		call pargstr (Memc[fname])
	}

	do k = 1, nim-1 {
	    call strcpy (Memc[fname], Memc[fname1], SZ_PATHNAME)

	    # Create lists of calibration sequences.
	    if (OBSTYPE(i)==ZERO || OBSTYPE(i) == DARK ||
	        OBSTYPE(i)==FLAT || OBSTYPE(i)==SFLAT)
		call ngtwrite (sfd, Memc[sfname], Memc[fname], APPEND, verbose)

	    j = INDEX(k)
	    call fpathname (OBSID(j), Memc[fname], SZ_PATHNAME)
	    call ngtwrite (fd, Memc[fname], IMAGE(j), NEW_FILE, verbose)

	    # Here is where we determine the end of a sequence.
	    dt = (MJD(j) - MJD(i)) * 24 * 3600 - EXPTIME(i)
	    if (OBSTYPE(j)==OBJECT || OBSTYPE(i)!=OBSTYPE(j) ||
	        strne(FILTER(i),FILTER(j)) || strne(CCDSUM(i),CCDSUM(j)) ||
		NEXTEND(i)!=NEXTEND(j) ||
		(EXPTIME(i)!=EXPTIME(j) && OBSTYPE(i)!=SFLAT) ||
		dt>dtmax) {
		if (OBSTYPE(i)==ZERO || OBSTYPE(i)==DARK) {
		    call ngtwrite (zfd, Memc[zfname], Memc[sfname], APPEND,
		        verbose)
		} else if (OBSTYPE(i)==FLAT||OBSTYPE(i)==SFLAT) {
		    call ngtwrite (ffd, Memc[ffname], Memc[sfname], APPEND,
		        verbose)
		} else {
		    call ngtwrite (ofd, Memc[ofname], Memc[fname1], APPEND,
		        verbose)
		}
		if (sfd != NULL) {
		    call close (sfd)
		    sfd = NULL
		}
		if (OBSTYPE(j)==ZERO) {
		    call sprintf (Memc[sfname], SZ_PATHNAME, "%sZ")
			call pargstr (Memc[fname])
		} else if (OBSTYPE(j)==DARK) {
		    call sprintf (Memc[sfname], SZ_PATHNAME, "%sD")
			call pargstr (Memc[fname])
		} else if (OBSTYPE(j)==FLAT) {
		    call sprintf (Memc[sfname], SZ_PATHNAME, "%sF")
			call pargstr (Memc[fname])
		} else if (OBSTYPE(j)==SFLAT) {
		    call sprintf (Memc[sfname], SZ_PATHNAME, "%sS")
			call pargstr (Memc[fname])
		} else {
		    call sprintf (Memc[sfname], SZ_PATHNAME, "%sO")
			call pargstr (Memc[fname])
		}
	    }
	    i = j
	}

	# Finish up.
	if (OBSTYPE(i)==ZERO || OBSTYPE(i)==DARK ||
	    OBSTYPE(i)==FLAT || OBSTYPE(i)==SFLAT)
	    call ngtwrite (sfd, Memc[sfname], Memc[fname], APPEND, verbose)

	if (OBSTYPE(i)==ZERO || OBSTYPE(i)==DARK)
	    call ngtwrite (zfd, Memc[zfname], Memc[sfname], APPEND, verbose)
	else if (OBSTYPE(i)==FLAT || OBSTYPE(i)==SFLAT)
	    call ngtwrite (ffd, Memc[ffname], Memc[sfname], APPEND, verbose)
	else
	    call ngtwrite (ofd, Memc[ofname], Memc[fname], APPEND, verbose)

	if (sfd != NULL)
	    call close (sfd)
	if (zfd != NULL)
	    call close (zfd)
	if (ffd != NULL)
	    call close (ffd)
	if (ofd != NULL)
	    call close (ofd)

	call sfree (sp)
end


# NGTWRITE -- Write string to a file.
# If not previously opened delete and open.  Use mode to select whether to
# keep the file open for later calls.

procedure ngtwrite (fd, fname, value, mode, verbose)

int	fd		#U File descriptor (input NULL to create)
char	fname[ARB]	#I File name (ignored if fd not NULL)
char	value[ARB]	#I String value to write
int	mode		#I Mode for file (APPEND to leave fd open)
bool	verbose		#I Verbose?

int	access(), open()
errchk	open

begin
	if (verbose) {
	    call printf ("  %s ->\n    %s\n")
	        call pargstr (value)
		call pargstr (fname)
	}

	if (fd == NULL) {
	    if (access (fname, 0, 0) == YES)
		iferr (call delete (fname))
		    ;
	    fd = open (fname, mode, TEXT_FILE)
	}
	call fprintf (fd, "%s\n")
	    call pargstr (value)
	if (mode != APPEND) {
	    call close (fd)
	    fd = NULL
	}
end


# MJDCOMPARE -- Compare two MJD values for the GQSORT routine.

int procedure mjdcompare (mjd, i, j)

pointer	mjd			#I MJD pointer
int	i, j			#I Indices to compare

double	mjd1, mjd2

begin
	mjd1 = Memd[mjd+i]
	mjd2 = Memd[mjd+j]
	if (mjd1 < mjd2)
	    return (-1)
	if (mjd1 > mjd2)
	    return (1)
	return (0)
end
