# CATMATCH.H -- Catalog information required by the task.
# The mapping from actual catalogs to these quantities is handled by ACECAT.

define	ID_RA		0 # d hr %.2h
define	ID_DEC		2 # d deg %.1h
define	ID_G		4 # r mag %.2f
define	ID_R		5 # r mag %.2f
define	ID_I		6 # r mag %.2f
define	ID_X		8 # d pixels %.2f
define	ID_Y		10 # d pixels %.2f
define	ID_MAG		12 # r magnitudes %15.7g
define  ID_WA           13 # r pixels %.2f
define  ID_WB           14 # r pixels %.2f
define	ID_FLAGS	15 # 8 "" ""
define	ID_PTR		19 # ii		/ Pointer/integer for internal use

define	ACM_RA		RECD($1,ID_RA)
define	ACM_DEC		RECD($1,ID_DEC)
define	ACM_G		RECR($1,ID_G)
define	ACM_R		RECR($1,ID_R)
define	ACM_I		RECR($1,ID_I)
define	ACM_X		RECD($1,ID_X)
define	ACM_Y		RECD($1,ID_Y)
define	ACM_MAG		RECR($1,ID_MAG)
define  ACM_WA          RECR($1,ID_WA)
define  ACM_WB          RECR($1,ID_WB)
define  ACM_FLAGS       RECT($1,ID_FLAGS)
define	ACM_PTR		RECI($1,ID_PTR)

define	ACM_BP		RECC($1,ID_FLAGS,4)
