# SENDTRIG -- Send trigger data.

procedure sendtrig (input, rpath, output, comments, rtn, trigger)

file	input			{prompt="Input data file"}
file	rpath			{prompt="Return path"}
file	output			{prompt="Output data file"}
file	comments		{prompt="Output comments file"}
file	rtn			{prompt="Output return file"}
file	trigger			{prompt="Output trigger file"}
bool	verbose = yes		{prompt="Verbose?"}

begin
	int	ncomments

	if (input != "") {
	    # Check for comments.
	    match ("\#", input) | count | scan (ncomments)

	    # Send output data file.
	    if (ncomments == 0) {
		if (verbose)
		    printf ("  %s -> %s\n", input, output)
		copy (input, output)
	    } else {
		if (verbose)
		    printf ("  %s -> %s\n", input, output)
		match ("\#", input, stop+, > output)
		if (comments != "") {
		    if (verbose)
			printf ("  %s -> %s\n", input, comments)
		    match ("\#", input, > comments)
		}
	    }
	}

	# Send return file.
	if (rtn != "") {
	    if (verbose)
		printf ("  pathname %s -> %s\n", rpath, rtn)
	    pathname (rpath, > rtn)
	}

	# Send trigger file.
	if (trigger != "") {
	    if (verbose)
		printf ("  touch %s\n", trigger)
	    touch (trigger)
	}
end
