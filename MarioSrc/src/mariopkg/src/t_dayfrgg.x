include	<error.h>


# T_DAYFRGG -- This task computes global scales for a set of images over
# a set of extensions.  The scale information for each extension is given
# in a separate input file.  Each input file must have the same number
# of lines, one per image, in the same order.

procedure t_dayfrgg ()

int	inlist			# List of extension files
int	nlowrej			# Number of low scales to reject
int	nhighrej		# Number of high scales to reject

char	fname[SZ_FNAME]
int	i, j, nextn, nimages, in
real	scale, wt, sumscale, sumwts
pointer	scales, wts

int	clgeti(), clpopnu(), clplen(), clgfil(), open(), fscan()
errchk	open, realloc

begin

	# Get input parameters.
	inlist = clpopnu ("input")
	nlowrej = clgeti ("nlowrej")
	nhighrej = clgeti ("nhighrej")

	nextn = clplen (inlist)
	if (nextn == 0)
	    call error (1, "No extension files")

	# Initial allocation before we now how many images per file.
	nimages = 100
	call malloc (scales, nextn*nimages, TY_REAL)
	call malloc (wts, nextn*nimages, TY_REAL)

	iferr {
	    # Collect data.
	    j = 0
	    while (clgfil (inlist, fname, SZ_FNAME) != EOF) {
		in = open (fname, READ_ONLY, TEXT_FILE)
		i = 0
		while (fscan (in) != EOF) {
		    if (i == nimages) {
			if (j != 0)
			    call error (2,
			        "Extension files not the same length")
			nimages = nimages + 100
			call realloc (scales, nextn*nimages, TY_REAL)
			call realloc (wts, nextn*nimages, TY_REAL)
		    }
		    call gargwrd (fname, SZ_FNAME)
		    call gargwrd (fname, SZ_FNAME)
		    call gargr (Memr[wts+i*nextn+j])
		    call gargr (Memr[scales+i*nextn+j])
		    i = i + 1
		}
		call close (in)
		if (i == 0)
		    call error (3, "No images")
		    
		nimages = i
		j = j + 1
	    }

	    # Compute the global scales.
	    do i = 0, nimages-1 {
		call xt_sort2 (Memr[scales+i*nextn], Memr[wts+i*nextn], nextn)
		sumscale = 0.
		sumwts = 0.
		do j = nlowrej, nextn-nhighrej-1 {
		    scale = Memr[scales+i*nextn+j]
		    wt = Memr[wts+i*nextn+j]
		    sumscale = sumscale + wt * scale
		    sumwts = sumwts + wt
		}
		if (sumwts == 0.)
		    call error (4, "Bad weights")
		scale = sumscale / sumwts
		call printf ("%g\n")
		    call pargr (scale)
	    }
	} then {
	    call mfree (scales, TY_REAL)
	    call mfree (wts, TY_REAL)
	    call clpcls (inlist)
	    call erract (EA_ERROR)
	}
	  
	# Free memory.
	call mfree (scales, TY_REAL)
	call mfree (wts, TY_REAL)
	call clpcls (inlist)
end
